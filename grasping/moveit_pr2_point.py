import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from moveit_msgs.msg import RobotTrajectory
import trajectory_msgs.msg
import tf2_geometry_msgs
import tf2_ros
import time

print "============ Starting tutorial setup"
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial',
                anonymous=True)


robot = moveit_commander.RobotCommander()

#scene = moveit_commander.PlanningSceneInterface()

group = moveit_commander.MoveGroupCommander("right_arm")

display_trajectory_publisher = rospy.Publisher(
                                    '/move_group/display_planned_path',
                                    moveit_msgs.msg.DisplayTrajectory)

#print "============ Waiting for RVIZ..."
#rospy.sleep(10)
#print "============ Starting tutorial "

#print "============ Reference frame: %s" % group.get_planning_frame()

#print "============ Reference frame: %s" % group.get_end_effector_link()

#print "============ Robot Groups:"
#print robot.get_group_names()

#print "============ Printing robot state"
print robot.get_current_state()
#print "============"


pose_target = geometry_msgs.msg.Pose()
pose_target.orientation.w = 1.0
pose_target.position.x = 0.7
pose_target.position.y = -0.05
pose_target.position.z = 1.1
group.set_pose_target(pose_target)
    
    
    

plan1 = group.plan()
    #rospy.sleep(2)
    
    #print 'pikabuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu'
    
    #group.clear_pose_targets()
    
    #group_variable_values = group.get_current_joint_values()
    
    #group_variable_values = [0.16, 1.4, -0.4, 2.0, 1.0, 0.0]
    
    #group.set_joint_value_target(group_variable_values)
    
    #plan2 = group.plan()
    

    
print "============ Visualizing plan1"
display_trajectory = moveit_msgs.msg.DisplayTrajectory()

display_trajectory.trajectory_start = robot.get_current_state()
display_trajectory.trajectory.append(plan1)
display_trajectory_publisher.publish(display_trajectory);

print "============ Waiting while plan1 is visualized (again)..."
rospy.sleep(5)


#print plan1
    
   
        