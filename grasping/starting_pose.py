#!/usr/bin/env python

import time
import rospy
import tf
import tf2_ros
import numpy
import copy
import sys

import tf2_geometry_msgs
from std_msgs.msg  import Float64MultiArray

from ihmc_msgs.msg import FootstepStatusRosMessage
from ihmc_msgs.msg import FootstepDataListRosMessage
from ihmc_msgs.msg import FootstepDataRosMessage
from ihmc_msgs.msg import FootTrajectoryRosMessage
from ihmc_msgs.msg import PelvisHeightTrajectoryRosMessage
from ihmc_msgs.msg import TrajectoryPoint1DRosMessage
from ihmc_msgs.msg import WholeBodyTrajectoryRosMessage
from ihmc_msgs.msg import ArmTrajectoryRosMessage
from ihmc_msgs.msg import OneDoFJointTrajectoryRosMessage
from ihmc_msgs.msg import NeckTrajectoryRosMessage
from ihmc_msgs.msg import SE3TrajectoryPointRosMessage
from ihmc_msgs.msg import SO3TrajectoryPointRosMessage
from ihmc_msgs.msg import  HandTrajectoryRosMessage
from ihmc_msgs.msg import  ChestTrajectoryRosMessage
from srcsim.msg._Console import Console

from geometry_msgs.msg import Vector3
from geometry_msgs.msg import Vector3Stamped
from geometry_msgs.msg import Quaternion


LEFT = 0
RIGHT = 1

ROBOT_NAME = None
LEFT_FOOT_FRAME_NAME = None
RIGHT_FOOT_FRAME_NAME = None


ZERO_VECTOR = [0.3, 1.3, -0.1, 1.3, 0.0, 0.0, 0.0]
ARM_PULLED_BACK = [-0.2, 1.4, 0.6, 1.3, 0.0, 0.0, 0.0]
ARM_PULLED_BACK_LEFT = [0.2, -1.45, 0.1, -1.65, 0.0, 0.0, 0.0]
ARM_PULLED_BACK_INIT = [0.9, 1.4, -0.1, 0.0, 0.0, 0.0, 0.0]
ARM_PULLED_BACK_INIT2 = [0.3, 1.3, -0.1, 0.65, 0.0, 0.0, 0.0]
ELBOW_BENT_UP = [-1.10, 0.85, 1.3, 0.0, 0.0, 0.0, 0.0]
ELBOW_BENT_UP_MIDDLE = [-1.10, 0.25, 1.3, 0.3, 0.0, 0.0, 0.0]
#ELBOW_BENT_UP_MIDDLE = [-0.1, 0.4, 0.56, 1.7, 0.0, 0.0, 0.0]
#touch ELBOW_BENT_UP = [1.8, -0.3, -1.5, 1.15, 0.0, 0.0, 0.0]
ELBOW_BENT_UP_INIT = [1.8, 0.4, -1.5, 0.95, 0.0, 0.0, 0.0]

ARM_DOWN_LEFT = [0.16, -1.4, 0.56, -1.7, 0.0, 0.0, 0.0]
ARM_DOWN_RIGHT = [0.16, 1.4, 0.56, 1.7, 0.0, 0.0, 0.0]
#was -0.22
ARM_DOWN_RIGHT_2 = [-0.1, 1.4, -0.18, 1.7, 0.0, 0.0, 0.0] #was [-0.1, 1.4, -0.35, 1.7, 0.0, 0.0, 0.0]
ARM_DOWN_RIGHT_3 = [-1.1, 1.5, -1.50, 1.05, 0.0, 0.0, 0.0] #not used currently- was [-1.1, 1.5, -1.55, 1.05, 0.0, 0.0, 0.0]
ARM_DOWN_RIGHT_INIT = [-1.10, 0.05, 1.3, 0.0, 0.0, 0.0, 0.0]
ARM_DOWN_STRETCHED_LEFT = [0.0, -1.4, 1.4, 0.0, 0.0, 0.0, 0.0]
ARM_DOWN_STRETCHED_RIGHT = [0.0, 1.4, 1.4, 0.0, 0.0, 0.0, 0.0]

ARM_SPREAD_LEFT = [0.16, -1.4, -0.4, -2.0, 1.0, 0.0, 0.0]
ARM_SPREAD_RIGHT = [0.16, 1.4, -0.4, 2.0, 1.0, 0.0, 0.0]
ARM_SPREAD_LEFT_WIDE = [0.16, -1.0, -0.4, -2.0, 1.0, 0.0, 0.0]
ARM_SPREAD_RIGHT_WIDE = [0.16, 1.0, -0.4, 2.0, 1.0, 0.0, 0.0]

class Position:
    x = 0.0
    y = 0.0
    z = 0.0

blueWheelPosition = Position()
redWheelPosition = Position()


def walkTest():       
    
   



    pelvisMsg2 = PelvisHeightTrajectoryRosMessage()
    pelvisMsg2.execution_mode = 0
    pelvisMsg2.unique_id = -1
    
    pelvisTraj2 = TrajectoryPoint1DRosMessage()
    pelvisTraj2.time = 1.0
    pelvisTraj2.position = 0.87
    pelvisTraj2.velocity = 0.0
    
    pelvisMsg2.trajectory_points.append(pelvisTraj2)
    
    rospy.loginfo('setting pelvis height...')
    pelvisHeightTrajectoryPublisher.publish(pelvisMsg2) 

    #neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([1.08, 0.0, -0.22], 1.0))
        
    #rospy.loginfo('finished walking')
    
    #rossleep(0.45)
   
    

    
    target_pose = tf2_geometry_msgs.PoseStamped()

   
    target_pose.header.frame_id = 'pelvis'
    target_pose.header.stamp = rospy.Time()
    
    #dir_quat = tf.transformations.quaternion_from_euler(0.0,0.4,0.4)
    
    dir_quat = tf.transformations.quaternion_from_euler(0.0,0.4,0.0)
    
    target_pose.pose.orientation.x = dir_quat[0]
    target_pose.pose.orientation.y = dir_quat[1]
    target_pose.pose.orientation.z = dir_quat[2]
    target_pose.pose.orientation.w = dir_quat[3]
    
    
    
    transformed_target_pose = tfBuffer.transform(target_pose, 'world',rospy.Duration(1.0))  

    chest_dir = tf.transformations.euler_from_quaternion([transformed_target_pose.pose.orientation.x,
                                                          transformed_target_pose.pose.orientation.y,
                                                          transformed_target_pose.pose.orientation.z,
                                                          transformed_target_pose.pose.orientation.w])
    
    
    #print chest_dir
    #time.sleep(2)
    
    
    
    rospy.loginfo('setting chest trajectory...')
    chestTrajectoryPublisher.publish(createChestMessage([chest_dir[2],chest_dir[1],chest_dir[0]], 1.0))



    print 'neeeeck'
    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([0.55,0.0,0], 1.0))
  
    
    
    sendArmTrajectory('SPREAD')

    
    
   

# Creates footstep with the current position and orientation of the foot.
def createFootStepInPlace(stepSide, swingHeight):
    footstep = FootstepDataRosMessage()
    footstep.robot_side = stepSide
    footstep.swing_height = swingHeight
    
    if stepSide == LEFT:
        foot_frame = LEFT_FOOT_FRAME_NAME
    else:
        foot_frame = RIGHT_FOOT_FRAME_NAME

    footWorld = tfBuffer.lookup_transform('world', foot_frame, rospy.Time())
    footstep.orientation = footWorld.transform.rotation
    footstep.location = footWorld.transform.translation

    return footstep

# Creates footstep offset from the current foot position. The offset is in foot frame.
def createFootStepOffset(stepSide, offset, swingHeight, turn):
    footstep = createFootStepInPlace(stepSide, swingHeight)

#    pista_quaternion = tf.transformations.quaternion_from_euler(turn, 0, 0, 'rzyx')
    
#    footstep.orientation.x = pista_quaternion[0]
#    footstep.orientation.y = pista_quaternion[1]
#    footstep.orientation.z = pista_quaternion[2]
#    footstep.orientation.w = pista_quaternion[3]

    # transform the offset to world frame
    quat = footstep.orientation
    rot = tf.transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
    transformedOffset = numpy.dot(rot[0:3, 0:3], offset)

    footstep.location.x += transformedOffset[0]
    footstep.location.y += transformedOffset[1]
    footstep.location.z += transformedOffset[2]


    pista_quaternion = tf.transformations.quaternion_from_euler(turn, 0, 0, 'rzyx')
    
    footstep.orientation.x = pista_quaternion[0]
    footstep.orientation.y = pista_quaternion[1]
    footstep.orientation.z = pista_quaternion[2]
    footstep.orientation.w = pista_quaternion[3]
    
    
#    footstep.location.x += offset[0]
#    footstep.location.y += offset[1]
#    footstep.location.z += offset[2]
    
    
#    footstep.trajectory_type = FootstepDataRosMessage.PUSH_RECOVERY

    return footstep

def waitForFootsteps(numberOfSteps):
    global stepCounter
    stepCounter = 0
    while stepCounter < numberOfSteps:
        rate.sleep()

    rospy.loginfo('finished set of steps')

def recievedFootStepStatus(msg):
    global stepCounter
    if msg.status == 1:
        stepCounter += 1
        
def receivedBlueWheelPosition(msg):
    rospy.loginfo('received x coordinate [%f]' % msg.x)
    rospy.loginfo('received y coordinate [%f]' % msg.y)
    rospy.loginfo('received z coordinate [%f]' % msg.z)
    blueWheelPosition.x = msg.x
    blueWheelPosition.y = msg.y
    blueWheelPosition.z = msg.z

def sendArmTrajectory(mode):   
    if mode == 'PUSH':
        msg = ArmTrajectoryRosMessage()
        msg.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg = appendTrajectoryPoint(msg, 0.12, ELBOW_BENT_UP_MIDDLE)
        msg = appendTrajectoryPoint(msg, 0.20, ELBOW_BENT_UP)
        msg.unique_id = -1
        rospy.loginfo('publishing right PUSH trajectory')
        armTrajectoryPublisher.publish(msg)

    elif mode == 'PULL':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 0.5, ARM_PULLED_BACK_INIT2)
        msg2 = appendTrajectoryPoint(msg2, 1.0, ARM_DOWN_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right PULL trajectory')
        armTrajectoryPublisher.publish(msg2)
        
        msg3 = ArmTrajectoryRosMessage()
        msg3.robot_side = ArmTrajectoryRosMessage.LEFT
        msg3 = appendTrajectoryPoint(msg3, 0.6, ARM_DOWN_LEFT)
        msg3.unique_id = -2
        rospy.loginfo('publishing left PULL trajectory')
        armTrajectoryPublisher.publish(msg3)

    elif mode == 'DOWN':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 1.0, ARM_DOWN_RIGHT_2)
        msg2.unique_id = -1
        rospy.loginfo('publishing right DOWN trajectory')
        armTrajectoryPublisher.publish(msg2)

        rossleep(0.4)
        
        msg3 = ArmTrajectoryRosMessage()
        msg3.robot_side = ArmTrajectoryRosMessage.LEFT
        msg3 = appendTrajectoryPoint(msg3, 1.0, ARM_DOWN_LEFT)
        msg3.unique_id = -2
        rospy.loginfo('publishing left DOWN trajectory')
        armTrajectoryPublisher.publish(msg3)

    elif mode == 'RIGHTDOWN':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 0.25, ARM_DOWN_RIGHT_INIT)
        msg2 = appendTrajectoryPoint(msg2, 0.60, ARM_DOWN_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right DOWN trajectory')
        armTrajectoryPublisher.publish(msg2)

    elif mode == 'SPREAD':
        msg1 = ArmTrajectoryRosMessage()
        msg1.robot_side = ArmTrajectoryRosMessage.LEFT
        msg1 = appendTrajectoryPoint(msg1, 0.60, ARM_SPREAD_LEFT_WIDE)
        msg1.unique_id = -1
        rospy.loginfo('publishing left SPREAD trajectory')
        armTrajectoryPublisher.publish(msg1)

        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 0.60, ARM_SPREAD_RIGHT_WIDE)
        msg2.unique_id = -1
        rospy.loginfo('publishing right SPREAD trajectory')
        armTrajectoryPublisher.publish(msg2)

def appendTrajectoryPoint(arm_trajectory, time, positions):
    if not arm_trajectory.joint_trajectory_messages:
        arm_trajectory.joint_trajectory_messages = [copy.deepcopy(OneDoFJointTrajectoryRosMessage()) for i in range(len(positions))]
    for i, pos in enumerate(positions):
        point = TrajectoryPoint1DRosMessage()
        point.time = time
        point.position = pos
        point.velocity = 0
        arm_trajectory.joint_trajectory_messages[i].trajectory_points.append(point)
    return arm_trajectory

def createNeckTrajectoryMessage(arr, secs):
    neckTrajPoint1 = TrajectoryPoint1DRosMessage()
    neckTrajPoint1.time = secs
    neckTrajPoint1.position = arr[0]
    neckTraj1 = OneDoFJointTrajectoryRosMessage()
    neckTraj1.trajectory_points.append(neckTrajPoint1)

    neckTrajPoint2 = TrajectoryPoint1DRosMessage()
    neckTrajPoint2.time = secs
    neckTrajPoint2.position = arr[1]
    neckTraj2 = OneDoFJointTrajectoryRosMessage()
    neckTraj2.trajectory_points.append(neckTrajPoint2)

    neckTrajPoint3 = TrajectoryPoint1DRosMessage()
    neckTrajPoint3.time = secs
    neckTrajPoint3.position = arr[2]
    neckTraj3 = OneDoFJointTrajectoryRosMessage()
    neckTraj3.trajectory_points.append(neckTrajPoint3)
    
    neckMsg = NeckTrajectoryRosMessage()
    neckMsg.unique_id = -1
    neckMsg.joint_trajectory_messages.append(neckTraj1)
    neckMsg.joint_trajectory_messages.append(neckTraj2)
    neckMsg.joint_trajectory_messages.append(neckTraj3)

    return neckMsg

def createChestMessage(arr, time):    
    chestMsg = ChestTrajectoryRosMessage()
    chestMsg.execution_mode = 0
    chestMsg.unique_id = -1
    
    chestTraj = SO3TrajectoryPointRosMessage()
    chestTraj.time = time
    
    pista_quaternion = tf.transformations.quaternion_from_euler(arr[0], arr[1], arr[2], 'rzyx')
    
    chestTraj.orientation.x = pista_quaternion[0]
    chestTraj.orientation.y = pista_quaternion[1]
    chestTraj.orientation.z = pista_quaternion[2]
    chestTraj.orientation.w = pista_quaternion[3]    
    
    chestMsg.taskspace_trajectory_points.append(chestTraj)
    
    return chestMsg

def rossleep(time):
    try:
        seconds = float(time)
    except (ValueError):
        print("Couldn't convert argument [%s] to float" % sys.argv[1])
    rospy.sleep(rospy.Duration.from_sec(seconds))

if __name__ == '__main__':
    try:
        rospy.init_node('ihmc_walk_test_pista_start')

        if not rospy.has_param('/ihmc_ros/robot_name'):
            rospy.logerr("Cannot run walk_test.py, missing parameters!")
            rospy.logerr("Missing parameter '/ihmc_ros/robot_name'")

        else:
            ROBOT_NAME = rospy.get_param('/ihmc_ros/robot_name')

            right_foot_frame_parameter_name = "/ihmc_ros/{0}/right_foot_frame_name".format(ROBOT_NAME)
            left_foot_frame_parameter_name = "/ihmc_ros/{0}/left_foot_frame_name".format(ROBOT_NAME)

            if rospy.has_param(right_foot_frame_parameter_name) and rospy.has_param(left_foot_frame_parameter_name):
                RIGHT_FOOT_FRAME_NAME = rospy.get_param(right_foot_frame_parameter_name)
                LEFT_FOOT_FRAME_NAME = rospy.get_param(left_foot_frame_parameter_name)

                footStepStatusSubscriber = rospy.Subscriber("/ihmc_ros/{0}/output/footstep_status".format(ROBOT_NAME), FootstepStatusRosMessage, recievedFootStepStatus)
                blueWheelPositionSubscrber = rospy.Subscriber("/srcsim/qual1/light", Console, receivedBlueWheelPosition)
                
                footStepListPublisher = rospy.Publisher("/ihmc_ros/{0}/control/footstep_list".format(ROBOT_NAME), FootstepDataListRosMessage, queue_size=20)
                footTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/foot_trajectory".format(ROBOT_NAME), FootTrajectoryRosMessage, queue_size=20)
                pelvisHeightTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/pelvis_height_trajectory".format(ROBOT_NAME), PelvisHeightTrajectoryRosMessage, queue_size=20)
                wholeBodyTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/whole_body_trajectory".format(ROBOT_NAME), WholeBodyTrajectoryRosMessage, queue_size=50)
                armTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/arm_trajectory".format(ROBOT_NAME), ArmTrajectoryRosMessage, queue_size=1)
                neckTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/neck_trajectory".format(ROBOT_NAME), NeckTrajectoryRosMessage, queue_size=1)
                handTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/hand_trajectory".format(ROBOT_NAME), HandTrajectoryRosMessage, queue_size=20)
                chestTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/chest_trajectory".format(ROBOT_NAME), ChestTrajectoryRosMessage, queue_size=20)
                rightHandPublisher = rospy.Publisher("/right_hand_position_controller/command", Float64MultiArray, queue_size=20)
                
                tfBuffer = tf2_ros.Buffer()
                
                tfListener = tf2_ros.TransformListener(tfBuffer)

                rate = rospy.Rate(10) # 10hz
                time.sleep(1)

                # make sure the simulation is running otherwise wait
                if footStepListPublisher.get_num_connections() == 0:
                    rospy.loginfo('waiting for subsciber...')
                    while footStepListPublisher.get_num_connections() == 0:
                        rate.sleep()

                if not rospy.is_shutdown():
                    walkTest()
            else:
                if not rospy.has_param(left_foot_frame_parameter_name):
                    rospy.logerr("Missing parameter {0}".format(left_foot_frame_parameter_name))
                if not rospy.has_param(right_foot_frame_parameter_name):
                    rospy.logerr("Missing parameter {0}".format(right_foot_frame_parameter_name))

    except rospy.ROSInterruptException:
        pass