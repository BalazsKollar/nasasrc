#!/usr/bin/env python

import time
import rospy
import tf
import tf2_ros
import numpy
import copy
import sys



import moveit_commander
import tf2_geometry_msgs
import geometry_msgs.msg
from moveit_msgs.msg import RobotTrajectory


from ihmc_msgs.msg import ArmTrajectoryRosMessage
from ihmc_msgs.msg import TrajectoryPoint1DRosMessage
from ihmc_msgs.msg import OneDoFJointTrajectoryRosMessage


#==========================================================================================

moveit_commander.roscpp_initialize(sys.argv)


robot = moveit_commander.RobotCommander()




#==========================================================================================

LEFT = 0
RIGHT = 1

ROBOT_NAME = None

#x = 0.411880
#y = -0.526453
#z = 1.224489

#roll = 0
#pitch = 0
#yaw = numpy.pi/2

#read in x,y,z,roll,pitch,yaw values from command line

x = float(sys.argv[1])
y = float(sys.argv[2])
z = float(sys.argv[3])

roll = float(sys.argv[4])
pitch = float(sys.argv[5])
yaw = float(sys.argv[6])

arm_choice = sys.argv[7];

if arm_choice=='LEFT':
    group = moveit_commander.MoveGroupCommander("left_arm")
else:
    group = moveit_commander.MoveGroupCommander("right_arm")


def move_arm():       
                   

    #initialisie stamped point of target in world frame
    
    target_point = tf2_geometry_msgs.PointStamped()
    transformed_target_point = tf2_geometry_msgs.PointStamped()
    target_point.header.frame_id = 'world'
    target_point.header.stamp = rospy.Time()
    
    #transform target into the pelvis coordinate system
    
    target_point.point.x = x
    target_point.point.y = y
    target_point.point.z = z
    
    transformed_target_point = tfBuffer.transform(target_point, 'pelvis')  
    
    #print transformed_target_point


    #the planner only takes in coordinates in pose format
    target_pose = geometry_msgs.msg.Pose()
    target_pose.position.x = transformed_target_point.point.x
    target_pose.position.y = transformed_target_point.point.y
    target_pose.position.z = transformed_target_point.point.z
    
    #convert roll, pitch, yaw (pelvis system) to quaternion for the planner
    

    
    dir_quat = tf.transformations.quaternion_from_euler(roll, pitch, yaw) #roll pitch yaw
    
    
    target_pose.orientation.x = dir_quat[0]
    target_pose.orientation.y = dir_quat[1]
    target_pose.orientation.z = dir_quat[2]
    target_pose.orientation.w = dir_quat[3]
    
    


    
    current_pose = group.get_current_pose()
    
    print 'current pose'
    print current_pose
    print 'target pose'
    print target_pose
    
    #planning 
    
    group.set_start_state_to_current_state()
    group.set_pose_target(target_pose)
    plan1 = RobotTrajectory()
    
    #max planning time (time out criteria)
    group.set_planning_time(40)
    
    #set target tolerance (meter and radian)
    group.set_goal_position_tolerance(0.01) #meters
    group.set_goal_orientation_tolerance(0.1) #radians
    plan1 = group.plan()
    
    #print plan1    
    
    #execute the joint value plan the planner came up with
    
    msg = ArmTrajectoryRosMessage() 
    
    if arm_choice=='LEFT':
        msg.robot_side = ArmTrajectoryRosMessage.LEFT
    else:
        msg.robot_side = ArmTrajectoryRosMessage.RIGHT
    
    
    for i in range(0,len(plan1.joint_trajectory.points)):
        
        armpositions = list(plan1.joint_trajectory.points[i].positions)
        armpositions.append(0.0)
        postime = float(str(plan1.joint_trajectory.points[i].time_from_start))*(1e-9)
        
        
        msg = appendTrajectoryPoint(msg, postime, armpositions)
        msg.unique_id = -1
        rospy.loginfo('appending planner point')
        
        
    rospy.loginfo('publishing planner points')
    armTrajectoryPublisher.publish(msg)
    


def appendTrajectoryPoint(arm_trajectory, time, positions):
    if not arm_trajectory.joint_trajectory_messages:
        arm_trajectory.joint_trajectory_messages = [copy.deepcopy(OneDoFJointTrajectoryRosMessage()) for i in range(len(positions))]
    for i, pos in enumerate(positions):
        point = TrajectoryPoint1DRosMessage()
        point.time = time
        point.position = pos
        point.velocity = 0
        arm_trajectory.joint_trajectory_messages[i].trajectory_points.append(point)
    return arm_trajectory



def rossleep(time):
    try:
        seconds = float(time)
    except (ValueError):
        print("Couldn't convert argument [%s] to float" % sys.argv[1])
    rospy.sleep(rospy.Duration.from_sec(seconds))

if __name__ == '__main__':
    try:
        rospy.init_node('move_arm')

        if not rospy.has_param('/ihmc_ros/robot_name'):
            rospy.logerr("Cannot run move_arm.py, missing parameters!")
            rospy.logerr("Missing parameter '/ihmc_ros/robot_name'")

        else:
            ROBOT_NAME = rospy.get_param('/ihmc_ros/robot_name')
            armTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/arm_trajectory".format(ROBOT_NAME), ArmTrajectoryRosMessage, queue_size=100) 
            tfBuffer = tf2_ros.Buffer()
            tfListener = tf2_ros.TransformListener(tfBuffer)

            rate = rospy.Rate(10) # 10hz
            time.sleep(1)

                # make sure the simulation is running otherwise wait


            if not rospy.is_shutdown():
                    move_arm()
                    
    except rospy.ROSInterruptException:
        pass
