#!/usr/bin/env python

import time
import rospy
import tf
import tf2_ros
import numpy
import copy
import sys



import moveit_commander
import tf2_geometry_msgs
import geometry_msgs.msg
from moveit_msgs.msg import RobotTrajectory
from std_msgs.msg import String


from ihmc_msgs.msg import ArmTrajectoryRosMessage
from ihmc_msgs.msg import TrajectoryPoint1DRosMessage
from ihmc_msgs.msg import OneDoFJointTrajectoryRosMessage




LEFT = 0
RIGHT = 1

ROBOT_NAME = None

#x = 0.411880
#y = -0.526453
#z = 1.224489

#roll = 0
#pitch = 0
#yaw = numpy.pi/2

#read in x,y,z,roll,pitch,yaw values from command line

#x = float(sys.argv[1])
#y = float(sys.argv[2])
#z = float(sys.argv[3])

#roll = float(sys.argv[4])
#pitch = float(sys.argv[5])
#yaw = float(sys.argv[6])

#arm_choice = sys.argv[7];
    

def callback(data):
    
    rospy.loginfo(rospy.get_caller_id() + ' Executing %s', data.data)
    
    string_command = data.data
    
    separated_cmd = string_command.split(',')
    
    x = float(separated_cmd[0])
    y = float(separated_cmd[1])
    z = float(separated_cmd[2])
    roll = float(separated_cmd[3])
    pitch = float(separated_cmd[4])
    yaw = float(separated_cmd[5])
    position_accuracy = float(separated_cmd[6])
    angle_accuracy = float(separated_cmd[7])
    try:
        last_element = str(separated_cmd[8])
    except IndexError:
        last_element = 'all'
                
    try:
        quick_exe = str(separated_cmd[9])
    except IndexError:
        quick_exe = 'slow'
    


    #the planner only takes in coordinates in pose format
    target_pose = geometry_msgs.msg.Pose()
    target_pose.position.x = x
    target_pose.position.y = y
    target_pose.position.z = z
    
    #convert roll, pitch, yaw (pelvis system) to quaternion for the planner
    
    dir_quat = tf.transformations.quaternion_from_euler(roll, pitch, yaw) #roll pitch yaw
    
    
    target_pose.orientation.x = dir_quat[0]
    target_pose.orientation.y = dir_quat[1]
    target_pose.orientation.z = dir_quat[2]
    target_pose.orientation.w = dir_quat[3]
    
    
    
    #current_pose = geometry_msgs.msg.Pose()
    
    current_pose = group.get_current_pose()
    #current_pose.header.frame_id = 'pelvis'
    #transformed_current_pose = tfBuffer.transform(current_pose, 'world')  
    
    print 'current pose'
    print current_pose
    #print 'current world pose'
    #print transformed_current_pose

    print 'target pose'
    print target_pose
    
    print 'angle accuracy'
    print angle_accuracy
    
    print 'position accuracy'
    print position_accuracy
    
    if last_element=='last':
        print 'executing last element of plan only'
    else:
        print 'executing entire plan'
        
    if quick_exe == 'quick':
        print 'quick mode'
    
    #planning 
    
    group.set_start_state_to_current_state()
    group.set_pose_target(target_pose)
    plan1 = RobotTrajectory()
    
    #max planning time (time out criteria)
    group.set_planning_time(30)
    
    #set target tolerance (meter and radian)
    group.set_goal_position_tolerance(position_accuracy) #meters
    group.set_goal_orientation_tolerance(angle_accuracy) #radians
    plan1 = group.plan()
    
    #print plan1    
    
    #execute the joint value plan the planner came up with
    
    msg = ArmTrajectoryRosMessage() 

    msg.robot_side = ArmTrajectoryRosMessage.LEFT
    
    if last_element=='last':
        for_start = len(plan1.joint_trajectory.points)-1
    else:
        for_start = 0
    
    if not len(plan1.joint_trajectory.points) == 0:
        
        for i in range(for_start,len(plan1.joint_trajectory.points)):
            
            armpositions = list(plan1.joint_trajectory.points[i].positions)
            if quick_exe == 'quick':
                postime = 0
            else:
                postime = float(str(plan1.joint_trajectory.points[i].time_from_start))*(1e-9)
            
            armpositions_checked = armlimitcheck(armpositions,'left')
            msg = appendTrajectoryPoint(msg, postime, armpositions_checked)
            msg.unique_id = -1
            rospy.loginfo('appending planner point')
            
            
        rospy.loginfo('publishing planner points')
        armTrajectoryPublisher.publish(msg)


def listener():      

    rospy.Subscriber('left_arm_moveit_position', String, callback)
    
    
    rospy.spin() 
    
def armlimitcheck(positionslist,arm):
    
    positions = numpy.array(positionslist)
    
    R_upper_limits = numpy.array([2.0,1.519,2.18,2.174,3.14,0.62,0.36])
    R_lower_limits = numpy.array([-2.85,-1.266,-3.1,-0.12,-2.019,-0.625,-0.49])
    L_lower_limits = numpy.array([-2.0,-1.519,-2.18,-2.174,-3.14,-0.62,-0.36])
    L_upper_limits = numpy.array([2.85,1.266,3.1,0.12,2.019,0.625,0.49])
    
    if arm == 'right':
        for i in range(0,7):
            if positions[i] < R_lower_limits[i]:
                positions[i] = R_lower_limits[i]
            if positions[i] > R_upper_limits[i]:
                positions[i] = R_upper_limits[i]

                
    if arm == 'left':
        for i in range(0,7):
            if positions[i] < L_lower_limits[i]:
                positions[i] = L_lower_limits[i]
            if positions[i] > L_upper_limits[i]:
                positions[i] = L_upper_limits[i]    
                            
    return positions
        


def appendTrajectoryPoint(arm_trajectory, time, positions):
    if not arm_trajectory.joint_trajectory_messages:
        arm_trajectory.joint_trajectory_messages = [copy.deepcopy(OneDoFJointTrajectoryRosMessage()) for i in range(len(positions))]
    for i, pos in enumerate(positions):
        point = TrajectoryPoint1DRosMessage()
        point.time = time
        point.position = pos
        point.velocity = 0
        arm_trajectory.joint_trajectory_messages[i].trajectory_points.append(point)
    return arm_trajectory



def rossleep(time):
    try:
        seconds = float(time)
    except (ValueError):
        print("Couldn't convert argument [%s] to float" % sys.argv[1])
    rospy.sleep(rospy.Duration.from_sec(seconds))

if __name__ == '__main__':
    try:
        rospy.init_node('moveit_left_arm_listener')

        if not rospy.has_param('/ihmc_ros/robot_name'):
            rospy.logerr("Cannot run moveit_left_arm_listener.py, missing parameters!")
            rospy.logerr("Missing parameter '/ihmc_ros/robot_name'")

        else:
            ROBOT_NAME = rospy.get_param('/ihmc_ros/robot_name')
            armTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/arm_trajectory".format(ROBOT_NAME), ArmTrajectoryRosMessage, queue_size=100) 
            tfBuffer = tf2_ros.Buffer()
            tfListener = tf2_ros.TransformListener(tfBuffer)

            rate = rospy.Rate(10) # 10hz
            time.sleep(1)

                # make sure the simulation is running otherwise wait


            if not rospy.is_shutdown():
                    moveit_commander.roscpp_initialize(sys.argv)
                    robot = moveit_commander.RobotCommander()
                    group = moveit_commander.MoveGroupCommander("left_arm")
                    listener()
                    
    except rospy.ROSInterruptException:
        pass
