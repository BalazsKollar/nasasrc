import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from moveit_msgs.msg import RobotTrajectory
import trajectory_msgs.msg
import tf2_geometry_msgs
import tf2_ros
import time

print "============ Starting tutorial setup"
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial',
                anonymous=True)


robot = moveit_commander.RobotCommander()

#scene = moveit_commander.PlanningSceneInterface()

group = moveit_commander.MoveGroupCommander("right_arm")

display_trajectory_publisher = rospy.Publisher(
                                    '/move_group/display_planned_path',
                                    moveit_msgs.msg.DisplayTrajectory)

#print "============ Waiting for RVIZ..."
#rospy.sleep(10)
#print "============ Starting tutorial "

#print "============ Reference frame: %s" % group.get_planning_frame()

#print "============ Reference frame: %s" % group.get_end_effector_link()

#print "============ Robot Groups:"
#print robot.get_group_names()

#print "============ Printing robot state"
#print robot.get_current_state()
#print "============"


#starterpose = group.get_current_joint_values()
#get_current_pose().pose

#print starterpose

#print "============ Generating plan 1"

#proba1
pose_target = geometry_msgs.msg.Pose()
pose_target.orientation.x = 0.225084991626
pose_target.orientation.y = 0.11737558342
pose_target.orientation.z = 0.921035950753
pose_target.orientation.w = 0.295385335422
pose_target.position.x = 0.461708889261
pose_target.position.y = -0.133353040622
pose_target.position.z = 0.0962227677894

#starter
pose_target2 = geometry_msgs.msg.Pose()
pose_target2.orientation.x = 0.0
pose_target2.orientation.y = 0.0
pose_target2.orientation.z = 0.0
pose_target2.orientation.w = 1.0
pose_target2.position.x = 0.01031
pose_target2.position.y = -0.867
pose_target2.position.z = 0.3187

#can


pose_target3 = geometry_msgs.msg.Pose()
pose_target3.orientation.x = 0.0
pose_target3.orientation.y = 0.0
pose_target3.orientation.z = 0.0
pose_target3.orientation.w = 1.0
pose_target3.position.x = 0.348
pose_target3.position.y = -0.088
pose_target3.position.z = 0.92


#group.set_start_state_to_current_state()

#group.set_pose_target(pose_target)

#plan1 = RobotTrajectory()

#plan1 = group.plan()

#janika = trajectory_msgs.msg.JointTrajectory()

#janika = trajectory_msgs.msg.JointTrajectoryPoint()

#for i in range(0, len(plan1.joint_trajectory.points)):
    

#print 'pityuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu'
#print plan1.joint_trajectory.points[20].positions
#print plan1.joint_trajectory.points[20].time_from_start
#print 'pistaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'
#print plan1.joint_trajectory.points[1]

if 1==1:
    
    janipont = tf2_geometry_msgs.PointStamped()
    trafojanipont = tf2_geometry_msgs.PointStamped()
    
    janipont.header.frame_id = 'pelvis'
    
    janipont.header.stamp = rospy.Time()
    
    
    
    #updateelve rendes sor a kezben (pelvis)
    janipont.point.x = 0.259560
    janipont.point.y = -0.051313
    janipont.point.z = 0.052779
    
    #faszom
    #janipont.point.x = 0.0
    #janipont.point.y = 0.0
    #janipont.point.z = 0.1
    
    #tfBuffer = tf2_ros.Buffer()
    #tfListener = tf2_ros.TransformListener(tfBuffer)
    
    #time.sleep(1)
    
    #trafojanipont = tfBuffer.transform(janipont, 'pelvis')
    
    trafojanipont = janipont
 
    print trafojanipont   
   
    
    janipont = tf2_geometry_msgs.PointStamped()
    trafojanipont = tf2_geometry_msgs.PointStamped()
    
    janipont.header.frame_id = 'world'
    
    janipont.header.stamp = rospy.Time()
    
    #janipont.point.x = 3.288547
    #janipont.point.y = 0.642
    #janipont.point.z = 0.945
    
    #updateelve rendes sor a kezben
    janipont.point.x = 0.259560
    janipont.point.y = -0.051313
    janipont.point.z = 0.952779
    
    #trafojanipont = tfBuffer.transform(janipont, 'pelvis')
 
    print trafojanipont   
   
    
    trafojanipose = geometry_msgs.msg.Pose()
    
    
    #trafojanipose.position.x = trafojanipont.point.x
    #trafojanipose.position.y = trafojanipont.point.y
    #trafojanipose.position.z = trafojanipont.point.z
    
    trafojanipose.position.x = 0.32656089
    trafojanipose.position.y = -0.1998773
    trafojanipose.position.z = -0.0483590
    
    
    #trafojanipose.orientation.x = -0.031511
    #trafojanipose.orientation.y = 0.096090
    #trafojanipose.orientation.z = 0.8863615
    #trafojanipose.orientation.w = 0.451815

    
    mostani = group.get_current_pose()
    
    print 'mostani'
    
    print mostani
    
    print 'janipose'
    
    print trafojanipose
    
    group.set_start_state_to_current_state()
    
    
    
    group.set_pose_target(trafojanipose)

    plan1 = RobotTrajectory()
    
    group.set_planning_time(40)
    
    group.set_goal_orientation_tolerance(100)
    

    plan1 = group.plan()
    
    print plan1

    
    print 'pikabuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu'
    
    
    #group.clear_pose_targets()
    
    #group_variable_values = group.get_current_joint_values()
    
    #group_variable_values = [0.16, 1.4, -0.4, 2.0, 1.0, 0.0]
    
    #group.set_joint_value_target(group_variable_values)
    
    #plan2 = group.plan()
    

    
    print "============ Visualizing plan1"
    display_trajectory = moveit_msgs.msg.DisplayTrajectory()

    display_trajectory.trajectory_start = robot.get_current_state()
    display_trajectory.trajectory.append(plan1)
    display_trajectory_publisher.publish(display_trajectory);

    print "============ Waiting while plan1 is visualized (again)..."
    #rospy.sleep(5)


    #print plan1
    
   
        