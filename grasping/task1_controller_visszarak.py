#!/usr/bin/env python

import time
import rospy
import tf
import tf2_ros
import numpy
import copy
import sys
import subprocess
import math
import atexit
import os
import signal
import pprint


from tf2_geometry_msgs import PointStamped
import tf2_geometry_msgs

from ihmc_msgs.msg import FootstepStatusRosMessage
from ihmc_msgs.msg import FootstepDataListRosMessage
from ihmc_msgs.msg import FootstepDataRosMessage
from ihmc_msgs.msg import FootTrajectoryRosMessage
from ihmc_msgs.msg import PelvisHeightTrajectoryRosMessage
from ihmc_msgs.msg import TrajectoryPoint1DRosMessage
from ihmc_msgs.msg import WholeBodyTrajectoryRosMessage
from ihmc_msgs.msg import ArmTrajectoryRosMessage
from ihmc_msgs.msg import OneDoFJointTrajectoryRosMessage
from ihmc_msgs.msg import NeckTrajectoryRosMessage
from ihmc_msgs.msg import ChestTrajectoryRosMessage
from ihmc_msgs.msg import SO3TrajectoryPointRosMessage
from std_msgs.msg import String
from std_msgs.msg import Bool
from std_msgs.msg import Float64MultiArray
from srcsim.msg._Console import Console
from srcsim.msg._Task import Task
from srcsim.msg import Satellite


LEFT = 0
RIGHT = 1

ROBOT_NAME = None
LEFT_FOOT_FRAME_NAME = None
RIGHT_FOOT_FRAME_NAME = None


ZERO_VECTOR = [0.3, 1.3, -0.1, 1.3, 0.0, 0.0, 0.0]
ARM_PULLED_BACK = [-0.2, 1.4, 0.6, 1.3, 0.0, 0.0, 0.0]
ARM_PULLED_BACK_LEFT = [0.2, -1.45, 0.1, -1.65, 0.0, 0.0, 0.0]
ARM_PULLED_BACK_INIT = [0.9, 1.4, -0.1, 0.0, 0.0, 0.0, 0.0]
ARM_PULLED_BACK_INIT2 = [0.3, 1.3, -0.1, 0.65, 0.0, 0.0, 0.0]
ELBOW_BENT_UP = [-1.10, 0.85, 1.3, 0.0, 0.0, 0.0, 0.0]
ELBOW_BENT_UP_MIDDLE = [-1.10, 0.25, 1.3, 0.3, 0.0, 0.0, 0.0]
#ELBOW_BENT_UP_MIDDLE = [-0.1, 0.4, 0.56, 1.7, 0.0, 0.0, 0.0]
#touch ELBOW_BENT_UP = [1.8, -0.3, -1.5, 1.15, 0.0, 0.0, 0.0]
ELBOW_BENT_UP_INIT = [1.8, 0.4, -1.5, 0.95, 0.0, 0.0, 0.0]

ARM_DOWN_LEFT = [0.16, -1.4, 0.46, -1.7, 0.0, 0.0, 0.0]
ARM_DOWN_RIGHT = [0.16, 1.4, 0.46, 1.7, 0.0, 0.0, 0.0]
#was -0.22
ARM_DOWN_RIGHT_2 = [-0.1, 1.4, -0.18, 1.7, 0.0, 0.0, 0.0] #was [-0.1, 1.4, -0.35, 1.7, 0.0, 0.0, 0.0]
ARM_DOWN_RIGHT_3 = [-1.1, 1.5, -1.50, 1.05, 0.0, 0.0, 0.0] #not used currently- was [-1.1, 1.5, -1.55, 1.05, 0.0, 0.0, 0.0]
ARM_DOWN_RIGHT_INIT = [-1.10, 0.05, 1.3, 0.0, 0.0, 0.0, 0.0]
ARM_DOWN_STRETCHED_LEFT = [0.0, -1.4, 1.4, 0.0, 0.0, 0.0, 0.0]
ARM_DOWN_STRETCHED_RIGHT = [0.0, 1.4, 1.4, 0.0, 0.0, 0.0, 0.0]

ARM_SPREAD_LEFT = [0.16, -1.4, -0.4, -2.0, 1.0, 0.0, 0.0]
ARM_SPREAD_RIGHT = [0.16, 1.4, -0.4, 2.0, 1.0, 0.0, 0.0]

ARM_SPREAD_LEFT_WIDE = [0.16, -0.8, -0.4, -2.0, 1.0, 0.0, 0.0]
ARM_SPREAD_RIGHT_WIDE = [0.16, 0.8, -0.4, 2.0, 1.0, 0.0, 0.0]

class Position:
    x = 0.0
    y = 0.0
    z = 0.0
    
class Waypoint2D:
    x = 0.0
    y = 0.0
    angle = 0.0
    
    def toString(self):
        return ("[x:[%s], y:[%s], a:[%s]]" % (self.x, self.y, self.angle))


global pastBlueWheelPositions
global pastRedWheelPositions

global blueWheelPosition
global redWheelPosition

global targetPitch
global targetYaw
global curentPitch
global currentYaw

global blueDetectorProcess
global redDetectorProcess
global blueReconstProcess
global redReconstProcess
#global moveitProcess
#global leftArmListenerProcess
#global rightArmListenerProcess

global stop_turning



def walkTest():
    atexit.register(shutDownHook)

    
    

    global pastBlueWheelPositions
    global pastRedWheelPositions
    pastBlueWheelPositions = [Position(), Position(), Position(), Position(), Position()]
    pastRedWheelPositions = [Position(), Position(), Position(), Position(), Position()]
    
    #Initialize nodes
    global blueDetectorProcess
    global redDetectorProcess
    global blueReconstProcess
    global redReconstProcess
 #   global moveitProcess
 #   global leftArmListenerProcess
 #   global rightArmListenerProcess
 
    global stop_turning
 
    
    rospy.loginfo('setting up 3d vision nodes...')
    FNULL = open(os.devnull, 'w')
    blueDetectorProcess = subprocess.Popen("rosrun vision image_processor /miki/blue blue 1 __name:=blue_detector", shell=True, stdout=FNULL, preexec_fn=os.setsid)
    time.sleep(8)
    redDetectorProcess = subprocess.Popen("rosrun vision image_processor /miki/red red 1 __name:=red_detector", shell=True, stdout=FNULL, preexec_fn=os.setsid)
    time.sleep(8)
    blueReconstProcess = subprocess.Popen("rosrun vision loc_reconst.py LED_data:=/miki/blue srcsim/vision/light:=/srcsim/vision/blue __name:=blue_reconst", shell=True, stdout=FNULL, preexec_fn=os.setsid)
    time.sleep(8)
    redReconstProcess = subprocess.Popen("rosrun vision loc_reconst.py LED_data:=/miki/red srcsim/vision/light:=/srcsim/vision/red __name:=red_reconst", shell=True, stdout=FNULL, preexec_fn=os.setsid)    
    time.sleep(8)
    
    
    rospy.loginfo('all nodes started')
    rossleep(0.5)
    
    assume_position()
    
    r0 = grabBlueWheel()
    
    stop_turning = False
    
    turnBlueWheelDecrease(r0)
    
    letGoOfBlueWheel()

def receivedSatelliteStatus(msg):
    global target_pitch
    global target_yaw
    global current_pitch
    global current_yaw
    global pitch_correct_now
    global yaw_correct_now
    global pitch_completed
    global yaw_completed
    
    target_pitch = msg.target_pitch
    target_yaw = msg.target_yaw
    current_pitch = msg.current_pitch
    current_yaw = msg.current_yaw
    pitch_correct_now = msg.pitch_correct_now
    yaw_correct_now = msg.yaw_correct_now
    pitch_completed = msg.pitch_completed
    yaw_completed = msg.yaw_completed
    
    
def receivedstopturningstatus(msg):
    
    global stop_turning 
    
    
    stop_turning = True
       

    
  
def turnBlueWheelDecrease(r0):
    
    global stop_turning
    
    i = 0
    
    j = 1

    
    stepping_angle = 5
    
    stop_turning = False
    
    while not stop_turning:
   
        calc_rP(i*stepping_angle+180,r0)
        if i%(360/stepping_angle) == 0 and not i == 0:
            adjust_hand()
  
        
        i = i + 1
        
        if j == 3:
            calc_rP((i-2)*stepping_angle+180,r0)
            calc_rP((i-1)*stepping_angle+180,r0)
            j = 1
        
        j = j + 1
        
        print stop_turning
        
        if pitch_correct_now == True:
            stop_turning = True
    
       

def grabBlueWheel():
    
    
    
    bwheel_pos_pelvis = Position()
    move_pos_pelvis = Position()
    
    
    print blueWheelPosition
    
    headPelvis = tfBuffer.lookup_transform('pelvis', 'head', rospy.Time(),rospy.Duration(1.0))
    headPoint = FootstepDataRosMessage()
    headPoint.orientation = headPelvis.transform.rotation
    headPoint.location = headPelvis.transform.translation
    quat = headPoint.orientation
    rot = tf.transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
    transformedOffset = numpy.dot(rot[0:3, 0:3], [blueWheelPosition.x, blueWheelPosition.y, blueWheelPosition.z])


    bwheel_pos_pelvis.x = headPoint.location.x + transformedOffset[0]
    bwheel_pos_pelvis.y = headPoint.location.y + transformedOffset[1]
    bwheel_pos_pelvis.z = headPoint.location.z + transformedOffset[2]
    
    print bwheel_pos_pelvis.x
    print bwheel_pos_pelvis.y
    print bwheel_pos_pelvis.z
   
    #bwheel_pos_pelvis.x = 0.696225173388
    #bwheel_pos_pelvis.y =-0.160718859211
    #bwheel_pos_pelvis.z =-0.0364084505282
   
    turn_chest([0.0,0.4,0.4])
    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([0.55,-0.3,0], 1.0))
    
    RIGHThandcommand('half')
    
    #point 0 (above, to the right)
    
    move_pos_pelvis.x = bwheel_pos_pelvis.x - 0.15
    move_pos_pelvis.y = bwheel_pos_pelvis.y -0.02
    move_pos_pelvis.z = bwheel_pos_pelvis.z + 0.06  
    cmd_str  = str(move_pos_pelvis.x)+","+str(move_pos_pelvis.y)+","+str(move_pos_pelvis.z)+",-0.47842,-0.15,1.47,0.03,0.15"
    print cmd_str
    rightArmPublisher.publish(cmd_str)
    rossleep(2)
    
    
    #point 2 (on it)
    move_pos_pelvis.x = bwheel_pos_pelvis.x - 0.28
    move_pos_pelvis.y = bwheel_pos_pelvis.y +0.03
    move_pos_pelvis.z = bwheel_pos_pelvis.z -0.08
    cmd_str  = str(move_pos_pelvis.x)+","+str(move_pos_pelvis.y)+","+str(move_pos_pelvis.z)+",-0.478423,-0.15,1.47,0.03,0.25"
    
    print cmd_str
    rightArmPublisher.publish(cmd_str)
    rossleep(1)
    
    RIGHThandcommand('half_finger_down')
    
    
    r0 = calc_r0() #calculates center of rotation circle
    
    #point 3 (shake it in)
    
    move_pos_pelvis.x = bwheel_pos_pelvis.x - 0.28
    move_pos_pelvis.y = bwheel_pos_pelvis.y + 0.06
    move_pos_pelvis.z = bwheel_pos_pelvis.z -0.08
    cmd_str  = str(move_pos_pelvis.x)+","+str(move_pos_pelvis.y)+","+str(move_pos_pelvis.z)+",-0.478423,-0.15,1.47,0.02,0.15"
    
    
    print cmd_str
    rightArmPublisher.publish(cmd_str)
    rossleep(0.5)
    
    RIGHThandcommand('full')
    
    move_pos_pelvis.x = bwheel_pos_pelvis.x - 0.24
    move_pos_pelvis.y = bwheel_pos_pelvis.y + 0.06
    move_pos_pelvis.z = bwheel_pos_pelvis.z -0.08
    cmd_str  = str(move_pos_pelvis.x)+","+str(move_pos_pelvis.y)+","+str(move_pos_pelvis.z)+",-0.478423,-0.15,1.47,0.02,0.15"
    
    
    print cmd_str
    rightArmPublisher.publish(cmd_str)
    rossleep(0.5)
    
    RIGHThandcommand('full_tight')
    
        
    return r0
    

    
    
    
 #------------------------------------------------------------------------------------------------------------------------------------------------------   


def assume_position():
    
    pelvisMsg2 = PelvisHeightTrajectoryRosMessage()
    pelvisMsg2.execution_mode = 0
    pelvisMsg2.unique_id = -1
    
    pelvisTraj2 = TrajectoryPoint1DRosMessage()
    pelvisTraj2.time = 1.0
    pelvisTraj2.position = 0.87
    pelvisTraj2.velocity = 0.0
    
    pelvisMsg2.trajectory_points.append(pelvisTraj2)
    
    rospy.loginfo('setting pelvis height...')
    pelvisHeightTrajectoryPublisher.publish(pelvisMsg2)
    
    turn_chest([0.0,0.4,0.0])
    
    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([0.55,0.0,0], 1.0))
    
    sendArmTrajectory('SPREAD_WIDE')
    
    rossleep(4)
    

def adjust_hand():
    
    rossleep(1)
    move_pos_pelvis = Position()
        
    palmPelvis = tfBuffer.lookup_transform('pelvis', 'rightPalm', rospy.Time(),rospy.Duration(1.0))
    palmPoint = FootstepDataRosMessage()
    palmPoint.location = palmPelvis.transform.translation
        
        
    RIGHThandcommand('half')
        
    #point 2 (on it)
    move_pos_pelvis.x = palmPelvis.transform.translation.x
    move_pos_pelvis.y = palmPelvis.transform.translation.y
    move_pos_pelvis.z = palmPelvis.transform.translation.z -0.05
    cmd_str  = str(move_pos_pelvis.x)+","+str(move_pos_pelvis.y)+","+str(move_pos_pelvis.z)+",-0.478423,-0.15,1.47,0.02,0.15"
        
    print cmd_str
    rightArmPublisher.publish(cmd_str)
    rossleep(1)
        
    RIGHThandcommand('half_finger_down')
        
        
        #point 3 (shake it in)
        
    move_pos_pelvis.x = palmPelvis.transform.translation.x
    move_pos_pelvis.y = palmPelvis.transform.translation.y +0.03
    move_pos_pelvis.z = palmPelvis.transform.translation.z -0.05
    cmd_str  = str(move_pos_pelvis.x)+","+str(move_pos_pelvis.y)+","+str(move_pos_pelvis.z)+",-0.478423,-0.15,1.47,0.02,0.15"
        
        
    print cmd_str
    rightArmPublisher.publish(cmd_str)
    rossleep(0.5)
        
    RIGHThandcommand('full')
      
    move_pos_pelvis.x = palmPelvis.transform.translation.x +0.03
    move_pos_pelvis.y = palmPelvis.transform.translation.y +0.03
    move_pos_pelvis.z = palmPelvis.transform.translation.z -0.05
    cmd_str  = str(move_pos_pelvis.x)+","+str(move_pos_pelvis.y)+","+str(move_pos_pelvis.z)+",-0.478423,-0.15,1.47,0.02,0.15"
        
        
    print cmd_str
    rightArmPublisher.publish(cmd_str)
    rossleep(0.5)
        
    RIGHThandcommand('full_tight')
    

def turn_chest(pel_dir):
    
    target_pose = tf2_geometry_msgs.PoseStamped()
   
    target_pose.header.frame_id = 'pelvis'
    target_pose.header.stamp = rospy.Time()
    
    #dir_quat = tf.transformations.quaternion_from_euler(0.0,0.4,0.4)
    
    dir_quat = tf.transformations.quaternion_from_euler(pel_dir[0],pel_dir[1],pel_dir[2])
    
    target_pose.pose.orientation.x = dir_quat[0]
    target_pose.pose.orientation.y = dir_quat[1]
    target_pose.pose.orientation.z = dir_quat[2]
    target_pose.pose.orientation.w = dir_quat[3]
    
    
    
    transformed_target_pose = tfBuffer.transform(target_pose, 'world',rospy.Duration(1.0))  

    chest_dir = tf.transformations.euler_from_quaternion([transformed_target_pose.pose.orientation.x,
                                                          transformed_target_pose.pose.orientation.y,
                                                          transformed_target_pose.pose.orientation.z,
                                                          transformed_target_pose.pose.orientation.w])
    
    
    #print chest_dir
    
    
    
    rospy.loginfo('setting chest trajectory...')
    chestTrajectoryPublisher.publish(createChestMessage([chest_dir[2],chest_dir[1],chest_dir[0]], 1.0))
    rossleep(1.5)
    
def createChestMessage(arr, time):    
    chestMsg = ChestTrajectoryRosMessage()
    chestMsg.execution_mode = 0
    chestMsg.unique_id = -1
    
    chestTraj = SO3TrajectoryPointRosMessage()
    chestTraj.time = time
    
    pista_quaternion = tf.transformations.quaternion_from_euler(arr[0], arr[1], arr[2], 'rzyx')
    
    chestTraj.orientation.x = pista_quaternion[0]
    chestTraj.orientation.y = pista_quaternion[1]
    chestTraj.orientation.z = pista_quaternion[2]
    chestTraj.orientation.w = pista_quaternion[3]    
    
    chestMsg.taskspace_trajectory_points.append(chestTraj)
    
    return chestMsg
    

def RIGHThandcommand(hand_cmd):
    hand_data = Float64MultiArray()
    
    if hand_cmd == 'letgo':
        hand_data.data = [0.3, 0.2, 0, 0, 0]
    elif hand_cmd == 'half_finger_down':
        hand_data.data = [2.2, 0.05, 0.6, 0.7, 0.7]
    elif hand_cmd == 'half':
        hand_data.data = [0.3, 0.05, 0.6, 0.7, 0.7]
    elif hand_cmd == 'full':
        hand_data.data = [2.2, 0.6, 0.6, 0.7, 1.0]
    elif hand_cmd == 'full_tight':
        hand_data.data = [1.9, 0.6, 0.9, 0.8, 1.0]  
    elif hand_cmd == 'loose_grip':
        hand_data.data = [2.2, 0.6, 0.3, 0.3, 0.3]
    elif hand_cmd == '2fingers_prep':
        hand_data.data = [1.7, 0.2, 0.3, 1.1, 1.1]
    elif hand_cmd == '2fingers_full':
        hand_data.data = [1.7, 0.6, 0.65, 1.1, 1.1]
    

    
    print ('hand command '+hand_cmd)
    rightHandPublisher.publish(hand_data)    
    rossleep(0.5)
    
def LEFThandcommand(hand_cmd):
    hand_data = Float64MultiArray()
    
    if hand_cmd == 'letgo':
        hand_data.data = [0.3, 0.2, 0, 0, 0]
    elif hand_cmd == 'half_finger_down':
        hand_data.data = [2.2, 0.05, 0.6, 0.7, 0.7]
    elif hand_cmd == 'half':
        hand_data.data = [0.3, 0.05, 0.6, 0.7, 0.7]
    elif hand_cmd == 'full':
        hand_data.data = [2.2, 0.6, 0.6, 0.7, 1.0]
    elif hand_cmd == 'full_tight':
        hand_data.data = [1.9, 0.6, 0.9, 0.8, 1.0]  
    elif hand_cmd == 'loose_grip':
        hand_data.data = [2.2, 0.6, 0.3, 0.3, 0.3]
    elif hand_cmd == '2fingers_prep':
        hand_data.data = [1.7, 0.2, 0.3, 1.1, 1.1]
    elif hand_cmd == '2fingers_full':
        hand_data.data = [1.7, 0.6, 0.65, 1.1, 1.1]
    

    
    print ('hand command '+hand_cmd)
    leftHandPublisher.publish(hand_data)    
    rossleep(0.5)
    
def calc_rP(alpha_deg,r0):

    alpha = alpha_deg*numpy.pi/180 # parameter of circle
    r = 0.12 #radius of wheel
    theta  = -30*numpy.pi/180 #tilt of table
    sinTh = math.sin(theta)
    cosTh = math.cos(theta)   
    traf = numpy.matrix([[cosTh,0,sinTh],
                        [0,1,0],
                        [-1*sinTh,0,cosTh]])

    
    rT = [r*math.cos(alpha),r*math.sin(alpha),0] #position of point in it's own base
    
    rP  = traf.dot(rT) + r0 #position of point in pelvis
    
    frac_angle = math.modf(float(alpha_deg)/float(360))
    
    if 0 < frac_angle[0] < 0.5 or -1 < frac_angle[0] < -0.5:
        print "trying" #different hand orientation
        print alpha_deg
        cmd_str  = str(rP[0,0])+","+str(rP[0,1])+","+str(rP[0,2])+",-0.478423,-0.15,2.0,0.02,0.15,last,quick"
    else:
        print "trying"
        print alpha_deg
        cmd_str  = str(rP[0,0])+","+str(rP[0,1])+","+str(rP[0,2])+",-0.478423,-0.15,1.47,0.02,0.15,last,quick"
        
    
    
    
    print cmd_str
    rightArmPublisher.publish(cmd_str)
    rossleep(0.5)
    
    #return rP
    
def calc_r0():
    
        
    palmPelvis = tfBuffer.lookup_transform('pelvis', 'rightPalm', rospy.Time(),rospy.Duration(1.0))
    palmPoint = FootstepDataRosMessage()
    palmPoint.location = palmPelvis.transform.translation
    
    r = 0.12 #radius of wheel
    theta  = -30*numpy.pi/180 #tilt of table
    
    r180 = [palmPelvis.transform.translation.x,palmPelvis.transform.translation.y,palmPelvis.transform.translation.z]
    
    r0 = numpy.subtract(r180,[-r*math.cos(theta),0,r*math.sin(theta)]) #position of center in pelvis
    
    print r180
    print r0
    
    return r0
    
    
def letGoOfBlueWheel():
   
    RIGHThandcommand('letgo')
    


    
def getWorldPointFromHeadPoint(headX, headY, headZ):
    headPoint = PointStamped()
   
    headPoint.header.frame_id = 'head'   
    headPoint.header.stamp = rospy.Time()
   
    headPoint.point.x = headX
    headPoint.point.y = headY
    headPoint.point.z = headZ
   
    worldPoint = tfBuffer.transform(headPoint, 'world')

    worldPosition = Position()
    worldPosition.x = worldPoint.point.x
    worldPosition.y = worldPoint.point.y
    worldPosition.z = worldPoint.point.z
    
    rospy.loginfo('world target point is x:[%f], y:[%f]' % (worldPosition.x, worldPosition.y))
    
    return worldPosition
   
    
def getWorldPointFromHeadPoint2(headX, headY, headZ):
    #using footstep only as a data structure, TODO refactor later
    footstep = FootstepDataRosMessage()

    footWorld = tfBuffer.lookup_transform('world', 'head', rospy.Time())
    footstep.orientation = footWorld.transform.rotation
    footstep.location = footWorld.transform.translation

    # transform the offset to world frame
    quat = footstep.orientation
    rot = tf.transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
    transformedOffset = numpy.dot(rot[0:3, 0:3], [headX, headY, headZ])

    footstep.location.x += transformedOffset[0]
    footstep.location.y += transformedOffset[1]
    footstep.location.z += transformedOffset[2]

    worldPoint = Position()
    worldPoint.x = footstep.location.x
    worldPoint.y = footstep.location.y
    worldPoint.z = footstep.location.z
    
    rospy.loginfo('world target point is x:[%f], y:[%f]' % (worldPoint.x, worldPoint.y))
    
    return worldPoint


def getHeadTargetInWorld(distanceFromPanel):
    rospy.loginfo('############################################################################################')
    xPos = (blueWheelPosition.x + redWheelPosition.x) / 2
    yPos = (blueWheelPosition.y + redWheelPosition.y) / 2
    #TODO this is fucking random? - it fluctuates because readings are unstable
    alpha = math.atan(abs(blueWheelPosition.x - redWheelPosition.x) / abs(blueWheelPosition.y - redWheelPosition.y))
    yShift = math.sin(alpha) * distanceFromPanel
    xShift = math.cos(alpha) * distanceFromPanel
    pointX = xPos - xShift

    if blueWheelPosition.x > redWheelPosition.x:
        if blueWheelPosition.y > redWheelPosition.y:
            yShift = -1 * yShift
    else:
        if redWheelPosition.y > blueWheelPosition.y:
            yShift = -1 * yShift
            
    pointY = yPos - yShift

    #checking if the alternative point ("behind the panel") is actually closer, in which case that should be our target, even if the angle suggests otherwise
    pointX2 = xPos + xShift
    pointY2 = yPos + yShift
    if math.sqrt(pointX2*pointX2 + pointY2*pointY2) < math.sqrt(pointX*pointX + pointY*pointY):
        pointX = pointX2
        pointY = pointY2
        
    if pointY < yPos:
        alpha = -1 * alpha

    rospy.loginfo('blue xyz [%f] [%f] [%f]' % (blueWheelPosition.x, blueWheelPosition.y, blueWheelPosition.z))
    rospy.loginfo('red xyz [%f] [%f] [%f]' % (redWheelPosition.x, redWheelPosition.y, redWheelPosition.z))
    rospy.loginfo('xpos [%f]' % xPos)
    rospy.loginfo('ypos [%f]' % yPos)
    rospy.loginfo('alpha [%f]' % alpha)
    rospy.loginfo('yShift [%f]' % yShift)
    rospy.loginfo('xShift [%f]' % xShift)
    rospy.loginfo('pointX [%f]' % pointX)
    rospy.loginfo('pointY [%f]' % pointY)
    
    rospy.loginfo('TARGET IN HEAD FRAME COORDINATES IS: x=[%f], y=[%f], z=[%f], angle=[%f]' % (pointX, pointY, blueWheelPosition.z, alpha))

    #convert the head frame point to world frame point
    headWorld = tfBuffer.lookup_transform('world', 'head', rospy.Time())
    headPoint = FootstepDataRosMessage()
    headPoint.orientation = headWorld.transform.rotation
    headPoint.location = headWorld.transform.translation
    quat = headPoint.orientation
    rot = tf.transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
    transformedOffset = numpy.dot(rot[0:3, 0:3], [pointX, pointY, blueWheelPosition.z])

    rospy.loginfo('TARGET IN WORLD FRAME COORDINATES IS: x=[%f], y=[%f], z=[%f], angle=[%f]' % (headPoint.location.x + transformedOffset[0], headPoint.location.y + transformedOffset[1], 0, alpha + getHeadAngle()))
    
    return [headPoint.location.x + transformedOffset[0], headPoint.location.y + transformedOffset[1], alpha + getHeadAngle()]


#get the point halfway between the two feet
def getFootSourceInWorld():
    foot_frame_left = LEFT_FOOT_FRAME_NAME
    foot_frame_right = RIGHT_FOOT_FRAME_NAME

    footWorldLeft = tfBuffer.lookup_transform('world', foot_frame_left, rospy.Time())
    rotL = footWorldLeft.transform.rotation    
    angL = tf.transformations.euler_from_quaternion([rotL.x, rotL.y, rotL.z, rotL.w], 'rzyx')

    footWorldRight = tfBuffer.lookup_transform('world', foot_frame_right, rospy.Time())
    rotR = footWorldRight.transform.rotation    
    angR = tf.transformations.euler_from_quaternion([rotR.x, rotR.y, rotR.z, rotR.w], 'rzyx')

    footX = (footWorldLeft.transform.translation.x + footWorldRight.transform.translation.x) / 2
    footY = (footWorldLeft.transform.translation.y + footWorldRight.transform.translation.y) / 2
    footAng = (angL[0] + angR[0]) / 2

    rospy.loginfo('AVG FOOT LOCATION IN WORLD FRAME COORDINATES IS: x=[%f], y=[%f], angle=[%f]' % (footX, footY, footAng))
    
    return [footX, footY, footAng]


def interpolateWayPoints(fromX, fromY, fromAngle, toX, toY, toAngle):
    rospy.loginfo("INTERPOLATION fromX [%f], fromY [%f], fromAngle [%f], toX [%f], toY [%f], toAngle [%f]" % (fromX, fromY, fromAngle, toX, toY, toAngle))
    
    MAX_STEP_SIZE = 0.4
    distance = math.sqrt(math.pow(toX - fromX, 2) + math.pow(toY - fromY, 2))
    stepCount = int(math.ceil(distance / MAX_STEP_SIZE))
    
    wayPoints = []
    for stepNum in range(1, stepCount + 1):
        if stepNum == 1:#small extra step at the beginning
            firstWayPoint = Waypoint2D()
            firstWayPoint.x = fromX + (toX - fromX) / stepCount * stepNum / 2
            firstWayPoint.y = fromY + (toY - fromY) / stepCount * stepNum / 2
            firstWayPoint.angle = fromAngle + (toAngle - fromAngle) / stepCount * stepNum / 2
            wayPoints.append(firstWayPoint)
        wayPoint = Waypoint2D()
        wayPoint.x = fromX + (toX - fromX) / stepCount * stepNum
        wayPoint.y = fromY + (toY - fromY) / stepCount * stepNum
        wayPoint.angle = fromAngle + (toAngle - fromAngle) / stepCount * stepNum
        wayPoints.append(wayPoint)
    return wayPoints
    
    
def createFootStepFromWayPoint(stepSide, wayPoint, z, swingHeight):
    if stepSide == LEFT:
        correction = 0.16
    else:
        correction = 0.16

    xCorr = math.sin(wayPoint.angle) * correction
    yCorr = math.cos(wayPoint.angle) * correction

    if stepSide == LEFT:
        return createFootStepInWorld(stepSide, [wayPoint.x - xCorr, wayPoint.y + yCorr, z], swingHeight, wayPoint.angle)    
    else:
        return createFootStepInWorld(stepSide, [wayPoint.x + xCorr, wayPoint.y - yCorr, z], swingHeight, wayPoint.angle)    


#offset is already in world coordinates    
def createFootStepInWorld(stepSide, offset, swingHeight, turn):
    footstep = FootstepDataRosMessage()
    footstep.robot_side = stepSide
    footstep.swing_height = swingHeight

    footstep.location.x = offset[0]
    footstep.location.y = offset[1]
    footstep.location.z = offset[2]

    rospy.loginfo("@@@@@@@ WORLD FOOTSTEP LOCATION @@@@@@@@@@ [%s] [%s] [%s] " % (footstep.location.x, footstep.location.y, footstep.location.z))

    pista_quaternion = tf.transformations.quaternion_from_euler(turn, 0, 0, 'rzyx')
    
    footstep.orientation.x = pista_quaternion[0]
    footstep.orientation.y = pista_quaternion[1]
    footstep.orientation.z = pista_quaternion[2]
    footstep.orientation.w = pista_quaternion[3]    
    
    pprint.pprint(turn)
    pprint.pprint(tf.transformations.euler_from_quaternion([pista_quaternion[0], pista_quaternion[1], pista_quaternion[2], pista_quaternion[3]], 'rzyx'))
    
    return footstep

    
# Creates footstep with the current position and orientation of the foot.
def createFootStepInPlace(stepSide, swingHeight):
    footstep = FootstepDataRosMessage()
    footstep.robot_side = stepSide
    footstep.swing_height = swingHeight
    
    if stepSide == LEFT:
        foot_frame = LEFT_FOOT_FRAME_NAME
    else:
        foot_frame = RIGHT_FOOT_FRAME_NAME

    footWorld = tfBuffer.lookup_transform('world', foot_frame, rospy.Time())
    footstep.orientation = footWorld.transform.rotation
    footstep.location = footWorld.transform.translation
    
#    rospy.loginfo("orientation of inplace step:")
#    ang = tf.transformations.euler_from_quaternion([footstep.orientation.x, footstep.orientation.y, footstep.orientation.z, footstep.orientation.w], 'rzyx')
#    pprint.pprint(ang)
    
    return footstep


# Creates footstep offset from the current foot position. The offset is in foot frame.
def createFootStepOffset(stepSide, offset, swingHeight, turn):
    footstep = createFootStepInPlace(stepSide, swingHeight)


    # transform the offset to world frame
    quat = footstep.orientation
    rot = tf.transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
    transformedOffset = numpy.dot(rot[0:3, 0:3], offset)

    footstep.location.x += transformedOffset[0]
    footstep.location.y += transformedOffset[1]
    footstep.location.z += transformedOffset[2]

    if turn != 0:
        pista_quaternion = tf.transformations.quaternion_from_euler(turn, 0, 0, 'rzyx')
    
        footstep.orientation.x = pista_quaternion[0]
        footstep.orientation.y = pista_quaternion[1]
        footstep.orientation.z = pista_quaternion[2]
        footstep.orientation.w = pista_quaternion[3]

    return footstep


def getHeadAngle():
    footWorld = tfBuffer.lookup_transform('world', 'head', rospy.Time())
    q = footWorld.transform.rotation
    return tf.transformations.euler_from_quaternion([q.x, q.y, q.z, q.w], 'rzyx')[0]


def getFootHeight(stepSide):
    if stepSide == LEFT:
        foot_frame = LEFT_FOOT_FRAME_NAME
    else:
        foot_frame = RIGHT_FOOT_FRAME_NAME

    footWorld = tfBuffer.lookup_transform('world', foot_frame, rospy.Time())
    return footWorld.transform.translation.z


def waitForFootsteps(numberOfSteps):
    global stepCounter
    stepCounter = 0
    while stepCounter < numberOfSteps:
        rate.sleep()

    rospy.loginfo('finished set of steps')


def recievedFootStepStatus(msg):
    global stepCounter
    if msg.status == 1:
        stepCounter += 1
        

def waitForNavigation():
    global navigationFinished
    navigationFinished = False
    while navigationFinished == False:
        rate.sleep()

    rospy.loginfo('finished navigation')


def receivedNavigationStatus(msg):
    rospy.loginfo('received navigation status: [%s]' % msg)
    global navigationFinished
    if msg.data.startswith('0'):
        navigationFinished = True
    else:
        navigationFinished = False
        

def startWandering():
    msg = Bool()
    msg.data = True
    wanderingPublisher.publish(msg)


def stopWandering():
    msg = Bool()
    msg.data = False
    wanderingPublisher.publish(msg)
    
    rospy.loginfo('requesting wander stop')
    waitForNavigation()
    rospy.loginfo('wandering stopped')


def receivedBlueWheelPosition(msg):
    global blueWheelPosition
    global pastBlueWheelPositions

#    rospy.loginfo("------- got blue position: [%f], [%f], [%f]" % (blueWheelPosition.x, blueWheelPosition.y, blueWheelPosition.z))
    
    setWheelPosition(msg, blueWheelPosition, pastBlueWheelPositions)
    
#    rospy.loginfo("[%f]" % pastBlueWheelPositions[2].x) 
#    rospy.loginfo("[%f]" % blueWheelPosition.x)  
    
#    rospy.loginfo("-------- wheel positions: [%f], [%f], [%f], [%f]" % (blueWheelPosition.x, blueWheelPosition.y, redWheelPosition.x, redWheelPosition.y))
    

def receivedRedWheelPosition(msg):
    global redWheelPosition
    global pastRedWheelPositions
    setWheelPosition(msg, redWheelPosition, pastRedWheelPositions)    
#    rospy.loginfo("------- got red position: [%f], [%f], [%f]" % (redWheelPosition.x, redWheelPosition.y, redWheelPosition.z))
#    rospy.loginfo("-------- wheel positions: [%f], [%f], [%f], [%f]" % (blueWheelPosition.x, blueWheelPosition.y, redWheelPosition.x, redWheelPosition.y))


def setWheelPosition(msg, pos, posArr):
    msgPosition = Position()
    msgPosition.x = msg.x
    msgPosition.y = msg.y
    msgPosition.z = msg.z
    
#    print(len(posArr))
    
    shift(posArr)
    posArr[len(posArr) - 1] = msgPosition
    updatePositionAvg(pos, posArr)


def waitForTask1():
    global task1Finished
    task1Finished = False
    while task1Finished == False:
        rate.sleep()

    rospy.loginfo('finished task 1')


def receivedTaskStatus(msg):
    global task1Finished
    if msg.finished == True:
        task1Finished = True
        

def shift(arr):
    for i in range(0, len(arr)-1):
        arr[i] = arr[i+1]


def updatePositionAvg(pos, arr):
    sumX = 0.0
    sumY = 0.0
    sumZ = 0.0

#    rospy.loginfo("from avg [%f]" % arr[2].x) 
    
    for i in range(0, len(arr)):
#        rospy.loginfo("[%i] [%f]" % (i, arr[i].x)) 

        sumX += arr[i].x
        sumY += arr[i].y
        sumZ += arr[i].z

#    rospy.loginfo("from avg sumX [%f]" % sumX) 

    pos.x = sumX / len(arr)
    pos.y = sumY / len(arr)
    pos.z = sumZ / len(arr)
    
'''
def receivedSatelliteStatus(msg):
    global targetPitch
    global targetYaw
    global curentPitch
    global currentYaw
    
    targetPitch = msg.targetPitch
    targetYaw = msg.targetYaw
    currentPitch = msg.currentPitch
    currentYaw = msg.currentYaw
'''    

def sendArmTrajectory(mode):   
    if mode == 'PUSH':
        msg = ArmTrajectoryRosMessage()
        msg.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg = appendTrajectoryPoint(msg, 0.12, ELBOW_BENT_UP_MIDDLE)
        msg = appendTrajectoryPoint(msg, 0.20, ELBOW_BENT_UP)
        msg.unique_id = -1
        rospy.loginfo('publishing right PUSH trajectory')
        armTrajectoryPublisher.publish(msg)

    elif mode == 'PULL':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 0.5, ARM_PULLED_BACK_INIT2)
        msg2 = appendTrajectoryPoint(msg2, 1.0, ARM_DOWN_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right PULL trajectory')
        armTrajectoryPublisher.publish(msg2)
        
        rossleep(0.2)
        
        msg3 = ArmTrajectoryRosMessage()
        msg3.robot_side = ArmTrajectoryRosMessage.LEFT
        msg3 = appendTrajectoryPoint(msg3, 0.6, ARM_DOWN_LEFT)
        msg3.unique_id = -2
        rospy.loginfo('publishing left PULL trajectory')
        armTrajectoryPublisher.publish(msg3)

    elif mode == 'DOWN':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 1.0, ARM_DOWN_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right DOWN trajectory')
        armTrajectoryPublisher.publish(msg2)

        rossleep(0.35)
        
        msg3 = ArmTrajectoryRosMessage()
        msg3.robot_side = ArmTrajectoryRosMessage.LEFT
        msg3 = appendTrajectoryPoint(msg3, 1.0, ARM_DOWN_LEFT)
        msg3.unique_id = -2
        rospy.loginfo('publishing left DOWN trajectory')
        armTrajectoryPublisher.publish(msg3)

    elif mode == 'RIGHTDOWN':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 0.25, ARM_DOWN_RIGHT_INIT)
        msg2 = appendTrajectoryPoint(msg2, 0.60, ARM_DOWN_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right DOWN trajectory')
        armTrajectoryPublisher.publish(msg2)

    elif mode == 'SPREAD':
        msg1 = ArmTrajectoryRosMessage()
        msg1.robot_side = ArmTrajectoryRosMessage.LEFT
        msg1 = appendTrajectoryPoint(msg1, 0.60, ARM_SPREAD_LEFT_WIDE)
        msg1.unique_id = -1
        rospy.loginfo('publishing left SPREAD trajectory')
        armTrajectoryPublisher.publish(msg1)

        rossleep(0.35)

        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 0.60, ARM_SPREAD_RIGHT_WIDE)
        msg2.unique_id = -1
        rospy.loginfo('publishing right SPREAD trajectory')
        armTrajectoryPublisher.publish(msg2)
        
    elif mode == 'SPREAD_WIDE':
        msg1 = ArmTrajectoryRosMessage()
        msg1.robot_side = ArmTrajectoryRosMessage.LEFT
        msg1 = appendTrajectoryPoint(msg1, 0.60, ARM_SPREAD_LEFT_WIDE)
        msg1.unique_id = -1
        rospy.loginfo('publishing left SPREAD WIDE trajectory')
        armTrajectoryPublisher.publish(msg1)

        rossleep(0.35)

        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 0.60, ARM_SPREAD_RIGHT_WIDE)
        msg2.unique_id = -1
        rospy.loginfo('publishing right SPREAD WIDE trajectory')
        armTrajectoryPublisher.publish(msg2)


def appendTrajectoryPoint(arm_trajectory, time, positions):
    if not arm_trajectory.joint_trajectory_messages:
        arm_trajectory.joint_trajectory_messages = [copy.deepcopy(OneDoFJointTrajectoryRosMessage()) for i in range(len(positions))]
    for i, pos in enumerate(positions):
        point = TrajectoryPoint1DRosMessage()
        point.time = time
        point.position = pos
        point.velocity = 0
        arm_trajectory.joint_trajectory_messages[i].trajectory_points.append(point)
    return arm_trajectory


def createNeckTrajectoryMessage(arr, secs):
    neckTrajPoint1 = TrajectoryPoint1DRosMessage()
    neckTrajPoint1.time = secs
    neckTrajPoint1.position = arr[0]
    neckTraj1 = OneDoFJointTrajectoryRosMessage()
    neckTraj1.trajectory_points.append(neckTrajPoint1)

    neckTrajPoint2 = TrajectoryPoint1DRosMessage()
    neckTrajPoint2.time = secs
    neckTrajPoint2.position = arr[1]
    neckTraj2 = OneDoFJointTrajectoryRosMessage()
    neckTraj2.trajectory_points.append(neckTrajPoint2)

    neckTrajPoint3 = TrajectoryPoint1DRosMessage()
    neckTrajPoint3.time = secs
    neckTrajPoint3.position = arr[2]
    neckTraj3 = OneDoFJointTrajectoryRosMessage()
    neckTraj3.trajectory_points.append(neckTrajPoint3)
    
    neckMsg = NeckTrajectoryRosMessage()
    neckMsg.unique_id = -1
    neckMsg.joint_trajectory_messages.append(neckTraj1)
    neckMsg.joint_trajectory_messages.append(neckTraj2)
    neckMsg.joint_trajectory_messages.append(neckTraj3)

    return neckMsg


def createPelvisMsg(height, secs):
    pelvisMsg = PelvisHeightTrajectoryRosMessage()
    pelvisMsg.execution_mode = 0
    pelvisMsg.unique_id = -1
    
    pelvisTraj = TrajectoryPoint1DRosMessage()
    pelvisTraj.time = secs
    pelvisTraj.position = height
    pelvisTraj.velocity = 0.0
    
    pelvisMsg.trajectory_points.append(pelvisTraj)
    
    return pelvisMsg


def rossleep(time):
    try:
        seconds = float(time)
    except (ValueError):
        print("Couldn't convert argument [%s] to float" % sys.argv[1])
    rospy.sleep(rospy.Duration.from_sec(seconds))
    

def shutDownHookWrapper(signal, frame):
    shutDownHook()
    

def shutDownHook():
    
    global blueDetectorProcess
    global redDetectorProcess
    global blueReconstProcess
    global redReconstProcess
#    global moveitProcess
#    global leftArmListenerProcess
#    global rightArmListenerProcess

    rospy.loginfo("shutdown hook called...")
    if blueDetectorProcess is not None:
        rospy.loginfo("killing blue detector...")
        os.killpg(os.getpgid(blueDetectorProcess.pid), signal.SIGKILL)
    if redDetectorProcess is not None:
        rospy.loginfo("killing red detector...")
        os.killpg(os.getpgid(redDetectorProcess.pid), signal.SIGKILL)
    if blueReconstProcess is not None:
        rospy.loginfo("killing blue reconst...")
        os.killpg(os.getpgid(blueReconstProcess.pid), signal.SIGKILL)
    if redReconstProcess is not None:
        rospy.loginfo("killing red reconst...")
        os.killpg(os.getpgid(redReconstProcess.pid), signal.SIGKILL)

#    if leftArmListenerProcess is not None:
#        rospy.loginfo("killing left arm listener...")
#        os.killpg(os.getpgid(leftArmListenerProcess.pid), signal.SIGKILL)
#    if rightArmListenerProcess is not None:
#        rospy.loginfo("killing right arm listener...")
#        os.killpg(os.getpgid(rightArmListenerProcess.pid), signal.SIGKILL)
#    if moveitProcess is not None:
#        rospy.loginfo("killing moveit...")
#        os.killpg(os.getpgid(moveitProcess.pid), signal.SIGKILL)
    sys.exit(0)


if __name__ == '__main__':
    global blueWheelPosition
    global redWheelPosition
    global navigationFinished
    global stepCounter


    blueWheelPosition = Position()
    redWheelPosition = Position()
    navigationFinished = False
    stepCounter = 0

    
    signal.signal(signal.SIGINT, shutDownHookWrapper)
    
    try:
        rospy.init_node('ihmc_walk_test')

        if not rospy.has_param('/ihmc_ros/robot_name'):
            rospy.logerr("Cannot run walk_test.py, missing parameters!")
            rospy.logerr("Missing parameter '/ihmc_ros/robot_name'")

        else:
            ROBOT_NAME = rospy.get_param('/ihmc_ros/robot_name')

            right_foot_frame_parameter_name = "/ihmc_ros/{0}/right_foot_frame_name".format(ROBOT_NAME)
            left_foot_frame_parameter_name = "/ihmc_ros/{0}/left_foot_frame_name".format(ROBOT_NAME)

            if rospy.has_param(right_foot_frame_parameter_name) and rospy.has_param(left_foot_frame_parameter_name):
                RIGHT_FOOT_FRAME_NAME = rospy.get_param(right_foot_frame_parameter_name)
                LEFT_FOOT_FRAME_NAME = rospy.get_param(left_foot_frame_parameter_name)

                footStepStatusSubscriber = rospy.Subscriber("/ihmc_ros/{0}/output/footstep_status".format(ROBOT_NAME), FootstepStatusRosMessage, recievedFootStepStatus)
                blueWheelPositionSubscrber = rospy.Subscriber("/srcsim/vision/blue", Console, receivedBlueWheelPosition)
                redWheelPositionSubscrber = rospy.Subscriber("/srcsim/vision/red", Console, receivedRedWheelPosition)
                navigationStatusSubscriber = rospy.Subscriber("/humanz/status", String, receivedNavigationStatus)
                taskStatusSubscriber = rospy.Subscriber("/srcsim/finals/task", Task, receivedTaskStatus)
                satelliteSubscriber = rospy.Subscriber("/task1/checkpoint2/satellite", Satellite, receivedSatelliteStatus)
                stopturningSubscriber = rospy.Subscriber("/humanz/stopturning", String, receivedstopturningstatus)
                
                footStepListPublisher = rospy.Publisher("/ihmc_ros/{0}/control/footstep_list".format(ROBOT_NAME), FootstepDataListRosMessage, queue_size=20)
                footTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/foot_trajectory".format(ROBOT_NAME), FootTrajectoryRosMessage, queue_size=20)
                pelvisHeightTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/pelvis_height_trajectory".format(ROBOT_NAME), PelvisHeightTrajectoryRosMessage, queue_size=20)
                wholeBodyTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/whole_body_trajectory".format(ROBOT_NAME), WholeBodyTrajectoryRosMessage, queue_size=50)
                armTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/arm_trajectory".format(ROBOT_NAME), ArmTrajectoryRosMessage, queue_size=1)
                neckTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/neck_trajectory".format(ROBOT_NAME), NeckTrajectoryRosMessage, queue_size=1)
                pathPublisher = rospy.Publisher("/humanz/walk_path", String, queue_size=1)
                navigationPublisher = rospy.Publisher("/humanz/navigate", String, queue_size=1)
                wanderingPublisher = rospy.Publisher("/humanz/wandering", Bool, queue_size=1)
                leftArmPublisher = rospy.Publisher("left_arm_moveit_position", String, queue_size=20)
                rightArmPublisher = rospy.Publisher("right_arm_moveit_position", String, queue_size=20)
                leftHandPublisher = rospy.Publisher("/left_hand_position_controller/command", Float64MultiArray, queue_size=20)
                rightHandPublisher = rospy.Publisher("/right_hand_position_controller/command", Float64MultiArray, queue_size=20)
                chestTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/chest_trajectory".format(ROBOT_NAME), ChestTrajectoryRosMessage, queue_size=20)

                tfBuffer = tf2_ros.Buffer()
                tfListener = tf2_ros.TransformListener(tfBuffer)

                rate = rospy.Rate(10) # 10hz
                time.sleep(1.5)

                # make sure the simulation is running otherwise wait
                if footStepListPublisher.get_num_connections() == 0:
                    rospy.loginfo('waiting for subsciber...')
                    while footStepListPublisher.get_num_connections() == 0:
                        rate.sleep()

                if not rospy.is_shutdown():
                    walkTest()
            else:
                if not rospy.has_param(left_foot_frame_parameter_name):
                    rospy.logerr("Missing parameter {0}".format(left_foot_frame_parameter_name))
                if not rospy.has_param(right_foot_frame_parameter_name):
                    rospy.logerr("Missing parameter {0}".format(right_foot_frame_parameter_name))

    except rospy.ROSInterruptException:
        pass