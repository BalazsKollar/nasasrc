#!/usr/bin/env python

import time
import rospy
import tf
import tf2_ros
import numpy
import copy
import sys



import moveit_commander
import moveit_msgs.msg
import tf2_geometry_msgs
from moveit_msgs.msg import RobotTrajectory
import trajectory_msgs.msg


from ihmc_msgs.msg import FootstepStatusRosMessage
from ihmc_msgs.msg import FootstepDataListRosMessage
from ihmc_msgs.msg import FootstepDataRosMessage
from ihmc_msgs.msg import FootTrajectoryRosMessage
from ihmc_msgs.msg import PelvisHeightTrajectoryRosMessage
from ihmc_msgs.msg import TrajectoryPoint1DRosMessage
from ihmc_msgs.msg import WholeBodyTrajectoryRosMessage
from ihmc_msgs.msg import ArmTrajectoryRosMessage
from ihmc_msgs.msg import OneDoFJointTrajectoryRosMessage
from ihmc_msgs.msg import NeckTrajectoryRosMessage
from ihmc_msgs.msg import ChestTrajectoryRosMessage
from ihmc_msgs.msg import SO3TrajectoryPointRosMessage
from srcsim.msg._Console import Console




LEFT = 0
RIGHT = 1

ROBOT_NAME = None
LEFT_FOOT_FRAME_NAME = None
RIGHT_FOOT_FRAME_NAME = None


ZERO_VECTOR = [0.3, 1.3, -0.1, 1.3, 0.0, 0.0, 0.0]
ARM_PULLED_BACK = [-0.2, 1.4, 0.6, 1.3, 0.0, 0.0, 0.0]
ARM_PULLED_BACK_LEFT = [0.2, -1.45, 0.1, -1.65, 0.0, 0.0, 0.0]
ARM_PULLED_BACK_INIT = [0.9, 1.4, -0.1, 0.0, 0.0, 0.0, 0.0]
ARM_PULLED_BACK_INIT2 = [0.3, 1.3, -0.1, 0.65, 0.0, 0.0, 0.0]
ELBOW_BENT_UP = [-1.10, 0.85, 1.3, 0.0, 0.0, 0.0, 0.0]
ELBOW_BENT_UP_MIDDLE = [-1.10, 0.25, 1.3, 0.3, 0.0, 0.0, 0.0]
#ELBOW_BENT_UP_MIDDLE = [-0.1, 0.4, 0.56, 1.7, 0.0, 0.0, 0.0]
#touch ELBOW_BENT_UP = [1.8, -0.3, -1.5, 1.15, 0.0, 0.0, 0.0]
ELBOW_BENT_UP_INIT = [1.8, 0.4, -1.5, 0.95, 0.0, 0.0, 0.0]

ARM_DOWN_LEFT = [0.16, -1.4, 0.56, -1.7, 0.0, 0.0, 0.0]
ARM_DOWN_RIGHT = [0.16, 1.4, 0.56, 1.7, 0.0, 0.0, 0.0]
#was -0.22
ARM_DOWN_RIGHT_2 = [-0.1, 1.4, -0.18, 1.7, 0.0, 0.0, 0.0] #was [-0.1, 1.4, -0.35, 1.7, 0.0, 0.0, 0.0]
ARM_DOWN_RIGHT_3 = [-1.1, 1.5, -1.50, 1.05, 0.0, 0.0, 0.0] #not used currently- was [-1.1, 1.5, -1.55, 1.05, 0.0, 0.0, 0.0]
ARM_DOWN_RIGHT_INIT = [-1.10, 0.05, 1.3, 0.0, 0.0, 0.0, 0.0]
ARM_DOWN_STRETCHED_LEFT = [0.0, -1.4, 1.4, 0.0, 0.0, 0.0, 0.0]
ARM_DOWN_STRETCHED_RIGHT = [0.0, 1.4, 1.4, 0.0, 0.0, 0.0, 0.0]

ARM_SPREAD_LEFT = [0.16, -1.4, -0.4, -2.0, 1.0, 0.0, 0.0]
ARM_SPREAD_RIGHT = [0.16, 1.4, -0.4, 2.0, 1.0, 0.0, 0.0]






def walkTest():       
                   
    
    #to proba1
    armpositions = [[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                     [-0.24569504536283002, 0.2313952807060638, 0.3558771763117248, 0.25693697221910294, -0.03727233750407122, -0.015294645799942906, 0],
                     [-0.687946127015924, 0.6479067859769786, 0.9964560936728293, 0.7194235222134882, -0.10436254501139941, -0.04282500823984013,0],
                     [-0.8845021633061879, 0.8330230105418296, 1.281157834722209, 0.9249730999887705, -0.13418041501465638, -0.05506072487979446,0],
                     [-1.277614235886716, 1.2032554596715317, 1.8505613168209687, 1.3360722555393352, -0.19381615502117033, -0.0795321581597031,0]
                     ]
    
    #to starter
    armpositions2 = [[-1.2244786738851403, 1.1466654606827444, 1.8389452559781165, 1.2942624361110875, -0.2510166461787196, -0.08333648328984682,0],
                     [-1.012512922038006, 0.9203328232806657, 1.7924016033769106, 1.1270138178235043, -0.47966471091984314, -0.09988414670054636,0],
                     [-0.8005471701908717, 0.6940001858785869, 1.7458579507757048, 0.9597651995359209, -0.7083127756609666, -0.11643181011124591,0],
                     [-0.5355899803819539, 0.41108438912598844, 1.6876783850241974, 0.7507044266764417, -0.9941228565873711, -0.13711638937462034,0],
                     [-0.37661566649660316, 0.2413349110744294, 1.6527706455732931, 0.6252679629607542, -1.165608905143214, -0.149527136932645,0],
                     [-0.1646499146494691, 0.015002273672350741, 1.6062269929720872, 0.45801934467317085, -1.3942569698843372, -0.16607480034334454,0],
                     [-0.005675600764118368, -0.1547472043792084, 1.5713192535211828, 0.33258288095748323, -1.56574301844018, -0.1784855479013692,0],
                     [4.542435817711521e-06, 2.6698819056036882e-05, -6.75925548421219e-05, 4.8888690798776224e-05, 0.00015322075341828167, 0.0003571909910533577,0]
                     ]

    #to can
    
    armpositions2 = [[-1.2244786738851403, 1.1466654606827444, 1.8389452559781165, 1.2942624361110875, -0.2510166461787196, -0.08333648328984682,0],
                     [-1.012512922038006, 0.9203328232806657, 1.7924016033769106, 1.1270138178235043, -0.47966471091984314, -0.09988414670054636,0],
                     [-0.8005471701908717, 0.6940001858785869, 1.7458579507757048, 0.9597651995359209, -0.7083127756609666, -0.11643181011124591,0],
                     [-0.5355899803819539, 0.41108438912598844, 1.6876783850241974, 0.7507044266764417, -0.9941228565873711, -0.13711638937462034,0],
                     [-0.37661566649660316, 0.2413349110744294, 1.6527706455732931, 0.6252679629607542, -1.165608905143214, -0.149527136932645,0],
                     [-0.1646499146494691, 0.015002273672350741, 1.6062269929720872, 0.45801934467317085, -1.3942569698843372, -0.16607480034334454,0],
                     [-0.005675600764118368, -0.1547472043792084, 1.5713192535211828, 0.33258288095748323, -1.56574301844018, -0.1784855479013692,0],
                     [4.542435817711521e-06, 2.6698819056036882e-05, -6.75925548421219e-05, 4.8888690798776224e-05, 0.00015322075341828167, 0.0003571909910533577,0]
                     ]
    
   # sendArmTrajectory('CUSTOM',armpositions[0],0,'RIGHT')
    
   # time.sleep(10)
    
    
  #  for i in range(0, len(armpositions2)):
   #     sendArmTrajectory('CUSTOM',armpositions2[i],i,'RIGHT')
        #sendArmTrajectory('SPREAD',0,0)
   
    handpelvis = tfBuffer.lookup_transform('pelvis', 'rightPalm', rospy.Time()) #right palm position in pelvis frame
    
    janipont = tf2_geometry_msgs.PointStamped()
    
    janipont.header.frame_id = 'world'
    
    janipont.header.stamp = rospy.Time()
    
    janipont.point.x = 0.011455
    janipont.point.y = -0.865676
    janipont.point.z = 1.254547
    
    
    #trafojanipont = tfBuffer.transform(janipont, 'pelvis')
    
    sendArmTrajectory('SPREAD',armpositions[0],0,'RIGHT')
    
    #print trafojanipont



def sendArmTrajectory(mode,joint_values,ticker,side):   
    
    if mode == 'CUSTOM':
        msg = ArmTrajectoryRosMessage()
        if side == 'RIGHT':
            msg.robot_side = ArmTrajectoryRosMessage.RIGHT
        elif side == 'LEFT':
            msg.robot_side = ArmTrajectoryRosMessage.LEFT
        
        postime = (ticker+1)*0.5
        msg = appendTrajectoryPoint(msg, postime, joint_values)
        msg.unique_id = -1
        rospy.loginfo('publishing custom point')
        armTrajectoryPublisher.publish(msg)
    
    
    elif mode == 'PUSH':
        msg = ArmTrajectoryRosMessage()
        msg.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg = appendTrajectoryPoint(msg, 0.12, ELBOW_BENT_UP_MIDDLE)
        msg = appendTrajectoryPoint(msg, 0.20, ELBOW_BENT_UP)
        msg.unique_id = -1
        rospy.loginfo('publishing right PUSH trajectory')
        armTrajectoryPublisher.publish(msg)

    elif mode == 'PULL':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 0.5, ARM_PULLED_BACK_INIT2)
        msg2 = appendTrajectoryPoint(msg2, 1.0, ARM_DOWN_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right PULL trajectory')
        armTrajectoryPublisher.publish(msg2)
        
        msg3 = ArmTrajectoryRosMessage()
        msg3.robot_side = ArmTrajectoryRosMessage.LEFT
        msg3 = appendTrajectoryPoint(msg3, 0.6, ARM_DOWN_LEFT)
        msg3.unique_id = -2
        rospy.loginfo('publishing left PULL trajectory')
        armTrajectoryPublisher.publish(msg3)

    elif mode == 'DOWN':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 1.0, ARM_DOWN_RIGHT_2)
        msg2.unique_id = -1
        rospy.loginfo('publishing right DOWN trajectory')
        armTrajectoryPublisher.publish(msg2)

        rossleep(0.4)
        
        msg3 = ArmTrajectoryRosMessage()
        msg3.robot_side = ArmTrajectoryRosMessage.LEFT
        msg3 = appendTrajectoryPoint(msg3, 1.0, ARM_DOWN_LEFT)
        msg3.unique_id = -2
        rospy.loginfo('publishing left DOWN trajectory')
        armTrajectoryPublisher.publish(msg3)

    elif mode == 'RIGHTDOWN':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 0.25, ARM_DOWN_RIGHT_INIT)
        msg2 = appendTrajectoryPoint(msg2, 0.60, ARM_DOWN_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right DOWN trajectory')
        armTrajectoryPublisher.publish(msg2)

    elif mode == 'SPREAD':
        msg1 = ArmTrajectoryRosMessage()
        msg1.robot_side = ArmTrajectoryRosMessage.LEFT
        msg1 = appendTrajectoryPoint(msg1, 0.60, ARM_SPREAD_LEFT)
        msg1.unique_id = -1
        rospy.loginfo('publishing left SPREAD trajectory')
        armTrajectoryPublisher.publish(msg1)

        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 0.60, ARM_SPREAD_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right SPREAD trajectory')
        armTrajectoryPublisher.publish(msg2)

def appendTrajectoryPoint(arm_trajectory, time, positions):
    if not arm_trajectory.joint_trajectory_messages:
        arm_trajectory.joint_trajectory_messages = [copy.deepcopy(OneDoFJointTrajectoryRosMessage()) for i in range(len(positions))]
    for i, pos in enumerate(positions):
        point = TrajectoryPoint1DRosMessage()
        point.time = time
        point.position = pos
        point.velocity = 0
        arm_trajectory.joint_trajectory_messages[i].trajectory_points.append(point)
    return arm_trajectory



def rossleep(time):
    try:
        seconds = float(time)
    except (ValueError):
        print("Couldn't convert argument [%s] to float" % sys.argv[1])
    rospy.sleep(rospy.Duration.from_sec(seconds))

if __name__ == '__main__':
    try:
        rospy.init_node('ihmc_walk_test')

        if not rospy.has_param('/ihmc_ros/robot_name'):
            rospy.logerr("Cannot run walk_test.py, missing parameters!")
            rospy.logerr("Missing parameter '/ihmc_ros/robot_name'")

        else:
            ROBOT_NAME = rospy.get_param('/ihmc_ros/robot_name')

            right_foot_frame_parameter_name = "/ihmc_ros/{0}/right_foot_frame_name".format(ROBOT_NAME)
            left_foot_frame_parameter_name = "/ihmc_ros/{0}/left_foot_frame_name".format(ROBOT_NAME)

            if rospy.has_param(right_foot_frame_parameter_name) and rospy.has_param(left_foot_frame_parameter_name):
                RIGHT_FOOT_FRAME_NAME = rospy.get_param(right_foot_frame_parameter_name)
                LEFT_FOOT_FRAME_NAME = rospy.get_param(left_foot_frame_parameter_name)


                
                footStepListPublisher = rospy.Publisher("/ihmc_ros/{0}/control/footstep_list".format(ROBOT_NAME), FootstepDataListRosMessage, queue_size=20)
                footTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/foot_trajectory".format(ROBOT_NAME), FootTrajectoryRosMessage, queue_size=20)
                pelvisHeightTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/pelvis_height_trajectory".format(ROBOT_NAME), PelvisHeightTrajectoryRosMessage, queue_size=20)
                wholeBodyTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/whole_body_trajectory".format(ROBOT_NAME), WholeBodyTrajectoryRosMessage, queue_size=50)
                armTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/arm_trajectory".format(ROBOT_NAME), ArmTrajectoryRosMessage, queue_size=1)
                neckTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/neck_trajectory".format(ROBOT_NAME), NeckTrajectoryRosMessage, queue_size=1)
                chestTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/chest_trajectory".format(ROBOT_NAME), ChestTrajectoryRosMessage, queue_size=20)

                tfBuffer = tf2_ros.Buffer()
                tfListener = tf2_ros.TransformListener(tfBuffer)

                rate = rospy.Rate(10) # 10hz
                time.sleep(1)

                # make sure the simulation is running otherwise wait
                if footStepListPublisher.get_num_connections() == 0:
                    rospy.loginfo('waiting for subsciber...')
                    while footStepListPublisher.get_num_connections() == 0:
                        rate.sleep()

                if not rospy.is_shutdown():
                    walkTest()
            else:
                if not rospy.has_param(left_foot_frame_parameter_name):
                    rospy.logerr("Missing parameter {0}".format(left_foot_frame_parameter_name))
                if not rospy.has_param(right_foot_frame_parameter_name):
                    rospy.logerr("Missing parameter {0}".format(right_foot_frame_parameter_name))

    except rospy.ROSInterruptException:
        pass