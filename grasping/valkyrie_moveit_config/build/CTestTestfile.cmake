# CMake generated Testfile for 
# Source directory: /nasasrc/grasping/valkyrie_moveit_config/src
# Build directory: /nasasrc/grasping/valkyrie_moveit_config/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
