#!/usr/bin/env python

import rospy
import tf2_ros
import tf
import time



# Interactive Marker stuff
from interactive_markers.interactive_marker_server import InteractiveMarkerServer
from interactive_markers.menu_handler import MenuHandler
from visualization_msgs.msg import InteractiveMarkerControl, Marker, InteractiveMarker, \
    InteractiveMarkerFeedback, InteractiveMarkerUpdate, InteractiveMarkerPose, MenuEntry
from geometry_msgs.msg import Pose, PoseStamped, Quaternion, Vector3
from tf.transformations import quaternion_from_euler

# Hand control stuff
from ihmc_msgs.msg import HandTrajectoryRosMessage
from ihmc_msgs.msg import SE3TrajectoryPointRosMessage

"""
Interactive markers that publish PoseStamped messages for each hand.
Also node that subscribes to the PoseStamped topics and sends
IHMC Whole body control messages to the controller.

Author: Sammy Pfeiffer <Sammy.Pfeiffer at student.uts.edu.au>
"""


class InteractiveMarkerPoseStampedPublisher():

    def __init__(self, topic,
                 initial_x, initial_y, initial_z,
                 initial_roll, initial_pitch, initial_yaw,
                 frame_id, side='r'):
        self.side = side
        topic_no_slash = topic.replace('/', '_')
        if topic_no_slash[0] == '_':
            topic_no_slash = topic_no_slash[1:]
        self.marker_name = topic_no_slash + "_im"

        self.server = InteractiveMarkerServer(self.marker_name)
        self.menu_handler = MenuHandler()
        self.pub = rospy.Publisher(topic, PoseStamped, queue_size=1)
        rospy.loginfo("Publishing posestampeds at topic: " +
                      str(self.pub.resolved_name))
        pose = Pose()
        pose.position.x = initial_x
        pose.position.y = initial_y
        pose.position.z = initial_z

        quat = quaternion_from_euler(initial_roll, initial_pitch, initial_yaw)
        pose.orientation = Quaternion(*quat)
        self.initial_pose = pose
        self.frame_id = frame_id

        # self.makeMenuMarker(pose)

        self.makeGraspIM(pose)

        self.server.applyChanges()

    def processFeedback(self, feedback):
        """
        :type feedback: InteractiveMarkerFeedback
        """
        s = "Feedback from marker '" + feedback.marker_name
        s += "' / control '" + feedback.control_name + "'"

        mp = ""
        if feedback.mouse_point_valid:
            mp = " at " + str(feedback.mouse_point.x)
            mp += ", " + str(feedback.mouse_point.y)
            mp += ", " + str(feedback.mouse_point.z)
            mp += " in frame " + feedback.header.frame_id

        if feedback.event_type == InteractiveMarkerFeedback.BUTTON_CLICK:
            rospy.loginfo(s + ": button click" + mp + ".")
        elif feedback.event_type == InteractiveMarkerFeedback.MENU_SELECT:
            rospy.loginfo(s + ": menu item " +
                          str(feedback.menu_entry_id) + " clicked" + mp + ".")
            # When clicking this event triggers!
        elif feedback.event_type == InteractiveMarkerFeedback.POSE_UPDATE:
            rospy.loginfo(s + ": pose changed")
            ps = PoseStamped()
            ps.header.frame_id = self.frame_id
            ps.pose = feedback.pose
            self.pub.publish(ps)

        elif feedback.event_type == InteractiveMarkerFeedback.MOUSE_DOWN:
            rospy.loginfo(s + ": mouse down" + mp + ".")
        elif feedback.event_type == InteractiveMarkerFeedback.MOUSE_UP:
            rospy.loginfo(s + ": mouse up" + mp + ".")
        self.server.applyChanges()

    def makeArrow(self, msg):
        marker = Marker()

        marker.type = Marker.ARROW
        marker.scale.x = 0.15  # msg.scale * 0.3
        marker.scale.y = 0.08  # msg.scale * 0.1
        marker.scale.z = 0.03  # msg.scale * 0.03
        marker.color.r = 0.3
        marker.color.g = 0.3
        marker.color.b = 0.5
        marker.color.a = 1.0

        return marker

    def makeHand(self, side):
        marker = Marker()
        marker.type = Marker.MESH_RESOURCE
        marker.scale.x = 1.0
        marker.scale.y = 1.0
        marker.scale.z = 1.0
        if side == 'r':
            marker.mesh_resource = "package://val_description/model/meshes/arms/palm_right.dae"
            marker.pose.position.y = 0.1
        elif side == 'l':
            marker.mesh_resource = "package://val_description/model/meshes/arms/palm_left.dae"
            marker.pose.position.y = -0.1

        marker.color.r = 0.0
        marker.color.g = 1.0
        marker.color.b = 0.0
        marker.color.a = 0.9
        return marker

    def makeBoxControl(self, msg):
        control = InteractiveMarkerControl()
        control.always_visible = True
        # control.markers.append(self.makeArrow(msg))
        control.markers.append(self.makeHand(self.side))
        msg.controls.append(control)
        return control

    def makeGraspIM(self, pose):
        """
        :type pose: Pose
        """
        int_marker = InteractiveMarker()
        int_marker.header.frame_id = self.frame_id
        int_marker.pose = pose
        int_marker.scale = 0.3

        int_marker.name = self.marker_name  # "6dof_eef"
        int_marker.description = ""  # "EEF 6DOF control"

        # insert a box, well, an arrow
        self.makeBoxControl(int_marker)
        int_marker.controls[
            0].interaction_mode = InteractiveMarkerControl.MOVE_ROTATE_3D

        control = InteractiveMarkerControl()
        control.orientation.w = 1
        control.orientation.x = 1
        control.orientation.y = 0
        control.orientation.z = 0
        control.name = "rotate_x"
        control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
        int_marker.controls.append(control)

        control = InteractiveMarkerControl()
        control.orientation.w = 1
        control.orientation.x = 1
        control.orientation.y = 0
        control.orientation.z = 0
        control.name = "move_x"
        control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
        int_marker.controls.append(control)

        control = InteractiveMarkerControl()
        control.orientation.w = 1
        control.orientation.x = 0
        control.orientation.y = 1
        control.orientation.z = 0
        control.name = "rotate_z"
        control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
        int_marker.controls.append(control)

        control = InteractiveMarkerControl()
        control.orientation.w = 1
        control.orientation.x = 0
        control.orientation.y = 1
        control.orientation.z = 0
        control.name = "move_z"
        control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
        int_marker.controls.append(control)

        control = InteractiveMarkerControl()
        control.orientation.w = 1
        control.orientation.x = 0
        control.orientation.y = 0
        control.orientation.z = 1
        control.name = "rotate_y"
        control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
        int_marker.controls.append(control)

        control = InteractiveMarkerControl()
        control.orientation.w = 1
        control.orientation.x = 0
        control.orientation.y = 0
        control.orientation.z = 1
        control.name = "move_y"
        control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
        int_marker.controls.append(control)

        control = InteractiveMarkerControl()
        control.orientation.w = 1
        control.orientation.x = 0
        control.orientation.y = 0
        control.orientation.z = 1
        control.name = "move_3d"
        control.interaction_mode = InteractiveMarkerControl.MOVE_3D
        int_marker.controls.append(control)

        # self.menu_handler.insert("Do stuff", callback=self.processFeedback)

        # This makes the floating text appear
        # make one control using default visuals
        # control = InteractiveMarkerControl()
        # control.interaction_mode = InteractiveMarkerControl.MENU
        # control.description="Menu"
        # control.name = "menu_only_control"
        # int_marker.controls.append(copy.deepcopy(control))

        # marker = self.makeArrow( int_marker )
        # control.markers.append( marker )
        # control.always_visible = False
        # int_marker.controls.append(control)

        self.server.insert(int_marker, self.processFeedback)
        # self.menu_handler.apply(self.server, int_marker.name)


class PoseHand(object):
    def __init__(self, pose_topic='/valkyrie_right_hand_pose', side='r'):
        self.side = side
        self.sub = rospy.Subscriber(pose_topic,
                                    PoseStamped, self.pose_cb, queue_size=1)
        rospy.loginfo("Subscribed to " + self.side +
                      " hand poses on topic: " + self.sub.resolved_name)

        self.pub = rospy.Publisher('/ihmc_ros/valkyrie/control/hand_trajectory',
                                   HandTrajectoryRosMessage, queue_size=1)
        rospy.loginfo("Publishing on topic: " + self.pub.resolved_name)
        self.id = 42

    def pose_cb(self, ps):
        p = ps.pose.position
        o = ps.pose.orientation
        rospy.loginfo("Marker pose: " + str(ps))
        h = HandTrajectoryRosMessage()
        h.unique_id = self.id
        self.id += 1
        if self.side == 'r':
            h.robot_side = HandTrajectoryRosMessage.RIGHT
        elif self.side == 'l':
            h.robot_side = HandTrajectoryRosMessage.LEFT
        else:
            rospy.logwarn("Wrong side defined on constructor...")
        h.execution_mode = HandTrajectoryRosMessage.OVERRIDE
        h.base_for_control = HandTrajectoryRosMessage.WORLD

        t = SE3TrajectoryPointRosMessage()
        t.time = 1.0
        t.position = Vector3(p.x, p.y, p.z)
        t.orientation = o
        t.unique_id = 0  # Ignored

        h.taskspace_trajectory_points.append(t)

        rospy.loginfo("Sending goal: " + str(h))
        self.pub.publish(h)


if __name__ == "__main__":
    rospy.init_node("pose_stamped_im")
    
    tfBuffer = tf2_ros.Buffer()
    tfListener = tf2_ros.TransformListener(tfBuffer)
    
    time.sleep(2)
    
    starting_position = SE3TrajectoryPointRosMessage()
 
    handWorld = tfBuffer.lookup_transform('world', 'rightPalm', rospy.Time())
 
    starting_position.orientation = handWorld.transform.rotation 
    starting_position.position = handWorld.transform.translation
    
    starting_orient = tf.transformations.euler_from_quaternion([starting_position.orientation.x,
                                                                starting_position.orientation.y,
                                                                starting_position.orientation.z,
                                                                starting_position.orientation.w])

    
    rospy.loginfo("Running Interactive marker...")
    ig = InteractiveMarkerPoseStampedPublisher('/valkyrie_right_hand_pose',
                                               starting_position.position.x, starting_position.position.y, starting_position.position.z,
                                               starting_orient[0], starting_orient[1], starting_orient[2],
                                               'world',
                                               side='r')
    
    
    handWorld = tfBuffer.lookup_transform('world', 'leftPalm', rospy.Time())
 
    starting_position.orientation = handWorld.transform.rotation 
    starting_position.position = handWorld.transform.translation
    
    starting_orient = tf.transformations.euler_from_quaternion([starting_position.orientation.x,
                                                                starting_position.orientation.y,
                                                                starting_position.orientation.z,
                                                                starting_position.orientation.w])

    
    ig = InteractiveMarkerPoseStampedPublisher('/valkyrie_left_hand_pose',
                                               starting_position.position.x, starting_position.position.y, starting_position.position.z,
                                               starting_orient[0], starting_orient[1], starting_orient[2],
                                               'world',
                                               side='l')



    rospy.loginfo("Running hand pose publisher...")
    ph = PoseHand(pose_topic='/valkyrie_right_hand_pose', side='r')
    ph = PoseHand(pose_topic='/valkyrie_left_hand_pose', side='l')

    rospy.spin()
