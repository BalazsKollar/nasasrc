#!/usr/bin/env python

# Python libs
import sys, time
import numpy as np
from scipy.misc import imsave

# Ros
import rospy
from sensor_msgs.msg import Image


if __name__ == '__main__':
    outfile = sys.argv[1]
    print "saving to", outfile
    rospy.init_node('image_feature', anonymous=True)
    msg = rospy.wait_for_message("/multisense/camera/right/image_color", Image)
    print msg.width, msg.height
    im = np.fromstring(msg.data, dtype=np.uint8).reshape((msg.height, msg.width, 3))[::-1,::-1]
    imsave(outfile, im)

