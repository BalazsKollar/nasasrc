#!/usr/bin/env python
import urllib
import time
import os
import sys
import signal
from shutil import copyfile
import subprocess

if __name__ == '__main__':
    if len(sys.argv) <3:
        print "Need more inputs: first resampling rate, then refresh rate"
        quit()
    
    try:
        monostereo="A"
        while monostereo!="m" and monostereo!="s":
            monostereo=raw_input("mono(m) or stereo(s): ")
        if monostereo == "s":
            print ("starting to download a huge png file")
            time.sleep(5)
        else: 
            blackwhite="A"
            while blackwhite!="y" and blackwhite!="n":
               blackwhite=raw_input("black and white? (y/n): ")
        
        #getting the filenames
        if monostereo == "s":
            imleft=str("http://172.17.0.1:8000/monkey_images/camim_left_216.png")
            imright=str("http://172.17.0.1:8000/monkey_images/camim_right_216.png")
        else:
            if blackwhite== "n":
                 imleft=str("http://172.17.0.1:8000/monkey_images/camim_left_"+str(sys.argv[1])+str(sys.argv[2])+".jpg")
            else:
                 imleft=str("http://172.17.0.1:8000/monkey_images/camim_left_"+str(sys.argv[1])+str(sys.argv[2])+"_bw.jpg")
        
        refresh=float(sys.argv[2])
        FNULL = open(os.devnull, 'w')
        monkey_island = subprocess.Popen(str("./monkey_island "+str(sys.argv[1])+" "+str(sys.argv[2])), shell=True, stdout=FNULL, preexec_fn=os.setsid)
        time.sleep(8)        
        while True:
            start_time = time.time()
            urllib.urlretrieve (imleft, "./monkey_image_left")
            if monostereo == "s":
                urllib.urlretrieve (imright, "./monkey_image_right")
            else:
                copyfile("./monkey_image_left", "./monkey_image_right")
#            print time.time()-start_time            
            os.rename("./monkey_image_left", "./monkey_image_left.jpg")            
            os.rename("./monkey_image_right", "./monkey_image_right.jpg") 
#            print time.time()-start_time  
#            print"step"
            time.sleep(0.5*refresh- time.time()+start_time)
    except KeyboardInterrupt:
        os.killpg(os.getpgid(monkey_island.pid), signal.SIGKILL)
        print('\n somebody pressed some buttons\n')
