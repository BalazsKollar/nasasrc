#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>
#include <opencv2/core/core.hpp>
#include <opencv/cv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <vector>
#include <thread>
#include <iostream>
#include <unistd.h>


//g++ -o monkey_island monkey_island.cpp `pkg-config opencv --cflags --libs` -std=c++11

using namespace cv;
using namespace std;

//Global variables
float rgb_left [3]={0,0,0}, rgb_right [3]={0,0,0}, loc_left [2]={0,0}, loc_right [2]={0,0}; //what either camera sees - LED color and position
int blur_size = 0; //by how much we want to blur the image, at least 1 four outer rim of pavement, otherwise 0
int const max_blur_size = 40;
int dilate_size = 0; //by how much we want to dilate the image, 0 is OK all over
int const max_dilate_size = 40;
//the colors we are looking for
int lowred=0, upred = 255 ; //[0,255] for red wheel, [61,68] four outer rim of pavement
int lowgreen = 0, upgreen=255; //[236,255] for red wheel, [0,0] four outer rim of pavement
int lowblue = 0, upblue = 255; //[0,0] for red wheel, [0,0] four outer rim of pavement

//the resampling rate
int resample;
bool left_image_pub=0, right_image_pub=0;

Mat src_left, src_right, dst; //the variable the image is stored in
Rect image_rect;
Mat3b canvas;
Rect red_button, blue_button, eraser_button, eraser_remove_button, addpoints_image_button, addpoints_poi_button, delete_button, pointdata, publish_button;
string red_buttonText("RED"), blue_buttonText("BLUE"), eraser_buttonText("ERASE POINTS"), eraser_remove_buttonText("REMOVE ERASER"), addpoints_poi_buttonText("ADD POINT OF INTEREST"), addpoints_image_buttonText("ADD POINTS FROM IMAGE"), delete_buttonText("DELETE LAST POINT"), pointdata_pic("Smaller Image"), pointdata_orig_pic("Original Image"), publish1_buttonText("PUBLISH"), publish2_buttonText("PUBLISHING");
Scalar font_color = Scalar(0,0,0);
vector<double> image_points, points_to_publish;
double point_loc_data[3];

bool addpoints_state=0, erasing_state=0; // are we adding points to the iamge
int num=0, boxheight=0; // how many points has been added to the image so far, where should it write the data on the gui
bool lets_publish=0; //if we publish the result
Point2f mc[2];
char* window_name = "Image";
char* window_name2 = "Trackbars";


//Function declarations
float* location_color(cv::Mat diffIm);//this gives the center of the LED - on the image it is called on
void imageCallback(const sensor_msgs::ImageConstPtr& msg, bool which_cam); //This gets called every time an image gets published
void ButtonClick(int event, int x, int y, int flags, void* userdata); //what happens if we click the image
void ButtonClick2(int event, int x, int y, int flags, void* userdata); //what happens if we click the "menu"
void ColorMassCent(int, void*); //showing the image
void publish_data();



//----------------GUI-----------------------------------
//If we click on the image
void ButtonClick(int event, int x, int y, int flags, void* userdata)
{
  if (event==EVENT_LBUTTONDOWN)
  {
    if (addpoints_state & x<1024/resample & &x>0 & y<544/resample & y>0 & num<20)
    {
	font_color = Scalar(0,0,0);
        num++;
	ostringstream buffer;
	point_loc_data[0]=x;point_loc_data[1]=y;point_loc_data[2]=0;
	image_points.insert (image_points.end(),point_loc_data,point_loc_data+3);
	buffer.str("");
        buffer<<"("<<image_points[(num-1)*3]<<","<<image_points[(num-1)*3+1]<<")"; 
        string pointloc_Text(buffer.str());
        putText(canvas(pointdata),pointloc_Text,Point(pointdata.width*0.01,pointdata.height*0.05+num*14),FONT_HERSHEY_PLAIN,1,font_color);
	//pointloc_Text(buffer.str());
	point_loc_data[0]=resample*x; point_loc_data[1]=resample*y;
	points_to_publish.insert (points_to_publish.end(),point_loc_data,point_loc_data+3);
	buffer.str("");
        buffer<<"("<<points_to_publish[(num-1)*3]<<","<<points_to_publish[(num-1)*3+1]<<")"; 
	putText(canvas(pointdata),buffer.str(),Point(pointdata.width*0.51,pointdata.height*0.05+num*14),FONT_HERSHEY_PLAIN,1,font_color);
	imshow(window_name2,canvas);
	ColorMassCent(0, 0);
    }
  }
  waitKey(1);
}

//IF we click the menu
void ButtonClick2(int event, int x, int y, int flags, void* userdata)
{
  if (event==EVENT_LBUTTONDOWN)
  {
    if (red_button.contains(Point(x,y)))
    {
       lowred=0; upred = 255;lowgreen = 236; upgreen=255; lowblue = 0; upblue = 0;
       font_color = Scalar(0,0,255);
       point_loc_data[2]=1;
       rectangle(canvas,red_button,Scalar(0,0,255),2);
       ColorMassCent(0, 0);
    }
    if (blue_button.contains(Point(x,y)))
    {
       lowred=0; upred = 255;lowgreen = 236; upgreen=255; lowblue = 103; upblue = 146;
       font_color = Scalar(255,0,0);
       point_loc_data[2]=2;
       rectangle(canvas,blue_button,Scalar(0,0,255),2);
       ColorMassCent(0, 0);
    }
    if (eraser_button.contains(Point(x,y)))
    {
	if(erasing_state)
	{
		erasing_state=0;
		canvas(eraser_button)=Vec3b(255,255,255);
	}
	else
	{
		erasing_state=1;
		canvas(eraser_button)=Vec3b(200,200,250);
	}
	putText(canvas(eraser_button),eraser_buttonText,Point(eraser_button.width*0.1,eraser_button.height*0.7),FONT_HERSHEY_PLAIN,1,Scalar(0,0,0)); 
		
    }
    if (addpoints_poi_button.contains(Point(x,y)) & num<19)
    {
	int j;
       	ostringstream buffer;
       	rectangle(canvas,addpoints_poi_button,Scalar(0,0,255),2);
	if(point_loc_data[2]==0)
	{
		point_loc_data[2]=3;
		font_color = Scalar(0,150,0);		
	}	
	for(j=0;j<2;j++)
	{
		num++;
	       	point_loc_data[0]=mc[j].x;point_loc_data[1]=mc[j].y;
	       	image_points.insert (image_points.end(),point_loc_data,point_loc_data+3);
		buffer.str("");
	       	buffer<<"("<<image_points[(num-1)*3]<<","<<image_points[(num-1)*3+1]<<")"; 
	       	string pointloc_Text(buffer.str());
	       	putText(canvas(pointdata),pointloc_Text,Point(pointdata.width*0.01,pointdata.height*0.05+num*14),FONT_HERSHEY_PLAIN,1,font_color);  
	
		point_loc_data[0]=resample*mc[j].x; point_loc_data[1]=resample*mc[j].y;
		points_to_publish.insert (points_to_publish.end(),point_loc_data,point_loc_data+3);
		buffer.str("");
	        buffer<<"("<<points_to_publish[(num-1)*3]<<","<<points_to_publish[(num-1)*3+1]<<")"; 
		putText(canvas(pointdata),buffer.str(),Point(pointdata.width*0.51,pointdata.height*0.05+num*14),FONT_HERSHEY_PLAIN,1,font_color);
	}
	point_loc_data[2]=0;
      	ColorMassCent(0, 0);
    }	
    if (addpoints_image_button.contains(Point(x,y)))
    {
	if(addpoints_state)
	{
		addpoints_state=0;
		rectangle(canvas,addpoints_image_button,Scalar(0,0,255),2);
		canvas(addpoints_image_button)=Vec3b(200,255,200);
	       	ColorMassCent(0, 0);
	}
	else
	{
	       addpoints_state=1;
	       rectangle(canvas,addpoints_image_button,Scalar(0,0,255),2);
	       canvas(addpoints_image_button)=Vec3b(100,200,0);
	       ColorMassCent(0, 0);
 	}
	putText(canvas(addpoints_image_button),addpoints_image_buttonText,Point(addpoints_image_button.width*0.1,addpoints_image_button.height*0.7),FONT_HERSHEY_PLAIN,1,Scalar(0,0,0));   
    }
    if (delete_button.contains(Point(x,y)))
    {
	rectangle(canvas,delete_button,Scalar(0,0,255),2);  
	if(num>0)
	{
		image_points.pop_back();image_points.pop_back();image_points.pop_back();
		points_to_publish.pop_back();points_to_publish.pop_back();points_to_publish.pop_back();
    		num--;
       		rectangle(canvas,Point(0,boxheight-284+num*14),Point(300,boxheight-284+num*14+15),Scalar(255,255,255),CV_FILLED);
	}
	ColorMassCent(0, 0);
    }
    if (publish_button.contains(Point(x,y)))
    {
	if(lets_publish)
	{
            lets_publish=0;
            int i=0;
            rectangle(canvas,publish_button,Scalar(0,0,255),2);
            canvas(publish_button)=Vec3b(200,255,200);
            ColorMassCent(0, 0);
            putText(canvas(publish_button),publish1_buttonText,Point(publish_button.width*0.1,publish_button.height*0.7),FONT_HERSHEY_PLAIN,1,Scalar(0,0,0)); 
	}
	else
	{
                publish_data();
                lets_publish=1;
                rectangle(canvas,publish_button,Scalar(0,0,255),2);
                canvas(publish_button)=Vec3b(100,200,0);
	       	//ColorMassCent(0, 0);
                putText(canvas(publish_button),publish2_buttonText,Point(publish_button.width*0.1,publish_button.height*0.7),FONT_HERSHEY_PLAIN,1,Scalar(0,0,0));  
 	} 
    }
  }
  if (event== EVENT_LBUTTONUP)
  {
	rectangle(canvas,red_button,Scalar(200,200,200),2);
        rectangle(canvas,blue_button,Scalar(200,200,200),2);
	rectangle(canvas,addpoints_poi_button,Scalar(200,255,200),2);
	rectangle(canvas,addpoints_image_button,Scalar(200,255,200),2);
	rectangle(canvas,delete_button,Scalar(50,200,200),2);
  }
  imshow(window_name2, canvas);
  waitKey(1);
}

//Showing all the stuff
void ColorMassCent(int, void*)
{
  	Mat blur_src_temp, blur_src, src_temp, src_temp_left,src_temp_right;
  	int i=0, j=0;
	
	while(j<2)
	{
		if (j==0)
		{
			src_temp=src_left.clone();
		}
		else
		{
			src_temp=src_right.clone();
		}
  	
		//Blurring image
  		Mat element = getStructuringElement(  cv::MORPH_RECT,cv::Size(blur_size+1,blur_size+1),cv::Point( -1, -1 ) );
  		erode(src_temp,blur_src_temp,element);

            //Selecting the color - everything else except the chosen color gets thrown out
            cvtColor(blur_src_temp, blur_src, cv::COLOR_BGR2HSV);
            inRange(blur_src, cv::Scalar(lowblue, lowgreen, lowred), cv::Scalar(upblue, upgreen, upred), blur_src);

  		//Dilating the image
  		element = getStructuringElement(  cv::MORPH_RECT,cv::Size(dilate_size+1,dilate_size+1),cv::Point( -1, -1 ) );
  		dilate(blur_src,blur_src_temp,element);

  		// Calculating the center of mass for the image
  		Moments mu=moments( blur_src_temp, false );
  		mc[j]=Point2f( mu.m10/mu.m00 , mu.m01/mu.m00);  

  		/// Show in a window
  		Scalar color = Scalar(0,255,0);
  		circle( src_temp, mc[j], max(4/resample,1), color, -1, 8, 0 );

  		//Plot points added with mouse
		if(j==0)
		{  	
			while(i<image_points.size())
	  		{
				if(image_points[i+2]==0 | image_points[i+2]==3)
				{
				  	Scalar color = Scalar(200,points_to_publish[i+2]*50,200-points_to_publish[i+2]*50);
				 	Point2f added_point=Point2f(image_points[i],image_points[i+1]);
				  	circle( src_temp, added_point, max(4/resample,1), color, -1, 8, 0 ); 
				}
				i=i+3;
	  		}
		}

  		cvtColor(blur_src_temp, blur_src_temp, cv::COLOR_GRAY2BGR);
		if (j==0)
		{
			vconcat(src_temp,blur_src_temp,src_temp_left);
		}
		else
		{
			vconcat(src_temp,blur_src_temp,src_temp_right);
		}
		j++;
	}	
  	hconcat(src_temp_left,src_temp_right,src_temp);
  	imshow(window_name,src_temp);
}

void publish_data()
{
        ostringstream oss;
        if(!points_to_publish.empty())
        {
            copy(points_to_publish.begin(),points_to_publish.end()-1,ostream_iterator<int>(oss,","));
            oss<<points_to_publish.back();
        }
        string bashCommand="python ./monkey_commander.py " + oss.str();
        system(bashCommand.c_str());
}

void image_read()
{
            while (1) //This gets called constantly
            {
                Mat src_left_temp=imread("./monkey_image_left.jpg", CV_LOAD_IMAGE_COLOR);
                Mat src_right_temp=imread("./monkey_image_right.jpg", CV_LOAD_IMAGE_COLOR);
                if(!src_left_temp.empty() && !src_right_temp.empty())
        		{
        			flip(src_left_temp, src_left, -1);
        			flip(src_right_temp, src_right, -1);
        	           ColorMassCent(0,0);
        		}
                 std::this_thread::sleep_for (std::chrono::milliseconds(100));	  	
            }
}

int main(int argc, char **argv)
{

	if(argc!=3) {cout<<"need the resample and refresh rate as input\n"; return -1;}
	resample=atof(argv[1]);
      int refresh=atof(argv[2]);    

	//--------Stuff for the GUI
	//adding the buttons
  	int height=0;
  	red_button=Rect(0,0,150,25); blue_button=Rect(150,0,150,25); 
  	height=red_button.height;
	eraser_button=Rect(0,height,150,25); eraser_remove_button=Rect(150,height,150,25); 
  	height=height+eraser_button.height;
  	addpoints_poi_button=Rect(0,height,300,25);
  	height=height+addpoints_poi_button.height;
  	addpoints_image_button=Rect(0,height,300,25);
  	height=height+addpoints_image_button.height;
  	delete_button=Rect(0,height,300,25);
  	height=height+delete_button.height;
  	pointdata=Rect(0,height,300,300);
  	height=height+pointdata.height;
  	boxheight=height;
  	publish_button=Rect(0,height,300,25);
  	height=height+addpoints_image_button.height;

  	canvas=Mat3b(height+10,red_button.width+blue_button.width,Vec3b(0,0,0));
  	canvas(red_button)=Vec3b(0,50,255);
  	putText(canvas(red_button),red_buttonText,Point(red_button.width*0.35,red_button.height*0.7),FONT_HERSHEY_PLAIN,1,Scalar(0,0,0));
  	canvas(blue_button)=Vec3b(200,100,0);
  	putText(canvas(blue_button),blue_buttonText,Point(red_button.width*0.35,red_button.height*0.7),FONT_HERSHEY_PLAIN,1,Scalar(0,0,0));
	canvas(eraser_button)=Vec3b(255,255,255);
  	putText(canvas(eraser_button),eraser_buttonText,Point(eraser_button.width*0.1,eraser_button.height*0.7),FONT_HERSHEY_PLAIN,1,Scalar(0,0,0));
  	canvas(eraser_remove_button)=Vec3b(200,200,200);
  	putText(canvas(eraser_remove_button),eraser_remove_buttonText,Point(eraser_remove_button.width*0.05,eraser_remove_button.height*0.7),FONT_HERSHEY_PLAIN,1,Scalar(0,0,0));
  	
  	canvas(addpoints_poi_button)=Vec3b(200,255,200);
  	putText(canvas(addpoints_poi_button),addpoints_poi_buttonText,Point(addpoints_poi_button.width*0.1,addpoints_poi_button.height*0.7),FONT_HERSHEY_PLAIN,1,Scalar(0,0,0));
  	canvas(addpoints_image_button)=Vec3b(200,255,200);
  	putText(canvas(addpoints_image_button),addpoints_image_buttonText,Point(addpoints_image_button.width*0.1,addpoints_image_button.height*0.7),FONT_HERSHEY_PLAIN,1,Scalar(0,0,0));
  	canvas(delete_button)=Vec3b(50,200,200);
  	putText(canvas(delete_button),delete_buttonText,Point(delete_button.width*0.1,delete_button.height*0.7),FONT_HERSHEY_PLAIN,1,Scalar(0,0,0));
  	canvas(pointdata)=Vec3b(255,255,255);
  	putText(canvas(pointdata),pointdata_pic,Point(pointdata.width*0.01,pointdata.height*0.05),FONT_HERSHEY_PLAIN,1,Scalar(0,0,0));
  	putText(canvas(pointdata),pointdata_orig_pic,Point(pointdata.width*0.55,pointdata.height*0.05),FONT_HERSHEY_PLAIN,1,Scalar(0,0,0));
  	canvas(publish_button)=Vec3b(200,255,200);
  	putText(canvas(publish_button),publish1_buttonText,Point(publish_button.width*0.1,publish_button.height*0.7),FONT_HERSHEY_PLAIN,1,Scalar(0,0,0));


	/// Create the windows
	namedWindow( window_name, WINDOW_NORMAL);
  	resizeWindow(window_name, 1024, 512); 
  	setMouseCallback(window_name,ButtonClick);
  	namedWindow( window_name2, WINDOW_NORMAL);
      resizeWindow(window_name2, 256, 700);
  	setMouseCallback(window_name2,ButtonClick2);
	startWindowThread();
	
	/// Create a Trackbar for user to enter threshold
  	//createTrackbar( "Blur size:", window_name2, &blur_size, max_blur_size, ColorMassCent );
  	//createTrackbar( "Dilate size:", window_name2, &dilate_size,max_dilate_size, ColorMassCent );
      createTrackbar( "Min red:", window_name2, &lowred, 255, ColorMassCent );
      createTrackbar( "Max red:", window_name2, &upred, 255, ColorMassCent );
      createTrackbar( "Min green:", window_name2, &lowgreen, 255, ColorMassCent );
      createTrackbar( "Max green:", window_name2, &upgreen, 255, ColorMassCent );
      createTrackbar( "Min blue :", window_name2, &lowblue, 255, ColorMassCent );
      createTrackbar( "Max blue:", window_name2, &upblue, 255, ColorMassCent );

	Mat bootimage;
	char* location=(char *)argv[0];
	string location_str=string(location);
	location_str.erase( location_str.size() - 13 );
	location_str=location_str+"THAD.png";
	cout <<location_str;
	bootimage = imread(location_str,CV_LOAD_IMAGE_UNCHANGED);
	if(!bootimage.empty()) {
		imshow(window_name, bootimage);
		sleep(2);
	} 

	//-----GUI finished

        std::thread image_refreshing (image_read); 
        image_refreshing.join();
        cv::destroyWindow(window_name);
        cv::destroyWindow(window_name2);
        return 0;
}

