import os
import sys
import signal
import time
import subprocess
import socket

#rostopic pub MI_pub std_msgs/Float32MultiArray "{layout.dim:15,data:[511.5, 271.5, 3.0, 511.5, 271.5, 3.0, 492.0, 427.0, 0.0, 497.0, 325.0, 0.0, 405.0, 274.0, 0.0]}"

if __name__ == '__main__':
#    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#    server_address = ('172.17.0.1', 8002)    
#    print >>sys.stderr, 'connecting to %s port %s' % server_address    
#    sock.connect(server_address)    
    FNULL = open(os.devnull, 'w')
    try:
        pointlist=str(sys.argv[1:]).replace("'","")
        command = str("rostopic pub MI_pub std_msgs/Float32MultiArray '{data:"+pointlist+"}' &")
        print >>sys.stderr, '\n command "%s"' % command
#        sock.sendall(command)        
#        time.sleep(2)
#        print >>sys.stderr, 'command sent'
    finally:
        print("")
#        sock.close()
        