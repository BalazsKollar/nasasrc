#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>

using namespace cv;
using namespace std;

Mat src; //the variable the image is stored in

int blur_size = 0; //at least 1 four outer rim of pavement
int const max_blur_size = 40;
int dilate_size = 0;
int const max_dilate_size = 40;
int kernel_size = 3;
int lowred=0, upred = 255 ; //[0,255] for red wheel, [61,68] four outer rim of pavement
int lowgreen = 236, upgreen=255; //[236,255] for red wheel, [0,0] four outer rim of pavement
int lowblue = 103, upblue = 146; //[0,0] for red wheel, [0,0] four outer rim of pavement
char* window_name = "Image";
char* window_name2 = "Trackbars";

void ColorMassCent(int, void*)
{
  Mat blur_src_temp, blur_src, src_temp=src.clone();

  //Blurring image
  Mat element = getStructuringElement(  cv::MORPH_RECT,cv::Size(blur_size+1,blur_size+1),cv::Point( -1, -1 ) );
  erode(src,blur_src_temp,element);

  //Selecting the color - everything else except the chosen color gets thrown out
  cvtColor(blur_src_temp, blur_src, cv::COLOR_BGR2HSV);
  inRange(blur_src, cv::Scalar(lowblue, lowgreen, lowred), cv::Scalar(upblue, upgreen, upred), blur_src);

  //Dilating the image
  element = getStructuringElement(  cv::MORPH_RECT,cv::Size(dilate_size+1,dilate_size+1),cv::Point( -1, -1 ) );
  dilate(blur_src,blur_src_temp,element);

  // Calculating the center of mass for the image
  Moments mu=moments( blur_src_temp, false );
  Point2f mc=Point2f( mu.m10/mu.m00 , mu.m01/mu.m00);  

  /// Show in a window
  Scalar color = Scalar(0,255,0);

  circle( src_temp, mc, 4, color, -1, 8, 0 );
  cvtColor(blur_src_temp, blur_src_temp, cv::COLOR_GRAY2BGR);
  vconcat(src_temp,blur_src_temp,blur_src_temp);
  imshow( window_name, blur_src_temp );
}

int main( int argc, char** argv )
{
  /// Load an image
  src = imread( argv[1] );

  if( !src.data )
  { return -1; }

  /// Create a matrix of the same type and size as src (for dst)
  dst.create( src.size(), src.type() );

  /// Convert the image to grayscale
  cvtColor( src, src_gray, CV_BGR2GRAY );

  /// Create a window
  namedWindow( window_name, WINDOW_NORMAL);
  resizeWindow(window_name, 1024, 960);
  namedWindow( window_name2, WINDOW_NORMAL);


  /// Create a Trackbar for user to enter threshold
  createTrackbar( "Blur size:", window_name2, &blur_size, max_blur_size, ColorMassCent );
createTrackbar( "Dilate size:", window_name2, &dilate_size,max_dilate_size, ColorMassCent );
  createTrackbar( "Min red:", window_name2, &lowred, 255, ColorMassCent );
  createTrackbar( "Max red:", window_name2, &upred, 255, ColorMassCent );
  createTrackbar( "Min green:", window_name2, &lowgreen, 255, ColorMassCent );
  createTrackbar( "Max green:", window_name2, &upgreen, 255, ColorMassCent );
  createTrackbar( "Min blue :", window_name2, &lowblue, 255, ColorMassCent );
  createTrackbar( "Max blue:", window_name2, &upblue, 255, ColorMassCent );


  /// Show the image
  ColorMassCent(0, 0);

  /// Wait until user exit program by pressing a key
  waitKey(0);

  return 0;
  }
