#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>

using namespace cv;
using namespace std;

Mat src, src_gray;
Mat dst, detected_edges;

int edgeThresh = 1;
int lowThreshold;
int const max_lowThreshold = 100;
int window_size = 100;
int const max_window_size = 200;
int blur_size = 0;
int const max_blur_size = 40;
int dilate_size = 6;
int const max_dilate_size = 40;
int kernel_size = 3;
int lowred=0, upred = 255;
int lowgreen = 246, upgreen=255;
int lowblue = 80, upblue = 146;
char* window_name = "Edge Map";
char* window_name2 = "Trackbars";

void CannyThreshold(int, void*)
{
  Mat canny_output, blur_src_temp, blur_src;

  Mat element = getStructuringElement(  cv::MORPH_RECT,cv::Size(blur_size+1,blur_size+1),cv::Point( -1, -1 ) );
  erode(src,blur_src_temp,element);

  cvtColor(blur_src_temp, blur_src, cv::COLOR_BGR2HSV);
  inRange(blur_src, cv::Scalar(lowblue, lowgreen, lowred), cv::Scalar(upblue, upgreen, upred), blur_src);
element = getStructuringElement(  cv::MORPH_RECT,cv::Size(dilate_size+1,dilate_size+1),cv::Point( -1, -1 ) );
dilate(blur_src,blur_src,element);
  
  /// Find contours
  vector<vector<Point> > contours;
  Canny( blur_src.clone(), canny_output, lowThreshold, lowThreshold+window_size+50, 3 );
  //findContours( canny_output, contours, RETR_TREE, CHAIN_APPROX_SIMPLE );
  vector<Vec4i> hierarchy;
  findContours( canny_output, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

  // Detect edges using cann
  //int thresh=100;
  //Canny( src, canny_output, thresh, thresh*2, 3 );
  // Find contours
  //findContours( canny_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

  Mat resImage0=Mat::zeros( blur_src.size(), CV_8UC3 );
  resImage0=Scalar(255,255,255);
  /// Draw contours
  for( size_t i = 0; i< contours.size(); i++ )
     {
       Scalar color = Scalar( 0, 0, 255 );
       Rect _boundingRect = boundingRect( contours[i] );
       Scalar mean_color0 = mean( blur_src_temp( _boundingRect ) );       
       drawContours( resImage0, contours, (int)i, mean_color0, FILLED );
     }

  /// Show in a window
  //imshow( window_name, blur_src );
vconcat(src,resImage0,resImage0);
  imshow( window_name, resImage0 );
}

int main( int argc, char** argv )
{
  /// Load an image
  src = imread( argv[1] );

  if( !src.data )
  { return -1; }

  /// Create a matrix of the same type and size as src (for dst)
  dst.create( src.size(), src.type() );

  /// Convert the image to grayscale
  cvtColor( src, src_gray, CV_BGR2GRAY );

  /// Create a window
  namedWindow( window_name, WINDOW_NORMAL);
  resizeWindow(window_name, 1024, 740);
  namedWindow( window_name2, WINDOW_NORMAL);

  /// Create a Trackbar for user to enter threshold
  createTrackbar( "Blur size:", window_name2, &blur_size, max_blur_size, CannyThreshold );
createTrackbar( "Dilate size:", window_name2, &dilate_size,max_dilate_size, CannyThreshold );
  createTrackbar( "Min red:", window_name2, &lowred, 255, CannyThreshold );
  createTrackbar( "Max red:", window_name2, &upred, 255, CannyThreshold );
  createTrackbar( "Min green:", window_name2, &lowgreen, 255, CannyThreshold );
  createTrackbar( "Max green:", window_name2, &upgreen, 255, CannyThreshold );
  createTrackbar( "Min blue :", window_name2, &lowblue, 255, CannyThreshold );
  createTrackbar( "Max blue:", window_name2, &upblue, 255, CannyThreshold );
  createTrackbar( "Min Threshold:", window_name2, &lowThreshold, max_lowThreshold, CannyThreshold );
  createTrackbar( "Window size:", window_name2, &window_size, max_window_size, CannyThreshold );


  /// Show the image
  CannyThreshold(0, 0);

  /// Wait until user exit program by pressing a key
  waitKey(0);

  return 0;
  }
