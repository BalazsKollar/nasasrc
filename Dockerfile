FROM vecseimiklos/thad4

# include nasasrc stuff
ADD ./grasping /nasasrc/grasping
ADD ./finals /nasasrc/finals
ADD ./misc /nasasrc/misc
ADD ./task1 /nasasrc/task1
ADD ./docker_data/ /home/docker/ws/docker_data/

ADD ./nasa /opt/nasa

EXPOSE 8000
EXPOSE 8001
EXPOSE 8002
EXPOSE 8003
EXPOSE 8004
EXPOSE 8005
EXPOSE 8006
ENV ROS_MASTER_URI http://127.0.0.1:8001

# startup script
# simple HTTP server and a roscore
ADD ./docker_data/startup.bash ./startup.bash
CMD ["./startup.bash"]
