#!/bin/bash

LOGFILE=$2
OUTFILE=$1

while true; do
  wget -O $OUTFILE "$LOGFILE"
  sleep 5
done
