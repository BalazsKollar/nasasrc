#!/bin/bash

LOGFILE=$1
OUTFILE=$2

while true; do
  cat $LOGFILE | tail -5 > $OUTFILE
  sleep 5
done
