#!/bin/bash
source /opt/ros/indigo/setup.bash
source /home/docker/ws/devel/setup.bash

ln /dev/null /dev/raw1394

python -m SimpleHTTPServer 8000 &
roscore -p 8001 &

mkdir /home/docker/ws/monkey_images

python /home/docker/ws/docker_data/server.py 8002 > /home/docker/ws/logs/server8002_logfile.log &
python /home/docker/ws/docker_data/server.py 8003 > /home/docker/ws/logs/server8003_logfile.log &
python /home/docker/ws/docker_data/server.py 8004 > /home/docker/ws/logs/server8004_logfile.log &
python /home/docker/ws/docker_data/server.py 8005 > /home/docker/ws/logs/server8005_logfile.log &
python /home/docker/ws/docker_data/server.py 8006 > /home/docker/ws/logs/server8006_logfile.log &


#starting the nodes
sleep 300

bash /home/docker/ws/docker_data/start_grasping.bash &
bash /home/docker/ws/docker_data/start_navigation.bash &
bash /home/docker/ws/docker_data/start_monkey.bash &
bash /home/docker/ws/docker_data/logcopy.bash /home/docker/ws/logs/daemon_logfile_x.log /home/docker/ws/logs/daemon_logfile_x_short.log &

sleep 120

bash /home/docker/ws/docker_data/start_daemon.bash 


 while :; do echo ''; sleep 1; done
