import socket
import sys
import time
import signal

def catch_ctrl_C(sig,frame):
    print " only if you say prettyprettypleasestop \ncommand: "

if __name__ == '__main__':
    signal.signal(signal.SIGINT, catch_ctrl_C)
    
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    # Connect the socket to the port on the server given by the caller
    #server_address = ('192.168.2.10', 8002)
    server_address = ('192.168.2.10', 8002)
    print >>sys.stderr, 'connecting to %s port %s' % server_address
    sock.connect(server_address)
    
    dontstopbelieving=True
    try:
        print >>sys.stderr, 'creating scripts folder on server'
        message="mkdir /home/docker/ws/scripts"
        sock.sendall(message)
        time.sleep(3)
        while dontstopbelieving:
            filename = raw_input("file name: ")
            if filename == "prettyprettypleasestop":
                dontstopbelieving=False
                sock.close()
            else:
                try:
                    print >>sys.stderr, 'opening: "%s"' % message
                    script_file=open(filename)
                    message="touch /home/docker/ws/scripts/"+filename
                    sock.sendall(message)
                    time.sleep(3)
                    print >>sys.stderr, 'file on server created'
                    for line in script_file:
                        message="echo '"+line.rstrip('\n')+"' >> /home/docker/ws/scripts/"+filename
                        print >>sys.stderr, 'line: "%s"' % message
                        sock.sendall(message)
                        time.sleep(3)
                    print >>sys.stderr, 'file sent'
                    script_file.close()
                except:
                    print  "something went wrong while opening/sending file"
                    continue
    except KeyboardInterrupt:
        print "?"
