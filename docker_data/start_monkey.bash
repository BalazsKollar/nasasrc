#!/bin/bash
source /opt/ros/indigo/setup.bash
source /home/docker/ws/devel/setup.bash
source /nasasrc/task1/devel/setup.bash

rosrun vision resample_image > /home/docker/ws/logs/monkey_logfile.log &
rosrun vision mi_listener.py >> /home/docker/ws/logs/monkey_logfile.log