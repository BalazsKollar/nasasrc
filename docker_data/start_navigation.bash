#!/bin/bash
source /opt/ros/indigo/setup.bash
source /home/docker/ws/devel/setup.bash

cd /nasasrc/finals

python -m navigation.main > /home/docker/ws/logs/navigation_logfile.log
