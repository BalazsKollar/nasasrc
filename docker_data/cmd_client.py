import socket
import sys
import time
import signal

def catch_ctrl_C(sig,frame):
    print " only if you say prettyprettypleasestop \ncommand: "

if __name__ == '__main__':
    signal.signal(signal.SIGINT, catch_ctrl_C)
    
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    # Connect the socket to the port on the server given by the caller
    #server_address = ('192.168.2.10', 8002)
    port_id = int(raw_input("port id (1/2/3/4/5): "))
    server_address = ('192.168.2.10', 8001+port_id)
    print >>sys.stderr, 'connecting to %s port %s' % server_address
    sock.connect(server_address)
    
    dontstopbelieving=True
    try:
        while dontstopbelieving:
            message = raw_input("command: ")
            if message == "prettyprettypleasestop":
                dontstopbelieving=False
                sock.close()
            else:
                print >>sys.stderr, 'sending: "%s"' % message
                sock.sendall(message)
                time.sleep(1)
                print >>sys.stderr, 'message sent'
    except KeyboardInterrupt:
        print "something"
