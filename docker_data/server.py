import socket
import sys
import subprocess
import time
from datetime import datetime

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the address given on the command line


port=int(str(sys.argv[1]))
#server_address = ('172.17.0.1', port)
server_address = ('192.168.2.10', port)
print >>sys.stderr, 'starting up on %s port %s' % server_address
sock.bind(server_address)
sock.listen(1)

while True:
    print >>sys.stderr, 'waiting for a connection'
    connection, client_address = sock.accept()
    try:
        print >>sys.stderr, 'client connected:', client_address
        logfilename = datetime.now().strftime('/home/docker/ws/logs/server_logfile_%H_%M_%S_%d_%m_%Y.log')
        while True:
            cmd = connection.recv(400) #lenght of string --------------- this might have to change if we want to send longer messages than 200 char (or shorter if it's not efficient)
            #outfile = open(logfilename, "a")
            print >>sys.stderr, 'received "%s"' % cmd
            proc=subprocess.Popen(cmd, shell=True)
            time.sleep(5)
            #outfile.close()
 	    if not cmd:
                break


    finally:
        connection.close()
