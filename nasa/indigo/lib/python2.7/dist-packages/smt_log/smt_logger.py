import logging
import time

from smt_topic_utils import ResourceProxy

from .h5py_logger import HDF5DataLogger

event_logger = logging.getLogger(__name__)

class SmtLoggerException(Exception):
    pass

class SmtLogger(object):

    def __init__(self, filename, topics=None, topicFilters={'name': '.*'}, buffer_size=1, max_num=-1):
        """
        :param filename: Name of the output file
        :param topics: List of the smt topics to log
        :param topicFilters: Dictionary of key-value pairs used as a filter for the ResourceProxy.
        :param buffer_size: Number of samples to buffer for each topic
        :param max_num: Maximum number of topics that will be logged.
        """
        self.resourceProxy = ResourceProxy(topicFilters)

        self.data_logger = HDF5DataLogger(filename, buffer_size=buffer_size)
        self.event_logger = logging.getLogger(__name__)
        self.resources = []
        self.topicNames = []
        self.proxyResources = {}
        self.reloadTopics()
        self.max_num = max_num

        if topics:
            for topic in topics:
                self.addTopic(topic)
        else:
            for topic in sorted(self.proxyResources.keys()):
                self.addTopic(topic)

    def addTopic(self, topic):
        """
        Add a topic to be logged.

        :param topic: name of the topic to be logged
        """
        if len(self.topicNames) > self.max_num and self.max_num > 0:
            logging.warn('max number of topics reached.  Topic:{} is being ignored'.format(topic))
        else:
            if topic not in self.proxyResources.keys():
                raise SmtLoggerException("Trying to log invalid topic:{}".format(topic))
            if topic in self.topicNames:
                raise SmtLoggerException("Trying to add topic that is already added:{}".format(topic))
            self.event_logger.debug('Adding topic[{}]:{}'.format(len(self.resources),topic))

            resource = self.proxyResources[topic]
            callback = lambda:(time.time(),resource())
            self.data_logger.addDataSet(name=topic, callback=callback, width=2)
            self.resources.append(self.proxyResources[topic])
            self.topicNames.append(topic)

    def reloadTopics(self):
        """
        Refresh the topics.  This will clear all topics that have been added to be logged.
        """
        self.resourceProxy.reloadTopics(leaf_only=True)
        self.proxyResources = self.resourceProxy.getResourceDict()
        self.resources = []
        self.topicNames = []

    def log(self):
        """
        Take a sample and log to file.
        """
        return self.data_logger.log()

    def start(self):
        """
        Start the logging process.

        NOTE: This can only be done AFTER all the topics have been added.
        """
        self.data_logger.start()

    def stop(self):
        """
        Stop the logger.
        """
        self.data_logger.stop()
