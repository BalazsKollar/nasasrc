from multiprocessing import Process, Pipe, Queue
from threading import Thread
import time
import signal
import h5py
import os
import logging
from collections import deque
import numpy

from smt_log.consumer_queue import ConsumerQueue, ProducerQueue, ProducerQueueException

logger = logging.getLogger(__name__)

QUEUE_MAX_SIZE = 10

class HDF5DataLogger(object):
    """
    Log data to an HDF5 file
    """
    def __init__(self, filename, buffer_size=1):
        """
        :param filename: Name of the HDF5 file
        :param period: Period between checks of the connection for new data
        :param buffer_size: Number of samples per dataset that will be buffered before sending data to the I/O process
                            This will also dictate the chunk_size.
        """
        self.queue = Queue(maxsize=QUEUE_MAX_SIZE)
        self.filename = filename
        self.buffer_size = buffer_size
        self.handler = HDF5Consumer(filename, self.queue)
        self.buffer = None
        self.callbacks = {}
        self.drop_count = 0

    def addDataSet(self, name, width, callback, dtype='float64'):
        """
        Add a dataset.

        :param name: String name of the dataset
        :param width: Number of items per sample
        :param callback: Method to invoke to generate the sample for the log
        :param dtype: Type of the data.
        :return:
        """
        self.callbacks[name] = callback
        self.handler.addDataSet(name, width, dtype, chunk_size=(self.buffer_size,width))

    def log(self):
        """
        Grab one sample of data from each registered dataset.
        """
        flush_time = 0
        for name, callback in sorted(self.callbacks.items()):
            try:
                flush_time += self.buffer.write(name, callback())
            except ProducerQueueException:
                self.drop_count += 1
                logger.warn('overflowing the buffer, dropping sample')
        return flush_time

    def start(self):
        """
        Start the logger.  This needs to be run before the first call to the "log" method
        """
        self.buffer = ProducerQueue(self.queue, length=self.buffer_size, width=len(self.callbacks))
        self.handler.start()

    def stop(self):
        """
        Stop Logging.
        """
        if self.handler.is_alive():
            t1 = time.time()
            try:
                flush_time = self.buffer.flush()
            except ProducerQueueException:
                pass
            logger.debug('sending stop signal at: {}'.format(t1))
            self.buffer.sendConsumerShutdown()
            t2 = time.time()
            while(self.handler.is_alive()):
                time.sleep(0.50)
                logger.debug('waiting...remaining queue items= {}'.format(self.buffer.getQueueSize()))

            t3 = time.time()
            logger.debug('time to join thread: {} sec'.format(t3-t2))
            logger.debug('Number of dropped buffers={}'.format(self.drop_count))

class HDF5Consumer(ConsumerQueue):
    """
    This is a sub-class of the Consumer Queue.
    """

    def __init__(self, filename, queue):
        """
        :param filename: Name of the HDF5 file
        :param queue: instance of a multiprocessing.Queue
        """
        ConsumerQueue.__init__(self, queue)
        self.filename = filename
        self.logger = logger.getChild('HDF5Consumer')

        self.dataSets = {}
        self.ds_info = []
        self.hdf5LogFile = None
        self.stop_signal_time = 0
        self.num_loops = 0

    def init(self):
        """
        Initialize the process.  This is called right before the ConsumerQueue's "run" loop
        """
        self.hdf5LogFile = h5py.File(self.filename, 'w')

        for info in self.ds_info:
            self.dataSets[info['name']] = self.createDataSet(self.hdf5LogFile, **info)

    def processData(self, process_queue):
        """

        :param process_queue: List of items that came through the queue.  For this class,
                              every item in the queue is a dictionary for datasetname-list of values.
        """
        for data in process_queue:
            for name, values in data.items():
                self.dataSets[name].write(values)

    def shutdown(self):
        self.logger.debug('starting shutdown')
        t2 = time.time()
        self.logger.debug('time to exit loop:  {}'.format(t2 - self.stop_signal_time))
        self.logger.debug('closing file...')
        self.hdf5LogFile.close()
        t3 = time.time()
        logger.debug('hdf5 file closed')
        self.logger.debug('time to close   : {}'.format(t3-t2))

    def addDataSet(self, name, width, dtype='float64', chunk_size=True):
        """
        Add a dataset to the file

        NOTE: Due to how h5py handles multiprocessing, it is important
        to create the HDF5 file for writing AFTER the thread is created.
        For this reason, 'addDataSet' simply records what the data sets will look
        like, and are actually created when the process is started.

        :param name: Name of the dataset
        :param width: Width of the data.
        :param dtype: Type of the data to be stored
        :param chunk_size: Size of the chunk
        """
        self.ds_info.append({'name':name, 'width':width, 'dtype':dtype, 'chunks':chunk_size})

    def createDataSet(self, hdf5LogFile, name, width, dtype='float64', chunks=True):
        """
        Create a DatasetProxy for the given hdf5LogFile

        :param hdf5LogFile:
        :param name: name of the data set
        :param width: width of the data set
        :param dtype: type of the data set
        :param chunks: Chunk size as a tuple.  To have HDF5 auto-select, set to 'True'
        :return: HDF5DatasetProxy object
        """
        ds = hdf5LogFile.create_dataset(name, shape=(0, width), dtype=dtype, maxshape=(None, width), chunks=chunks)
        return HDF5DatasetProxy(ds)

class HDF5DatasetProxy(object):
    """
    This class is a wrapper for an HDF5 dataset.  When writing to the dataset, it will automatically grow to the proper size
    """

    def __init__(self, dataset):
        """
        :param dataset: h5py.dataset object
        """
        self.dataset = dataset
        self.index = 0

    def write(self, data):
        """
        Write data to the dataset

        :param data: list of python objects to write to the dataset
        """
        last_idx = self.index + len(data)
        self.dataset.resize(last_idx, 0)

        self.dataset[self.index:last_idx] = data
        self.index += len(data)

