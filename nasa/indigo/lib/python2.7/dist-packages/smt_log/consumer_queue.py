from multiprocessing import Process, Queue
from Queue import Full
import signal
import time

class ProducerQueueException(Exception):
    pass

class ProducerQueue(object):
    """
    The ProducerQueue will collect data until it is full, and then pass the data through it's connection.

    The ProducerQueue stores a dictionary of named lists (buffers),  Each call to 'write' will append
    a new value to the named list.  If that particular buffer has reached the 'size' argument set in the constructor,
    that list of named data will get passed through the connection, probably to another Process of Thread.
    """

    def __init__(self, q, length, width):
        """
        :param connection: Either a QueueConnection or a PipeConnection within this module
        :param size: Size of the buffer
        :return:
        """
        self.q = q
        self.length = length
        self.width = width
        self.buffers = {}
        self.idx = {}
        self.full_count = 0

    def write(self, name, data):
        """
        Write a piece of data to the buffer
        :param: name: Name of the data
        :param data: Value for the data
        """
        try:
            idx = self.idx[name]
        except KeyError:
            self.buffers[name] = self.length * [None]
            self.idx[name] = 0

        try:
            self.buffers[name][self.idx[name]] = data
        except IndexError:
            raise Exception('name={}, idx={}, full_count={}, width={}, length={}'.format(name,self.idx[name], self.full_count, self.width,self.length))
        self.idx[name] += 1
        if self.idx[name] >= (self.length):
            self.full_count += 1

        if self.full_count >= self.width:
            return self.flush()

        return 0

    def sendConsumerShutdown(self, timeout=0):
        t = time.time()

        while (time.time() - t) < timeout:
            if self.q.full():
                time.sleep(0.1)
            else:
                break
        self.q.put(ConsumerQueue.StopProcess)

    def flush(self):
        """
        Send the dictionary buffer through the pipe

        :param name:  Name of the data
        """
        t1 = time.time()
        data = {}
        for name, value in self.buffers.items():
            data[name] = value[0:self.idx[name]]
            self.idx[name] = 0
        self.full_count = 0

        try:
            self.q.put_nowait(data)
        except Full:
            raise ProducerQueueException("Flush failed.  Queue is full")

        t2 = time.time()
        return t2-t1

    def getNames(self):
        """
        Get the name of the data buffers

        :return: List of strings
        """
        return self.buffers.keys()

    def getQueueSize(self):
        return self.q.qsize()


class ConsumerQueue(Process):
    """
    The ConsumerQueue is a consumer process in a Producer-Consumer pattern.  This class is meant to be subclassed,
      and then implement the following methods:
      init()        - called after the process has started, but before the "run" loop
      shutdown()    - called when the process is commanded to shutdown
      processData() - called whenever new data comes in through the Queue.

    This class (or subclass) can be used with the ProducerQueue.  Simply instantiate a multiprocessing,Queue object,
    and pass it to the constructor of the ProducerQueue and ConsumerQueue.  To stop the ConsumerQueue, send
    'ConsumerQueue.StopProcess' object through the queue.  If using the ProducerQueue, simply call
    ProducerQueue.sendConsumerShutdown.
    """
    StopProcess = None

    def __init__(self, q):
        """
        :param q: a multiprocessing.Queue object
        """
        Process.__init__(self)
        self.q = q
        self.running = False

    def _handler(self, sig, frame):
        if sig == signal.SIGINT:
            pass

        elif sig == signal.SIGTERM:
            self.running = False

    def run(self):
        self.init()
        signal.signal(signal.SIGINT,self._handler)
        signal.signal(signal.SIGTERM, self._handler)
        self.running = True
        while self.running:
            process_queue = []
            while(not self.q.empty()):
                data = self.q.get()
                if data == ConsumerQueue.StopProcess:
                    self.running = False
                else:
                    process_queue.append(data)
            self.processData(process_queue)
        self.shutdown()

    def init(self):
        """
        Overload this method to run code after the process is started, but before it starts it's loop
        """
        pass

    def shutdown(self):
        """
        Overload this method to run code after this class has received it's StopProcess object.
        """
        pass

    def processData(self, data_queue):
        """
        Overload this method to process data that comes through the queue.

        :param data_queue: List of items from the process queue
        """
        pass