#!/usr/bin/python
from lxml import etree
import sys

#<unittest test=" " type=" " resource_name=" " test_value=" ">
#
#ex:
#	<unittest test="TestNoArg" type="uint32_t" resource_name="/left_leg/joint0/CtrlReg1" test_value="0xA000"/>
#
#'EXPECT_EQ(TestNoArg<{}>({},{},{},true)'.format(type, apicall, resource_name, expected_value)
#'EXPECT_NEAR(TestSetWithArg<{}>({},{},{}),{},{})'.format(type, apicall, resource_name, setVal, setVal, tol)
#'EXPECT_NEAR(TestGetWithArg<{}>({},{},{}),{},{}'.format(type, apicall, resource_name, setVal, setVal, tol)
#
#
#testDict = dict()
#testDict['TestNoArg'] = 'EXPECT_EQ(TestNoArg<{}>({},{},{},true)'
#testDict['TestSetWithArg'] = 'EXPECT_EQ(TestNoArg<{}>({},{},{},true)'
#testDict['TestGetWithArg'] = 'EXPECT_EQ(TestNoArg<{}>({},{},{},true)'

def GenerateCppHeader(apiName,cpp_namespaces_names,cpp_namespaces_using):
	header = []
	header.append('#include <gtest/gtest.h>')
	header.append('#include <nasa_ros_shm/resource_shm.hpp>')
	header.append('#include <{}.hpp>'.format(apiName))
	header.append('')
	header.append('using namespace SHM;')
	if cpp_namespaces_using != None:
		for using in cpp_namespaces_using:
			header.append('using namespace {};'.format(cpp_namespaces_using))
	if cpp_namespaces_names != None:
		for namespace in cpp_namespaces_names:
			header.append('namespace {} {{'.format(namespace))
	header.append('')
	header.append('')
	return header

def GenerateTestFixtureClass(apiName,fixtureName,nodePath):
	fixture = []
	fixture.append('class {} : public testing::Test'.format(fixtureName))
	fixture.append('{')
	fixture.append('public:')
	fixture.append('\t' + fixtureName + '() {')
	fixture.append('\t\ttry {')
	fixture.append('\t\t\ttestApi.initialize("{}");'.format(nodePath))
	fixture.append('\t\t}')
	fixture.append('\t\tcatch(SHM::SHMException e) {')
	fixture.append('\t\t\tstd::cout << "Exception: {}:" << e.what() << std::endl;'.format(fixtureName))
	fixture.append('\t\t}')
	fixture.append('\t}')
	fixture.append('protected:')
	fixture.append('\t{} testApi;'.format(apiName))
	fixture.append('};\n')
	return fixture


def GenerateTestFixtureBlock(fixtureName,blockName,testLines):
	block = []
	block.append('TEST_F({},{})'.format(fixtureName,blockName))
	block.append('{')
	for line in testLines:
		block.append('\t' + line)
	block.append('}')
	return block

def WriteTestFile(fileName,lines):
	fileDir = os.path.dirname(fileName)
	print fileDir
	if not os.path.exists(fileDir):
		os.makedirs(fileDir)

	with open(fileName,'w') as fd:
		for line in lines:
			fd.write(line + '\n')
	fd.close

def GenerateTestNoArg(apiCall, templateType, resource_name, test_value):
	test = []
	test.append('SetResource<{}>("{}",0);'.format(templateType,resource_name))
	test.append('testApi.{}();'.format(apiCall))
	test.append('EXPECT_EQ(CheckResource<{}>("{}",{}),true);'.format(templateType,resource_name,test_value))
	test.append('')
	return test

def GenerateTestSetWithArg(apiCall, templateType, resource_name, test_value, tol):
	test = []
	test.append('{} setVal = ({}){};'.format(templateType,templateType,test_value))
	test.append('testApi.{}(setVal);'.format(apiCall))
	test.append('EXPECT_NEAR(GetResource<{}>("{}"),{},{});'.format(templateType,resource_name,test_value,tol))
	test.append('')
	return test

def GenerateTestGetWithArg(apiCall, templateType, resource_name, test_value, tol):
	test = []
	test.append('{} container;'.format(templateType))
	test.append('SetResource<{}>("{}",{});'.format(templateType,resource_name,test_value))
	test.append('testApi.{}(container);'.format(apiCall))
	test.append('EXPECT_NEAR(container,{},{});'.format(test_value,tol))
	test.append('')
	return test

def GenerateMain():
	lines = []
	lines.append('// Run all the tests that were declared with TEST()')
	lines.append('int main(int argc, char **argv){')
	lines.append('\ttesting::InitGoogleTest(&argc, argv);')
	lines.append('\treturn RUN_ALL_TESTS();')
	lines.append('}')
	lines
	return lines

def GenerateFooter(cpp_namespaces_names):
	lines = []
	if cpp_namespaces_names is not None:
		for namespace in cpp_namespaces_names:
			lines.append('}')
	return lines

def BuildUnittests(apiFile,outputFile=None):
	api = etree.parse(apiFile).getroot()
	
	classTag = api.find('class')
	className = classTag.get('name')

	# Namespaces:
	cpp_namespaces = classTag.find('cpp_namespace',None)
	cpp_namespaces_names = None
	cpp_namespaces_using = None
	if cpp_namespaces !=None:
		cpp_namespaces_using = cpp_namespaces.get('using',None)
		if cpp_namespaces_using != None:
			cpp_namespaces_using = cpp_namespaces_using.split(',')
		cpp_namespaces_names = cpp_namespaces.get('names',None)
		if cpp_namespaces_names != None:
			cpp_namespaces_names= cpp_namespaces_names.split(',')

	fixtureName = className + 'Test'
	functions = api.find('functions')
	testBlock = []
	for function in functions:
		
		apiCall = function.get('name')
		unittests = function.findall('unittest')
		if unittests:
			for unittest,i in zip(unittests,range(len(unittests))):
				testlines = []
				test = unittest.get('test')
				templateType = unittest.get('type')
				resource_name = unittest.get('resource_name')
				nodePath = resource_name.rsplit('/',1)[0]
				test_value = unittest.get('test_value')
				tol = unittest.get('tol')
				if test == 'TestNoArg':
					testlines = testlines + GenerateTestNoArg(apiCall,templateType,resource_name,test_value)
				elif test == 'TestSetWithArg':
					testlines = testlines + GenerateTestSetWithArg(apiCall, templateType, resource_name, test_value, tol)
				elif test == 'TestGetWithArg':
					testlines = testlines + GenerateTestGetWithArg(apiCall, templateType, resource_name, test_value, tol)
		
				testFunctionBlock = GenerateTestFixtureBlock(fixtureName,apiCall + 'Test' + str(i+1),testlines)
				testBlock = testBlock + testFunctionBlock
	
	if len(testBlock) == 0:
		print 'No unit tests generated for:{}'.format(className)
		print 'Exiting...'
		sys.exit()
	header = GenerateCppHeader(className,cpp_namespaces_names,cpp_namespaces_using)
	fixture = GenerateTestFixtureClass(className,fixtureName,nodePath)
	footer = GenerateFooter(cpp_namespaces_names)
	mainBlock = GenerateMain()
	testFile = header + fixture + testBlock + footer + mainBlock
	# for line in testFile:
	# 	print line

	print args
	if outputFile != None:
		WriteTestFile(args.outputFile[0],testFile)

if __name__ == "__main__":

	parser = argparse.ArgumentParser(description='From an apiFile, generate a set of gtests for C++')
	parser.add_argument('apiFile',nargs=1,help='input api file')
	parser.add_argument('-o','--output',nargs=1,dest='outputFile',help='output file')

	args,unknown = parser.parse_known_args(sys.argv[1:])
	apiFile = args.apiFile[0]

	

