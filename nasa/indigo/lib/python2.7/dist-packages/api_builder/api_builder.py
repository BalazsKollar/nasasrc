import sys
import os
from lxml import etree
import string
import logging

logger = logging.getLogger(__name__)

class Function(object):
	'''Container class for an API function.
	'''
	def __init__(self,className,name,args,commandList):
		''' Constructor
		@param className Name of the class for the API.  This will be in the api's class name attribute.
		@param name The name of the function
		@param args The arguments to the function
		@param commandList A list of the commands to be executed within this function
		'''
		self.rtype = 'void'
		self.className  = className
		self.name = name
		self.args = args
		self.commandList = commandList
		self.subscriberList = []
		for command in commandList:
			if command.subscriber not in self.subscriberList:
				self.subscriberList.append(command)
		self.cppArgString = self.GenerateCppArgString(args)
		self.pyArgString = self.GeneratePyArgString(args)
		self.hppDeclString = '{} {}({});'.format(self.rtype, self.name, self.cppArgString)
		self.cppDeclString = '{} {}::{}({}) '.format(self.rtype, self.className,self.name,self.cppArgString) + ' {'
		self.pyDeclString = 'def {}(self{}):'.format(self.name,self.pyArgString)
		self.cppLines = []
		self.cppLines.append(self.cppDeclString)
		self.pyLines = []
		self.pyLines.append(self.pyDeclString)
		self.pyLines.append('\ttry:')
		for command in commandList:
			self.cppLines.append('\t' + command.cppCode)
			self.pyLines.append('\t\t' + command.pyCode)
		self.cppLines.append('}')
		self.pyLines.append('\texcept AttributeError:')
		self.pyLines.append('\t\treturn')
		self.pyLines.append('\treturn {}'.format(self.pyArgString.strip(',')))

	def GenerateCppArgString(self,args):
		'''Generates the argument string for C++
		@param args The list of arguments
		@returns The C++ argument string used in the function declaration
		'''
		argString = ''
		for arg in args:
			if argString != '':
				argString = argString + ', '
			argString = argString + '{} &{}'.format(arg[1],arg[0])
		return argString

	def GeneratePyArgString(self,args):
		'''Generates the argument string for python
		@param args The list of arguments
		@returns The python argument string used in the function declaration
		'''
		argString = ''
		for arg in args:
			argString = argString + ', {}'.format(arg[0])
		return argString

	def Print(self):
		print 'className     : {}'.format(self.className)
		print 'name          : {}'.format(self.name)
		print 'args          : {}'.format(self.args)
		print 'commandList   :'
		for command in self.commandList:
			command.Print()
		print 'cppArgString  : {}'.format(self.cppArgString)
		print 'hppDeclString : {}'.format(self.hppDeclString)
		print 'cppDeclString : {}'.format(self.cppDeclString)
		print 'cppLines :'
		for line in self.cppLines:
			print line

class Command(object):
	'''Each function of the generated API is composed of commands.  Each command defines the get
	and set operations within the function.
	'''

	def __init__(self,op,offset,value,cppType,field=''):
		'''Constructor
		@param op The command operation.  This will be either "get" or "set"
		@param offset The offset of this piece of data within a node's memory space
		@param value The value for the operation.  This can be a variable name or a value.
		@param cppType The type for the assignment value
		@param field The register field.  If the offset is a register, this will not be blank.
		'''
		self.op = op
		self.offset = offset
		self.value=value
		self.field=field
		self.subscriber = Subscriber(offset,field)
		self.name    = '{}{}{}Cmd'.format(self.op,self.offset,self.field)
		#self.pyCode  = 'self.{}({});'.format(self.name,self.value)
		self.cppType = cppType
		self.pySubscribeString = 'self.{} = Resource(nodePath + \'{}\')'.format(self.subscriber.name,self.offset)
	def Print(self):
		print 'op          : {}'.format(self.op)
		print 'offset      : {}'.format(self.offset )
		print 'value       : {}'.format(self.value)
		print 'field       : {}'.format(self.field)
		print 'subscriber-'
		self.subscriber.Print()
		print 'name        : {}'.format(self.name)
		print 'cppCode     : {}'.format(self.cppCode)
		print 'pyCode      : {}'.format(self.pyCode)
		print 'cppType     : {}'.format(self.cppType)
		print 'hppDeclString : {}'.format(self.hppDeclString)
		print 'cppSubscribeString : {}'.format(self.cppSubscribeString)
		print 'pySubscribeString : {}'.format(self.pySubscribeString)

class Subscriber(object):
	def __init__(self,offset,field):
		self.name = 'subscriber{}'.format(offset)
		self.offset = offset
		if field == '':
			self.subtype = 'data'
		else:
			self.subtype = 'register'
	def Print(self):
		print 'name    : {}'.format(self.name)
		print 'offset  : {}'.format(self.offset)
		print 'subtype : {}'.format(self.subtype)

class SetRegisterCommand(Command):
	def __init__(self,op,offset,value,cppType,field):
		super(SetRegisterCommand,self).__init__(op,offset,value,cppType,field)
		self.cppSubscribeString = '{} = {}->getRegister()->loadSetter<{}>("{}");'.format(self.name,self.subscriber.name,cppType,field)
		self.hppDeclString = 'std::function<void({})> {}'.format(self.cppType,self.name)
		self.cppCode = '{}({});'.format(self.name,self.value)
		self.pyCode  = 'self.{}({});'.format(self.name,self.value)

class GetRegisterCommand(Command):
	def __init__(self,op,offset,value,cppType,field):
		super(GetRegisterCommand,self).__init__(op,offset,value,cppType,field)
		self.cppSubscribeString = '{} = {}->getRegister()->loadGetter<{}>("{}");'.format(self.name,self.subscriber.name,cppType,field)
		self.hppDeclString = 'std::function<{}()> {}'.format(self.cppType,self.name)
		self.cppCode = '{} = {}();'.format(self.value, self.name)
		self.pyCode  = '{} = self.{}();'.format(self.value,self.name)

class SetDataCommand(Command):
	def __init__(self,op,offset,value,cppType,field):
		super(SetDataCommand,self).__init__(op,offset,value,cppType,field)
		#self.cppSubscribeString = '{} = {}->setter<{}>(SHM::WRITE);'.format(self.name,self.subscriber.name,self.cppType)
		#self.hppDeclString = 'std::function<void({})> {}'.format(self.cppType,self.name)
		self.cppCode = '{}->{}(({}){});'.format(self.subscriber.name,self.op,cppType,self.value)
		self.cppSubscribeString=None
		self.hppDeclString=None
		self.pyCode  = 'self.{}({});'.format(self.name,self.value)

class GetDataCommand(Command):
	def __init__(self,op,offset,value,cppType,field):
		super(GetDataCommand,self).__init__(op,offset,value,cppType,field)
		#self.cppSubscribeString = '{} = {}->getter<{}>(SHM::READ);'.format(self.name,self.subscriber.name,self.cppType)
		#self.hppDeclString = 'std::function<void({} &)> {}'.format(self.cppType,self.name)
		self.cppCode = '{value} = {subscriber}->{op}<{type}>();'.format(value=self.value,subscriber=self.subscriber.name,op=self.op,type=cppType)
		self.cppSubscribeString=None
		self.hppDeclString=None
		self.pyCode  = '{} = self.{}();'.format(self.value,self.name)

class ApiBuilder(object):
	def __init__(self):
		self.functionDict   = dict()
		self.commandDict    = dict()
		self.subscriberDict = dict()
		self.cppLines     = []
		self.hppLines     = []
		self.pyLines      = []
		self.offsets      = []
		self.defaultCppType = 'uint32_t'
		self.headerPathRelative = None

		self.logger = logger.getChild('ApiBuilder')

	#Generate the data and write them to file
	def Build(self,apiFileName,cppFilePath,hppFilePath,headerPathRelative,pyFilePath, schemaFile=None):
		self.headerPathRelative = headerPathRelative
		self.Generate(apiFileName, schemaFile)
		
		self.logger.debug('apiFileName:{}'.format(apiFileName))
		self.logger.debug('cppFilePath:{}'.format(cppFilePath))
		self.logger.debug('hppFilePath:{}'.format(hppFilePath))
		self.logger.debug('headerPathRelative:{}'.format(headerPathRelative))
		self.logger.debug('pyFilePath:{}'.format(pyFilePath))

		# If the directories don't exists, then create the directories
		for filePath in cppFilePath,hppFilePath,pyFilePath:
			fileDir = os.path.dirname(filePath)
			if not os.path.exists(fileDir):
				os.makedirs(fileDir)
		self.WriteCppFile(cppFilePath)
		self.WriteHppFile(hppFilePath)
		self.WritePyFile(pyFilePath)

	#Generate all the API's
	def Generate(self,apiFileName,schemaFile):
		self.apiFileName = apiFileName
		self.parseXml(apiFileName,schemaFile)
		self.GenerateCpp()
		self.GenerateHpp()
		self.GeneratePy()

	# Parse the XML file and populate the dictionaries
	def parseXml(self,apiFileName,schemaFile=None):
		parser = etree.XMLParser(remove_blank_text=True,remove_comments=True)
		tree = etree.parse(apiFileName,parser)

		if schemaFile == None:
			import inspect
			schemaFile = os.path.join(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))), 'xml', 'api_builder.xsd')

		xmlschema = etree.XMLSchema(etree.parse(schemaFile))
		try:
			xmlschema.assertValid(tree)
		except etree.DocumentInvalid as e:
			print e
			sys.exit()

		# Find the class name and the base for the functions
		classTag = tree.find('class')
		self.className = classTag.get('name')

		#################
		# Parse cpp info:
		#################

		# Base Classes"
		self.cpp_baseClasses = classTag.find('cpp_base',None)
		if self.cpp_baseClasses != None:
			self.cpp_baseClasses = self.cpp_baseClasses.get('names').split(',')
		
		# #includes:
		self.cpp_includes = classTag.find('cpp_includes',None)
		if self.cpp_includes != None:
			self.cpp_includes = self.cpp_includes.get('names').split(',')

		# Namespaces:
		self.cpp_namespaces = classTag.find('cpp_namespace',None)
		self.cpp_namespaces_names = None
		self.cpp_namespaces_using = None
		if self.cpp_namespaces !=None:
			self.cpp_namespaces_using = self.cpp_namespaces.get('using',None)
			if self.cpp_namespaces_using != None:
				self.cpp_namespaces_using = self.cpp_namespaces_using.split(',')
			self.cpp_namespaces_names = self.cpp_namespaces.get('names',None)
			if self.cpp_namespaces_names != None:
				self.cpp_namespaces_names= self.cpp_namespaces_names.split(',')

		####################
		# Parse python info:
		####################

		# ros packages
		self.ros_package_imports = classTag.find('ros_package_import',None)
		if self.ros_package_imports != None:
			self.ros_package_imports = self.ros_package_imports.get('names').split(',')

		# python base classes
		self.py_baseClasses = classTag.find('py_base',None)
		if self.py_baseClasses != None:
			self.py_baseClasses = self.py_baseClasses.get('names').split(',')

		# python imports:
		self.py_includes = classTag.find('py_includes',None)
		if self.py_includes != None:
			self.py_includes = self.py_includes.get('names').split(',')

		functions = tree.find('functions')
		for function in functions:
			functionName = function.get('name')
			args = []
			argDict = dict()
			for arg in function.iter('arg'):
				argDict[arg.get('name')] = arg.get('type')
				args.append([arg.get('name'),arg.get('type')])
			commandList = []   # list of all the function calls for an API call
			for cmd in function.iter('command'):
				op = cmd.get('op')
				offset = cmd.get('offset')
				value  = cmd.get('value')
				field = cmd.get('field')
				valueType = cmd.get('valueType')
				if field == None:
					field = ''
					subscriberType = 'data'
				else:
					subscriberType = 'register'
				if value not in argDict:
					if valueType != None:
						cppType = valueType
					else:
						cppType = self.defaultCppType
				else:
					cppType = argDict[value]
				if subscriberType == 'data':
					if op == 'get':
						command = GetDataCommand(op,offset,value,cppType,field)
					elif op == 'set':
						command = SetDataCommand(op,offset,value,cppType,field)
				elif subscriberType == 'register':
					if op == 'get':
						command = GetRegisterCommand(op,offset,value,cppType,field)
					elif op == 'set':
						command = SetRegisterCommand(op,offset,value,cppType,field)

				commandList.append(command)
				# Add them to the dictionary/list, if they don't exist
				self.commandDict[command.name] = command
				self.subscriberDict[command.subscriber.name] = command.subscriber
			self.functionDict[functionName] = Function(self.className,functionName,args,commandList)
	
	# Populates cppLines with the C++ API source file.  Returns cppLines
	def GenerateCpp(self):
		lines = []
		className = self.className

		#Header:
		lines.append(str('#include "{}"'.format(self.headerPathRelative)))
	
		#Namespaces:
		lines.append('\n//Namespaces:')
		if self.cpp_namespaces != None:
			if self.cpp_namespaces != None:
				if self.cpp_namespaces_using != None:
					for name in self.cpp_namespaces_using:
						lines.append('using namespace ' + name +';')
				if self.cpp_namespaces_names != None:
					for name in self.cpp_namespaces_names:
						lines.append('namespace ' + name + '{')


		#Constructors:
		lines.append(str('{}::{}() '.format(className,className)) + '{')
		lines.append('}')

		lines.append(str('{}::{}(const std::string &nodePath, SMT::SMTClient* smtClient) '.format(className,className)) + '{')
		lines.append(str('\tSubscribe(nodePath, smtClient);'))
		lines.append('}')

		#Destructor:
		lines.append(str('{}::~{}() '.format(className,className)) + '{')
		lines.append('}')
		
		lines.append(str('std::string {}::getNodePath() const ').format(className) + '{')
		lines.append('\treturn nodePath;')
		lines.append('}')

		#Subscribe function
		lines.append(str('void {}::Subscribe(const std::string &nodePath, SMT::SMTClient* smtClient) ').format(className) + '{')
		lines.append('\tclient = smtClient;')
		lines.append('\tthis->nodePath = nodePath;')
		lines.append('\tint errorcount = 0;')
		lines.append('\tstd::vector<std::string> errorstrings;')
		#assign subscribers
		for subscriber in sorted(self.subscriberDict.values(),key=lambda m: m.name):
			lines.append(str('\ttry{'))
			if subscriber.subtype == 'register':
				lines.append(str('\t\t{}.reset(new SMT::SMT_utils::Register(client->getResource(nodePath + "/{}")));'.format(subscriber.name,subscriber.offset)))
			else:
				lines.append(str('\t\t{}.reset(new SMT::Resource(client->getResource(nodePath + "/{}")));'.format(subscriber.name,subscriber.offset)))
			lines.append(str('\t}} catch (SMT::SMTException e) {{ errorcount++; errorstrings.push_back("exception subscribing to {} : " + std::string(e.what()));}}'.format(subscriber.offset)))
		lines.append(str('\tif(errorcount > 0){'))
		lines.append(str('\t\tfor(unsigned int i= 0; i < errorstrings.size(); i++){'))
		lines.append(str('\t\t\tstd::cerr << errorstrings[i] << std::endl;'))
		lines.append(str('\t\t}'))
		lines.append(str('\t\tthrow SMT::SMTException("missing subscriber data for node:" + nodePath);;'))
		lines.append(str('\t}'))
		#assign commands
		for command in sorted(self.commandDict.values(),key=lambda m: m.name):
			if command.cppSubscribeString != None:
				lines.append('\t{}'.format(command.cppSubscribeString))
		lines.append('}')

		for function in sorted(self.functionDict.values(),key=lambda m: m.name):
			for line in function.cppLines:
				lines.append(line)

		lines.append('//Close namespaces')
		if self.cpp_namespaces_names != None:
			for name in self.cpp_namespaces_names:
				lines.append('}')
		self.cppLines = lines
		return lines

	# Populates hppLines with the C++ API header file.  Returns hppLines
	def GenerateHpp(self):
		lines = []
		className = self.className
		lines.append('#ifndef __' + className + '__HPP__')
		lines.append('#define __' + className + '__HPP__')
		lines.append('')
		# Includes:
		lines.append('// Includes:')
		lines.append('#include "register.hpp"')
		lines.append('#include <shared_memory_transport/smtclient.hpp>')
		if self.cpp_includes != None:
			for include in self.cpp_includes:
				lines.append('#include {}'.format(include))
		
		lines.append('\n//Namespaces:')
		if self.cpp_namespaces != None:
			if self.cpp_namespaces != None:
				if self.cpp_namespaces_using != None:
					for name in self.cpp_namespaces_using:
						lines.append('using namespace ' + name +';')
				if self.cpp_namespaces_names != None:
					for name in self.cpp_namespaces_names:
						lines.append('namespace ' + name + '{')

		lines.append('')
		# Class definition:
		classString = str('class ' + className)
		if self.cpp_baseClasses != None:
			classString = classString + ':'
			baseClasses = []
			for baseClass in self.cpp_baseClasses:
				baseClasses.append(' public ' + baseClass)
			classString = classString + string.join(baseClasses,',')

		lines.append(classString)
		lines.append('{')
	
		lines.append(str('public:'))
		lines.append('//Contructors')
		lines.append(str('\t{}();'.format(className)))
		lines.append(str('\t{}(const std::string &nodePath, SMT::SMTClient* smtClient);'.format(className)))
		lines.append('//Destructor')
		lines.append(str('\t~{}();'.format(className)))
		lines.append('//Subscribe function')
		lines.append(str('\tvoid Subscribe(const std::string &nodePath, SMT::SMTClient* smtClient);'))
		lines.append('//API Functions')
		lines.append('\tstd::string getNodePath() const;')
		for function in sorted(self.functionDict.values(),key=lambda m: m.name):
			lines.append('\t{}'.format(function.hppDeclString))
	
		lines.append('private:')
		lines.append('\tstd::string nodePath;')
		lines.append('\tSMT::SMTClient* client;')
		lines.append('//Commands in the command dictionary')
		for command in sorted(self.commandDict.values(),key=lambda m: m.name):
			if command.hppDeclString != None:
				lines.append('\t{};'.format(command.hppDeclString))
		
		lines.append('//Subscriber declarations')
		lines.append('protected:')
		for subscriber in sorted(self.subscriberDict.values(),key=lambda m: m.name):
			if subscriber.subtype == 'register':
				lines.append('\tstd::shared_ptr<SMT::SMT_utils::Register> {};'.format(subscriber.name))
			else:
				lines.append('\tstd::shared_ptr<SMT::Resource> {};'.format(subscriber.name))
		lines.append('};')

		lines.append('//Close namespaces')
		if self.cpp_namespaces_names != None:
			for name in self.cpp_namespaces_names:
				lines.append('}')

		lines.append('#endif // __' + className + '__HPP__')
		self.hppLines = lines
		return lines

	# Populates pyLines with the Python file.  Returns pyLines
	def GeneratePy(self):
		lines = []
		className = self.className
		lines.append('import pysmt')
		lines.append('import register_builder')
		if self.ros_package_imports != None:
			for package in self.ros_package_imports:
				lines.append('roslib.load_manifest(\'{}\')'.format(package))
		if self.py_includes != None:
			for include in self.py_includes:
				lines.append('import {}'.format(include))
		classString = 'class {}('.format(className)

		if self.py_baseClasses != None:
			classString = classString + string.join(self.py_baseClasses,',') + '):'
		else:
			classString = classString + 'object):'
		lines.append(classString)
		lines.append('')
		#subscribers
		# for subscriber in self.subscriberDict.values():
		# 	lines.append('\t{}'.format(subscriber.name))
		# lines.append('')
		# #commands
		# for command in self.commandDict.values():
		# 	lines.append('\t{}'.format(command.name))
		lines.append('')
		lines.append('\tdef __init__(self,nodePath=\'\',hostUri=\'http://localhost:11312\'):')
		if self.py_baseClasses != None:
			lines.append('\t\tsuper({},self).__init__()'.format(self.className))
		lines.append('\t\tself.client = pysmt.SMTClientFactory(hostUri)')
		lines.append('\t\tself.client.abstractFactories[\'register\'] = register_builder.RegisterFactory')
		lines.append('\t\tif nodePath != \'\':')
		lines.append('\t\t\tself.nodePath = nodePath')
		lines.append('\t\t\tself.Subscribe(nodePath)')
		lines.append('')
		#Subscriber
		lines.append('\tdef Subscribe(self,nodePath):')
		lines.append('\t\tself.nodePath = nodePath')
		lines.append('\t\terrorstrings = []')
		for subscriber in sorted(self.subscriberDict.values(),key=lambda m: m.name):
			lines.append('\t\ttry:')
			lines.append('\t\t\tself.{} = self.client.getResource(nodePath + \'/{}\')'.format(subscriber.name,subscriber.offset))
			for command in sorted(self.commandDict.values(),key=lambda m: m.name):
				# Assignments in try / except block
				if command.subscriber.name == subscriber.name:
					if command.subscriber.subtype == 'register':
						lines.append('\t\t\tself.{} = self.{}.{}'.format(command.name,command.subscriber.name,command.field))
					else:
						lines.append('\t\t\tself.{} = self.{}'.format(command.name,command.subscriber.name))
			lines.append('\t\texcept Exception as e:')
			lines.append('\t\t\terrorstrings.append(\'exception subscribing to {}: "{{}}"\'.format(e))'.format(subscriber.offset))
		lines.append('\t\tif len(errorstrings) > 0:')
		lines.append('\t\t\tfor error in errorstrings:')
		lines.append('\t\t\t\tprint error')
		#lines.append('\t\t\traise Exception(\'missing subscriber data for node: {}\'.format(nodePath))')
		lines.append('\t\t\tprint \'missing subscriber data for node: {}\'.format(nodePath)')
		#assignments
		#for command in sorted(self.commandDict.values(),key=lambda m: m.name):
		#	if command.subscriber.subtype == 'register':
		#		lines.append('\t\tself.{} = self.{}.{}'.format(command.name,command.subscriber.name,command.field))
		#	else:
		#		lines.append('\t\tself.{} = self.{}'.format(command.name,command.subscriber.name))
		lines.append('')
		# Get node path
		lines.append('\tdef getNodePath(self):')
		lines.append('\t\treturn self.nodePath')
		#API
		for function in sorted(self.functionDict.values(),key=lambda m: m.name):
			for line in function.pyLines:
				lines.append('\t{}'.format(line))
			lines.append('')
	
		lines.append('\tdef getApi(self):')
		lines.append('\t\tapi=[]')
		for function in sorted(self.functionDict.values(),key=lambda m: m.name):
			lines.append('\t\tapi.append(self.{})'.format(function.name))
		lines.append('\t\treturn api')
		self.pyLines = lines
		return lines

	# Generates the setup.py file for the python package
	def GenerateSetup(self, package_name, package_path):
		packages = []
		for i in os.listdir(package_path):
			print i
			if os.path.isdir(os.path.join(package_path,i)):
				packages.append(i)

		setupfile = '''
from setuptools import setup
import subprocess
from datetime import datetime
import getpass
import socket

release = 'alpha'
timestamp = datetime.now().strftime('%Y.%m.%d.%H%M.%S')
tag_info = subprocess.check_output(['git', 'describe', '--tags', '--long']).strip()
version = '{}-{}-{}'.format(tag_info,release,timestamp)

user = getpass.getuser()
machine = socket.gethostname()
branch = subprocess.check_output('git rev-parse --abbrev-ref HEAD'.split()).strip()
gitsha = subprocess.check_output('git rev-parse HEAD'.split()).strip()

build_info = '{}@{} - {}:{}'.format(user,machine,branch,gitsha)
source_url = subprocess.check_output('git config --get remote.origin.url'.split()).strip()

description = 'api_builder generated api'
long_description = """{}.
  This package is a generated api using the API_builder package.
  Build_info: {}.
  SourceUrl: {}.
  
""".format(description,build_info,source_url)

'''
		setupfile = setupfile + "setup(name='{}', packages={},version=version,description=description,long_description=long_description,)\n".format(package_name, packages)
		return setupfile

	def GenerateStdebCfg(self,package_name):
		stdebCfg = []
		stdebCfg.append("[DEFAULT]")
		stdebCfg.append("Package: {}".format(package_name.lower().replace('_','-')))
		stdebCfg.append("Depends: nasa-python-pysmt, nasa-python-pysmt-utils")
		return stdebCfg


	# Writes the contents of "cppLines" to "fileName"
	def WriteCppFile(self,fileName):
		with open(fileName,'w') as f:
			for line in self.cppLines:
				f.write(line + '\n')
		f.close()

	# Writes the contents of "hppLines" to "fileName"
	def WriteHppFile(self,fileName):
		with open(fileName,'w') as f:
			for line in self.hppLines:
				f.write(line + '\n')
		f.close()

	# Writes the contents of "pyLines" to "fileName"
	def WritePyFile(self,fileName):
		with open(fileName,'w') as f:
			for line in self.pyLines:
				f.write(line + '\n')
		f.close()

def test():
	src = Command('set','CtrlReg1','1','BrakeRelease')
	grc = Command('get','StatReg1','val','Fault')
	sdc = Command('set','MotCom','val')
	gdc = Command('get','aps1','val')

	src.Print()
	grc.Print()
	sdc.Print()
	gdc.Print()

def testFunction():
	argList = []
	argList.append(['faultBit','bool'])
	argList.append(['mc','uint16_t'])
	argList.append(['aps1','float'])
	commandList = []
	commandList.append(Command('set','CtrlReg1','1','BrakeRelease'))
	commandList.append(Command('get','StatReg1','faultBit','Fault'))
	commandList.append(Command('set','MotCom','mc'))
	commandList.append(Command('get','APS1','aps1'))

	print argList
	print commandList
	
	#def __init__(self,className,name,args,commandList):
	func = Function('TurboDriverAPI','DoStuff',argList,commandList)
	func.Print()
	pass

def testApiBuilder():
	fileName = 'api/TurboDriverAPI.xml'
	ab = ApiBuilder(fileName)
	for function in ab.functionDict.values():
		function.Print()
		print '-------------'
	print '#############'
	for command in ab.commandDict.values():
		command.Print()
		print '-------------'
	print '+++++++++++++++'
	for subscriber in ab.subscriberDict.values():
		subscriber.Print()

def testGenerateCpp():
	fileName = 'api/TurboDriverAPI.xml'
	ab = ApiBuilder(fileName)
	for line in ab.GenerateCpp():
		print line

def testGenerateHpp():
	fileName = 'api/TurboDriverAPI.xml'
	ab = ApiBuilder(fileName)
	for line in ab.GenerateHpp():
		print line
	print '+++++++++++++++'
	for line in ab.GenerateCpp():
		print line

def testGeneratePy():
	fileName = 'api/TurboDriverAPI.xml'
	ab = ApiBuilder(fileName)
	for line in ab.GeneratePy():
		print line

def testBuild():
	ab = ApiBuilder()
	apiFileName = 'api/TurboDriverAPI.xml'
	cppFileName = 'test/cpp/TurboDriverAPI.cpp'
	hppFileName = 'test/cpp/TurboDriverAPI.hpp'
	pyFileName  = 'test/python/TurboDriverAPI.py'
	ab.Build(apiFileName,cppFileName,hppFileName,pyFileName)
