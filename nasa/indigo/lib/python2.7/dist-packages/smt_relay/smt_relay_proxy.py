import pysmt
import zmq
import struct


class SmtReplayProxyException(Exception):
    pass


class SmtRelayProxy(object):

    def __init__(self, smt_uri='http://localhost:11312', zmq_uri="tcp://*:19141"):
        """The SMT Relay Proxy class receives remote client requests and writes new values to SMT topics.

        Initialization contacts the local SMT core and creates an ZMQ REP socket for handling requests.
        Additionally, a ZMQ proxy device (ROUTER/DEALER) is created to ease future expansion of REQ/REP functionality.

        Args:
            smt_uri: the URI of the (local) SMT core (default 'http://localhost:11312')
            zmq_uri: the URI of the ZMQ reply socket (default 'tcp://*:19141')

        Raises:
            SmtReplayProxyException: if SMT core is unavailable

        Example:
            > proxy = SmtRelayProxy()
            > while (True):
            >   proxy.run()
            > proxy.stop()
        """

        self.msg_count = 0
        self.client = pysmt.SMTClientFactory(hostURI=smt_uri)
        self.resources = {}

        if not self.client.alive():
            raise SmtReplayProxyException('SMT Core is unavailable. Try again, later.')

        print 'Proxying...'

        # create a zmq proxy (makes adding new reqs and reps to the system easier)
        zproxy = zmq.devices.ProcessProxy(zmq.ROUTER, zmq.DEALER)
        zproxy.bind_in(zmq_uri)
        zproxy.bind_out('ipc:///tmp/smt_relay_proxy_backend')
        # optional 'monitoring' channel, for watching requests as they are processed
        zproxy.bind_mon('ipc:///tmp/smt_relay_proxy_monitor')
        zproxy.start()

        # create zmq subscriber and subscribe to topic
        context = zmq.Context()
        self.rep_socket = context.socket(zmq.REP)
        self.rep_socket.connect('ipc:///tmp/smt_relay_proxy_backend')

        # create zmq poller for subscriber
        self.poller = zmq.Poller()
        self.poller.register(self.rep_socket, zmq.POLLIN)

    def run(self):
        """Polls for requests, writing new SMT topic values as they are received.

        Requests must be of the form:
            [topic_name, topic_type, value, topic_name, ...]

        Where:
            topic_name - a bytearray() of the topic name string
            topic_type - a bytearray() of the topic type string, defined in the pysmt.smtclient._type_map dictionary
            value - a struct.pack()ed value, packed using pysmt.smtclient._type_map[topic_type]

        Multiple write requests can be put in the same ZMQ REQ msg, as long as each contains those three values in order.

        The ZMQ reply message contains the number of requests that were handled.

        All smt_relay service classes provide run().
        """

        # try-catch zmq poll so 'ctrl-c' is gracefully handled
        try:
            # poll for incoming messages (poll instead of block so it can broken)
            socks = dict(self.poller.poll(10))
            if self.rep_socket in socks and socks[self.rep_socket] == zmq.POLLIN:
                # recv request
                zmsg = self.rep_socket.recv_multipart()
                # print zmsg
                # zmsg = [topic_name, topic_type, value, topic_name, ...]
                zmsg_offset = 0
                self.msg_count += 1
                count = 0
                while (len(zmsg) - zmsg_offset) >= 3:
                    try:
                        # Get resource associated with topic_name
                        if zmsg[0 + zmsg_offset] not in self.resources.keys():
                            self.resources[zmsg[0 + zmsg_offset]] = self.client.getResource(zmsg[0 + zmsg_offset])

                        # Process value
                        value = struct.unpack(zmsg[1 + zmsg_offset], zmsg[2 + zmsg_offset])[0]

                        # Write value to topic_name
                        self.resources[zmsg[0 + zmsg_offset]].write(value)
                        count += 1

                    except pysmt.SMTClientException:
                        print 'Caught SMTClientException while trying to service request for topic:', zmsg[0 + zmsg_offset]

                    zmsg_offset += 3

                # send reply
                self.rep_socket.send(struct.pack('I', count))
        except zmq.error.ZMQError:
            print 'ZMQ Error'

    def stop(self):
        """Prints usage statistics.

        All smt_relay service classes provide stop().
        """
        self.rep_socket.close()
        print 'Received', self.msg_count, 'messages'
