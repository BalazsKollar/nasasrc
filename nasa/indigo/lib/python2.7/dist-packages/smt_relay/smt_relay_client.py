import pysmt
import zmq
import struct


class SmtRelayClientException(Exception):
    pass


class SmtRelayClient(object):

    def __init__(self, zmq_uri=None):
        """The SMT Relay Client class sends remote client requests to the SMT Relay Proxy.

        Initialization creates an ZMQ REQ socket for sending requests.

        Args:
            zmq_uri: the URI of the ZMQ request socket

        Raises:
            SmtReplayClientException: if zmq_uri is not set

        Example:
            # send request for single value
            > client = SmtRelayClient(zmq_uri='tcp://remote_host:19141')
            > client.write('/foo/bar', 'float32', 3.14)

            # send request for multiple values
            > client = SmtRelayClient(zmq_uri='tcp://remote_host:19141')
            > client.add_value('/foo/bar', 'float32', 6.28)
            > client.add_value('/foo/baz', 'int64', 12345)
            > client.send_values()
        """

        if not zmq_uri:
            raise SmtRelayClientException("Argument zmq_uri must be set (e.g., tcp://ip:port)")

        # create zmq publisher
        context = zmq.Context()
        self.req_socket = context.socket(zmq.REQ)
        self.req_socket.connect(zmq_uri)

        self.zmsg = []

    def write(self, topic_name, topic_type, value):
        """Send a request to modify a single topic's value.

        Calls add_value() and send_values() under the covers.

        Args:
            topic_name: the SMT topic name
            topic_type: the SMT topic type, defined in the pysmt.smtclient._type_map dictionary
            value: the new value to request

        Returns:
            True or False, if successful or not
        """

        self.add_value(topic_name, topic_type, value)
        return self.send_values()

    def add_value(self, topic_name, topic_type, value):
        """Adds a request to the current request message.

        Args:
            topic_name: the SMT topic name
            topic_type: the SMT topic type, defined in the pysmt.smtclient._type_map dictionary
            value: the new value to request

        Can be called multiple times before send_values() is called.
        """

        self.zmsg.append(bytearray(topic_name))
        self.zmsg.append(bytearray(pysmt.smtclient._type_map[topic_type]))
        self.zmsg.append(struct.pack(pysmt.smtclient._type_map[topic_type], value))

    def send_values(self):
        """Creates and sends ZMQ request message.

        Once sent, any requests added with add_value are cleared using _clear_values().

        Returns:
            True or False, if the reply value matches the number of requests sent.
        """

        # print self.zmsg
        self.req_socket.send_multipart(self.zmsg)
        zmsg = self.req_socket.recv()
        if ((len(self.zmsg) / 3) == struct.unpack('I', zmsg)[0]):
            self._clear_values()
            return True
        else:
            self._clear_values()
            return False

    def _clear_values(self):
        """Clears the request message."""

        self.zmsg = []
