#! /usr/bin/env python

from val_transmissions._PythonWristTransmissionLibrary import WristPushrodTransmission
from val_transmissions._PythonWristTransmissionLibrary import RobotSide


class PythonWristPushrodTransmissions(object):

    def __init__(self, robotSide):
        if robotSide.lower() == "left":
            self.robotSide = RobotSide.LEFT
        elif robotSide.lower() == "right":
            self.robotSide = RobotSide.RIGHT
        else:
            raise Exception("robotSide can ONLY be a string argument and it must be left or right.")

        self.wristPushrodTransmission = WristPushrodTransmission(self.robotSide)

    def getPushrodPositionsFromJointAngles(self, jointAngles):
        return self.wristPushrodTransmission.getPushrodPositionsFromAngles(jointAngles)

    def getPushrodVelocitiesFromJointVelocities(self, jointAngles, jointVelocities):
        return self.wristPushrodTransmission.getPushrodVelocitiesFromAngularRates(jointAngles, jointVelocities)

    def getPushrodForcesFromJointForces(self, jointAngles, jointTorques):
        return self.wristPushrodTransmission.getPushrodForcesFromJointTorques(jointAngles, jointTorques)
