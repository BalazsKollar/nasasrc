#! /usr/bin/env python

from val_transmissions._PythonTorsoTransmissionLibrary import TorsoPushrodTransmission


class PythonTorsoPushrodTransmissions(object):

    def __init__(self):
        self.torsoPushrodTransmission = TorsoPushrodTransmission()

    def getPushrodPositionsFromJointAngles(self, jointAngles):
        return self.torsoPushrodTransmission.getPushrodPositionsFromAngles(jointAngles)

    def getPushrodVelocitiesFromJointVelocities(self, jointAngles, jointVelocities):
        return self.torsoPushrodTransmission.getPushrodVelocitiesFromAngularRates(jointAngles, jointVelocities)

    def getPushrodForcesFromJointForces(self, jointAngles, jointTorques):
        return self.torsoPushrodTransmission.getPushrodForcesFromJointTorques(jointAngles, jointTorques)
