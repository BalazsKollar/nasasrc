#! /usr/bin/env python

from val_transmissions._PythonAnkleTransmissionLibrary import AnklePushrodTransmission


class PythonAnklePushrodTransmissions(object):

    def __init__(self):
        self.anklePushrodTransmission = AnklePushrodTransmission()

    def getPushrodPositionsFromJointAngles(self, jointAngles):
        return self.anklePushrodTransmission.getPushrodPositionsFromAngles(jointAngles)

    def getPushrodVelocitiesFromJointVelocities(self, jointAngles, jointVelocities):
        return self.anklePushrodTransmission.getPushrodVelocitiesFromAngularRates(jointAngles, jointVelocities)

    def getPushrodForcesFromJointForces(self, jointAngles, jointTorques):
        return self.anklePushrodTransmission.getPushrodForcesFromJointTorques(jointAngles, jointTorques)
