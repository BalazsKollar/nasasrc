#!/usr/bin/env python

# Implement a Shared Memory Core compatible client that can be used with the
# old Resource API

import mmap
import os
import struct
import xmlrpclib
import binascii
import tempfile
import platform
import importlib
import logging
from threading import Lock

logger = logging.getLogger(__name__)

# Map SMTCore type specifier to Python struct specifier and size
_type_map = {
    'int8'    : 'b',
    'uint8'   : 'B',
    'int16'   : 'h',
    'uint16'  : 'H',
    'int32'   : 'i',
    'uint32'  : 'I',
    'int64'   : 'q',
    'uint64'  : 'Q',
    'float32' : 'f',
    'float64' : 'd',
}

# Map SMTCore endian specifier to Python struct specifier
_endian_map = {
    'native' : '@',
    'big'    : '>',
    'little' : '<',
}

_default_size_map = {
        1 : 'uint8',
        2 : 'uint16',
        4 : 'uint32',
        8 : 'uint64', 
}

_ctype_map = {
    'int8_t'   : 'int8',
    'uint8_t'  : 'uint8',
    'int16_t'  : 'int16',
    'uint16_t' : 'uint16',
    'int32_t'  : 'int32',
    'uint32_t' : 'uint32',
    'int64_t'  : 'int64',
    'uint64_t' : 'uint64',
    'float'    : 'float32',
    'double'   : 'float64',
}

def ctypeAsSMTType(name):
    '''
    Return the type string for SMT from a c type name
    '''
    try:
        return _ctype_map[name]
    except KeyError:
        raise KeyError(name + " is an unknown ctype")


class AbstractTopicError(Exception):
    pass

def getAbstractTopicType(topicInfo):
    """
    Get the class definition of an abstract topic

    :param topicInfo: Topic info as a python dictionary.  This is the topic info that is retrieved from smtcore
    :return: The type of the topic
    """
    try:
        topic_name = topicInfo['name']
        package_name = topicInfo['abstractInfo']['package']
        module_name  = topicInfo['abstractInfo']['module']
        type_name    = topicInfo['abstractInfo']['type']
    except KeyError as e:
        raise AbstractTopicError('Topic "{}" missing part of abstractInfo: "{}"'.format(topic_name,e.message))

    try:
        module = importlib.import_module( '.' + module_name,package_name)
    except ImportError as e:
        raise AbstractTopicError('Import error for topic "{}": "{}"'.format(topic_name,e.message))

    try:
        type_class = getattr(module, type_name)
    except AttributeError as e:
        raise AbstractTopicError('Attribute error for topic "{}": "{}"'.format(topic_name, e.message))

    return type_class

def mmapFit(mm, size):
    '''
    Grow the memory map to fit the given size. Does nothing if already big enough.
    '''
    if size > len(mm): mm.resize(size) 

def checkOverlap(a,b):
    '''
    return true if tuples describe overlapping inclusive ranges
    '''
    if a[0] > a[1] or b[0] > b[1]: raise ValueError("Ranges must be ordered least to greatest")

    if a[1] < b[0]: return False # a is less than
    if a[0] > b[1]: return False # a is greater than

    return True

class SMTClientException(Exception):
    pass

class SMTClientFactory(object):
    '''
    A factory for clients that can also create topics
    '''

    def __init__(self, hostURI='http://localhost:11312', smtPrefix='smt', smtBaseName=None, smtDir=None):
        '''
        Initialize the factory

        Arguments: 
            hostURI:     Host:port of SMTCore
            smtBaseName: Set a file name for the named shared memory file. Leave as None to get a unique name based on smtPrefix
            smtDir:      Override directory for shared memory files to be created in
            smtPrefix:   Override prefix used in generated file names
        '''

        # members for subscribed topics

        self.hostURI = hostURI
        self.smtCore = xmlrpclib.ServerProxy(hostURI)
        self._mmap_cache = {} # keep a dictionary of memory maps we have open
        self._mmap_cache_lock = Lock()
        self.abstractFactories = {}

        # members for created topics

        if not smtDir:
            system = platform.system() 
            if system == 'Windows': smtDir = 'Global\\'
            elif system == 'Linux':   smtDir = '/dev/shm'
            elif system == 'Darwin':  smtDir = '/tmp'
            else: raise NotImplementedError("SMT doesn't support this platform ({})".format(system))

        self.smtBaseName = smtBaseName
        self.smtDir = smtDir
        self.smtPrefix = smtPrefix
        self.smtFile = None

    def addAbstractFactory(self, name, factory):
        """
        Add an abstract factory.

        Args:
            name:    name of the abstract type
            factory: class for the factory
        """
        if name in self.abstractFactories:
            raise SMTClientException("Trying to register abstract factory '{}', but a factory with that name already exists.")

        self.abstractFactories[name] = factory

    def alive(self):
        """
        Return whether or not the smtcore is alive
        """
        try:
            success, alive = self.smtCore.alive()
            if success:
                return alive
        except Exception as e:
            return False

    def shutdown(self):
        """
        Shutdown smtcore.
        """
        if self.alive():
            success, msg = self.smtCore.shutdown()

    def listTopics(self):
        """
        Return a list of published topic names
        """
        return self.smtCore.getLocations()

    def getResource(self, topic, read_only=False):
        """
        Get a new Resource object for a topic

        Args:
            topic:     String topic name
            read_only: Boolean read only flag
        """
        success, msg, loc = self.smtCore.getLocation(topic)
        # make sure theres no error
        if not success:
            raise SMTClientException('SMTCore returned error: {}'.format(msg))
        # get memory maps
        try:
            rf = self._getMMap(loc['path'][0])
            ro = loc['offset'][0]
        except IndexError:
            raise SMTClientException('getResource does not currently support types with no file backing')
        if len(loc['path']) == 2 and not read_only:
            wf = self._getMMap(loc['path'][1])
            wo = loc['offset'][1]
        else:
            wf = None
            wo = 0
        # extract other params
        topic    = loc['name']
        typename = loc['type']
        size     = int(loc['size'])
        endian   = loc['endian']
        # make sure our cached mmap actually can reach this data (it could have
        # been added after we first opened it)
        mmapFit(rf, ro+size)
        if wf: mmapFit(wf, wo+size)
        # determine constructor
        if loc['type'] == 'abstract':
            atype = loc['abstractType']
            ainfo = loc.get('abstractInfo', None)

            try:
                # first, try registered abstract factories
                if atype in self.abstractFactories:
                    resourceType = self.abstractFactories[atype]
                    res = resourceType(topic, atype, (rf, ro), (wf, wo), size, endian, ainfo)
                # Second, try to get the type automatically
                else:
                    resourceType = getAbstractTopicType(loc)
                    res = resourceType(topic, atype, (rf, ro), (wf, wo), size, endian, ainfo)

            # If that doesn't work, then try to create a resource based on it's size
            except (SMTClientException, AbstractTopicError) as e:
                res = Resource(topic, atype, (rf, ro), (wf, wo), size, endian, ainfo)
        else:
            res  = Resource(topic, typename, (rf, ro), (wf, wo), size, endian)
        return res

    def createResource(self, topic, topic_type, offset=None, size=None, endian='native', abstractInfo=None, description=''):
        """
        Fill out the details for a new Resource and attempt to actually create it

        Arguments:
            topic        : string topic name
            topic_type   : string topic type
            offset       : optional integer offset. Will auto allocate if None
            size         : integer by size. optional is type is known to smtclient
            endian       : optional specification of byte ordering for data
            abstractInfo : dictionary of values for abstract data types
            description  : optional string describing object
        """
        if size is None:
            try:
                size = struct.calcsize(_type_map[topic_type])
            except KeyError:
                raise SMTClientException("Either size or known topic_type must be specified")

        filename = self._getSmtFile()

        # allocate space if no offset specified
        if offset == None: offset = self._SMTAlloc(filename, size)

        self._createResource(filename, topic, topic_type, offset, size, endian, abstractInfo, description)
        #TODO There is a possibility for _SMTAlloc to find space but then have
        # another factory allocate it before it can be registered. This race
        # condition could be prevented by moving the allocation logic to
        # SMTCore or perhaps something more clever
        return self.getResource(topic)

    def registerLocation(self, location):
        return self.smtCore.registerLocation(location)

    def registerLocations(self, locations):
        return self.smtCore.registerLocations(locations)

    def unregisterLocation(self, locationName):
        return self.smtCore.unregisterLocation(locationName)

    def unregisterLocations(self, locations):
        return self.smtCore.unregisterLocations(locations)

    def _createResource(self, filename, topic, topic_type, offset, size, endian, abstractInfo, description):
        """
        Create a new Resource and register it
        """
        location = {}
        location['name'] = topic
        if abstractInfo:
            #TODO not everything is a register. Make this more generic
            location['type'] = 'abstract'
            location['abstractType'] = topic_type
            location['abstractInfo'] = abstractInfo
        else:
            location['type'] = topic_type
        location['path'] = []
        location['path'].append(filename)
        location['path'].append(filename)
        location['offset'] = []
        location['offset'].append(offset)
        location['offset'].append(offset)
        location['endian'] = endian
        location['size'] = size
        location['description'] = description
        status, message = self.smtCore.registerLocation(location)
        if not status: raise SMTClientException(message)

    def _SMTAlloc(self, filename, size):
        """
        Return an offset that will fit size bytes in filename

        Contacts SMTCore to get current allocations and then determines where new allocation of size bytes can reside

        WARNING: Only works for single file topics
        """
        # filter locations for only ones in the given filename, then sort by offset
        locs = sorted([ l for l in self.smtCore.getLocations() if filename in l['path'] ], key = lambda x: x['offset'])

        # no allocations! zero offset is good
        if not locs: return 0

        alloc = (0,size-1)

        # check each registered location against our current choice. If
        # overlapping bump our choice past the current location. Since they are
        # sorted this works.
        for l in locs:
            l_loc = (l['offset'][0],l['offset'][0]+l['size']-1)
            if not checkOverlap(alloc, l_loc):
                continue
            else:
                new_offset = l_loc[1] + 1
                # try to get alignment optimal by aligning to size
                misaligned = new_offset % size
                if misaligned: new_offset += size - misaligned

                alloc = (new_offset, new_offset + size - 1)

        return alloc[0]

    def _getMMap(self, filename, read_only=False):
        """
        Get a memory map for a file

        This function checks the cache for a matching file name and return it.
        If not found, attempts to open the file and create a memory map, adding
        it to the cache. If the file does not have write access it is opened
        read only no matter the state of the read_only argument

        Note that if the permissions of the file change after the file's memory
        map has been created there is no mechanism to update the cache or any
        existing Resources with the new permissions.

        Args:
            filename: Name of file to memory map
            read_only: Boolean flag to map the file in read only mode
        """
        # determine file mode
        if read_only or not os.access(filename,os.W_OK):
            mode = 'rb'
            prot = mmap.PROT_READ
        else:
            mode = 'r+b'
            prot = mmap.PROT_READ | mmap.PROT_WRITE

        try:
            with self._mmap_cache_lock:
                if filename in self._mmap_cache:
                    dev = self._mmap_cache[filename]
                else:
                    f = open(filename, mode)
                    dev = mmap.mmap(f.fileno(), 0, prot=prot) # zero maps the whole file
                    self._mmap_cache[filename] = dev
        except IOError as e:
            raise SMTClientException('Unable to open memmap for file {}'.format(filename))
        except mmap.error as err:
            raise SMTClientException('Error on mmap {}: {}'.format(filename,err))

        return dev

    def _getSmtFile(self):
        if self.smtFile == None: return self._createSmtFile()
        else: return self.smtFile

    def _createSmtFile(self):
        if self.smtBaseName:
            # use the given path and basename to create the factory file
            escaped_name = self.smtBaseName.strip('/').replace('/','-')
            filename = os.path.join(self.smtDir,escaped_name)
        else:
            # otherwise generate a name
            fileno, filename = tempfile.mkstemp(prefix=self.smtPrefix + '-', dir=self.smtDir)
            os.close(fileno)

        # the file must not be empty in order to memmap it
        # we assume here that the specified file is write accesible by us
        with open(filename, 'wb') as f:
            f.write('\x00')
            f.close()

        self.smtFile = filename

        return self.smtFile

    def removeFile(self):
        """
        Delete the file created for topics made with this factory and any
        memory maps using it in the cache
        """
        if not self.smtFile: return

        with self._mmap_cache_lock:
            if self.smtFile in self._mmap_cache:
                self._mmap_cache[self.smtFile].close()
                del self._mmap_cache[self.smtFile]

        os.remove(self.smtFile)
        self.smtFile = None

class Resource(object):
    """
    Class for accessing data in shared memory
    """
    def __init__(self, topic, typename, readInfo, writeInfo, size, endian, info=''):

        if size <= 0:
            raise SMTClientException('Trying to create resource "{}" with invalid size "{}"'.format(topic,size))

        self.topic = topic
        self.typename = typename
        self.writeable = writeInfo[0] is not None

        rfile, roff = readInfo
        wfile, woff = writeInfo

        # Determine the format string for packing and unpacking the data
        endian_fmt = _endian_map[endian]

        if typename not in _type_map:
            if size in _default_size_map:
                fmtStr = endian_fmt + _type_map[_default_size_map[size]]
            else:
                fmtStr = None
        else:
            fmtStr = endian_fmt + _type_map[typename]

        # we use inline functions with closure because there is about a 30%
        # speed penalty when using full class members
        if fmtStr:
            def readFunc():
                return struct.unpack(fmtStr, rfile[roff:roff+size])[0]
        else:
            def readFunc():
                return rfile[roff:roff+size]

        self.read = readFunc

        if not self.writeable:
            def writeFunc(data):
                raise SMTClientException(topic + ' is read only')

            def readWriteFileFunc():
                raise SMTClientException(topic + ' is read only')
        else:
            if fmtStr:
                def writeFunc(data):
                    wfile[woff:woff+size] = struct.pack(fmtStr, data)

                def readWriteFileFunc():
                    return struct.unpack(fmtStr, wfile[woff:woff+size])[0]
            else:
                def writeFunc(data):
                    wfile[woff:woff+len(data)] = data

                def readWriteFileFunc():
                    return wfile[woff:woff+size]

        self.write = writeFunc
        self.readWriteArea = readWriteFileFunc
        self.fieldNames = []

    def __str__(self):
        return str(self.read())

    def __repr__(self):
        return "<SMTClient:{},{}>".format(self.typename, self.topic)

    def __call__(self, data=None):
        if data is None: return self.read()
        else: self.write(data)

    def addFieldName(self, name):
        """
        Add a field name to this resource.

        'fields' are defined by the sub-class.  All 'fields' must be a member or method of the subclass.
        If the field already exists, and SMTClientException will be raised.
        If the field is invalid (there is no member or method of that name) and SMTClientException will be raised.

        :param name: Name of the field to add.
        """
        if name in self.fieldNames:
            raise SMTClientException('Trying to add field "{}", but that field already exists'.format(name))

        attr = getattr(self, name, None)
        if attr is None:
            raise SMTClientException('Trying to add field "{}", but no member or method of that name exists'.format(name))

        self.fieldNames.append(name)

    def getField(self, name):
        """
        Get a field

        :param name: name of the field
        :return: The field
        """
        if name not in self.fieldNames:
            raise SMTClientException('Trying to get field "{}", but no field of that name exists'.format(name))

        attr = getattr(self, name, None)
        if attr is None:
            raise SMTClientException('Trying to get field "{}", but no field of that name exists'.format(name))
        return attr

    def getFieldNames(self):
        """
        Get a list of field names from this resource
        :return: List of strings
        """
        return self.fieldNames

# Glue to work with old API

defaultFactory = SMTClientFactory()

def getResource(topic):
    return defaultFactory.getResource(topic)

def listTopics():
    return defaultFactory.listTopics()

