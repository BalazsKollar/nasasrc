
# Functions for evaluating performance of the library

import timeit
import pysmt
import os

def benchSMTClient():
    '''
    Runs benchmarks on SMTClient

    :rtype: Dictionary of {benchmark names: [ (subname, time), ... ]}
    '''

    results = {}

    # create a shared memory location because we dont have code for that yet
    shmfile = '/dev/shm/smtClientBenchmark'
    with open(shmfile,'w') as f:
        os.ftruncate(f.fileno(),8)
    
    # run the shared memory server
    smtcore = pysmt.SMTCore("0.0.0.0", 11312)
    smtcore.process()

    ##### Read Testing #####
    endianReadBench = []
    typeReadBench = []
    iterations = 1000000
    locdict = {
        'name':'testTopic',
        'type':'uint32',
        'path': [shmfile, shmfile],
        'offset': [0, 0],
        'endian': 'native',
    }
    def testread(ld):
        smtcore.registerLocation(ld)
        r = pysmt.getResource('testTopic')
        res = timeit.timeit(r.read,number=iterations)/float(iterations)
        smtcore.unregisterLocation('testTopic')
        return res

    # endian
    locdict['endian'] = 'native'
    endianReadBench.append(('readUint32Native', testread(locdict)))
    locdict['endian'] = 'big'
    endianReadBench.append(('readUint32Big', testread(locdict)))
    locdict['endian'] = 'little'
    endianReadBench.append(('readUint32Little', testread(locdict)))

    # type
    locdict['endian'] = 'native'

    locdict['type'] = 'uint8'
    typeReadBench.append(('readUint8', testread(locdict)))
    locdict['type'] = 'uint16'
    typeReadBench.append(('readUint16', testread(locdict)))
    locdict['type'] = 'uint32'
    typeReadBench.append(('readUint32', testread(locdict)))
    locdict['type'] = 'uint64'
    typeReadBench.append(('readUint64', testread(locdict)))
    locdict['type'] = 'float32'
    typeReadBench.append(('readFloat32', testread(locdict)))
    locdict['type'] = 'float64'
    typeReadBench.append(('readFloat64', testread(locdict)))

    results['endianRead'] = endianReadBench
    results['typeRead'] = typeReadBench

    smtcore.shutdown()
    try: os.remove(shmfile)
    except IOError: pass

    return results

def runBenchmarksAndPrintResults():
    print 'Running benchmarks'
    print '------------------'
    print 'Benchmarking SMTClient'   
    results = benchSMTClient()
    print 'Done!'
    for k, v in results.iteritems():
        print 
        print 'Test:', k
        print '-'*10
        pad = max([ len(t[0]) for t in v ])
        fastest = min([ t[1] for t in v ])
        v.sort(key=lambda x: x[1])
        for t in v:
            print "{:{PAD}s} {:>0.3e} {:+3.2%}".format(
                    t[0],t[1],(t[1]-fastest)/fastest,PAD=pad)
        print '-'*10
