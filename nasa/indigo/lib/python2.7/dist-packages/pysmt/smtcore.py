import SimpleXMLRPCServer
import socket
import threading
import os
import time
import sys
import logging

logger = logging.getLogger(__name__)

class SMTException(Exception):
    pass

class SMTCore:
    def __init__(self, hostname, port, verbose=False):
        try:
            self.server = SimpleXMLRPCServer.SimpleXMLRPCServer((hostname, port), logRequests=verbose, allow_none=True)
        except socket.error as e:
            raise SMTException("Error starting SMTCore at {}:{} -- {}".format(hostname,port, e))

        self.port = port
        self.locationTypes = {
        'int8': 1, 'uint8': 1,
        'int16': 2, 'uint16': 2,
        'int32': 4, 'uint32': 4,
        'int64': 8, 'uint64': 8,
        'float32': 4, 'float64': 8,
        'abstract': None}
    
        # Memory locations that are registered with server.
        # Description.
        # Name - Name of location in shared memory.
        # Type - Data type that the location contains. If Abstract, AbstractInfo must be populated.
        # AbstractType - Default: None (i.e. Ringbuffer)        
        # AbstractInfo - Default: None
            # Implementation specific 
        # Path: [readPath, (Optional) writePath]
            # If no write path is specified, location is considered read-only.
        # Offset - [readOffset, (Optional) writeOffset] 
            # If no writeOffset is provided and writePath is specified, registration will fail.
        # Endian (Optional) - Default:native
            #big, little, native.   
        # Size (Bytes) - Type size.
        # Description (Optional)- Description of memory location usage.
        self.locations = {}
        
        #Register functions to server.
        self.server.register_function(self.alive, "alive")
        self.server.register_function(self.getLocation, "getLocation")
        self.server.register_function(self.getLocations, "getLocations")
        self.server.register_function(self.registerLocation, "registerLocation")
        self.server.register_function(self.registerLocations, "registerLocations")
        self.server.register_function(self.shutdown_request, "shutdown")
        self.server.register_function(self.unregisterLocation, "unregisterLocation")
        self.server.register_function(self.unregisterLocations, "unregisterLocations")
        self.server.register_function(self.unregisterAllLocations, "unregisterAllLocations")

        self.serverThread = None
        self.shutdown_delay = 0
        
    def alive(self):
        """
        Return whether or not the server thread is still alive

        @type return tuple
        @param return (True, alive) where alive is either True or False

        """
        return (True, self.serverThread.isAlive())

    def getLocation(self, name):
        if name in self.locations:        
            return (True, "", self.locations[name])
        else:
            return (False, "{n} has not been registered.".format(n=name), {})
        
    def getLocations(self):
        return self.locations.values()
    
    def getServerInfo(self):
        return self.server.server_address

    def process(self):
        """
        Start the XMLRPCServer serve_forever loop in it's own thread
        """
        if self.serverThread:
            if self.serverThread.isAlive():
                return
        self.serverThread = threading.Thread(target=self.processThread)
        self.serverThread.start()
    
    def processThread(self):
        """
        Start the XMLRPCServer serve_forever loop.  This method blocks until the server
        loop has exited.  The server loop will exit if self.shutdown() or self.shutdown_request()
        is called.
        """
        logger.info('start with pid[{}] on port[{}]'.format(str(os.getpid()),str(self.port)))
        self.server.serve_forever()
        logger.info('smtcore has shutdown')

    def registerLocation(self, location):
        if location['name'] in self.locations:
            return (False, "{name} has already been registered.".format(name=location['name']))
        else:
            # Verify valid type
            if not 'type' in location:
                return (False, "A type must be provided when registering a location.")
            if not self.validLocationType(location['type']):
                return (False, "{lt} is not a supported type.".format(lt=location['type']))
            # Check if type is abstract, and if true, ensure that abstract info is provided.
            if location['type'] == 'abstract':
                if (not 'abstractType' in location) or (location['abstractType'] == None):
                    return (False, 'abstractType must be provided when type is abstract.')

            # check path and offset
            if not 'path' in location:
                return (False, 'Path is required.')
            if not 'offset' in location:
                return (False, 'Offset is required.')
            if not (type(location['path']) == list and type(location['offset']) == list):
                return (False, 'Path and Offset must be lists.')
            if not len(location['path']) == len(location['offset']):
                return (False, 'Path and Offset must have the same length.')

            # Check if an endian was provided.  If None, set to native as default.
            if (not 'endian' in location) or (location['endian'] == None):
                location['endian'] = 'native'
                
            if location['type'] == 'abstract':
                if(not 'size' in location) or (location['size'] == None):
                    return (False, 'size must be provided with type is abstract.')
            else:
                location['size'] = self.locationTypes[location['type']]
                
            self.locations[location['name']] = location
            return (True, "")
            
    def registerLocations(self, locations):
        failures = []
        for location in locations:
            code, msg = self.registerLocation(location)
            if not code:
                failures.append((location['name'], msg))
        return (not len(failures) > 0, failures)

    def serverBind(self):
        socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        SimpleXMLRPCServer.SimpleXMLRPCServer.server_bind(self)

    def shutdown(self):
        """
        Stop the XMLRPCServer.  This thread will block until the server has stopped.
        """
        time.sleep(self.shutdown_delay)
        logger.info('Shutdown requested.')
        self.server.shutdown()

    def shutdown_request(self):
        """
        Stop the XMLRPCServer.  This will shutdown the server in another thread to 
        allow enough time for this method to return a value to a remote client.
        """
        self.shutdown_delay = 0.1
        shutdownThread = threading.Thread(target=self.shutdown)
        shutdownThread.start()
        return (True,"")

    def unregisterLocation(self, locationName):
        if locationName in self.locations:
            del self.locations[locationName]
            return (True, "")
        else:
            return (False, "{name} has not been registered.".format(name=locationName))
            
    def unregisterLocations(self, locations):
        failures = []
        for location in locations:
            code, msg = self.unregisterLocation(location)
            if not code:
                print msg
                failures.append((location, msg))
        if len(failures) > 0:
            return (False, failures)
        else:
            return (True, "")
                
    def unregisterAllLocations(self):
        self.locations = {}
    
    def validLocationType(self, locationType):
        return locationType in self.locationTypes

    def waitForShutdown(self):
        while(self.serverThread.isAlive()):
            time.sleep(0.01)
