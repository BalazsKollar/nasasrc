
schedule_offsets = {
        'node': 27,
        'rw': 24,
        'addr1': 16,
        'addr2': 8,
        'addr3': 0
        }

dma_offsets = {
        'type': 28,
        'channel': 23,
        'addr_start': 12,
        'addr_end': 1,
        'last_entry': 15
        }

dma_types = {
        'version': 0,
        'schedule': 1,
        'write': 2,
        'read': 3,
        'schedule_readback': 4,
        'write_enable': 5,
        'get_comm_period': 6,
        'get_comm_stats': 7,
        'stream_write_read': 8
        }

TYPE_READ = 0
TYPE_WRITE = 1
TYPE_STREAM = 4

NODES = 8
ADDRESSES = 128
CHANNELS = 8

COMM_STATS_LEN = 4*CHANNELS + 4*CHANNELS*NODES
