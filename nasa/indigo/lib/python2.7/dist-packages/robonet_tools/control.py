#!/usr/bin/env python

'''
Controller and data locator/publisher for a Robonet master.

* Loads a schedule per channel and a write enable table to configure the fabric.
* Registers the scheduled data with the ROS master
* Resolves shared memory offsets for local clients

Author: Brian Wightman
'''

import sys
import struct
import os
import time
import xmlrpclib
import socket
import signal
from threading import Thread
from Queue import Queue
from argparse import ArgumentParser
from pysmt import SMTClientFactory
import logging

import ctypes

from build_schedule import *
from dma_defs import *

#from nasa_rsmp import nodeStreamInterface, stdMsgHandler

DB_NAME = 'config'
STREAM_TIMEOUT = 0.007  # time in seconds between stream operations
published_topic_list = []

logger = logging.getLogger(__name__)


def buildHeader(type, channel=None, addr_start=None, addr_end=None,
                last_entry=None):
    header = dma_types[type] << dma_offsets['type']
    if type == 'schedule':
        header |= channel << dma_offsets['channel']
        header |= last_entry << dma_offsets['last_entry']
    if type == 'schedule_readback':
        header |= channel << dma_offsets['channel']
    if type in ['read', 'write']:
        header |= channel << dma_offsets['channel']
        header |= addr_start << dma_offsets['addr_start']
        header |= addr_end << dma_offsets['addr_end']
    if type == 'stream_write_read':
        header |= channel << dma_offsets['channel']
    return header


class ScheduleLoader():

    def __init__(self):
        self.offsets = {}

    def buildSchedule(self, schedule):
        ''' convert a list of reads/writes into a list of packets per channel '''
        elements = {}
        write_enables = [0 for i in range(0, NODES * (2 * ADDRESSES))]

        # each channel
        for c, s in schedule.items():
            if not c in elements:
                elements[c] = []
            # each node
            for n, d in s.items():
                # reads
                for entry in d['read']:
                    while len(entry) < 3:
                        entry.append(entry[-1])
                    raw = (n << schedule_offsets['node'] |
                           TYPE_READ << schedule_offsets['rw'] |
                           entry[0] << schedule_offsets['addr1'] |
                           entry[1] << schedule_offsets['addr2'] |
                           entry[2] << schedule_offsets['addr3'])
                    elements[c].append(raw)
                # writes
                for entry in d['write']:
                    while len(entry) < 2:
                        entry.append(entry[-1])
                    # Set an enable bit for each write entry
                    # The address is composed of the node offset + data address
                    # in 32-bit offsets
                    write_enables[ADDRESSES * n + (entry[0] >> 1)] |= 1 << c
                    write_enables[ADDRESSES * n + (entry[1] >> 1)] |= 1 << c
                    raw = (n << schedule_offsets['node'] |
                           TYPE_WRITE << schedule_offsets['rw'] |
                           entry[0] << schedule_offsets['addr1'] |
                           entry[1] << schedule_offsets['addr2'])
                    elements[c].append(raw)
                # stream topic for node
                for entry in d['streams']:
                    # add stream packet
                    raw = (n << schedule_offsets['node'] |
                           TYPE_STREAM << schedule_offsets['rw'])
                    elements[c].append(raw)
        self.elements = dict(
            [(c, ''.join([struct.pack('I', i) for i in ii])) for c, ii in elements.items()])
        self.enables = ''.join([struct.pack('I', i) for i in write_enables])


class Scheduler():

    def __init__(self, loader, use_dummy_driver):
        self.loader = loader
        self.sched = None
        self.driver = None if use_dummy_driver else ctypes.cdll.LoadLibrary(
            'libsynapse.so')

    def start(self):
        def run_scheduler(channels):
            channel_mask = 0
            for c in channels:
                channel_mask |= 1 << c
            self.driver.runDMA(ctypes.c_uint32(channel_mask))

        self.sched = Thread(target=run_scheduler,
                            args=(self.loader.elements.keys(),))
        self.load()
        self.load()
        self.sched.start()

    def load(self):
        # load enables
        header = struct.pack('I', buildHeader('write_enable'))
        packet = header + self.loader.enables
        self.driver.enqueuePacket(ctypes.c_char_p(
            packet), ctypes.c_int(len(packet)))

        # load schedule packets
        for c, d in self.loader.elements.items():
            last = (len(self.loader.elements[c]) >> 2) - 1
            if last < 0:    # empty channel
                continue
            if last >= 0xFF:
                logger.error(
                    'Error: channel {} schedule is {} entries long, max is 254'.format(c, last + 1))
            header = struct.pack('I', buildHeader(
                'schedule', channel=c, last_entry=last))
            packet = header + d
            self.driver.enqueuePacket(ctypes.c_char_p(
                packet), ctypes.c_int(len(packet)))

    def stop(self):
        if self.sched:
            self.driver.doShutdown()
            self.sched.join()
            self.sched = None


def create_dummy_shm():
    import os
    import getpass
    size = 16 * 512 * 8
    fname = '/dev/shm/robonet_' + getpass.getuser()
    with open(fname, 'a') as f:
        os.utime(fname, None)   # equivalent to the 'touch' command
        f.truncate(size)        # resize file
    return fname

# def get_live_nodes():
#    # find all communicating nodes
#    stats = get_comm_stats()
#    if stats is None:
#        print 'Error: unable to read comm stats'
#        return []
#    nodes = []
#    for ci, c in enumerate(stats):
#        for ni, r in enumerate(c):
#            # if we have marginally usable comm, try to grab tokens
#            if r > 0.1:
#                nodes.append((ci, ni))
#    if not nodes:
#        print 'Warning: no live nodes found, check cables and power.'
#    return nodes


def main():
    parser = ArgumentParser(
        description='Robonet schedule loader and DMA transaction handler')
    parser.add_argument('-r', '--robot_description', dest='robot_description', default='',
                        help='path to a YAML robot description file')
    parser.add_argument('-y', '--dump_node_yaml', dest='dump_node_yaml', nargs=3, type=str,
                        help='save the tokens from a node to disk "channel_name node_name file_name"')
    parser.add_argument('-s', '--stream_tokens', dest='stream_tokens', action='store_true',
                        help='load tokens from each joint over streaming and merge with tokens in the YAML')
    parser.add_argument('-d', '--dummy_driver', dest='dummy_driver', action='store_true',
                        help='the dummy driver is loaded, don\'t try to talk to hardware')
    parser.add_argument('--hostUri', type=str, default="http://localhost:11312",
                        help='The URI for smtcore.  Default is http://localhost:11312')

    args, unknown = parser.parse_known_args()

    if args.dummy_driver:
        if os.path.exists('/dev/robonet_read'):
            print 'INFO: Kernel devices detected while running in dummy mode; run `sudo apt-get remove nasa-robonet-s6ec-dummy` to remove this deprecated driver if applicable.'
        dummy_fname = create_dummy_shm()

    if not args.robot_description:
        logger.error('"--robot_description" is required')
        raise SystemExit

    logger.info('Using robot_description: "{}"'.format(args.robot_description))

    loader = ScheduleLoader()
    scheduler = Scheduler(loader, args.dummy_driver)

    client = SMTClientFactory(args.hostUri)

    if not client.alive():
        msg = 'Unable to contact smtcore.  Is it running?'
        raise Exception(msg)
        sys.exit(-1)

    tokens = {}

    parser = Schedule(args.robot_description)
    channel_names = dict([(j, i) for i, j in parser.channels.items()])
    node_names = dict([(c, dict([(ni, n) for n, ni in parser.nodes[c].items()]))
                       for c in parser.channels.values()])

    # assembly lookup for stream interfaces
    HwStreamIfaces = {}
    for c, ci in parser.channels.items():
        for n, ni in parser.nodes[ci].items():
            HwStreamIfaces[
                '/{}/{}/StreamInterface'.format(c, n)] = '/dev/shm/robonet_stream_{}_{}'.format(ci, ni)

    def publishTopics():
        global published_topic_list
        to_be_published = []
        type_type_map = {'uint8_t': ('uint8', 1),
                         'int8_t': ('int8', 1),
                         'uint16_t': ('uint16', 2),
                         'int16_t': ('int16', 2),
                         'uint32_t': ('uint32', 4),
                         'int32_t': ('int32', 4),
                         'float': ('float32', 4)}

        try:
            logger.info('Publishing topics...')
            # publish normal Robonet elements
            for key, val in parser.element_offsets.items():
                out_type = type_type_map.get(val[1], ('abstract', 2))
                location = {'name': key,
                            'endian': 'big',
                            'type': out_type[0],
                            'size': out_type[1],
                            'offset': [val[0], val[0]] if val[2] == 'write' else [val[0]]
                            }
                if args.dummy_driver:
                    location['path'] = [dummy_fname, dummy_fname] if val[
                        2] == 'write' else [dummy_fname]
                else:
                    location['path'] = ['/dev/shm/robonet_read', '/dev/shm/robonet_write'] if val[
                        2] == 'write' else ['/dev/shm/robonet_read']

                # add type info for registers
                if not val[1] in type_type_map:
                    location['abstractType'] = 'register'
                    reg_type = val[1].split('/')
                    if not len(reg_type) == 3:
                        raise Exception(
                            'For element {}, invalid type "{}" found'.format(key, val[1]))
                    location['abstractInfo'] = {'package': reg_type[
                        0], 'module': reg_type[1], 'type': reg_type[2]}
                to_be_published.append(location)

            # publish stream interfaces
            for key in HwStreamIfaces:
                location = {'name': key,
                            'offset': [0],
                            'path': [HwStreamIfaces[key]],
                            'size': 0,
                            'type': 'abstract',
                            'abstractType': 'RobonetStream',
                            }
                to_be_published.append(location)
            # publish comm stats
            # for channel in parser.channels:
            #     location = {'name': '/comm_stats/loop_rates/{}'.format(channel),
            #                 'offset': [4*parser.channels[channel]],
            #                 'path': ['/dev/shm/robonet_stats'],
            #                 'endian': 'big',
            #                 'size': 4,
            #                 'type': 'float32',
            #                }
            #     to_be_published.append(location)
            #     for node in parser.nodes[parser.channels[channel]]:
            #         ch = parser.channels[channel]
            #         location = {'name': '/comm_stats/recv/{}/{}'.format(channel, node),
            #                     'offset': [4*CHANNELS + 4*(ch*NODES + parser.nodes[ch][node])],
            #                     'path': ['/dev/shm/robonet_stats'],
            #                     'endian': 'big',
            #                     'size': 4,
            #                     'type': 'float32',
            #                    }
            #         to_be_published.append(location)
            published_topic_list = [i['name'] for i in to_be_published]
            a, b = client.registerLocations(to_be_published)
            if not a:
                msg = 'Error registering publisher: {}'.format(b)
                raise Exception(msg)

        except socket.error:
            msg = 'Unable to contact smtcore, is it running?'
            raise Exception(msg)

    def unPublishTopics():
        global published_topic_list
        try:
            logger.info('Unregistering topics...')
            client.unregisterLocations(published_topic_list)
            published_topic_list = []
        except socket.error:
            msg = 'Unable to unregister topics.  Is smtcore running?'
            raise Exception(msg)

    def loadGeneratedSchedule():
        unPublishTopics()
        scheduler.stop()
        parser.build_schedule(tokens)
        scheduler.loader.buildSchedule(parser.schedule)
        if not args.dummy_driver:
            scheduler.start()
        publishTopics()
        return 1

    def loadAllStreamSchedule(parser):
        unPublishTopics()
        if not args.dummy_driver:
            scheduler.stop()

        schedule = {}
        for c in range(CHANNELS):
            schedule[c] = {}
            for n in range(NODES):
                schedule[c][n] = {'read': [[0]], 'write': []}

        scheduler.loader.buildSchedule(schedule)
        if not args.dummy_driver:
            scheduler.start()
        publishTopics()

    def reload_tokens(tokens):
        '''load tokens over stream mode from all reachable nodes'''
        loadAllStreamSchedule(parser)
        tokens.clear()

        tokens_queue = Queue()

        def getTokens(c, n):
            if not c in tokens:
                tokens[c] = {}
            if not c in channel_names or not c in node_names:
                logger.warn(
                    'Found node /_{}_/_{}_ not in schedule'.format(c, n))
                return
            if not n in node_names[c]:
                logger.warn(
                    'Found node /{}/_{}_ not in schedule'.format(channel_names[c], n))
                return
            logger.info(
                'Found node /{}/{}'.format(channel_names[c], node_names[c][n]))
            fs = nodeStreamInterface.NodeFileStreamInterface(
                (channel_names[c], node_names[c][n]))
            smh = stdMsgHandler.StdMsgHandler(fs, timeout=1)
            # make sure no data hanging out in buffers
            fs.flushInput()

            try:
                new_tokens = smh.getDataTokens()
                tokens_queue.put((c, n, new_tokens))
            except stdMsgHandler.MsgUnsupportedError as e:
                logger.warn("Node does not support getting data tokens")
            except stdMsgHandler.MsgHandlerError as e:
                logger.warn("Failed to get data tokens:", e)

        # pause to let comm stats update after the schedule change
        time.sleep(0.5)
        # find all communicating nodes
        threads = []
        for c, n in get_live_nodes():
            t = Thread(target=getTokens, args=(c, n))
            threads.append(t)
            t.daemon = True
            t.start()

        for t in threads:
            t.join()

        while not tokens_queue.empty():
            c, n, t = tokens_queue.get()
            tokens[c][n] = t
        if args.dump_node_yaml:
            if args.dump_node_yaml[0] in parser.channels:
                ch = parser.channels[args.dump_node_yaml[0]]
                if args.dump_node_yaml[1] in parser.nodes[ch]:
                    nd = parser.nodes[ch][args.dump_node_yaml[1]]
                    with open(args.dump_node_yaml[2], 'w') as f:
                        if not ch in tokens or not nd in tokens[ch]:
                            logger.warn('Warning: For option dump_node_yaml, node /{}/{} not found.'.format(
                                args.dump_node_yaml[0], args.dump_node_yaml[1]))
                            return
                        f.write(yaml.dump(tokens[ch][nd]))

    # set up handler to unregister topics on shutdown
    def signal_handler(signal, frame):
        try:
            unPublishTopics()
        except Exception as e:
            logger.error(e, exc_info=1)

        scheduler.stop()
        sys.exit(0)

    signal.signal(signal.SIGINT, signal_handler)

    def getRobot():
        # marshal through yaml because xmlrpc requires dictionary keys to be
        # strings
        return yaml.dump(channel_names), yaml.dump(node_names)

    def reloadSchedule():
        if not args.dummy_driver and args.stream_tokens:
            reload_tokens(tokens)
        loadGeneratedSchedule()

    def runInBackground(f, args=None):
        if args:
            t = Thread(target=f, args=args)
        else:
            t = Thread(target=f)
        t.daemon = True
        t.start()

    if not args.dummy_driver and args.stream_tokens:
        reload_tokens(tokens)
    loadGeneratedSchedule()

    while True:
        time.sleep(1)
