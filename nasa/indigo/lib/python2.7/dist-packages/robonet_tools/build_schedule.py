
# build_schedule.py
# @author Brian Wightman
# @brief Builds a program to load to a RoboNet master based on a high-level xml description
# RoboNet v1 packets contain 3 read addresses or 2 write addresses. This script takes an xml
# file (see format below) and converts it to a list of data that needs to be scheduled. This
# data is then packed into corresponding read or write packets.
#
# Also, a list of fully-qualified tokens paired with their offsets is
# generated.

import sys
import os
from operator import or_
import yaml

DB_NAME = 'config'

address_offsets = {
    'channel': 12,
    'node': 9,
    'address': 0
}

generic_types = {
    'uint8_t': 1,
    'int8_t': 1,
    'uint16_t': 1,
    'int16_t': 1,
    'uint32_t': 2,
    'int32_t': 2,
    'float': 2
}

# mimic standard print but send it to stderr


def print_err(*args):
    sys.stderr.write(' '.join(map(str, args)) + '\n')
    os._exit(1)

_prev_warnings = []


def print_warn(*args):
    out = ' '.join(map(str, args)) + '\n'
    if not out in _prev_warnings:
        _prev_warnings.append(out)
        sys.stderr.write(out)


class Loader(yaml.Loader):

    ''' Loader subclass that adds an include directive in YAML '''

    def __init__(self, stream):
        self._root = os.path.split(stream.name)[0]
        super(Loader, self).__init__(stream)

    def include(self, node):
        filename = os.path.join(self._root, self.construct_scalar(node))
        try:
            with open(filename, 'r') as f:
                return yaml.load(f, Loader)
        except IOError:
            print_err('Error, could not open file "{}" referenced in file "{}"'.format(
                filename, self.stream.name))
Loader.add_constructor('!include', Loader.include)


def checkAddKey(d, key, val):
    if not key in d:
        d[key] = val


class Schedule(object):

    '''
    Combines tokens loaded from each joint and tokens from a schedule file. Tokens are filtered by
    a list of tags and then built into a list of packets per channel to be loaded to a Synapse.

    Note: the special include directive below can be used to load a YAML file in place
    !include relative/path/to/file.yaml

    ################
    ### schedule ###
    ################

    channel_name:
        id: 1           # 0-based channel index
        nodes:
            node_name:
                id: 0   # 0-based node index
                tags:   # a list of tags to use, optional and defaults to every token if omitted
                    - DEBUG
                    - TORQUE
                tokens: # a list of tokens to use in addition to the stream-loaded list
                    - {read: true, name: Proc_Utilization, offset: 223, type: uint16_t, tags: [aaa]}
                    - {read: true, name: CurrentB_Calc_Raw, offset: 227, type: uint16_t, tags: [aaa]}
                    - {read: true, name: StatReg1, offset: 3, type: uint16_t, api: Turbodriver, apitype: StatReg1}
            node_name2:
                id: 2
                tokens: !include turbo.yaml
    channel_name4:
        id: 6
        nodes:


    ##############
    ### tokens ###
    ##############

    read: bool      # true == node -> master
    name: str       # last part of /channel/node/<token_name>
    offset: int     # 2-byte-aligned offset into node memory (token offset 2 == byte offset 4)
    type: str       # [(u)int8_t, (u)int16_t, (u)int32_t, float]
    tags: [str]     # optional: [DEBUG, DEPLOY, etc.] - used to minimize the number of tokens
    api: str        # optional: the name of an api ROS package
    apitype: str    # optional, required with api attr: specifies the type in the api package
                    #   to load for this token in place of the standard type
    '''

    def __init__(self, path):
        '''
        path = a file containing a schedule
        '''
        self.tokens = {
        }            # {channel_name: {node_name: [{read: bool, name: str, offset: int, type: str, tags: [str]} ] } }
        # {channel_addr: {node_addr: [{read: bool, name: str, offset: int, type: str, tags: [str]} ] } }
        self.auto_tokens = {}
        self.channels = {}          # {channel_name: index}
        self.nodes = {}             # [(channel_index, [(node_name, index)] ]
        self.tags = {}
        self.element_offsets = {}   # [(fqtn, (byte_offset, type, direction))]

        # {channel_addr: {node_addr: {read: [(off1, off2, off3)], write: [(off1, off2)]} } }
        self.schedule = {}

        try:
            with open(path) as f:
                try:
                    data = yaml.load(f, Loader)
                except yaml.parser.ParserError, ye:
                    print_err('Syntax error in schedule', ye)
        except IOError:
            print_err('Failed to open schedule file {}'.format(path))
        # verify correct formatting
        # for each channel
        for c, cc in data.items():
            if not 'address' in cc:
                print_err('Schedule: No "address" in channel {}'.format(c))
            self.channels[c] = cc['address']
            checkAddKey(self.tags, self.channels[c], {})
            checkAddKey(self.channels, c, {})
            checkAddKey(self.tokens, c, {})
            # for each node
            for n, nn in cc['nodes'].items():
                checkAddKey(self.nodes, self.channels[c], {})
                checkAddKey(self.tokens[c], n, [])
                if not 'address' in nn:
                    print_err(
                        'Schedule: No "address" in channel {}, node {}'.format(c, n))
                self.nodes[self.channels[c]][n] = nn['address']
                self.tags[self.channels[c]][self.nodes[
                    self.channels[c]][n]] = nn.get('tags', [])
                # for each token
                for it, t in enumerate(nn.get('tokens', [])):
                    # if the topic is not a stream topic, it needs to be
                    # checked to ensure it has all of the correct fields
                    if not ('stream' in t):
                        for attr in ['read', 'type', 'name', 'offset']:
                            if not attr in t:
                                print_err('Missing attr "{}" in /{}/{} token "{}"'.format(
                                    attr, c, n, it if attr == 'name' else t['name']))
                        if 'api' in t and not 'apitype' in t:
                            print_err(
                                'Missing attr "apitype" in /{}/{} token {}, implied by attr "api"'.format(attr, c, n, it))
                        # add token
                    self.tokens[c][n].append(t)

        # warn about duplicate names for the same channel address
        dups = set([x for x in self.channels.values()
                    if self.channels.values().count(x) > 1])
        if dups:
            print 'Warning: duplicate names for channel address(es) {}'.format(list(dups))
        # warn about duplicate names for the same node address
        for c, cx in self.channels.items():
            dups = set([x for x in self.nodes[cx].values()
                        if self.nodes[cx].values().count(x) > 1])
            if dups:
                print 'Warning: in channel {}, duplicate names for node address(es) {}'.format(c, list(dups))

    def build_schedule(self, auto_tokens={}):
        '''
        Combine stream-loaded tokens and tokens from the schedule, filter with "tags" list,
        then group them into read and write packets.
        '''
        self.schedule = {}
        self.element_offsets = {}

        merged_tokens = {}

        # merge manual tokens and auto_tokens
        for c, cc in self.tokens.items():
            ci = self.channels[c]
            checkAddKey(merged_tokens, ci, {})
            for n, nn in cc.items():
                ni = self.nodes[ci][n]
                merged_tokens[ci][ni] = list(
                    auto_tokens.get(ci, {}).get(ni, []))  # make a copy
                for t in nn:
                    found = False
                    for t2 in auto_tokens.get(ci, {}).get(ni, []):
                        if t['name'] == t2['name']:
                            print 'Warning: Skipping duplicate token {} in channel {} node {}'.format(t['name'], c, n)
                            found = True
                            break
                    if not found:
                        merged_tokens[ci][ni].append(t)
        unknowns = []
        for c, cc in merged_tokens.items():
            checkAddKey(self.schedule, c, {})
            for n, nn in cc.items():
                # find the real names for the channel and node addresses
                found_ch = None
                found_nd = None
                for ch_n, ch_a in self.channels.items():
                    if c == ch_a:
                        found_ch = ch_n
                        for nd_n, nd_a in self.nodes[c].items():
                            if n == nd_a:
                                found_nd = nd_n
                                break
                        break
                if not (found_ch and found_nd):
                    if not (found_ch, found_nd) in unknowns:
                        print 'Warning: Found tokens for channel {} node {} but a name was not defined, skipping.'.format(c, n)
                    continue

                checkAddKey(self.schedule[c], n, {
                            'read': [], 'write': [], 'streams': []})
                # sort and filter list - a topic is either 16 bits (single), 32
                # bits (double), or a stream topic
                singles = []
                doubles = []
                streams = []
                for t in nn:
                    # filter by tags
                    if self.tags[c][n] and not reduce(or_, (tag in t.get('tags', []) for tag in self.tags[c][n])):
                        continue
                    # checking for stream topic needs to occur first since they
                    # do not have explicit types
                    if 'stream' in t:
                        streams.append(t)
                        continue
                    elif not t['type'] in generic_types:
                        print 'Warning: Skipping unknown data type {} for token {}'.format(t['type'], t['name'])
                        continue
                    if generic_types[t['type']] == 1:
                        singles.append(t)
                    else:
                        doubles.append(t)

                # add doubles first for optimal packing
                for t in doubles + singles:
                    offsets = [t['offset']]
                    if generic_types[t['type']] == 2:
                        offsets.append(t['offset'] + 1)
                    # add token's offset(s) to a packet
                    added = False
                    if t['read']:
                        for pkt in self.schedule[c][n]['read']:
                            if len(pkt) + generic_types[t['type']] <= 3:
                                pkt.extend(offsets)
                                added = True
                                break
                        if not added:
                            self.schedule[c][n]['read'].append(offsets)
                    else:
                        for pkt in self.schedule[c][n]['write']:
                            if len(pkt) + generic_types[t['type']] <= 2:
                                pkt.extend(offsets)
                                added = True
                                break
                        if not added:
                            self.schedule[c][n]['write'].append(offsets)

                    # add this element info to the published list
                    fqtn = '/{}/{}/{}'.format(found_ch, found_nd, t['name'])
                    # calculate the byte-wise offset
                    calc_off = ((c << address_offsets['channel'])
                                | (n << address_offsets['node'])
                                | (t['offset'] << (address_offsets['address'] + 1)))
                    if 'package' in t:
                        self.element_offsets[fqtn] = calc_off, '{}/{}/{}'.format(
                            t['package'], t['module'], t['regtype']), 'read' if t['read'] else 'write'
                    else:
                        self.element_offsets[fqtn] = calc_off, t[
                            'type'], 'read' if t['read'] else 'write'
                for t in streams:
                    # add so the length increases for each stream packet needed
                    self.schedule[c][n]['streams'].append(t)

        ### sanity checks ###

        # look for conflicting read/write pairs in a single 32-bit word
        for c, cc in self.schedule.items():
            for n, nn in cc.items():
                writes = [i for j in nn['write'] for i in j]
                reads = [i for j in nn['read'] for i in j]
                for write in writes:
                    read = write + 1 if write % 2 == 0 else write - 1
                    if read in reads:
                        print_err(
                            'Error: in channel {} node {}, write addr {} and read addr {} conflict. A read and a write cannot be in the same 32-bit word.'.format(c, n, write, read))


if __name__ == "__main__":

    auto_sched = {1: {2: [{'read': True, 'name': 'Thingy',
                           'offset': 4, 'type': 'uint16_t', 'tags': ['DEBUG']}]}}

    s = Schedule(sys.argv[1])
    s.build_schedule(auto_sched)
