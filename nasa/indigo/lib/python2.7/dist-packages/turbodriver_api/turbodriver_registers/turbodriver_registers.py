from register_builder import Register
class StatReg1(Register):
    def applyCertificate(self, val):
        return val & (~0) | 0
    def clear(self):
        self.write(self.applyCertificate(0))
    def MotorEnable(self, val=None):
        """0=disable,1=enable"""
        if val is None:
            return (self.read() >> 0) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 0)) | (val << 0)))
    def BrakeRelease(self, val=None):
        """0=engage,1=release"""
        if val is None:
            return (self.read() >> 1) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 1)) | (val << 1)))
    def ProcReset(self, val=None):
        """1=reset"""
        if val is None:
            return (self.read() >> 2) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 2)) | (val << 2)))
    def CourseMode(self, val=None):
        """0=fine,1=course"""
        if val is None:
            return (self.read() >> 3) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 3)) | (val << 3)))
    def BridgeEnable(self, val=None):
        """0=disable,1=enable"""
        if val is None:
            return (self.read() >> 4) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 4)) | (val << 4)))
    def ClearEncoder(self, val=None):
        """1=clear"""
        if val is None:
            return (self.read() >> 5) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 5)) | (val << 5)))
    def MotComSource(self, val=None):
        """0=brainstem,1=embedded"""
        if val is None:
            return (self.read() >> 6) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 6)) | (val << 6)))
    def CommutationSel(self, val=None):
        """0=6s2q,1=6s4q,2=vector"""
        if val is None:
            return (self.read() >> 7) & ((1<<2)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<2)-1) << 7)) | (val << 7)))
    def EstopOK(self, val=None):
        """1=System OK, 0=Estopped"""
        if val is None:
            return (self.read() >> 9) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 9)) | (val << 9)))
    def EstopReset(self, val=None):
        """Echo of Ctrl bit"""
        if val is None:
            return (self.read() >> 10) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 10)) | (val << 10)))
    def CurrentLimit(self, val=None):
        """1=Current Limiting"""
        if val is None:
            return (self.read() >> 11) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 11)) | (val << 11)))
    def MotorPowerDetected(self, val=None):
        """0=notDetected,1=detected"""
        if val is None:
            return (self.read() >> 13) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 13)) | (val << 13)))
    def DriveModFault(self, val=None):
        """0=clear,1=fault"""
        if val is None:
            return (self.read() >> 14) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 14)) | (val << 14)))
    def ProcAlive(self, val=None):
        """1=alive"""
        if val is None:
            return (self.read() >> 15) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 15)) | (val << 15)))
class StatReg2(Register):
    def applyCertificate(self, val):
        return val & (~0) | 0
    def clear(self):
        self.write(self.applyCertificate(0))
    def OverTempFault(self, val=None):
        """0=clear,1=fault"""
        if val is None:
            return (self.read() >> 0) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 0)) | (val << 0)))
    def SpringDefFault(self, val=None):
        """0=clear,1=fault"""
        if val is None:
            return (self.read() >> 1) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 1)) | (val << 1)))
    def ApsFault(self, val=None):
        """0=clear,1=fault"""
        if val is None:
            return (self.read() >> 2) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 2)) | (val << 2)))
    def LimitFault(self, val=None):
        """0=clear,1=fault"""
        if val is None:
            return (self.read() >> 3) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 3)) | (val << 3)))
    def TorqueLimitFault(self, val=None):
        """0=clear,1=fault"""
        if val is None:
            return (self.read() >> 4) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 4)) | (val << 4)))
    def EncDriftFault(self, val=None):
        """0=clear,1=fault"""
        if val is None:
            return (self.read() >> 5) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 5)) | (val << 5)))
    def JerkFault(self, val=None):
        """0=clear,1=fault"""
        if val is None:
            return (self.read() >> 6) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 6)) | (val << 6)))
    def JointFault(self, val=None):
        """0=clear,1=fault"""
        if val is None:
            return (self.read() >> 7) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 7)) | (val << 7)))
    def ChecksumFault(self, val=None):
        """0=clear,1=fault"""
        if val is None:
            return (self.read() >> 8) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 8)) | (val << 8)))
    def CommFault(self, val=None):
        """0=clear,1=fault"""
        if val is None:
            return (self.read() >> 9) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 9)) | (val << 9)))
    def VelocityFault(self, val=None):
        """0=clear,1=fault"""
        if val is None:
            return (self.read() >> 10) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 10)) | (val << 10)))
    def BootloaderMode(self, val=None):
        """1=In bootloader"""
        if val is None:
            return (self.read() >> 11) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 11)) | (val << 11)))
    def Aps1TolFault(self, val=None):
        """0=clear,1=fault"""
        if val is None:
            return (self.read() >> 12) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 12)) | (val << 12)))
    def Aps2TolFault(self, val=None):
        """0=clear,1=fault"""
        if val is None:
            return (self.read() >> 13) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 13)) | (val << 13)))
    def RotorInitDone(self, val=None):
        """0=false,1=true"""
        if val is None:
            return (self.read() >> 14) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 14)) | (val << 14)))
    def CalibrationMode(self, val=None):
        """0=run,1=calibrate"""
        if val is None:
            return (self.read() >> 15) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 15)) | (val << 15)))
class StatReg3(Register):
    def applyCertificate(self, val):
        return val & (~0) | 0
    def clear(self):
        self.write(self.applyCertificate(0))
    def InitAPS2toAPS1(self, val=None):
        """0=disable,1=enable"""
        if val is None:
            return (self.read() >> 0) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 0)) | (val << 0)))
    def EnableBiasCompAPS1(self, val=None):
        """0=disable,1=enable"""
        if val is None:
            return (self.read() >> 1) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 1)) | (val << 1)))
    def EnableBiasCompAPS2(self, val=None):
        """0=disable,1=enable"""
        if val is None:
            return (self.read() >> 2) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 2)) | (val << 2)))
    def BlendAngleSensors(self, val=None):
        """0=encoder only,1=blend w/ APS1"""
        if val is None:
            return (self.read() >> 3) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 3)) | (val << 3)))
    def EnableFFPosCom(self, val=None):
        """0=Raw PD, 1=PD with FF"""
        if val is None:
            return (self.read() >> 5) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 5)) | (val << 5)))
    def UseErrorDot(self, val=None):
        """0=joint vel,1=error dot"""
        if val is None:
            return (self.read() >> 6) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 6)) | (val << 6)))
    def UseJointPos(self, val=None):
        """0=motor pos,1=joint pos"""
        if val is None:
            return (self.read() >> 7) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 7)) | (val << 7)))
class CtrlReg1(Register):
    def applyCertificate(self, val):
        return val & (~61440) | 40960
    def clear(self):
        self.write(self.applyCertificate(0))
    def Mode(self, val=None):
        """Used to set value of entire lower byte."""
        if val is None:
            return (self.read() >> 0) & ((1<<8)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<8)-1) << 0)) | (val << 0)))
    def MotorEnable(self, val=None):
        """0=disable,1=enable"""
        if val is None:
            return (self.read() >> 0) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 0)) | (val << 0)))
    def BrakeRelease(self, val=None):
        """0=engage,1=release"""
        if val is None:
            return (self.read() >> 1) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 1)) | (val << 1)))
    def ProcReset(self, val=None):
        """1=reset"""
        if val is None:
            return (self.read() >> 2) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 2)) | (val << 2)))
    def CourseMode(self, val=None):
        """0=fine,1=course"""
        if val is None:
            return (self.read() >> 3) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 3)) | (val << 3)))
    def BridgeEnable(self, val=None):
        """0=disable,1=enable"""
        if val is None:
            return (self.read() >> 4) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 4)) | (val << 4)))
    def ClearEncoder(self, val=None):
        """1=clear"""
        if val is None:
            return (self.read() >> 5) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 5)) | (val << 5)))
    def MotComSource(self, val=None):
        """0=brainstem,1=embedded"""
        if val is None:
            return (self.read() >> 6) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 6)) | (val << 6)))
    def CommutationSel(self, val=None):
        """0=6s2q,1=6s4q,2=vector"""
        if val is None:
            return (self.read() >> 7) & ((1<<2)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<2)-1) << 7)) | (val << 7)))
    def EstopReset(self, val=None):
        """0-1 Rising Edge Resets Estop"""
        if val is None:
            return (self.read() >> 10) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 10)) | (val << 10)))
class CtrlReg3(Register):
    def applyCertificate(self, val):
        return val & (~61440) | 40960
    def clear(self):
        self.write(self.applyCertificate(0))
    def InitAPS2toAPS1(self, val=None):
        """0=disable,1=enable"""
        if val is None:
            return (self.read() >> 0) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 0)) | (val << 0)))
    def EnableBiasCompAPS1(self, val=None):
        """0=disable,1=enable"""
        if val is None:
            return (self.read() >> 1) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 1)) | (val << 1)))
    def EnableBiasCompAPS2(self, val=None):
        """0=disable,1=enable"""
        if val is None:
            return (self.read() >> 2) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 2)) | (val << 2)))
    def BlendAngleSensors(self, val=None):
        """0=encoder only,1=blend w/ APS1"""
        if val is None:
            return (self.read() >> 3) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 3)) | (val << 3)))
    def EnableFFPosCom(self, val=None):
        """0=Raw PD, 1=PD with FF"""
        if val is None:
            return (self.read() >> 5) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 5)) | (val << 5)))
    def UseErrorDot(self, val=None):
        """0=joint vel,1=error dot"""
        if val is None:
            return (self.read() >> 6) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 6)) | (val << 6)))
    def UseJointPos(self, val=None):
        """0=motor pos,1=joint pos"""
        if val is None:
            return (self.read() >> 7) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 7)) | (val << 7)))
class CtrlReg2(Register):
    def applyCertificate(self, val):
        return val & (~61440) | 20480
    def clear(self):
        self.write(self.applyCertificate(0))
    def DisableFaults(self, val=None):
        """1=Disable faults specified by coeff file"""
        if val is None:
            return (self.read() >> 0) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 0)) | (val << 0)))
    def ControlMode(self, val=None):
        """0=curr,1=trq,2=pos,3=vel,4=imp"""
        if val is None:
            return (self.read() >> 1) & ((1<<3)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<3)-1) << 1)) | (val << 1)))
    def SlidingDB(self, val=None):
        """0=No Smoothing, 1=Sliding DB"""
        if val is None:
            return (self.read() >> 5) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 5)) | (val << 5)))
    def ClearFault(self, val=None):
        """1=clear fault"""
        if val is None:
            return (self.read() >> 6) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 6)) | (val << 6)))
    def CalibrationMode(self, val=None):
        """0=run,1=calibrate"""
        if val is None:
            return (self.read() >> 7) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 7)) | (val << 7)))
    def StreamMode(self, val=None):
        """0=disable, 1=enable"""
        if val is None:
            return (self.read() >> 8) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 8)) | (val << 8)))
    def StreamModeReset(self, val=None):
        """1=reset"""
        if val is None:
            return (self.read() >> 9) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 9)) | (val << 9)))
    def BootloaderMode(self, val=None):
        """1=Stay in bootloader on reset"""
        if val is None:
            return (self.read() >> 10) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 10)) | (val << 10)))
class fabricCtrlReg(Register):
    def applyCertificate(self, val):
        return val & (~61440) | 20480
    def clear(self):
        self.write(self.applyCertificate(0))
    def FaultedBit(self, val=None):
        """0=disable,1=enable"""
        if val is None:
            return (self.read() >> 0) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 0)) | (val << 0)))
    def EnableCoarse(self, val=None):
        """0=disable,1=enable"""
        if val is None:
            return (self.read() >> 1) & ((1<<2)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<2)-1) << 1)) | (val << 1)))
    def SetBrake(self, val=None):
        """0=Raw PD, 1=PD with FF"""
        if val is None:
            return (self.read() >> 4) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 4)) | (val << 4)))
class BrakePWM(Register):
    def applyCertificate(self, val):
        return val & (~61440) | 49152
    def clear(self):
        self.write(self.applyCertificate(0))
    def DutyCycle(self, val=None):
        """Used to set value of entire lower byte."""
        if val is None:
            return (self.read() >> 0) & ((1<<8)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<8)-1) << 0)) | (val << 0)))

class MotCom:
    mask = 0x0E00

    def __init__(self, dev, offset):
        self.fields = [u'value']

        def f_set(val):
            dev[offset:offset + 2] = struct.pack(">H", val)
        self.f_set = f_set
        self.f_get = lambda: struct.unpack(">H", dev[offset:offset + 2])[0]
        self._type = "uint16_t"

    def __call__(self, val=None):
        if val is None:
            return self.f_get()
        self.f_set(val)

    def value(self, val=None):
        """range=[-255, 255]"""
        if val is None:
            val = self.f_get()
            if val & 0x0100:
                return -(val & 0xFF)
            return val & 0xFF
        if val < 0:
            self.f_set(MotCom.mask | 0x0100 | (abs(val) & 0xFF))
        else:
            self.f_set(MotCom.mask | (val & 0xFF))
