import pysmt
import register_builder
class TurbodriverSimAPI(object):


	def __init__(self,nodePath='',hostUri='http://localhost:11312'):
		self.client = pysmt.SMTClientFactory(hostUri)
		self.client.abstractFactories['register'] = register_builder.RegisterFactory
		if nodePath != '':
			self.nodePath = nodePath
			self.Subscribe(nodePath)

	def Subscribe(self,nodePath):
		self.nodePath = nodePath
		errorstrings = []
		try:
			self.subscriberJointAPS_Angle_Rad = self.client.getResource(nodePath + '/JointAPS_Angle_Rad')
			self.getJointAPS_Angle_RadCmd = self.subscriberJointAPS_Angle_Rad
			self.setJointAPS_Angle_RadCmd = self.subscriberJointAPS_Angle_Rad
		except Exception as e:
			errorstrings.append('exception subscribing to JointAPS_Angle_Rad: "{}"'.format(e))
		try:
			self.subscriberJointAPS_Vel_Radps = self.client.getResource(nodePath + '/JointAPS_Vel_Radps')
			self.getJointAPS_Vel_RadpsCmd = self.subscriberJointAPS_Vel_Radps
			self.setJointAPS_Vel_RadpsCmd = self.subscriberJointAPS_Vel_Radps
		except Exception as e:
			errorstrings.append('exception subscribing to JointAPS_Vel_Radps: "{}"'.format(e))
		try:
			self.subscriberJointTorque_Des_Nm = self.client.getResource(nodePath + '/JointTorque_Des_Nm')
			self.getJointTorque_Des_NmCmd = self.subscriberJointTorque_Des_Nm
			self.setJointTorque_Des_NmCmd = self.subscriberJointTorque_Des_Nm
		except Exception as e:
			errorstrings.append('exception subscribing to JointTorque_Des_Nm: "{}"'.format(e))
		try:
			self.subscriberJointTorque_Meas_Nm = self.client.getResource(nodePath + '/JointTorque_Meas_Nm')
			self.getJointTorque_Meas_NmCmd = self.subscriberJointTorque_Meas_Nm
			self.setJointTorque_Meas_NmCmd = self.subscriberJointTorque_Meas_Nm
		except Exception as e:
			errorstrings.append('exception subscribing to JointTorque_Meas_Nm: "{}"'.format(e))
		try:
			self.subscriberPosition_Des_Rad = self.client.getResource(nodePath + '/Position_Des_Rad')
			self.getPosition_Des_RadCmd = self.subscriberPosition_Des_Rad
			self.setPosition_Des_RadCmd = self.subscriberPosition_Des_Rad
		except Exception as e:
			errorstrings.append('exception subscribing to Position_Des_Rad: "{}"'.format(e))
		try:
			self.subscriberVelocity_Des_Radps = self.client.getResource(nodePath + '/Velocity_Des_Radps')
			self.getVelocity_Des_RadpsCmd = self.subscriberVelocity_Des_Radps
			self.setVelocity_Des_RadpsCmd = self.subscriberVelocity_Des_Radps
		except Exception as e:
			errorstrings.append('exception subscribing to Velocity_Des_Radps: "{}"'.format(e))
		if len(errorstrings) > 0:
			for error in errorstrings:
				print error
			print 'missing subscriber data for node: {}'.format(nodePath)

	def getNodePath(self):
		return self.nodePath
	def getEffort(self, val):
		try:
			val = self.getJointTorque_Meas_NmCmd();
		except AttributeError:
			return
		return  val

	def getEffortCommand(self, val):
		try:
			val = self.getJointTorque_Des_NmCmd();
		except AttributeError:
			return
		return  val

	def getPosition(self, val):
		try:
			val = self.getJointAPS_Angle_RadCmd();
		except AttributeError:
			return
		return  val

	def getPositionCommand(self, val):
		try:
			val = self.getPosition_Des_RadCmd();
		except AttributeError:
			return
		return  val

	def getVelocity(self, val):
		try:
			val = self.getJointAPS_Vel_RadpsCmd();
		except AttributeError:
			return
		return  val

	def getVelocityCommand(self, val):
		try:
			val = self.getVelocity_Des_RadpsCmd();
		except AttributeError:
			return
		return  val

	def setEffort(self, val):
		try:
			self.setJointTorque_Meas_NmCmd(val);
		except AttributeError:
			return
		return  val

	def setEffortCommand(self, val):
		try:
			self.setJointTorque_Des_NmCmd(val);
		except AttributeError:
			return
		return  val

	def setPosition(self, val):
		try:
			self.setJointAPS_Angle_RadCmd(val);
		except AttributeError:
			return
		return  val

	def setPositionCommand(self, val):
		try:
			self.setPosition_Des_RadCmd(val);
		except AttributeError:
			return
		return  val

	def setVelocity(self, val):
		try:
			self.setJointAPS_Vel_RadpsCmd(val);
		except AttributeError:
			return
		return  val

	def setVelocityCommand(self, val):
		try:
			self.setVelocity_Des_RadpsCmd(val);
		except AttributeError:
			return
		return  val

	def getApi(self):
		api=[]
		api.append(self.getEffort)
		api.append(self.getEffortCommand)
		api.append(self.getPosition)
		api.append(self.getPositionCommand)
		api.append(self.getVelocity)
		api.append(self.getVelocityCommand)
		api.append(self.setEffort)
		api.append(self.setEffortCommand)
		api.append(self.setPosition)
		api.append(self.setPositionCommand)
		api.append(self.setVelocity)
		api.append(self.setVelocityCommand)
		return api
