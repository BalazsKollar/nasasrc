import pysmt
import register_builder
class SimTurbodriverForceTorqueSensor(object):


	def __init__(self,nodePath='',hostUri='http://localhost:11312'):
		self.client = pysmt.SMTClientFactory(hostUri)
		self.client.abstractFactories['register'] = register_builder.RegisterFactory
		if nodePath != '':
			self.nodePath = nodePath
			self.Subscribe(nodePath)

	def Subscribe(self,nodePath):
		self.nodePath = nodePath
		errorstrings = []
		try:
			self.subscriberSix_Axis_01 = self.client.getResource(nodePath + '/Six_Axis_01')
			self.getSix_Axis_01Cmd = self.subscriberSix_Axis_01
		except Exception as e:
			errorstrings.append('exception subscribing to Six_Axis_01: "{}"'.format(e))
		try:
			self.subscriberSix_Axis_02 = self.client.getResource(nodePath + '/Six_Axis_02')
			self.getSix_Axis_02Cmd = self.subscriberSix_Axis_02
		except Exception as e:
			errorstrings.append('exception subscribing to Six_Axis_02: "{}"'.format(e))
		try:
			self.subscriberSix_Axis_03 = self.client.getResource(nodePath + '/Six_Axis_03')
			self.getSix_Axis_03Cmd = self.subscriberSix_Axis_03
		except Exception as e:
			errorstrings.append('exception subscribing to Six_Axis_03: "{}"'.format(e))
		try:
			self.subscriberSix_Axis_04 = self.client.getResource(nodePath + '/Six_Axis_04')
			self.getSix_Axis_04Cmd = self.subscriberSix_Axis_04
		except Exception as e:
			errorstrings.append('exception subscribing to Six_Axis_04: "{}"'.format(e))
		try:
			self.subscriberSix_Axis_05 = self.client.getResource(nodePath + '/Six_Axis_05')
			self.getSix_Axis_05Cmd = self.subscriberSix_Axis_05
		except Exception as e:
			errorstrings.append('exception subscribing to Six_Axis_05: "{}"'.format(e))
		try:
			self.subscriberSix_Axis_06 = self.client.getResource(nodePath + '/Six_Axis_06')
			self.getSix_Axis_06Cmd = self.subscriberSix_Axis_06
		except Exception as e:
			errorstrings.append('exception subscribing to Six_Axis_06: "{}"'.format(e))
		if len(errorstrings) > 0:
			for error in errorstrings:
				print error
			print 'missing subscriber data for node: {}'.format(nodePath)

	def getNodePath(self):
		return self.nodePath
	def getSixAxis01(self, val):
		try:
			val = self.getSix_Axis_01Cmd();
		except AttributeError:
			return
		return  val

	def getSixAxis02(self, val):
		try:
			val = self.getSix_Axis_02Cmd();
		except AttributeError:
			return
		return  val

	def getSixAxis03(self, val):
		try:
			val = self.getSix_Axis_03Cmd();
		except AttributeError:
			return
		return  val

	def getSixAxis04(self, val):
		try:
			val = self.getSix_Axis_04Cmd();
		except AttributeError:
			return
		return  val

	def getSixAxis05(self, val):
		try:
			val = self.getSix_Axis_05Cmd();
		except AttributeError:
			return
		return  val

	def getSixAxis06(self, val):
		try:
			val = self.getSix_Axis_06Cmd();
		except AttributeError:
			return
		return  val

	def getApi(self):
		api=[]
		api.append(self.getSixAxis01)
		api.append(self.getSixAxis02)
		api.append(self.getSixAxis03)
		api.append(self.getSixAxis04)
		api.append(self.getSixAxis05)
		api.append(self.getSixAxis06)
		return api
