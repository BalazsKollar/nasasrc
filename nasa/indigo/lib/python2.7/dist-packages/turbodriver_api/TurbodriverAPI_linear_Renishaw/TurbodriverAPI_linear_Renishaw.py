import pysmt
import register_builder
class TurbodriverAPI_linear_Renishaw(object):


	def __init__(self,nodePath='',hostUri='http://localhost:11312'):
		self.client = pysmt.SMTClientFactory(hostUri)
		self.client.abstractFactories['register'] = register_builder.RegisterFactory
		if nodePath != '':
			self.nodePath = nodePath
			self.Subscribe(nodePath)

	def Subscribe(self,nodePath):
		self.nodePath = nodePath
		errorstrings = []
		try:
			self.subscriberBridgeTemp_C = self.client.getResource(nodePath + '/BridgeTemp_C')
			self.getBridgeTemp_CCmd = self.subscriberBridgeTemp_C
		except Exception as e:
			errorstrings.append('exception subscribing to BridgeTemp_C: "{}"'.format(e))
		try:
			self.subscriberCtrlReg1 = self.client.getResource(nodePath + '/CtrlReg1')
			self.getCtrlReg1MotComSourceCmd = self.subscriberCtrlReg1.MotComSource
			self.getCtrlReg1MotorEnableCmd = self.subscriberCtrlReg1.MotorEnable
			self.setCtrlReg1BrakeReleaseCmd = self.subscriberCtrlReg1.BrakeRelease
			self.setCtrlReg1BridgeEnableCmd = self.subscriberCtrlReg1.BridgeEnable
			self.setCtrlReg1ClearEncoderCmd = self.subscriberCtrlReg1.ClearEncoder
			self.setCtrlReg1CommutationSelCmd = self.subscriberCtrlReg1.CommutationSel
			self.setCtrlReg1EstopResetCmd = self.subscriberCtrlReg1.EstopReset
			self.setCtrlReg1MotComSourceCmd = self.subscriberCtrlReg1.MotComSource
			self.setCtrlReg1MotorEnableCmd = self.subscriberCtrlReg1.MotorEnable
			self.setCtrlReg1ProcResetCmd = self.subscriberCtrlReg1.ProcReset
		except Exception as e:
			errorstrings.append('exception subscribing to CtrlReg1: "{}"'.format(e))
		try:
			self.subscriberCtrlReg2 = self.client.getResource(nodePath + '/CtrlReg2')
			self.getCtrlReg2ClearFaultCmd = self.subscriberCtrlReg2.ClearFault
			self.getCtrlReg2ControlModeCmd = self.subscriberCtrlReg2.ControlMode
			self.getCtrlReg2DisableFaultsCmd = self.subscriberCtrlReg2.DisableFaults
			self.setCtrlReg2BootloaderModeCmd = self.subscriberCtrlReg2.BootloaderMode
			self.setCtrlReg2CalibrationModeCmd = self.subscriberCtrlReg2.CalibrationMode
			self.setCtrlReg2ClearFaultCmd = self.subscriberCtrlReg2.ClearFault
			self.setCtrlReg2ControlModeCmd = self.subscriberCtrlReg2.ControlMode
			self.setCtrlReg2DisableFaultsCmd = self.subscriberCtrlReg2.DisableFaults
			self.setCtrlReg2StreamModeCmd = self.subscriberCtrlReg2.StreamMode
			self.setCtrlReg2StreamModeResetCmd = self.subscriberCtrlReg2.StreamModeReset
		except Exception as e:
			errorstrings.append('exception subscribing to CtrlReg2: "{}"'.format(e))
		try:
			self.subscriberEncPosOffsetMode = self.client.getResource(nodePath + '/EncPosOffsetMode')
			self.setEncPosOffsetModeCmd = self.subscriberEncPosOffsetMode
		except Exception as e:
			errorstrings.append('exception subscribing to EncPosOffsetMode: "{}"'.format(e))
		try:
			self.subscriberIncEnc_Pos_m = self.client.getResource(nodePath + '/IncEnc_Pos_m')
			self.getIncEnc_Pos_mCmd = self.subscriberIncEnc_Pos_m
		except Exception as e:
			errorstrings.append('exception subscribing to IncEnc_Pos_m: "{}"'.format(e))
		try:
			self.subscriberJointAPS_Angle_Rad = self.client.getResource(nodePath + '/JointAPS_Angle_Rad')
			self.getJointAPS_Angle_RadCmd = self.subscriberJointAPS_Angle_Rad
		except Exception as e:
			errorstrings.append('exception subscribing to JointAPS_Angle_Rad: "{}"'.format(e))
		try:
			self.subscriberJointForce_Des_N = self.client.getResource(nodePath + '/JointForce_Des_N')
			self.getJointForce_Des_NCmd = self.subscriberJointForce_Des_N
			self.setJointForce_Des_NCmd = self.subscriberJointForce_Des_N
		except Exception as e:
			errorstrings.append('exception subscribing to JointForce_Des_N: "{}"'.format(e))
		try:
			self.subscriberJointForce_Meas_N = self.client.getResource(nodePath + '/JointForce_Meas_N')
			self.getJointForce_Meas_NCmd = self.subscriberJointForce_Meas_N
		except Exception as e:
			errorstrings.append('exception subscribing to JointForce_Meas_N: "{}"'.format(e))
		try:
			self.subscriberLinear_Vel_mps = self.client.getResource(nodePath + '/Linear_Vel_mps')
			self.getLinear_Vel_mpsCmd = self.subscriberLinear_Vel_mps
		except Exception as e:
			errorstrings.append('exception subscribing to Linear_Vel_mps: "{}"'.format(e))
		try:
			self.subscriberLogicCardTemp_C = self.client.getResource(nodePath + '/LogicCardTemp_C')
			self.getLogicCardTemp_CCmd = self.subscriberLogicCardTemp_C
		except Exception as e:
			errorstrings.append('exception subscribing to LogicCardTemp_C: "{}"'.format(e))
		try:
			self.subscriberMotorTemp_C = self.client.getResource(nodePath + '/MotorTemp_C')
			self.getMotorTemp_CCmd = self.subscriberMotorTemp_C
		except Exception as e:
			errorstrings.append('exception subscribing to MotorTemp_C: "{}"'.format(e))
		try:
			self.subscriberPosition_Des_m = self.client.getResource(nodePath + '/Position_Des_m')
			self.getPosition_Des_mCmd = self.subscriberPosition_Des_m
			self.setPosition_Des_mCmd = self.subscriberPosition_Des_m
		except Exception as e:
			errorstrings.append('exception subscribing to Position_Des_m: "{}"'.format(e))
		try:
			self.subscriberProc_HeartBeat = self.client.getResource(nodePath + '/Proc_HeartBeat')
			self.getProc_HeartBeatCmd = self.subscriberProc_HeartBeat
		except Exception as e:
			errorstrings.append('exception subscribing to Proc_HeartBeat: "{}"'.format(e))
		try:
			self.subscriberStatReg1 = self.client.getResource(nodePath + '/StatReg1')
			self.getStatReg1BrakeReleaseCmd = self.subscriberStatReg1.BrakeRelease
			self.getStatReg1BridgeEnableCmd = self.subscriberStatReg1.BridgeEnable
			self.getStatReg1ClearEncoderCmd = self.subscriberStatReg1.ClearEncoder
			self.getStatReg1CommutationSelCmd = self.subscriberStatReg1.CommutationSel
			self.getStatReg1DriveModFaultCmd = self.subscriberStatReg1.DriveModFault
			self.getStatReg1EstopOKCmd = self.subscriberStatReg1.EstopOK
			self.getStatReg1MotComSourceCmd = self.subscriberStatReg1.MotComSource
			self.getStatReg1MotorEnableCmd = self.subscriberStatReg1.MotorEnable
			self.getStatReg1MotorPowerDetectedCmd = self.subscriberStatReg1.MotorPowerDetected
			self.getStatReg1ProcAliveCmd = self.subscriberStatReg1.ProcAlive
			self.getStatReg1ProcResetCmd = self.subscriberStatReg1.ProcReset
			self.setStatReg1ProcResetCmd = self.subscriberStatReg1.ProcReset
		except Exception as e:
			errorstrings.append('exception subscribing to StatReg1: "{}"'.format(e))
		try:
			self.subscriberStatReg2 = self.client.getResource(nodePath + '/StatReg2')
			self.getStatReg2Aps1TolFaultCmd = self.subscriberStatReg2.Aps1TolFault
			self.getStatReg2Aps2TolFaultCmd = self.subscriberStatReg2.Aps2TolFault
			self.getStatReg2ApsFaultCmd = self.subscriberStatReg2.ApsFault
			self.getStatReg2BootloaderModeCmd = self.subscriberStatReg2.BootloaderMode
			self.getStatReg2CalibrationModeCmd = self.subscriberStatReg2.CalibrationMode
			self.getStatReg2ChecksumFaultCmd = self.subscriberStatReg2.ChecksumFault
			self.getStatReg2CommFaultCmd = self.subscriberStatReg2.CommFault
			self.getStatReg2EncDriftFaultCmd = self.subscriberStatReg2.EncDriftFault
			self.getStatReg2JerkFaultCmd = self.subscriberStatReg2.JerkFault
			self.getStatReg2JointFaultCmd = self.subscriberStatReg2.JointFault
			self.getStatReg2LimitFaultCmd = self.subscriberStatReg2.LimitFault
			self.getStatReg2OverTempFaultCmd = self.subscriberStatReg2.OverTempFault
			self.getStatReg2RotorInitDoneCmd = self.subscriberStatReg2.RotorInitDone
			self.getStatReg2SpringDefFaultCmd = self.subscriberStatReg2.SpringDefFault
			self.getStatReg2TorqueLimitFaultCmd = self.subscriberStatReg2.TorqueLimitFault
			self.getStatReg2VelocityFaultCmd = self.subscriberStatReg2.VelocityFault
		except Exception as e:
			errorstrings.append('exception subscribing to StatReg2: "{}"'.format(e))
		try:
			self.subscriberVelocity_Des_mps = self.client.getResource(nodePath + '/Velocity_Des_mps')
			self.getVelocity_Des_mpsCmd = self.subscriberVelocity_Des_mps
			self.setVelocity_Des_mpsCmd = self.subscriberVelocity_Des_mps
		except Exception as e:
			errorstrings.append('exception subscribing to Velocity_Des_mps: "{}"'.format(e))
		if len(errorstrings) > 0:
			for error in errorstrings:
				print error
			print 'missing subscriber data for node: {}'.format(nodePath)

	def getNodePath(self):
		return self.nodePath
	def clearBootloaderMode(self):
		try:
			self.setCtrlReg2BootloaderModeCmd(0);
		except AttributeError:
			return
		return 

	def clearFault(self):
		try:
			self.setCtrlReg1MotorEnableCmd(0);
			self.setCtrlReg1BrakeReleaseCmd(0);
			self.setCtrlReg1ProcResetCmd(0);
			self.setCtrlReg1BridgeEnableCmd(1);
			self.setCtrlReg1ClearEncoderCmd(0);
			self.setCtrlReg1MotComSourceCmd(0);
			self.setCtrlReg1EstopResetCmd(1);
			self.setCtrlReg2ControlModeCmd(0);
			self.setCtrlReg2ClearFaultCmd(1);
			self.setCtrlReg2CalibrationModeCmd(0);
			self.setCtrlReg2StreamModeCmd(0);
			self.setCtrlReg2StreamModeResetCmd(0);
			self.setCtrlReg2BootloaderModeCmd(0);
		except AttributeError:
			return
		return 

	def clearProcReset(self):
		try:
			self.setStatReg1ProcResetCmd(0);
		except AttributeError:
			return
		return 

	def deltaHysteresis(self):
		try:
			self.setCtrlReg1BridgeEnableCmd(1);
			self.setCtrlReg1MotComSourceCmd(1);
			self.setCtrlReg1CommutationSelCmd(3);
		except AttributeError:
			return
		return 

	def disableFaults(self):
		try:
			self.setCtrlReg2DisableFaultsCmd(1);
		except AttributeError:
			return
		return 

	def disableMotor(self):
		try:
			self.setCtrlReg1MotorEnableCmd(0);
		except AttributeError:
			return
		return 

	def enableFaults(self):
		try:
			self.setCtrlReg2DisableFaultsCmd(0);
		except AttributeError:
			return
		return 

	def enableMotor(self):
		try:
			self.setCtrlReg1MotorEnableCmd(1);
		except AttributeError:
			return
		return 

	def encoderPosNormalMode(self):
		try:
			self.setEncPosOffsetModeCmd(0);
		except AttributeError:
			return
		return 

	def encoderPosOffsetMode(self):
		try:
			self.setEncPosOffsetModeCmd(1);
		except AttributeError:
			return
		return 

	def enterCalMode(self):
		try:
			self.setCtrlReg2CalibrationModeCmd(1);
		except AttributeError:
			return
		return 

	def exitCalMode(self):
		try:
			self.setCtrlReg2CalibrationModeCmd(0);
		except AttributeError:
			return
		return 

	def getAps1TolFault(self, val):
		try:
			val = self.getStatReg2Aps1TolFaultCmd();
		except AttributeError:
			return
		return  val

	def getAps2TolFault(self, val):
		try:
			val = self.getStatReg2Aps2TolFaultCmd();
		except AttributeError:
			return
		return  val

	def getApsFault(self, val):
		try:
			val = self.getStatReg2ApsFaultCmd();
		except AttributeError:
			return
		return  val

	def getBootloaderMode(self, val):
		try:
			val = self.getStatReg2BootloaderModeCmd();
		except AttributeError:
			return
		return  val

	def getBrakeRelease(self, val):
		try:
			val = self.getStatReg1BrakeReleaseCmd();
		except AttributeError:
			return
		return  val

	def getBridgeEnable(self, val):
		try:
			val = self.getStatReg1BridgeEnableCmd();
		except AttributeError:
			return
		return  val

	def getBridgeTemp_C(self, val):
		try:
			val = self.getBridgeTemp_CCmd();
		except AttributeError:
			return
		return  val

	def getCalibrationMode(self, val):
		try:
			val = self.getStatReg2CalibrationModeCmd();
		except AttributeError:
			return
		return  val

	def getChecksumFault(self, val):
		try:
			val = self.getStatReg2ChecksumFaultCmd();
		except AttributeError:
			return
		return  val

	def getClearEncoder(self, val):
		try:
			val = self.getStatReg1ClearEncoderCmd();
		except AttributeError:
			return
		return  val

	def getClearFaultCmd(self, val):
		try:
			val = self.getCtrlReg2ClearFaultCmd();
		except AttributeError:
			return
		return  val

	def getCommFault(self, val):
		try:
			val = self.getStatReg2CommFaultCmd();
		except AttributeError:
			return
		return  val

	def getCommutationSel(self, val):
		try:
			val = self.getStatReg1CommutationSelCmd();
		except AttributeError:
			return
		return  val

	def getControlModeCmd(self, val):
		try:
			val = self.getCtrlReg2ControlModeCmd();
		except AttributeError:
			return
		return  val

	def getDisableFaults(self, val):
		try:
			val = self.getCtrlReg2DisableFaultsCmd();
		except AttributeError:
			return
		return  val

	def getDriveModFault(self, val):
		try:
			val = self.getStatReg1DriveModFaultCmd();
		except AttributeError:
			return
		return  val

	def getEffort(self, val):
		try:
			val = self.getJointForce_Meas_NCmd();
		except AttributeError:
			return
		return  val

	def getEffortCommand(self, val):
		try:
			val = self.getJointForce_Des_NCmd();
		except AttributeError:
			return
		return  val

	def getEncDriftFault(self, val):
		try:
			val = self.getStatReg2EncDriftFaultCmd();
		except AttributeError:
			return
		return  val

	def getEncoderPosition(self, val):
		try:
			val = self.getIncEnc_Pos_mCmd();
		except AttributeError:
			return
		return  val

	def getEstopOK(self, val):
		try:
			val = self.getStatReg1EstopOKCmd();
		except AttributeError:
			return
		return  val

	def getJerkFault(self, val):
		try:
			val = self.getStatReg2JerkFaultCmd();
		except AttributeError:
			return
		return  val

	def getJointFault(self, val):
		try:
			val = self.getStatReg2JointFaultCmd();
		except AttributeError:
			return
		return  val

	def getLimitFault(self, val):
		try:
			val = self.getStatReg2LimitFaultCmd();
		except AttributeError:
			return
		return  val

	def getLogicCardTemp_C(self, val):
		try:
			val = self.getLogicCardTemp_CCmd();
		except AttributeError:
			return
		return  val

	def getMotComSource(self, val):
		try:
			val = self.getStatReg1MotComSourceCmd();
		except AttributeError:
			return
		return  val

	def getMotComSourceCmd(self, val):
		try:
			val = self.getCtrlReg1MotComSourceCmd();
		except AttributeError:
			return
		return  val

	def getMotorEnable(self, val):
		try:
			val = self.getStatReg1MotorEnableCmd();
		except AttributeError:
			return
		return  val

	def getMotorEnableCmd(self, val):
		try:
			val = self.getCtrlReg1MotorEnableCmd();
		except AttributeError:
			return
		return  val

	def getMotorPowerDetected(self, val):
		try:
			val = self.getStatReg1MotorPowerDetectedCmd();
		except AttributeError:
			return
		return  val

	def getMotorTemp_C(self, val):
		try:
			val = self.getMotorTemp_CCmd();
		except AttributeError:
			return
		return  val

	def getOverTempFault(self, val):
		try:
			val = self.getStatReg2OverTempFaultCmd();
		except AttributeError:
			return
		return  val

	def getPosition(self, val):
		try:
			val = self.getJointAPS_Angle_RadCmd();
		except AttributeError:
			return
		return  val

	def getPositionCommand(self, val):
		try:
			val = self.getPosition_Des_mCmd();
		except AttributeError:
			return
		return  val

	def getProcAlive(self, val):
		try:
			val = self.getStatReg1ProcAliveCmd();
		except AttributeError:
			return
		return  val

	def getProcHeartbeat(self, val):
		try:
			val = self.getProc_HeartBeatCmd();
		except AttributeError:
			return
		return  val

	def getProcReset(self, val):
		try:
			val = self.getStatReg1ProcResetCmd();
		except AttributeError:
			return
		return  val

	def getRotorInitDone(self, val):
		try:
			val = self.getStatReg2RotorInitDoneCmd();
		except AttributeError:
			return
		return  val

	def getSpringDefFault(self, val):
		try:
			val = self.getStatReg2SpringDefFaultCmd();
		except AttributeError:
			return
		return  val

	def getTorqueLimitFault(self, val):
		try:
			val = self.getStatReg2TorqueLimitFaultCmd();
		except AttributeError:
			return
		return  val

	def getVelocity(self, val):
		try:
			val = self.getLinear_Vel_mpsCmd();
		except AttributeError:
			return
		return  val

	def getVelocityCommand(self, val):
		try:
			val = self.getVelocity_Des_mpsCmd();
		except AttributeError:
			return
		return  val

	def getVelocityFault(self, val):
		try:
			val = self.getStatReg2VelocityFaultCmd();
		except AttributeError:
			return
		return  val

	def impedanceMode(self):
		try:
			self.setCtrlReg1MotorEnableCmd(1);
			self.setCtrlReg1BrakeReleaseCmd(1);
			self.setCtrlReg1ProcResetCmd(0);
			self.setCtrlReg1BridgeEnableCmd(1);
			self.setCtrlReg1ClearEncoderCmd(0);
			self.setCtrlReg1MotComSourceCmd(1);
			self.setCtrlReg1CommutationSelCmd(3);
			self.setCtrlReg1EstopResetCmd(0);
			self.setCtrlReg2ControlModeCmd(4);
			self.setCtrlReg2ClearFaultCmd(0);
			self.setCtrlReg2CalibrationModeCmd(0);
			self.setCtrlReg2StreamModeCmd(0);
			self.setCtrlReg2StreamModeResetCmd(0);
			self.setCtrlReg2BootloaderModeCmd(0);
		except AttributeError:
			return
		return 

	def park(self):
		try:
			self.setCtrlReg1MotorEnableCmd(0);
			self.setCtrlReg1BrakeReleaseCmd(0);
			self.setCtrlReg1ProcResetCmd(0);
			self.setCtrlReg1BridgeEnableCmd(1);
			self.setCtrlReg1ClearEncoderCmd(0);
			self.setCtrlReg1EstopResetCmd(0);
			self.setCtrlReg2ControlModeCmd(0);
			self.setCtrlReg2ClearFaultCmd(0);
			self.setCtrlReg2CalibrationModeCmd(0);
			self.setCtrlReg2StreamModeCmd(0);
			self.setCtrlReg2StreamModeResetCmd(0);
			self.setCtrlReg2BootloaderModeCmd(0);
		except AttributeError:
			return
		return 

	def positionMode(self):
		try:
			self.setCtrlReg1MotorEnableCmd(1);
			self.setCtrlReg1BrakeReleaseCmd(1);
			self.setCtrlReg1ProcResetCmd(0);
			self.setCtrlReg1BridgeEnableCmd(1);
			self.setCtrlReg1ClearEncoderCmd(0);
			self.setCtrlReg1MotComSourceCmd(1);
			self.setCtrlReg1CommutationSelCmd(3);
			self.setCtrlReg1EstopResetCmd(0);
			self.setCtrlReg2ControlModeCmd(2);
			self.setCtrlReg2ClearFaultCmd(0);
			self.setCtrlReg2CalibrationModeCmd(0);
			self.setCtrlReg2StreamModeCmd(0);
			self.setCtrlReg2StreamModeResetCmd(0);
			self.setCtrlReg2BootloaderModeCmd(0);
		except AttributeError:
			return
		return 

	def setBootloaderMode(self):
		try:
			self.setCtrlReg2BootloaderModeCmd(1);
		except AttributeError:
			return
		return 

	def setEffortCommand(self, val):
		try:
			self.setJointForce_Des_NCmd(val);
		except AttributeError:
			return
		return  val

	def setPositionCommand(self, val):
		try:
			self.setPosition_Des_mCmd(val);
		except AttributeError:
			return
		return  val

	def setProcReset(self):
		try:
			self.setStatReg1ProcResetCmd(1);
		except AttributeError:
			return
		return 

	def setVelocityCommand(self, val):
		try:
			self.setVelocity_Des_mpsCmd(val);
		except AttributeError:
			return
		return  val

	def spaceVector(self):
		try:
			self.setCtrlReg1BridgeEnableCmd(1);
			self.setCtrlReg1MotComSourceCmd(1);
			self.setCtrlReg1CommutationSelCmd(2);
		except AttributeError:
			return
		return 

	def torqueMode(self):
		try:
			self.setCtrlReg1MotorEnableCmd(1);
			self.setCtrlReg1BrakeReleaseCmd(1);
			self.setCtrlReg1ProcResetCmd(0);
			self.setCtrlReg1BridgeEnableCmd(1);
			self.setCtrlReg1ClearEncoderCmd(0);
			self.setCtrlReg1MotComSourceCmd(1);
			self.setCtrlReg1CommutationSelCmd(3);
			self.setCtrlReg1EstopResetCmd(0);
			self.setCtrlReg2ControlModeCmd(1);
			self.setCtrlReg2ClearFaultCmd(0);
			self.setCtrlReg2CalibrationModeCmd(0);
			self.setCtrlReg2StreamModeCmd(0);
			self.setCtrlReg2StreamModeResetCmd(0);
			self.setCtrlReg2BootloaderModeCmd(0);
		except AttributeError:
			return
		return 

	def zeroEffortCommand(self):
		try:
			self.setJointForce_Des_NCmd(0);
		except AttributeError:
			return
		return 

	def getApi(self):
		api=[]
		api.append(self.clearBootloaderMode)
		api.append(self.clearFault)
		api.append(self.clearProcReset)
		api.append(self.deltaHysteresis)
		api.append(self.disableFaults)
		api.append(self.disableMotor)
		api.append(self.enableFaults)
		api.append(self.enableMotor)
		api.append(self.encoderPosNormalMode)
		api.append(self.encoderPosOffsetMode)
		api.append(self.enterCalMode)
		api.append(self.exitCalMode)
		api.append(self.getAps1TolFault)
		api.append(self.getAps2TolFault)
		api.append(self.getApsFault)
		api.append(self.getBootloaderMode)
		api.append(self.getBrakeRelease)
		api.append(self.getBridgeEnable)
		api.append(self.getBridgeTemp_C)
		api.append(self.getCalibrationMode)
		api.append(self.getChecksumFault)
		api.append(self.getClearEncoder)
		api.append(self.getClearFaultCmd)
		api.append(self.getCommFault)
		api.append(self.getCommutationSel)
		api.append(self.getControlModeCmd)
		api.append(self.getDisableFaults)
		api.append(self.getDriveModFault)
		api.append(self.getEffort)
		api.append(self.getEffortCommand)
		api.append(self.getEncDriftFault)
		api.append(self.getEncoderPosition)
		api.append(self.getEstopOK)
		api.append(self.getJerkFault)
		api.append(self.getJointFault)
		api.append(self.getLimitFault)
		api.append(self.getLogicCardTemp_C)
		api.append(self.getMotComSource)
		api.append(self.getMotComSourceCmd)
		api.append(self.getMotorEnable)
		api.append(self.getMotorEnableCmd)
		api.append(self.getMotorPowerDetected)
		api.append(self.getMotorTemp_C)
		api.append(self.getOverTempFault)
		api.append(self.getPosition)
		api.append(self.getPositionCommand)
		api.append(self.getProcAlive)
		api.append(self.getProcHeartbeat)
		api.append(self.getProcReset)
		api.append(self.getRotorInitDone)
		api.append(self.getSpringDefFault)
		api.append(self.getTorqueLimitFault)
		api.append(self.getVelocity)
		api.append(self.getVelocityCommand)
		api.append(self.getVelocityFault)
		api.append(self.impedanceMode)
		api.append(self.park)
		api.append(self.positionMode)
		api.append(self.setBootloaderMode)
		api.append(self.setEffortCommand)
		api.append(self.setPositionCommand)
		api.append(self.setProcReset)
		api.append(self.setVelocityCommand)
		api.append(self.spaceVector)
		api.append(self.torqueMode)
		api.append(self.zeroEffortCommand)
		return api
