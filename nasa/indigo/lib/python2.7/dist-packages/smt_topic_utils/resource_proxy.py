import logging

import pysmt
from pysmt import SMTClientException
from .topic_utils import getTopicInfo

logger = logging.getLogger(__name__)

class FieldProxy(object):
    def __init__(self, fieldObj, name, parent):
        self.topic = name
        self.parent = parent
        self.typename = 'field'
        self.fieldObj = fieldObj

    def __str__(self):
        return str(self.fieldObj())

    def __call__(self, val=None):
        if val is None:
            return self.fieldObj()
        else:
            self.fieldObj(val)

class ResourceProxy(object):
    """
    The resource proxy creates resource objects for all topics listed in smtcore.
    For abstract topics, proxy objects are created for each field for that resource.
    """

    def __init__(self, filters={'name':'.*'}, client=None):
        self.topicFilters = filters
        self.topicInfo = {}
        self.resourceDict = {}
        if client is None:
            client = pysmt.SMTClientFactory()
        self.client = client

    def getResource(self, topic):
        return self.resourceDict.get(topic,None)

    def getResourceDict(self):
        """
        Get a dictionary of resources.  The resource dictionary is 'topic name' : resource object
        """
        return self.resourceDict

    def getTopicInfo(self, topic=None):
        """
        Get the resource info by topic name.

        :param topic: Name of the topic.  If None, all the info is returned
        :return: Dictionary of topic info.  If the topic does not exist, None is returned
        """
        if topic:
            return self.topicInfo.get(topic,None)
        else:
            return self.topicInfo

    def reloadTopics(self, leaf_only=False):
        """
        Rebuild the local resources

        :param leaf_only: If True, for topics with fields, only the fields will be added, and not the 'parent' resource
        """
        self.topicInfo = {}
        self.resourceDict = {}
        info = getTopicInfo(self.topicFilters, client=self.client)
        for topic in info:
            topicName = topic['name']
            topicType = topic['type']

            self.topicInfo[topicName] = topic

            try:
                res = self.client.getResource(topicName)
                if leaf_only:
                    if len(res.getFieldNames()) == 0:
                        self.resourceDict[topicName] = res
                else:
                    self.resourceDict[topicName] = res

                # if topic is abstract, add field proxies
                if topicType == 'abstract':
                    for fieldName in res.getFieldNames():
                        fieldObj = res.getField(fieldName)
                        fieldPath = '/'.join([topicName,fieldName])
                        fieldProxy = FieldProxy(fieldObj, fieldPath, topicName)
                        self.resourceDict[fieldPath] = fieldProxy

            except SMTClientException as e:
                logger.warn(e)
