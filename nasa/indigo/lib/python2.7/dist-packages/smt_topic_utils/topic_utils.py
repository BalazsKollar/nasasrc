import re

import pysmt

SMT_FILTER_ATTRIBUTES = [
    'name',
    'type',
    'abstractType',
    'size',
    'endian'
]

def getTopicInfo(filters={'name':'.*'}, client=None):
    """
    Return topic info for all topics whose attributes match regex filters.

    The "filters" parameter is a dictionary of topic attributes to regular expressions:
     Example:
        This filter will return all topics whose name contains 'APS' and whose type is 'float64'
        filters = {
            'name' : '.*APS',
            'type' : 'float64'
        }

    All the filters must match for a topic to be returned.  If an attribute is invalid, the topic is
    not considered to match.

    :param filters: Dictionary of attribute name to regex filter string
    :param client: Custom SMTClient.  Useful if you aren't using the default host URI
    :return: list of topic info (dictionaries) that match all the filters
    """

    attrib_regexes = {}

    for attrib, expression in filters.items():
        if attrib not in SMT_FILTER_ATTRIBUTES:
            raise KeyError('Trying to filter on an invalid attribute name "{}"'.format(attrib))
        attrib_regexes[attrib] = re.compile(expression)

    if client is None:
        client = pysmt.SMTClientFactory()

    all_topics = client.listTopics()
    return_topics = []
    for topic in all_topics:
        topic_match = False
        for attrib, regex in attrib_regexes.items():
            try:
                if regex.match(topic[attrib]):
                    topic_match = True
                else:
                    topic_match = False
                    break
            except KeyError:
                topic_match = False
                break
        if topic_match:
            return_topics.append(topic)
    return return_topics

def getTopicNames(filters={'name':'.*'}, client=None):
    """
    Return all topic names whose attributes match regex filters.

    This function works similar to getTopicInfo.  It simply returns the names of the topics,
    as opposed to all of the topic info.

    :param filters: Dictionary of attribute name to regex filter
    :param client: Custom SMTClient.  Useful if you aren't using the default host URI
    :return: list of topic names that match all the filters
    """

    topics = getTopicInfo(filters,client)

    return_names = []
    for topic in topics:
        return_names.append(topic['name'])

    return return_names
