#!/usr/bin/python

import xml.parsers.expat
import sys
import StringIO

# mimic standard print but send it to stderr
def print_err(*args):
    sys.stderr.write(' '.join(map(str,args)) + '\n')
    sys.exit(1)

class Tab():
    def __init__(self):
        self.tab = ['']
        self.out = StringIO.StringIO()
    def add(self):
        self.tab.extend([' ']*4)
    def rem(self):
        self.tab = self.tab[:-4]
    def write(self, args=''):
        self.out.write(''.join(self.tab) + args + '\n')

class RegisterBuilder():
    def __init__(self, register_file):
        self.register_file = register_file
        self.parser = xml.parsers.expat.ParserCreate()
        self.registers = {}
        self.path = []
        self.current_id = None

    def start_element(self, name, attrs):
        self.path.append(name)

        if self.path == ['registers', 'register']:
            self.in_register = True
            for i in ['id', 'size']:
                if not i in attrs.keys():
                    print_err('Error: generate tag missing attr "{}" on line {}'.format(i, self.parser.CurrentLineNumber))
            self.registers[attrs['id']] = {'size':int(attrs['size'], 0)}
            self.current_id = attrs['id']

        if self.path == ['registers', 'register', 'certificate']:
            self.registers[self.current_id]['certificate'] = dict([(i,int(attrs[i],0)) for i in ['value', 'mask']])

        if self.path == ['registers', 'register', 'field']:
            if not 'fields' in self.registers[self.current_id]:
                self.registers[self.current_id]['fields'] = []
            tmpdict = dict()
            tmpdict['id'] = attrs['id']
            tmpdict['index'] = int(attrs['index'],0)
            tmpdict['size'] = int(attrs['size'],0)
            tmpdict['info'] = attrs['info']
            self.registers[self.current_id]['fields'].append(tmpdict)

    def end_element(self, name):
        if name == self.path[-1]:
            self.path = self.path[:-1]

    def parse(self):
        self.parser.StartElementHandler = self.start_element
        self.parser.EndElementHandler = self.end_element

        with open(self.register_file) as f:
            register_xml = f.read()

        try:
            # parse the registers
            self.parser.Parse(register_xml, 1)
        except xml.parsers.expat.ExpatError, e:
            print_err('Error:', e)

    def generate_cpp(self, name):
        tab_h = Tab()
        tab_c = Tab()

        guardname = '_' +name.upper() + '_'

        tab_h.write('#ifndef {}'.format(guardname))
        tab_h.write('#define {}'.format(guardname))
        tab_h.write()
        tab_h.write('#include "register.hpp"')
        tab_h.write()
        tab_h.write('#ifdef __cplusplus')   # disable C++ name mangling to enable dynamic loading
        tab_h.write('extern "C" {')
        tab_h.write('#endif')
        tab_h.write()
        tab_h.write('#include <stdint.h>')

        tab_c.write('#include "{}.hpp"'.format(name))

        for reg, data in self.registers.items():
            if data['size'] > 16:
                reg_type = 'uint32_t'
            else:
                reg_type = 'uint16_t'

            if 'certificate' in data:
                tab_h.write('{0} {1}_applyCertificate({0} val);'.format(reg_type, reg))

                tab_c.write('{0} {1}_applyCertificate({0} val)'.format(reg_type, reg))
                tab_c.write('{')
                tab_c.add()
                tab_c.write('return val & (~{}U) | {}U;'.format(data['certificate']['mask'], data['certificate']['value']))
                tab_c.rem()
                tab_c.write('}')

            tab_h.rem()
            tab_h.write()
            tab_h.write('//')
            tab_h.write('void {}_clear(SMT::Resource *resA);'.format(reg))
            tab_h.write()

            tab_c.write()
            tab_c.write()
            tab_c.write('void {}_clear(SMT::Resource *resA)'.format(reg))
            tab_c.write('{')
            tab_c.add()

            if 'certificate' in data:
                tab_c.write('resA->set<{}>({}_applyCertificate(0));'.format(reg_type, reg))
            else:
                tab_c.write('resA->set<{}>(0);'.format(reg_type))

            tab_c.rem()
            tab_c.write('}')
            tab_c.write()

            tab_h.write()
            tab_h.write('std::vector<std::string> {}_getFieldNames();'.format(reg))
            tab_h.write()

            tab_c.write()
            tab_c.write('std::vector<std::string> {}_getFieldNames()'.format(reg))
            tab_c.write('{')
            tab_c.add()
            tab_c.write('return {')
            for f in data['fields']:
                tab_c.write('"{}",'.format(f['id']))
            tab_c.write('};')
            tab_c.rem()
            tab_c.write('}')
            tab_c.write()

            for f in data['fields']:
                tab_h.write('// {}'.format(f['info']))
                tab_h.write('void {}_set{}(SMT::Resource *resA, uint32_t valA);'.format(reg, f['id']))

                tab_c.write('void {}_set{}(SMT::Resource *resA, uint32_t valA)'.format(reg, f['id']))
                tab_c.write('{')
                tab_c.add()
                tab_c.write('{0} valueL = resA->get<{0}>(SMT::Resource::WRITE);'.format(reg_type))

                if 'certificate' in data:
                    tab_c.write('valueL = {2}_applyCertificate(valueL & (~(((1<<{0})-1) << {1})) | (valA << {1}));'
                            .format(f['size'], f['index'], reg))
                else:
                    tab_c.write('valueL = valueL & (~(((1<<{0})-1) << {1})) | (valA << {1});'.format(f['size'], f['index']))

                tab_c.write('resA->set<{}>(valueL);'.format(reg_type))
                tab_c.rem()
                tab_c.write('}')

                tab_h.write('uint32_t {}_get{}(SMT::Resource *resA);'.format(reg, f['id']))

                tab_c.write()
                tab_c.write('uint32_t {}_get{}(SMT::Resource *resA)'.format(reg, f['id']))
                tab_c.write('{')
                tab_c.add()
                tab_c.write('{0} valueL = resA->get<{0}>();'.format(reg_type))
                tab_c.write('return valueL >> {} & ((1<<{})-1);'.format(f['index'], f['size']))
                tab_c.rem()
                tab_c.write('}')

            tab_h.rem()

        tab_h.write()
        tab_h.write('#ifdef __cplusplus')
        tab_h.write('}')
        tab_h.write('#endif'.format(guardname))
        tab_h.write('#endif // {}'.format(guardname))
        return tab_h.out.getvalue(), tab_c.out.getvalue()

    def generate_python(self, name):
        tab = Tab()
        tab.write('from register_builder import Register')
        for reg, data in self.registers.items():
            tab.write('class {}(Register):'.format(reg))
            tab.add()
            if 'certificate' in data:
                tab.write('def applyCertificate(self, val):')
                tab.add()
                tab.write('return val & (~{}) | {}'.format(data['certificate']['mask'], data['certificate']['value']))
                tab.rem()
            tab.write('def clear(self):')
            tab.add()
            if 'certificate' in data:
                tab.write('self.write(self.applyCertificate(0))')
            else:
                tab.write('self.write(0)')
            tab.rem()
            for f in data['fields']:
                tab.write('def {}(self, val=None):'.format(f['id']))
                tab.add()
                tab.write('''"""{}"""'''.format(f['info']))
                tab.write('if val is None:')
                tab.add()
                tab.write('return (self.read() >> {}) & ((1<<{})-1)'
                        .format(f['index'], f['size']))
                tab.rem()
                if 'certificate' in data:
                    tab.write('self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<{0})-1) << {1})) | (val << {1})))'
                            .format(f['size'], f['index']))
                else:
                    tab.write('self.write(self.readWriteArea() & (~(((1<<{0})-1) << {1})) | (val << {1}))'
                            .format(f['size'], f['index']))
                tab.rem()
            tab.rem()
        return tab.out.getvalue()

    # Generates the __init__.py and setup.py file for the python package
    def generate_setup(self, package_name):
        setupfile = '''
# THIS IS A GENERATED FILE. DO NOT EDIT BY HAND!!!
from setuptools import setup
import subprocess
from datetime import datetime
import getpass
import socket

release = 'alpha'
timestamp = datetime.now().strftime('%Y.%m.%d.%H%M.%S')
tag_info = subprocess.check_output(['git', 'describe', '--tags', '--long']).strip()
version = '{}-{}-{}'.format(tag_info,release,timestamp)

user = getpass.getuser()
machine = socket.gethostname()
branch = subprocess.check_output('git rev-parse --abbrev-ref HEAD'.split()).strip()
gitsha = subprocess.check_output('git rev-parse HEAD'.split()).strip()

build_info = '{}@{} - {}:{}'.format(user,machine,branch,gitsha)
source_url = subprocess.check_output('git config --get remote.origin.url'.split()).strip()

description = 'generated register api'
long_description = """{}.
  This package is a generated api using the register_builder package.
  Build_info: {}.
  SourceUrl: {}.

""".format(description,build_info,source_url)

'''
        setupfile = setupfile + "setup(name='{0}', packages=['{0}'],version=version,description=description,long_description=long_description,)\n".format(package_name)
        return setupfile

    def generate_stdebcfg(self,package_name):
        stdebCfg = []
        stdebCfg.append("[DEFAULT]")
        stdebCfg.append("Package: {}".format(package_name.lower().replace('_','-')))
        stdebCfg.append("Depends: nasa-python-pysmt, nasa-python-pysmt-utils")
        return stdebCfg
