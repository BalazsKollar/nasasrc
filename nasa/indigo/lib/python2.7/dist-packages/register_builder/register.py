import inspect
import struct
from pysmt import Resource

class Register(Resource):
    """
    This is an extension to the resource class, specific to 'registers'.  In general, 'registers' are
    treated like an array of fields, where each field is usually only one bit.  Register builder will generate
    a new class (using this as a base class) with methods to access these fields directly.  After the object is
    constructed, the registerFields method is called to create a way to get a list of fields for the object
    """
    ignore_names = ['addFieldName',
                    'getField',
                    'getFieldNames',
                    'applyCertificate',
                    'clear']

    def __init__(self, topic, typename, readInfo, writeInfo, size, endian, info=''):
        Resource.__init__(self, topic, typename, readInfo, writeInfo, size, endian, info)

        rfile, roff = readInfo
        wfile, woff = writeInfo

        for member in inspect.getmembers(self):
            memberName, memberObj = member
            if inspect.ismethod(memberObj) and not memberName.startswith('_') and memberName not in Register.ignore_names:
                self.addFieldName(memberName)

    def __str__(self):
        return "0x{:04X}".format(self.read())