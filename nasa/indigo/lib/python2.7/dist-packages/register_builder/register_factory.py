import os
import imp
import importlib

def RegisterFactory(topic, typename, rinfo, winfo, size, endian, abstractInfo):
    # check that type is defined
    if 'type' not in abstractInfo:
        raise KeyError('abstractInfo must list a valid "type"')

    if ('module' not in abstractInfo) and ('module_path' not in abstractInfo):
        raise KeyError('abstractInfo must list a valid "module" or "module_path"')

    # check that only one is defined
    if 'module' in abstractInfo and 'module_path' in abstractInfo:
        raise  KeyError('abstractInfo must list one of "module" or "module_path"')

    if 'module' in abstractInfo:
        if 'package' in abstractInfo:
            registers = importlib.import_module('{}.{}'.format(abstractInfo['package'], abstractInfo['module']))
        else:
            registers = importlib.import_module(abstractInfo['module'])

    elif 'module_path' in abstractInfo:
        # full file path to the module source
        modulepath = os.path.expanduser(abstractInfo['module_path'])
        # file name (without path)
        filename = os.path.basename(modulepath)
        # file name without extension
        module = os.path.splitext(filename)[0]
        # import module from source
        registers = imp.load_source('{0}'.format(module), modulepath)

    reg = getattr(registers, abstractInfo['type'])
    regObj = reg(topic, 'uint16', rinfo, winfo, size, endian)
    return regObj
