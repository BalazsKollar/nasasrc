from register_builder import Register
class StatusRegister(Register):
    def applyCertificate(self, val):
        return val & (~61440) | 49152
    def clear(self):
        self.write(self.applyCertificate(0))
    def AllBits(self, val=None):
        """Used to set value of entire lower byte."""
        if val is None:
            return (self.read() >> 0) & ((1<<8)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<8)-1) << 0)) | (val << 0)))
    def Bit0(self, val=None):
        """info for bit0"""
        if val is None:
            return (self.read() >> 0) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 0)) | (val << 0)))
    def Bit1(self, val=None):
        """info for bit1"""
        if val is None:
            return (self.read() >> 1) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 1)) | (val << 1)))
    def Bit2(self, val=None):
        """info for bit2"""
        if val is None:
            return (self.read() >> 2) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 2)) | (val << 2)))
    def Bit3(self, val=None):
        """info for bit3"""
        if val is None:
            return (self.read() >> 3) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 3)) | (val << 3)))
    def Bit4(self, val=None):
        """info for bit4"""
        if val is None:
            return (self.read() >> 4) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 4)) | (val << 4)))
    def Bit5(self, val=None):
        """info for bit5"""
        if val is None:
            return (self.read() >> 5) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 5)) | (val << 5)))
    def Bit6(self, val=None):
        """info for bit6"""
        if val is None:
            return (self.read() >> 6) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 6)) | (val << 6)))
    def Bit7(self, val=None):
        """info for bit7"""
        if val is None:
            return (self.read() >> 7) & ((1<<2)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<2)-1) << 7)) | (val << 7)))
    def Bit9(self, val=None):
        """info for bit9"""
        if val is None:
            return (self.read() >> 8) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 8)) | (val << 8)))
    def Bit10(self, val=None):
        """info for bit10"""
        if val is None:
            return (self.read() >> 9) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 9)) | (val << 9)))
class ControlRegister(Register):
    def applyCertificate(self, val):
        return val & (~61440) | 49152
    def clear(self):
        self.write(self.applyCertificate(0))
    def AllBits(self, val=None):
        """Used to set value of entire lower byte."""
        if val is None:
            return (self.read() >> 0) & ((1<<8)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<8)-1) << 0)) | (val << 0)))
    def Bit0(self, val=None):
        """info for bit0"""
        if val is None:
            return (self.read() >> 0) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 0)) | (val << 0)))
    def Bit1(self, val=None):
        """info for bit1"""
        if val is None:
            return (self.read() >> 1) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 1)) | (val << 1)))
    def Bit2(self, val=None):
        """info for bit2"""
        if val is None:
            return (self.read() >> 2) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 2)) | (val << 2)))
    def Bit3(self, val=None):
        """info for bit3"""
        if val is None:
            return (self.read() >> 3) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 3)) | (val << 3)))
    def Bit4(self, val=None):
        """info for bit4"""
        if val is None:
            return (self.read() >> 4) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 4)) | (val << 4)))
    def Bit5(self, val=None):
        """info for bit5"""
        if val is None:
            return (self.read() >> 5) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 5)) | (val << 5)))
    def Bit6(self, val=None):
        """info for bit6"""
        if val is None:
            return (self.read() >> 6) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 6)) | (val << 6)))
    def Bit7(self, val=None):
        """info for bit7"""
        if val is None:
            return (self.read() >> 7) & ((1<<2)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<2)-1) << 7)) | (val << 7)))
    def Bit9(self, val=None):
        """info for bit9"""
        if val is None:
            return (self.read() >> 8) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 8)) | (val << 8)))
    def Bit10(self, val=None):
        """info for bit10"""
        if val is None:
            return (self.read() >> 9) & ((1<<1)-1)
        self.write(self.applyCertificate(self.readWriteArea() & (~(((1<<1)-1) << 9)) | (val << 9)))

class MotCom:
    mask = 0x0E00

    def __init__(self, dev, offset):
        self.fields = [u'value']

        def f_set(val):
            dev[offset:offset + 2] = struct.pack(">H", val)
        self.f_set = f_set
        self.f_get = lambda: struct.unpack(">H", dev[offset:offset + 2])[0]
        self._type = "uint16_t"

    def __call__(self, val=None):
        if val is None:
            return self.f_get()
        self.f_set(val)

    def value(self, val=None):
        """range=[-255, 255]"""
        if val is None:
            val = self.f_get()
            if val & 0x0100:
                return -(val & 0xFF)
            return val & 0xFF
        if val < 0:
            self.f_set(MotCom.mask | 0x0100 | (abs(val) & 0xFF))
        else:
            self.f_set(MotCom.mask | (val & 0xFF))
