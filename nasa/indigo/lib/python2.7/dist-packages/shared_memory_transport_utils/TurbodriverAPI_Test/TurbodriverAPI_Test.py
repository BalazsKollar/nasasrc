import pysmt
import register_builder
import TurbodriverInterface
class TurbodriverAPI_Test(object):


	def __init__(self,nodePath='',hostUri='http://localhost:11312'):
		self.client = pysmt.SMTClientFactory(hostUri)
		self.client.abstractFactories['register'] = register_builder.RegisterFactory
		if nodePath != '':
			self.nodePath = nodePath
			self.Subscribe(nodePath)

	def Subscribe(self,nodePath):
		self.nodePath = nodePath
		errorstrings = []
		try:
			self.subscriberCtrlReg1 = self.client.getResource(nodePath + '/CtrlReg1')
			self.setCtrlReg1BrakeReleaseCmd = self.subscriberCtrlReg1.BrakeRelease
			self.setCtrlReg1BridgeEnableCmd = self.subscriberCtrlReg1.BridgeEnable
			self.setCtrlReg1ClearEncoderCmd = self.subscriberCtrlReg1.ClearEncoder
			self.setCtrlReg1CommutationSelCmd = self.subscriberCtrlReg1.CommutationSel
			self.setCtrlReg1MotComSourceCmd = self.subscriberCtrlReg1.MotComSource
			self.setCtrlReg1MotorEnableCmd = self.subscriberCtrlReg1.MotorEnable
			self.setCtrlReg1ProcResetCmd = self.subscriberCtrlReg1.ProcReset
		except Exception as e:
			errorstrings.append('exception subscribing to CtrlReg1: "{}"'.format(e))
		try:
			self.subscriberCtrlReg2 = self.client.getResource(nodePath + '/CtrlReg2')
			self.setCtrlReg2BootloaderModeCmd = self.subscriberCtrlReg2.BootloaderMode
			self.setCtrlReg2CalibrationModeCmd = self.subscriberCtrlReg2.CalibrationMode
			self.setCtrlReg2ClearFaultCmd = self.subscriberCtrlReg2.ClearFault
			self.setCtrlReg2ControlModeCmd = self.subscriberCtrlReg2.ControlMode
			self.setCtrlReg2StreamModeCmd = self.subscriberCtrlReg2.StreamMode
			self.setCtrlReg2StreamModeResetCmd = self.subscriberCtrlReg2.StreamModeReset
		except Exception as e:
			errorstrings.append('exception subscribing to CtrlReg2: "{}"'.format(e))
		try:
			self.subscriberJointTorque_Des_Nm = self.client.getResource(nodePath + '/JointTorque_Des_Nm')
			self.getJointTorque_Des_NmCmd = self.subscriberJointTorque_Des_Nm
		except Exception as e:
			errorstrings.append('exception subscribing to JointTorque_Des_Nm: "{}"'.format(e))
		if len(errorstrings) > 0:
			for error in errorstrings:
				print error
			print 'missing subscriber data for node: {}'.format(nodePath)

	def getNodePath(self):
		return self.nodePath
	def getEffortCommand(self, val):
		try:
			val = self.getJointTorque_Des_NmCmd();
		except AttributeError:
			return
		return  val

	def park(self):
		try:
			self.setCtrlReg1MotorEnableCmd(0);
			self.setCtrlReg1BrakeReleaseCmd(0);
			self.setCtrlReg1ProcResetCmd(0);
			self.setCtrlReg1BridgeEnableCmd(1);
			self.setCtrlReg1ClearEncoderCmd(0);
			self.setCtrlReg2ControlModeCmd(0);
			self.setCtrlReg2ClearFaultCmd(0);
			self.setCtrlReg2CalibrationModeCmd(0);
			self.setCtrlReg2StreamModeCmd(0);
			self.setCtrlReg2StreamModeResetCmd(0);
			self.setCtrlReg2BootloaderModeCmd(0);
		except AttributeError:
			return
		return 

	def torqueMode(self):
		try:
			self.setCtrlReg1MotorEnableCmd(1);
			self.setCtrlReg1BrakeReleaseCmd(1);
			self.setCtrlReg1ProcResetCmd(0);
			self.setCtrlReg1BridgeEnableCmd(1);
			self.setCtrlReg1ClearEncoderCmd(0);
			self.setCtrlReg1MotComSourceCmd(1);
			self.setCtrlReg1CommutationSelCmd(3);
			self.setCtrlReg2ControlModeCmd(1);
			self.setCtrlReg2ClearFaultCmd(0);
			self.setCtrlReg2CalibrationModeCmd(0);
			self.setCtrlReg2StreamModeCmd(0);
			self.setCtrlReg2StreamModeResetCmd(0);
			self.setCtrlReg2BootloaderModeCmd(0);
		except AttributeError:
			return
		return 

	def getApi(self):
		api=[]
		api.append(self.getEffortCommand)
		api.append(self.park)
		api.append(self.torqueMode)
		return api
