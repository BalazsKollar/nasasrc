import math

#generates a chirp signal
Prev_inst_chirp_angle=0.0 #global var for function getChirpSignal
def getChirpSignal(elapsedTime_sec, samplePeriod_sec, amplitude, offset, lowFreq_Hz, highFreq_hz, rate):
	global Prev_inst_chirp_angle
	range = highFreq_hz-lowFreq_Hz
	end = 0
	if elapsedTime_sec > 0.0:
		inst_chirp_angle = 2 * math.pi * lowFreq_Hz * (pow(rate*range,elapsedTime_sec)-1)/math.log(rate*range)
		chirp_signal = amplitude*math.sin(inst_chirp_angle) + offset
		if samplePeriod_sec <= 0.0:
			inst_chirp_freq_hz = 0.0
		else:
			inst_chirp_freq_hz = (inst_chirp_angle - Prev_inst_chirp_angle)/samplePeriod_sec/2/math.pi
		Prev_inst_chirp_angle = inst_chirp_angle
		if inst_chirp_freq_hz > highFreq_hz:
			end = 1
	else:
		chirp_signal = offset
		inst_chirp_freq_hz = 0.0
  	return chirp_signal, inst_chirp_freq_hz, end

def getLinearChirpSignal(elapsedTime_sec, samplePeriod_sec, amplitude, offset, lowFreq_Hz, highFreq_hz, rate):
	global Prev_inst_chirp_angle
	range = highFreq_hz-lowFreq_Hz
	end = 0
	if elapsedTime_sec > 0.0:
		inst_chirp_angle = 2 * math.pi * ( lowFreq_Hz + rate*(highFreq_hz-lowFreq_Hz)*elapsedTime_sec ) * elapsedTime_sec
		chirp_signal = amplitude*math.sin(inst_chirp_angle) + offset
		if samplePeriod_sec <= 0.0:
			inst_chirp_freq_hz = 0.0
		else:
			inst_chirp_freq_hz = (inst_chirp_angle - Prev_inst_chirp_angle)/samplePeriod_sec/2/math.pi
		Prev_inst_chirp_angle = inst_chirp_angle
		if inst_chirp_freq_hz > highFreq_hz:
			end = 1
	else:
		chirp_signal = offset
		inst_chirp_freq_hz = 0.0
  	return chirp_signal, inst_chirp_freq_hz, end
  	
#generates a sine wave
def getSineSignal(elapsedTime_sec, amplitude, offset, freq_hz):
  	return amplitude*math.sin(elapsedTime_sec * 2 * math.pi * freq_hz) + offset

#generates a cosine wave
def getDCosineSignal(elapsedTime_sec, amplitude, offset, freq_hz):
  	return 2 * math.pi * freq_hz * amplitude*math.cos(elapsedTime_sec * 2 * math.pi * freq_hz) + offset

#generates a cosine wave
def getDDSineSignal(elapsedTime_sec, amplitude, offset, freq_hz):
  	return -4 * math.pi * math.pi * freq_hz * freq_hz * amplitude*math.sin(elapsedTime_sec * 2 * math.pi * freq_hz) + offset

def getSquareSignal(elapsedTime_sec, amplitude, offset, freq_hz):
	if((int)(elapsedTime_sec*2*freq_hz)%2) :
		return offset + amplitude; 
	else :
		return offset - amplitude; 