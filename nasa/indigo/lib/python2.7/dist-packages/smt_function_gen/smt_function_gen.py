import reference_signals
import time

#class SineWaveGenerator(object):
#	def __init__(self, amplitude, offset, freq_hz):
#		self.amplitude = amplitude
#		self.offset = offset
#		self.freq_hz = freq_hz
#		self.timestamp = time.time()
#		self.elapsedTime_sec = 0
#		self.running = False
#
#
#	def start(self):
#		self.start_time = time.time()
#		self.running = True
#
#	def stop(self):
#		self.start_time = time.time()
#		self.running = False
#		pass
#
#	def update(self):
#		if self.running:
#			elapsedTime = time.time() - self.start_time()
#		pass

class SineWaveGenerator(object):
	def __init__(self, amplitude, offset, freq_hz):
		self.amplitude = amplitude
		self.offset = offset
		self.freq_hz = freq_hz

	def update(self):
		try:
			elapsedTime_sec = time.time() - self.start_time
		except AttributeError:
			self.start_time = time.time()
			elapsedTime_sec = time.time() - self.start_time
		amp = reference_signals.getSineSignal(elapsedTime_sec, self.amplitude, self.offset, self.freq_hz)
		vel = reference_signals.getDCosineSignal(elapsedTime_sec, self.amplitude, self.offset, self.freq_hz)
		acc = reference_signals.getDDSineSignal(elapsedTime_sec, self.amplitude, self.offset, self.freq_hz)
		return amp,vel,acc
