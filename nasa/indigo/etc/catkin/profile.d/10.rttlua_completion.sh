#!/bin/sh
if [ -z "$LUA_PATH" ]; then
    LUA_PATH=";"
fi
export LUA_PATH="/opt/nasa/indigo/share/lua/5.1/rttlua_completion/?.lua;$LUA_PATH"

if [ -z "$LUA_CPATH" ]; then
    LUA_CPATH=";"
fi
export LUA_CPATH="/opt/nasa/indigo/share/lua/5.1/rttlua_completion/?.so;$LUA_CPATH"
