# - Try to find ZeroMQ
# Once done this will define
#  ZeroMQ_FOUND - System has ZeroMQ
#  ZeroMQ_INCLUDE_DIRS - The ZeroMQ include directories
#  ZeroMQ_LIBRARIES - The libraries needed to use ZeroMQ

find_package(PkgConfig)
pkg_check_modules(PC_ZEROMQ libzmq)
# message(STATUS "PKG_CONFIG_FOUND: ${PKG_CONFIG_FOUND}")
# message(STATUS "PKG_CONFIG_EXECUTABLE: ${PKG_CONFIG_EXECUTABLE}")
# message(STATUS "PKG_CONFIG_VERSION_STRING: ${PKG_CONFIG_VERSION_STRING}")
# message(STATUS "PC_ZEROMQ_FOUND: ${PC_ZEROMQ_FOUND}")
# message(STATUS "PC_ZeroMQ_INCLUDE_DIRS: ${PC_ZeroMQ_INCLUDE_DIRS}")
# message(STATUS "PC_ZEROMQ_LIBRARY_DIRS: ${PC_ZEROMQ_LIBRARY_DIRS}")

find_path(ZEROMQ_INCLUDE_DIR zmq.hpp
          HINTS ${PC_ZeroMQ_INCLUDE_DIRS}
          PATH_SUFFIXES include)

find_library(ZEROMQ_LIBRARY NAMES zmq
             HINTS ${PC_ZEROMQ_LIBRARY_DIRS})

set(ZeroMQ_LIBRARIES ${ZEROMQ_LIBRARY})
set(ZeroMQ_INCLUDE_DIRS ${ZEROMQ_INCLUDE_DIR})
# message(STATUS "ZEROMQ_LIBRARY: ${ZEROMQ_LIBRARY}")
# message(STATUS "ZEROMQ_INCLUDE_DIR: ${ZEROMQ_INCLUDE_DIR}")

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set ZEROMQ_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(ZeroMQ DEFAULT_MSG
                                  ZeroMQ_LIBRARIES ZeroMQ_INCLUDE_DIRS)

mark_as_advanced(ZeroMQ_INCLUDE_DIRS ZeroMQ_LIBRARIES)

# ensure that they are cached
SET(ZeroMQ_INCLUDE_DIRS ${ZeroMQ_INCLUDE_DIRS} CACHE INTERNAL "The libzmq include path")
SET(ZeroMQ_LIBRARIES ${ZeroMQ_LIBRARIES} CACHE INTERNAL "The libraries needed to use libzmq library")