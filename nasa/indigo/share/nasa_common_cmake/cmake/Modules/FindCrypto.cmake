# - Try to find Crypto
# Once done this will define
#  Crypto_FOUND - System has Crypto
#  Crypto_INCLUDE_DIRS - The Crypto include directories
#  Crypto_LIBRARIES - The libraries needed to use Crypto

find_package(PkgConfig)
pkg_check_modules(PC_CRYPTO libcrypto)
# message(STATUS "PKG_CONFIG_FOUND: ${PKG_CONFIG_FOUND}")
# message(STATUS "PKG_CONFIG_EXECUTABLE: ${PKG_CONFIG_EXECUTABLE}")
# message(STATUS "PKG_CONFIG_VERSION_STRING: ${PKG_CONFIG_VERSION_STRING}")
# message(STATUS "PC_CRYPTO_FOUND: ${PC_CRYPTO_FOUND}")
# message(STATUS "PC_Crypto_INCLUDE_DIRS: ${PC_Crypto_INCLUDE_DIRS}")
# message(STATUS "PC_CRYPTO_LIBRARY_DIRS: ${PC_CRYPTO_LIBRARY_DIRS}")

find_path(CRYPTO_INCLUDE_DIR openssl/crypto.h
          HINTS ${PC_Crypto_INCLUDE_DIRS}
          PATH_SUFFIXES include)

find_library(CRYPTO_LIBRARY NAMES crypto
             HINTS ${PC_CRYPTO_LIBRARY_DIRS})

set(Crypto_LIBRARIES ${CRYPTO_LIBRARY})
set(Crypto_INCLUDE_DIRS ${CRYPTO_INCLUDE_DIR})
# message(STATUS "CRYPTO_LIBRARY: ${CRYPTO_LIBRARY}")
# message(STATUS "CRYPTO_INCLUDE_DIR: ${CRYPTO_INCLUDE_DIR}")

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set CRYPTO_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(Crypto DEFAULT_MSG
                                  Crypto_LIBRARIES Crypto_INCLUDE_DIRS)

mark_as_advanced(Crypto_INCLUDE_DIRS Crypto_LIBRARIES)

# ensure that they are cached
SET(Crypto_INCLUDE_DIRS ${Crypto_INCLUDE_DIRS} CACHE INTERNAL "The libcrypto include path")
SET(Crypto_LIBRARIES ${Crypto_LIBRARIES} CACHE INTERNAL "The libraries needed to use libcrypto library")