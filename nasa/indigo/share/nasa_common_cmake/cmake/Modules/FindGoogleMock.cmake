# - Try to find GoogleMock
# Once done this will define
#  GoogleMock_FOUND - System has GoogleMock
#  GoogleMock_INCLUDE_DIRS - The GoogleMock include directories
#  GoogleMock_SRC_FILES - The source files needed to use GoogleMock
#  GoogleMockMain_SRC_FILES - The source files needed to use GoogleMock (with main() defined)
#  GoogleMock_LIBRARIES - The libraries needed to use GoogleMock
#  GoogleMockMain_LIBRARIES - The libraries needed to use GoogleMock (with main() defined)
#  GoogleMockGTest_INCLUDE_DIRS - The GoogleMock-provided GTest include directories
#  GoogleMockGTest_SRC_FILES - The source files needed to use GoogleMock-provided GTest
#  GoogleMockGTestMain_SRC_FILES - The source files needed to use GoogleMock-provided GTest (with main() defined)
#
# For users that need Google Mock by itself, use GoogleMock_* variables.
# For users that need both Google Mock and Google Test in the same CMake project, use GoogleMock_* for mock things and GoogleMockGTest_* for gtest things.
# For users that need Google Test by itself, don't use any of these variables.
#
# Users of Google Mock should use both SRC_FILES and LIBRARIES. Older versions of google-mock don't provide sources,
#   and newer versions don't provide libraries.  Only one will be populated, so just use both and things will work.

find_package(PkgConfig)
pkg_check_modules(PC_GOOGLEMOCK google-mock)
# message(STATUS "PKG_CONFIG_FOUND: ${PKG_CONFIG_FOUND}")
# message(STATUS "PKG_CONFIG_EXECUTABLE: ${PKG_CONFIG_EXECUTABLE}")
# message(STATUS "PKG_CONFIG_VERSION_STRING: ${PKG_CONFIG_VERSION_STRING}")
# message(STATUS "PC_GOOGLEMOCK_FOUND: ${PC_GOOGLEMOCK_FOUND}")
# message(STATUS "PC_GOOGLEMOCK_INCLUDE_DIRS: ${PC_GoogleMock_INCLUDE_DIRS}")

find_path(GOOGLEMOCK_INCLUDE_DIR gmock/gmock.h
          HINTS ${PC_GOOGLEMOCK_INCLUDE_DIRS}
          PATH_SUFFIXES include)

find_path(GOOGLEMOCK_SRC_DIR src/gmock.cc
          HINTS ${PC_GOOGLEMOCK_INCLUDE_DIRS}
          PATH_SUFFIXES src/gmock)

find_path(GOOGLETEST_INCLUDE_DIR gtest/gtest.h
          HINTS ${PC_GOOGLEMOCK_INCLUDE_DIRS} ${GOOGLEMOCK_SRC_DIR}
          PATH_SUFFIXES gtest/include
          PATHS /usr/include)

find_path(GOOGLETEST_SRC_DIR src/gtest.cc
          HINTS ${PC_GOOGLEMOCK_INCLUDE_DIRS} ${GOOGLEMOCK_SRC_DIR}
          PATH_SUFFIXES gtest
          PATHS /usr/src)

find_library(GOOGLEMOCK_LIB gmock
          HINTS ${PC_GOOGLEMOCK_LIBRARY_DIRS}
          PATH_SUFFIXES lib)

find_library(GOOGLEMOCK_MAIN_LIB gmock_main
          HINTS ${PC_GOOGLEMOCK_LIBRARY_DIRS}
          PATH_SUFFIXES lib)

set(GOOGLEMOCK_SRCS
    ${GOOGLEMOCK_SRC_DIR}/src/gmock-all.cc
    # ${GOOGLEMOCK_SRC_DIR}/src/gmock-cardinalities.cc
    # ${GOOGLEMOCK_SRC_DIR}/src/gmock-internal-utils.cc
    # ${GOOGLEMOCK_SRC_DIR}/src/gmock-matchers.cc
    # ${GOOGLEMOCK_SRC_DIR}/src/gmock-spec-builders.cc
    # ${GOOGLEMOCK_SRC_DIR}/src/gmock.cc
)

set(GOOGLEMOCK_MAIN_SRCS
    ${GOOGLEMOCK_SRCS}
    ${GOOGLEMOCK_SRC_DIR}/src/gmock_main.cc
)

set(GOOGLETEST_SRCS 
    ${GOOGLETEST_SRC_DIR}/src/gtest-all.cc
    # ${GOOGLETEST_SRC_DIR}/src/gtest-death-test.cc
    # ${GOOGLETEST_SRC_DIR}/src/gtest-filepath.cc
    # ${GOOGLETEST_SRC_DIR}/src/gtest-port.cc
    # ${GOOGLETEST_SRC_DIR}/src/gtest-printers.cc
    # ${GOOGLETEST_SRC_DIR}/src/gtest-test-part.cc
    # ${GOOGLETEST_SRC_DIR}/src/gtest-typed-test.cc
    # ${GOOGLETEST_SRC_DIR}/src/gtest.cc
)

set(GOOGLETEST_MAIN_SRCS 
    ${GOOGLETEST_SRCS}
    ${GOOGLETEST_SRC_DIR}/src/gtest_main.cc
)

if (NOT GOOGLEMOCK_LIB)
  set(GoogleMock_INCLUDE_DIRS ${GOOGLEMOCK_INCLUDE_DIR} ${GOOGLEMOCK_SRC_DIR} ${GOOGLETEST_INCLUDE_DIR} ${GOOGLETEST_SRC_DIR})
  set(GoogleMock_SRC_FILES ${GOOGLEMOCK_SRCS} ${GOOGLETEST_SRCS})
  set(GoogleMockMain_SRC_FILES ${GOOGLEMOCK_MAIN_SRCS} ${GOOGLETEST_SRCS})
else (NOT GOOGLEMOCK_LIB)
  set(GoogleMock_INCLUDE_DIRS ${GOOGLEMOCK_INCLUDE_DIR} ${GOOGLETEST_INCLUDE_DIR} ${GOOGLETEST_SRC_DIR})
  set(GoogleMock_LIBRARIES ${GOOGLEMOCK_LIB})
  set(GoogleMockMain_LIBRARIES ${GOOGLEMOCK_MAIN_LIB})
endif (NOT GOOGLEMOCK_LIB)

set(GoogleMockGTest_INCLUDE_DIRS ${GOOGLETEST_INCLUDE_DIR} ${GOOGLETEST_SRC_DIR})
set(GoogleMockGTest_SRC_FILES ${GOOGLETEST_SRCS})
set(GoogleMockGTestMain_SRC_FILES ${GOOGLETEST_MAIN_SRCS})
# message(STATUS "GOOGLEMOCK_INCLUDE_DIR: ${GOOGLEMOCK_INCLUDE_DIR}")
# message(STATUS "GOOGLEMOCK_SRC_DIR: ${GOOGLEMOCK_SRC_DIR}")
# message(STATUS "GOOGLEMOCK_SRCS: ${GOOGLEMOCK_SRCS}")
# message(STATUS "GOOGLEMOCK_LIB: ${GOOGLEMOCK_LIB}")
# message(STATUS "GOOGLETEST_INCLUDE_DIR: ${GOOGLETEST_INCLUDE_DIR}")
# message(STATUS "GOOGLETEST_SRC_DIR: ${GOOGLETEST_SRC_DIR}")
# message(STATUS "GOOGLETEST_SRCS: ${GOOGLETEST_SRCS}")

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set GOOGLEMOCK_FOUND to TRUE
# if all listed variables are TRUE
if (NOT GOOGLEMOCK_LIB)
  # message(STATUS "GOOGLE MOCK - USING SOURCES")
  find_package_handle_standard_args(GoogleMock DEFAULT_MSG
                                    GoogleMock_INCLUDE_DIRS GoogleMock_SRC_FILES GoogleMockMain_SRC_FILES
                                    GoogleMockGTest_INCLUDE_DIRS GoogleMockGTest_SRC_FILES GoogleMockGTestMain_SRC_FILES)

  mark_as_advanced(GoogleMock_INCLUDE_DIRS GoogleMock_SRC_FILES GoogleMockMain_SRC_FILES
                   GoogleMockGTest_INCLUDE_DIRS GoogleMockGTest_SRC_FILES GoogleMockGTestMain_SRC_FILES)
else (NOT GOOGLEMOCK_LIB)
  # message(STATUS "GOOGLE MOCK - USING LIBRARIES")
  find_package_handle_standard_args(GoogleMock DEFAULT_MSG
                                    GoogleMock_INCLUDE_DIRS GoogleMock_LIBRARIES GoogleMockMain_LIBRARIES
                                    GoogleMockGTest_INCLUDE_DIRS GoogleMockGTest_SRC_FILES GoogleMockGTestMain_SRC_FILES)

  mark_as_advanced(GoogleMock_INCLUDE_DIRS GoogleMock_LIBRARIES GoogleMockMain_LIBRARIES
                   GoogleMockGTest_INCLUDE_DIRS GoogleMockGTest_SRC_FILES GoogleMockGTestMain_SRC_FILES)
endif (NOT GOOGLEMOCK_LIB)

# ensure that they are cached
SET(GoogleMock_INCLUDE_DIRS ${GoogleMock_INCLUDE_DIRS} CACHE INTERNAL "The google-mock include path")
SET(GoogleMock_SRC_FILES ${GoogleMock_SRC_FILES} CACHE INTERNAL "The source files needed to use google-mock library")
SET(GoogleMockMain_SRC_FILES ${GoogleMockMain_SRC_FILES} CACHE INTERNAL "The source files needed to use google-mock library (with main() defined)")
SET(GoogleMock_LIBRARIES ${GoogleMock_LIBRARIES} CACHE INTERNAL "The libraries needed to use google-mock library")
SET(GoogleMockMain_LIBRARIES ${GoogleMockMain_LIBRARIES} CACHE INTERNAL "The libraries files needed to use google-mock library (with main() defined)")
SET(GoogleMockGTest_INCLUDE_DIRS ${GoogleMockGTest_INCLUDE_DIRS} CACHE INTERNAL "The google-mock-provided gtest include path")
SET(GoogleMockGTest_SRC_FILES ${GoogleMockGTest_SRC_FILES} CACHE INTERNAL "The source files needed to use google-mock-provided gtest library")
SET(GoogleMockGTestMain_SRC_FILES ${GoogleMockGTestMain_SRC_FILES} CACHE INTERNAL "The source files needed to use google-mock-provided gtest library (with main() defined)")