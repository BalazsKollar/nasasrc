# Find MesaSR on Linux.
#
# Once loaded this will define
#   MesaSR_FOUND           - system has MesaSR
#   MesaSR_INCLUDE_DIRS    - include directory for MesaSR
#   MesaSR_LIBRARIES       - libraries you need to link to

find_path(MesaSR_INCLUDE_DIRS
  NAMES libMesaSR.h
  PATHS "/usr/include"
)

if(MesaSR_INCLUDE_DIRS)
  set(MesaSR_LIBRARY_DIR /usr/lib/)
  find_library(MesaSR_LIBRARIES
    NAMES mesasr
    PATHS
    "${MesaSR_LIBRARY_DIR}"
  )
endif(MesaSR_INCLUDE_DIRS)

if(MesaSR_LIBRARIES)
  set(MesaSR_FOUND TRUE)
else(MesaSR_LIBRARIES)
  message(STATUS "Warning: cannot find MesaSR")
endif(MesaSR_LIBRARIES)

MARK_AS_ADVANCED(MesaSR_INCLUDE_DIRS MesaSR_LIBRARIES)