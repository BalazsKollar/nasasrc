# Wrapper for calling api_builder with proper environment variables set.

foreach(var API_PATH PY_PATH API_FILE LIB_NAME CPP_DIR HPP_BASE HPP_DIR PY_DIR PY_PKG)
  if(NOT DEFINED ${var})
    message(FATAL_ERROR "${var} must be passed on the command line")
  endif()
endforeach()

set(ENV{PATH} "${API_PATH}:$ENV{PATH}")
set(ENV{PYTHONPATH} "${PY_PATH}:$ENV{PYTHONPATH}")

execute_process(
  COMMAND api_builder 
            ${API_FILE}
            --name ${LIB_NAME}
            --cpp ${CPP_DIR}
            --header_base ${HPP_BASE}
            --hpp ${HPP_DIR}
            --py ${PY_DIR}
            --pypackage ${PY_PKG})