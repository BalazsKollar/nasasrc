# Wrapper for calling api_builder with proper environment variables set.

foreach(var REG_PATH PY_PATH REG_FILE LIB_NAME CPP_DIR HPP_DIR PY_DIR PKG_PREFIX)
  if(NOT DEFINED ${var})
    message(FATAL_ERROR "${var} must be passed on the command line")
  endif()
endforeach()

set(ENV{PATH} "${REG_PATH}:$ENV{PATH}")
set(ENV{PYTHONPATH} "${PY_PATH}:$ENV{PYTHONPATH}")

execute_process(
  COMMAND register_builder
            ${REG_FILE}
            --name ${LIB_NAME}
            --cpp ${CPP_DIR}
            --hpp ${HPP_DIR}
            --py ${PY_DIR}
            --prefix ${PKG_PREFIX})