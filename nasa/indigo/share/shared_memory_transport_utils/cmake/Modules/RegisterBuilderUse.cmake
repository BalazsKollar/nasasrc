# run after catkin_destinations() and before catkin_python_setup()
# provides ${REGBUILD_INCLUDE_DIRS_${libname}} for catkin_package(INCLUDE_DIRS)
function(register_builder_destinations libname)
  set(REG_BUILD_CPP_DIR_${libname} ${CATKIN_DEVEL_PREFIX}/${CATKIN_PACKAGE_LIB_DESTINATION}/${PROJECT_NAME}/${libname}/src CACHE INTERNAL "Register Builder generation dir for ${libname}, cpp")
  set(REG_BUILD_HPP_DIR_${libname} ${CATKIN_DEVEL_PREFIX}/${CATKIN_PACKAGE_INCLUDE_DESTINATION}/${libname} CACHE INTERNAL "Register Builder generation dir for ${libname}, hpp")
  set(REG_BUILD_PYTHON_DIR_${libname} ${CATKIN_DEVEL_PREFIX}/${CATKIN_PACKAGE_PYTHON_DESTINATION} CACHE INTERNAL "Register Builder generation dir for ${libname}, python")
  file(MAKE_DIRECTORY ${REG_BUILD_CPP_DIR_${libname}})
  file(MAKE_DIRECTORY ${REG_BUILD_HPP_DIR_${libname}})
  file(MAKE_DIRECTORY ${REG_BUILD_PYTHON_DIR_${libname}}/${libname})
  set(REGBUILD_INCLUDE_DIRS_${libname} ${REG_BUILD_HPP_DIR_${libname}} ${REG_BUILD_PYTHON_DIR_${libname}} CACHE INTERNAL "Register Builder include dirs for ${libname}")
  message(STATUS "Register Builder Include Dirs for ${libname}: ${REGBUILD_INCLUDE_DIRS_${libname}}")
endfunction()

# This macro will create a target called <libname>.  This target is a shared object library built using register builder.
# The input files is assumed to be in the ${PROJECT_SOURCE_DIR}/registers and should be a register_builder compilant register
# definition file.
#
#   registerfile - base name of the register file to pass to register_builder.  Ex: for 'registers/myregFile.xml', simply put 'myregFile'
#   libname      - output library name.  This will also be the name of the target
#
# run after catkin_package()
# provides ${libname} for catkin_package(LIBRARIES)
macro(generate_register registerfile libname)
    if(NOT EXISTS ${REG_BUILD_PYTHON_DIR_${libname}}/__init__.py)
        file(WRITE ${REG_BUILD_PYTHON_DIR_${libname}}/__init__.py "")
        set_source_files_properties(${REG_BUILD_PYTHON_DIR_${libname}}/__init__.py PROPERTIES GENERATED TRUE)
        install(FILES ${REG_BUILD_PYTHON_DIR_${libname}}/__init__.py
            DESTINATION ${CATKIN_PACKAGE_PYTHON_DESTINATION}
        )
    endif(NOT EXISTS ${REG_BUILD_PYTHON_DIR_${libname}}/__init__.py)

    # These are the generated cpp/hpp file needed for the library
    set(register_lib_sources 
      ${REG_BUILD_CPP_DIR_${libname}}/${libname}.cpp 
      ${REG_BUILD_HPP_DIR_${libname}}/${libname}.hpp
      ${REG_BUILD_PYTHON_DIR_${libname}}/${libname}/${libname}.py
    )

    # This custom command will invoke register_builder and put the generated files in the gen/cpp folder
    add_custom_command(OUTPUT ${register_lib_sources}
                       COMMAND "${CMAKE_COMMAND}"
                         -DREG_PATH=${CATKIN_DEVEL_PREFIX}/${CATKIN_GLOBAL_BIN_DESTINATION}:${CMAKE_CURRENT_SOURCE_DIR}/scripts
                         -DPY_PATH=${CATKIN_DEVEL_PREFIX}/${CATKIN_GLOBAL_PYTHON_DESTINATION}:${CMAKE_CURRENT_SOURCE_DIR}/src
                         -DREG_FILE=${registerfile}
                         -DLIB_NAME=${libname}
                         -DCPP_DIR=${REG_BUILD_CPP_DIR_${libname}}
                         -DHPP_DIR=${REG_BUILD_HPP_DIR_${libname}}
                         -DPY_DIR=${REG_BUILD_PYTHON_DIR_${libname}}
                         -DPKG_PREFIX="nasa-python-"
                         -P "${SHARED_MEMORY_TRANSPORT_UTILS_MODULES}/register_builder_wrapper.cmake"
                       DEPENDS ${registerfile}
                       COMMENT "generating register code for: ${registerfile}")

    # Create a custom target for the output of register_builder.  Need to  set the source_file_properties so that the build rule doesn't 
    # break when the source files haven't been generated
    add_custom_target(${libname}a DEPENDS ${register_lib_sources})
    set_source_files_properties(${register_lib_sources} PROPERTIES GENERATED TRUE)

    # check for c++0x
    # It is forbidden to set CMAKE_CXX_FLAGS in catkin. http://docs.ros.org/hydro/api/catkin/html/user_guide/standards.html
    # Hydro standardizes on C++03. http://www.ros.org/reps/rep-0003.html#hydro-medusa-aug-2013
    # C++[0|1]x is not allowed/recommended. http://www.ros.org/reps/rep-0003.html#c
    include(CheckCXXCompilerFlag)
    CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
    if(COMPILER_SUPPORTS_CXX0X)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
    else()
        message(FATAL_ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++0x support. Please use a different C++ compiler.")
    endif()
    
    # If the module has a ${registerfile}.cpp, compile it into the librabry
    get_filename_component(registerfile_PATH ${registerfile} PATH)

    if(EXISTS ${registerfile_PATH}/registers.cpp)
        list(APPEND register_lib_sources ${registerfile_PATH}/registers.cpp)
    endif()

    # Build rule to make a the shared library, and link it against the shared memory transport libraries
    include_directories(${REG_BUILD_HPP_DIR_${libname}})
    add_library(${libname} SHARED ${register_lib_sources})
    target_link_libraries(${libname} ${shared_memory_transport_LIBRARIES} ${shared_memory_transport_utils_LIBRARIES})

    # This dependency forces the library to get rebuilt if the .xml input file changes
    add_dependencies(${libname} ${libname}_register_builder_output)

    install(TARGETS ${libname}
        ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
    )

    install(DIRECTORY ${REG_BUILD_HPP_DIR_${libname}}/
        DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}/${libname}
        FILES_MATCHING PATTERN "*.hpp"
    )    

    install(DIRECTORY ${REG_BUILD_PYTHON_DIR_${libname}}/
        DESTINATION ${CATKIN_PACKAGE_PYTHON_DESTINATION}
        FILES_MATCHING 
            PATTERN "*.py"
            PATTERN "setup.py" EXCLUDE
            PATTERN "__init__.py*" EXCLUDE
    )

    install(DIRECTORY ${REG_BUILD_PYTHON_DIR_${libname}}/${libname}/
        DESTINATION ${CATKIN_PACKAGE_PYTHON_DESTINATION}/${libname}
        FILES_MATCHING 
            PATTERN "*.py"
            PATTERN "setup.py" EXCLUDE
    )

    set(doxy_sources_${libname}
        REG_BUILD_CPP_DIR_${libname}
        REG_BUILD_HPP_DIR_${libname}
        REG_BUILD_PYTHON_DIR_${libname}
    )
    catkin_doxygen(${libname}_docs "${doxy_sources_${libname}}")
endmacro(generate_register)
