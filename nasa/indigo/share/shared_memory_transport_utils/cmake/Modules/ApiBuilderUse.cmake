# run after catkin_destinations() and before catkin_python_setup()
# provides ${APIBUILD_INCLUDE_DIRS_${libname}} for catkin_package(INCLUDE_DIRS)
function(api_builder_destinations libname)
  set(API_BUILD_CPP_DIR_${libname} ${CATKIN_DEVEL_PREFIX}/${CATKIN_PACKAGE_LIB_DESTINATION}/${PROJECT_NAME}/${libname}/src CACHE INTERNAL "API Builder generation dir for ${libname}, cpp")
  set(API_BUILD_HPP_DIR_${libname} ${CATKIN_DEVEL_PREFIX}/${CATKIN_PACKAGE_INCLUDE_DESTINATION}/${libname} CACHE INTERNAL "API Builder generation dir for ${libname}, hpp")
  set(API_BUILD_HPP_BASE_${libname} ${CATKIN_DEVEL_PREFIX}/${CATKIN_GLOBAL_INCLUDE_DESTINATION} CACHE INTERNAL "API Builder generation dir for ${libname}, hpp")
  set(API_BUILD_HPP_DIR_REL_${libname} ${PROJECT_NAME}/${libname} CACHE INTERNAL "API Builder relative header path for ${libname}, hpp")
  set(API_BUILD_PYTHON_DIR_${libname} ${CATKIN_DEVEL_PREFIX}/${CATKIN_PACKAGE_PYTHON_DESTINATION} CACHE INTERNAL "API Builder generation dir for ${libname}, python")
  file(MAKE_DIRECTORY ${API_BUILD_CPP_DIR_${libname}})
  file(MAKE_DIRECTORY ${API_BUILD_HPP_DIR_${libname}})
  file(MAKE_DIRECTORY ${API_BUILD_PYTHON_DIR_${libname}}/${libname})
  set(APIBUILD_INCLUDE_DIRS_${libname} ${API_BUILD_HPP_BASE_${libname}}  ${API_BUILD_PYTHON_DIR_${libname}} CACHE INTERNAL "API Builder include dirs for ${libname}")
  message(STATUS "API Builder Include Dirs for ${libname}: ${APIBUILD_INCLUDE_DIRS_${libname}}")
endfunction()

# run after catkin_package()
# provides ${libname} for catkin_package(LIBRARIES)
macro(generate_api apifile libname pypackagename)
    if(NOT EXISTS ${API_BUILD_PYTHON_DIR_${libname}}/__init__.py)
        file(WRITE ${API_BUILD_PYTHON_DIR_${libname}}/__init__.py "")
        set_source_files_properties(${API_BUILD_PYTHON_DIR_${libname}}/__init__.py PROPERTIES GENERATED TRUE)
        install(FILES ${API_BUILD_PYTHON_DIR_${libname}}/__init__.py
            DESTINATION ${CATKIN_PACKAGE_PYTHON_DESTINATION}
        )
    endif(NOT EXISTS ${API_BUILD_PYTHON_DIR_${libname}}/__init__.py)

    # These are the generated cpp/hpp files needed for the library
    set(api_lib_sources 
      ${API_BUILD_CPP_DIR_${libname}}/${libname}.cpp 
      ${API_BUILD_HPP_DIR_${libname}}/${libname}.hpp
      ${API_BUILD_PYTHON_DIR_${libname}}/${libname}/${libname}.py
    )

    # This custom command will invoke api_builder
    add_custom_command(OUTPUT ${api_lib_sources}
                       COMMAND "${CMAKE_COMMAND}"
                         -DAPI_PATH=${CATKIN_DEVEL_PREFIX}/${CATKIN_GLOBAL_BIN_DESTINATION}:${CMAKE_CURRENT_SOURCE_DIR}/scripts
                         -DPY_PATH=${CATKIN_DEVEL_PREFIX}/${CATKIN_GLOBAL_PYTHON_DESTINATION}:${CMAKE_CURRENT_SOURCE_DIR}/src
                         -DAPI_FILE=${apifile}
                         -DLIB_NAME=${libname}
                         -DCPP_DIR=${API_BUILD_CPP_DIR_${libname}}
                         -DHPP_BASE=${API_BUILD_HPP_BASE_${libname}}
                         -DHPP_DIR=${API_BUILD_HPP_DIR_REL_${libname}}
                         -DPY_DIR=${API_BUILD_PYTHON_DIR_${libname}}
                         -DPY_PKG=${pypackagename}
                         -P "${SHARED_MEMORY_TRANSPORT_UTILS_MODULES}/api_builder_wrapper.cmake"
                       DEPENDS ${apifile}
                       COMMENT "generating api code for: ${apifile}")

    # Create a custom target for the output of api_builder.  Need to  set the source_file_properties so that the build rule doesn't
    # break when the source files haven't been generated
    add_custom_target(${libname}_api_builder_outputs DEPENDS ${api_lib_sources})
    set_source_files_properties(${api_lib_sources} PROPERTIES GENERATED TRUE)

    # check for c++0x
    # It is forbidden to set CMAKE_CXX_FLAGS in catkin. http://docs.ros.org/hydro/api/catkin/html/user_guide/standards.html
    # Hydro standardizes on C++03. http://www.ros.org/reps/rep-0003.html#hydro-medusa-aug-2013
    # C++[0|1]x is not allowed/recommended. http://www.ros.org/reps/rep-0003.html#c
    include(CheckCXXCompilerFlag)
    CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
    if(COMPILER_SUPPORTS_CXX0X)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
    else()
        message(FATAL_ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++0x support. Please use a different C++ compiler.")
    endif()

    # Build rule to make a the shared library, and link it against the shared memory transport libraries
    include_directories(${API_BUILD_HPP_BASE_${libname}})
    add_library(${libname} SHARED ${api_lib_sources})
    target_link_libraries(${libname} ${shared_memory_transport_LIBRARIES})

    # This dependency forces the library to get rebuilt if the .xml input file changes
    add_dependencies(${libname} ${libname}_api_builder_outputs)

    install(TARGETS ${libname}
        ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
    )

    install(DIRECTORY ${API_BUILD_HPP_DIR_${libname}}/
        DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}/${libname}
        FILES_MATCHING PATTERN "*.hpp"
    )    

    install(DIRECTORY ${API_BUILD_PYTHON_DIR_${libname}}/
        DESTINATION ${CATKIN_PACKAGE_PYTHON_DESTINATION}
        FILES_MATCHING 
            PATTERN "*.py"
            PATTERN "setup.py" EXCLUDE
            PATTERN "__init__.py*" EXCLUDE
    )

    install(DIRECTORY ${API_BUILD_PYTHON_DIR_${libname}}/${libname}/
        DESTINATION ${CATKIN_PACKAGE_PYTHON_DESTINATION}/${libname}
        FILES_MATCHING 
            PATTERN "*.py"
            PATTERN "setup.py" EXCLUDE
    )

    set(doxy_sources_${libname}
        API_BUILD_CPP_DIR_${libname}
        API_BUILD_HPP_DIR_${libname}
        API_BUILD_PYTHON_DIR_${libname}
    )
    catkin_doxygen(${libname}_docs "${doxy_sources_${libname}}")
endmacro(generate_api)
