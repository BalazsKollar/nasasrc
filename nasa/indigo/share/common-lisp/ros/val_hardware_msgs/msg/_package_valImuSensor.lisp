(cl:in-package val_hardware_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          NAME-VAL
          NAME
          ORIENTATION-VAL
          ORIENTATION
          ANGULARVELOCITY-VAL
          ANGULARVELOCITY
          LINEARACCELERATION-VAL
          LINEARACCELERATION
))