
(cl:in-package :asdf)

(defsystem "val_hardware_msgs-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :geometry_msgs-msg
               :std_msgs-msg
)
  :components ((:file "_package")
    (:file "valAtiSensor" :depends-on ("_package_valAtiSensor"))
    (:file "_package_valAtiSensor" :depends-on ("_package"))
    (:file "valImuSensor" :depends-on ("_package_valImuSensor"))
    (:file "_package_valImuSensor" :depends-on ("_package"))
  ))