require 'val_utilities'

-- provides 'timer' component pointer
-- sets VAL_GLOBALS['timer_setup']

function deployTimer()
    timer = deployComponent("Timer", "OCL::TimerComponent", "ocl")
    if not timer then
        rtt.logl('Error', 'deployComponent(Timer) returned false')
        return false
    end

    return true
end

function connectTimer()
    return true
end

function configureTimer()
    if not timer:configure() then
        rtt.logl('Error', 'Timer not configured properly')
        return false
    end

    return true
end

function scheduleTimer()
    timer:setPeriod(0.0)

    if VAL_GLOBALS['REAL_TIME'] then
        depl:setActivity("Timer", timer:getPeriod(), VAL_GLOBALS['HIGH_PRIORITY'], rtt.globals.ORO_SCHED_RT)
    end

    return true
end

function startTimer()
    if not timer:start() then
        rtt.logl('Error', 'Timer not started properly')
        return false
    end

    return true
end

function setupTimer()
    if VAL_GLOBALS['timer_setup'] then
        rtt.logl('Info', 'setupTimer() has already been called')
        return true
    end

    VAL_GLOBALS['timer_setup'] = false

    if not deployTimer() then
        rtt.logl('Error', 'setupTimer(): deployTimer() returned false')
        return false
    end

    if not connectTimer() then
        rtt.logl('Error', 'setupTimer(): connectTimer() returned false')
        return false
    end

    if not configureTimer() then
        rtt.logl('Error', 'setupTimer(): configureTimer() returned false')
        return false
    end

    if not scheduleTimer() then
        rtt.logl('Error', 'setupTimer(): scheduleTimer() returned false')
        return false
    end

    if not startTimer() then
        rtt.logl('Error', 'setupTimer(): startTimer() returned false')
        return false
    end

    VAL_GLOBALS['timer_setup'] = true

    return true
end
