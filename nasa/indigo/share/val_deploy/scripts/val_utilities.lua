if (not VAL_GLOBALS_EXISTS) then
    VAL_GLOBALS = {}
    VAL_GLOBALS_EXISTS = true
end

function string:split(sSeparator, nMax, bRegexp)
	assert(sSeparator ~= '')
	assert(nMax == nil or nMax >= 1)

	local aRecord = {}

	if self:len() > 0 then
		local bPlain = not bRegexp
		nMax = nMax or -1

		local nField=1 nStart=1
		local nFirst,nLast = self:find(sSeparator, nStart, bPlain)
		while nFirst and nMax ~= 0 do
			aRecord[nField] = self:sub(nStart, nFirst-1)
			nField = nField+1
			nStart = nLast+1
			nFirst,nLast = self:find(sSeparator, nStart, bPlain)
			nMax = nMax-1
		end
		aRecord[nField] = self:sub(nStart)
	end

	return aRecord
end

function tableHas(t, element)
    local j, value
    for j, value in ipairs(t) do
        if value == element then
            return true
        end
    end
    return false
end

function tableFind(t, element)
    local j, value
    for j, value in ipairs(t) do
        if value == element then
            return j
        end
    end
    return nil
end

-- Removes last element in table of value "element"
function tableRemoveElement(t,element)
    local j, value
    found = false
    foundIndex = -1
    for j, value in ipairs(t) do
        if value == element then
            found = true
            foundIndex = j
        end
    end
    
    if found then
        table.remove(t,foundIndex)
    end
end

function printTable(t,verbose)
    local i,el
    if(#t == 0) then
        print("Table has length == 0")
    else
        if(verbose==true) then
            for i,el in ipairs(t) do
                print("element["..i.."]=",el)
            end
        else
            for i,el in ipairs(t) do
                print(el)
            end
        end
    end
end

function tableLength(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end

function isEqualFloat(a, b, tol)
    return (math.abs(a-b)<tol)
end

function os.capture(cmd, raw)
    -- cmd: the command to run
    -- raw: true to return the raw string, false to return a string that has been cleaned up
    local f = assert(io.popen(cmd, 'r'))
    local s = assert(f:read('*a'))
    f:close()
    if raw then return s end
    s = string.gsub(s, '^%s+', '')
    s = string.gsub(s, '%s+$', '')
    s = string.gsub(s, '[\n\r]+', ' ')
    return s
end

function fileExists(name)
   local f=io.open(name,"r")
   if f~=nil then io.close(f) return true else return false end
end

function deployComponent(componentName, componentType, componentLibrary)
    if not componentName then
        rtt.logl('Warning', 'deployComponent(): missing componentName')
        return nil
    end
    if not componentType then
        rtt.logl('Warning', 'deployComponent(): missing componentType')
        return nil
    end
    if not componentLibrary then
        rtt.logl('Warning', 'deployComponent(): missing componentLibrary')
        return nil
    end

    local componentPtr = nil
    if not tableHas(depl:getPeers(), componentName) then
        if not rtt.provides("ros"):import(componentLibrary) then
            logger:logError("deployComponent(): failed to import "..componentLibrary..". Try \"catkin_make "..componentLibrary.."\"")
            return nil
        end
        depl:loadComponent(componentName, componentType)
    end
    componentPtr = depl:getPeer(componentName)

    return componentPtr
end