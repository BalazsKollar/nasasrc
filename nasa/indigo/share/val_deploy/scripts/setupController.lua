require 'val_utilities'

-- provides 'valControllerManager' component pointer
-- sets VAL_GLOBALS['valControllerManager_setup']

function deployController()
    valControllerManager = deployComponent("ValControllerManager", "ValControllerManager", "val_controller_manager_rtt")
    if not valControllerManager then
        rtt.logl('Error', 'deployComponent(ValControllerManager) returned false')
        return false
    end

    return true
end

function connectController()
    local cp = rtt.Variable('ConnPolicy')

    if not depl:connect('Timer.timer_'..VAL_GLOBALS['controller_trigger'], 'ValControllerManager.triggerPort', cp) then
        rtt.logl('Error', "ValControllerManager.triggerPort not connected properly")
        return false
    end

    return true
end

function configureController()
    if not valControllerManager:configure() then
        rtt.logl('Error', 'ValControllerManager not configured properly')
        return false
    end

    return true
end

function scheduleController()
    valControllerManager:setPeriod(0.0)

    if VAL_GLOBALS['REAL_TIME'] then
        depl:setActivity("ValControllerManager", valControllerManager:getPeriod(), VAL_GLOBALS['HIGH_PRIORITY'], rtt.globals.ORO_SCHED_RT)
    end

    return true
end

function startController()
    if not valControllerManager:start() then
        rtt.logl('Error', 'ValControllerManager not started properly')
        return false
    end

    return true
end

function setupController()
    if VAL_GLOBALS['valControllerManager_setup'] then
        rtt.logl('Info', 'setupController() has already been called')
        return true
    end

    VAL_GLOBALS['valControllerManager_setup'] = false

    if not deployController() then
        rtt.logl('Error', 'setupController(): deployController() returned false')
        return false
    end

    if not connectController() then
        rtt.logl('Error', 'setupController(): connectController() returned false')
        return false
    end

    if not configureController() then
        rtt.logl('Error', 'setupController(): configureController() returned false')
        return false
    end

    if not scheduleController() then
        rtt.logl('Error', 'setupController(): scheduleController() returned false')
        return false
    end

    if not startController() then
        rtt.logl('Error', 'setupController(): startController() returned false')
        return false
    end

    VAL_GLOBALS['valControllerManager_setup'] = true

    return true
end
