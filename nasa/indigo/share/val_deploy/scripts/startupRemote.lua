require 'rttros'

-- Fancy arg parsing (getopt-like)
function getopt( arg, options )
  local tab = {}
  for k, v in ipairs(arg) do
    if string.sub( v, 1, 2) == "--" then
      local x = string.find( v, "=", 1, true )
      if x then tab[ string.sub( v, 3, x-1 ) ] = string.sub( v, x+1 )
      else      tab[ string.sub( v, 3 ) ] = true
      end
    elseif string.sub( v, 1, 1 ) == "-" then
      local y = 2
      local l = string.len(v)
      local jopt
      while ( y <= l ) do
        jopt = string.sub( v, y, y )
        if string.find( options, jopt, 1, true ) then
          if y < l then
            tab[ jopt ] = string.sub( v, y+1 )
            y = l
          else
            tab[ jopt ] = arg[ k + 1 ]
          end
        else
          tab[ jopt ] = true
        end
        y = y + 1
      end
    end
  end
  return tab
end

-- Grab and processes execution arguments
opts = getopt( arg, "s" )

if (opts["s"] == nil) then
    print("Missing required argument!  syntax should be: rttlua [-i] startupRemote.lua -s startup-file-name-without-extension")
    os.exit()
end

-- Load requested startup file
require(opts["s"])

-- When running rttlua via a Popen call without a controlling terminal, interactive ("-i") mode
-- will immediately exit because it receives an EOF when it tries to read from stdin as part of
-- its prompt.

-- A loop and signal handler are needed to allow the process to continue running and still be
-- killable via a SIGTERM (ctrl+c).

-- $ sudo luarocks install luaposix
local P = require 'posix'
P.signal(P.SIGINT, function() exit() end)

while 1 do
    P.sleep(1)
end