rtt.setLogLevel('Warning')

require 'val_utilities'

VAL_GLOBALS['HIGH_PRIORITY'] = 99
VAL_GLOBALS['STD_PRIORITY'] = 50
VAL_GLOBALS['LOW_PRIORITY'] = 1

VAL_GLOBALS['controller_trigger'] = 0
VAL_GLOBALS['controller_rate'] = 500

-- Start Deployer
tc = rtt.getTC()
depl = tc:getPeer("Deployer")

-- Get ROS bindings set up
require 'rttros'
depl:import("rtt_ros")
rtt.provides("ros"):import("rtt_rospack")

-- Check for RT kernel
output = os.capture("uname -a | grep 'PREEMPT' | wc -l", false)
if (output == "1") then
    VAL_GLOBALS['REAL_TIME'] = true
    rtt.logl('Warning', 'Using RT threads.')
end

function exit()
    rtt.logl('Info', 'Shutting down')
    os.execute("sleep 5")
    depl:kickOutAll()
    os.exit()
end

-- Deploy Timer
require 'setupTimer'
if not setupTimer() then
    exit()
end

-- Deploy Val Controller
require 'setupController'
if setupController() then
    timer:startTimer(VAL_GLOBALS['controller_trigger'], 1.0 / VAL_GLOBALS['controller_rate'])
end