//
// Created by jordan on 5/10/16.
//

#ifndef __SMT_VARIABLE_FACTORY_HPP__
#define __SMT_VARIABLE_FACTORY_HPP__

#include "robot_variable/RobotVariableFactory.hpp"
#include "shared_memory_transport/smtclient.hpp"
#include "robot_variable_smt/SmtVariable.hpp"

namespace robot_variable_smt
{

    class SmtVariableFactory : public robot_variable::RobotVariableFactory
    {
    public:
        SmtVariableFactory(SMT::SMTClient *smtClient) : RobotVariableFactory()
        {
            this->smtClient = smtClient;
        }

        virtual ~SmtVariableFactory()
        {

        }

        std::shared_ptr<robot_variable::RobotVariable<double>> makeDoubleVariable(const std::string name)
        {
            std::shared_ptr<robot_variable::RobotVariable<double>> smtVariable(new robot_variable_smt::SmtVariable<double>(smtClient,name));
            return smtVariable;
        }

        std::shared_ptr<robot_variable::RobotVariable<float>> makeFloatVariable(const std::string name)
        {
            std::shared_ptr<robot_variable::RobotVariable<float>> smtVariable(new robot_variable_smt::SmtVariable<float>(smtClient,name));
            return smtVariable;
        }

        std::shared_ptr<robot_variable::RobotVariable<int>> makeInt32Variable(const std::string name)
        {
            std::shared_ptr<robot_variable::RobotVariable<int>> smtVariable(new robot_variable_smt::SmtVariable<int>(smtClient,name));
            return smtVariable;
        }

        std::shared_ptr<robot_variable::RobotVariable<uint32_t>> makeUnsignedInt32Variable(const std::string name)
        {
            std::shared_ptr<robot_variable::RobotVariable<uint32_t>> smtVariable(new robot_variable_smt::SmtVariable<uint32_t>(smtClient,name));
            return smtVariable;
        }

        std::shared_ptr<robot_variable::RobotVariable<int16_t>> makeInt16Variable(const std::string name)
        {
            std::shared_ptr<robot_variable::RobotVariable<int16_t>> smtVariable(new robot_variable_smt::SmtVariable<int16_t>(smtClient,name));
            return smtVariable;
        }

        std::shared_ptr<robot_variable::RobotVariable<uint16_t>> makeUnsignedInt16Variable(const std::string name)
        {
            std::shared_ptr<robot_variable::RobotVariable<uint16_t>> smtVariable(new robot_variable_smt::SmtVariable<uint16_t>(smtClient,name));
            return smtVariable;
        }

    protected:
        SMT::SMTClient* smtClient;

    };

}

#endif //__SMT_VARIABLE_FACTORY_HPP__
