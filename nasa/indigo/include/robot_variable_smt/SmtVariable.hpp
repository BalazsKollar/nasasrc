
#ifndef __SMT_VARIABLE_HPP__
#define __SMT_VARIABLE_HPP__

/**
 * The SmtVariable class is a template for creating SmtVariables.
 * It also provides some simple methods for arithmetic. This class
 * creates a shared memory resource and manages a shared pointer to
 * the newly created resource.
 */

#include "shared_memory_transport/smtclient.hpp"
#include "robot_variable/RobotVariable.hpp"
#include "robot_variable/TypeNameUtils.hpp"
#include "nasa_common_logging/Logger.h"
#include <stdexcept>

namespace robot_variable_smt
{
    template<typename T>
    class SmtVariable : public robot_variable::RobotVariable<T>
    {
    public:
        /**
         * Constructor that handles the creation
         * @param smtClient raw pointer to an SMTClient
         * @param name The variable name that will show up in shared memory
         * @param offset The shared memory offset
         * @param endian Endianness of the variable to be added, default is big
         */
        SmtVariable(SMT::SMTClient* smtClient, const std::string& name, const int offset = 0, const std::string& endian = "big")  : robot_variable::RobotVariable<T>(name)
        {
            if(!smtClient)
            {
                throw std::runtime_error("SMTClient pointer is NULL!");
            }

            this->smtClient = smtClient;
            this->name = name;

            createResource(this->name, offset, endian);
        }

        virtual ~SmtVariable()
        {
            
        }

        /**
         * Constructor that allows the variable to be initialized to some value
         * @param smtClient raw pointer to an SMTClient
         * @param name The variable name that will show up in shared memory
         * @param node The node to add the variable to
         * @param initialValue The value to initialize the shared memory variable to
         * @param offset The shared memory offset
         * @param endian Endianness of the variable to be added, default is big
         */
        SmtVariable(SMT::SMTClient *smtClient, const std::string& name, const T initialValue, const int offset = 0, const std::string& endian = "big") : SmtVariable(smtClient, name, offset, endian)
        {

            set(initialValue);
        }

        /**
         * Set the value of this SmtVariable
         * @param value The value to set this SmtVariable to
         */
        void set(const T value, const bool callVariableChangedCallbacks=true)
        {
            if(value != resource->get<T>())
            {
                this->value = value;
                resource->set<T>(value);

                if(callVariableChangedCallbacks)
                {
                    this->notifyVariableChangedListeners();
                }
            }
        }

        /**
         * Get the value of this SmtVariable
         */
        T get() const
        {
            return resource->get<T>();
        }

        /**
         * Method for getting the variable type
         * @return Variable type as string
         */
        std::string getType() const
        {
            return robot_variable::TypeName<T>::getTemplateType();
        }

    protected:
        std::shared_ptr<SMT::Resource> resource;
        SMT::SMTClient *smtClient;

    private:
        /**
         * This method does the work of creating the shared memory resource
         */
        void createResource(const std::string& name, const int offset = 0, const std::string& endian = "big")
        {
            try
            {
                resource.reset(new SMT::Resource(smtClient->createResource(name, robot_variable::TypeName<T>::getTemplateType(), offset, sizeof(T), endian, "")));
            }
            catch(SMT::SMTException &e)
            {
                resource.reset(new SMT::Resource(smtClient->getResourceInfo(name)));
                this->value = resource->get<T>();
            }
        }
    };
}

#endif