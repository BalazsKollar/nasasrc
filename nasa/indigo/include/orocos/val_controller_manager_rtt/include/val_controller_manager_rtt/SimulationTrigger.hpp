#ifndef MANAGER_SIMULATION_TRIGGER_HPP
#define MANAGER_SIMULATION_TRIGGER_HPP

#include "robot_variable_smt/SmtVariableFactory.hpp"
#include <rtt/RTT.hpp>
#include <ros/ros.h>
#include <memory>

namespace simulation_trigger_component
{
    class SimulationTrigger : public RTT::TaskContext
    {
    public:
        SimulationTrigger(const std::string& name);
        ~SimulationTrigger();

        // Required orocos classes inherited from RTT::TaskContext
        bool configureHook();
        bool startHook();
        void updateHook();
        void stopHook();
        void exceptionHook();

        void timerTrigger(const ros::TimerEvent& event);
        void startTimer(int32_t trigID, double dura);

    private:

        std::shared_ptr<SMT::SMTClient> smtClient;
        std::shared_ptr<robot_variable::RobotVariableFactory> robotVariableFactory;

        std::shared_ptr<robot_variable::RobotVariable<int16_t>> simulationTrigger;

        RTT::InputPort<int32_t> triggerPort;
    };

}

#endif