#ifndef VAL_SIM_CONTROLLER_MANAGER_HPP
#define VAL_SIM_CONTROLLER_MANAGER_HPP

#include <controller_manager/controller_manager.h>
#include "val_hardware_interface/Robot_Hardware_Interface.hpp"
#include <realtime_tools/realtime_publisher.h>
#include <std_msgs/Float64.h>
#include <rtt/RTT.hpp>
#include <ros/ros.h>
#include <memory>

namespace val_sim_controller_manager
{
    class ValSimControllerManager : public RTT::TaskContext
    {
    public:
        ValSimControllerManager(const std::string& name);
        ~ValSimControllerManager();

        // Required orocos classes inherited from RTT::TaskContext
        bool configureHook();
        bool startHook();
        void updateHook();
        void stopHook();
        void exceptionHook();

    private:
        std::shared_ptr<SMT::SMTClient> smtClient;
        std::shared_ptr<robot_variable::RobotVariableFactory> robotVariableFactory;
        // Real time orocos ports

        // pointer to controller manager
        std::shared_ptr<controller_manager::ControllerManager> controllerManager;

        // pointer to hardware interface. Needs to be in a namespace for easy reading.
        std::shared_ptr<RobotHardwareInterface> hardwareInterface;

        // ROS required components
        ros::NodeHandle rosNode;
        ros::Time lastUpdateTime;

        int32_t heartBeat;

        //ros realtime publishers (sort of)
        std::unique_ptr<realtime_tools::RealtimePublisher<std_msgs::Float64>> rttPeriodOverflowPublisher, rttRatePublisher;
        RTT::InputPort<int32_t> triggerPort;
        RTT::OutputPort<int32_t> heartBeatPort;
    };

}

#endif