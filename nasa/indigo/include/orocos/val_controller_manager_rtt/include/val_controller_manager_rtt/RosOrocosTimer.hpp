#ifndef ROS_OROCOS_TIMER_HPP
#define ROS_OROCOS_TIMER_HPP

#include <rtt/RTT.hpp>
#include <ros/ros.h>
#include <memory>

namespace ros_time_component
{
    class RosOrocosTimer : public RTT::TaskContext
    {
    public:
        RosOrocosTimer(const std::string& name);
        ~RosOrocosTimer();

        // Required orocos classes inherited from RTT::TaskContext
        bool configureHook();
        bool startHook();
        void updateHook();
        void stopHook();
        void exceptionHook();

        void timerTrigger(const ros::TimerEvent& event);
        void startTimer(int32_t trigID, double dura);

    private:

        // Real time orocos ports

        // ROS required components
        ros::NodeHandle rosNode;
        ros::Timer timer_;
        double rateTime;
        std::map<std::string, std::string> emptyArgs;
        int32_t triggerID;

        RTT::OutputPort<int32_t> timeTriggerPort;

    };

}

#endif