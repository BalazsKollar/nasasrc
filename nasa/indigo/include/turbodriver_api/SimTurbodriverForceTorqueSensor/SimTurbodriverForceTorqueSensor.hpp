#ifndef __SimTurbodriverForceTorqueSensor__HPP__
#define __SimTurbodriverForceTorqueSensor__HPP__

// Includes:
#include "register.hpp"
#include <shared_memory_transport/smtclient.hpp>

//Namespaces:
namespace smt_interface{

class SimTurbodriverForceTorqueSensor
{
public:
//Contructors
	SimTurbodriverForceTorqueSensor();
	SimTurbodriverForceTorqueSensor(const std::string &nodePath, SMT::SMTClient* smtClient);
//Destructor
	~SimTurbodriverForceTorqueSensor();
//Subscribe function
	void Subscribe(const std::string &nodePath, SMT::SMTClient* smtClient);
//API Functions
	std::string getNodePath() const;
	void getSixAxis01(float &val);
	void getSixAxis02(float &val);
	void getSixAxis03(float &val);
	void getSixAxis04(float &val);
	void getSixAxis05(float &val);
	void getSixAxis06(float &val);
private:
	std::string nodePath;
	SMT::SMTClient* client;
//Commands in the command dictionary
//Subscriber declarations
protected:
	std::shared_ptr<SMT::Resource> subscriberSix_Axis_01;
	std::shared_ptr<SMT::Resource> subscriberSix_Axis_02;
	std::shared_ptr<SMT::Resource> subscriberSix_Axis_03;
	std::shared_ptr<SMT::Resource> subscriberSix_Axis_04;
	std::shared_ptr<SMT::Resource> subscriberSix_Axis_05;
	std::shared_ptr<SMT::Resource> subscriberSix_Axis_06;
};
//Close namespaces
}
#endif // __SimTurbodriverForceTorqueSensor__HPP__
