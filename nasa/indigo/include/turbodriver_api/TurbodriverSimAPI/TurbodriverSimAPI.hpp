#ifndef __TurbodriverSimAPI__HPP__
#define __TurbodriverSimAPI__HPP__

// Includes:
#include "register.hpp"
#include <shared_memory_transport/smtclient.hpp>
#include <turbodriver_api/TurbodriverBase.hpp>

//Namespaces:
namespace turbodriver_api{

class TurbodriverSimAPI: public TurbodriverSimApiBase
{
public:
//Contructors
	TurbodriverSimAPI();
	TurbodriverSimAPI(const std::string &nodePath, SMT::SMTClient* smtClient);
//Destructor
	~TurbodriverSimAPI();
//Subscribe function
	void Subscribe(const std::string &nodePath, SMT::SMTClient* smtClient);
//API Functions
	std::string getNodePath() const;
	void getEffort(float &val);
	void getEffortCommand(float &val);
	void getPosition(float &val);
	void getPositionCommand(float &val);
	void getVelocity(float &val);
	void getVelocityCommand(float &val);
	void setEffort(float &val);
	void setEffortCommand(float &val);
	void setPosition(float &val);
	void setPositionCommand(float &val);
	void setVelocity(float &val);
	void setVelocityCommand(float &val);
private:
	std::string nodePath;
	SMT::SMTClient* client;
//Commands in the command dictionary
//Subscriber declarations
protected:
	std::shared_ptr<SMT::Resource> subscriberJointAPS_Angle_Rad;
	std::shared_ptr<SMT::Resource> subscriberJointAPS_Vel_Radps;
	std::shared_ptr<SMT::Resource> subscriberJointTorque_Des_Nm;
	std::shared_ptr<SMT::Resource> subscriberJointTorque_Meas_Nm;
	std::shared_ptr<SMT::Resource> subscriberPosition_Des_Rad;
	std::shared_ptr<SMT::Resource> subscriberVelocity_Des_Radps;
};
//Close namespaces
}
#endif // __TurbodriverSimAPI__HPP__
