#ifndef __TurbodriverForceTorqueSensor__HPP__
#define __TurbodriverForceTorqueSensor__HPP__

// Includes:
#include "register.hpp"
#include <shared_memory_transport/smtclient.hpp>

//Namespaces:
namespace smt_interface{

class TurbodriverForceTorqueSensor
{
public:
//Contructors
	TurbodriverForceTorqueSensor();
	TurbodriverForceTorqueSensor(const std::string &nodePath, SMT::SMTClient* smtClient);
//Destructor
	~TurbodriverForceTorqueSensor();
//Subscribe function
	void Subscribe(const std::string &nodePath, SMT::SMTClient* smtClient);
//API Functions
	std::string getNodePath() const;
	void getSixAxis01(int16_t &val);
	void getSixAxis02(int16_t &val);
	void getSixAxis03(int16_t &val);
	void getSixAxis04(int16_t &val);
	void getSixAxis05(int16_t &val);
	void getSixAxis06(int16_t &val);
private:
	std::string nodePath;
	SMT::SMTClient* client;
//Commands in the command dictionary
//Subscriber declarations
protected:
	std::shared_ptr<SMT::Resource> subscriberSix_Axis_01;
	std::shared_ptr<SMT::Resource> subscriberSix_Axis_02;
	std::shared_ptr<SMT::Resource> subscriberSix_Axis_03;
	std::shared_ptr<SMT::Resource> subscriberSix_Axis_04;
	std::shared_ptr<SMT::Resource> subscriberSix_Axis_05;
	std::shared_ptr<SMT::Resource> subscriberSix_Axis_06;
};
//Close namespaces
}
#endif // __TurbodriverForceTorqueSensor__HPP__
