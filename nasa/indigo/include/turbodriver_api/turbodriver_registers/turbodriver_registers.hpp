#ifndef _TURBODRIVER_REGISTERS_
#define _TURBODRIVER_REGISTERS_

#include "register.hpp"

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
uint16_t StatReg1_applyCertificate(uint16_t val);

//
void StatReg1_clear(SMT::Resource *resA);


std::vector<std::string> StatReg1_getFieldNames();

// 0=disable,1=enable
void StatReg1_setMotorEnable(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg1_getMotorEnable(SMT::Resource *resA);
// 0=engage,1=release
void StatReg1_setBrakeRelease(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg1_getBrakeRelease(SMT::Resource *resA);
// 1=reset
void StatReg1_setProcReset(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg1_getProcReset(SMT::Resource *resA);
// 0=fine,1=course
void StatReg1_setCourseMode(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg1_getCourseMode(SMT::Resource *resA);
// 0=disable,1=enable
void StatReg1_setBridgeEnable(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg1_getBridgeEnable(SMT::Resource *resA);
// 1=clear
void StatReg1_setClearEncoder(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg1_getClearEncoder(SMT::Resource *resA);
// 0=brainstem,1=embedded
void StatReg1_setMotComSource(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg1_getMotComSource(SMT::Resource *resA);
// 0=6s2q,1=6s4q,2=vector
void StatReg1_setCommutationSel(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg1_getCommutationSel(SMT::Resource *resA);
// 1=System OK, 0=Estopped
void StatReg1_setEstopOK(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg1_getEstopOK(SMT::Resource *resA);
// Echo of Ctrl bit
void StatReg1_setEstopReset(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg1_getEstopReset(SMT::Resource *resA);
// 1=Current Limiting
void StatReg1_setCurrentLimit(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg1_getCurrentLimit(SMT::Resource *resA);
// 0=notDetected,1=detected
void StatReg1_setMotorPowerDetected(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg1_getMotorPowerDetected(SMT::Resource *resA);
// 0=clear,1=fault
void StatReg1_setDriveModFault(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg1_getDriveModFault(SMT::Resource *resA);
// 1=alive
void StatReg1_setProcAlive(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg1_getProcAlive(SMT::Resource *resA);
uint16_t StatReg2_applyCertificate(uint16_t val);

//
void StatReg2_clear(SMT::Resource *resA);


std::vector<std::string> StatReg2_getFieldNames();

// 0=clear,1=fault
void StatReg2_setOverTempFault(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg2_getOverTempFault(SMT::Resource *resA);
// 0=clear,1=fault
void StatReg2_setSpringDefFault(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg2_getSpringDefFault(SMT::Resource *resA);
// 0=clear,1=fault
void StatReg2_setApsFault(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg2_getApsFault(SMT::Resource *resA);
// 0=clear,1=fault
void StatReg2_setLimitFault(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg2_getLimitFault(SMT::Resource *resA);
// 0=clear,1=fault
void StatReg2_setTorqueLimitFault(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg2_getTorqueLimitFault(SMT::Resource *resA);
// 0=clear,1=fault
void StatReg2_setEncDriftFault(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg2_getEncDriftFault(SMT::Resource *resA);
// 0=clear,1=fault
void StatReg2_setJerkFault(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg2_getJerkFault(SMT::Resource *resA);
// 0=clear,1=fault
void StatReg2_setJointFault(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg2_getJointFault(SMT::Resource *resA);
// 0=clear,1=fault
void StatReg2_setChecksumFault(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg2_getChecksumFault(SMT::Resource *resA);
// 0=clear,1=fault
void StatReg2_setCommFault(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg2_getCommFault(SMT::Resource *resA);
// 0=clear,1=fault
void StatReg2_setVelocityFault(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg2_getVelocityFault(SMT::Resource *resA);
// 1=In bootloader
void StatReg2_setBootloaderMode(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg2_getBootloaderMode(SMT::Resource *resA);
// 0=clear,1=fault
void StatReg2_setAps1TolFault(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg2_getAps1TolFault(SMT::Resource *resA);
// 0=clear,1=fault
void StatReg2_setAps2TolFault(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg2_getAps2TolFault(SMT::Resource *resA);
// 0=false,1=true
void StatReg2_setRotorInitDone(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg2_getRotorInitDone(SMT::Resource *resA);
// 0=run,1=calibrate
void StatReg2_setCalibrationMode(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg2_getCalibrationMode(SMT::Resource *resA);
uint16_t StatReg3_applyCertificate(uint16_t val);

//
void StatReg3_clear(SMT::Resource *resA);


std::vector<std::string> StatReg3_getFieldNames();

// 0=disable,1=enable
void StatReg3_setInitAPS2toAPS1(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg3_getInitAPS2toAPS1(SMT::Resource *resA);
// 0=disable,1=enable
void StatReg3_setEnableBiasCompAPS1(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg3_getEnableBiasCompAPS1(SMT::Resource *resA);
// 0=disable,1=enable
void StatReg3_setEnableBiasCompAPS2(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg3_getEnableBiasCompAPS2(SMT::Resource *resA);
// 0=encoder only,1=blend w/ APS1
void StatReg3_setBlendAngleSensors(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg3_getBlendAngleSensors(SMT::Resource *resA);
// 0=Raw PD, 1=PD with FF
void StatReg3_setEnableFFPosCom(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg3_getEnableFFPosCom(SMT::Resource *resA);
// 0=joint vel,1=error dot
void StatReg3_setUseErrorDot(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg3_getUseErrorDot(SMT::Resource *resA);
// 0=motor pos,1=joint pos
void StatReg3_setUseJointPos(SMT::Resource *resA, uint32_t valA);
uint32_t StatReg3_getUseJointPos(SMT::Resource *resA);
uint16_t CtrlReg1_applyCertificate(uint16_t val);

//
void CtrlReg1_clear(SMT::Resource *resA);


std::vector<std::string> CtrlReg1_getFieldNames();

// Used to set value of entire lower byte.
void CtrlReg1_setMode(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg1_getMode(SMT::Resource *resA);
// 0=disable,1=enable
void CtrlReg1_setMotorEnable(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg1_getMotorEnable(SMT::Resource *resA);
// 0=engage,1=release
void CtrlReg1_setBrakeRelease(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg1_getBrakeRelease(SMT::Resource *resA);
// 1=reset
void CtrlReg1_setProcReset(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg1_getProcReset(SMT::Resource *resA);
// 0=fine,1=course
void CtrlReg1_setCourseMode(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg1_getCourseMode(SMT::Resource *resA);
// 0=disable,1=enable
void CtrlReg1_setBridgeEnable(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg1_getBridgeEnable(SMT::Resource *resA);
// 1=clear
void CtrlReg1_setClearEncoder(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg1_getClearEncoder(SMT::Resource *resA);
// 0=brainstem,1=embedded
void CtrlReg1_setMotComSource(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg1_getMotComSource(SMT::Resource *resA);
// 0=6s2q,1=6s4q,2=vector
void CtrlReg1_setCommutationSel(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg1_getCommutationSel(SMT::Resource *resA);
// 0-1 Rising Edge Resets Estop
void CtrlReg1_setEstopReset(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg1_getEstopReset(SMT::Resource *resA);
uint16_t CtrlReg3_applyCertificate(uint16_t val);

//
void CtrlReg3_clear(SMT::Resource *resA);


std::vector<std::string> CtrlReg3_getFieldNames();

// 0=disable,1=enable
void CtrlReg3_setInitAPS2toAPS1(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg3_getInitAPS2toAPS1(SMT::Resource *resA);
// 0=disable,1=enable
void CtrlReg3_setEnableBiasCompAPS1(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg3_getEnableBiasCompAPS1(SMT::Resource *resA);
// 0=disable,1=enable
void CtrlReg3_setEnableBiasCompAPS2(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg3_getEnableBiasCompAPS2(SMT::Resource *resA);
// 0=encoder only,1=blend w/ APS1
void CtrlReg3_setBlendAngleSensors(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg3_getBlendAngleSensors(SMT::Resource *resA);
// 0=Raw PD, 1=PD with FF
void CtrlReg3_setEnableFFPosCom(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg3_getEnableFFPosCom(SMT::Resource *resA);
// 0=joint vel,1=error dot
void CtrlReg3_setUseErrorDot(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg3_getUseErrorDot(SMT::Resource *resA);
// 0=motor pos,1=joint pos
void CtrlReg3_setUseJointPos(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg3_getUseJointPos(SMT::Resource *resA);
uint16_t CtrlReg2_applyCertificate(uint16_t val);

//
void CtrlReg2_clear(SMT::Resource *resA);


std::vector<std::string> CtrlReg2_getFieldNames();

// 1=Disable faults specified by coeff file
void CtrlReg2_setDisableFaults(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg2_getDisableFaults(SMT::Resource *resA);
// 0=curr,1=trq,2=pos,3=vel,4=imp
void CtrlReg2_setControlMode(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg2_getControlMode(SMT::Resource *resA);
// 0=No Smoothing, 1=Sliding DB
void CtrlReg2_setSlidingDB(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg2_getSlidingDB(SMT::Resource *resA);
// 1=clear fault
void CtrlReg2_setClearFault(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg2_getClearFault(SMT::Resource *resA);
// 0=run,1=calibrate
void CtrlReg2_setCalibrationMode(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg2_getCalibrationMode(SMT::Resource *resA);
// 0=disable, 1=enable
void CtrlReg2_setStreamMode(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg2_getStreamMode(SMT::Resource *resA);
// 1=reset
void CtrlReg2_setStreamModeReset(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg2_getStreamModeReset(SMT::Resource *resA);
// 1=Stay in bootloader on reset
void CtrlReg2_setBootloaderMode(SMT::Resource *resA, uint32_t valA);
uint32_t CtrlReg2_getBootloaderMode(SMT::Resource *resA);
uint16_t fabricCtrlReg_applyCertificate(uint16_t val);

//
void fabricCtrlReg_clear(SMT::Resource *resA);


std::vector<std::string> fabricCtrlReg_getFieldNames();

// 0=disable,1=enable
void fabricCtrlReg_setFaultedBit(SMT::Resource *resA, uint32_t valA);
uint32_t fabricCtrlReg_getFaultedBit(SMT::Resource *resA);
// 0=disable,1=enable
void fabricCtrlReg_setEnableCoarse(SMT::Resource *resA, uint32_t valA);
uint32_t fabricCtrlReg_getEnableCoarse(SMT::Resource *resA);
// 0=Raw PD, 1=PD with FF
void fabricCtrlReg_setSetBrake(SMT::Resource *resA, uint32_t valA);
uint32_t fabricCtrlReg_getSetBrake(SMT::Resource *resA);
uint16_t BrakePWM_applyCertificate(uint16_t val);

//
void BrakePWM_clear(SMT::Resource *resA);


std::vector<std::string> BrakePWM_getFieldNames();

// Used to set value of entire lower byte.
void BrakePWM_setDutyCycle(SMT::Resource *resA, uint32_t valA);
uint32_t BrakePWM_getDutyCycle(SMT::Resource *resA);

#ifdef __cplusplus
}
#endif
#endif // _TURBODRIVER_REGISTERS_
