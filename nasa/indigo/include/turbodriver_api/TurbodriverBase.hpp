#ifndef TURBODRIVER_BASE_HPP
#define TURBODRIVER_BASE_HPP

namespace turbodriver_api
{
    class TurbodriverApiBase
    {
    public:
        TurbodriverApiBase() {}
        virtual ~TurbodriverApiBase() {}

        virtual void Subscribe(const std::string& nodePath, SMT::SMTClient* smtClient) = 0;
        virtual void clearBootloaderMode() = 0;
        virtual void clearFault() = 0;
        virtual void clearProcReset() = 0;
        virtual void deltaHysteresis() = 0;
        virtual void disableFaults() = 0;
        virtual void disableMotor() = 0;
        virtual void enableFaults() = 0;
        virtual void enableMotor() = 0;
        virtual void enterCalMode() = 0;
        virtual void exitCalMode() = 0;
        virtual void getAps1TolFault(uint32_t& val) = 0;
        virtual void getAps2TolFault(uint32_t& val) = 0;
        virtual void getApsFault(uint32_t& val) = 0;
        virtual void getBootloaderMode(uint32_t& val) = 0;
        virtual void getBrakeRelease(uint32_t& val) = 0;
        virtual void getBridgeEnable(uint32_t& val) = 0;
        virtual void getBridgeTemp_C(uint16_t& val) = 0;
        virtual void getCalibrationMode(uint32_t& val) = 0;
        virtual void getChecksumFault(uint32_t& val) = 0;
        virtual void getClearEncoder(uint32_t& val) = 0;
        virtual void getClearFaultCmd(uint32_t& val) = 0;
        virtual void getCommFault(uint32_t& val) = 0;
        virtual void getCommutationSel(uint32_t& val) = 0;
        virtual void getControlModeCmd(uint32_t& val) = 0;
        virtual void getDisableFaults(uint32_t& val) = 0;
        virtual void getDriveModFault(uint32_t& val) = 0;
        virtual void getEffort(float& val) = 0;
        virtual void getEffortCommand(float& val) = 0;
        virtual void getEncDriftFault(uint32_t& val) = 0;
        virtual void getEstopOK(uint32_t& val) = 0;
        virtual void getJerkFault(uint32_t& val) = 0;
        virtual void getJointFault(uint32_t& val) = 0;
        virtual void getLimitFault(uint32_t& val) = 0;
        virtual void getLogicCardTemp_C(int16_t& val) = 0;
        virtual void getMotComSource(uint32_t& val) = 0;
        virtual void getMotComSourceCmd(uint32_t& val) = 0;
        virtual void getMotorEnable(uint32_t& val) = 0;
        virtual void getMotorEnableCmd(uint32_t& val) = 0;
        virtual void getMotorPowerDetected(uint32_t& val) = 0;
        virtual void getMotorTemp_C(uint16_t& val) = 0;
        virtual void getOverTempFault(uint32_t& val) = 0;
        virtual void getPosition(float& val) = 0;
        virtual void getPositionCommand(float& val) = 0;
        virtual void getProcAlive(uint32_t& val) = 0;
        virtual void getProcHeartbeat(uint16_t& val) = 0;
        virtual void getProcReset(uint32_t& val) = 0;
        virtual void getRotorInitDone(uint32_t& val) = 0;
        virtual void getSpringDefFault(uint32_t& val) = 0;
        virtual void getTorqueLimitFault(uint32_t& val) = 0;
        virtual void getVelocity(float& val) = 0;
        virtual void getVelocityCommand(float& val) = 0;
        virtual void getVelocityFault(uint32_t& val) = 0;
        virtual void impedanceMode() = 0;
        virtual void park() = 0;
        virtual void positionMode() = 0;
        virtual void setBootloaderMode() = 0;
        virtual void setEffortCommand(float& val) = 0;
        virtual void setPositionCommand(float& val) = 0;
        virtual void setProcReset() = 0;
        virtual void setVelocityCommand(float& val) = 0;
        virtual void spaceVector() = 0;
        virtual void torqueMode() = 0;
        virtual void zeroEffortCommand() = 0;
    };

    class TurbodriverSimApiBase
    {
    public:
        TurbodriverSimApiBase() {}
        virtual ~TurbodriverSimApiBase() {}

        virtual void Subscribe(const std::string& nodePath, SMT::SMTClient* smtClient) = 0;
        virtual void getEffort(float& val) = 0;
        virtual void getEffortCommand(float& val) = 0;
        virtual void getPosition(float& val) = 0;
        virtual void getPositionCommand(float& val) = 0;
        virtual void getVelocity(float& val) = 0;
        virtual void getVelocityCommand(float& val) = 0;
        virtual void setEffort(float& val) = 0;
        virtual void setEffortCommand(float& val) = 0;
        virtual void setPosition(float& val) = 0;
        virtual void setPositionCommand(float& val) = 0;
        virtual void setVelocity(float& val) = 0;
        virtual void setVelocityCommand(float& val) = 0;
    };
}

#endif