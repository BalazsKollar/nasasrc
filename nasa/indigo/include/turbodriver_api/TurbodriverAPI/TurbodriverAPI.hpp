#ifndef __TurbodriverAPI__HPP__
#define __TurbodriverAPI__HPP__

// Includes:
#include "register.hpp"
#include <shared_memory_transport/smtclient.hpp>
#include <turbodriver_api/TurbodriverBase.hpp>

//Namespaces:
namespace turbodriver_api{

class TurbodriverAPI: public TurbodriverApiBase
{
public:
//Contructors
	TurbodriverAPI();
	TurbodriverAPI(const std::string &nodePath, SMT::SMTClient* smtClient);
//Destructor
	~TurbodriverAPI();
//Subscribe function
	void Subscribe(const std::string &nodePath, SMT::SMTClient* smtClient);
//API Functions
	std::string getNodePath() const;
	void clearBootloaderMode();
	void clearFault();
	void clearProcReset();
	void deltaHysteresis();
	void disableFaults();
	void disableMotor();
	void enableFaults();
	void enableMotor();
	void enterCalMode();
	void exitCalMode();
	void getAps1TolFault(uint32_t &val);
	void getAps2TolFault(uint32_t &val);
	void getApsFault(uint32_t &val);
	void getBootloaderMode(uint32_t &val);
	void getBrakeRelease(uint32_t &val);
	void getBridgeEnable(uint32_t &val);
	void getBridgeTemp_C(uint16_t &val);
	void getCalibrationMode(uint32_t &val);
	void getChecksumFault(uint32_t &val);
	void getClearEncoder(uint32_t &val);
	void getClearFaultCmd(uint32_t &val);
	void getCommFault(uint32_t &val);
	void getCommutationSel(uint32_t &val);
	void getControlModeCmd(uint32_t &val);
	void getDisableFaults(uint32_t &val);
	void getDriveModFault(uint32_t &val);
	void getEffort(float &val);
	void getEffortCommand(float &val);
	void getEncDriftFault(uint32_t &val);
	void getEstopOK(uint32_t &val);
	void getJerkFault(uint32_t &val);
	void getJointFault(uint32_t &val);
	void getLimitFault(uint32_t &val);
	void getLogicCardTemp_C(int16_t &val);
	void getMotComSource(uint32_t &val);
	void getMotComSourceCmd(uint32_t &val);
	void getMotorEnable(uint32_t &val);
	void getMotorEnableCmd(uint32_t &val);
	void getMotorPowerDetected(uint32_t &val);
	void getMotorTemp_C(uint16_t &val);
	void getOverTempFault(uint32_t &val);
	void getPosition(float &val);
	void getPositionCommand(float &val);
	void getProcAlive(uint32_t &val);
	void getProcHeartbeat(uint16_t &val);
	void getProcReset(uint32_t &val);
	void getRotorInitDone(uint32_t &val);
	void getSpringDefFault(uint32_t &val);
	void getTorqueLimitFault(uint32_t &val);
	void getVelocity(float &val);
	void getVelocityCommand(float &val);
	void getVelocityFault(uint32_t &val);
	void impedanceMode();
	void park();
	void positionMode();
	void setBootloaderMode();
	void setEffortCommand(float &val);
	void setPositionCommand(float &val);
	void setProcReset();
	void setVelocityCommand(float &val);
	void spaceVector();
	void torqueMode();
	void zeroEffortCommand();
private:
	std::string nodePath;
	SMT::SMTClient* client;
//Commands in the command dictionary
	std::function<uint32_t()> getCtrlReg1MotComSourceCmd;
	std::function<uint32_t()> getCtrlReg1MotorEnableCmd;
	std::function<uint32_t()> getCtrlReg2ClearFaultCmd;
	std::function<uint32_t()> getCtrlReg2ControlModeCmd;
	std::function<uint32_t()> getCtrlReg2DisableFaultsCmd;
	std::function<uint32_t()> getStatReg1BrakeReleaseCmd;
	std::function<uint32_t()> getStatReg1BridgeEnableCmd;
	std::function<uint32_t()> getStatReg1ClearEncoderCmd;
	std::function<uint32_t()> getStatReg1CommutationSelCmd;
	std::function<uint32_t()> getStatReg1DriveModFaultCmd;
	std::function<uint32_t()> getStatReg1EstopOKCmd;
	std::function<uint32_t()> getStatReg1MotComSourceCmd;
	std::function<uint32_t()> getStatReg1MotorEnableCmd;
	std::function<uint32_t()> getStatReg1MotorPowerDetectedCmd;
	std::function<uint32_t()> getStatReg1ProcAliveCmd;
	std::function<uint32_t()> getStatReg1ProcResetCmd;
	std::function<uint32_t()> getStatReg2Aps1TolFaultCmd;
	std::function<uint32_t()> getStatReg2Aps2TolFaultCmd;
	std::function<uint32_t()> getStatReg2ApsFaultCmd;
	std::function<uint32_t()> getStatReg2BootloaderModeCmd;
	std::function<uint32_t()> getStatReg2CalibrationModeCmd;
	std::function<uint32_t()> getStatReg2ChecksumFaultCmd;
	std::function<uint32_t()> getStatReg2CommFaultCmd;
	std::function<uint32_t()> getStatReg2EncDriftFaultCmd;
	std::function<uint32_t()> getStatReg2JerkFaultCmd;
	std::function<uint32_t()> getStatReg2JointFaultCmd;
	std::function<uint32_t()> getStatReg2LimitFaultCmd;
	std::function<uint32_t()> getStatReg2OverTempFaultCmd;
	std::function<uint32_t()> getStatReg2RotorInitDoneCmd;
	std::function<uint32_t()> getStatReg2SpringDefFaultCmd;
	std::function<uint32_t()> getStatReg2TorqueLimitFaultCmd;
	std::function<uint32_t()> getStatReg2VelocityFaultCmd;
	std::function<void(uint32_t)> setCtrlReg1BrakeReleaseCmd;
	std::function<void(uint32_t)> setCtrlReg1BridgeEnableCmd;
	std::function<void(uint32_t)> setCtrlReg1ClearEncoderCmd;
	std::function<void(uint32_t)> setCtrlReg1CommutationSelCmd;
	std::function<void(uint32_t)> setCtrlReg1EstopResetCmd;
	std::function<void(uint32_t)> setCtrlReg1MotComSourceCmd;
	std::function<void(uint32_t)> setCtrlReg1MotorEnableCmd;
	std::function<void(uint32_t)> setCtrlReg1ProcResetCmd;
	std::function<void(uint32_t)> setCtrlReg2BootloaderModeCmd;
	std::function<void(uint32_t)> setCtrlReg2CalibrationModeCmd;
	std::function<void(uint32_t)> setCtrlReg2ClearFaultCmd;
	std::function<void(uint32_t)> setCtrlReg2ControlModeCmd;
	std::function<void(uint32_t)> setCtrlReg2DisableFaultsCmd;
	std::function<void(uint32_t)> setCtrlReg2StreamModeCmd;
	std::function<void(uint32_t)> setCtrlReg2StreamModeResetCmd;
	std::function<void(uint32_t)> setStatReg1ProcResetCmd;
//Subscriber declarations
protected:
	std::shared_ptr<SMT::Resource> subscriberBridgeTemp_C;
	std::shared_ptr<SMT::SMT_utils::Register> subscriberCtrlReg1;
	std::shared_ptr<SMT::SMT_utils::Register> subscriberCtrlReg2;
	std::shared_ptr<SMT::Resource> subscriberJointAPS_Angle_Rad;
	std::shared_ptr<SMT::Resource> subscriberJointAPS_Vel_Radps;
	std::shared_ptr<SMT::Resource> subscriberJointTorque_Des_Nm;
	std::shared_ptr<SMT::Resource> subscriberJointTorque_Meas_Nm;
	std::shared_ptr<SMT::Resource> subscriberLogicCardTemp_C;
	std::shared_ptr<SMT::Resource> subscriberMotorTemp_C;
	std::shared_ptr<SMT::Resource> subscriberPosition_Des_Rad;
	std::shared_ptr<SMT::Resource> subscriberProc_HeartBeat;
	std::shared_ptr<SMT::SMT_utils::Register> subscriberStatReg1;
	std::shared_ptr<SMT::SMT_utils::Register> subscriberStatReg2;
	std::shared_ptr<SMT::Resource> subscriberVelocity_Des_Radps;
};
//Close namespaces
}
#endif // __TurbodriverAPI__HPP__
