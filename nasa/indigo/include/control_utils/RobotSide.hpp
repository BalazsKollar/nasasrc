#ifndef __ROBOT_SIDE_HPP__
#define __ROBOT_SIDE_HPP__

/**
 * This class and its implementation is an adaptation of SideDependentList.java by Jerry Pratt
 * and the IHMC robotics group. All credit goes to them.
 */

#include <string>

namespace RobotSide
{
    enum Side
    {
        RIGHT,
        LEFT
    };

    static const Side values[] = {RIGHT, LEFT};

    static std::string getCamelCaseNameForStartOfExpression(Side side)
    {
        if (side == Side::LEFT)
        {
            return "left";
        }
        else
        {
            return "right";
        }
    }

    static std::string getCamelCaseNameForMiddleOfExpression(Side side)
    {
        if (side == Side::LEFT)
        {
            return "Left";
        }
        else
        {
            return "Right";
        }
    }
}

#endif //ROBOT_SIDE
