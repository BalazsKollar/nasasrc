
/**
 * A simple implementation of a minimum jerk trajectory
 */

#ifndef  __MINIMUMJERKTRAJECTORY_HPP__
#define  __MINIMUMJERKTRAJECTORY_HPP__

#include "nasa_common_logging/Logger.h"

namespace control_utils
{

    class MinimumJerkTrajectory
    {

    public:

        /**
         * Constructor
         */
        MinimumJerkTrajectory() : position(0.0), velocity(0.0), acceleration(0.0)
        {

        }

        /**
         * Get the trajectory's current position
         *
         * @return The current position
         */
        double getPosition() const
        {
            return position;
        }

        /**
         * Get the trajectory's current velocity
         *
         * @return The current velocity
         */
        double getVelocity() const
        {
            return velocity;
        }

        /**
         * Get the trajectory's current acceleration
         *
         * @return The current acceleration
         */
        double getAcceleration() const
        {
            return acceleration;
        }


        /**
         * Set the trajectories parameters. This must be called each time the trajectory is changed.
         *
         * @param T0 The initial time
         * @param Tf The final time
         * @param X0 The initial position
         * @param V0 The initial velocity
         * @param Xf The final position
         * @param A0 The initial acceleration. Default value is zero
         * @param Vf The final velocity. Default value is zero
         * @param Af The final acceleration. Default value is zero
         */
        void setTrajectoryParameters(const double &T0, const double &Tf, const double &X0, const double &V0,
                                     const double &Xf, const double &A0 = 0.0, const double &Vf = 0.0,
                                     const double &Af = 0.0)
        {
            if (Tf <= T0)
            {
                std::string msg = "Minimum jerk trajectory duration must be greater than zero!";
                NasaCommonLogging::Logger::log("gov.nasa.MinimumJerkTrajectory.setTrajectoryParameters",
                                               log4cpp::Priority::ERROR, msg);
                throw std::runtime_error(msg);
            }

            this->T0 = T0;
            this->Tf = Tf;
            this->X0 = X0;
            this->Xf = Xf;
            this->V0 = V0;
            this->Vf = Vf;
            this->A0 = A0;
            this->Af = Af;

            double DT = Tf - T0;
            double DT2 = DT * DT;

            C0 = 1.0 * X0;
            C1 = 1.0 * V0 * DT;
            C2 = 0.5 * A0 * DT2;
            C3 = -10.0 * X0 - 6.0 * V0 * DT - 1.5 * A0 * DT2 + 10.0 * Xf - 4.0 * Vf * DT + 0.5 * Af * DT2;
            C4 = 15.0 * X0 + 8.0 * V0 * DT + 1.5 * A0 * DT2 - 15.0 * Xf + 7.0 * Vf * DT - 1.0 * Af * DT2;
            C5 = -6.0 * X0 - 3.0 * V0 * DT - 0.5 * A0 * DT2 + 6.0 * Xf - 3.0 * Vf * DT + 0.5 * Af * DT2;
        }

        /**
         * Computes the position, velocity, and acceleration at a given point in time
         *
         * @param time The time at which the position, velocity, and acceleration of the trajectory are computed
         */
        void computeTrajectory(double time)
        {
            if (time < T0)
            {
                position = X0;
                velocity = V0;
                acceleration = A0;
                return;
            }
            else if (time > Tf)
            {
                position = Xf;
                velocity = Vf;
                acceleration = Af;
                return;
            }

            double tau = (time - T0) / (Tf - T0);
            double tau2 = tau * tau;
            double tau3 = tau * tau2;
            double tau4 = tau * tau3;
            double tau5 = tau * tau4;

            double DT = Tf - T0;
            double DT2 = DT * DT;

            position = C0 + C1 * tau + C2 * tau2 + C3 * tau3 + C4 * tau4 + C5 * tau5;
            velocity = (C1 + 2.0 * C2 * tau + 3.0 * C3 * tau2 + 4.0 * C4 * tau3 + 5.0 * C5 * tau4) / DT;
            acceleration = (2.0 * C2 + 6.0 * C3 * tau + 12.0 * C4 * tau2 + 20.0 * C5 * tau3) / DT2;

        }

    private:

        double C0, C1, C2, C3, C4, C5;
        double T0, Tf, X0, Xf, V0, Vf, A0, Af;
        double position, velocity, acceleration;
    };

}
#endif // __MINIMUMJERKTRAJECTORY_HPP__
