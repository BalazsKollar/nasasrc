
/**
 * This file lays out interfaces for different types of filters.
 */

#ifndef __FILTER_INTERFACES_HPP__
#define __FILTER_INTERFACES_HPP__

#include "control_utils/MathTools.hpp"

namespace control_utils
{
    template<typename T>
    class AlphaFilterInterface
    {
    public:
        AlphaFilterInterface()
        {

        }

        void setFilterAlphaByBreakFrequency(const T &breakFrequencyInHertz, const T &dt)
        {
            setAlpha(control_utils::MathTools::computeAlphaFilterAlphaFromBreakFrequency(breakFrequencyInHertz, dt));
        }

        virtual ~AlphaFilterInterface(){};

        virtual void update()
        {
            setFilteredValue(control_utils::MathTools::computeAlphaFilterOutput(getFilteredValue(),getSignalToFilterCurrentValue(),getAlpha()));
        }

        virtual void setAlpha(const T alpha) = 0;
        virtual void setFilteredValue(const T value) = 0;

        virtual T getFilteredValue() const = 0;
        virtual T getAlpha() const = 0;
        virtual T getSignalToFilterCurrentValue() const = 0;
        virtual void reset() = 0;
    };

}

#endif
