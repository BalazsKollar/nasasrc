
/**
 * This class is intended to hold simple mathematical tools that are application agnostic.
 */

#ifndef __MATHTOOLS_HPP__
#define __MATHTOOLS_HPP__

#include "nasa_common_logging/Logger.h"
#include <eigen3/Eigen/Eigen>
#include <math.h>
#include <limits>
#include <cmath>

const double EPSILON = 1e-10;

namespace control_utils
{
    class MathTools
    {
    public:
        /**
        * A common place where things can go to access a gravitational constant. This is basically just here to prevent 
        * the various things that need a gravitational constant from hardcoding it everywhere and then if it needs to be 
        * tuned you have to go change it everywhere it's hardcoded or there will be discrepencies.
        */
        template<typename T>
        static T getGravity()
        {
            return (T)9.80665;
        }

        /**
        * @brief Method for getting a static quaternion rotation that will rotate a Northeast down(N.E.D) frame to a Northwest up(N.W.U) frame.
        * @return Quaternion rotation from N.E.D to N.W.U
        */
        template<typename T>
        static Eigen::Quaternion<T> getRotationFromNorthEastDownToNorthWestUp()
        {
            Eigen::Quaternion<T> rotation((T)cos(M_PI_2),(T)sin(M_PI_2),0.0,0.0);
            return rotation;
        }

		/**
         * @brief Method for converting from Euler angles(yaw, pitch, roll ordering here) into a quaternion.
         * @param yaw The rotation about the Z-axis in radians
         * @param pitch The rotation about the Y-axis in radians
         * @param roll The rotation about the X-axis in radians
         * @return quaternion The normalized quaternion resulting from the given yaw,pitch, and roll angles.
         */
        template<typename T>
        static void getQuaternionFromEulerYawPitchRoll(const T &yaw, const T& pitch, const T &roll, Eigen::Quaternion<T> &quaternion)
        {
            T halfYaw = yaw/2;
            T halfRoll = roll/2;
            T halfPitch = pitch/2;

            T cosYaw = cos(halfYaw);
            T cosRoll = cos(halfRoll);
            T cosPitch = cos(halfPitch);
            T sinYaw = sin(halfYaw);
            T sinRoll = sin(halfRoll);
            T sinPitch = sin(halfPitch);

            T cosRollCosPitch = cosRoll*cosPitch;
            T sinRollSinPitch = sinRoll*sinPitch;
            T cosRollSinPitch = cosRoll*sinPitch;
            T sinRollCosPitch = sinRoll*cosPitch;

            quaternion.w() = cosRollCosPitch*cosYaw + sinRollSinPitch*sinYaw;
            quaternion.x() = sinRollCosPitch*cosYaw - cosRollSinPitch*sinYaw;
            quaternion.y() = cosRollSinPitch*cosYaw + sinRollCosPitch*sinYaw;
            quaternion.z() = cosRollCosPitch*sinYaw + sinRollSinPitch*cosYaw;

            quaternion.normalize();
        }

		/**
         * @brief Method for converting from Euler angles(roll, pitch, yaw ordering here) into a quaternion.
         * @param roll The rotation about the X-axis in radians
         * @param pitch The rotation about the Y-axis in radians
         * @param yaw The rotation about the Z-axis in radians
         * @return quaternion The normalized quaternion resulting from the given roll,pitch, and yaw angles.
         */
        template<typename T>
        static void getQuaternionFromEulerRollPitchYaw(const T &roll, const T& pitch, const T &yaw, Eigen::Quaternion<T> &quaternion)
        {
            T halfYaw = yaw/2;
            T halfRoll = roll/2;
            T halfPitch = pitch/2;

            T cosYaw = cos(halfYaw);
            T cosRoll = cos(halfRoll);
            T cosPitch = cos(halfPitch);
            T sinYaw = sin(halfYaw);
            T sinRoll = sin(halfRoll);
            T sinPitch = sin(halfPitch);

            T cosRollCosPitch = cosRoll*cosPitch;
            T sinRollSinPitch = sinRoll*sinPitch;
            T cosRollSinPitch = cosRoll*sinPitch;
            T sinRollCosPitch = sinRoll*cosPitch;

            quaternion.w() = cosRollCosPitch*cosYaw - sinRollSinPitch*sinYaw;
            quaternion.x() = sinRollCosPitch*cosYaw + cosRollSinPitch*sinYaw;
            quaternion.y() = cosRollSinPitch*cosYaw - sinRollCosPitch*sinYaw;
            quaternion.z() = sinRollSinPitch*cosYaw + cosRollCosPitch*sinYaw;

            quaternion.normalize();
        }

		template<typename T>
        static bool areQuaternionsEpsilonEqual(const Eigen::Quaternion<T> &q1, const Eigen::Quaternion<T> &q2, const T &epsilon)
        {
            if(fabs(q1.x()-q2.x())<epsilon && fabs(q1.y()-q2.y())<epsilon && fabs(q1.z()-q2.z())<epsilon && fabs(q1.w()-q2.w())<epsilon)
            {
                return true;
            }
            else if(fabs(q1.x()+q2.x())<epsilon && fabs(q1.y()+q2.y())<epsilon && fabs(q1.z()+q2.z())<epsilon && fabs(q1.w()+q2.w())<epsilon)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /**
         * Given a min value, a max value, and a current value, clip the current value
         * such that if value is greater than max, set it equal to max. If current value
         * is less than min, set it equal to min, otherwise return value unchanged.
         *
         * @param min The minimum value allowed
         * @param max The maximum value allowed
         * @param value The value subjected to clipping
         * @return The clipped value
         */
        template<typename T>
        static T clipMinMax(const T& min, const T &max, const T&value)
        {
            if(min>max)
            {
                std::string msg = "The minimum value must be LESS than the maximum value";
                NasaCommonLogging::Logger::log("gov.nasa.MathTools.clipMinMax",
                                               log4cpp::Priority::ERROR, msg);
                throw std::runtime_error(msg);
            }

            return std::min(max,std::max(min,value));
        }

        /**
         * Compute the break frequency of an low pass(i.e. alpha filter) given the filters
         * alpha coefficient
         *
         * @param alpha The filter coefficient
         * @param dt The timestep
         * @return 
         */
        template<typename T>
        static T computeAlphaFilterBreakFrequencyGivenAlpha(const T& alpha, const T &dt)
        {
            if(alpha > 1 || alpha < 0)
            {
               std::string msg = "Alpha out of bounds! Alpha must be between zero and one!";
                NasaCommonLogging::Logger::log("gov.nasa.MathTools.computeAlphaFilterBreakFrequencyGivenAlpha",
                                               log4cpp::Priority::ERROR, msg);
                throw std::runtime_error(msg);
            }
            if(alpha ==1)
            {
                return std::numeric_limits<T>::infinity();
            }

            T tmpValue = 2*M_PI*dt;

            return alpha/(tmpValue - alpha*tmpValue);
        }

        /**
         * Compute an alpha filter's alpha parameter from a desired break frequency in Hz and a timestep
         * @param breakFrequency the desired filter break frequency
         * @param dt The time step
         */
        template<typename T>
        static T computeAlphaFilterAlphaFromBreakFrequency(const T &breakFrequencyInHertz, const T &dt)
        {
            if (std::isinf(breakFrequencyInHertz))
            {
                return 0.0;
            }

            T value = 2 * M_PI * breakFrequencyInHertz * dt;
            T alpha = value / (value + 1);

            return clipMinMax<double>(0.0, 1.0, alpha);
        }

        template<typename T>
        static T computeAlphaFilterOutput(const T &previousFilterValue, const T &filteredSignalCurrentValue, const T &alpha)
        {
            return alpha* previousFilterValue + (1 - alpha) * filteredSignalCurrentValue;
        }

        /**
         * Given two angles, compute the distance between them on the range -PI to PI.
         *
         * @param angle1
         * @param angle2
         */
        template<typename T>
        static T computeAngleDifferenceMinusPiToPi(const T &angle1, const T angle2)
        {
            T pi = static_cast<T>(M_PI);
            T difference = angle1 - angle2;
            T diffMod2Pi = fmod(difference,(2*pi));
            return shiftAngleToStartOfRange<T>(diffMod2Pi,-pi);
        }

        /**
         * Shift an angle onto a desired range
         *
         * @param angleToShift - The angle to be shifted
         * @param startOfAngleRange - The beginning of the new range the angle will be shifted into
         */
        template<typename T>
        static T shiftAngleToStartOfRange(const T &angleToShift, const T &startOfAngleRange)
        {
            T tmpStartOfAngleRange = startOfAngleRange - EPSILON;

            T shiftedAngle = angleToShift;

            if(angleToShift < tmpStartOfAngleRange)
            {
                shiftedAngle = angleToShift + ceil((tmpStartOfAngleRange - angleToShift)/(2.0*M_PI))*M_PI*2.0;
                return shiftedAngle;
            }

            if(angleToShift >= (tmpStartOfAngleRange + M_PI * 2.0))
            {
                shiftedAngle = angleToShift - floor((tmpStartOfAngleRange - angleToShift)/(2.0*M_PI))*M_PI*2.0;
                return shiftedAngle;
            }

            return shiftedAngle;
        }
    };
}

#endif //__MATHTOOLS_HPP__
