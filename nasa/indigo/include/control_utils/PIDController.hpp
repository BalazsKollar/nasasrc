
/**
 * A simple implementation of a PID controller.
 */

#ifndef __PID_CONTROLLER_HPP__
#define __PID_CONTROLLER_HPP__

#include "control_utils/PIDControllerInterfaces.hpp"
#include "control_utils/MathTools.hpp"

namespace control_utils
{
    template<typename T>
    class PIDController : public ProportionalControllerInterface<T>, public DerivativeControllerInterface<T>, public IntegralControllerInterface<T>
    {
    public:
        /**
         * Constructor
         *
         * @param kp The proportional gain
         * @param kd The derivative gain
         * @param ki The integral gain
         * @param maxIntegralError An optional cap on the allowed integral error. Default value is infinity
         * @param minEffort The minimum allowed controller effort. Default value is negative infinity
         * @param maxEffort The maximum allowed controller effort. Default value is infinity
         * @param integralLeakRatio A fractional amount of the total error that is ignored by the integrator.
         */
        PIDController(const T kp, const T kd, const T ki, const T maxIntegralError=std::numeric_limits<T>::infinity(), const T minEffort=std::numeric_limits<T>::infinity(), const T maxEffort=std::numeric_limits<T>::infinity(), const T integralLeakRatio = 1.0) : ProportionalControllerInterface<T>(), DerivativeControllerInterface<T>(), IntegralControllerInterface<T>()
        {
            this->ki = ki;
            this->kp = kp;
            this->kd = kd;
            this->maxIntegralError = maxIntegralError;
            this->minEffort = minEffort;
            this->maxEffort = maxEffort;
            this->integralLeakRatio = integralLeakRatio;
        }

        virtual ~PIDController(){};

        /**
         * Set the controllers proportional gain
         *
         * @param kp The new value of the proportional gain
         */
        void setProportionalGain(const T kp)
        {
            this->kp = kp;
        }

        /**
         * Get the current proportional gain
         *
         * @return Proportional gain
         */
        T getProportionalGain() const
        {
            return kp;
        }

        /**
         * Get the derivative gain
         *
         * @return Derivative gain
         */
        T getDerivativeGain() const
        {
            return kd;
        }

        /**
         * Set the controllers derivative gain
         *
         * @param kd The new value of the derivative gain
         */
        void setDerivativeGain( const T kd)
        {
            this->kd = kd;
        }

        /**
         * Get the integral leak ratio
         *
         * @return The integral leak ratio
         */
        T getIntegralLeakRatio() const
        {
            return integralLeakRatio;
        }

        /**
         * Get the max allowed integral error
         *
         * @return Max integral error
         */
        T getMaxIntegralError() const
        {
            return maxIntegralError;
        }

        /**
         * Get the integral gain
         *
         * @return Integral gain
         */
        T getIntegralGain() const
        {
            return ki;
        }

        /**
         * Get the max allowed positive effort from the integrator
         *
         * @return The max allowed positive effort from the integrator
         */
        T getMaxIntegralEffort() const
        {
            return maxEffort;
        }

        /**
         * Get the minimum allowed effort from the integrator
         *
         * @return The minimum allowed effort from the integrator
         */
        T getMinIntegralEffort() const
        {
            return minEffort;
        }

        /**
         * Set the controllers integral leak ratio
         *
         * @param integralLeakRatio The new value of the integral leak ratio
         */
        void setIntegralLeakRatio(const T integralLeakRatio)
        {
            this->integralLeakRatio = integralLeakRatio;
        }

        /**
         * Set the controllers integral gain
         *
         * @param ki The new value of the integral gain
         */
        void setIntegralGain(const T ki)
        {
            this->ki = ki;
        }

        /**
         * Set the controllers max allowed integral error.
         *
         * @param maxIntegralError The new value of the max integral error
         */
        void setMaxIntegralError(const T maxIntegralError)
        {
            this->maxIntegralError = maxIntegralError;
        }

        /**
         * Set the maximum allowed positive effort for the integrator
         *
         * @param maxIntegralEffort The new value for the maximum allowed positive integral effort
         */
        void setMaxIntegralEffort(const T maxIntegralEffort)
        {
            this->maxEffort = maxIntegralEffort;
        }

        /**
         * Set the maximum allowed negative effort for the integrator
         *
         * @param minIntegralEffort The new value for the maximum allowed negative integral effort
         */
        void setMinIntegralEffort(const T minIntegralEffort)
        {
            this->minEffort = minIntegralEffort;
        }

        /**
         * Computes the controllers output
         *
         * @param desiredPosition The desired position
         * @param currentPosition The current position
         * @param desiredVelocity The desired velocity
         * @param currentVelocity The current velocity
         * @param deltaTime The time step for the integrator
         */
        T compute(const T desiredPosition, const T currentPosition, const T desiredVelocity, const T currentVelocity, const T deltaTime)
        {
            this->proportionalControllerOutput = this->computeProportionalControllerOutput(desiredPosition,currentPosition);
            this->derivativeControllerOutput = this->computeDerivativeControllerOutput(desiredVelocity,currentVelocity);
            this->integralControllerOutput = this->computeIntegralControllerOutput(desiredPosition,currentPosition,deltaTime);

            return (this->proportionalControllerOutput + this->derivativeControllerOutput + this->integralControllerOutput);
        }

        /**
         * Computes the controllers output and intelligently handles the case where angles do not wrap.
         *
         * @param desiredPosition The desired position
         * @param currentPosition The current position
         * @param desiredVelocity The desired velocity
         * @param currentVelocity The current velocity
         * @param deltaTime The time step for the integrator
         */
        T computeForAngles(const T desiredPosition, const T currentPosition, const T desiredVelocity, const T currentVelocity, const T deltaTime)
        {
            this->proportionalControllerOutput = this->computeProportionalControllerOutputForAngles(desiredPosition,currentPosition);
            this->derivativeControllerOutput = this->computeDerivativeControllerOutput(desiredVelocity,currentVelocity);
            this->integralControllerOutput = this->computeIntegralControllerOutput(desiredPosition,currentPosition,deltaTime);

            return (this->proportionalControllerOutput + this->derivativeControllerOutput + this->integralControllerOutput);
        }

    protected:

        T ki,kd,kp;
        T integralLeakRatio;
        T maxIntegralError;
        T minEffort, maxEffort;
    };

}

#endif //__PIDCONTROLLER_HPP__
