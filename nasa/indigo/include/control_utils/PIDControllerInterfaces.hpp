
/**
 * This file provides a interfaces that different types of
 * proportional/derivative/integral controllers can implement. Child classes need only implement the
 * methods for getting/setting PD gains, etc.
 */

#ifndef  __PID_CONTROLLER_INTERFACES_HPP__
#define  __PID_CONTROLLER_INTERFACES_HPP__

#include <math.h>
#include "control_utils/MathTools.hpp"

namespace control_utils
{

    template<typename T>
    class ProportionalControllerInterface
    {
    public:
        ProportionalControllerInterface() : positionError(0.0), proportionalEffort(0.0)
        {

        }

        virtual ~ProportionalControllerInterface(){};

        T getProportionalEffort() const
        {
            return proportionalEffort;
        }

        T getPositionError() const
        {
            return positionError;
        }

        T computeProportionalControllerOutput(const T desiredPosition, const T currentPosition)
        {
            computePositionError(desiredPosition, currentPosition);
            computeProportionalEffort();

            return proportionalEffort;
        }

        T computeProportionalControllerOutputForAngles(const T desiredPosition, const T currentPosition)
        {
            computePositionErrorForAngles(desiredPosition,currentPosition);
            computeProportionalEffort();

            return proportionalEffort;
        }

        virtual void setProportionalGain(const T kp) = 0;
        virtual T getProportionalGain() const = 0;

    protected:
        T proportionalControllerOutput;

        void computeProportionalEffort()
        {
            proportionalEffort = getProportionalGain() * positionError;
        }

        void computePositionError(const T desiredPosition, const T currentPosition)
        {
            positionError = desiredPosition - currentPosition;
        }

        void computePositionErrorForAngles(const T desiredPosition, const T currentPosition)
        {
            positionError = control_utils::MathTools::computeAngleDifferenceMinusPiToPi(desiredPosition,currentPosition);
        }

        T positionError;
        T proportionalEffort;
    };

    template<typename T>
    class DerivativeControllerInterface
    {
    public:
        DerivativeControllerInterface() : velocityError(0.0), derivativeEffort(0.0)
        {

        }

        virtual ~DerivativeControllerInterface(){};

        T getVelocityError() const
        {
            return velocityError;
        }

        T getDerivativeEffort() const
        {
            return derivativeEffort;
        }

        T computeDerivativeControllerOutput(const T desiredVelocity, const T currentVelocity)
        {
            computeVelocityError(desiredVelocity,currentVelocity);

            computeDerivativeEffort();

            return derivativeEffort;
        }

        virtual T getDerivativeGain() const = 0;
        virtual void setDerivativeGain( const T kd) = 0;

    protected:
        T derivativeControllerOutput;
        T velocityError;
        T derivativeEffort;

        void computeVelocityError(const T desiredVelocity, const T currentVelocity)
        {
            velocityError = desiredVelocity - currentVelocity;
        }

        void computeDerivativeEffort()
        {
            derivativeEffort = getDerivativeGain() * velocityError;
        }

    };

    template<typename T>
    class IntegralControllerInterface
    {
    public:
        IntegralControllerInterface() : integralEffort(0.0), integralError(0.0), positionError(0.0)
        {

        }

        virtual ~IntegralControllerInterface(){};

        T getPositionError() const
        {
            return positionError;
        }

        T getIntegralError() const
        {
            return integralError;
        }

        T getIntegralEffort() const
        {
            return integralEffort;
        }

        T computeIntegralControllerOutput(const T desiredPosition, const T currentPosition, const T deltaTime)
        {
            computePositionError(desiredPosition,currentPosition);
            computeIntegralEffort(deltaTime);

            return integralEffort;
        }

        T computeIntegralControllerOutputForAngles(const T desiredPosition, const T currentPosition, const T deltaTime)
        {
            computePositionErrorForAngles(desiredPosition,currentPosition);
            computeIntegralEffort(deltaTime);

            return integralEffort;
        }

        virtual T getMaxIntegralError() const = 0;
        virtual T getIntegralLeakRatio() const = 0;
        virtual T getIntegralGain() const = 0;
        virtual T getMaxIntegralEffort() const = 0;
        virtual T getMinIntegralEffort() const = 0;

        virtual void setIntegralGain(const T ki) = 0;
        virtual void setIntegralLeakRatio(const T integralLeakRatio) = 0;
        virtual void setMaxIntegralError(const T maxIntegralError) = 0;
        virtual void setMaxIntegralEffort(const T maxIntegralEffort) = 0;
        virtual void setMinIntegralEffort(const T minIntegralEffort) = 0;

    protected:
        T integralControllerOutput;
        T integralError;
        T integralEffort;
        T positionError;

        /**
         * Compute the integral portion of the controller effort
         * @param deltaTime The timestep
         */
        void computeIntegralEffort(const T deltaTime)
        {
            T maxIntegralError = getMaxIntegralError();

            integralError = positionError*deltaTime + getIntegralLeakRatio() * integralError;

            integralError = control_utils::MathTools::clipMinMax(-maxIntegralError,maxIntegralError,integralError);

            integralEffort = getIntegralGain() * integralError;
        }

        void computePositionError(const T desiredPosition, const T currentPosition)
        {
            positionError = desiredPosition - currentPosition;
        }

        void computePositionErrorForAngles(const T desiredPosition, const T currentPosition)
        {
            positionError = control_utils::MathTools::computeAngleDifferenceMinusPiToPi(desiredPosition,currentPosition);
        }
    };
}
#endif
