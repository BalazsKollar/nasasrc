#ifndef __SIDE_DEPENDENT_LIST_HPP
#define __SIDE_DEPENDENT_LIST_HPP

/**
 * This class and its implementation is an adaptation of SideDependentList.java by Jerry Pratt
 * and the IHMC robotics group. All credit goes to them.
 */

#include <map>
#include "control_utils/RobotSide.hpp"
#include <string>
#include <stdexcept>

namespace control_utils
{
template<class T>
class SideDependentList
{
	public:

		SideDependentList()
		{

		}
		SideDependentList(const T &leftObject, const T &rightObject)
		{
			this->sideDependentList[RobotSide::LEFT] = leftObject;
			this->sideDependentList[RobotSide::RIGHT] = rightObject;
		}

		SideDependentList(T* leftObject, T* rightObject)
		{
			this->sideDependentList[RobotSide::LEFT] = leftObject;
			this->sideDependentList[RobotSide::RIGHT] = rightObject;
		}

        //Copy constructor
		SideDependentList(const SideDependentList &list) : sideDependentList(list.sideDependentList)
		{

		}

		~SideDependentList()
		{
		};

		void set(const RobotSide::Side &robotSide, const T &object)
		{
			this->sideDependentList[robotSide] = object;
		}

		void set(const RobotSide::Side &robotSide, T* object)
		{
			this->sideDependentList[robotSide] = object;
		}

		T get(const RobotSide::Side &robotSide) const
		{
			return sideDependentList[robotSide];
		}

		T get(const RobotSide::Side &robotSide)
		{
			return sideDependentList[robotSide];
		}

		void set(const SideDependentList &sideDependentList)
		{
			for (const auto robotSide : RobotSide::values)
			{
				set(robotSide, sideDependentList.get(robotSide));
			}
		}

protected:
	std::map<RobotSide::Side, T> sideDependentList;

};
}

#endif