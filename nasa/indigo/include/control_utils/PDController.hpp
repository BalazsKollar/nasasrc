
/**
 * A simple implementation of a PD controller.
 */

#ifndef __PD_CONTROLLER_HPP__
#define __PD_CONTROLLER_HPP__

#include <math.h>
#include "control_utils/MathTools.hpp"
#include "control_utils/PIDControllerInterfaces.hpp"

namespace control_utils
{

    template<typename T>
    class PDController : public ProportionalControllerInterface<T>, public DerivativeControllerInterface<T>
    {
    public:
        /**
         * Constructor
         *
         * @param kp The proportional gain
         * @param kd The derivative gain
         */
        PDController(const T kp, const T kd) : ProportionalControllerInterface<T>(), DerivativeControllerInterface<T>()
        {
            this->kp = kp;
            this->kd = kd;
        }

        virtual ~PDController(){};

        /**
         * Set the controllers proportional gain
         *
         * @param kp The new value of the proportional gain
         */
        void setProportionalGain(const T kp)
        {
            this->kp = kp;
        }

        /**
         * Set the controllers derivative gain
         *
         * @param kd The new value of the derivative gain
         */
        void setDerivativeGain(const T kd)
        {
            this->kd = kd;
        }

        /**
         * Get the current proportional gain
         *
         * @return Proportional gain
         */
        T getProportionalGain() const
        {
            return kp;
        }

        /**
         * Get the derivative gain
         *
         * @return Derivative gain
         */
        T getDerivativeGain() const
        {
            return kd;
        }

        /**
         * Computes the controllers output and intelligently handles the case where angles do not wrap.
         *
         * @param desiredPosition The desired position
         * @param currentPosition The current position
         * @param desiredVelocity The desired velocity
         * @param currentVelocity The current velocity
         */
        T computeForAngles(const T desiredPosition, const T currentPosition, const T desiredVelocity, const T currentVelocity)
        {
            this->proportionalControllerOutput = this->computeProportionalControllerOutputForAngles(desiredPosition,currentPosition);
            this->derivativeControllerOutput = this->computeDerivativeControllerOutput(desiredVelocity,desiredVelocity);

            return this->proportionalControllerOutput + this->derivativeControllerOutput;
        }

        /**
         * Computes the controllers output
         *
         * @param desiredPosition The desired position
         * @param currentPosition The current position
         * @param desiredVelocity The desired velocity
         * @param currentVelocity The current velocity
         */
        T compute(const T desiredPosition, const T currentPosition, const T desiredVelocity, const T currentVelocity)
        {
            this->proportionalControllerOutput = this->computeProportionalControllerOutput(desiredPosition,currentPosition);
            this->derivativeControllerOutput = this->computeDerivativeControllerOutput(desiredVelocity,currentVelocity);

            return this->proportionalControllerOutput + this->derivativeControllerOutput;
        }

    protected:

        T kp, kd;

    };

}
#endif //PROJECT_PDCONTROLLER_HPP
