// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: SmtRelayMsgs.proto

#ifndef PROTOBUF_SmtRelayMsgs_2eproto__INCLUDED
#define PROTOBUF_SmtRelayMsgs_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 2005000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 2005000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/unknown_field_set.h>
// @@protoc_insertion_point(includes)

namespace SmtRelayMsgs {

// Internal implementation detail -- do not call these.
void  protobuf_AddDesc_SmtRelayMsgs_2eproto();
void protobuf_AssignDesc_SmtRelayMsgs_2eproto();
void protobuf_ShutdownFile_SmtRelayMsgs_2eproto();

class ShmFileMsg;
class SmtRelayMsg;

// ===================================================================

class ShmFileMsg : public ::google::protobuf::Message {
 public:
  ShmFileMsg();
  virtual ~ShmFileMsg();

  ShmFileMsg(const ShmFileMsg& from);

  inline ShmFileMsg& operator=(const ShmFileMsg& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const ShmFileMsg& default_instance();

  void Swap(ShmFileMsg* other);

  // implements Message ----------------------------------------------

  ShmFileMsg* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const ShmFileMsg& from);
  void MergeFrom(const ShmFileMsg& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // required string filename = 1;
  inline bool has_filename() const;
  inline void clear_filename();
  static const int kFilenameFieldNumber = 1;
  inline const ::std::string& filename() const;
  inline void set_filename(const ::std::string& value);
  inline void set_filename(const char* value);
  inline void set_filename(const char* value, size_t size);
  inline ::std::string* mutable_filename();
  inline ::std::string* release_filename();
  inline void set_allocated_filename(::std::string* filename);

  // required uint32 fileSize = 2;
  inline bool has_filesize() const;
  inline void clear_filesize();
  static const int kFileSizeFieldNumber = 2;
  inline ::google::protobuf::uint32 filesize() const;
  inline void set_filesize(::google::protobuf::uint32 value);

  // required bytes fileContents = 3;
  inline bool has_filecontents() const;
  inline void clear_filecontents();
  static const int kFileContentsFieldNumber = 3;
  inline const ::std::string& filecontents() const;
  inline void set_filecontents(const ::std::string& value);
  inline void set_filecontents(const char* value);
  inline void set_filecontents(const void* value, size_t size);
  inline ::std::string* mutable_filecontents();
  inline ::std::string* release_filecontents();
  inline void set_allocated_filecontents(::std::string* filecontents);

  // @@protoc_insertion_point(class_scope:SmtRelayMsgs.ShmFileMsg)
 private:
  inline void set_has_filename();
  inline void clear_has_filename();
  inline void set_has_filesize();
  inline void clear_has_filesize();
  inline void set_has_filecontents();
  inline void clear_has_filecontents();

  ::google::protobuf::UnknownFieldSet _unknown_fields_;

  ::std::string* filename_;
  ::std::string* filecontents_;
  ::google::protobuf::uint32 filesize_;

  mutable int _cached_size_;
  ::google::protobuf::uint32 _has_bits_[(3 + 31) / 32];

  friend void  protobuf_AddDesc_SmtRelayMsgs_2eproto();
  friend void protobuf_AssignDesc_SmtRelayMsgs_2eproto();
  friend void protobuf_ShutdownFile_SmtRelayMsgs_2eproto();

  void InitAsDefaultInstance();
  static ShmFileMsg* default_instance_;
};
// -------------------------------------------------------------------

class SmtRelayMsg : public ::google::protobuf::Message {
 public:
  SmtRelayMsg();
  virtual ~SmtRelayMsg();

  SmtRelayMsg(const SmtRelayMsg& from);

  inline SmtRelayMsg& operator=(const SmtRelayMsg& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const SmtRelayMsg& default_instance();

  void Swap(SmtRelayMsg* other);

  // implements Message ----------------------------------------------

  SmtRelayMsg* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const SmtRelayMsg& from);
  void MergeFrom(const SmtRelayMsg& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // repeated .SmtRelayMsgs.ShmFileMsg shmFileMsg = 1;
  inline int shmfilemsg_size() const;
  inline void clear_shmfilemsg();
  static const int kShmFileMsgFieldNumber = 1;
  inline const ::SmtRelayMsgs::ShmFileMsg& shmfilemsg(int index) const;
  inline ::SmtRelayMsgs::ShmFileMsg* mutable_shmfilemsg(int index);
  inline ::SmtRelayMsgs::ShmFileMsg* add_shmfilemsg();
  inline const ::google::protobuf::RepeatedPtrField< ::SmtRelayMsgs::ShmFileMsg >&
      shmfilemsg() const;
  inline ::google::protobuf::RepeatedPtrField< ::SmtRelayMsgs::ShmFileMsg >*
      mutable_shmfilemsg();

  // @@protoc_insertion_point(class_scope:SmtRelayMsgs.SmtRelayMsg)
 private:

  ::google::protobuf::UnknownFieldSet _unknown_fields_;

  ::google::protobuf::RepeatedPtrField< ::SmtRelayMsgs::ShmFileMsg > shmfilemsg_;

  mutable int _cached_size_;
  ::google::protobuf::uint32 _has_bits_[(1 + 31) / 32];

  friend void  protobuf_AddDesc_SmtRelayMsgs_2eproto();
  friend void protobuf_AssignDesc_SmtRelayMsgs_2eproto();
  friend void protobuf_ShutdownFile_SmtRelayMsgs_2eproto();

  void InitAsDefaultInstance();
  static SmtRelayMsg* default_instance_;
};
// ===================================================================


// ===================================================================

// ShmFileMsg

// required string filename = 1;
inline bool ShmFileMsg::has_filename() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void ShmFileMsg::set_has_filename() {
  _has_bits_[0] |= 0x00000001u;
}
inline void ShmFileMsg::clear_has_filename() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void ShmFileMsg::clear_filename() {
  if (filename_ != &::google::protobuf::internal::kEmptyString) {
    filename_->clear();
  }
  clear_has_filename();
}
inline const ::std::string& ShmFileMsg::filename() const {
  return *filename_;
}
inline void ShmFileMsg::set_filename(const ::std::string& value) {
  set_has_filename();
  if (filename_ == &::google::protobuf::internal::kEmptyString) {
    filename_ = new ::std::string;
  }
  filename_->assign(value);
}
inline void ShmFileMsg::set_filename(const char* value) {
  set_has_filename();
  if (filename_ == &::google::protobuf::internal::kEmptyString) {
    filename_ = new ::std::string;
  }
  filename_->assign(value);
}
inline void ShmFileMsg::set_filename(const char* value, size_t size) {
  set_has_filename();
  if (filename_ == &::google::protobuf::internal::kEmptyString) {
    filename_ = new ::std::string;
  }
  filename_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* ShmFileMsg::mutable_filename() {
  set_has_filename();
  if (filename_ == &::google::protobuf::internal::kEmptyString) {
    filename_ = new ::std::string;
  }
  return filename_;
}
inline ::std::string* ShmFileMsg::release_filename() {
  clear_has_filename();
  if (filename_ == &::google::protobuf::internal::kEmptyString) {
    return NULL;
  } else {
    ::std::string* temp = filename_;
    filename_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
    return temp;
  }
}
inline void ShmFileMsg::set_allocated_filename(::std::string* filename) {
  if (filename_ != &::google::protobuf::internal::kEmptyString) {
    delete filename_;
  }
  if (filename) {
    set_has_filename();
    filename_ = filename;
  } else {
    clear_has_filename();
    filename_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
  }
}

// required uint32 fileSize = 2;
inline bool ShmFileMsg::has_filesize() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void ShmFileMsg::set_has_filesize() {
  _has_bits_[0] |= 0x00000002u;
}
inline void ShmFileMsg::clear_has_filesize() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void ShmFileMsg::clear_filesize() {
  filesize_ = 0u;
  clear_has_filesize();
}
inline ::google::protobuf::uint32 ShmFileMsg::filesize() const {
  return filesize_;
}
inline void ShmFileMsg::set_filesize(::google::protobuf::uint32 value) {
  set_has_filesize();
  filesize_ = value;
}

// required bytes fileContents = 3;
inline bool ShmFileMsg::has_filecontents() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void ShmFileMsg::set_has_filecontents() {
  _has_bits_[0] |= 0x00000004u;
}
inline void ShmFileMsg::clear_has_filecontents() {
  _has_bits_[0] &= ~0x00000004u;
}
inline void ShmFileMsg::clear_filecontents() {
  if (filecontents_ != &::google::protobuf::internal::kEmptyString) {
    filecontents_->clear();
  }
  clear_has_filecontents();
}
inline const ::std::string& ShmFileMsg::filecontents() const {
  return *filecontents_;
}
inline void ShmFileMsg::set_filecontents(const ::std::string& value) {
  set_has_filecontents();
  if (filecontents_ == &::google::protobuf::internal::kEmptyString) {
    filecontents_ = new ::std::string;
  }
  filecontents_->assign(value);
}
inline void ShmFileMsg::set_filecontents(const char* value) {
  set_has_filecontents();
  if (filecontents_ == &::google::protobuf::internal::kEmptyString) {
    filecontents_ = new ::std::string;
  }
  filecontents_->assign(value);
}
inline void ShmFileMsg::set_filecontents(const void* value, size_t size) {
  set_has_filecontents();
  if (filecontents_ == &::google::protobuf::internal::kEmptyString) {
    filecontents_ = new ::std::string;
  }
  filecontents_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* ShmFileMsg::mutable_filecontents() {
  set_has_filecontents();
  if (filecontents_ == &::google::protobuf::internal::kEmptyString) {
    filecontents_ = new ::std::string;
  }
  return filecontents_;
}
inline ::std::string* ShmFileMsg::release_filecontents() {
  clear_has_filecontents();
  if (filecontents_ == &::google::protobuf::internal::kEmptyString) {
    return NULL;
  } else {
    ::std::string* temp = filecontents_;
    filecontents_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
    return temp;
  }
}
inline void ShmFileMsg::set_allocated_filecontents(::std::string* filecontents) {
  if (filecontents_ != &::google::protobuf::internal::kEmptyString) {
    delete filecontents_;
  }
  if (filecontents) {
    set_has_filecontents();
    filecontents_ = filecontents;
  } else {
    clear_has_filecontents();
    filecontents_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
  }
}

// -------------------------------------------------------------------

// SmtRelayMsg

// repeated .SmtRelayMsgs.ShmFileMsg shmFileMsg = 1;
inline int SmtRelayMsg::shmfilemsg_size() const {
  return shmfilemsg_.size();
}
inline void SmtRelayMsg::clear_shmfilemsg() {
  shmfilemsg_.Clear();
}
inline const ::SmtRelayMsgs::ShmFileMsg& SmtRelayMsg::shmfilemsg(int index) const {
  return shmfilemsg_.Get(index);
}
inline ::SmtRelayMsgs::ShmFileMsg* SmtRelayMsg::mutable_shmfilemsg(int index) {
  return shmfilemsg_.Mutable(index);
}
inline ::SmtRelayMsgs::ShmFileMsg* SmtRelayMsg::add_shmfilemsg() {
  return shmfilemsg_.Add();
}
inline const ::google::protobuf::RepeatedPtrField< ::SmtRelayMsgs::ShmFileMsg >&
SmtRelayMsg::shmfilemsg() const {
  return shmfilemsg_;
}
inline ::google::protobuf::RepeatedPtrField< ::SmtRelayMsgs::ShmFileMsg >*
SmtRelayMsg::mutable_shmfilemsg() {
  return &shmfilemsg_;
}


// @@protoc_insertion_point(namespace_scope)

}  // namespace SmtRelayMsgs

#ifndef SWIG
namespace google {
namespace protobuf {


}  // namespace google
}  // namespace protobuf
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_SmtRelayMsgs_2eproto__INCLUDED
