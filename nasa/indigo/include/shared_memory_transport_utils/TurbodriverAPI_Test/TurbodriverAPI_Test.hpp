#ifndef __TurbodriverAPI_Test__HPP__
#define __TurbodriverAPI_Test__HPP__

// Includes:
#include "register.hpp"
#include <shared_memory_transport/smtclient.hpp>

//Namespaces:
namespace robot_hardware_interface{

class TurbodriverAPI_Test
{
public:
//Contructors
	TurbodriverAPI_Test();
	TurbodriverAPI_Test(const std::string &nodePath, SMT::SMTClient* smtClient);
//Destructor
	~TurbodriverAPI_Test();
//Subscribe function
	void Subscribe(const std::string &nodePath, SMT::SMTClient* smtClient);
//API Functions
	std::string getNodePath() const;
	void getEffortCommand(float &val);
	void park();
	void torqueMode();
private:
	std::string nodePath;
	SMT::SMTClient* client;
//Commands in the command dictionary
	std::function<void(uint32_t)> setCtrlReg1BrakeReleaseCmd;
	std::function<void(uint32_t)> setCtrlReg1BridgeEnableCmd;
	std::function<void(uint32_t)> setCtrlReg1ClearEncoderCmd;
	std::function<void(uint32_t)> setCtrlReg1CommutationSelCmd;
	std::function<void(uint32_t)> setCtrlReg1MotComSourceCmd;
	std::function<void(uint32_t)> setCtrlReg1MotorEnableCmd;
	std::function<void(uint32_t)> setCtrlReg1ProcResetCmd;
	std::function<void(uint32_t)> setCtrlReg2BootloaderModeCmd;
	std::function<void(uint32_t)> setCtrlReg2CalibrationModeCmd;
	std::function<void(uint32_t)> setCtrlReg2ClearFaultCmd;
	std::function<void(uint32_t)> setCtrlReg2ControlModeCmd;
	std::function<void(uint32_t)> setCtrlReg2StreamModeCmd;
	std::function<void(uint32_t)> setCtrlReg2StreamModeResetCmd;
//Subscriber declarations
protected:
	std::shared_ptr<SMT::SMT_utils::Register> subscriberCtrlReg1;
	std::shared_ptr<SMT::SMT_utils::Register> subscriberCtrlReg2;
	std::shared_ptr<SMT::Resource> subscriberJointTorque_Des_Nm;
};
//Close namespaces
}
#endif // __TurbodriverAPI_Test__HPP__
