
#ifndef __SMT_RELAY_PUBLISHER__
#define __SMT_RELAY_PUBLISHER__

#include "shared_memory_transport_utils/SmtRelayInterface.hpp"
#include <fstream>

namespace smt_utils
{
    /**
     * @brief Send a string message with the ZMQ_SNDMORE flag
     *
     * @param socket The zmq socket that sends the message
     * @param string The string to be sent
     *
     * @return bool A boolean returned from zmq::socket.send()
     */
    static bool zmqSendString (zmq::socket_t &socket, const std::string & string, const bool sendMore)
    {

        zmq::message_t message(string.size());
        memcpy (message.data(), string.data(), string.size());

        bool ret;
        if(sendMore)
        {
            ret = socket.send(message, ZMQ_SNDMORE);
        }
        else
        {
            ret = socket.send(message);
        }

        return (ret);
    }

    template<typename T>
    static bool zmqSendSimpleNumericType(zmq::socket_t &socket, const T value, const bool sendMore)
    {
        zmq::message_t message(sizeof(value));
        memcpy(message.data(),&value,sizeof(value));

        bool ret;
        if(sendMore)
        {
            ret = socket.send(message,ZMQ_SNDMORE);
        }
        else
        {
            ret = socket.send(message);
        }

        return ret;
    }

    template<typename T>
    static bool zmqSendSimpleNumericType(zmq::socket_t &socket, const T value)
    {
        zmq::message_t message(sizeof(value));
        memcpy(message.data(),&value,sizeof(value));

        bool ret = socket.send(message);

        return ret;
    }

    class SmtRelayPublisher : public SmtRelayInterface
    {
    public:
        SmtRelayPublisher(const std::string smtUri = "http://localhost:11312", const std::string zmqUri = "tcp://*:19140", const std::string zmqTopic= "SMTR") : SmtRelayInterface(smtUri,zmqUri,zmqTopic)
        {
            this->zmqSocket.reset(new zmq::socket_t(*this->zmqContext,ZMQ_PUB));

            try
            {
                this->zmqSocket->bind(zmqUri.c_str());
            }
            catch(zmq::error_t &e)
            {
                this->zmqSocket.reset();
                std::string msg = "Error encountered binding smt relay publisher socket: ";
                msg += std::strerror(errno);
                throw smt_utils::SmtRelayException(msg);
            }

            if(!updateSharedMemoryData())
            {
                throw SmtRelayException("SMT relay publisher failed to updateSharedMemoryData!");
            }

            if(!updateSharedMemoryFiles())
            {
                throw SmtRelayException("SMT relay publisher failed to updateSharedMemoryFiles!");
            }
        }

        void run();

        void update();
        bool updateSharedMemoryFiles();
        bool updateSharedMemoryData();

        static bool DEBUG; //Set to true if you want to publish a CRC

    private:
        void throwZmqSendError(const std::string &wtfFailed)
        {
            std::string msg = "socket_t::send() returned an error code while sending " + wtfFailed + ": ";
            msg += std::strerror(errno);
            throw smt_utils::SmtRelayException(msg);
        }

        std::shared_ptr<zmq::message_t> zmqMsg;
        std::string msgAsString;
    };

}
#endif
