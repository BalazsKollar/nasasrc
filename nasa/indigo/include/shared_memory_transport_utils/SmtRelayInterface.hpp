
#ifndef __SMT_RELAY_INTERFACE__
#define __SMT_RELAY_INTERFACE__

#include "shared_memory_transport/smtclient.hpp"
#include "shared_memory_transport_utils/crc.hpp"
#include <nasa_common_logging/Logger.h>
#include <zmq.hpp>
#include <boost/algorithm/string.hpp>
#include "shared_memory_transport_utils/SmtRelayMsgs.pb.h"

namespace smt_utils
{
        class SmtRelayException : public std::exception
        {
        public:

            SmtRelayException(const std::string &msg) : exceptionMsg(msg)
            {

            }

        const char *what() const throw()
        {
            return exceptionMsg.c_str();
        }

        std::string exceptionMsg;
    };

    class SmtRelayInterface
    {
    public:

        SmtRelayInterface(const std::string &localSmtUri, const std::string &zmqUri, const std::string &zmqTopic= "SMTR")
        {
            this->zmqTopic = zmqTopic;
            this->zmqUri = zmqUri;
            this->localSmtUri = localSmtUri;

            localSmtClient.reset(new SMT::SMTClient(localSmtUri));

            if(!localSmtClient->alive())
            {
                throw SMT::SMTException("SMTCore is not running!");
            }

            this->zmqContext.reset(new zmq::context_t(1));

            messageCount = 0;

            SmtRelayMsgs::SmtRelayMsg msg;
            msg.add_shmfilemsg();
        }

        virtual ~SmtRelayInterface()
        {
            if(zmqSocket)
            {
                if(zmqSocket->connected())
                {
                    zmqSocket->disconnect(zmqUri.c_str());
                }

                // If you don't either reset this shared_ptr or call close() it will hang!
                zmqSocket->close();
            }

            for(auto const &keyValuePair : fileNameToFileContentsMap)
            {
                if(fileToFileSizeMap.find(keyValuePair.first) == fileToFileSizeMap.end())
                {
                    std::string msg = "Shared memory file " + keyValuePair.first + " is not contained in the file map and will not be munmap'd.";
                    NasaCommonLogging::Logger::log("gov.nasa.SmtRelayInterface.Destructor", log4cpp::Priority::WARN, msg);

                    continue;
                }

                munmap(keyValuePair.second,fileToFileSizeMap[keyValuePair.first]);
            }
        }

        uint32_t getMessageCount() const
        {
            return messageCount;
        }

        virtual void update() = 0;
        virtual void run() = 0;

    protected:
        crc_t checkSum;
        std::shared_ptr <SMT::SMTClient> localSmtClient;
        std::vector <SMT::ResourceInfo> smtTopicInfo;
        std::vector <std::string> smtFiles;
        std::map<std::string, unsigned int> fileToFileSizeMap;
        std::map<std::string, void*> fileNameToFileContentsMap;
        std::shared_ptr <zmq::socket_t> zmqSocket;
        std::shared_ptr <zmq::context_t> zmqContext;
        std::string zmqTopic, zmqUri, localSmtUri;
        uint32_t messageCount;
        SmtRelayMsgs::SmtRelayMsg smtRelayMsg;
    };

    inline crc_t crcFromString(const std::string& s)
    {
        return crc_update(0, (unsigned char*)s.c_str(), s.size());
    }

    /**
     * @brief Pack all SMT::ResourceInfo's and shared memory files into the associated containers
     *
     * @param smtClient Shared memory transport client
     * @param smtTopicInfo A vector where all SMT::ResourceInfo's will be packed
     * @param smtFiles A vector where the shared memory file names will be packed
     */
    static void packSharedMemoryRelayInfo(SMT::SMTClient *smtClient, std::vector<SMT::ResourceInfo> &smtTopicInfo, std::vector<std::string> &smtFiles)
    {
        smtTopicInfo.clear();
        smtFiles.clear();

        unsigned int zeroSizeTopics = 0;

        std::vector<std::string> smtTopics = smtClient->listTopics();

        for (int i = 0; i < smtTopics.size(); i++)
        {
            SMT::ResourceInfo resourceInfo = smtClient->getResourceInfo(smtTopics[i]);

            if (resourceInfo.getSize() > 0)
            {
                smtTopicInfo.push_back(resourceInfo);

                if (std::find(smtFiles.begin(), smtFiles.end(), resourceInfo.getReadPath()) == smtFiles.end())
                {
                    smtFiles.push_back(resourceInfo.getReadPath());
                }

                if(resourceInfo.getWritePath()!="")
                {
                    if (std::find(smtFiles.begin(), smtFiles.end(), resourceInfo.getWritePath()) == smtFiles.end())
                    {
                        smtFiles.push_back(resourceInfo.getWritePath());
                    }
                }
            }
            else
            {
                if(!zeroSizeTopics)
                {
                    NasaCommonLogging::Logger::log("gov.nasa.SmtRelayInterface.packSharedMemoryRelayInfo", log4cpp::Priority::WARN, "SMT resource " + resourceInfo.getName() + " has a size of zero and will be ignored by the relay. Further log messages of this type will be suppressed.");
                }
                zeroSizeTopics++;
            }
        }
    }

    /**
     * @brief Extract the filename of a shared memory file from the full path
     *
     * @param fullFilePath The full file path of the shared memory file, e.g. /dev/shm/robonet-file
     *
     * @return The shared memory file name with the path and slashes removed
     */
    static std::string extractSharedMemoryFileName(const std::string fullFilePath)
    {
        std::vector<std::string> strVec;
        boost::split(strVec, fullFilePath, boost::is_any_of("/"));

        return strVec[strVec.size() - 1];
    }
}

#endif //__SMT_RELAY_PUBLISHER__
