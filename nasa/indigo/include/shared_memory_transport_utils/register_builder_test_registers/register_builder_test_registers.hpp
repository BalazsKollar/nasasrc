#ifndef _REGISTER_BUILDER_TEST_REGISTERS_
#define _REGISTER_BUILDER_TEST_REGISTERS_

#include "register.hpp"

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
uint16_t StatusRegister_applyCertificate(uint16_t val);

//
void StatusRegister_clear(SMT::Resource *resA);


std::vector<std::string> StatusRegister_getFieldNames();

// Used to set value of entire lower byte.
void StatusRegister_setAllBits(SMT::Resource *resA, uint32_t valA);
uint32_t StatusRegister_getAllBits(SMT::Resource *resA);
// info for bit0
void StatusRegister_setBit0(SMT::Resource *resA, uint32_t valA);
uint32_t StatusRegister_getBit0(SMT::Resource *resA);
// info for bit1
void StatusRegister_setBit1(SMT::Resource *resA, uint32_t valA);
uint32_t StatusRegister_getBit1(SMT::Resource *resA);
// info for bit2
void StatusRegister_setBit2(SMT::Resource *resA, uint32_t valA);
uint32_t StatusRegister_getBit2(SMT::Resource *resA);
// info for bit3
void StatusRegister_setBit3(SMT::Resource *resA, uint32_t valA);
uint32_t StatusRegister_getBit3(SMT::Resource *resA);
// info for bit4
void StatusRegister_setBit4(SMT::Resource *resA, uint32_t valA);
uint32_t StatusRegister_getBit4(SMT::Resource *resA);
// info for bit5
void StatusRegister_setBit5(SMT::Resource *resA, uint32_t valA);
uint32_t StatusRegister_getBit5(SMT::Resource *resA);
// info for bit6
void StatusRegister_setBit6(SMT::Resource *resA, uint32_t valA);
uint32_t StatusRegister_getBit6(SMT::Resource *resA);
// info for bit7
void StatusRegister_setBit7(SMT::Resource *resA, uint32_t valA);
uint32_t StatusRegister_getBit7(SMT::Resource *resA);
// info for bit9
void StatusRegister_setBit9(SMT::Resource *resA, uint32_t valA);
uint32_t StatusRegister_getBit9(SMT::Resource *resA);
// info for bit10
void StatusRegister_setBit10(SMT::Resource *resA, uint32_t valA);
uint32_t StatusRegister_getBit10(SMT::Resource *resA);
uint16_t ControlRegister_applyCertificate(uint16_t val);

//
void ControlRegister_clear(SMT::Resource *resA);


std::vector<std::string> ControlRegister_getFieldNames();

// Used to set value of entire lower byte.
void ControlRegister_setAllBits(SMT::Resource *resA, uint32_t valA);
uint32_t ControlRegister_getAllBits(SMT::Resource *resA);
// info for bit0
void ControlRegister_setBit0(SMT::Resource *resA, uint32_t valA);
uint32_t ControlRegister_getBit0(SMT::Resource *resA);
// info for bit1
void ControlRegister_setBit1(SMT::Resource *resA, uint32_t valA);
uint32_t ControlRegister_getBit1(SMT::Resource *resA);
// info for bit2
void ControlRegister_setBit2(SMT::Resource *resA, uint32_t valA);
uint32_t ControlRegister_getBit2(SMT::Resource *resA);
// info for bit3
void ControlRegister_setBit3(SMT::Resource *resA, uint32_t valA);
uint32_t ControlRegister_getBit3(SMT::Resource *resA);
// info for bit4
void ControlRegister_setBit4(SMT::Resource *resA, uint32_t valA);
uint32_t ControlRegister_getBit4(SMT::Resource *resA);
// info for bit5
void ControlRegister_setBit5(SMT::Resource *resA, uint32_t valA);
uint32_t ControlRegister_getBit5(SMT::Resource *resA);
// info for bit6
void ControlRegister_setBit6(SMT::Resource *resA, uint32_t valA);
uint32_t ControlRegister_getBit6(SMT::Resource *resA);
// info for bit7
void ControlRegister_setBit7(SMT::Resource *resA, uint32_t valA);
uint32_t ControlRegister_getBit7(SMT::Resource *resA);
// info for bit9
void ControlRegister_setBit9(SMT::Resource *resA, uint32_t valA);
uint32_t ControlRegister_getBit9(SMT::Resource *resA);
// info for bit10
void ControlRegister_setBit10(SMT::Resource *resA, uint32_t valA);
uint32_t ControlRegister_getBit10(SMT::Resource *resA);

#ifdef __cplusplus
}
#endif
#endif // _REGISTER_BUILDER_TEST_REGISTERS_
