
#ifndef __SMT_RELAY_SUBSCRIBER__
#define __SMT_RELAY_SUBSCRIBER__

#include "shared_memory_transport_utils/SmtRelayInterface.hpp"
#include <csignal>

namespace smt_utils
{
    /**
     * @brief Receieve a zmq string message
     *
     * @param socket The socket to reveive the message on
     *
     * @return The string message
     */
    static std::string zmqReceiveString(zmq::socket_t &socket)
    {
        zmq::message_t zmqMsg;
        socket.recv(&zmqMsg);
        std::string ret = std::string(static_cast<char*>(zmqMsg.data()),zmqMsg.size());

        return ret;
    }

    class SmtRelaySubscriber : public SmtRelayInterface
    {
    public:
        SmtRelaySubscriber(const std::string remoteSmtUri,const std::string zmqUri,const std::string localSmtUri="http://localhost:11312", const std::string zmqTopic="SMTR") : SmtRelayInterface(localSmtUri,zmqUri,zmqTopic)
        {
            this->remoteSmtUri = remoteSmtUri;

            remoteSmtClient.reset(new SMT::SMTClient(remoteSmtUri));

            if(!remoteSmtClient->alive())
            {
                throw SMT::SMTException("Remote SMTCore is not running!");
            }

            this->zmqSocket.reset(new zmq::socket_t(*zmqContext,ZMQ_SUB));
            try
            {
                this->zmqSocket->connect(zmqUri.c_str());
            }
            catch(smt_utils::SmtRelayException &e)
            {
                this->zmqSocket.reset();
                std::string msg = "Error encountered connecting smt relay subscriber socket: ";
                msg += std::strerror(errno);
                throw smt_utils::SmtRelayException(msg);
            }

            this->zmqSocket->setsockopt(ZMQ_SUBSCRIBE,this->zmqTopic.c_str(),this->zmqTopic.size());

            if(!updateSharedMemoryData())
            {
                throw SmtRelayException("SMT relay publisher failed to updateSharedMemoryData!");
            }

            if(!updateSharedMemoryFiles())
            {
                throw SmtRelayException("SMT relay publisher failed to updateSharedMemoryFiles!");
            }
        }

        bool updateSharedMemoryData();
        bool updateSharedMemoryFiles();
        void update();
        void run();

    private:

        std::string remoteSmtUri;
        std::shared_ptr<SMT::SMTClient> remoteSmtClient;
        std::vector<std::string> localSmtTopicNames, remoteSmtTopicNames, remoteSmtFiles;
        std::vector<SMT::ResourceInfo> remoteSmtTopicInfo;
        std::string protobufString;
    };
}

#endif
