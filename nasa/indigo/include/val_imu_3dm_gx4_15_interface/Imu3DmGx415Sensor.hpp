#ifndef __IMU_3DM_GX4_15_SENSOR_HPP__
#define __IMU_3DM_GX4_15_SENSOR_HPP__

#include <imu_3dm_gx4_15/imu_3dm_gx4_15_driver.hpp>
#include <shared_memory_transport/smtclient.hpp>
#include "val_robot_interface/ImuSensorInterface.hpp"
#include "control_utils/MathTools.hpp"
#include <mutex>
#include <thread>

namespace imu_3dm_gx4_15_sensor
{
    class Imu3DmGx415Sensor : public imu_sensor_interface::ImuSensorInterface
    {
    public:
        Imu3DmGx415Sensor() : running(true), runningErrorFree(true)
        {
            gravity = control_utils::MathTools::getGravity<double>();
        }

        ~Imu3DmGx415Sensor()
        {
            stop();
            disconnect();
        }

        void setImuFrameToZUpFrameQuaternion()
        {
            rotationFromImuFrameToZUpFrame = control_utils::MathTools::getRotationFromNorthEastDownToNorthWestUp<double>();
        }

        void initialize(const std::string& name, const std::string& nodePath, const std::string& devicePort)
        {
            try
            {
                imu.connectPlug(devicePort);

                imu.setFilterDataCallback(std::bind(&Imu3DmGx415Sensor::callbackFilter, this, std::placeholders::_1));
                imu.setIMUDataCallback(std::bind(&Imu3DmGx415Sensor::callbackIMU, this, std::placeholders::_1));
            }
            catch (...)
            {
                std::string msg = "Unable to connect to imu " + name;
                NasaCommonLogging::Logger::log("gov.nasa.Imu3dmGx4Interface.initialize", log4cpp::Priority::FATAL, msg);
                throw std::runtime_error(msg);
            }

            running = true;
            runningErrorFree = true;
            imuThread.push_back(std::thread(&Imu3DmGx415Sensor::readUSB, this));
        }

        void callbackFilter(const imu_3dm_gx4_15_driver::Imu_Driver::FilterData& data);
        void callbackIMU(const imu_3dm_gx4_15_driver::Imu_Driver::IMUData& data);

        void disconnect()
        {
            imu.disconnect();
        }

        void stop()
        {
            if (running)
            {
                running = false;

                for (unsigned int i = 0; i < imuThread.size(); i++)
                {
                    imuThread[i].join();
                }
            }
        }

    protected:
        void getData();
        void readUSB();

        imu_3dm_gx4_15_driver::Imu_Driver imu;

        std::mutex mu;
        std::vector<std::thread> imuThread;

        bool running;
        bool runningErrorFree;
        double gravity;
    };

}
#endif // VAL_IMU_3DM_GX4_15_INTERFACE_HPP_