#ifndef __SENSOR_INTERFACES_SIM_FORCE_TORQUE_SENSOR__
#define __SENSOR_INTERFACES_SIM_FORCE_TORQUE_SENSOR__

#include <val_robot_interface/ForceTorqueSensorInterface.hpp>
#include <turbodriver_api/SimTurbodriverForceTorqueSensor/SimTurbodriverForceTorqueSensor.hpp>
#include "nasa_common_logging/Logger.h"

namespace sensor_interface
{
    class SimForceTorqueSensor : public robot_hardware_interface::ForceTorqueSensorInterface
    {
    public:
        SimForceTorqueSensor() {}
        virtual ~SimForceTorqueSensor() {}

        virtual void makeHandle(const std::string &name, const std::string& configFilePath)
        {
            handleSensor.reset(new hardware_interface::ForceTorqueSensorHandle(name, "frame", force.data(), torque.data()));

            handleOffset.reset(new hardware_interface::ForceTorqueSensorHandle(name + "Offset", "frame", forceOffset.data(), forceOffset.data()));
        }

        virtual hardware_interface::ForceTorqueSensorHandle* getSensorHandle()
        {
            return handleSensor.get();
        }

        virtual hardware_interface::ForceTorqueSensorHandle* getOffsetHandle()
        {
            return handleOffset.get();
        }

        void Tare()
        {
            for (unsigned int i = 0; i < 3; i++)
            {
                forceOffset[i] = forceDynOffset[i];
                torqueOffset[i] = torqueDynOffset[i];
            }

            modeState_ = robot_hardware_interface::ForceTorqueSensorModeState::Tare;
        }

        void UnTare()
        {
            for (unsigned int i = 0; i < 3; i++)
            {
                forceOffset[i] = 0;
                torqueOffset[i] = 0;
            }

            modeState_ = robot_hardware_interface::ForceTorqueSensorModeState::UnTare;
        }

        virtual void update();

        virtual void initialize(const std::string& nodePath, SMT::SMTClient* smtClient, robot_variable::RobotVariableFactory* variableFactory)
        {
            if (!smtClient)
            {
                throw SMT::SMTException("SMTClient pointer is null!");
            }

            if(!variableFactory)
            {
                throw robot_variable::RobotVariableFactoryNullException();
            }

            // Register the mode names with the handle
            modeCommandMap["Tare"] = std::bind(&SimForceTorqueSensor::Tare, this);
            modeNames.push_back("Tare");
            modeCommandMap["UnTare"] = std::bind(&SimForceTorqueSensor::UnTare, this);
            modeNames.push_back("UnTare");

            modeStateMap[(int)robot_hardware_interface::ForceTorqueSensorModeState::Tare] = "Tare";
            modeStateMap[(int)robot_hardware_interface::ForceTorqueSensorModeState::UnTare] = "UnTare";
            modeStateMap[(int)robot_hardware_interface::ForceTorqueSensorModeState::Undefined] = "Undefined";

            forceTorqueApi.Subscribe(nodePath, smtClient);
        }

    protected:
        smt_interface::SimTurbodriverForceTorqueSensor forceTorqueApi;
        float val;
    };

}

#endif // __SIM_FORCE_TORQUE_SENSOR__