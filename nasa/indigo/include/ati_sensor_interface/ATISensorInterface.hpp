#ifndef __ATI_SENSOR_INTERFACE__
#define __ATI_SENSOR_INTERFACE__

#include <val_robot_interface/ForceTorqueSensorInterface.hpp>
#include <turbodriver_api/TurbodriverForceTorqueSensor/TurbodriverForceTorqueSensor.hpp>
#include <map>
#include "yaml-cpp/yaml.h"
#include <sys/stat.h>
#include <stdexcept>
#include "nasa_common_logging/Logger.h"

namespace ati_sensor_interface
{

    class ATISensorInterface : public robot_hardware_interface::ForceTorqueSensorInterface
    {
    public:
        ATISensorInterface() {}
        virtual ~ATISensorInterface() {}

        virtual void makeHandle(const std::string &name, const std::string& configFilePath);

        void Tare();

        void UnTare();

        virtual hardware_interface::ForceTorqueSensorHandle* getSensorHandle()
        {
            return handleSensor.get();
        }

        virtual hardware_interface::ForceTorqueSensorHandle* getOffsetHandle()
        {
            return handleOffset.get();
        }

        virtual void update();

        bool doesConfigFileExist(const std::string& configFileName)
        {
            struct stat buffer;
            return (stat (configFileName.c_str(), &buffer) == 0);
        }

        virtual void initialize(const std::string& nodePath, SMT::SMTClient* smtClient, robot_variable::RobotVariableFactory* variableFactory)
        {
            if (!smtClient)
            {
                std::string msg = "smt client input argument is null";
                throw SMT::SMTException(msg);
            }

            if(!variableFactory)
            {
                throw robot_variable::RobotVariableFactoryNullException();
            }

            // Register the mode names with the handle
            modeCommandMap["Tare"] = std::bind(&ATISensorInterface::Tare, this);
            modeNames.push_back("Tare");
            modeCommandMap["UnTare"] = std::bind(&ATISensorInterface::UnTare, this);
            modeNames.push_back("UnTare");

            modeStateMap[(int)robot_hardware_interface::ForceTorqueSensorModeState::Tare] = "Tare";
            modeStateMap[(int)robot_hardware_interface::ForceTorqueSensorModeState::UnTare] = "UnTare";
            modeStateMap[(int)robot_hardware_interface::ForceTorqueSensorModeState::Undefined] = "Undefined";

            forceTorqueApi.Subscribe(nodePath, smtClient);

            forceX = variableFactory->makeVariable<double>("/sensors" + nodePath + "/ForceX");
            forceY = variableFactory->makeVariable<double>("/sensors" + nodePath + "/ForceY");
            forceZ = variableFactory->makeVariable<double>("/sensors" + nodePath + "/ForceZ");
            torqueX = variableFactory->makeVariable<double>("/sensors" + nodePath + "/TorqueX");
            torqueY = variableFactory->makeVariable<double>("/sensors" + nodePath + "/TorqueY");
            torqueZ = variableFactory->makeVariable<double>("/sensors" + nodePath + "/TorqueZ");
        }

    protected:
        smt_interface::TurbodriverForceTorqueSensor forceTorqueApi;
        int16_t val;
        double transformationMatrix[6][6];
        double countsPerTorque;
        double countsPerForce;
    };

    typedef boost::shared_ptr<ATISensorInterface> ATISensorInterfacePtr;

} // end namespace smt_interface

#endif // __ATI_SENSOR_INTERFACE__
