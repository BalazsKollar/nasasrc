#ifndef __SIMIMUSENSORINTERFACE__HPP__
#define __SIMIMUSENSORINTERFACE__HPP__

// Includes:
#include <shared_memory_transport/smtclient.hpp>

//Namespaces:
namespace val_gazebo
{

    class SimImuSensorInterface
    {
    public:
//Contructors
        SimImuSensorInterface();
        SimImuSensorInterface(const std::string& nodePath, const std::string& hostUri = "http://localhost:11312");
//Destructor
        ~SimImuSensorInterface();
//Subscribe function
        void Subscribe(const std::string& nodePath, const std::string& hostUri = "http://localhost:11312");
//API Functions
        std::string getNodePath() const;
        void setAngularVelocityX(const float& val);
        void setAngularVelocityY(const float& val);
        void setAngularVelocityZ(const float& val);
        void setLinearAccelerationX(const float& val);
        void setLinearAccelerationY(const float& val);
        void setLinearAccelerationZ(const float& val);
        void setQuaternionW(const float& val);
        void setQuaternionX(const float& val);
        void setQuaternionY(const float& val);
        void setQuaternionZ(const float& val);
    private:
        std::string nodePath;
        std::shared_ptr<SMT::SMTClient> client;
//Commands in the command dictionary
//Subscriber declarations
    protected:
        std::shared_ptr<SMT::Resource> subscriberAngularVelocityX;
        std::shared_ptr<SMT::Resource> subscriberAngularVelocityY;
        std::shared_ptr<SMT::Resource> subscriberAngularVelocityZ;
        std::shared_ptr<SMT::Resource> subscriberLinearAccelerationX;
        std::shared_ptr<SMT::Resource> subscriberLinearAccelerationY;
        std::shared_ptr<SMT::Resource> subscriberLinearAccelerationZ;
        std::shared_ptr<SMT::Resource> subscriberQuaternionW;
        std::shared_ptr<SMT::Resource> subscriberQuaternionX;
        std::shared_ptr<SMT::Resource> subscriberQuaternionY;
        std::shared_ptr<SMT::Resource> subscriberQuaternionZ;
    };
//Close namespaces
}
#endif
