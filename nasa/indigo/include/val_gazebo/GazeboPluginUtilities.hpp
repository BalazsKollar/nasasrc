//
// Created by jordan on 6/21/16.
//

#ifndef __GAZEBO_PLUGIN_UTILITIES__
#define __GAZEBO_PLUGIN_UTILITIES__

#include <gazebo/common/common.hh>

namespace val_gazebo
{

    static double getDesiredGazeboPluginUpdateRateInSeconds(sdf::ElementPtr sdf)
    {
        double updateRate;
        if (!sdf->HasElement("updateRateHz"))
        {
            return updateRate = 0.0;
        }
        else
        {
            return updateRate = 1/sdf->GetElement("updateRateHz")->Get<double>();
        }
    }

    static double getDesiredGazeboPluginUpdateRateInHz(sdf::ElementPtr sdf)
    {
        double updateRate;
        if (!sdf->HasElement("updateRateHz"))
        {
            return updateRate = std::numeric_limits<int>::infinity();
        }
        else
        {
            return updateRate = sdf->GetElement("updateRateHz")->Get<double>();
        }
    }
}
#endif
