//
// Created by jlack on 10/20/16.
//

#ifndef __VAL_GAZEBO_SIM_TOOLS_HPP__
#define __VAL_GAZEBO_SIM_TOOLS_HPP__

#include <transmission_interface/transmission_interface.h>
#include <transmission_interface/transmission_info.h>
#include <transmission_interface/transmission_parser.h>
#include "shared_memory_transport/smtclient.hpp"
#include <tinyxml.h>
#include <yaml-cpp/yaml.h>
#include "nasa_common_logging/Logger.h"

namespace val_gazebo
{
    namespace sim_tools
    {
        enum RESULT{FAIL=0,SUCCESS=1,SKIP=2};

        /**
        * @brief Return boolean signifying whether or not a file exists
        * @param fileName The full file path
        * @return bool True of file exists, False if it doesn't
        */
        static bool doesFileExist(const std::string& fileName)
        {
            struct stat buffer;
            return (stat (fileName.c_str(), &buffer) == 0);
        }

        /**
         * @brief This method parses an XML string and returns a vector of shared_ptr's to ChildJointData objects.
         * @param urdfString Xml formatted string describing your URDF
         * @param jointName Name of the joint you are looking to get the children joints of
         * @param childJointDataPtrs A reference to pack the result into
         * @return bool True for success, false for failure
         */
        static bool getChildJoints(const std::vector<transmission_interface::TransmissionInfo> &transmissionInfos, const std::string jointName, std::vector<std::string> &childJoints, std::vector<double> &childJointRatios)
        {
            for(int i = 0;i<transmissionInfos.size(); i++)
            {
                if(transmissionInfos[i].joints_.size()==1)
                {
                    TiXmlDocument doc;

                    if(!doc.Parse(transmissionInfos[i].joints_[0].xml_element_.c_str()) && doc.Error())
                    {
                        NasaCommonLogging::Logger::log("gov.nasa.SharedMemorySimInterfacePlugin.checkTransmissionsForParentJoints", log4cpp::Priority::ERROR, "Failed to parse transmissions!");
                        return false;
                    }

                    if(std::strcmp(transmissionInfos[i].joints_[0].name_.c_str(),jointName.c_str())==0)
                    {
                        for(TiXmlElement* elem=doc.RootElement()->FirstChildElement("child"); elem != nullptr; elem = elem->NextSiblingElement("child"))
                        {
                            std::string childName;
                            double gearRatio = 0.;

                            if(elem->QueryStringAttribute("name", &childName) != TIXML_SUCCESS)
                            {
                                std::string msg = "Unable to get child joint name of transmission " + jointName;
                                NasaCommonLogging::Logger::log("gov.nasa.SharedMemorySimInterfacePlugin.checkTransmissionsForParentJoints", log4cpp::Priority::ERROR, msg);
                                return false;
                            }

                            childJoints.push_back(childName);
                            NasaCommonLogging::Logger::log("gov.nasa.SharedMemorySimInterfacePlugin.checkTransmissionsForParentJoints", log4cpp::Priority::INFO, "Adding joint " + childName + " as child of " + jointName);

                            if(elem->QueryDoubleAttribute("ratio", &gearRatio) != TIXML_SUCCESS)
                            {
                                std::string msg = "Unable to get ratio of child joint for transmission " + jointName;
                                NasaCommonLogging::Logger::log("gov.nasa.SharedMemorySimInterfacePlugin.checkTransmissionsForParentJoints", log4cpp::Priority::ERROR, msg);
                                return false;
                            }

                            childJointRatios.push_back(gearRatio);
                        }

                        return true;
                    }
                }
            }

            return true;
        }

        /**
         * @TODO - Get rid of this as soon as possible.
         * @brief This method parses a top level shared memory schedule file and creates the desired topics. This
         * was created as a hacky way to get around the fact that we can't handle more than 8 channels/nodes at a time
         * to deal with the fingers in simulation.
         * @param xmlString - The XML string is generally the URDF as string. An example of it is the following which
         * requires the package containing the config file and the relative path to said file.
         *
         * <schedule_topic_creator_file package="val_gazebo" relative_file_path="/configuration/valkyrie_sim_shm_topics.yaml"/>
         *
         * This file should look like the following,
         *
         * left_hand:
            thumb_roll: {package: val_config_files, schedule_file: /robonet_schedules/turbo_sim.yaml}

            where it'll create the topics in turbo_sim.yaml at location /left_hand/thumb_roll/blah
         *
         * @param client - SMT Client pointer
         * @return RESULT enum signifying the outcome of running this  method.
         */
        static RESULT createSharedMemoryTopics(const std::string &xmlString,SMT::SMTClient* client)
        {
            TiXmlDocument xmlDoc;

            if(!client)
            {
                NasaCommonLogging::Logger::log("gov.nasa.SimTools.createSharedMemoryTopics", log4cpp::Priority::ERROR, "SMTCore is nullptr!");
                return RESULT::FAIL;
            }

            if (!xmlDoc.Parse(xmlString.c_str()) && xmlDoc.Error())
            {
                return RESULT::FAIL;
            }

            TiXmlElement *root = xmlDoc.RootElement();

            TiXmlElement *element = root->FirstChildElement("schedule_topic_creator_file");

            if(!element)
            {
                NasaCommonLogging::Logger::log("gov.nasa.SimTools.createSharedMemoryTopics", log4cpp::Priority::DEBUG, "Xml element 'shm_remap_file' not found in URDF. If it is supposed to be there, then check for spelling.");
                return RESULT::SKIP;
            }
            else
            {
                NasaCommonLogging::Logger::log("gov.nasa.SimTools.createSharedMemoryTopics", log4cpp::Priority::INFO, "Found 'shm_remap_file' tag in URDF, will be performing shared memory topic mapping.");
            }

            std::string rosPackageName,relativeFilePath;

            if(element->QueryValueAttribute("package", &rosPackageName)!=TIXML_SUCCESS)
            {
                NasaCommonLogging::Logger::log("gov.nasa.SimTools.createSharedMemoryTopics", log4cpp::Priority::ERROR, "Xml attribute 'package' not found!");
                return RESULT::FAIL;
            }

            if(element->QueryValueAttribute("relative_file_path", &relativeFilePath)!=TIXML_SUCCESS)
            {
                NasaCommonLogging::Logger::log("gov.nasa.SimTools.createSharedMemoryTopics", log4cpp::Priority::ERROR, "Xml attribute 'relative_file_path' not found!");
                return RESULT::FAIL;
            }

            std::string packagePath = ros::package::getPath(rosPackageName);

            if(!std::strcmp(packagePath.c_str(),""))
            {
                std::string msg = "Unable to find ROS package " + rosPackageName;
                NasaCommonLogging::Logger::log("gov.nasa.SimTools.createSharedMemoryTopics", log4cpp::Priority::ERROR, msg);
                return RESULT::FAIL;
            }

            std::string yamlRemapConfigFilePath = packagePath + relativeFilePath;

            std::cout << yamlRemapConfigFilePath << std::endl;
            if(!doesFileExist(yamlRemapConfigFilePath))
            {
                std::string msg = "Shared memory remap config file " + yamlRemapConfigFilePath + " does NOT exist!";
                NasaCommonLogging::Logger::log("gov.nasa.SimTools.createSharedMemoryTopics", log4cpp::Priority::ERROR, msg);
                return RESULT::FAIL;
            }

            YAML::Node rootNode = YAML::LoadFile(yamlRemapConfigFilePath);

            if(!rootNode.IsMap())
            {
                NasaCommonLogging::Logger::log("gov.nasa.SimTools.createSharedMemoryTopics", log4cpp::Priority::ERROR, "Expected YAML map but got YAML::NodeType = " + rootNode.Type());
                return RESULT::FAIL;
            }

            for(auto i = rootNode.begin(); i!=rootNode.end();++i)
            {
                std::string channelName = i->first.as<std::string>();
                for(auto j = i->second.begin();j!=i->second.end();++j)
                {
                    YAML::Node key = j->first;
                    std::string shmNodeName = key.as<std::string>();

                    YAML::Node value = j->second;
                    std::string packageName;
                    if(value["package"])
                    {
                        packageName = value["package"].as<std::string>();
                    }
                    else
                    {
                        NasaCommonLogging::Logger::log("gov.nasa.SimTools.createSharedMemoryTopics", log4cpp::Priority::ERROR, "Your YAML doesn't have the required field 'package'.");
                        return RESULT::FAIL;
                    }

                    std::string relativeFilePath;
                    if(value["schedule_file"])
                    {
                        relativeFilePath = value["schedule_file"].as<std::string>();
                    }
                    else
                    {
                        NasaCommonLogging::Logger::log("gov.nasa.SimTools.createSharedMemoryTopics", log4cpp::Priority::ERROR, "Your YAML doesn't have the required field 'schedule_file'.");
                        return RESULT::FAIL;
                    }

                    std::string packagePath = ros::package::getPath(packageName);
                    if(packagePath=="")
                    {
                        NasaCommonLogging::Logger::log("gov.nasa.SimTools.createSharedMemoryTopics", log4cpp::Priority::ERROR, "Unable to find ROS package " + packageName);
                        return RESULT::FAIL;
                    }

                    std::string fullPath = packagePath + relativeFilePath;

                    if(!doesFileExist(fullPath))
                    {
                        std::string msg = "Shared memory schedule file " + fullPath + " does NOT exist!";
                        NasaCommonLogging::Logger::log("gov.nasa.SimTools.createSharedMemoryTopics", log4cpp::Priority::ERROR, msg);
                        return RESULT::FAIL;
                    }

                    YAML::Node componentScheduleNode = YAML::LoadFile(fullPath);

                    if(!componentScheduleNode.IsSequence())
                    {
                        NasaCommonLogging::Logger::log("gov.nasa.SimTools.createSharedMemoryTopics", log4cpp::Priority::ERROR, "Expected YAML sequence but got YAML::NodeType = " + componentScheduleNode.Type());
                        return RESULT::FAIL;
                    }

                    for(int k = 0; k<componentScheduleNode.size(); k++)
                    {
                        YAML::Node sequenceNode = componentScheduleNode[k];
                        if(!sequenceNode["name"] || !sequenceNode["offset"] || !sequenceNode["type"])
                        {
                            NasaCommonLogging::Logger::log("gov.nasa.SimTools.createSharedMemoryTopics", log4cpp::Priority::ERROR, "YAML schedule file missing required field, MUST contain 'name', 'offset', and 'type'!");
                            return RESULT::FAIL;
                        }

                        std::string name,type;
                        int offset,size;
                        name = sequenceNode["name"].as<std::string>();
                        type = sequenceNode["type"].as<std::string>();
                        offset = sequenceNode["offset"].as<int>();

                        if(type=="float")
                        {
                            size = sizeof(float);
                            type="float32";
                        }
                        else if(type=="double")
                        {
                            size = sizeof(double);
                            type="float64";
                        }
                        else if(type=="int32_t")
                        {
                            size = sizeof(int32_t);
                            type="int32";
                        }
                        else if(type=="uint32_t")
                        {
                            size = sizeof(uint32_t);
                            type="uint32";
                        }
                        else if(type=="int16_t")
                        {
                            size = sizeof(int16_t);
                            type="int16";
                        }
                        else if(type=="uint16_t")
                        {
                            size = sizeof(uint16_t);
                            type="uint16";
                        }
                        else
                        {
                            NasaCommonLogging::Logger::log("gov.nasa.SimTools.createSharedMemoryTopics", log4cpp::Priority::ERROR, "SMT type " + type + " not supported!");
                            return RESULT::FAIL;
                        }

                        try
                        {
                            std::string fullTopicName = "/" + channelName + "/" + shmNodeName + "/" + name;
                            client->createResource(fullTopicName, type, offset, size, "big", "");

                            NasaCommonLogging::Logger::log("gov.nasa.SimTools.createSharedMemoryTopics", log4cpp::Priority::DEBUG, "Creating topic " + fullTopicName);
                        }
                        catch(SMT::SMTException &e)
                        {
                            NasaCommonLogging::Logger::log("gov.nasa.SimTools.createSharedMemoryTopics", log4cpp::Priority::ERROR, "Exception encountered while creating topic " + name + ": " + e.what());
                            return RESULT::FAIL;
                        }
                    }
                }
            }

            return RESULT::SUCCESS;
        }
    }
}

#endif //__VAL_GAZEBO_SIM_TOOLS_HPP__
