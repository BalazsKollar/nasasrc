#ifndef ROBOT_SIM_INTERFACE_HPP
#define ROBOT_SIM_INTERFACE_HPP

#include "val_robot_interface/Robot_Interface.hpp"
#include <gazebo/physics/Model.hh>
#include "val_gazebo/SimJoint.hpp"
#include "nasa_common_logging/Logger.h"

namespace val_gazebo
{
    class RobotSimInterface : public robot_hardware_interface::RobotInterface
    {
    public:
        RobotSimInterface(gazebo::physics::ModelPtr modelPointer,SMT::SMTClient* smtClient, robot_variable::RobotVariableFactory* robotVariableFactory);
        ~RobotSimInterface();

        void read(const ros::Time& timeStamp);
        void write(const ros::Time& timeStamp);

        bool initialize(const std::string& parameterName, const ros::NodeHandle& nodeToLoadFrom);

    protected:
        robot_hardware_interface::JointPtr createJoint(const std::string &jointName);

    private:
        void extractJointGainsFromRobotDescription(const std::string& parameterName, const ros::NodeHandle& nodeToLoadFrom);
        void extractJointInterfacesFromRobotDescription(const std::string& parameterName, const ros::NodeHandle& nodeToLoadFrom);

        gazebo::physics::ModelPtr robotModel;
        std::map<std::string,std::map<std::string,double>> jointGainsMap;
        std::map<std::string, std::string> jointNameInterfaceTypeMap;
        std::map<std::string,std::vector<std::string>> childJointMap;
        std::map<std::string,std::vector<double>> childJointRatioMap;
        std::map<std::string, std::shared_ptr<val_gazebo::SimJoint>> simJointMap;
    };
}

#endif