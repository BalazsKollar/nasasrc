#ifndef SHARED_MEMORY_SIM_INTERFACE_PLUGIN_HPP
#define SHARED_MEMORY_SIM_INTERFACE_PLUGIN_HPP

/**
/*
/*  SharedMemorySimSimInterface plugin is a gazebo plugin
/*  that ushers actuator and joint information back and forth
/*  between shared memory and gazebo.
/*
*/

#include "val_gazebo/RobotSimInterface.hpp"
#include "gazebo/physics/physics.hh"
#include <gazebo/common/common.hh>
#include <ros/ros.h>
#include <map>
#include "val_gazebo/SimImuSensorInterface.hpp"
#include "val_gazebo/SimForceTorqueSensorInterface.hpp"
#include "robot_variable_smt/SmtVariableFactory.hpp"

namespace val_gazebo
{
    static double maxAllowedSleepTime = 1.e3; //1 millisecond in microseconds
    static double defaultSleepTime = 10; //microseconds
    static double timeBeforeBlockingStarts = 30.;//seconds

    class SharedMemorySimInterfacePlugin : public gazebo::ModelPlugin
    {
    public:

        SharedMemorySimInterfacePlugin() : currentIter(0), ticsBeforeUpdate(0)
        {
            //To be more general, need to extract this URI out somewhere else eventually
            smtClient.reset(new SMT::SMTClient("http://127.0.0.1:11312"));
            robotVariableFactory.reset(new robot_variable_smt::SmtVariableFactory(smtClient.get()));

            this->updateTrigger = robotVariableFactory->makeVariable<int16_t>("/sim/trigger/update_trigger");
        }

        ~SharedMemorySimInterfacePlugin()
        {
            gazebo::event::Events::DisconnectWorldUpdateBegin(this->updateConnection);
            this->rosNode->shutdown();
        }

        void Load(gazebo::physics::ModelPtr parent, sdf::ElementPtr sdf);
        void Init();
        void Reset();

    private:
        void Update();
        void findUrdfSensors();

        std::shared_ptr<SMT::SMTClient> smtClient;
        std::shared_ptr<robot_variable::RobotVariableFactory> robotVariableFactory;

        gazebo::physics::ModelPtr robotModel;
        std::unique_ptr<val_gazebo::RobotSimInterface> robotInterface;
        gazebo::event::ConnectionPtr updateConnection;

        ros::NodeHandlePtr rosNode;
        gazebo::physics::ModelPtr gazeboModel;

        std::map < std::string, gazebo::sensors::ImuSensorPtr > gazeboImuSensorMap;
        std::map < std::string, SimImuSensorInterface > simImuSensorApiMap;
        std::map < std::string, std::string > simImuSensorSharedMemoryNodeMap,simForceTorqueSensorSharedMemoryNodeMap;
        std::map < std::string, gazebo::sensors::ForceTorqueSensorPtr > gazeboForceTorqueSensorMap;
        std::map < std::string, SimForceTorqueSensorInterface > simForceTorqueSensorMap;
        std::map < std::string, gazebo::math::Quaternion> simImuStaticTransformMap,simImuInverseTransformToParent;
        gazebo::sensors::Sensor_V sensorList;
        gazebo::math::Quaternion quaternion;
        double updateRateInSeconds,gazeboPhysicsUpdateRate,initialTime;
        unsigned int currentIter, ticsBeforeUpdate;
        std::shared_ptr<robot_variable::RobotVariable<int16_t>> updateTrigger;
    };

}

#endif
