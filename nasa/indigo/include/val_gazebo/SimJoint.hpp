#ifndef SIM_JOINT_HPP
#define SIM_JOINT_HPP

/**
/*  Author: Jordan Lack - jordan.t.lack@nasa.gov
/*
/*  The SimJoint class is an instance of a joint that can
/*  be used in gazebo.
/*
*/

#include <gazebo/physics/Joint.hh>
#include <gazebo/common/common.hh>
#include <val_robot_interface/JointInterface.hpp>
#include "val_gazebo/SimTools.hpp"
#include <string>

namespace val_gazebo
{
    class SimJoint : public robot_hardware_interface::JointInterface
    {
    public:
        SimJoint(gazebo::physics::JointPtr jointPtr,const double &kp=0.0, const double &kd=0.0, const double &ki=0.0, const std::string &commandInterfaceType="EffortJointInterface");
        ~SimJoint();
        void updateStateData(ros::Time const& timeStamp);
        void updateStateCommand(ros::Time const& timeStamp);

        inline double computePositionControlledJointEffort(const double dt)
        {
            this->pidController.Update(position_ - positionCommand_, dt);
            return this->pidController.GetCmd();
        }

        // Do NOT call this without first updating dt, otherwise it'll produce undesirable results!
        inline double computePositionControlledJointEffort()
        {
            return computePositionControlledJointEffort(this->dt);
        }

        inline void setJointEffort(const double effort)
        {
            this->joint->SetForce(0, effort);
        }

        inline double getPosition() const
        {
            return position_;
        }

        inline double getVelocity() const
        {
            return velocity_;
        }

        inline double getEffort() const
        {
            return effort_;
        }

        inline void setPositionCommand(const double &command)
        {
            this->positionCommand_ = command;
        }

        inline void setVelocityCommand(const double &command)
        {
            this->velocityCommand_ = command;
        }

        inline void setEffortCommand(const double &command)
        {
            this->velocityCommand_ = command;
        }

    private:
        gazebo::physics::JointPtr joint;
        std::string commandInterfaceType;
        ros::Time lastControllerUpdateTime;
        double dt;
        gazebo::common::PID pidController;
    };
}

#endif
