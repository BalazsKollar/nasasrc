#ifndef __VAL_GAZEBO_SIM_FORCE_TORQUE_SENSOR_INTERFACE_HPP__
#define __VAL_GAZEBO_SIM_FORCE_TORQUE_SENSOR_INTERFACE_HPP__

// Includes:
#include <shared_memory_transport/smtclient.hpp>

//Namespaces:
namespace val_gazebo
{

    class SimForceTorqueSensorInterface
    {
    public:
//Contructors
        SimForceTorqueSensorInterface();
        SimForceTorqueSensorInterface(const std::string& nodePath, const std::string& hostUri = "http://localhost:11312");
//Destructor
        ~SimForceTorqueSensorInterface();
//Subscribe function
        void Subscribe(const std::string& nodePath, const std::string& hostUri = "http://localhost:11312");
//API Functions
        std::string getNodePath() const;
        void setForceX(const float val);
        void setForceY(const float val);
        void setForceZ(const float val);
        void setTorqueX(const float val);
        void setTorqueY(const float val);
        void setTorqueZ(const float val);
    private:
        std::string nodePath;
        std::shared_ptr<SMT::SMTClient> client;
//Commands in the command dictionary
//Subscriber declarations
    protected:
        std::shared_ptr<SMT::Resource> subscriberForceX;
        std::shared_ptr<SMT::Resource> subscriberForceY;
        std::shared_ptr<SMT::Resource> subscriberForceZ;
        std::shared_ptr<SMT::Resource> subscriberTorqueX;
        std::shared_ptr<SMT::Resource> subscriberTorqueY;
        std::shared_ptr<SMT::Resource> subscriberTorqueZ;
    };
//Close namespaces
}
#endif
