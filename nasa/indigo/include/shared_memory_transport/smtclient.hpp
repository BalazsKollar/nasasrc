#ifndef __SMT_CLIENT_HPP__
#define __SMT_CLIENT_HPP__

#include <cstdlib>
#include <string>
#include <errno.h>
#include <cstring>
#include <iostream>
#include <algorithm>

#include <xmlrpc-c/girerr.hpp>
#include <xmlrpc-c/base.hpp>
#include <xmlrpc-c/client.h>
#include <xmlrpc-c/client_simple.hpp>

#include "resource.hpp"

namespace SMT
{

#define SMT_SIZE_LIMIT 2e7

/**
 * @brief The Shared Memory Transport Client class
 * The SMTClient serves as an interface to the Shared Memory Transport Core.
 * On a given system, there will be one shared memory core which serves as
 * a name server for all agents who want to post information for other clients
 * to consume.  Simply instantiate this class by passing in the URI for the SMT
 * Core.  The default is "http://localhost:11312"
 */
class SMTClient
{
protected:
    xmlrpc_c::clientSimple client; //XMLRPC client object
    const std::string hostURI;     //the SMT core hostURI

public:
    SMTClient(const std::string &hostURI="http://localhost:11312");
    virtual ~SMTClient();
    bool alive();
    std::vector<std::string> listTopics();
    SMT::Resource getResource(const std::string &topic);
    SMT::ResourceInfo getResourceInfo(const std::string &topic);
    SMT::Resource createResource(const std::string &name,
                                 const std::string &type,
                                 const int &offset,
                                 const int &size,
                                 const std::string &endian,
                                 const std::string &description);
    SMT::Resource createAbstractResource(
                                     const std::string &name,
                                     const std::string &abstractType,
                                     const std::map<std::string, std::string> &abstractInfo,
                                     const int &offset,
                                     const int &size,
                                     const std::string &endian,
                                     const std::string &description);
    void destroyResource(const std::string &topic);
    void registerLocation(SMT::ResourceInfo resource);
    bool shutdown(bool block=true);
    void unregisterLocation(const std::string &name);
protected:
    std::string createShmFile(std::string resourceName, int size);
    void deleteShmFile(std::string resourceName);
};


} //namespace SMT
#endif // __SMT_CLIENT_HPP__
