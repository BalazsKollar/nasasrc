#ifndef __SMTCOREDAEMON_HPP__
#define __SMTCOREDAEMON_HPP__

#include <sys/socket.h>
#include <time.h>
#include <thread>    // for std::thread
#include <unistd.h>  // for sleep
#include <string>    // for std::to_string, std::string
#include <cstdlib>   // for system(), rand()
#include <stdlib.h>  // for rand_r()
#include <cassert>
#include <shared_memory_transport/smtclient.hpp>
#include <exception>

#include <nasa_common_logging/Logger.h>

#define SMTCORE_START_MAX_ATTEMPT 50
#define SMTCORE_MIN_PORT 11312
#define SMTCORE_MAX_PORT 12312
#define SMTCORE_DEFAULT_PORT SMTCORE_MIN_PORT
#define uSEC_TO_SEC 1000000.0
#define SMTCORE_SHUTDOWN_TIMEOUT 10 * uSEC_TO_SEC

/**
  * @brief Wait for a random number of seconds between min and max
  * @param min Minimum wait time in seconds
  * @param max Maximum wait time in seconds
  */ 
float waitRandom(float min=0.05, float max=0.2)
{
	float time = (rand() % (int)(max * uSEC_TO_SEC - min * uSEC_TO_SEC)) + min;
	usleep(time);
}

/**
  * @brief Wait for a random number of seconds between min and max
  * @param minPort Lowest port number
  * @param maxPort Highest port number
  */
unsigned int getRandomPort(unsigned int minPort=SMTCORE_MIN_PORT, unsigned int maxPort=SMTCORE_MAX_PORT)
{
	assert(maxPort >= minPort);
	unsigned int diff = maxPort - minPort;
	//std::cout << "getRandomPort: diff = " << diff << std::endl;
	int val = rand() % diff;
	//std::cout << "rand value = " << val << std::endl;
	unsigned int port = minPort + val;
	assert(minPort <= port && port <= maxPort);
	return port;
}

/**
  * @brief Start SMTCore on a given port
  *
  * This function makes a system call to run the Python script that starts SMTCore.
  * Once started, the function call will block until SMTCore exits.  This function
  * was meant to be run in it's own thread. 
  *
  * @param port Port number for SMTCore
  */
void startSmt(int port)
{
	//std::cout << "\n\nATTEMPT OPEN SMTCORE ON PORT:[" << port << "]\n\n" << std::endl;
	std::string callString = std::string("smtcore --port ") + std::to_string(port);
	int val = system(callString.c_str());
	if(val > 0)
	{
		std::cout << "return val = " << val << std::endl;	
	}
}


/**
  * SMTCoreDaemon
  *
  * The SMTCoreDaemon is useful for starting up and shuting down
  * an smtcore on an arbitrary port in another program.  While the 
  * original use-case was for testing, this could be used in any
  * C++ program that needs to control the starting and stopping
  * of an smtcore.  After construction, the "startSmtCore" method
  * will try to start an smtcore on the given port.  If that port
  * already has an smtcore, the next port will be tried.  This process
  * is continued until either an smtcore has been successfully started
  * on an empty port, or the "max_tries" has been reached.  In order to
  * stop the smtcore, simply call the "shutdownSmtcore" method.  This
  * method will block until the smtcore has been shut down.  If you 
  * want to start smtcore and simply block your program, use the 
  * "waitForShutdown" method.  This method will block until the smtcore
  * has shutdown, more than likely by some other process.  When the SMTCoreDaemon
  * object goes out of scope, the smtcore will be shutdown automatically
  * via this method's destructor.
  *
  *
  */
class SMTCoreDaemon
{
public:
	SMTCoreDaemon() : smtCorePort(-1){
		srand(getpid());
	}
	virtual ~SMTCoreDaemon()
	{
		shutdownSmtCore();
	}

	/**
	  * @brief SMTCoreDaemon::getPort - Get the port of the smtcore
	  * @return Port number or -1 if server not active or error.
	  */
	int getPort()
	{
		return smtCorePort;
	}

	/**
	  * @brief SMTCoreDaemon::startSmtCore - Start smtcore in it's own thread.
	  *
	  * This method starts up smtcore in it's own thread.  This method will block
	  * until the server is active, or the maximum number of tries has been reached.
	  * The server will be started on a random port between SMTCORE_MIN_PORT and 
	  * SMTCORE_MAX_PORT.
	  *
	  * @param randomziePort If true, smtcore will start on a randomized port between 
	  *                      SMTCORE_MIN_PORT and SMTCORE_MAX_PORT.
	  * @param port Port to start smtcore.  If randomizePort is true, this value is ignored.
	  * @param max_tries Max number of ports to try to start the server.  If randomizePort is true,
	                     the port will increment in subsequent tries, otherwise, the port will stay the same.
	  * @return Return the port number of the server or -1 on error.
	  */

	int startSmtCore(bool randomizePort=false, int port=SMTCORE_DEFAULT_PORT, int max_tries=SMTCORE_START_MAX_ATTEMPT, bool verbose=false)
	{
		this->verbose = verbose;
		// Get the starting random port
		if(randomizePort)
		{
			port = getRandomPort();
		}
		int tries = 0;

		while(tries < max_tries)
		{
			// Wait a short, random amout of time.  Because several SMTCoreDaemons
			// could be started at the same time, a random wait helps a race condition.
			waitRandom();

			std::string uri = "http://localhost:" + std::to_string(port);
			smtClient.reset(new SMT::SMTClient(uri));

			// First, make sure we cannot connect to an smtcore
			if(!smtClient->alive())
			{
				// Start SMTCore in it's own thread.  Note that this is a blocking call.
				// When this call returns, smtcore should be running
				smtCoreThread = startSmtThread(port);

				// spin up smtcore.  If there is an error, the thread should die fairly quickly
				if(smtCoreThread.joinable())
				{
					smtCorePort = port;
					return port;
				}
			}

			// If smtcore isn't started, increment the port and try again.

			std::string message = "Port:[" + std::to_string(port) + "] is in use.  Trying again...";
			NasaCommonLogging::Logger::getCategory("gov.nasa.nasa_common_logging.SMTCoreDaemon") << log4cpp::Priority::WARN << message;
			if(randomizePort)
			{
				port = port + 1;
			}
			tries = tries + 1;
		}
		NasaCommonLogging::Logger::getCategory("gov.nasa.nasa_common_logging.SMTCoreDaemon") << log4cpp::Priority::ERROR <<  "Failed to get a valid port for SMTCore";
		throw std::runtime_error("Failed to get a valid port for SMTCore");
	}

	/**
	  * @bried SMTCoreDaemon::shutdownSmtCore - Shutdown the smtcore
	  * 
	  * Stop the smtcore server.  This method will call waitForShutdown()
	  * and blocks until the server has shutdown.
	  *
	  */
	void shutdownSmtCore()
	{
		if(verbose)
		{
			std::cout << "shutdownSmtCore [" << smtCorePort << "]" << std::endl;
		}
		if (smtCoreThread.joinable())
		{
			smtClient->shutdown(true);
			smtCoreThread.join();
		}
	}

	/**
	  * @brief SMTCoreDaemon::waitForShutdown - Block until the server has been shutdown.
	  */
	void waitForShutdown()
	{
		if (smtCoreThread.joinable())
		{
			smtCoreThread.join();
		}
	}

protected:

	/**
	  * @bried SMTCoreDaemon::startSmtThread - Start the smtcore in a thread
	  *
	  * Start an smtcore on the given port in it's own thread.  This method
	  * will block until the smtcore responds to an "alive()" RPC call.
	  * 
	  * @param port - Port for the smtcore
	  * @return The thread of execution containing the smtcore
	  */
	std::thread startSmtThread(int port)
	{
		std::thread t(startSmt, port);

		// Poll smtcore until it is alive
		while(!smtClient->alive())
		{
			// wait for just a little bit before polling again
			usleep(10000);
		}
		return t;
	}

	std::shared_ptr<SMT::SMTClient> smtClient;
	std::thread smtCoreThread;
	int smtCorePort;
	bool verbose;
};

#endif //__SMTCOREDAEMON_HPP__