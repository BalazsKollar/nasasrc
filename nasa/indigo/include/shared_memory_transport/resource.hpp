#ifndef RESOURCE_HPP
#define RESOURCE_HPP


#include <memory>        // for shared_ptr
#include <sys/mman.h>    // for memory map header
#include <netinet/in.h>  // for ntohs, htons, etc.
#include <iostream>      // for std::cout, printing
#include <fcntl.h>       // for "open"
#include <stdexcept>     // for std::runtime_error

#include <sstream>       // for << used in CustomRegister
#include <dlfcn.h>       // for dynamic loading in CustomRegister

#include <stdint.h>

// for fstat(), used to figure out the size of the shared memory file
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

// XML RPC headers
#include <xmlrpc-c/girerr.hpp>
#include <xmlrpc-c/base.hpp>
#include <xmlrpc-c/client.h>
#include <xmlrpc-c/client_simple.hpp>


namespace SMT
{

class SMTException : public std::runtime_error
{
public:
    SMTException( const std::string & msg = "" ) : std::runtime_error(msg){ }
};

/**
 * @brief The MemoryMap struct - holds information on mapped memory
 * "fd" is the file descriptor for the mapped memory file, and "length" is the size of the file.
 * "data" is a character pointer to beginning of the memory map.  This struct will unmap the the memory
 * when it goes out of scope.
 */
struct MemoryMap {
    int fd;
    char* data;
    size_t length;

    ~MemoryMap() {
        munmap(data, length);
        close(fd);
    }
};

typedef std::shared_ptr<MemoryMap> MemoryMapPtr;
typedef std::map<std::string,MemoryMapPtr> MemoryMapLookupTable; // map of location name to memory map objects
extern MemoryMapLookupTable global_mapped_memory_regions; // global map object

// XML RPC array
typedef std::vector<xmlrpc_c::value> carray;
// XML RPC struct
typedef std::map<std::string, xmlrpc_c::value> cstruct;
typedef std::pair<std::string, xmlrpc_c::value> cpair;

/**
 * @brief The ResourceInfo class
 *  The ResourceInfo class is a container for data stored on the SMTCore about
 *  a resource.  This class will decode the "location" portion of the XML RPC
 *  structure returned when a "getLocation" call is made to the SMT Core.
 */
class ResourceInfo
{
public:
    ResourceInfo(const std::string &name,
                 const std::string &type,
                 const std::string &abstractType,
                 const std::map<std::string, std::string> &abstractInfo,
                 const std::string &readPath,
                 const std::string &writePath,
                 const int &readOffset,
                 const int &writeOffset,
                 const int &size,
                 const std::string &endian,
                 const std::string &description);

    ResourceInfo(xmlrpc_c::value value);
    xmlrpc_c::value_struct getValueStruct();
    void print();

    std::string const &getName() const { return name; }
    std::string const &getType() const { return type; }
    std::string const &getAbstractType() const { return abstractType;}
    std::map<std::string, std::string> const &getAbstractInfo() const{ return abstractInfo;}
    std::string const &getReadPath() const{ return readPath; }
    std::string const &getWritePath() const{ return writePath; }
    int const &getReadOffset() const{ return readOffset; }
    int const &getWriteOffset() const{ return writeOffset; }
    int const &getSize() const{ return size; }
    std::string const &getEndian() const{ return endian; }
    std::string const &getDescription() const{ return description; }
protected:
    std::string name;
    std::string type;
    std::string abstractType;
    std::map<std::string, std::string> abstractInfo;
    std::string readPath;
    std::string writePath;
    int readOffset;
    int writeOffset;
    int size;
    std::string endian;
    std::string description;
};

/**
 * @brief The Resource class
 * The shared memory resource class is the primary object used to get / set
 * values to/from a shared memory location in the SMT framework.
 */
class Resource
{
public:
    enum PtrDir{
        READ_WRITE,
        READ,
        WRITE
    };

    Resource(ResourceInfo resourceInfo);
    char *getPtr(PtrDir dir=READ_WRITE);
    std::string getType();
    std::map<std::string,std::string> getAbstractInfo();
    std::string getAbstractInfo(std::string abstractInfoKey);

    /**
     * @brief get the resource's data value
     * The get method is a template in order to handle the various types of
     * shared memory data.  Assuming that the Resource has been constructed,
     * an example of how to read the resource as a float is as follows:
     * @code{.cpp}
     *   float val;
     *   val = my_resource.get<float>();
     * @endcode
     *
     * @param T Return type.
     * @param dir Direction of the pointer.  Default is READ
     */
    template <class T>
    T get(PtrDir dir=READ)
    {
        if(sizeof(T) <= 2)
        {
            uint16_t val = ntohs(*(uint16_t*)getPtr(dir));
            return *(T *)&val;
        }
        else if(sizeof(T) <= 4)
        {
            uint32_t val = ntohl(*(uint32_t*)getPtr(dir));
            return *(T *)&val;
        }
        else
        {
            uint64_t val = be64toh(*(uint64_t*)getPtr(dir));
            return *(T *)&val;
        }
    }

    /**
     * @brief set the resource's data value
     * The set method is a template in order to handle the various types of
     * shared memory data.  Assuming that the Resource has been constructed,
     * an example of how to set the resource as a float is as follows:
     * @code{.cpp}
     *   float val = 1.234;
     *   my_resource.set<float>(val);
     * @endcode
     *
     * @param T Return type.
     * @param dir Direction of the pointer.  Default is WRITE
     */
    template <class T>
    void set(const T& val, PtrDir dir=WRITE)
    {
        if(sizeof(T) <= 2)
        {
            *(uint16_t*)getPtr(dir) = htons(*(uint16_t*)&val);
        }
        else if(sizeof(T) <= 4)
        {
            *(uint32_t*)getPtr(dir) = htonl(*(uint32_t*)&val);
        }
        else
        {
            *(uint64_t*)getPtr(dir) = htobe64(*(uint64_t*)&val);
        }
    }
protected:
    void mapResource(const std::string &path, int offset, int size, MemoryMapPtr &memoryMap, char* &dataPtr);
    bool split_rw;
    MemoryMapPtr mem_map1;
    MemoryMapPtr mem_map2;
    char* data1;    // read if split, else read/write
    char* data2;    // write
    std::string name;
    std::string type;
    std::map<std::string,std::string>abstractInfo;

};

} //namespace SMT
#endif // RESOURCE_HPP
