#ifndef __MODE_STATES_DECLARE_HPP__
#define __MODE_STATES_DECLARE_HPP__

namespace robot_hardware_interface
{

    //! @brief ModeState is a stuct that contains the modes the actuator interfaces can take
    struct ModeState
    {
        static int const Resetting =        1;
        static int const Parked =           2;
        static int const BrakeReleased =    3;
        static int const MotorMode =        4;
        static int const PositionMode =     5;
        static int const TorqueMode =       6;
        static int const CurrentMode =      7;
        static int const VelocityMode =     8;
        static int const ImpedanceMode =    9;
        static int const Undefined =       -1;
    };

    struct EnableState
    {
        static int const Disabled =     1;
        static int const Faulted =      2;
        static int const Ready =        3;
        static int const NotReady =     4;
        static int const Undefined =   -1;
    };

    struct ForceTorqueSensorModeState
    {
        static int const Tare =        1;
        static int const UnTare =      2;
        static int const Undefined =  -1;
    };
}

#endif