//! @file BaseAthenaJointActuatorStates.hpp
#ifndef __BASE_ATHENA_JOINTACTUATORSTATES_HPP__
#define __BASE_ATHENA_JOINTACTUATORSTATES_HPP__

#include <memory>
#include <functional>

#include <transmission_interface/transmission.h>
#include <val_robot_interface/ModeInterface.hpp>
#include "val_robot_interface/ModeStates.hpp"
#include "val_robot_interface/JointStateStructs.hpp"

namespace robot_hardware_interface
{
    //! @brief BaseAthenaJointActuator is a base class for the joint and actuator interfaces
    class BaseAthenaJointActuator
    {
    public:
        BaseAthenaJointActuator() :
            stateData(),
            positionCommand(),
            velocityCommand(),
            effortCommand(),
            modeState_(ModeState::Undefined),
            enableState_(EnableState::Undefined),
            powerGood_(true),
            initialized_(false),
            enabled_(false)
        {
            position_.push_back(0.0);
            velocity_.push_back(0.0);
            effort_.push_back(0.0);

            effortCommand_.push_back(0.0);
            velocityCommand_.push_back(0.0);
            positionCommand_.push_back(0.0);

            stateData.position = position_.data();
            stateData.velocity = velocity_.data();
            stateData.effort = effort_.data();
            positionCommand.command = positionCommand_.data();
            velocityCommand.command = velocityCommand_.data();
            effortCommand.command = effortCommand_.data();
        }

        virtual ~BaseAthenaJointActuator() {}

        //virtual void updateEnableState() {}

        State const& getData() const                        {return stateData;}
        Command const& getPositionCommand() const           {return positionCommand;}
        Command const& getVelocityCommand() const           {return velocityCommand;}
        Command const& getEffortCommand() const             {return effortCommand;}

        void resizeStateData(std::size_t toSize)
        {
            position_.resize(toSize);
            velocity_.resize(toSize);

            for (unsigned int i = 0; i < position_.size(); i++)
            {
                position_[i] = 0.0;
                velocity_[i] = 0.0;
            }

            stateData.position = position_.data();
            stateData.velocity = velocity_.data();
        }

        void resizeCommandData(std::size_t toSize)
        {
            effort_.resize(toSize);
            effortCommand_.resize(toSize);
            velocityCommand_.resize(toSize);
            positionCommand_.resize(toSize);

            for (unsigned int i = 0; i < effort_.size(); i++)
            {
                effort_[i] = 0.0;
                effortCommand_[i] = 0.0;
                velocityCommand_[i] = 0.0;
                positionCommand_[i] = 0.0;
            }

            stateData.effort = effort_.data();
            positionCommand.command = positionCommand_.data();
            velocityCommand.command = velocityCommand_.data();
            effortCommand.command = effortCommand_.data();
        }
        /** Return the attribute by name
          *
          */
        State getAttribute(std::string const& name)
        {
            if (name == "stateData") { return getData(); }

            if (name == "positionCommand") { return State(positionCommand_.data(), velocity_.data(), effort_.data()); }

            if (name == "velocityCommand") { return State(position_.data(), velocityCommand_.data(), effort_.data()); }

            if (name == "effortCommand") { return State(position_.data(), velocity_.data(), effortCommand_.data()); }

            std::stringstream sstr; sstr << "Attribute '" << name << "' not recognized.";
            throw hardware_interface::HardwareInterfaceException(sstr.str());
        }

        ModeHandle* getModeHandle()
        {
            robot_hardware_interface::ModeData modeData;

            modeData.name = name_+"_mode";
            modeData.data_ = &modeState_;
            modeData.modeNames_ = modeNames;
            modeData.modeMap_ = &modeCommandMap;
            modeData.stateMap_ = &modeStateMap;

            modeHandle_.reset(new robot_hardware_interface::ModeHandle(modeData));

            return modeHandle_.get();
        }

        ModeStateHandle* getModeStateHandle()
        {
            robot_hardware_interface::ModeData modeData;

            modeData.name = name_+"_mode";
            modeData.data_ = &modeState_;
            modeData.modeNames_ = modeNames;
            modeData.modeMap_ = &modeCommandMap;
            modeData.stateMap_ = &modeStateMap;

            modeStateHandle_.reset(new robot_hardware_interface::ModeStateHandle(modeData));

            return modeStateHandle_.get();
        }

        ModeHandle* getEnableHandle()
        {
            robot_hardware_interface::ModeData modeData;

            modeData.name = name_+"_enable";
            modeData.data_ = &enableState_;
            modeData.modeNames_ = enableNames;
            modeData.modeMap_ = &enableCommandMap;
            modeData.stateMap_ = &enableStateMap;

            enableHandle_.reset(new robot_hardware_interface::ModeHandle(modeData));
        }

        ModeStateHandle* getEnableStateHandle()
        {
            robot_hardware_interface::ModeData modeData;

            modeData.name = name_+"_enable";
            modeData.data_ = &enableState_;
            modeData.modeNames_ = enableNames;
            modeData.modeMap_ = &enableCommandMap;
            modeData.stateMap_ = &enableStateMap;

            enableStateHandle_.reset(new robot_hardware_interface::ModeStateHandle(modeData));

            return enableStateHandle_.get();
        }

        std::string name_;
        std::string node_;
        std::string api_tag_;

        void enable() { enabled_ = true; }
        void disable() { enabled_ = false; }

    protected:

        //state and command data declaration for the interface
        robot_hardware_interface::State stateData;
        robot_hardware_interface::Command positionCommand;
        robot_hardware_interface::Command velocityCommand;
        robot_hardware_interface::Command effortCommand;

        //individual elements for interface class allowing the state and command data to be
        //pulled out
        std::vector<double> position_;
        std::vector<double> velocity_;
        std::vector<double> effort_;

        std::vector<double> effortCommand_;
        std::vector<double> velocityCommand_;
        std::vector<double> positionCommand_;

        //declaration of mode for the joint actuator interfaces
        robot_hardware_interface::ModeHandlePtr modeHandle_;
        robot_hardware_interface::ModeStateHandlePtr modeStateHandle_;
        std::map<std::string, std::function<void(void)>> modeCommandMap;
        std::map<int, std::string> modeStateMap;
        std::vector<std::string> modeNames;
        int modeState_;

        robot_hardware_interface::ModeHandlePtr enableHandle_;
        robot_hardware_interface::ModeStateHandlePtr enableStateHandle_;
        std::map<std::string, std::function<void(void)>> enableCommandMap;
        std::map<int, std::string> enableStateMap;
        std::vector<std::string> enableNames;
        int enableState_;

        bool enabled_;
        bool powerGood_;
        bool initialized_;
    };

    typedef std::shared_ptr<BaseAthenaJointActuator> BaseAthenaJointActuatorPtr;

}

#endif //__BASE_ATHENA_JOINTACTUATORSTATES_HPP__