#ifndef __ROBOT_INTERFACE_HPP__
#define __ROBOT_INTERFACE_HPP__

#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/robot_hw.h>
#include <hardware_interface/imu_sensor_interface.h>
#include <hardware_interface/force_torque_sensor_interface.h>
#include <transmission_interface/transmission_interface.h>
#include <pluginlib/class_loader.h>
#include <nasa_common_logging/Logger.h>
#include "val_robot_interface/JointInterface.hpp"
#include "val_robot_interface/ActuatorInterface.hpp"
#include "val_robot_interface/ModeInterface.hpp"
#include "val_transmissions/loadable_transmission/Loader_Helper_Functions.hpp"
#include "val_robot_interface/Hardware_Interface_Exceptions.hpp"
#include "val_robot_interface/ImuSensorInterface.hpp"
#include "val_robot_interface/AthenaActuatorsInterface.hpp"
#include "val_robot_interface/AthenaJointsInterface.hpp"
#include "val_robot_interface/ForceTorqueSensorInterface.hpp"
#include "robot_variable/RobotVariableFactory.hpp"
#include "shared_memory_transport/smtclient.hpp"

namespace robot_hardware_interface
{

    typedef std::map<std::string, imu_sensor_interface::ImuSensorInterfacePtr> IMUSensors;
    typedef std::map<std::string, robot_hardware_interface::ForceTorqueSensorInterfacePtr> ForceTorqueSensors;
    typedef transmission_interface::TransmissionLoader::TransmissionPtr TransmissionPointer;

    //Create Mode Interface for registering modes at later time

    class RobotInterface : public hardware_interface::RobotHW
    {
    public:

		RobotInterface(SMT::SMTClient* smtClient, robot_variable::RobotVariableFactory* robotVariableFactory);
        virtual ~RobotInterface()
        {
            for(int i = 0;i<listOfActuatorInterfaces.size();i++)
            {
                listOfActuatorInterfaces[i].reset();
            }

            for(int i = 0;i<listOfAthenaActuatorBoardInterfaces.size();i++)
            {
                listOfAthenaActuatorBoardInterfaces[i].reset();
            }

            for(int i = 0;i<listOfAthenaJointInterfaces.size();i++)
            {
                listOfAthenaJointInterfaces[i].reset();
            }

            for(int i = 0;i<listOfTransmissions.size();i++)
            {
                listOfTransmissions[i].reset();
            }

            for(int i = 0;i<listOfJointInterfaces.size();i++)
            {
                listOfJointInterfaces[i].reset();
            }

            for(auto &keyValuePair: _imuSensors)
            {
                keyValuePair.second.reset();
            }

            for(auto &keyValuePair: forceTorqueSensors)
            {
                keyValuePair.second.reset();
            }
        }

        //virtual methods for read and write since they need to change based on interface (sim vs hardware)
        //the will be declared in respective clase definition
        virtual void read(const ros::Time& time) = 0;
        virtual void write(const ros::Time& time) = 0;

        //initialize data based on URDF
        bool initializeFromURDF(const std::string& urdfFile);
        bool initializeFromRosRobotDescription(const std::string& parameterName = "/robot_description", const ros::NodeHandle& = ros::NodeHandle());

    protected:
        SMT::SMTClient* client;
        robot_variable::RobotVariableFactory* variableFactory;

        virtual JointPtr createJoint(const std::string& name) = 0;
        bool addJointsAndActuators(const std::string &urdfFile);
        bool addSensors(const std::string &urdfFile);

        void registerModeInterfaces();

        // create a actuator loader to load the actuator type from the turbodriver interface (in turbodriver api)

        void addActuator(std::string const& name, std::string const& actuatorXmlInformation);
        void addJoint(std::string const& name, std::vector<std::string> HardwareInterfaces);
        void addTransmission(transmission_interface::TransmissionInfo transmissionToAddInfo);

        void registerTransmission(const std::string& transmissionName, TransmissionPointer transmission, std::vector<JointPtr> jointList, std::vector<ActuatorPtr> actuatorList);

        std::string getActuatorNodeName(const std::string& xmlElementData);
        std::string getTurboApiName(const std::string& xmlElementData);

        ActuatorPtr findActuatorByName(std::string actuatorname);

        JointPtr findJointByName(std::string jointname);

        /**
         * Pluginlib loaders. Note it is important for these to be declared BEFORE the instances they create,
         * because the loaders MUST get destructed AFTER the instance pointers.
         */
        robot_hardware_interface::ActuatorPluginLoader actuatorLoader;
        robot_hardware_interface::AthenaActuatorPluginLoader athenaActuatorLoader;
        robot_transmission_interface::TransmissionPluginLoader transmissionLoader;
        pluginlib::ClassLoader<imu_sensor_interface::ImuSensorInterface> imuSensorLoader;
        robot_hardware_interface::ForceTorqueSensorPluginLoader forceTorqueSensorLoader;

//        Containers and managers for joint and actuator instances
        std::vector<JointPtr> listOfJointInterfaces;
        std::vector<ActuatorPtr> listOfActuatorInterfaces;
        std::vector<TransmissionPointer> listOfTransmissions;
        std::vector<AthenaJointsPtr> listOfAthenaJointInterfaces;
        std::vector<AthenaActuatorsPtr> listOfAthenaActuatorBoardInterfaces;
        IMUSensors _imuSensors;
        ForceTorqueSensors forceTorqueSensors;

        robot_transmission_interface::UrdfRobotFileReader urdfRobotTransmissionReader;

        //declare mode interfaces
        JointModeInterface modeInterface;
        JointStateModeInterface modeStateInterface;

        //declare TransmissionInterfaces
        transmission_interface::ActuatorToJointStateInterface actuatorToJointStateInterface;
        transmission_interface::ActuatorToJointPositionInterface actuatorToJointPositionInterface;
        transmission_interface::ActuatorToJointVelocityInterface actuatorToJointVelocityInterface;
        transmission_interface::ActuatorToJointEffortInterface actuatorToJointEffortInterface;
        transmission_interface::JointToActuatorStateInterface jointToActuatorStateInterface;
        transmission_interface::JointToActuatorPositionInterface jointToActuatorPositionInterface;
        transmission_interface::JointToActuatorVelocityInterface jointToActuatorVelocityInterface;
        transmission_interface::JointToActuatorEffortInterface jointToActuatorEffortInterface;

        //Ros control hardware interfaces for joint controllers
        hardware_interface::PositionJointInterface joint_position_interface;
        hardware_interface::VelocityJointInterface joint_velocity_interface;
        hardware_interface::EffortJointInterface joint_effort_interface;
        hardware_interface::JointStateInterface joint_state_interface;

        /********************************************************/
        /********Athena Specific Things**************************/
        /********************************************************/

        void addAthenaTransmission(transmission_interface::TransmissionInfo transmissionToAddInfo);
        void registerAthenaTransmission(const std::string& transmissionName, TransmissionPointer transmission, std::vector<AthenaJointsPtr> jointList, std::vector<AthenaActuatorsPtr> actuatorList);

        AthenaJointsPtr findAthenaJointByName(std::string jointname);
        AthenaActuatorsPtr findAthenaActuatorByName(std::string actuatorname);

        void addAthenaActuator(std::string const& name, std::string const& actuatorXmlInformation);
        void addAthenaJoint(std::string const& name, std::vector<std::string> HardwareInterfaces);

        void registerAthenaModeInterfaces();

        /********************************************************/
        /******** Sensor Declarations ***************************/
        /********************************************************/
        // Unique ptr to IMUSensorInterface.
        hardware_interface::ImuSensorInterface _imuSensorInterface;

        // Called to add imu sensor.
        void addIMU(const std::string& name, const std::string& nodeName, const std::string& sensorType, const std::string& portName);
        // Update all imu sensors.

        /***********************************************/
        /******    Force_Torque Sensors     ************/
        /***********************************************/

        // Unique ptr to ForceTorqueSensorInterface.
        hardware_interface::ForceTorqueSensorInterface forcetorqueSensorInterface;

        // Called to add force-torque sensor.
        void addForceTorque(std::string const& name, std::string const& nodeName, std::string const& sensorType, std::string const& sensorNumber);

        bool runningOnRealRobot;

        std::map<std::string, std::string> jointFirstRole;
        std::map<std::string, std::string> actuatorFirstRole;
    };

}

#endif
