//! @file ModeInterface.hpp
#ifndef __MODE_INTERFACE_HPP__
#define __MODE_INTERFACE_HPP__

#include "val_robot_interface/ModeStateInterface.hpp"

namespace robot_hardware_interface
{
    //! @brief ModeHandle is a class which stores the required function hadle of the inteface modes.
    class ModeHandle : public ModeStateHandle
    {
        //! @brief ModeState is a stuct that contains the modes the actuator interfaces can take

    public:
        ModeHandle() : ModeStateHandle() {}
        ModeHandle(const ModeData& modeData) : ModeStateHandle(modeData) {}

        void sendCommand(std::string modeName)
        {
            try
            {
                modeMap_->at(modeName)();
            }
            catch (std::out_of_range e)
            {
                ROS_ERROR("Mode %s does not exist.", modeName.c_str());
                throw e;
            }
        }

    };

    typedef std::shared_ptr<ModeHandle> ModeHandlePtr;

    class JointModeInterface : public hardware_interface::HardwareResourceManager<robot_hardware_interface::ModeHandle, hardware_interface::ClaimResources> {};

}


#endif //_NASA_robot_HARDWARE_INTERFACE_MODE_INTERFACE_H