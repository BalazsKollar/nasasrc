//! @file ModeInterface.hpp
#ifndef __MODE_STATE_INTERFACE_HPP__
#define __MODE_STATE_INTERFACE_HPP__

#include <hardware_interface/internal/hardware_resource_manager.h>

namespace robot_hardware_interface
{
    struct ModeData
    {
        ModeData() : name(), data_(0), modeNames_(0), modeMap_(0), stateMap_(0) {}

        std::string name;
        int* data_;
        int modeNamesSize_;
        std::vector<std::string> modeNames_;
        std::map<std::string, std::function<void(void)>>* modeMap_;
        std::map<int, std::string>* stateMap_;
    };
    //! @brief ModeStateHandle is a class which stores the required function hadle of the inteface modes and only exposes get function to prevent modification.
    class ModeStateHandle
    {
        //! @brief ModeState is a stuct that contains the modes the actuator interfaces can take

    public:
        ModeStateHandle() : name_(), data_(0) {}
        ModeStateHandle(const ModeData& modeData) : name_(modeData.name), data_(modeData.data_), modeMap_(modeData.modeMap_), stateMap_(modeData.stateMap_), modeNames_(modeData.modeNames_)
        {}

        std::string              getName()         const { return name_; }
        std::vector<std::string> getCommandNames() const { return modeNames_; }

        int  getData() const
        {
            {
                if (data_ == NULL)
                {
                    return 0;
                }

                return *data_;
            }
        }

        std::string getState()
        {
            if (data_ != NULL)
            {
                if (stateMap_->find(*data_) != stateMap_->end())
                {
                    return stateMap_->at(*data_);
                }
            }

            return "undefined";
        }

    protected:
        std::string name_;

        int* data_;
        std::vector<std::string> modeNames_;
        std::map<std::string, std::function<void(void)>>* modeMap_;
        std::map<int, std::string>* stateMap_;
    };

    typedef std::shared_ptr<ModeStateHandle> ModeStateHandlePtr;

    class JointStateModeInterface : public hardware_interface::HardwareResourceManager<robot_hardware_interface::ModeStateHandle> {};
}


#endif //_NASA_robot_HARDWARE_INTERFACE_MODE_INTERFACE_H