#ifndef __JOINT_STATE_STRUCTS_HPP__
#define __JOINT_STATE_STRUCTS_HPP__

namespace robot_hardware_interface
{
    struct State
    {
        State() : position(0), velocity(0), effort(0) {}

        State(double* pos, double* vel, double* eff) :
            position(pos), velocity(vel), effort(eff) {}

        double* position;
        double* velocity;
        double* effort;
    };

    //! @brief Command is a structure to store commands associated with each interface
    struct Command
    {
        Command() : command(0) {}
        explicit Command(double* cmd) : command(cmd) {}
        double* command;
    };
}

#endif