#ifndef __HARDWARE_INTERFACE_EXCEPTIONS_HPP__
#define __HARDWARE_INTERFACE_EXCEPTIONS_HPP__

#include <exception>

class HardwareInterfaceException : public std::exception
{
public:
    HardwareInterfaceException(const std::string& message) : msg(message) {}
    virtual ~HardwareInterfaceException() throw() {}
    const char* what () const throw() {return msg.c_str();}

private:
    std::string msg;
};

#endif