//! @file joint_Interface.hpp
#ifndef __JOINT_INTERFACE_HPP__
#define __JOINT_INTERFACE_HPP__

#include <memory>
#include "val_robot_interface/BaseJointActuatorStates.hpp"

namespace robot_hardware_interface
{
    //! @brief JointInterface is a class which will have methods overlayed for interfacing with joints
    class JointInterface : public BaseJointActuator
    {
    public:
        virtual void initialize(const std::string& nodepath) {}
        virtual void updateStateData(ros::Time const& timeStamp) {}
        virtual void updateStateCommand(ros::Time const& timeStamp) {}
        virtual void updateModeData() {}

    };

    typedef std::shared_ptr<JointInterface> JointPtr;
}
#endif