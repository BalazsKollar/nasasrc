//! @file joint_Interface.hpp
#ifndef __ATHENA_JOINT_INTERFACE_HPP__
#define __ATHENA_JOINT_INTERFACE_HPP__

#include <memory>
#include "val_robot_interface/AthenaBaseActuators.hpp"

namespace robot_hardware_interface
{
    //! @brief JointInterface is a class which will have methods overlayed for interfacing with joints
    class AthenaJointsInterface : public BaseAthenaJointActuator
    {
    public:
        virtual void initialize(const std::string& nodepath) {}
        virtual void updateStateData(ros::Time const& timeStamp) {}
        virtual void updateStateCommand(ros::Time const& timeStamp) {}
        virtual void updateModeData() {}

    };

    typedef std::shared_ptr<AthenaJointsInterface> AthenaJointsPtr;
}
#endif