
/**
 * A base class for IMU's. You are required to implement a rotateToZupFrame method that rotates the data from
 * your specific IMU to a z-up frame. This is to make it easier for different types of IMU's to be used with
 * the same high level controller and/or state estimator without any modifications to the high level.
 */

#ifndef __IMU_SENSOR_INTERFACE_HPP__
#define __IMU_SENSOR_INTERFACE_HPP__

#include <hardware_interface/imu_sensor_interface.h>
#include "robot_variable/RobotVariableFactory.hpp"
#include <nasa_common_logging/Logger.h>
#include <shared_memory_transport/smtclient.hpp>
#include "val_robot_interface/Hardware_Interface_Exceptions.hpp"
#include <eigen3/Eigen/Eigen>

namespace imu_sensor_interface
{
    class ImuSensorInterface
    {
    public:
        ImuSensorInterface()
        {
        }

        virtual ~ImuSensorInterface()
        {
        }

        void initialize(const std::string& name, const std::string &nodePath, robot_variable::RobotVariableFactory* variableFactory, const std::string& devicePort, SMT::SMTClient* smtClient)
        {
            if(!variableFactory)
            {
                std::string msg = "RobotVariableFactory is nullptr!";
                NasaCommonLogging::Logger::log("gov.nasa.ImuSensorInterface.initialize", log4cpp::Priority::FATAL, msg);
                throw HardwareInterfaceException(msg);
            }

            if(!smtClient)
            {
                std::string msg = "SMTClient is nullptr!";
                NasaCommonLogging::Logger::log("gov.nasa.ImuSensorInterface.initialize", log4cpp::Priority::FATAL, msg);
                throw HardwareInterfaceException(msg);
            }

            this->robotVariableFactory = variableFactory;
            this->smtClient = smtClient;

            setImuFrameToZUpFrameQuaternion();

            createRobotVariables(name,nodePath);

            initialize(name,nodePath,devicePort);
        }

        virtual void getData() = 0;
        virtual void setImuFrameToZUpFrameQuaternion() = 0;
        virtual void stop() = 0;
        virtual void disconnect() = 0;

        virtual void update()
        {
            getData();

            zUpQuatOrientation = rotationFromImuFrameToZUpFrame*quatOrientation;
            setRobotVariables();
        }

        double *getLinearAcceleration()
        {
            return linearAcceleration.data();
        }

        double *getAngularVelocity()
        {
            return angularVelocity.data();
        }

        double *getQuaternion()
        {
            return zUpQuatOrientation.coeffs().data();
        }

        virtual void makeHandle(const std::string name)
        {
            data.angular_velocity = angularVelocity.data();
            data.linear_acceleration = linearAcceleration.data();
            data.orientation = zUpQuatOrientation.coeffs().data();
            data.orientation_covariance = orientationCovariance.coeffs().data();
            data.angular_velocity_covariance = angularVelocityCovariance.data();
            data.linear_acceleration_covariance = linearAccelerationCovariance.data();
            data.name = name;
            handle.reset(new hardware_interface::ImuSensorHandle(data));
        }

        virtual hardware_interface::ImuSensorHandle* getHandle()
        {
            return handle.get();
        }

    protected:


        hardware_interface::ImuSensorHandle::Data data;
        std::shared_ptr<hardware_interface::ImuSensorHandle> handle;
        robot_variable::RobotVariableFactory* robotVariableFactory;
        SMT::SMTClient* smtClient;

        Eigen::Quaterniond quatOrientation,zUpQuatOrientation,rotationFromImuFrameToZUpFrame,orientationCovariance;
        Eigen::Vector3d angularVelocity,linearAcceleration,angularVelocityCovariance,linearAccelerationCovariance;

        std::shared_ptr<robot_variable::RobotVariable<float>> rawOrientationW,rawOrientationX,rawOrientationY,rawOrientationZ;
        std::shared_ptr<robot_variable::RobotVariable<float>> zUpOrientationW,zUpOrientationX,zUpOrientationY,zUpOrientationZ;
        std::shared_ptr<robot_variable::RobotVariable<float>> angularVelocityX,angularVelocityY,angularVelocityZ;
        std::shared_ptr<robot_variable::RobotVariable<float>> linearAccelerationX,linearAccelerationY,linearAccelerationZ;

    private:
        virtual void initialize(const std::string& name, const std::string &nodePath,const std::string& devicePort) = 0;

        void createRobotVariables(const std::string &name, const std::string &nodePath)
        {
            try
            {
                rawOrientationW = robotVariableFactory->makeVariable<float>(nodePath + "/QuaternionW");
                rawOrientationX = robotVariableFactory->makeVariable<float>(nodePath + "/QuaternionX");
                rawOrientationY = robotVariableFactory->makeVariable<float>(nodePath + "/QuaternionY");
                rawOrientationZ = robotVariableFactory->makeVariable<float>(nodePath + "/QuaternionZ");

                zUpOrientationW = robotVariableFactory->makeVariable<float>(nodePath + "/QuaternionZUpFrameW");
                zUpOrientationX = robotVariableFactory->makeVariable<float>(nodePath + "/QuaternionZUpFrameX");
                zUpOrientationY = robotVariableFactory->makeVariable<float>(nodePath + "/QuaternionZUpFrameY");
                zUpOrientationZ = robotVariableFactory->makeVariable<float>(nodePath + "/QuaternionZUpFrameZ");

                angularVelocityX = robotVariableFactory->makeVariable<float>(nodePath + "/AngularVelocityX");
                angularVelocityY = robotVariableFactory->makeVariable<float>(nodePath + "/AngularVelocityY");
                angularVelocityZ = robotVariableFactory->makeVariable<float>(nodePath + "/AngularVelocityZ");

                linearAccelerationX = robotVariableFactory->makeVariable<float>(nodePath + "/LinearAccelerationX");
                linearAccelerationY = robotVariableFactory->makeVariable<float>(nodePath + "/LinearAccelerationY");
                linearAccelerationZ = robotVariableFactory->makeVariable<float>(nodePath + "/LinearAccelerationZ");
            }
            catch(SMT::SMTException &e)
            {
                std::string msg = "Exception encountered while creating shared memory variables for imu " + name;
                NasaCommonLogging::Logger::log("gov.nasa.ImuSensorInterface.createRobotVariables", log4cpp::Priority::FATAL, msg);
                throw SMT::SMTException(e.what());
            }
        }

        void setRobotVariables()
        {
            rawOrientationW->set((float)quatOrientation.w());
            rawOrientationX->set((float)quatOrientation.x());
            rawOrientationY->set((float)quatOrientation.y());
            rawOrientationZ->set((float)quatOrientation.z());

            zUpOrientationW->set((float)zUpQuatOrientation.w());
            zUpOrientationX->set((float)zUpQuatOrientation.x());
            zUpOrientationY->set((float)zUpQuatOrientation.y());
            zUpOrientationZ->set((float)zUpQuatOrientation.z());

            angularVelocityX->set((float)angularVelocity.x());
            angularVelocityY->set((float)angularVelocity.y());
            angularVelocityZ->set((float)angularVelocity.z());

            linearAccelerationX->set((float)linearAcceleration.x());
            linearAccelerationY->set((float)linearAcceleration.y());
            linearAccelerationZ->set((float)linearAcceleration.z());
        }
    };

    typedef boost::shared_ptr<ImuSensorInterface> ImuSensorInterfacePtr;
}
#endif
