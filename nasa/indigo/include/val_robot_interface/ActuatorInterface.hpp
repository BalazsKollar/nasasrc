//! @file Actuator_Interface.hpp
#ifndef __ACTUATOR_INTERFACE_HPP__
#define __ACTUATOR_INTERFACE_HPP__

#include <memory>
#include <pluginlib/class_loader.h>
#include <shared_memory_transport/smtclient.hpp>
#include "val_robot_interface/BaseJointActuatorStates.hpp"
#include "nasa_common_logging/Logger.h"

namespace robot_hardware_interface
{
    //! @brief ActuatorInterfce is a class which will have methods overlayed which interface with actuators
    class ActuatorInterface : public BaseJointActuator
    {
    public:
        virtual void initialize(const std::string& nodepath, SMT::SMTClient* smtClient) {}
        virtual void updateStateData(ros::Time const& timeStamp) {}
        virtual void updateStateCommand(ros::Time const& timeStamp) {}
        virtual void updateModeData() {}

        // Used for the sim interface only
        virtual void setStateData(ros::Time const& timeStamp) {}
        virtual void getStateCommand(ros::Time const& timeStamp) {}

    };

    typedef boost::shared_ptr<ActuatorInterface> ActuatorPtr;

    struct ActuatorPluginLoader
    {
        ActuatorPluginLoader()
            : class_loader_("val_robot_interface", "robot_hardware_interface::ActuatorInterface")
        {
        }

        ActuatorPtr create(const std::string& type)
        {

            try
            {
                return class_loader_.createInstance(type);
            }
            catch (...)
            {
                std::string msg = "Failed to load actuator instance of type: " + type;
                NasaCommonLogging::Logger::log("gov.nasa.ActuatorPluginLoader.create", log4cpp::Priority::ERROR, msg);
                ROS_ERROR("%s", msg.c_str());
                return ActuatorPtr();
            }
        }

    private:
        //must keep it alive because instance destroyers need it
        pluginlib::ClassLoader<robot_hardware_interface::ActuatorInterface>  class_loader_;
    };
}
#endif