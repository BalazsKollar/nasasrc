//! @file BaseJointActuatorStates.hpp
#ifndef __BASE_JOINTACTUATORSTATES_HPP__
#define __BASE_JOINTACTUATORSTATES_HPP__

#include <memory>
#include <functional>

#include <transmission_interface/transmission.h>
#include <val_robot_interface/ModeInterface.hpp>
#include "val_robot_interface/ModeStates.hpp"
#include "val_robot_interface/JointStateStructs.hpp"

namespace robot_hardware_interface
{
    //! @brief BaseJointActuator is a base class for the joint and actuator interfaces
    class BaseJointActuator
    {
    public:
        BaseJointActuator() :
            stateData(&position_, &velocity_, &effort_),
            positionCommand(&positionCommand_),
            velocityCommand(&velocityCommand_),
            effortCommand(&effortCommand_),
            position_(0.0),
            velocity_(0.0),
            effort_(0.0),
            effortCommand_(0.0),
            velocityCommand_(0.0),
            positionCommand_(0.0),
            modeState_(ModeState::Undefined),
            enableState_(EnableState::Undefined),
            powerGood_(true),
            initialized_(false),
            enabled_(false) {}

        virtual ~BaseJointActuator() {}

        //virtual void updateEnableState() {}

        State const& getData() const                        {return stateData;}
        Command const& getPositionCommand() const           {return positionCommand;}
        Command const& getVelocityCommand() const           {return velocityCommand;}
        Command const& getEffortCommand() const             {return effortCommand;}

        /** Return the attribute by name
          *
          */
        State getAttribute(std::string const& name)
        {
            if (name == "stateData") { return getData(); }

            if (name == "positionCommand") { return State(&positionCommand_, &velocity_, &effort_); }

            if (name == "velocityCommand") { return State(&position_, &velocityCommand_, &effort_); }

            if (name == "effortCommand") { return State(&position_, &velocity_, &effortCommand_); }

            std::stringstream sstr; sstr << "Attribute '" << name << "' not recognized.";
            throw hardware_interface::HardwareInterfaceException(sstr.str());
        }

        ModeHandle* getModeHandle()
        {
            robot_hardware_interface::ModeData modeData;

            modeData.name = name_+"_mode";
            modeData.data_ = &modeState_;
            modeData.modeNames_ = modeNames;
            modeData.modeMap_ = &modeCommandMap;
            modeData.stateMap_ = &modeStateMap;

            modeHandle_.reset(new robot_hardware_interface::ModeHandle(modeData));

            return modeHandle_.get();
        }

        ModeStateHandle* getModeStateHandle()
        {
            robot_hardware_interface::ModeData modeData;

            modeData.name = name_+"_mode";
            modeData.data_ = &modeState_;
            modeData.modeNames_ = modeNames;
            modeData.modeMap_ = &modeCommandMap;
            modeData.stateMap_ = &modeStateMap;

            modeStateHandle_.reset(new robot_hardware_interface::ModeStateHandle(modeData));

            return modeStateHandle_.get();
        }

        ModeHandle* getEnableHandle()
        {
            robot_hardware_interface::ModeData modeData;

            modeData.name = name_+"_enable";
            modeData.data_ = &enableState_;
            modeData.modeNames_ = enableNames;
            modeData.modeMap_ = &enableCommandMap;
            modeData.stateMap_ = &enableStateMap;

            enableHandle_.reset(new robot_hardware_interface::ModeHandle(modeData));
        }

        ModeStateHandle* getEnableStateHandle()
        {
            robot_hardware_interface::ModeData modeData;

            modeData.name = name_+"_enable";
            modeData.data_ = &enableState_;
            modeData.modeNames_ = enableNames;
            modeData.modeMap_ = &enableCommandMap;
            modeData.stateMap_ = &enableStateMap;

            enableStateHandle_.reset(new robot_hardware_interface::ModeStateHandle(modeData));

            return enableStateHandle_.get();
        }

        std::string name_;
        std::string node_;
        std::string api_tag_;

        void enable() { enabled_ = true; }
        void disable() { enabled_ = false; }

    protected:

        //state and command data declaration for the interface
        robot_hardware_interface::State stateData;
        robot_hardware_interface::Command positionCommand;
        robot_hardware_interface::Command velocityCommand;
        robot_hardware_interface::Command effortCommand;

        //individual elements for interface class allowing the state and command data to be
        //pulled out
        double position_;
        double velocity_;
        double effort_;

        double effortCommand_;
        double velocityCommand_;
        double positionCommand_;

        //declaration of mode for the joint actuator interfaces
        robot_hardware_interface::ModeHandlePtr modeHandle_;
        robot_hardware_interface::ModeStateHandlePtr modeStateHandle_;
        int modeState_;
        std::map<std::string, std::function<void(void)>> modeCommandMap;
        std::map<int, std::string> modeStateMap;
        std::vector<std::string> modeNames;

        robot_hardware_interface::ModeHandlePtr enableHandle_;
        robot_hardware_interface::ModeStateHandlePtr enableStateHandle_;
        int enableState_;
        std::map<std::string, std::function<void(void)>> enableCommandMap;
        std::map<int, std::string> enableStateMap;
        std::vector<std::string> enableNames;

        bool enabled_;
        bool powerGood_;
        bool initialized_;
    };

    typedef std::shared_ptr<BaseJointActuator> BaseJointActuatorPtr;

}

#endif //_HARDWARE_INTERFACE_BASE_JOINTACTUATORSTATES_HPP_