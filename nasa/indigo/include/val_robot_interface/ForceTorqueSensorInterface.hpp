#ifndef _ATI_SENSOR_BASE_HPP__
#define _ATI_SENSOR_BASE_HPP__

#include <hardware_interface/force_torque_sensor_interface.h>
#include "robot_variable/RobotVariableFactory.hpp"
#include <pluginlib/class_loader.h>
#include <shared_memory_transport/smtclient.hpp>
#include <val_robot_interface/ModeInterface.hpp>
#include "val_robot_interface/ModeStates.hpp"
#include "nasa_common_logging/Logger.h"

namespace robot_hardware_interface
{
    class ForceTorqueSensorInterface
    {
    public:
        ForceTorqueSensorInterface() : modeState_(robot_hardware_interface::ForceTorqueSensorModeState::UnTare)
        {
            force.resize(3);
            torque.resize(3);
            strainGaugeValues.resize(6);

            forceDynOffset.resize(3, 0.0);
            torqueDynOffset.resize(3, 0.0);
            forceOffset.resize(3, 0.0);
            torqueOffset.resize(3, 0.0);

            circularBufferSize = 300;
            currentBufferLoc = 0;
            std::vector<double> tmp (circularBufferSize, 0.0);

            for (unsigned int i = 0; i < 3; i++)
            {
                fOffset.push_back(tmp);
                tOffset.push_back(tmp);
            }
        }

        ~ForceTorqueSensorInterface() {}

        robot_hardware_interface::ModeHandle* getModeHandle()
        {
            robot_hardware_interface::ModeData modeData;

            modeData.name = name_;
            modeData.data_ = &modeState_;
            modeData.modeNames_ = modeNames;
            modeData.modeMap_ = &modeCommandMap;
            modeData.stateMap_ = &modeStateMap;

            modeHandle_.reset(new robot_hardware_interface::ModeHandle(modeData));

            return modeHandle_.get();
        }

        robot_hardware_interface::ModeStateHandle* getModeStateHandle()
        {
            robot_hardware_interface::ModeData modeData;

            modeData.name = name_;
            modeData.data_ = &modeState_;
            modeData.modeNames_ = modeNames;
            modeData.modeMap_ = &modeCommandMap;
            modeData.stateMap_ = &modeStateMap;

            modeStateHandle_.reset(new robot_hardware_interface::ModeStateHandle(modeData));

            return modeStateHandle_.get();
        }

        virtual void makeHandle(const std::string &name, const std::string& configFilePath) = 0;
        virtual void update() = 0;

        virtual void initialize(const std::string& nodePath, SMT::SMTClient* smtClient, robot_variable::RobotVariableFactory* variableFactory) = 0;

        virtual hardware_interface::ForceTorqueSensorHandle* getSensorHandle() = 0;

        virtual hardware_interface::ForceTorqueSensorHandle* getOffsetHandle() = 0;

        std::string name_;
    protected:
        std::vector<double> force;
        std::vector<double> torque;

        std::vector<double> forceDynOffset;
        std::vector<double> torqueDynOffset;
        std::vector<double> forceOffset;
        std::vector<double> torqueOffset;

        std::vector<std::vector<double>> fOffset;
        std::vector<std::vector<double>> tOffset;

        unsigned int currentBufferLoc;

        unsigned int circularBufferSize;

        robot_hardware_interface::ModeHandlePtr modeHandle_;
        robot_hardware_interface::ModeStateHandlePtr modeStateHandle_;
        std::map<std::string, std::function<void(void)>> modeCommandMap;
        std::map<int, std::string> modeStateMap;
        std::vector<std::string> modeNames;
        int modeState_;

        std::vector<double> strainGaugeValues;

        std::shared_ptr<hardware_interface::ForceTorqueSensorHandle> handleSensor;
        std::shared_ptr<hardware_interface::ForceTorqueSensorHandle> handleOffset;

        std::shared_ptr<robot_variable::RobotVariable<double>> forceX,forceY,forceZ,torqueX,torqueY,torqueZ;
    };

    typedef boost::shared_ptr<ForceTorqueSensorInterface> ForceTorqueSensorInterfacePtr;

    struct ForceTorqueSensorPluginLoader
    {
        /**
         * @Todo Need to remove the hard coded package name "val_robot_interface" to truly make this agnostic.
         */
        ForceTorqueSensorPluginLoader()
            : class_loader_("val_robot_interface", "robot_hardware_interface::ForceTorqueSensorInterface")
        {
        }

        ForceTorqueSensorInterfacePtr create(const std::string& type)
        {
            try
            {
                return class_loader_.createInstance(type);
            }
            catch (pluginlib::LibraryLoadException &e)
            {
                std::string msg = "Exception encountered loading force torque sensor plugin of type " + type + ": " + e.what();
                NasaCommonLogging::Logger::log("gov.nasa.ForceTorqueSensorPluginLoader.create", log4cpp::Priority::ERROR, msg);
                return ForceTorqueSensorInterfacePtr();
            }
            catch(pluginlib::CreateClassException &e)
            {
                std::string msg = "Exception encountered creating the clas for force torque sensor plugin " + type + ": " + e.what();
                NasaCommonLogging::Logger::log("gov.nasa.ForceTorqueSensorPluginLoader.create", log4cpp::Priority::ERROR, msg);
                return ForceTorqueSensorInterfacePtr();
            }
        }

    private:
        //must keep it alive because instance destroyers need it
        pluginlib::ClassLoader<robot_hardware_interface::ForceTorqueSensorInterface>  class_loader_;
    };
}

#endif
