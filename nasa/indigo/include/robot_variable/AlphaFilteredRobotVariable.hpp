/**
 * The AlphaFilteredRobotVariable creates a filtered SMT variable from either an already existing SmtVariable,
 * or just a standard double/float/int etc. If you are filtering an existing SmtVariable, each tic you simply
 * call the update() method. If you are filtering a primitive type, you wall update(T variable) where T is the
 * template type. The boolean hasUpdateBeenCalled takes care of making sure the alpha filtered variable is
 * initialized before any filtering begins. Upon creation, an SmtVariable will be created for the alpha
 * parameter as well as the alpha filtered variable. The filter formula is the following:
 *
 * y[i] = alpha * x[i] + (1-alpha) * y[i-1]
 *
 * where x[i] is the signal you are filtering, y[i] is the current filtered value, and y[i-1] is the
 * previous filtered value. This means that alpha=1 means there is no filtering, and alpha=0 means
 * the entire signal is filtered out.
 *
 */

#ifndef __ALPHA_FILTERED_SMTVARIABLE_HPP__
#define __ALPHA_FILTERED_SMTVARIABLE_HPP__

#include "robot_variable/RobotVariable.hpp"
#include "robot_variable/ProcessingRobotVariable.hpp"
#include "robot_variable/RobotVariableFactory.hpp"
#include "control_utils/MathTools.hpp"
#include "control_utils/FilterInterfaces.hpp"

namespace robot_variable
{
    template<typename T>
    class AlphaFilteredRobotVariable : public robot_variable::ProcessingRobotVariable, public control_utils::AlphaFilterInterface<T>
    {
    public:

        /**
         * Constructor that handles the creation of an alpha filtered SmtVariable
         * @param smtClient raw pointer to an SMTClient
         * @param SmtVariable A pointer to the SmtVariable<double> that you want to create a filtered version of
         */
        AlphaFilteredRobotVariable(robot_variable::RobotVariableFactory* variableFactory, robot_variable::RobotVariable<T>* robotVariable, const T alpha = 1) : robotVariableToFilter(robotVariable),control_utils::AlphaFilterInterface<T>()
        {
            this->alpha = variableFactory->makeVariable<T>(robotVariable->getName()+"_filterAlpha");
            this->filteredVariable = variableFactory->makeVariable<T>(robotVariable->getName() + "_filtered");
            setAlpha(alpha);
        }

        virtual ~AlphaFilteredRobotVariable() {};

        void update()
        {
            setFilteredValue(control_utils::MathTools::computeAlphaFilterOutput(filteredVariable->get(),robotVariableToFilter->get(),alpha->get()));
        }

        T getSignalToFilterCurrentValue() const
        {
            return robotVariableToFilter->get();
        }

        void setFilteredValue(const T value)
        {
            filteredVariable->set(value);
        }

        T getFilteredValue() const
        {
            return filteredVariable->get();
        }

        /**
         * Reset the filter.
         */
        void reset()
        {
            setFilteredValue(robotVariableToFilter->get());
        }

        /**
         * Set the filter alpha to a desired value.
         *
         * @param alpha The desired filter coefficient
         */
        void setAlpha(const T alpha)
        {
            this->alpha->set(alpha);
        }

        /**
         * Get the filter alpha
         *
         * @return The value of alpha
         */
        T getAlpha() const
        {
            return alpha->get();
        }

    protected:
        std::shared_ptr<robot_variable::RobotVariable<T>> alpha,filteredVariable;
        const robot_variable::RobotVariable<T>* robotVariableToFilter;
    };

}

#endif
