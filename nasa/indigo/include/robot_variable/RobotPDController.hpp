
/**
 * RobotPDController provides a PD controller that creates shared memory variables
 * for the tunable controller parameters.
 */

#ifndef __SMT_PD_CONTROLLER_HPP__
#define __SMT_PD_CONTROLLER_HPP__

#include "robot_variable/RobotVariable.hpp"
#include "robot_variable/RobotVariableFactory.hpp"
#include "control_utils/PIDControllerInterfaces.hpp"

namespace robot_variable
{
    template<typename T>
    class RobotPDController : public control_utils::ProportionalControllerInterface<T>, public control_utils::DerivativeControllerInterface<T>
    {
    public:

        /**
         * Constructor
         *
         * @param variableFactory A pointer to a a RobotVariableFactory.
         * @param name The name of the smt controller. Shared memory variables created within will be prependend with this name
         * @param kp The proportional gain
         * @param kd The derivative gain
         */
        RobotPDController(robot_variable::RobotVariableFactory* variableFactory,const std::string &name, const T kp, const T kd) : control_utils::ProportionalControllerInterface<T>(), control_utils::DerivativeControllerInterface<T>()
        {
            this->kp = variableFactory->makeVariable<T>(name + "kp");
            this->kd = variableFactory->makeVariable<T>(name + "kd");
            this->kp->set(kp);
            this->kd->set(kd);
        }

        virtual ~RobotPDController(){};

        /**
         * Set the controllers proportional gain
         *
         * @param kp The new value of the proportional gain
         */
        void setProportionalGain(const T kp)
        {
            this->kp->set(kp);
        }

        /**
         * Set the controllers derivative gain
         *
         * @param kd The new value of the derivative gain
         */
        void setDerivativeGain(const T kd)
        {
            this->kd->set(kd);
        }

        /**
         * Get the current proportional gain
         *
         * @return Proportional gain
         */
        T getProportionalGain() const
        {
            return kp->get();
        }

        /**
         * Get the derivative gain
         *
         * @return Derivative gain
         */
        T getDerivativeGain() const
        {
            return kd->get();
        }

        /**
         * Computes the controllers output
         *
         * @param desiredPosition The desired position
         * @param currentPosition The current position
         * @param desiredVelocity The desired velocity
         * @param currentVelocity The current velocity
         */
        T compute(const T desiredPosition, const T currentPosition, const T desiredVelocity, const T currentVelocity)
        {
            this->proportionalControllerOutput = this->computeProportionalControllerOutput(desiredPosition,currentPosition);
            this->derivativeControllerOutput = this->computeDerivativeControllerOutput(desiredVelocity,currentVelocity);

            return (this->proportionalControllerOutput + this->derivativeControllerOutput);
        }

        /**
         * Computes the controllers output and intelligently handles the case where angles do not wrap.
         *
         * @param desiredPosition The desired position
         * @param currentPosition The current position
         * @param desiredVelocity The desired velocity
         * @param currentVelocity The current velocity
         */
        T computeForAngles(const T desiredPosition, const T currentPosition, const T desiredVelocity, const T currentVelocity)
        {
            this->proportionalControllerOutput = this->computeProportionalControllerOutputForAngles(desiredPosition,currentPosition);
            this->derivativeControllerOutput = this->computeDerivativeControllerOutput(desiredVelocity,currentVelocity);

            return (this->proportionalControllerOutput + this->derivativeControllerOutput);
        }

    protected:

        std::shared_ptr<robot_variable::RobotVariable<T>> kp;
        std::shared_ptr<robot_variable::RobotVariable<T>> kd;

    };

}

#endif //PROJECT_RobotPDController_HPP
