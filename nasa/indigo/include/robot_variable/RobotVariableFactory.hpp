/**
 * Robot variable factory is a centralized object for storing and
 * creating robot variables.
 */

#ifndef __ROBOT_VARIABLE_FACTORY_HPP__
#define __ROBOT_VARIABLE_FACTORY_HPP__

#include "robot_variable/RobotVariable.hpp"
#include "robot_variable/RobotVariableDoubleProvider.hpp"
#include "robot_variable/RobotVariableExceptions.hpp"
#include <map>
#include <memory>

namespace robot_variable
{
    class RobotVariableFactory
    {
    public:
        RobotVariableFactory()
        {

        }

        virtual ~RobotVariableFactory()
        {

        }

        /**
         * @brief Register a robot variable to this factory
         * @param variableToRegister Pointer to variable to add
         */
        template<typename T>
        void registerVariable(RobotVariable<T>* variableToRegister)
        {
            if(robotVariableDataProviderMap.find(variableToRegister->getName())!=robotVariableDataProviderMap.end())
            {
                throw RobotVariableAlreadyExistsException();
            }

            this->robotVariableDataProviderMap[variableToRegister->getName()] = variableToRegister;
        }

        /**
         * @brief Create a robot variable of type double
         * @param name Name of the variable to create
         * @return shared pointer to created variable
         */
        virtual std::shared_ptr<RobotVariable<double>> makeDoubleVariable(const std::string name)
        {
            std::shared_ptr<RobotVariable<double>> var(new RobotVariable<double>(name));
            return var;
        }

        /**
         * @brief Create a robot variable of type float
         * @param name Name of the variable to create
         * @return shared pointer to created variable
         */
        virtual std::shared_ptr<RobotVariable<float>> makeFloatVariable(const std::string name)
        {
            std::shared_ptr<RobotVariable<float>> var(new RobotVariable<float>(name));
            return var;
        }

        /**
         * @brief Create a robot variable of type int
         * @param name Name of the variable to create
         * @return shared pointer to created variable
         */
        virtual std::shared_ptr<RobotVariable<int>> makeInt32Variable(const std::string name)
        {
            std::shared_ptr<RobotVariable<int>> var(new RobotVariable<int>(name));
            return var;
        }

        /**
         * @brief Create a robot variable of type uint32_t
         * @param name Name of the variable to create
         * @return shared pointer to created variable
         */
        virtual std::shared_ptr<RobotVariable<uint32_t>> makeUnsignedInt32Variable(const std::string name)
        {
            std::shared_ptr<RobotVariable<uint32_t>> var(new RobotVariable<uint32_t>(name));
            return var;
        }

        /**
         * @brief Create a robot variable of type int16_t
         * @param name Name of the variable to create
         * @return shared pointer to created variable
         */
        virtual std::shared_ptr<RobotVariable<int16_t>> makeInt16Variable(const std::string name)
        {
            std::shared_ptr<RobotVariable<int16_t>> var(new RobotVariable<int16_t>(name));
            return var;
        }

        /**
         * @brief Create a robot variable of type uint16_t
         * @param name Name of the variable to create
         * @return shared pointer to created variable
         */
        virtual std::shared_ptr<RobotVariable<uint16_t>> makeUnsignedInt16Variable(const std::string name)
        {
            std::shared_ptr<RobotVariable<uint16_t>> var(new RobotVariable<uint16_t>(name));
            return var;
        }

        /**
         * @brief Templated function to create robot variables. Template specialization makes
         * this capable of creating multiple types of variables.
         * @param name Name of the variable to create
         * @return shared pointer to created variable
         */
        template<typename T>
        inline std::shared_ptr<RobotVariable<T>> makeVariable(const std::string name)
        {
            throw std::runtime_error("Invalid template type for RobotVariable!");
        }

    protected:
        std::map<std::string, RobotVariableDoubleProvider*> robotVariableDataProviderMap;
    };

    template<>
    inline std::shared_ptr<RobotVariable<double>> RobotVariableFactory::makeVariable(const std::string name)
    {
        if(this->robotVariableDataProviderMap.find(name)!=this->robotVariableDataProviderMap.end())
        {
            throw RobotVariableAlreadyExistsException();
        }

        std::shared_ptr<RobotVariable<double>> var = makeDoubleVariable(name);
        this->robotVariableDataProviderMap[name] = var.get();
        return var;
    }

    template<>
    inline std::shared_ptr<RobotVariable<float>> RobotVariableFactory::makeVariable(const std::string name)
    {
        if(this->robotVariableDataProviderMap.find(name)!=this->robotVariableDataProviderMap.end())
        {
            throw RobotVariableAlreadyExistsException();
        }

        std::shared_ptr<RobotVariable<float>> var = makeFloatVariable(name);
        this->robotVariableDataProviderMap[name] = var.get();
        return var;
    }

    template<>
    inline std::shared_ptr<RobotVariable<int>> RobotVariableFactory::makeVariable(const std::string name)
    {
        if(this->robotVariableDataProviderMap.find(name)!=this->robotVariableDataProviderMap.end())
        {
            throw RobotVariableAlreadyExistsException();
        }

        std::shared_ptr<RobotVariable<int>> var = makeInt32Variable(name);
        this->robotVariableDataProviderMap[name] = var.get();
        return var;
    }

    template<>
    inline std::shared_ptr<RobotVariable<int16_t>> RobotVariableFactory::makeVariable(const std::string name)
    {
        if(this->robotVariableDataProviderMap.find(name)!=this->robotVariableDataProviderMap.end())
        {
            throw RobotVariableAlreadyExistsException();
        }

        std::shared_ptr<RobotVariable<int16_t>> var = makeInt16Variable(name);
        this->robotVariableDataProviderMap[name] = var.get();
        return var;
    }

    template<>
    inline std::shared_ptr<RobotVariable<uint16_t>> RobotVariableFactory::makeVariable(const std::string name)
    {
        if(this->robotVariableDataProviderMap.find(name)!=this->robotVariableDataProviderMap.end())
        {
            throw RobotVariableAlreadyExistsException();
        }

        std::shared_ptr<RobotVariable<uint16_t>> var = makeUnsignedInt16Variable(name);
        this->robotVariableDataProviderMap[name] = var.get();
        return var;
    }

    template<>
    inline std::shared_ptr<RobotVariable<uint32_t>> RobotVariableFactory::makeVariable(const std::string name)
    {
        if(this->robotVariableDataProviderMap.find(name)!=this->robotVariableDataProviderMap.end())
        {
            throw RobotVariableAlreadyExistsException();
        }

        std::shared_ptr<RobotVariable<uint32_t>> var = makeUnsignedInt32Variable(name);
        this->robotVariableDataProviderMap[name] = var.get();
        return var;
    }
}
#endif //__ROBOT_VARIABLE_FACTORY_HPP__
