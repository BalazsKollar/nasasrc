//
// Created by jordan on 5/10/16.
//

#ifndef __TYPENAME_UTILS_HPP__
#define __TYPENAME_UTILS_HPP__

#include <string>
#include <stdexcept>
namespace robot_variable
{
    template<typename T>
    struct TypeName
    {
        static const std::string getTemplateType()
        {
            throw std::runtime_error("Invalid template type for shared memory variable!");
        }
    };

    template<>
    struct TypeName<int>
    {
        static const std::string getTemplateType()
        {
            return "int32";
        }
    };

    template<>
    struct TypeName<float>
    {
        static const std::string getTemplateType()
        {
            return "float32";
        }
    };

    template<>
    struct TypeName<double>
    {
        static const std::string getTemplateType()
        {
            return "float64";
        }
    };

    template<>
    struct TypeName<int16_t>
    {
        static const std::string getTemplateType()
        {
            return "int16";
        }
    };

    template<>
    struct TypeName<uint16_t>
    {
        static const std::string getTemplateType()
        {
            return "uint16";
        }
    };

    template<>
    struct TypeName<uint32_t>
    {
        static const std::string getTemplateType()
        {
            return "uint32";
        }
    };

}
#endif //__TYPENAME_UTILS_HPP__
