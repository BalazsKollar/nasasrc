//
// Created by jordan on 8/15/16.
//

#ifndef __ROBOT_VARIABLE_EXCEPTIONS_HPP__
#define __ROBOT_VARIABLE_EXCEPTIONS_HPP__

#include <stdexcept>

namespace robot_variable
{
    class RobotVariableAlreadyExistsException : public std::exception
    {
    public:

        const char *what() const throw()
        {
            return "Can't create multiple robot variables with the same name!";
        }
    };

    class RobotVariableFactoryNullException : public std::exception
    {
    public:

        const char *what() const throw()
        {
            return "Robot variable factory is null!";
        }
    };
}
#endif //__ROBOT_VARIABLE_EXCEPTIONS_HPP__
