//
// Created by jordan on 8/15/16.
//

#ifndef __ROBOT_VARIABLE_BASE_HPP
#define __ROBOT_VARIABLE_BASE_HPP

class RobotVariableDoubleProvider
{
    virtual double getValueAsDouble() const = 0;
};

#endif //__ROBOT_VARIABLE_BASE_HPP
