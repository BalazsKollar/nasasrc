/**
 * The RobotVariable class is a special, templated class that
 * stores the variable name and the variable value as well as a
 * vector of callback function pointers to be called when the
 * variable changes(i.e. when the `set` method is called).
 *
 * All RobotVariables are RobotVariableDoubleProviders because,
 * since RobotVariables are templated, RobotVariables can't be stored
 * in a vector, however you can store RobotVariableDoubleProvider's in
 * a vector and thus can get the double value of any RobotVariable.
 * This is necessary for logging.
 */

#ifndef __ROBOT_VARIABLE_HPP__
#define __ROBOT_VARIABLE_HPP__

#include <string>
#include "robot_variable/RobotVariableDoubleProvider.hpp"
#include <functional>
#include <vector>
#include <cmath>

namespace robot_variable
{
    /**
     * Typedef for variable changed callbacks. Variable changed callbacks are
     * pointers to functions that return nothing and take no arguments.
     */
    typedef std::function<void(void)> variableChangedCallback;

    template<typename T>
    class RobotVariable : public RobotVariableDoubleProvider
    {
    public:
        RobotVariable(const std::string &name)
        {
            this->name = name;
            this->set(0);
        }

        template<typename TYPE>
        RobotVariable(const TYPE val, const std::string &name)
        {
            this->name = name;
            this->set(val);
        }

        /**
         * @brief Add a variable changed callback to this robot variable
         * @param callback A function pointer to a callback that will get
         * called when the value of this robot variable is changed.
         */
        void addVariableChangedCallback(std::function<void(void)> callback)
        {
            this->variableChangedCallbacks.push_back(callback);
        }

        /**
         * @brief Get the value of the robot variable as type double
         * @return The value of this robot variable as type double
         */
        virtual double getValueAsDouble() const
        {
            return this->value;
        }

        virtual ~RobotVariable()
        { }

        /**
         * @brief call all variable changed callbacks associated with this variable
         */
        void notifyVariableChangedListeners() const
        {
            for(int i = 0;i<variableChangedCallbacks.size();i++)
            {
                this->variableChangedCallbacks[i]();
            }
        }

        /**
         * @brief Set the value of this variable and, if callVariableChangedCallbacks=true, call variable changed callbacks
         * @param value The value to set this variable to
         * @param callVariableChangedCallbacks Boolean to decide whether to call variable changed callbacks or not
         */
        virtual void set(const T value, const bool callVariableChangedCallbacks=true)
        {
            if(value!=this->value)
            {
                this->value = value;

                if(callVariableChangedCallbacks)
                {
                    notifyVariableChangedListeners();
                }
            }
        }

        /**
         * @brief Get the value of this robot variable
         * @return The value of this robot variable
         */
        virtual T get() const
        {
            return this->value;
        }

        /**
         * @brief Set the value of this robot variable to that of another robot variable
         * @param robotVariable Robot variable whose value this robot variable will be set to
         * @param callVariableChangedCallbacks Boolean to decide whether to call variable changed callbacks or not
         */
        template<typename TYPE>
        inline void set(const RobotVariable<TYPE> &robotVariable, const bool callVariableChangedCallbacks=true)
        {
            set(robotVariable.get(),callVariableChangedCallbacks);
        }

        /**
         * @brief Simple addition
         * @param value Value to add to this
         */
        inline void add(const T value)
        {
            set(get()+value);
        }

        /**
         * @brief Addition between two robot variables
         * @param robotVariable The variable to be added to this
         */
        template<typename TYPE>
        inline void add(const RobotVariable<TYPE> &robotVariable)
        {
            set(get()+robotVariable.get());
        }

        /**
         * @brief Simple subtraction
         * @param value Value to subtract from this
         */
        inline void subtract(const T value)
        {
            add(-value);
        }

        /**
         * @brief Subtraction between robot variables
         * @param robotVariable Robot variable whose value will be subtracted from this
         */
        template<typename TYPE>
        inline void subtract(const RobotVariable<TYPE> &robotVariable)
        {
            add(-robotVariable.get());
        }

        /**
         * @brief Simple multiplication
         * @param value Value to multiply this by
         */
        inline void multiply(const T value)
        {
            set(get()*value);
        }

        /**
         * @brief Multiplication between two robot variables
         * @param robotVariable  Robot variable whose value this will be multiplied by
         */
        template<typename TYPE>
        inline void multiply(const RobotVariable<TYPE> &robotVariable)
        {
            set(get()*robotVariable.get());
        }

        /**
         * @brief Simple division
         * @param value Value to divide this by
         */
        inline void divide(const T value)
        {
            multiply(1/value);
        }

        /**
         * @brief Division between robot variables
         * @param robotVariable Robot variable whose walue this will be divided by
         */
        template<typename TYPE>
        inline void divide(const RobotVariable<TYPE> &robotVariable)
        {
            multiply(1/robotVariable.get());
        }

        /**
         * @brief Checks if the arg value is equal to the value of this shared memory resource
         * @param value A template type value to compare this to for equality
         */
        inline bool equals(const T value)
        {
            return (get() == value);
        }

        /**
         * @brief Checks if the arg value is equal to the value stored in this
         * @param value A RobotVariable to compare this RobotVariable to for equality
         */
        template<typename TYPE>
        inline bool equals(const RobotVariable<TYPE> &robotVariable)
        {
            return equals(robotVariable.get());
        }

        /**
         * @brief Check if the arg value is within epsilon of this RobotVariable
         * @param value The value we are comparing this RobotVariable to
         * @param epsilon The range we are checking
         */
        inline bool epsilonEquals(const T& value, const T& epsilon)
        {
            return (fabs(get() - value) < epsilon);
        }

        /**
         * @brief Check if the arg value is within epsilon of this RobotVariable
         * @param RobotVariable The RobotVariable we are comparing this RobotVariable to
         * @param epsilon The range we are checking
         */
        template<typename TYPE>
        inline bool epsilonEquals(const RobotVariable<TYPE> &robotVariable, const T& epsilon)
        {
            return epsilonEquals(robotVariable.get(), epsilon);
        }

        inline std::string getName() const
        {
            return name;
        }

        template<typename TYPE>
        inline RobotVariable<T>& operator+=(const RobotVariable<TYPE> & rightHandSide)
        {
            this->set(this->get()+rightHandSide.get());
            return *this;
        }

        template<typename TYPE>
        inline RobotVariable<T>& operator-=(const RobotVariable<TYPE> & rightHandSide)
        {
            this->set(this->get()-rightHandSide.get());
            return *this;
        }

        template<typename TYPE>
        inline RobotVariable<T>& operator*=(const RobotVariable<TYPE> & rightHandSide)
        {
            this->set(this->get()*rightHandSide.get());
            return *this;
        }

        template<typename TYPE>
        inline RobotVariable<T>& operator/=(const RobotVariable<TYPE> & rightHandSide)
        {
            this->set(this->get()/rightHandSide.get());
            return *this;
        }

        template<typename TYPE>
        inline bool operator==(const RobotVariable<TYPE> &rightHandSide)
        {
            return this->get()==rightHandSide.get();
        }

        template<typename TYPE>
        inline bool operator!=(const RobotVariable<TYPE> &rightHandSide)
        {
            return this->get()!=rightHandSide.get();
        }

    protected:
        std::string name;
        T value;
        std::vector<variableChangedCallback> variableChangedCallbacks;
    };
}
#endif //__ROBOT_VARIABLE__
