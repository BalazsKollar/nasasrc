
/**
 * Interface to enforce all SMTVariables that do any
 * data processing implement an update() method.
 */

#ifndef __PROCESSING_ROBOT_VARIABLE_HPP__
#define __PROCESSING_ROBOT_VARIABLE_HPP__

namespace robot_variable
{

    class ProcessingRobotVariable
    {
    public:
        virtual void update() = 0;
    };

}
#endif //__PROCESSING_ROBOT_VARIABLE_HPP__
