/**
 * RobotPIDController provides a PID controller that creates shared memory variables
 * for the tunable controller parameters.
 */

#ifndef __SMT_PID_CONTROLLER_HPP__
#define __SMT_PID_CONTROLLER_HPP__

#include "robot_variable/RobotVariable.hpp"
#include "robot_variable/RobotVariableFactory.hpp"
#include "control_utils/PIDControllerInterfaces.hpp"

namespace robot_variable
{
    template<typename T>
    class RobotPIDController : public control_utils::ProportionalControllerInterface<T>, public control_utils::DerivativeControllerInterface<T>, public control_utils::IntegralControllerInterface<T>
    {
    public:
        /**
         * Constructor
         *
         * @param variableFactory A pointer to a a RobotVariableFactory.
         * @param name The name of the smt controller. Shared memory variables created within will be prependend with this name
         * @param kp The proportional gain
         * @param kd The derivative gain
         * @param ki The integral gain
         * @param maxIntegralError An optional cap on the allowed integral error. Default value is infinity
         * @param minEffort The minimum allowed controller effort. Default value is negative infinity
         * @param maxEffort The maximum allowed controller effort. Default value is infinity
         * @param integralLeakRatio A fractional amount of the total error that is ignored by the integrator.
         */
        RobotPIDController(robot_variable::RobotVariableFactory* variableFactory, const std::string name, const T kp, const T kd, const T ki, const T maxIntegralError=std::numeric_limits<T>::infinity(), const T minEffort=-std::numeric_limits<T>::infinity(), const T maxEffort=std::numeric_limits<T>::infinity(), const T integralLeakRatio = 1.0) : control_utils::ProportionalControllerInterface<T>(), control_utils::DerivativeControllerInterface<T>(), control_utils::IntegralControllerInterface<T>()
        {
            this->minEffort = variableFactory->makeVariable<T>(name+"_minEffort");
            this->maxEffort = variableFactory->makeVariable<T>(name + "_maxEffort");
            this->kp = variableFactory->makeVariable<T>(name + "_kp");
            this->kd = variableFactory->makeVariable<T>(name + "_kd");
            this->ki = variableFactory->makeVariable<T>(name + "_ki");
            this->maxIntegralError = variableFactory->makeVariable<T>(name + "_maxIntegralError");
            this->integralLeakRatio = variableFactory->makeVariable<T>(name + "_integralLeakRatio");

            this->minEffort->set(minEffort);
            this->maxEffort->set(maxEffort);
            this->kp->set(kp);
            this->kd->set(kd);
            this->ki->set(ki);
            this->maxIntegralError->set(maxIntegralError);
            this->integralLeakRatio->set(integralLeakRatio);
        }

        virtual ~RobotPIDController(){};

        /**
         * Computes the controllers output
         *
         * @param desiredPosition The desired position
         * @param currentPosition The current position
         * @param desiredVelocity The desired velocity
         * @param currentVelocity The current velocity
         * @param deltaTime The time step for the integrator
         */
        T compute(const T desiredPosition, const T currentPosition, const T desiredVelocity, const T currentVelocity, const T deltaTime)
        {
            this->proportionalControllerOutput = this->computeProportionalControllerOutput(desiredPosition,currentPosition);
            this->derivativeControllerOutput = this->computeDerivativeControllerOutput(desiredVelocity,currentVelocity);
            this->integralControllerOutput = this->computeIntegralControllerOutput(desiredPosition,currentPosition,deltaTime);

            return (this->proportionalControllerOutput + this->derivativeControllerOutput + this->integralControllerOutput);
        }

        /**
         * Computes the controllers output and intelligently handles the case where angles do not wrap.
         *
         * @param desiredPosition The desired position
         * @param currentPosition The current position
         * @param desiredVelocity The desired velocity
         * @param currentVelocity The current velocity
         * @param deltaTime The time step for the integrator
         */
        T computeForAngles(const T desiredPosition, const T currentPosition, const T desiredVelocity, const T currentVelocity,const T deltaTime)
        {
            this->proportionalControllerOutput = this->computeProportionalControllerOutputForAngles(desiredPosition,currentPosition);
            this->derivativeControllerOutput = this->computeDerivativeControllerOutput(desiredVelocity,currentVelocity);
            this->integralControllerOutput = this->computeIntegralControllerOutputForAngles(desiredPosition,currentPosition,deltaTime);

            return (this->proportionalControllerOutput + this->derivativeControllerOutput + this->integralControllerOutput);
        };


        /**
         * Set the controllers proportional gain
         *
         * @param kp The new value of the proportional gain
         */
        void setProportionalGain(const T kp)
        {
            this->kp->set(kp);
        }

        /**
         * Set the controllers integral gain
         *
         * @param ki The new value of the integral gain
         */
        void setIntegralGain(const T ki)
        {
            this->ki->set(ki);
        }

        /**
         * Set the controllers derivative gain
         *
         * @param kd The new value of the derivative gain
         */
        void setDerivativeGain(const T kd)
        {
            this->kd->set(kd);
        }

        /**
         * Set the controllers integral leak ratio
         *
         * @param integralLeakRatio The new value of the integral leak ratio
         */
        void setIntegralLeakRatio(const T integralLeakRatio)
        {
            this->integralLeakRatio->set(integralLeakRatio);
        }

        /**
         * Set the controllers max allowed integral error.
         *
         * @param maxIntegralError The new value of the max integral error
         */
        void setMaxIntegralError(const T maxIntegralError)
        {
            this->maxIntegralError->set(maxIntegralError);
        }

        /**
         * Set the maximum allowed positive effort for the integrator
         *
         * @param maxIntegralEffort The new value for the maximum allowed positive integral effort
         */
        void setMaxIntegralEffort(const T maxIntegralEffort)
        {
            this->maxEffort->set(maxIntegralEffort);
        }

        /**
         * Set the maximum allowed negative effort for the integrator
         *
         * @param minIntegralEffort The new value for the maximum allowed negative integral effort
         */
        void setMinIntegralEffort(const T minIntegralEffort)
        {
            this->minEffort->set(minIntegralEffort);
        }

        /**
         * Get the current proportional gain
         *
         * @return Proportional gain
         */
        T getProportionalGain() const
        {
            return this->kp->get();
        }

        /**
         * Get the derivative gain
         *
         * @return Derivative gain
         */
        T getDerivativeGain() const
        {
            return this->kd->get();
        }

        /**
         * Get the integral gain
         *
         * @return Integral gain
         */
        T getIntegralGain() const
        {
            return this->ki->get();
        }

        /**
         * Get the max allowed integral error
         *
         * @return Max integral error
         */
        T getMaxIntegralError() const
        {
            return this->maxIntegralError->get();
        }

        /**
         * Get the integral leak ratio
         *
         * @return The integral leak ratio
         */
        T getIntegralLeakRatio() const
        {
            return this->integralLeakRatio->get();
        }

        /**
         * Get the max allowed positive effort from the integrator
         *
         * @return The max allowed positive effort from the integrator
         */
        T getMaxIntegralEffort() const
        {
            return maxEffort->get();
        }

        /**
         * Get the minimum allowed effort from the integrator
         *
         * @return The minimum allowed effort from the integrator
         */
        T getMinIntegralEffort() const
        {
            return minEffort->get();
        }

    protected:

        std::shared_ptr<robot_variable::RobotVariable<T>> minEffort;
        std::shared_ptr<robot_variable::RobotVariable<T>> maxEffort;
        std::shared_ptr<robot_variable::RobotVariable<T>> kp;
        std::shared_ptr<robot_variable::RobotVariable<T>> kd;
        std::shared_ptr<robot_variable::RobotVariable<T>> ki;
        std::shared_ptr<robot_variable::RobotVariable<T>> maxIntegralError;
        std::shared_ptr<robot_variable::RobotVariable<T>> integralLeakRatio;

    };

}

#endif
