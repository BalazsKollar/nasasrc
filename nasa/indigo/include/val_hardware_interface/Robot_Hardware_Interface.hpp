#ifndef __ROBOT_HARDWARE_INTERFACE_HPP__
#define __ROBOT_HARDWARE_INTERFACE_HPP__

#include "val_robot_interface/Robot_Interface.hpp"
#include "val_hardware_interface/JointStatePublisher.hpp"
#include "val_hardware_interface/JointCommandPublisher.hpp"
#include "val_hardware_interface/AtiSensorPublisher.hpp"
#include "val_hardware_interface/ImuSensorPublisher.hpp"
#include "nasa_common_logging/Logger.h"

class RobotHardwareInterface : public robot_hardware_interface::RobotInterface
{
public:
    RobotHardwareInterface(SMT::SMTClient* smtClient,robot_variable::RobotVariableFactory* robotVariableFactory);
    ~RobotHardwareInterface()
    {
        NasaCommonLogging::Logger::log("gov.nasa.Robot_Hardware_Interface.destructor", log4cpp::Priority::INFO, "Stopping hardware interface!");
    }

    bool initXmlfromPath(std::string path);
    bool initializeRobotHardware(std::string const& paramName);

    void read(const ros::Time& time);
    void write(const ros::Time& time);

    void updateIMUSensors(const ros::Time& time);
    void updateForceTorqueSensors(const ros::Time& time);

    void stopHardware();

protected:
    virtual robot_hardware_interface::JointPtr createJoint(const std::string& name);

    ros::NodeHandle nh;
    RtJointStatePublisher jointStatePublisher;
    RtJointCommandPublisher jointCommandPublisher;
    RtAtiSensorPublisher forceTorquePublisher;
    RtImuSensorPublisher imuSensorPublisher;
};

#endif