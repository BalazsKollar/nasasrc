#ifndef __ATI_SENSOR_PUBLISHER_HPP__
#define __ATI_SENSOR_PUBLISHER_HPP__

#include <realtime_tools/realtime_publisher.h>
#include "ros/ros.h"
#include <sensor_msgs/Imu.h>
#include <val_hardware_msgs/valAtiSensor.h>
#include <hardware_interface/force_torque_sensor_interface.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Wrench.h>
#include "val_robot_interface/ForceTorqueSensorInterface.hpp"

typedef std::map<std::string, robot_hardware_interface::ForceTorqueSensorInterfacePtr> ForceTorqueSensors;
class RtAtiSensorPublisher
{
public:
    //typedef typename robot_hardware_interface::ObjectInterface<T>::ObjectMap ObjectMap;

    RtAtiSensorPublisher(ros::NodeHandle& nh, double rosPublishRate, unsigned int queueSize);
    virtual ~RtAtiSensorPublisher() {};

    /*! @brief Sizes internal storage.
     *  Given an expected number of objects to publish, this method will size internal
     *  storage to make subsequent publish() calls RT-safe.
     *
     *  @note This is a blocking call while it waits for non-RT threads to yield.
     */
    void setSample(ForceTorqueSensors forceTorqueSensors);

    /*! @brief Publishes state data. */
    void publish(ros::Time const& time, ForceTorqueSensors forceTorqueSensors);

protected:

    // ROS subsystem
    realtime_tools::RealtimePublisher<val_hardware_msgs::valAtiSensor> rosPublisher;
    ros::Time lastPublishTime;
    double rosPublishRate;

    bool isSampleSet;
};

#endif