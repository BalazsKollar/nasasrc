#ifndef __IMU_SENSOR_PUBLISHER_HPP__
#define __IMU_SENSOR_PUBLISHER_HPP__

#include <realtime_tools/realtime_publisher.h>
#include "ros/ros.h"
#include <sensor_msgs/Imu.h>
#include <hardware_interface/imu_sensor_interface.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Quaternion.h>
#include "val_robot_interface/ImuSensorInterface.hpp"
#include <val_hardware_msgs/valImuSensor.h>

typedef std::map<std::string, imu_sensor_interface::ImuSensorInterfacePtr> IMUSensors;
class RtImuSensorPublisher
{
public:
    //typedef typename robot_hardware_interface::ObjectInterface<T>::ObjectMap ObjectMap;

    RtImuSensorPublisher(ros::NodeHandle& nh, double rosPublishRate, unsigned int queueSize);
    virtual ~RtImuSensorPublisher() {};

    /*! @brief Sizes internal storage.
     *  Given an expected number of objects to publish, this method will size internal
     *  storage to make subsequent publish() calls RT-safe.
     *
     *  @note This is a blocking call while it waits for non-RT threads to yield.
     */
    void setSample(IMUSensors _imuSensors);

    /*! @brief Publishes state data. */
    void publish(ros::Time const& time, IMUSensors _imuSensors);

protected:

    // ROS subsystem
    realtime_tools::RealtimePublisher<val_hardware_msgs::valImuSensor> rosPublisher;
    ros::Time lastPublishTime;
    hardware_interface::ImuSensorHandle imuHandle;
    double rosPublishRate;

    bool isSampleSet;
};

#endif