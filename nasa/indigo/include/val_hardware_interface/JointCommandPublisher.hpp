#ifndef __JOINT_COMMAND_PUBLISHER_HPP__
#define __JOINT_COMMAND_PUBLISHER_HPP__

#include <realtime_tools/realtime_publisher.h>
#include "ros/ros.h"
#include <sensor_msgs/JointState.h>
#include <val_robot_interface/JointInterface.hpp>

class RtJointCommandPublisher
{
public:
    //typedef typename robot_hardware_interface::ObjectInterface<T>::ObjectMap ObjectMap;

    RtJointCommandPublisher(ros::NodeHandle& nh, double rosPublishRate, unsigned int queueSize);
    virtual ~RtJointCommandPublisher() {};

    /*! @brief Sizes internal storage.
     *  Given an expected number of objects to publish, this method will size internal
     *  storage to make subsequent publish() calls RT-safe.
     *
     *  @note This is a blocking call while it waits for non-RT threads to yield.
     */
    void setSample(std::vector<robot_hardware_interface::JointPtr> const& listOfJointInterfaces);

    /*! @brief Publishes state data. */
    void publish(ros::Time const& time, std::vector<robot_hardware_interface::JointPtr> const& listOfJointInterfaces);

protected:
    // ROS subsystem
    realtime_tools::RealtimePublisher<sensor_msgs::JointState> rosPublisher;
    ros::Time lastPublishTime;
    double rosPublishRate;

    bool isSampleSet;
};

#endif