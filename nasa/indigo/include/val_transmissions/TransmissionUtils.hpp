//
// Created by jordan on 3/22/16.
//

#ifndef __TRANSMISSION_UTILS__
#define __TRANSMISSION_UTILS__

#include "tinyxml.h"
#include <nasa_common_logging/Logger.h>

namespace transmission_utils
{
    class TransmissionUtils
    {
    public:


        /**
         * @brief A method for getting xml attributes from transmission xml strings
         * @param xmlElementData A string of xml data as shown below in the example
         * @param desiredElement The desired xml element of which you are trying to grab
         * @param desiredAttribute The attribute name, e.g. id, name, etc.
         *
         * @return A string of the attribute your trying to grab.
         */
        /************* Example Xml **************************
            "<actuator name="upperNeckPitch">
                <node id="/neck/j3"/>
                <mechanicalReduction>1</mechanicalReduction>
                <api name="robot_hardware_interface/TurbodriverAPI"/>
             </actuator>"
         *****************************************************/
        static std::string getTransmissionXmlAttributeAsString(const std::string &xmlElementData, const std::string &desiredElement, const std::string &desiredAttribute="id")
        {
            std::string actuatorNodeName;

            TiXmlDocument xmlDoc;

            if (!xmlDoc.Parse(xmlElementData.c_str()) && xmlDoc.Error())
            {
                std::string msg = "Unable to parse xml data!";
                NasaCommonLogging::Logger::log("gov.nasa.TransmissionUtils.getTransmissionXmlAttribute", log4cpp::Priority::ERROR, msg);

                throw std::runtime_error(msg);
            }

            TiXmlElement *root = xmlDoc.RootElement();

            TiXmlElement *contents = nullptr;

            contents = root->FirstChildElement(desiredElement);

            if (!contents)
            {
                std::string errorMessage = "Element " + desiredElement + " is missing from XML!";
                throw std::runtime_error(errorMessage);
            }

            std::string attributeValue;
            if (contents->Attribute(desiredAttribute))
            {
                contents->QueryValueAttribute(desiredAttribute, &attributeValue);
            }
            else
            {
                std::string errorMessage = "Attribute " + desiredAttribute + " is missing from XML element!";
                throw std::runtime_error(errorMessage);
            }

            return attributeValue;
        }
    };

}
#endif //__TRANSMISSION_UTILS__
