#ifndef __ANKLE_PUSHROD_TRANSMISSION_HPP__
#define __ANKLE_PUSHROD_TRANSMISSION_HPP__

//library included from ros-control
#include <transmission_interface/transmission.h>
#include "transmission_interface_exception.hpp"
#include <eigen3/Eigen/Eigen>
#include <vector>
#include <map>
#include <boost/python.hpp>


namespace robot_transmission_interface
{

    class AnklePushrodTransmission : public transmission_interface::Transmission
    {
    protected:

        std::map<std::string, unsigned int> act_role;
        std::map<std::string, unsigned int> jnt_role;

    public:

        AnklePushrodTransmission(std::map<std::string, unsigned int> jointMap, std::map<std::string, unsigned int> actuatorMap, const bool& useFutekSensors=true, const bool &useJacobianToComputeForceMap=true)
        {
            this->jnt_role = jointMap;
            this->act_role = actuatorMap;
            this->useJacobianToComputeForceMap = useJacobianToComputeForceMap;
            this->useFutekForceTorqueSensors = useFutekSensors;
        }

        AnklePushrodTransmission(const bool& useFutekSensors=true,const bool &useJacobianToComputeForceMap=true)
        {
            jnt_role["pitch"] = 0;
            jnt_role["roll"] = 1;
            act_role["left"] = 0;
            act_role["right"] = 1;
            this->useJacobianToComputeForceMap = useJacobianToComputeForceMap;
            this->useFutekForceTorqueSensors = useFutekSensors;
        };

        Eigen::Vector2d getPushrodPositionsFromAngles(Eigen::Vector2d jointAngle);

        Eigen::Vector2d getPushrodVelocitiesFromAngularRates(Eigen::Vector2d jointAngle, Eigen::Vector2d jointAngularVelocity);

        Eigen::Vector2d getTorqueFromPushrodForce(Eigen::Vector2d jointAngle, Eigen::Vector2d pushrodPosition, Eigen::Vector2d pushrodForce);
        Eigen::Vector2d getPushrodForceFromTorque(Eigen::Vector2d jointAngle, Eigen::Vector2d pushrodPosition, Eigen::Vector2d jointTorque);
        Eigen::Vector2d getAngularRatesFromPushrodVelocities(Eigen::Vector2d jointAngle, Eigen::Vector2d pushrodVelocities);

        //funciton call to convert from actuator position (pushrod positions) to joint angles
        void actuatorToJointPosition(const transmission_interface::ActuatorData& actuatorData, transmission_interface::JointData& jointData);

        //function call to convert from actuator velocity (pushrod velocity) to joint velocity
        void actuatorToJointVelocity(const transmission_interface::ActuatorData& actuatorData, transmission_interface::JointData& jointData);

        //function call to convert from actuator force (pushrod force) to joint torque
        void actuatorToJointEffort(const transmission_interface::ActuatorData& actuatorData, transmission_interface::JointData& jointData);

        //function call to convert from joint angles to actuator position (pushrod positions)
        void jointToActuatorPosition(const transmission_interface::JointData& jointData, transmission_interface::ActuatorData& actuatorData);

        //function call to convert from joint angular velocities to actuator velocities (pushrod velocities)
        void jointToActuatorVelocity(const transmission_interface::JointData& jointData, transmission_interface::ActuatorData& actuatorData);

        //function call to convert from joint torque to actuator force (pushrod force)
        void jointToActuatorEffort(const transmission_interface::JointData& jointData, transmission_interface::ActuatorData& actuatorData);

        boost::python::list getPushrodForcesPython(const boost::python::list pythonJointAngles, const boost::python::list pythonJointTorques);
        boost::python::list getPushrodPositionsPython(const boost::python::list pythonJointAngles);
        boost::python::list getPushrodVelocitiesPython(const boost::python::list pythonJointAngles, const boost::python::list pythonJointVelocities);

        std::size_t numActuators() const {return 2;}
        std::size_t numJoints()     const {return 2;}

        bool getUsingFutekForForceControl() const
        {
            return useFutekForceTorqueSensors;
        }

    private:
        bool useJacobianToComputeForceMap;
        bool useFutekForceTorqueSensors;

        double TwoForceMemberLength=0.104965;
        double cThetaFromBaseFrameToLeftPushrodFrame=1.;
        double sThetaFromBaseFrameToLeftPushrodFrame=0.;
        double cThetaFromBaseFrameToRightPushrodFrame=1.;
        double sThetaFromBaseFrameToRightPushrodFrame=0.;
        double pFromJ1ToJ2X=0.;
        double pFromJ1ToJ2Y=0.;
        double pFromJ1ToJ2Z=0.;
        double pFromJ2FrameToLeftTwoForceMemberOutputSideX=-0.0362189;
        double pFromJ2FrameToLeftTwoForceMemberOutputSideY=0.0413004;
        double pFromJ2FrameToLeftTwoForceMemberOutputSideZ=0.0176054;
        double pFromJ2FrameToRightTwoForceMemberOutputSideX=-0.0362189;
        double pFromJ2FrameToRightTwoForceMemberOutputSideY=-0.0413004;
        double pFromJ2FrameToRightTwoForceMemberOutputSideZ=0.0176054;
        double pFromBaseFrameToLeftTwoForceMemberInputSideX=-0.0292628;
        double pFromBaseFrameToLeftTwoForceMemberInputSideY=0.0412737;
        double pFromBaseFrameToLeftTwoForceMemberInputSideZ=0.12234;
        double pFromBaseFrameToRightTwoForceMemberInputSideX=-0.0292628;
        double pFromBaseFrameToRightTwoForceMemberInputSideY=-0.0412737;
        double pFromBaseFrameToRightTwoForceMemberInputSideZ=0.12234;

        double getALeft();
        double getBLeft(const double &sq1, const double &sq2, const double &cq1, const double &cq2);
        double getCLeft(const double &sq1, const double &sq2, const double &cq1, const double &cq2);
        double getARight();
        double getBRight(const double &sq1, const double &sq2, const double &cq1, const double &cq2);
        double getCRight(const double &sq1, const double &sq2, const double &cq1, const double &cq2);
        double getDbLeftDq1(const double &sq1, const double &sq2, const double &cq1, const double &cq2);
        double getDbLeftDq2(const double &sq1, const double &sq2, const double &cq1, const double &cq2);
        double getDcLeftDq1(const double &sq1, const double &sq2, const double &cq1, const double &cq2);
        double getDcLeftDq2(const double &sq1, const double &sq2, const double &cq1, const double &cq2);
        double getDbRightDq1(const double &sq1, const double &sq2, const double &cq1, const double &cq2);
        double getDbRightDq2(const double &sq1, const double &sq2, const double &cq1, const double &cq2);
        double getDcRightDq1(const double &sq1, const double &sq2, const double &cq1, const double &cq2);
        double getDcRightDq2(const double &sq1, const double &sq2, const double &cq1, const double &cq2);

        Eigen::Matrix2d getJointVelocityToPushrodVelocityMap(const double &sq1, const double &sq2, const double &cq1, const double &cq2);
        Eigen::Matrix2d getPushrodVelocityToJointVelocityMap(const double &sq1, const double &sq2, const double &cq1, const double &cq2);
        Eigen::Matrix2d getPushrodForceToJointTorqueMap(const Eigen::Vector2d &pushrodPosition,const double &sq1, const double &sq2, const double &cq1, const double &cq2);
        Eigen::Matrix2d getPushrodForceToJointTorqueMapUsingJacobian(const Eigen::Vector2d &pushrodPosition,const double &sq1, const double &sq2, const double &cq1, const double &cq2);
        Eigen::Matrix2d getPushrodForceToJointTorqueMapUsingRCrossF(const Eigen::Vector2d &pushrodPosition,const double &sq1, const double &sq2, const double &cq1, const double &cq2);
        Eigen::Matrix2d getJointTorqueToPushrodForceMap(const Eigen::Vector2d &pushrodPosition,const double &sq1, const double &sq2, const double &cq1, const double &cq2);

    };

}

#endif
