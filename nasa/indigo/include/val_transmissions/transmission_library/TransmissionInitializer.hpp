#ifndef __TRANSMISSION_INITIALIZER_HPP__
#define __TRANSMISSION_INITIALIZER_HPP__

#include "control_utils/SideDependentList.hpp"
#include "val_transmissions/transmission_library/AnklePushrodTransmission.hpp"
#include "val_transmissions/transmission_library/WristPushrodTransmission.hpp"
#include "val_transmissions/transmission_library/TorsoPushrodTransmission.hpp"
#include <ros/node_handle.h>
#include <memory>
#include <eigen3/Eigen/Eigen>

#include "shared_memory_transport/smtclient.hpp"

namespace robot_transmission_interface
{

    class TransmissionInitializer
    {
    public:
        TransmissionInitializer();

        ~TransmissionInitializer(){};

        bool initializeFromRosRobotDescription(const std::string &parameterName = "/robot_description", const ros::NodeHandle &nodeToLoadFrom = ros::NodeHandle());

        void initializeLinearActuatorPositions();


    private:
        std::string getActuatorNodeName(const std::string &xmlElementData);

        control_utils::SideDependentList<std::map<std::string, std::shared_ptr<SMT::Resource> > > ankleJointPositionResourceMap;
        control_utils::SideDependentList<std::map<std::string, std::shared_ptr<SMT::Resource> > > wristJointPositionResourceMap;

        control_utils::SideDependentList<robot_transmission_interface::AnklePushrodTransmission> ankleTransmissions;
        control_utils::SideDependentList<robot_transmission_interface::WristPushrodTransmission> wristTransmissions;

        control_utils::SideDependentList<control_utils::SideDependentList<std::shared_ptr<SMT::Resource> > > anklePushrodPositionNodeNames;
        control_utils::SideDependentList<control_utils::SideDependentList<std::shared_ptr<SMT::Resource> > > wristPushrodPositionNodeNames;
        control_utils::SideDependentList<std::shared_ptr<SMT::Resource> > torsoPushrodPositionNodeNames;

        control_utils::SideDependentList<control_utils::SideDependentList<std::shared_ptr<SMT::Resource> > > anklePushrodOffsetToggleNodeNames;
        control_utils::SideDependentList<control_utils::SideDependentList<std::shared_ptr<SMT::Resource> > > wristPushrodOffsetToggleNodeNames;
        control_utils::SideDependentList<std::shared_ptr<SMT::Resource> > torsoPushrodOffsetToggleNodeNames;

        std::map<std::string, std::shared_ptr<SMT::Resource> > torsoJointPositionResourceMap;
        robot_transmission_interface::TorsoPushrodTransmission torsoTransmissions;

        std::string torsoNodePath = "/trunk";
        std::string rightAnkleNodePath = "/right_leg/ankle";
        std::string leftAnkleNodePath = "/left_leg/ankle";
        std::string rightWristNodePath = "/right_arm/athena1";
        std::string leftWristNodePath = "/left_arm/athena1";

        std::string wristRollJointPositionTopicName = "/wrist_left/JointAPS_Angle_Rad";
        std::string wristPitchJointPositionTopicName = "/wrist_right/JointAPS_Angle_Rad";

        std::string ankleRollJointPositionTopicName = "/left_actuator/JointAPS_Angle_Rad";
        std::string anklePitchJointPositionTopicName = "/right_actuator/JointAPS_Angle_Rad";

        std::string torsoRollJointPositionTopicName = "/left_actuator/JointAPS_Angle_Rad";
        std::string torsoPitchJointPositionTopicName = "/right_actuator/JointAPS_Angle_Rad";

        std::string linearActuatorPositionOffsetTopicName = "/EncPosOffset_m";
        std::string linearActuatorOffsetModeToToggleTopicName = "/EncPosOffsetMode";

        std::string wristLinearActuatorPositionOffsetTopicName = "/currentPosMeters";

        std::shared_ptr<SMT::SMTClient> client;

        bool foundRightAnkle = false;
        bool foundLeftAnkle = false;

        bool foundRightWrist = false;
        bool foundLeftWrist = false;

        bool foundTorso = false;

        ros::NodeHandle nodeHandle;
    };
}

#endif
