#ifndef __ATHENA_FINGER_TRANSMISSION_HPP__
#define __ATHENA_FINGER_TRANSMISSION_HPP__

//library included from ros-control
#include <transmission_interface/transmission.h>
#include "transmission_interface_exception.hpp"
#include <eigen3/Eigen/Eigen>
#include <vector>
#include <ros/ros.h>
#include <map>
#include <boost/python.hpp>


namespace robot_transmission_interface
{

    class AthenaFingerTransmission : public transmission_interface::Transmission
    {
    protected:
        std::map<std::string, unsigned int> jnt_role;

    public:

        AthenaFingerTransmission(std::map<std::string, unsigned int> jointMap)
        {
            this->jnt_role = jointMap;
        }

        AthenaFingerTransmission()
        {
            jnt_role["index"] = 0;
            jnt_role["middle"] = 1;
            jnt_role["pinky"] = 2;
            jnt_role["thumb"] = 3;
        }

        //funciton call to convert from actuator position (pushrod positions) to joint angles
        void actuatorToJointPosition(const transmission_interface::ActuatorData& actuatorData, transmission_interface::JointData& jointData);

        //function call to convert from actuator velocity (pushrod velocity) to joint velocity
        void actuatorToJointVelocity(const transmission_interface::ActuatorData& actuatorData, transmission_interface::JointData& jointData);

        //function call to convert from actuator force (pushrod force) to joint torque
        void actuatorToJointEffort(const transmission_interface::ActuatorData& actuatorData, transmission_interface::JointData& jointData);

        //function call to convert from joint angles to actuator position (pushrod positions)
        void jointToActuatorPosition(const transmission_interface::JointData& jointData, transmission_interface::ActuatorData& actuatorData);

        //function call to convert from joint angular velocities to actuator velocities (pushrod velocities)
        void jointToActuatorVelocity(const transmission_interface::JointData& jointData, transmission_interface::ActuatorData& actuatorData);

        //function call to convert from joint torque to actuator force (pushrod force)
        void jointToActuatorEffort(const transmission_interface::JointData& jointData, transmission_interface::ActuatorData& actuatorData);

        std::size_t numActuators() const {return 1;}
        std::size_t numJoints()    const {return 4;}

    };

}
#endif // __ATHENA_FINGER_TRANSMISSION_HPP__
