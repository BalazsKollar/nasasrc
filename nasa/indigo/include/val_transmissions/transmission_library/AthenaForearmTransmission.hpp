#ifndef __ATHENA_FOREARM_TRANSMISSION_HPP__
#define __ATHENA_FOREARM_TRANSMISSION_HPP__

//library included from ros-control
#include <transmission_interface/transmission.h>
#include <val_transmissions/transmission_library/WristPushrodTransmission.hpp>
#include "transmission_interface_exception.hpp"
#include <eigen3/Eigen/Eigen>
#include <vector>
#include <ros/ros.h>
#include <map>
#include <boost/python.hpp>


namespace robot_transmission_interface
{

    class AthenaForearmTransmission : public transmission_interface::Transmission
    {
    protected:
        std::map<std::string, unsigned int> jnt_role;
        Eigen::Vector2d wristJointPosition;
        Eigen::Vector2d wristJointVelocity;
        Eigen::Vector2d wristJointEffort;

        Eigen::Vector2d wristActuatorPosition;
        Eigen::Vector2d wristActuatorVelocity;
        Eigen::Vector2d wristActuatorEffort;

        robot_transmission_interface::WristPushrodTransmission wristTransmission;
        RobotSide::Side robotSide;

    public:

        AthenaForearmTransmission(RobotSide::Side robotSideIn, std::map<std::string, unsigned int> jointMap)
        {
            this->jnt_role = jointMap;
            this->robotSide = robotSideIn;

            std::map<std::string, unsigned int> wristActuatorMap;
            std::map<std::string, unsigned int> wristJointMap;

            wristJointMap["roll"] = 0;
            wristJointMap["pitch"] = 1;
            wristActuatorMap["bottom"] = 0;
            wristActuatorMap["top"] = 1;

            wristTransmission.SetupTransmission(robotSideIn, wristJointMap, wristActuatorMap);

            for (unsigned int i = 0; i < 2; i++)
            {
                wristJointPosition(i) = 0.0;
                wristJointVelocity(i) = 0.0;
                wristJointEffort(i) = 0.0;

                wristActuatorPosition(i) = 0.0;
                wristActuatorVelocity(i) = 0.0;
                wristActuatorEffort(i) = 0.0;
            }
        }

        AthenaForearmTransmission(RobotSide::Side robotSideIn)
        {
            this->robotSide = robotSideIn;
            jnt_role["wristroll"] = 0;
            jnt_role["wristpitch"] = 1;
            jnt_role["thumbroll"] = 2;
            jnt_role["thumbpitch"] = 3;
        }

        //funciton call to convert from actuator position (pushrod positions) to joint angles
        void actuatorToJointPosition(const transmission_interface::ActuatorData& actuatorData, transmission_interface::JointData& jointData);

        //function call to convert from actuator velocity (pushrod velocity) to joint velocity
        void actuatorToJointVelocity(const transmission_interface::ActuatorData& actuatorData, transmission_interface::JointData& jointData);

        //function call to convert from actuator force (pushrod force) to joint torque
        void actuatorToJointEffort(const transmission_interface::ActuatorData& actuatorData, transmission_interface::JointData& jointData);

        //function call to convert from joint angles to actuator position (pushrod positions)
        void jointToActuatorPosition(const transmission_interface::JointData& jointData, transmission_interface::ActuatorData& actuatorData);

        //function call to convert from joint angular velocities to actuator velocities (pushrod velocities)
        void jointToActuatorVelocity(const transmission_interface::JointData& jointData, transmission_interface::ActuatorData& actuatorData);

        //function call to convert from joint torque to actuator force (pushrod force)
        void jointToActuatorEffort(const transmission_interface::JointData& jointData, transmission_interface::ActuatorData& actuatorData);

        std::size_t numActuators() const {return 1;}
        std::size_t numJoints()    const {return 4;}

    private:


    };

}
#endif // __ATHENA_FOREARM_TRANSMISSION_HPP__
