#ifndef __TRANSMISSION_INTERFACE_EXCEPTION_H__
#define __TRANSMISSION_INTERFACE_EXCEPTION_H__

#include <exception>

namespace robot_transmission_interface
{

    class TransmissionInterfaceException: public std::exception
    {
    public:
        TransmissionInterfaceException(const std::string& message) : msg(message) {}
        virtual ~TransmissionInterfaceException() throw() {}
        virtual const char* what() const throw() {return msg.c_str();}
    private:
        std::string msg;
    };

} // transmission_interface

#endif // TRANSMISSION_INTERFACE_TRANSMISSION_INTERFACE_EXCEPTION_H