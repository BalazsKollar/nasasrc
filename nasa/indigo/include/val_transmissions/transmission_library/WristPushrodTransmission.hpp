#ifndef __WRIST_PUSHROD_TRANSMISSION_HPP__
#define __WRIST_PUSHROD_TRANSMISSION_HPP__

/*
 * Wrist pushrod transmission is used to map wrist position velocity and torque from joint space to actuator space.
 * For the wrist, since the left and right are mirrors of each other, the solution is generated for the right wrist
 * and things are reflected as necessary. Also, the reference to left/right is looking down on the back side of the palm
 * with up facing the robot and down facing away from the robot. Sorry if it causes confusion. *J-
 */

#include <transmission_interface/transmission.h>
#include <eigen3/Eigen/Eigen>
#include "transmission_interface_exception.hpp"
#include <vector>
#include <ros/ros.h>
#include "control_utils/RobotSide.hpp"
#include <map>
#include <boost/python.hpp>

namespace robot_transmission_interface
{

    class WristPushrodTransmission : public transmission_interface::Transmission
    {
    protected:

        RobotSide::Side robotSide;
        std::map<std::string, unsigned int> act_role;
        std::map<std::string, unsigned int> jnt_role;

    public:

        WristPushrodTransmission(RobotSide::Side wristSide, std::map<std::string, unsigned int> jointMap,
                                 std::map<std::string, unsigned int> actuatorMap,
                                 const bool& useJacobianToComputeForceMap = true)
        {
            this->robotSide = wristSide;
            jnt_role = jointMap;
            act_role = actuatorMap;
            this->useJacobianToComputeForceMap = useJacobianToComputeForceMap;
        }

        // Default constructor is not to be used for creating a useful wrist transmission object. Without a default constructor, things such as
        // using brackets to insert a WristPushrodTrasmission into a map, i.e. myMap[key] = wristPushRodTransmission will not work.
        WristPushrodTransmission()
        {
            jnt_role["roll"] = 0;
            jnt_role["pitch"] = 1;
            act_role["bottom"] = 0;
            act_role["top"] = 1;
        }

        WristPushrodTransmission(RobotSide::Side wristSide)
        {
            this->robotSide = wristSide;
            jnt_role["roll"] = 0;
            jnt_role["pitch"] = 1;
            act_role["bottom"] = 0;
            act_role["top"] = 1;
        }

        void SetupTransmission(RobotSide::Side wristSide, std::map<std::string, unsigned int> jointMap,
                               std::map<std::string, unsigned int> actuatorMap,
                               const bool& useJacobianToComputeForceMap = true)
        {
            this->robotSide = wristSide;
            jnt_role = jointMap;
            act_role = actuatorMap;
            this->useJacobianToComputeForceMap = useJacobianToComputeForceMap;
        }

        Eigen::Vector2d getPushrodPositionsFromAngles(Eigen::Vector2d jointAngle);

        Eigen::Vector2d getPushrodVelocitiesFromAngularRates(Eigen::Vector2d jointAngle,
                Eigen::Vector2d jointAngularVelocity);

        Eigen::Vector2d getTorqueFromPushrodForce(Eigen::Vector2d jointAngle, Eigen::Vector2d pushrodPosition,
                Eigen::Vector2d pushrodForce);

        Eigen::Vector2d getPushrodForceFromTorque(Eigen::Vector2d jointAngle, Eigen::Vector2d pushrodPosition,
                Eigen::Vector2d jointTorque);

        Eigen::Vector2d getAngularRatesFromPushrodVelocities(Eigen::Vector2d jointAngle,
                Eigen::Vector2d pushrodVelocities);

        //funciton call to convert from actuator position (pushrod positions) to joint angles
        void actuatorToJointPosition(const transmission_interface::ActuatorData& actuatorData,
                                     transmission_interface::JointData& jointData);

        //function call to convert from actuator velocity (pushrod velocity) to joint velocity
        void actuatorToJointVelocity(const transmission_interface::ActuatorData& actuatorData,
                                     transmission_interface::JointData& jointData);

        //function call to convert from actuator force (pushrod force) to joint torque
        void actuatorToJointEffort(const transmission_interface::ActuatorData& actuatorData,
                                   transmission_interface::JointData& jointData);

        //function call to convert from joint angles to actuator position (pushrod positions)
        void jointToActuatorPosition(const transmission_interface::JointData& jointData,
                                     transmission_interface::ActuatorData& actuatorData);

        //function call to convert from joint angular velocities to actuator velocities (pushrod velocities)
        void jointToActuatorVelocity(const transmission_interface::JointData& jointData,
                                     transmission_interface::ActuatorData& actuatorData);

        //function call to convert from joint torque to actuator force (pushrod force)
        void jointToActuatorEffort(const transmission_interface::JointData& jointData,
                                   transmission_interface::ActuatorData& actuatorData);

        boost::python::list getPushrodForcesPython(const boost::python::list pythonJointAngles,
                const boost::python::list pythonJointTorques);

        boost::python::list getPushrodPositionsPython(const boost::python::list pythonJointAngles);

        boost::python::list getPushrodVelocitiesPython(const boost::python::list pythonJointAngles,
                const boost::python::list pythonJointVelocities);


        std::size_t numActuators() const
        {
            return 2;
        }

        std::size_t numJoints() const
        {
            return 2;
        }

    private:
        bool useJacobianToComputeForceMap;

        double TwoForceMemberLength = 0.0381364;
        double cThetaFromBaseFrameToLeftPushrodFrame = 1.;
        double sThetaFromBaseFrameToLeftPushrodFrame = 0.;
        double cThetaFromBaseFrameToRightPushrodFrame = 1.;
        double sThetaFromBaseFrameToRightPushrodFrame = 0.;
        double pFromJ1ToJ2X = 0.;
        double pFromJ1ToJ2Y = 0.;
        double pFromJ1ToJ2Z = 0.;
        double pFromJ2FrameToLeftTwoForceMemberOutputSideX = -0.0282575;
        double pFromJ2FrameToLeftTwoForceMemberOutputSideY = -0.0000254;
        double pFromJ2FrameToLeftTwoForceMemberOutputSideZ = -0.0254;
        double pFromJ2FrameToRightTwoForceMemberOutputSideX = -0.0282575;
        double pFromJ2FrameToRightTwoForceMemberOutputSideY = -0.0000254;
        double pFromJ2FrameToRightTwoForceMemberOutputSideZ = 0.0254;
        double pFromBaseFrameToLeftTwoForceMemberInputSideX = -0.0263549;
        double pFromBaseFrameToLeftTwoForceMemberInputSideY = 0.0366928;
        double pFromBaseFrameToLeftTwoForceMemberInputSideZ = -0.015455;
        double pFromBaseFrameToRightTwoForceMemberInputSideX = -0.0263549;
        double pFromBaseFrameToRightTwoForceMemberInputSideY = 0.0366926;
        double pFromBaseFrameToRightTwoForceMemberInputSideZ = 0.015455;

        double getALeft();

        double getBLeft(const double& sq1, const double& sq2, const double& cq1, const double& cq2);

        double getCLeft(const double& sq1, const double& sq2, const double& cq1, const double& cq2);

        double getARight();

        double getBRight(const double& sq1, const double& sq2, const double& cq1, const double& cq2);

        double getCRight(const double& sq1, const double& sq2, const double& cq1, const double& cq2);

        double getDbLeftDq1(const double& sq1, const double& sq2, const double& cq1, const double& cq2);

        double getDbLeftDq2(const double& sq1, const double& sq2, const double& cq1, const double& cq2);

        double getDcLeftDq1(const double& sq1, const double& sq2, const double& cq1, const double& cq2);

        double getDcLeftDq2(const double& sq1, const double& sq2, const double& cq1, const double& cq2);

        double getDbRightDq1(const double& sq1, const double& sq2, const double& cq1, const double& cq2);

        double getDbRightDq2(const double& sq1, const double& sq2, const double& cq1, const double& cq2);

        double getDcRightDq1(const double& sq1, const double& sq2, const double& cq1, const double& cq2);

        double getDcRightDq2(const double& sq1, const double& sq2, const double& cq1, const double& cq2);

        Eigen::Matrix2d getJointVelocityToPushrodVelocityMap(const double& sq1, const double& sq2, const double& cq1,
                const double& cq2);

        Eigen::Matrix2d getPushrodVelocityToJointVelocityMap(const double& sq1, const double& sq2, const double& cq1,
                const double& cq2);

        Eigen::Matrix2d getPushrodForceToJointTorqueMap(const Eigen::Vector2d& pushrodPosition, const double& sq1,
                const double& sq2, const double& cq1, const double& cq2);

        Eigen::Matrix2d getPushrodForceToJointTorqueMapUsingJacobian(const Eigen::Vector2d& pushrodPosition,
                const double& sq1, const double& sq2,
                const double& cq1, const double& cq2);

        Eigen::Matrix2d getPushrodForceToJointTorqueMapUsingRCrossF(const Eigen::Vector2d& pushrodPosition,
                const double& sq1, const double& sq2,
                const double& cq1, const double& cq2);

        Eigen::Matrix2d getJointTorqueToPushrodForceMap(const Eigen::Vector2d& pushrodPosition, const double& sq1,
                const double& sq2, const double& cq1, const double& cq2);

    };
}
#endif // TRANSMISSION_LIBRARY_WRIST_PUSHROD_TRANSMISSION_HPP
