#ifndef __ANKLE_PUSHROD_TRANSMISSION_LOADER_HPP__
#define __ANKLE_PUSHROD_TRANSMISSION_LOADER_HPP__

#include <transmission_interface/transmission_loader.h>
#include <tinyxml.h>

namespace robot_transmission_interface
{
    class AnklePushrodTransmissionLoader : public transmission_interface::TransmissionLoader
    {
    public:
        TransmissionPtr load(const transmission_interface::TransmissionInfo& transmission_info);

    private:
        static bool getActuatorConfig(const transmission_interface::TransmissionInfo& transmission_info,
                                      std::vector<double>&    actuator_reduction, std::map<std::string, unsigned int>& actuator_role);

        static bool getJointConfig(const transmission_interface::TransmissionInfo& transmission_info,
                                   std::vector<double>&    joint_reduction,
                                   std::vector<double>&    joint_offset,
                                   std::map<std::string, unsigned int>& joint_role);

    };
}
#endif