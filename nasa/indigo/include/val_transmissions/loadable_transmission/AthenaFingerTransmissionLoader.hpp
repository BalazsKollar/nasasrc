#ifndef ___ATHENA_FINGER_TRANSMISSION_LOADER_HPP__
#define ___ATHENA_FINGER_TRANSMISSION_LOADER_HPP__

#include <transmission_interface/transmission_loader.h>
#include <tinyxml.h>

namespace robot_transmission_interface
{
    class AthenaFingerTransmissionLoader : public transmission_interface::TransmissionLoader
    {
    public:

        TransmissionPtr load(const transmission_interface::TransmissionInfo& transmissionInfo);

    private:
        static bool getActuatorConfig(const transmission_interface::TransmissionInfo& transmissionInfo,
                                      std::vector<double>&    actuatorReduction, std::map<std::string, unsigned int>& actuatorRole) {}

        static bool getJointConfig(const transmission_interface::TransmissionInfo& transmissionInfo,
                                   std::vector<double>&    jointReduction,
                                   std::vector<double>&    jointOffset,
                                   std::map<std::string, unsigned int>& jointRole);
    };
}
#endif