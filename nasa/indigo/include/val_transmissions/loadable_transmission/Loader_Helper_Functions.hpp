#ifndef __LOADER_HELPER_FUNCTIONS_HPP__
#define __LOADER_HELPER_FUNCTIONS_HPP__

#include <gtest/gtest.h>
#include <string>
#include <transmission_interface/transmission_loader.h>
#include <transmission_interface/transmission_info.h>
#include <transmission_interface/transmission_parser.h>
#include <boost/foreach.hpp>
#include <pluginlib/class_loader.h>
#include <resource_retriever/retriever.h>
#include <ros/console.h>

namespace robot_transmission_interface
{

    class UrdfFileReader
    {
    public:
        UrdfFileReader() {};

        bool readFile(const std::string& filename, std::string& contents)
        {
            resource_retriever::Retriever retriever;
            resource_retriever::MemoryResource resource;

            try
            {
                resource = retriever.get("package://val_transmissions/" + filename);
            }
            catch (resource_retriever::Exception& e)
            {
                ROS_ERROR("Failed to retrieve file: %s", e.what());
                return false;
            }

            contents.assign(resource.data.get(), resource.data.get() + resource.size);
            return true;
        }

        std::vector<transmission_interface::TransmissionInfo> parseUrdf(const std::string& filename)
        {
            std::vector<transmission_interface::TransmissionInfo> infos;

            std::string urdf;

            if (!readFile(filename, urdf)) {return std::vector<transmission_interface::TransmissionInfo>();}

            transmission_interface::TransmissionParser parser;

            if (!parser.parse(urdf, infos)) {return std::vector<transmission_interface::TransmissionInfo>();}

            return infos;
        }
    };

    class UrdfRobotFileReader
    {
    public:
        UrdfRobotFileReader() {};

        bool readFile(const std::string& filename, std::string& contents)
        {
            resource_retriever::Retriever retriever;
            resource_retriever::MemoryResource resource;

            try
            {
                resource = retriever.get(filename);
            }
            catch (resource_retriever::Exception& e)
            {
                ROS_ERROR("Failed to retrieve file: %s", e.what());
                return false;
            }

            contents.assign(resource.data.get(), resource.data.get() + resource.size);
            return true;
        }

        std::vector<transmission_interface::TransmissionInfo> parseUrdf(const std::string& filename)
        {
            std::vector<transmission_interface::TransmissionInfo> infos;

            std::string urdf;

            if (!readFile(filename, urdf)) {return std::vector<transmission_interface::TransmissionInfo>();}

            transmission_interface::TransmissionParser parser;

            if (!parser.parse(urdf, infos)) {return std::vector<transmission_interface::TransmissionInfo>();}

            return infos;
        }
    };

    struct TransmissionPluginLoader
    {
        TransmissionPluginLoader()
            : class_loader_("transmission_interface", "transmission_interface::TransmissionLoader")
        {
        }

        boost::shared_ptr<transmission_interface::TransmissionLoader> create(const std::string& type)
        {

            try
            {
                return class_loader_.createInstance(type);
            }
            catch (...)
            {
                ROS_ERROR("Failed to load transmission instance");
                return boost::shared_ptr<transmission_interface::TransmissionLoader>();
            }
        }

    private:
        //must keep it alive because instance destroyers need it
        pluginlib::ClassLoader<transmission_interface::TransmissionLoader>  class_loader_;
    };
}
#endif