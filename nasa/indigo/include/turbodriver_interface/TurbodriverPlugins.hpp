#ifndef TURBODRIVER_PLUGINS_HPP
#define TURBODRIVER_PLUGINS_HPP

#include "turbodriver_interface/TurbodriverInterface.hpp"
#include <turbodriver_api/TurbodriverAPI/TurbodriverAPI.hpp>
#include <turbodriver_api/TurbodriverSimAPI/TurbodriverSimAPI.hpp>
#include <turbodriver_api/TurbodriverAPI_linear_futek/TurbodriverAPI_linear_futek.hpp>
#include <turbodriver_api/TurbodriverAPI_linear_Renishaw/TurbodriverAPI_linear_Renishaw.hpp>
#include <val_robot_interface/ActuatorInterface.hpp>

namespace robot_hardware_interface
{
    class TurbodriverStdInterface : public robot_hardware_interface::TurbodriverInterface<turbodriver_api::TurbodriverAPI> {};
    class TurbodriverLinearInterfaceRenishaw : public TurbodriverInterface<turbodriver_api::TurbodriverAPI_linear_Renishaw> {};
    class TurbodriverLinearInterfaceFutek : public TurbodriverInterface<turbodriver_api::TurbodriverAPI_linear_futek> {};
    class TurbodriverStdSimInterface : public robot_hardware_interface::TurbodriverSimInterface<turbodriver_api::TurbodriverSimAPI> {};
}

#endif