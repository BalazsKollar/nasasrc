#ifndef TURBODRIVER_API_TURBODRIVER_INTERFACE_HPP_
#define TURBODRIVER_API_TURBODRIVER_INTERFACE_HPP_

#include <val_robot_interface/ActuatorInterface.hpp>
#include <nasa_common_logging/Logger.h>
#include <shared_memory_transport/smtclient.hpp>
#include "turbodriver_api/TurbodriverBase.hpp"
#include <type_traits>

namespace robot_hardware_interface
{

    namespace sp = std::placeholders;

    template <class T>
    class TurbodriverInterface : public ActuatorInterface
    {
        static_assert(std::is_base_of< turbodriver_api::TurbodriverApiBase, T>::value, "T must inherit from TurbodriverApiBase");
    public:
        TurbodriverInterface()
        {
            previousValue = 0.0;
            glitchFilterThreshold = 0.025;
        }

        void initialize(const std::string& nodepath, SMT::SMTClient* smtClient)
        {
            this->nodePath = nodepath;

            if (!smtClient)
            {
                std::string msg = "smt client input argument is null";
                throw std::runtime_error(msg);
            }

            client = smtClient;

            turboAPI.Subscribe(nodepath, smtClient);

            // Register the mode names with the handle
            modeCommandMap["Park"] = std::bind(&TurbodriverInterface::park, this);
            modeNames.push_back("Park");
            modeCommandMap["Reset"] = std::bind(&TurbodriverInterface::clearFault, this);
            modeNames.push_back("Reset");
            modeCommandMap["TorqueMode"] = std::bind(&TurbodriverInterface::torqueMode, this);
            modeNames.push_back("TorqueMode");
            modeCommandMap["PositionMode"] = std::bind(&TurbodriverInterface::positionMode, this);
            modeNames.push_back("PositionMode");

            modeStateMap[(int)ModeState::Resetting] = "Resetting";
            modeStateMap[(int)ModeState::Parked] = "Parked";
            modeStateMap[(int)ModeState::BrakeReleased] = "BrakeReleased";
            modeStateMap[(int)ModeState::PositionMode] = "PositionMode";
            modeStateMap[(int)ModeState::VelocityMode] = "VelocityMode";
            modeStateMap[(int)ModeState::Undefined] = "Undefined";

            enableCommandMap["Enable"] = std::bind(&robot_hardware_interface::BaseJointActuator::enable, this);
            enableNames.push_back("Enable");

            enableCommandMap["Disable"] = std::bind(&robot_hardware_interface::BaseJointActuator::disable, this);
            enableNames.push_back("Disable");

            // Fault monitors
            faultMonitors.push_back( TurbodriverInterface::FaultMonitor(nodepath + " APS sensor fault", std::bind(&TurbodriverInterface::getApsFault, this, sp::_1)) );
            faultMonitors.push_back( TurbodriverInterface::FaultMonitor(nodepath + " limit fault", std::bind(&TurbodriverInterface::getLimitFault, this, sp::_1)) );
            faultMonitors.push_back( TurbodriverInterface::FaultMonitor(nodepath + " encoder drift fault", std::bind(&TurbodriverInterface::getEncDriftFault, this, sp::_1)) );
            faultMonitors.push_back( TurbodriverInterface::FaultMonitor(nodepath + " jerk fault", std::bind(&TurbodriverInterface::getJerkFault, this, sp::_1)) );
            faultMonitors.push_back( TurbodriverInterface::FaultMonitor(nodepath + " joint fault", std::bind(&TurbodriverInterface::getJointFault, this, sp::_1)) );
            faultMonitors.push_back( TurbodriverInterface::FaultMonitor(nodepath + " checksum fault", std::bind(&TurbodriverInterface::getChecksumFault, this, sp::_1)) );
            faultMonitors.push_back( TurbodriverInterface::FaultMonitor(nodepath + " communication fault", std::bind(&TurbodriverInterface::getCommFault, this, sp::_1)) );
            faultMonitors.push_back( TurbodriverInterface::FaultMonitor(nodepath + " velocity fault", std::bind(&TurbodriverInterface::getVelocityFault, this, sp::_1)) );
            faultMonitors.push_back( TurbodriverInterface::FaultMonitor(nodepath + " APS 1 tolerance fault", std::bind(&TurbodriverInterface::getAps1TolFault, this, sp::_1)) );
            faultMonitors.push_back( TurbodriverInterface::FaultMonitor(nodepath + " APS 2 tolerance fault", std::bind(&TurbodriverInterface::getAps2TolFault, this, sp::_1)) );

            // Tare position stuff upon initialization
            tarePositionCommand();
            // Add some position command switching option in driver? How to declare this out in the open?
            // Read the positionCommand sensor feedback option?
        }

        virtual void updateStateData(ros::Time const& timeStamp)
        {
            float pos = 0.0;
            turboAPI.getPosition(pos);

            if ( fabs(pos - previousValue) < glitchFilterThreshold)
            {
                position_ = (double)pos;
            }
            else
            {
                std::string msg = "Filtered a glitch at node path " + nodePath;
                NasaCommonLogging::Logger::log("gov.nasa.TurbodriverInterface.updateStateData", log4cpp::Priority::WARN, msg);
            }

            previousValue = pos;

            float vel = 0.0;
            turboAPI.getVelocity(vel);
            velocity_ = (double)vel;

            float eff = 0.0;
            turboAPI.getEffort(eff);
            effort_   = (double)eff;
        }

        virtual void updateStateCommand(ros::Time const& timeStamp)
        {
            float posCom = (float)positionCommand_;
            turboAPI.setPositionCommand(posCom);

            float velCom = velocityCommand_;
            turboAPI.setVelocityCommand(velCom);

            float effortCom = (float)effortCommand_;
            turboAPI.setEffortCommand(effortCom);
        }

        virtual void updateModeData()
        {
            // Check for fault conditions
            for (auto& faultMonitor : faultMonitors) { faultMonitor(); }

            // Determine enableState:
            uint32_t jointFault = 0;
            turboAPI.getJointFault(jointFault);

            if ( !enabled_ )
            {
                enableState_ = EnableState::Disabled;
            }
            else if ( jointFault )
            {
                enableState_ = EnableState::Faulted;
            }
            else if ( !powerGood_ )
            {
                enableState_ = EnableState::NotReady;
            }
            else
            {
                enableState_ = EnableState::Ready;
            }

            // Determine enableState:
            uint32_t brakeRelease = 0;
            turboAPI.getBrakeRelease(brakeRelease);

            uint32_t motorDriverEnable = 0;
            turboAPI.getMotorEnable(motorDriverEnable);

            uint32_t motComSource = 0;
            turboAPI.getMotComSource(motComSource);

            uint32_t clearFault = 0;
            turboAPI.getClearFaultCmd(clearFault);

            uint32_t ctrlMode = 0;
            turboAPI.getControlModeCmd(ctrlMode);

            // Determine servo state:
            if (clearFault)
            {
                modeState_ = ModeState::Resetting;
            }
            else if ( !brakeRelease && !motorDriverEnable)
            {
                modeState_ = ModeState::Parked;
            }
            else if (brakeRelease && !motorDriverEnable)
            {
                modeState_ = ModeState::BrakeReleased;
            }
            else if ( brakeRelease && motorDriverEnable && motComSource)
            {
                switch (ctrlMode)
                {
                    case 0:
                        modeState_ = ModeState::CurrentMode;
                        break;

                    case 1:
                        modeState_ = ModeState::TorqueMode;
                        break;

                    case 2:
                        modeState_ = ModeState::PositionMode;
                        break;

                    case 3:
                        modeState_ = ModeState::VelocityMode;
                        break;

                    case 4:
                        modeState_ = ModeState::ImpedanceMode;

                    default:
                        modeState_ = ModeState::Undefined;
                }
            }
            else
            {
                modeState_ = ModeState::Undefined;
            }
        }

        void tarePositionCommand()
        {
            // Read in from shared memory, store in Axis's data
            float tarePos;
            turboAPI.getPosition(tarePos);
            positionCommand_ = (double)tarePos;
            // Write this value
            turboAPI.setPositionCommand(tarePos);
        }

    protected:

        // Pure virtual functions to be defined in generated API child classes
        //  These functions are called by updateModeData
        void getBrakeRelease(uint32_t& val) { turboAPI.getBrakeRelease(val);}
        void getMotorEnable(uint32_t& val) {turboAPI.getMotorEnable(val);}
        void getMotComSource(uint32_t& val) {turboAPI.getMotComSource(val);}
        void getClearFaultCmd(uint32_t& val) {turboAPI.getClearFaultCmd(val);}
        void getControlModeCmd(uint32_t& val) {turboAPI.getControlModeCmd(val);}
        void getApsFault(uint32_t& val) {turboAPI.getApsFault(val);}
        void getLimitFault(uint32_t& val) {turboAPI.getLimitFault(val);}
        void getEncDriftFault(uint32_t& val) {turboAPI.getEncDriftFault(val);}
        void getJerkFault(uint32_t& val) {turboAPI.getJerkFault(val);}
        void getJointFault(uint32_t& val) {turboAPI.getJointFault(val);}
        void getChecksumFault(uint32_t& val) {turboAPI.getChecksumFault(val);}
        void getCommFault(uint32_t& val) {turboAPI.getCommFault(val);}
        void getVelocityFault(uint32_t& val) {turboAPI.getVelocityFault(val);}
        void getAps1TolFault(uint32_t& val) {turboAPI.getAps1TolFault(val);}
        void getAps2TolFault(uint32_t& val) {turboAPI.getAps2TolFault(val);}

        // Pure virtual functions to be defined in generated API child classes
        //  These functions are called by updateModeCommand
        void park() { turboAPI.park(); }
        void clearFault() { turboAPI.clearFault();}
        //virtual void releaseBrake() = 0;
        //virtual void motorMode() = 0;
        void positionMode() { turboAPI.positionMode();}
        void torqueMode() { turboAPI.torqueMode();}

        // Wrapper function for low-level api servo call
        void servoTorqueMode()
        {
            // By wrapping the underlying servo command, we can protect low-level api calls based on the state of the actuator
            if ( enableState_ == EnableState::Ready)
            {
                turboAPI.torqueMode();
            }
        }
        // overloading disable defined in 'Axis' base class
        void disable()
        {
            BaseJointActuator::disable();
            turboAPI.park();
        }

        T turboAPI;
        float previousValue;
        float glitchFilterThreshold;
        std::string nodePath;

        SMT::SMTClient* client;

        struct FaultMonitor
        {
            typedef std::function<void(uint32_t&)> CheckFaultFunction_t;

            FaultMonitor(std::string const& errMsg, CheckFaultFunction_t func) :
                checkFaultFunc_(func), errorMessage_(errMsg), latchedValue_(0) {};

            void operator()()
            {
                uint32_t fault = 0;
                checkFaultFunc_(fault);

                // If fault occured, log and latch
                if ((fault > 0) and (latchedValue_ == 0))
                {
                    //ROS_ERROR_RT(errorMessage_);
                    latchedValue_ = fault;
                }

                // If fault has been reset, reset the latch
                if ((latchedValue_ > 0) and (fault == 0))
                {
                    latchedValue_ = 0;
                }
            }

        private:
            CheckFaultFunction_t checkFaultFunc_;
            std::string errorMessage_;
            uint32_t latchedValue_;
        };

        std::vector<FaultMonitor> faultMonitors;
    };

    template <class T>
    class TurbodriverSimInterface : public ActuatorInterface
    {
        static_assert(std::is_base_of< turbodriver_api::TurbodriverSimApiBase, T>::value, "T must inherit from TurbodriverSimApiBase");
    public:
        TurbodriverSimInterface() {}
        void initialize(const std::string& nodepath, SMT::SMTClient* smtClient)
        {
            turboAPI.Subscribe(nodepath, smtClient);
        }

        virtual void updateStateData(ros::Time const& timeStamp)
        {
            float posCom(0.0);
            turboAPI.getPosition(posCom);
            position_ = (double)posCom;

            float velCom(0.0);
            turboAPI.getVelocity(velCom);
            velocity_ = (double)velCom;

            float effortCom(0.0);
            turboAPI.getEffort(effortCom);
            effort_ = (double)effortCom;
        }

        virtual void updateStateCommand(ros::Time const& timeStamp)
        {
            float pos = (float)positionCommand_ ;
            turboAPI.setPositionCommand(pos);

            float vel = (float)velocityCommand_;
            turboAPI.setVelocityCommand(vel);

            float eff = (float)effortCommand_;
            turboAPI.setEffortCommand(eff);
        }

        virtual void setStateData(ros::Time const& timeStamp)
        {
            float pos = (float)position_;
            turboAPI.setPosition(pos);

            float vel = (float)velocity_;
            turboAPI.setVelocity(vel);

            float eff = (float)effort_;
            turboAPI.setEffort(eff);
        }

        virtual void getStateCommand(ros::Time const& timeStamp)
        {
            float posCom(0.0);
            turboAPI.getPositionCommand(posCom);
            positionCommand_ = (double)posCom;

            float velCom(0.0);
            turboAPI.getVelocityCommand(velCom);
            velocityCommand_ = (double)velCom;

            float effortCom(0.0);
            turboAPI.getEffortCommand(effortCom);
            effortCommand_ = (double)effortCom;
        }
        //void updateModeData();
    protected:
        T turboAPI;
    };

}
#endif // TURBODRIVER_API_TURBODRIVER_INTERFACE_HPP_
