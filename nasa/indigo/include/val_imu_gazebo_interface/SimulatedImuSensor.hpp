#ifndef __SIMULATED_IMU_INTERFACE_HPP__
#define __SIMULATED_IMU_INTERFACE_HPP__

#include "val_robot_interface/ImuSensorInterface.hpp"

namespace simulated_imu_sensor
{
    class SimulatedImuSensor : public imu_sensor_interface::ImuSensorInterface
    {
    public:
        SimulatedImuSensor()
        {

        }

        virtual ~SimulatedImuSensor()
        {

        }

        void initialize(const std::string& name, const std::string& nodePath, const std::string& devicePort)
        {
            subscribe(nodePath);
        }

    protected:
        void subscribe(const std::string& nodePath);

        void setImuFrameToZUpFrameQuaternion()
        {
            rotationFromImuFrameToZUpFrame = Eigen::Quaternion<double>(1.0, 0.0, 0.0, 0.0);

            return;
        }

        void stop()
        {
            // Nothing to be done to stop a sim IMU
            return;
        }

        void disconnect()
        {
            // Nothing to disconnect for a sim IMU
            return;
        }

        void getData();

        std::shared_ptr<SMT::Resource> subscriberAngularVelocityX;
        std::shared_ptr<SMT::Resource> subscriberAngularVelocityY;
        std::shared_ptr<SMT::Resource> subscriberAngularVelocityZ;
        std::shared_ptr<SMT::Resource> subscriberLinearAccelerationX;
        std::shared_ptr<SMT::Resource> subscriberLinearAccelerationY;
        std::shared_ptr<SMT::Resource> subscriberLinearAccelerationZ;
        std::shared_ptr<SMT::Resource> subscriberQuaternionW;
        std::shared_ptr<SMT::Resource> subscriberQuaternionX;
        std::shared_ptr<SMT::Resource> subscriberQuaternionY;
        std::shared_ptr<SMT::Resource> subscriberQuaternionZ;
    };

}
#endif // VAL_IMU_GAZEBO_INTERFACE_HPP_