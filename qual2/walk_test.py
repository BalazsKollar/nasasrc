#!/usr/bin/env python

import subprocess
import os
import time
import rospy
import tf
import tf2_ros
import numpy


from ihmc_msgs.msg import FootstepStatusRosMessage
from ihmc_msgs.msg import FootstepDataListRosMessage
from ihmc_msgs.msg import FootstepDataRosMessage
from ihmc_msgs.msg import FootTrajectoryRosMessage
from ihmc_msgs.msg import SE3TrajectoryPointRosMessage
from ihmc_msgs.msg import PelvisHeightTrajectoryRosMessage
from ihmc_msgs.msg import TrajectoryPoint1DRosMessage
from ihmc_msgs.msg import WholeBodyTrajectoryRosMessage


LEFT = 0
RIGHT = 1

ROBOT_NAME = None
LEFT_FOOT_FRAME_NAME = None
RIGHT_FOOT_FRAME_NAME = None

def walkTest():
    subprocess.call(os.environ.get('NASA_REPO_FOLDER') + "/qual2/push_button.py --mode=DOWN", shell=True)

    time.sleep(10)

    pelvisMsg = PelvisHeightTrajectoryRosMessage()
    pelvisMsg.execution_mode = 0
    pelvisMsg.unique_id = -1
    
    pelvisTraj = TrajectoryPoint1DRosMessage()
    pelvisTraj.time = 1.5
    pelvisTraj.position = 1.15
    pelvisTraj.velocity = 0.0
    
    pelvisMsg.trajectory_points.append(pelvisTraj)
    
    rospy.loginfo('setting pelvis height...')
    pelvisHeightTrajectoryPublisher.publish(pelvisMsg)


    
    time.sleep(7)

    msg = FootstepDataListRosMessage()
    msg.transfer_time = 0.30 #on two legs
    msg.swing_time = 0.55 #on one leg
    msg.execution_mode = 0
    msg.unique_id = -1
    
    # walk forward starting LEFT #0.40 transfer / 0.70 swing
#    msg.footstep_data_list.append(createFootStepOffset(LEFT, [0.52, 0.015, 0.0], 0.1))
#    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [1.04, -0.015, 0.0], 0.1))
#    msg.footstep_data_list.append(createFootStepOffset(LEFT, [1.56, 0.015, 0.0], 0.1))
#    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [2.08, -0.015, 0.0], 0.1))
#    msg.footstep_data_list.append(createFootStepOffset(LEFT, [2.60, 0.015, 0.0], 0.1))
#    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [3.10, -0.015, 0.0], 0.1))


#this is a walk sample. it doesn't take the initial small step before the red line
    msg.footstep_data_list.append(createFootStepOffset(LEFT, [0.6, 0.01, 0.0], 0.1))
    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [1.2, -0.01, 0.0], 0.1))
    msg.footstep_data_list.append(createFootStepOffset(LEFT, [1.8, 0.01, 0.0], 0.1))
    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [2.40, -0.01, 0.0], 0.1))
    msg.footstep_data_list.append(createFootStepOffset(LEFT, [3.00, 0.01, 0.0], 0.1))

    # walk with 0.3 transfer 0.7 swing, raised pelvis, big steps, small prestep
#    msg.footstep_data_list.append(createFootStepOffset(LEFT, [0.48, 0.01, 0.0], 0.1))
#    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [1.12, -0.01, 0.0], 0.1))
#    msg.footstep_data_list.append(createFootStepOffset(LEFT, [1.76, 0.01, 0.0], 0.1))
#    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [2.40, -0.01, 0.0], 0.1))
#    msg.footstep_data_list.append(createFootStepOffset(LEFT, [3.02, 0.01, 0.0], 0.1))
# arm needs to be tuned

#    msg.footstep_data_list.append(createFootStepOffset(LEFT, [0.48, 0.0, 0.0], 0.1))
#    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [1.12, 0.0, 0.0], 0.1))
#    msg.footstep_data_list.append(createFootStepOffset(LEFT, [1.76, 0.0, 0.0], 0.1))
#    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [2.40, 0.0, 0.0], 0.1))
#    msg.footstep_data_list.append(createFootStepOffset(LEFT, [3.02, 0.0, 0.0], 0.1))

#    msg.footstep_data_list.append(createFootStepOffset(LEFT, [3.0, 0.015, 0.0], 0.1))
#    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [3.10, -0.015, 0.0], 0.1))

    # walk forward taking 1 small step and 5 bigger ones
    # 0.55 transfer / 0.85 swing (0.55 / 0.75 didn't work)
#    msg.footstep_data_list.append(createFootStepOffset(LEFT, [0.25, 0.0, 0.0]))
#    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [0.80, 0.0, 0.0]))
#    msg.footstep_data_list.append(createFootStepOffset(LEFT, [1.35, 0.0, 0.0]))
#    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [1.90, 0.0, 0.0]))
#    msg.footstep_data_list.append(createFootStepOffset(LEFT, [2.45, 0.0, 0.0]))
#    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [3.0, 0.0, 0.0]))


    footStepListPublisher.publish(msg)
    rospy.loginfo('walk forward...')
    waitForFootsteps(len(msg.footstep_data_list))

    subprocess.call(os.environ.get('NASA_REPO_FOLDER') + "/qual2/push_button.py --mode=PUSH", shell=True)
    time.sleep(10)
    subprocess.call(os.environ.get('NASA_REPO_FOLDER') + "/qual2/push_button.py --mode=PULL", shell=True)
    time.sleep(10)
    msgb = FootstepDataListRosMessage()
    msgb.transfer_time = 0.60 #on two legs
    msgb.swing_time = 0.90 #on one leg
    msgb.execution_mode = 0
    msgb.unique_id = -3

    # walk with 0.3 transfer 0.7 swing, raised pelvis, big steps, small prestep
    msgb.footstep_data_list.append(createFootStepOffset(RIGHT, [1.14, -0.035, 0.0], 0.25))
    msgb.footstep_data_list.append(createFootStepOffset(LEFT, [1.0, 0.035, 0.0], 0.25))
    msgb.footstep_data_list.append(createFootStepOffset(RIGHT, [2.14, -0.035, 0.0], 0.25))
    msgb.footstep_data_list.append(createFootStepOffset(LEFT, [2.00, -0.035, 0.0], 0.25))

    footStepListPublisher.publish(msgb)
    rospy.loginfo('walk forward again...')
    waitForFootsteps(len(msgb.footstep_data_list))


    subprocess.call(os.environ.get('NASA_REPO_FOLDER') + "/qual2/push_button.py --mode=PUSH", shell=True)

    time.sleep(14)

    subprocess.call(os.environ.get('NASA_REPO_FOLDER') + "/qual2/push_button.py --mode=PULL", shell=True)

    time.sleep(10)

    msg2 = FootstepDataListRosMessage()
    msg2.transfer_time = 0.50 #on two legs
    msg2.swing_time = 0.80 #on one leg
    msg2.execution_mode = 0
    msg2.unique_id = -1
    
    msg2.footstep_data_list.append(createFootStepOffset(LEFT, [0.52, 0.025, 0.0], 0.2))
    msg2.footstep_data_list.append(createFootStepOffset(RIGHT, [0.52, -0.025, 0.0], 0.2))
    msg2.footstep_data_list.append(createFootStepOffset(LEFT, [1.56, 0.025, 0.0], 0.2))
    msg2.footstep_data_list.append(createFootStepOffset(RIGHT, [1.56, -0.025, 0.0], 0.2))
    msg2.footstep_data_list.append(createFootStepOffset(LEFT, [2.60, 0.025, 0.0], 0.2))
    msg2.footstep_data_list.append(createFootStepOffset(RIGHT, [2.60, -0.025, 0.0], 0.2))

    rospy.loginfo('walking again...')
    footStepListPublisher.publish(msg2)
    waitForFootsteps(len(msg2.footstep_data_list))
    rospy.loginfo('finished walking')
    

# by Balazs    
def createFootTrajectoryInPlace(stepSide):
    traj = SE3TrajectoryPointRosMessage()
    
    if stepSide == LEFT:
        foot_frame = LEFT_FOOT_FRAME_NAME
    else:
        foot_frame = RIGHT_FOOT_FRAME_NAME

    footWorld = tfBuffer.lookup_transform('world', foot_frame, rospy.Time())
    traj.orientation = footWorld.transform.rotation
    traj.position = footWorld.transform.translation

    return traj

# by Balazs
def createFootTrajectoryOffset(stepSide, offset):
    traj = createFootTrajectoryInPlace(stepSide)

    # transform the offset to world frame
    quat = traj.orientation
    rot = tf.transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
    transformedOffset = numpy.dot(rot[0:3, 0:3], offset)

    traj.time = 2.0

    traj.position.x += transformedOffset[0]
    traj.position.y += transformedOffset[1]
    traj.position.z += transformedOffset[2]
    
#    traj.linear_velocity.x = 1.0
#    traj.linear_velocity.y = 0.0
#    traj.linear_velocity.z = 1.0
#    traj.angular_velocity.x = 1.0
#    traj.angular_velocity.y = 0.0
#    traj.angular_velocity.z = 1.0

    return traj

# Creates footstep with the current position and orientation of the foot.
def createFootStepInPlace(stepSide, swingHeight):
    footstep = FootstepDataRosMessage()
    footstep.robot_side = stepSide
    footstep.swing_height = swingHeight
    
    if stepSide == LEFT:
        foot_frame = LEFT_FOOT_FRAME_NAME
    else:
        foot_frame = RIGHT_FOOT_FRAME_NAME

    footWorld = tfBuffer.lookup_transform('world', foot_frame, rospy.Time())
    footstep.orientation = footWorld.transform.rotation
    footstep.location = footWorld.transform.translation

    return footstep

# Creates footstep offset from the current foot position. The offset is in foot frame.
def createFootStepOffset(stepSide, offset, swingHeight):
    footstep = createFootStepInPlace(stepSide, swingHeight)

    # transform the offset to world frame
    quat = footstep.orientation
    rot = tf.transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
    transformedOffset = numpy.dot(rot[0:3, 0:3], offset)

    footstep.location.x += transformedOffset[0]
    footstep.location.y += transformedOffset[1]
    footstep.location.z += transformedOffset[2]
    
    footstep.trajectory_type = FootstepDataRosMessage.OBSTACLE_CLEARANCE

    return footstep

def waitForFootsteps(numberOfSteps):
    global stepCounter
    stepCounter = 0
    while stepCounter < numberOfSteps:
        rate.sleep()

    rospy.loginfo('finished set of steps')

def recievedFootStepStatus(msg):
    global stepCounter
    if msg.status == 1:
        stepCounter += 1

if __name__ == '__main__':
    try:
        rospy.init_node('ihmc_walk_test')

        if not rospy.has_param('/ihmc_ros/robot_name'):
            rospy.logerr("Cannot run walk_test.py, missing parameters!")
            rospy.logerr("Missing parameter '/ihmc_ros/robot_name'")

        else:
            ROBOT_NAME = rospy.get_param('/ihmc_ros/robot_name')

            right_foot_frame_parameter_name = "/ihmc_ros/{0}/right_foot_frame_name".format(ROBOT_NAME)
            left_foot_frame_parameter_name = "/ihmc_ros/{0}/left_foot_frame_name".format(ROBOT_NAME)

            if rospy.has_param(right_foot_frame_parameter_name) and rospy.has_param(left_foot_frame_parameter_name):
                RIGHT_FOOT_FRAME_NAME = rospy.get_param(right_foot_frame_parameter_name)
                LEFT_FOOT_FRAME_NAME = rospy.get_param(left_foot_frame_parameter_name)

                footStepStatusSubscriber = rospy.Subscriber("/ihmc_ros/{0}/output/footstep_status".format(ROBOT_NAME), FootstepStatusRosMessage, recievedFootStepStatus)
                footStepListPublisher = rospy.Publisher("/ihmc_ros/{0}/control/footstep_list".format(ROBOT_NAME), FootstepDataListRosMessage, queue_size=20)
                footTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/foot_trajectory".format(ROBOT_NAME), FootTrajectoryRosMessage, queue_size=20)
                pelvisHeightTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/pelvis_height_trajectory".format(ROBOT_NAME), PelvisHeightTrajectoryRosMessage, queue_size=20)
                wholeBodyTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/whole_body_trajectory".format(ROBOT_NAME), WholeBodyTrajectoryRosMessage, queue_size=50)

                tfBuffer = tf2_ros.Buffer()
                tfListener = tf2_ros.TransformListener(tfBuffer)

                rate = rospy.Rate(10) # 10hz
                time.sleep(1)

                # make sure the simulation is running otherwise wait
                if footStepListPublisher.get_num_connections() == 0:
                    rospy.loginfo('waiting for subsciber...')
                    while footStepListPublisher.get_num_connections() == 0:
                        rate.sleep()

                if not rospy.is_shutdown():
                    walkTest()
            else:
                if not rospy.has_param(left_foot_frame_parameter_name):
                    rospy.logerr("Missing parameter {0}".format(left_foot_frame_parameter_name))
                if not rospy.has_param(right_foot_frame_parameter_name):
                    rospy.logerr("Missing parameter {0}".format(right_foot_frame_parameter_name))

    except rospy.ROSInterruptException:
        pass
