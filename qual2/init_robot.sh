#!/bin/bash

echo "wait $SYSTEM_LOAD_SLEEP seconds for system to load"
sleep $SYSTEM_LOAD_SLEEP

echo "lower harness"
rostopic pub -1 /valkyrie/harness/velocity std_msgs/Float32 '{data: -0.05}'

echo "wait $HARNESS_LOWER_SLEEP seconds for the harness to descend"
sleep $HARNESS_LOWER_SLEEP

echo "low level control starting"
rostopic pub -1 /ihmc_ros/valkyrie/control/low_level_control_mode ihmc_valkyrie_ros/ValkyrieLowLevelControlModeRosMessage '{requested_control_mode: 2, unique_id: -1}'

echo "detach in $HARNESS_DETACH_SLEEP seconds"
sleep $HARNESS_DETACH_SLEEP
rostopic pub -1 /valkyrie/harness/detach std_msgs/Bool true &

if [ $1 = "true" ]; then
  echo "wait $GAIN_BALANCE_SLEEP seconds before starting to walk"
  sleep $GAIN_BALANCE_SLEEP
  echo "start walking"
  rosrun srcsim walk_test.py
fi

#echo "reached door"
#echo "waiting $FINALIZE_POSTURE_SLEEP seconds to finalize posture"
#sleep $FINALIZE_POSTURE_SLEEP

#echo "pushing button"
#python $NASA_REPO_FOLDER/qual2/push_button.py

#echo "arm signals sent"

echo "init_robot.sh finished"
