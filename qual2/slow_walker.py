#!/usr/bin/env python

import subprocess
import os
import time
import rospy
import tf
import tf2_ros
import numpy

import copy
import sys
import getopt

from numpy import append

from ihmc_msgs.msg import ArmTrajectoryRosMessage
from ihmc_msgs.msg import OneDoFJointTrajectoryRosMessage
from ihmc_msgs.msg import TrajectoryPoint1DRosMessage



from ihmc_msgs.msg import FootstepStatusRosMessage
from ihmc_msgs.msg import FootstepDataListRosMessage
from ihmc_msgs.msg import FootstepDataRosMessage
from ihmc_msgs.msg import FootTrajectoryRosMessage
from ihmc_msgs.msg import SE3TrajectoryPointRosMessage
from ihmc_msgs.msg import PelvisHeightTrajectoryRosMessage
from ihmc_msgs.msg import TrajectoryPoint1DRosMessage
from ihmc_msgs.msg import WholeBodyTrajectoryRosMessage




LEFT = 0
RIGHT = 1

ROBOT_NAME = None
LEFT_FOOT_FRAME_NAME = None
RIGHT_FOOT_FRAME_NAME = None


LEG_OFFSET = 0.02
SWING_HEIGHT = 0.1
PRE_STEP = 0.40
STEP_DIST = 0.44

def walkTest():
    subprocess.call(os.environ.get('NASA_REPO_FOLDER') + "/qual2/push_button.py --mode=DOWN", shell=True)

    time.sleep(10)
    subprocess.call(os.environ.get('NASA_REPO_FOLDER') + "/qual2/push_button.py --mode=PUSH", shell=True)
    time.sleep(10)


    pelvisMsg = PelvisHeightTrajectoryRosMessage()
    pelvisMsg.execution_mode = 0
    pelvisMsg.unique_id = -1
    
    pelvisTraj = TrajectoryPoint1DRosMessage()
    pelvisTraj.time = 1.5
    pelvisTraj.position = 1.15
    pelvisTraj.velocity = 0.0
    
    pelvisMsg.trajectory_points.append(pelvisTraj)
    
    rospy.loginfo('setting pelvis height...')
    pelvisHeightTrajectoryPublisher.publish(pelvisMsg)
    
    time.sleep(7)

    #subprocess.call(os.environ.get('NASA_REPO_FOLDER') + "/qual2/push_button.py --mode=PUSH", shell=True)

    msg = FootstepDataListRosMessage()
    msg.transfer_time = 0.30 #on two legs
    msg.swing_time = 0.5 #on one leg
    msg.execution_mode = 0
    msg.unique_id = -1
    
    # walk forward starting LEFT
    num_steps = 7
    xpos = numpy.arange(num_steps)*STEP_DIST + PRE_STEP
    print "Next step sequence:", xpos
    for step in range(num_steps):
        foot = LEFT if step % 2 == 0 else RIGHT
        msg.footstep_data_list.append(createFootStepOffset(foot, [xpos[step], -foot*LEG_OFFSET, 0.0], SWING_HEIGHT))

    #msg.footstep_data_list.append(createFootStepOffset(RIGHT, [3.15, -0.01, 0.0], 0.1))


    footStepListPublisher.publish(msg)
    rospy.loginfo('walk forward...')
    waitForFootsteps(len(msg.footstep_data_list))


    rospy.loginfo("ready to press button")
    #subprocess.call(os.environ.get('NASA_REPO_FOLDER') + "/qual2/push_button.py --mode=DOWN", shell=True)
    #moveArmBack()
    #rospy.sleep(rospy.Duration.from_sec(1))
    #time.sleep(3)

    time.sleep(3)

    subprocess.call(os.environ.get('NASA_REPO_FOLDER') + "/qual2/push_button.py --mode=RIGHTDOWN", shell=True)
    #time.sleep(2)

    time.sleep(6)
    rospy.loginfo("door should be open")

    # Take additional steps
    msg = FootstepDataListRosMessage()
    msg.transfer_time = 0.30 #on two legs
    msg.swing_time = 0.50 #on one leg
    msg.execution_mode = 0
    msg.unique_id = -1

    rospy.loginfo("Walking through the door...")
    xpos = numpy.arange(num_steps*2)*STEP_DIST + PRE_STEP
    print "Next step sequence:", xpos
    print "Next foot:", ("LEFT" if range(num_steps,num_steps*2)[0] == 0 else "RIGHT")

    for step in range(num_steps,num_steps*2):
        foot = LEFT if step % 2 == 0 else RIGHT
        print "foot", foot, "going to xpos", xpos[step]
        msg.footstep_data_list.append(createFootStepOffset(foot, [xpos[step], -foot*LEG_OFFSET, 0.0], SWING_HEIGHT))
    footStepListPublisher.publish(msg)

    waitForFootsteps(len(msg.footstep_data_list))
    rospy.loginfo('finished walking')


# by Balazs    
def createFootTrajectoryInPlace(stepSide):
    traj = SE3TrajectoryPointRosMessage()
    
    if stepSide == LEFT:
        foot_frame = LEFT_FOOT_FRAME_NAME
    else:
        foot_frame = RIGHT_FOOT_FRAME_NAME

    footWorld = tfBuffer.lookup_transform('world', foot_frame, rospy.Time())
    traj.orientation = footWorld.transform.rotation
    traj.position = footWorld.transform.translation

    return traj

# by Balazs
def createFootTrajectoryOffset(stepSide, offset):
    traj = createFootTrajectoryInPlace(stepSide)

    # transform the offset to world frame
    quat = traj.orientation
    rot = tf.transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
    transformedOffset = numpy.dot(rot[0:3, 0:3], offset)

    traj.time = 2.0

    traj.position.x += transformedOffset[0]
    traj.position.y += transformedOffset[1]
    traj.position.z += transformedOffset[2]
    
#    traj.linear_velocity.x = 1.0
#    traj.linear_velocity.y = 0.0
#    traj.linear_velocity.z = 1.0
#    traj.angular_velocity.x = 1.0
#    traj.angular_velocity.y = 0.0
#    traj.angular_velocity.z = 1.0

    return traj

# Creates footstep with the current position and orientation of the foot.
def createFootStepInPlace(stepSide, swingHeight):
    footstep = FootstepDataRosMessage()
    footstep.robot_side = stepSide
    footstep.swing_height = swingHeight
    
    if stepSide == LEFT:
        foot_frame = LEFT_FOOT_FRAME_NAME
    else:
        foot_frame = RIGHT_FOOT_FRAME_NAME

    footWorld = tfBuffer.lookup_transform('world', foot_frame, rospy.Time())
    footstep.orientation = footWorld.transform.rotation
    footstep.location = footWorld.transform.translation

    return footstep

# Creates footstep offset from the current foot position. The offset is in foot frame.
def createFootStepOffset(stepSide, offset, swingHeight):
    footstep = createFootStepInPlace(stepSide, swingHeight)

    # transform the offset to world frame
    quat = footstep.orientation
    rot = tf.transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
    transformedOffset = numpy.dot(rot[0:3, 0:3], offset)

    footstep.location.x += transformedOffset[0]
    footstep.location.y += transformedOffset[1]
    footstep.location.z += transformedOffset[2]
    
    footstep.trajectory_type = FootstepDataRosMessage.PUSH_RECOVERY

    return footstep

def waitForFootsteps(numberOfSteps):
    global stepCounter
    stepCounter = 0
    while stepCounter < numberOfSteps:
        rate.sleep()

    rospy.loginfo('finished set of steps')

def recievedFootStepStatus(msg):
    global stepCounter
    if msg.status == 1:
        stepCounter += 1

if __name__ == '__main__':
    try:
        rospy.init_node('ihmc_walk_test')

        if not rospy.has_param('/ihmc_ros/robot_name'):
            rospy.logerr("Cannot run walk_test.py, missing parameters!")
            rospy.logerr("Missing parameter '/ihmc_ros/robot_name'")

        else:
            ROBOT_NAME = rospy.get_param('/ihmc_ros/robot_name')

            right_foot_frame_parameter_name = "/ihmc_ros/{0}/right_foot_frame_name".format(ROBOT_NAME)
            left_foot_frame_parameter_name = "/ihmc_ros/{0}/left_foot_frame_name".format(ROBOT_NAME)

            if rospy.has_param(right_foot_frame_parameter_name) and rospy.has_param(left_foot_frame_parameter_name):
                RIGHT_FOOT_FRAME_NAME = rospy.get_param(right_foot_frame_parameter_name)
                LEFT_FOOT_FRAME_NAME = rospy.get_param(left_foot_frame_parameter_name)

                footStepStatusSubscriber = rospy.Subscriber("/ihmc_ros/{0}/output/footstep_status".format(ROBOT_NAME), FootstepStatusRosMessage, recievedFootStepStatus)
                footStepListPublisher = rospy.Publisher("/ihmc_ros/{0}/control/footstep_list".format(ROBOT_NAME), FootstepDataListRosMessage, queue_size=20)
                footTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/foot_trajectory".format(ROBOT_NAME), FootTrajectoryRosMessage, queue_size=20)
                pelvisHeightTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/pelvis_height_trajectory".format(ROBOT_NAME), PelvisHeightTrajectoryRosMessage, queue_size=20)
                wholeBodyTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/whole_body_trajectory".format(ROBOT_NAME), WholeBodyTrajectoryRosMessage, queue_size=50)


                # armTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/arm_trajectory".format(ROBOT_NAME), ArmTrajectoryRosMessage, queue_size=1)

                tfBuffer = tf2_ros.Buffer()
                tfListener = tf2_ros.TransformListener(tfBuffer)

                rate = rospy.Rate(10) # 10hz
                time.sleep(1)

                # make sure the simulation is running otherwise wait
                if footStepListPublisher.get_num_connections() == 0:
                    rospy.loginfo('waiting for subsciber...')
                    while footStepListPublisher.get_num_connections() == 0:
                        rate.sleep()

                if not rospy.is_shutdown():
                    walkTest()
            else:
                if not rospy.has_param(left_foot_frame_parameter_name):
                    rospy.logerr("Missing parameter {0}".format(left_foot_frame_parameter_name))
                if not rospy.has_param(right_foot_frame_parameter_name):
                    rospy.logerr("Missing parameter {0}".format(right_foot_frame_parameter_name))

    except rospy.ROSInterruptException:
        pass
