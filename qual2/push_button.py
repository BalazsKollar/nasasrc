#!/usr/bin/env python

import copy
import time
import rospy
import sys
import getopt

from numpy import append

from ihmc_msgs.msg import ArmTrajectoryRosMessage
from ihmc_msgs.msg import OneDoFJointTrajectoryRosMessage
from ihmc_msgs.msg import TrajectoryPoint1DRosMessage


ZERO_VECTOR = [0.3, 1.3, -0.1, 1.3, 0.0, 0.0, 0.0]
ARM_PULLED_BACK = [-0.2, 1.4, 0.6, 1.3, 0.0, 0.0, 0.0]
ARM_PULLED_BACK_LEFT = [0.2, -1.45, 0.1, -1.65, 0.0, 0.0, 0.0]
ARM_PULLED_BACK_INIT = [0.9, 1.4, -0.1, 0.0, 0.0, 0.0, 0.0]
ARM_PULLED_BACK_INIT2 = [0.3, 1.3, -0.1, 0.65, 0.0, 0.0, 0.0]
ELBOW_BENT_UP = [1.00, -1.0, -1.0, 1.0, -0.0, 0.0, -0.1]
ELBOW_BENT_UP_MIDDLE = [0.9, 0.3, 0.6, 1.3, 0.0, 0.0, 0.0]
#ELBOW_BENT_UP_MIDDLE = [-0.1, 0.4, 0.56, 1.7, 0.0, 0.0, 0.0]
#touch ELBOW_BENT_UP = [1.8, -0.3, -1.5, 1.15, 0.0, 0.0, 0.0]
ELBOW_BENT_UP_INIT = [1.8, 0.4, -1.5, 0.95, 0.0, 0.0, 0.0]

ARM_DOWN_LEFT = [0.16, -1.4, 0.56, -1.7, 0.0, 0.0, 0.0]
ARM_DOWN_RIGHT = [0.16, 1.4, 0.56, 1.7, 0.0, 0.0, 0.0]
ARM_DOWN_STRETCHED_LEFT = [0.0, -1.4, 1.4, 0.0, 0.0, 0.0, 0.0]
ARM_DOWN_STRETCHED_RIGHT = [0.0, 1.4, 1.4, 0.0, 0.0, 0.0, 0.0]


#ELBOW_BENT_UP2 = [-0.3, 0.4, 0.1, 0.90, 0.0, 0.0, 0.0]
#ELBOW_BENT_UP = [-0.3, 0.4, 0.2, 1.05, 0.0, 0.0, 0.0]
# successful: ELBOW_BENT_UP = [-0.3, 0.4, 0.1, 0.8, 0.0, 0.0, 0.0]
# closest: ELBOW_BENT_UP = [-0.3, 0.5, 0.3, 0.9, 0.0, 0.0, 0.0]
# 2nd closest yet: ELBOW_BENT_UP = [0.0, 0.1, 0.3, 0.9, 0.0, 0.0, 0.1]
# ? ? rotate_shoulder_upper_arm_stays


WIND_UP = [2.0, -.8, -1.2, 0,0,0,0]
ARM_STRAIGHT_BACK = [2.0, .8, -1.2, 0,0,0,0]
ARM_WAY_BACK = [2.0, .8, 1.2, 1.0,0,0,0]


ROBOT_NAME = None

def sendRightArmTrajectory():
    
    if mode == 'PUSH':
        msg = ArmTrajectoryRosMessage()
        msg.robot_side = ArmTrajectoryRosMessage.RIGHT
#        msg = appendTrajectoryPoint(msg, 0.5, ELBOW_BENT_UP_MIDDLE)
        msg = appendTrajectoryPoint(msg, 1.2, ELBOW_BENT_UP)
        msg.unique_id = -1
        rospy.loginfo('publishing right PUSH trajectory')
        armTrajectoryPublisher.publish(msg)

    elif mode == 'PULL':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 0.5, ARM_PULLED_BACK_INIT2)
        msg2 = appendTrajectoryPoint(msg2, 1.0, ARM_DOWN_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right PULL trajectory')
        armTrajectoryPublisher.publish(msg2)
        
        msg3 = ArmTrajectoryRosMessage()
        msg3.robot_side = ArmTrajectoryRosMessage.LEFT
        msg3 = appendTrajectoryPoint(msg3, 0.6, ARM_DOWN_LEFT)
        msg3.unique_id = -2
        rospy.loginfo('publishing left PULL trajectory')
        armTrajectoryPublisher.publish(msg3)

    elif mode == 'DOWN':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 1.0, ARM_DOWN_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right DOWN trajectory')
        armTrajectoryPublisher.publish(msg2)

        time.sleep(2)
        
        msg3 = ArmTrajectoryRosMessage()
        msg3.robot_side = ArmTrajectoryRosMessage.LEFT
        msg3 = appendTrajectoryPoint(msg3, 1.0, ARM_DOWN_LEFT)
        msg3.unique_id = -2
        rospy.loginfo('publishing left DOWN trajectory')
        armTrajectoryPublisher.publish(msg3)

    elif mode == 'RIGHTDOWN':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 1.0, ARM_DOWN_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right DOWN trajectory')
        armTrajectoryPublisher.publish(msg2)


def appendTrajectoryPoint(arm_trajectory, time, positions):
    if not arm_trajectory.joint_trajectory_messages:
        arm_trajectory.joint_trajectory_messages = [copy.deepcopy(OneDoFJointTrajectoryRosMessage()) for i in range(len(positions))]
    for i, pos in enumerate(positions):
        point = TrajectoryPoint1DRosMessage()
        point.time = time
        point.position = pos
        point.velocity = 0
        arm_trajectory.joint_trajectory_messages[i].trajectory_points.append(point)
    return arm_trajectory

if __name__ == '__main__':
    mode = 'PUSH'
    try:
        opts, args = getopt.getopt(sys.argv[1:],"m",["mode="])
    except getopt.GetoptError:
        print 'push_button.py --mode=[PUSH/PULL]'
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-m", "--mode"):
            mode = arg
    try:
        rospy.init_node('ihmc_arm_demo1')

        ROBOT_NAME = rospy.get_param('/ihmc_ros/robot_name')

        armTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/arm_trajectory".format(ROBOT_NAME), ArmTrajectoryRosMessage, queue_size=1)

        rate = rospy.Rate(10) # 10hz
        time.sleep(1)

        # make sure the simulation is running otherwise wait
        if armTrajectoryPublisher.get_num_connections() == 0:
            rospy.loginfo('waiting for subscriber...')
            while armTrajectoryPublisher.get_num_connections() == 0:
                rate.sleep()

        if not rospy.is_shutdown():
            sendRightArmTrajectory()
            time.sleep(2)

    except rospy.ROSInterruptException:
        pass
