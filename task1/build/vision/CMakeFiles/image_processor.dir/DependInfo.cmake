# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/nasasrc/task1/src/vision/image_processor.cpp" "/nasasrc/task1/build/vision/CMakeFiles/image_processor.dir/image_processor.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "EIGEN_USE_NEW_STDVECTOR"
  "EIGEN_YES_I_KNOW_SPARSE_MODULE_IS_NOT_STABLE_YET"
  "FLANN_STATIC"
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"vision\""
  "qh_QHpointer"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/nasasrc/task1/build/vision_opencv/cv_bridge/src/CMakeFiles/cv_bridge.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/vtk-5.8"
  "/nasasrc/task1/src/vision/include"
  "/nasasrc/task1/src/vision_opencv/cv_bridge/include"
  "/opt/ros/indigo/include"
  "/usr/local/include"
  "/usr/local/include/opencv"
  "/usr/include/pcl-1.7"
  "/usr/include/eigen3"
  "/usr/include/ni"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
