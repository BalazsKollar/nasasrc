# CMake generated Testfile for 
# Source directory: /nasasrc/task1/src/image_pipeline/image_proc/test
# Build directory: /nasasrc/task1/build/image_pipeline/image_proc/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(_ctest_image_proc_rostest_test_test_rectify.xml "/nasasrc/task1/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/indigo/share/catkin/cmake/test/run_tests.py" "/nasasrc/task1/build/test_results/image_proc/rostest-test_test_rectify.xml" "--return-code" "/opt/ros/indigo/share/rostest/cmake/../../../bin/rostest --pkgdir=/nasasrc/task1/src/image_pipeline/image_proc --package=image_proc --results-filename test_test_rectify.xml --results-base-dir \"/nasasrc/task1/build/test_results\" /nasasrc/task1/src/image_pipeline/image_proc/test/test_rectify.xml ")
