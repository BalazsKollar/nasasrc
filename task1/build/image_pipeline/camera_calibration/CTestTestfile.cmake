# CMake generated Testfile for 
# Source directory: /nasasrc/task1/src/image_pipeline/camera_calibration
# Build directory: /nasasrc/task1/build/image_pipeline/camera_calibration
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(_ctest_camera_calibration_nosetests_test.directed.py "/nasasrc/task1/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/indigo/share/catkin/cmake/test/run_tests.py" "/nasasrc/task1/build/test_results/camera_calibration/nosetests-test.directed.py.xml" "--return-code" "/usr/bin/cmake -E make_directory /nasasrc/task1/build/test_results/camera_calibration" "/usr/bin/nosetests-2.7 -P --process-timeout=60 /nasasrc/task1/src/image_pipeline/camera_calibration/test/directed.py --with-xunit --xunit-file=/nasasrc/task1/build/test_results/camera_calibration/nosetests-test.directed.py.xml")
ADD_TEST(_ctest_camera_calibration_nosetests_test.multiple_boards.py "/nasasrc/task1/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/indigo/share/catkin/cmake/test/run_tests.py" "/nasasrc/task1/build/test_results/camera_calibration/nosetests-test.multiple_boards.py.xml" "--return-code" "/usr/bin/cmake -E make_directory /nasasrc/task1/build/test_results/camera_calibration" "/usr/bin/nosetests-2.7 -P --process-timeout=60 /nasasrc/task1/src/image_pipeline/camera_calibration/test/multiple_boards.py --with-xunit --xunit-file=/nasasrc/task1/build/test_results/camera_calibration/nosetests-test.multiple_boards.py.xml")
