#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
    DESTDIR_ARG="--root=$DESTDIR"
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/nasasrc/task1/src/vision_opencv/image_geometry"

# snsure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/nasasrc/task1/install/lib/python2.7/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/nasasrc/task1/install/lib/python2.7/dist-packages:/nasasrc/task1/build/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/nasasrc/task1/build" \
    "/usr/bin/python" \
    "/nasasrc/task1/src/vision_opencv/image_geometry/setup.py" \
    build --build-base "/nasasrc/task1/build/vision_opencv/image_geometry" \
    install \
    $DESTDIR_ARG \
    --install-layout=deb --prefix="/nasasrc/task1/install" --install-scripts="/nasasrc/task1/install/bin"
