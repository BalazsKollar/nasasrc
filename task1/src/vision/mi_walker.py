#!/usr/bin/env python

from collections import OrderedDict
from math import cos, radians, sin, sqrt
import select
import sys
import termios
import tty

from geometry_msgs.msg import Quaternion, Transform, Vector3

from ihmc_msgs.msg import AbortWalkingRosMessage
from ihmc_msgs.msg import ArmTrajectoryRosMessage
from ihmc_msgs.msg import FootstepDataListRosMessage
from ihmc_msgs.msg import FootstepDataRosMessage
from ihmc_msgs.msg import FootstepStatusRosMessage
from ihmc_msgs.msg import HeadTrajectoryRosMessage
from ihmc_msgs.msg import NeckTrajectoryRosMessage
from ihmc_msgs.msg import OneDoFJointTrajectoryRosMessage
from ihmc_msgs.msg import SO3TrajectoryPointRosMessage
from ihmc_msgs.msg import PelvisHeightTrajectoryRosMessage
from ihmc_msgs.msg import TrajectoryPoint1DRosMessage

import numpy

import rospy

from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import MultiArrayDimension

from tf.transformations import euler_from_quaternion, quaternion_from_euler, quaternion_matrix
import tf2_ros


class Walker(object):

    # constants used for walking
    LEFT_FOOT_FRAME_NAME = None
    RIGHT_FOOT_FRAME_NAME = None
    TRANS_STEP = 0.4  # distance of each step (metres)
    ROT_STEP = radians(45)  # each rotation will be 45degrees

    def receivedFootStepStatus_cb(self, msg):
        if msg.status == FootstepStatusRosMessage.COMPLETED:
            self.footstep_count += 1

    def __init__(self):
        self.joint_values = {
            'left arm': [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            'right arm': [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            'left hand': [0.0, 0.0, 0.0, 0.0, 0.0],
            'right hand': [0.0, 0.0, 0.0, 0.0, 0.0],
            'head': [0.0, 0.0, 0.0],
            'neck': [0.0, 0.0, 0.0],
        }

    def init(self,buffername):

        self.tfBuffer = buffername
        self.footstep_count = 0
        self.status = "0:waiting"
        # create publisher for arm trajectories
        robot_name = rospy.get_param('/ihmc_ros/robot_name')
        self.neck_publisher = rospy.Publisher(
            '/ihmc_ros/{0}/control/neck_trajectory'.format(robot_name),
            NeckTrajectoryRosMessage, queue_size=1)
        self.head_publisher = rospy.Publisher(
            '/ihmc_ros/{0}/control/head_trajectory'.format(robot_name),
            HeadTrajectoryRosMessage, queue_size=1)
        self.footstep_publisher = rospy.Publisher(
            '/ihmc_ros/{0}/control/footstep_list'.format(robot_name),
            FootstepDataListRosMessage, queue_size=1)
        self.footstep_status_subscriber = rospy.Subscriber(
            '/ihmc_ros/{0}/output/footstep_status'.format(robot_name),
            FootstepStatusRosMessage, self.receivedFootStepStatus_cb)
        self.abort_walking_publisher = rospy.Publisher(
            '/ihmc_ros/{0}/control/abort_walking'.format(robot_name),
            AbortWalkingRosMessage, queue_size=1)
        self.pelvisHeightTrajectoryPublisher = rospy.Publisher(
            "/ihmc_ros/{0}/control/pelvis_height_trajectory".format(robot_name),
            PelvisHeightTrajectoryRosMessage, queue_size=20)
        right_foot_frame_parameter_name = "/ihmc_ros/{0}/right_foot_frame_name".format(robot_name)
        left_foot_frame_parameter_name = "/ihmc_ros/{0}/left_foot_frame_name".format(robot_name)
        if rospy.has_param(right_foot_frame_parameter_name) and \
                rospy.has_param(left_foot_frame_parameter_name):
            self.RIGHT_FOOT_FRAME_NAME = rospy.get_param(right_foot_frame_parameter_name)
            self.LEFT_FOOT_FRAME_NAME = rospy.get_param(left_foot_frame_parameter_name)
        # make sure the simulation is running otherwise wait
        self.rate = rospy.Rate(2)  # 2hz
        publishers = [ self.neck_publisher, self.head_publisher, self.footstep_publisher]
        if any([p.get_num_connections() == 0 for p in publishers]):
            while any([p.get_num_connections() == 0 for p in publishers]):
                rospy.loginfo('waiting for subscribers: ' + ', '.join(sorted([
                    p.name for p in publishers if p.get_num_connections() == 0])))
                self.rate.sleep()
        self.set_init_pose()
        self.raise_pelvis()
        self.rate.sleep()
        self.look_down()
        self.rate.sleep()
        rospy.loginfo('ready for walking directions')

    def _update_head_neck(self):
        msgN = NeckTrajectoryRosMessage()
        msgN.unique_id = -1
        self._append_trajectory_point_1d(msgN, 0.5, self.joint_values['neck'])
        self.neck_publisher.publish(msgN)
        msgH = HeadTrajectoryRosMessage()
        msgH.unique_id = -1
        self._append_trajectory_point_so3(msgH, 0.5, self.joint_values['head'])
        self.head_publisher.publish(msgH)

    def look_straight(self):
        self.joint_values['head'] = [0.0, 0.0, 0.0]
        self.joint_values['neck'] = [0.0, 0.0, 0.0]
        self._update_head_neck()

    def look_down(self):
        self.joint_values['head'] = [0.0, 0.2, 0.0]
        self.joint_values['neck'] = [0.4, 0.0, 0.0]
        self._update_head_neck()

    def walk_distance(self, target):
        direction = 1.0 if target > 0.0 else -1.0
        self.footstep_count = 0.0
        assert direction > 0 # TODO: walking backwards
        self.loginfo("walking a distance of %0.1f meters" % target)
        msg = self.getEmptyFootsetListMsg()
        total = 0.0
        foot_ix = 0
        feet = [FootstepDataRosMessage.LEFT, FootstepDataRosMessage.RIGHT]
        feet_together = True
        MAX_STEPS = 100
        for i in range(MAX_STEPS):
            if self.TRANS_STEP + total >= target:
                # end of trajectory
                offset = [target, 0.0, 0.0]
                msg.footstep_data_list.append(self.createTranslationFootStepOffset(feet[foot_ix], offset))
                self.loginfo("STEP %r OFFSET %r" % (feet[foot_ix], offset))
                foot_ix = (foot_ix+1)%2
                msg.footstep_data_list.append(self.createTranslationFootStepOffset(feet[foot_ix], offset))
                self.loginfo("STEP %r OFFSET %r" % (feet[foot_ix], offset))
                break
            else:
                total += self.TRANS_STEP
                offset = [total, 0.0, 0.0]
                msg.footstep_data_list.append(self.createTranslationFootStepOffset(feet[foot_ix], offset))
                self.loginfo("STEP %r OFFSET %r" % (feet[foot_ix], offset))
                foot_ix = (foot_ix+1)%2
        res = self.execute_footsteps(msg)
        if res:
            self.loginfo('done walking')
            return

    def createRotationFootStepList(self, yaw):
        left_footstep = FootstepDataRosMessage()
        left_footstep.robot_side = FootstepDataRosMessage.LEFT
        right_footstep = FootstepDataRosMessage()
        right_footstep.robot_side = FootstepDataRosMessage.RIGHT

        left_foot_world = self.tfBuffer.lookup_transform(
            'world', self.LEFT_FOOT_FRAME_NAME, rospy.Time())
        right_foot_world = self.tfBuffer.lookup_transform(
            'world', self.RIGHT_FOOT_FRAME_NAME, rospy.Time())
        intermediate_transform = Transform()
        # create a virtual fram between the feet, this will be the center of the rotation
        intermediate_transform.translation.x = (
            left_foot_world.transform.translation.x + right_foot_world.transform.translation.x)/2.
        intermediate_transform.translation.y = (
            left_foot_world.transform.translation.y + right_foot_world.transform.translation.y)/2.
        intermediate_transform.translation.z = (
            left_foot_world.transform.translation.z + right_foot_world.transform.translation.z)/2.
        # here we assume that feet have the same orientation so we can pick arbitrary left or right
        intermediate_transform.rotation = left_foot_world.transform.rotation

        left_footstep.location = left_foot_world.transform.translation
        right_footstep.location = right_foot_world.transform.translation

        # define the turning radius
        radius = sqrt(
            (
                right_foot_world.transform.translation.x -
                left_foot_world.transform.translation.x
            )**2 + (
                right_foot_world.transform.translation.y -
                left_foot_world.transform.translation.y
            )**2) / 2.

        left_offset = [-radius*sin(yaw), radius*(1-cos(yaw)), 0]
        right_offset = [radius*sin(yaw), -radius*(1-cos(yaw)), 0]
        intermediate_euler = euler_from_quaternion([
            intermediate_transform.rotation.x,
            intermediate_transform.rotation.y,
            intermediate_transform.rotation.z,
            intermediate_transform.rotation.w])
        resulting_quat = quaternion_from_euler(
            intermediate_euler[0], intermediate_euler[1],
            intermediate_euler[2] + yaw)

        rot = quaternion_matrix([
            resulting_quat[0], resulting_quat[1], resulting_quat[2], resulting_quat[3]])
        left_transformedOffset = numpy.dot(rot[0:3, 0:3], left_offset)
        right_transformedOffset = numpy.dot(rot[0:3, 0:3], right_offset)
        quat_final = Quaternion(
            resulting_quat[0], resulting_quat[1], resulting_quat[2], resulting_quat[3])

        left_footstep.location.x += left_transformedOffset[0]
        left_footstep.location.y += left_transformedOffset[1]
        left_footstep.location.z += left_transformedOffset[2]
        left_footstep.orientation = quat_final

        right_footstep.location.x += right_transformedOffset[0]
        right_footstep.location.y += right_transformedOffset[1]
        right_footstep.location.z += right_transformedOffset[2]
        right_footstep.orientation = quat_final

        if yaw > 0:
            return [left_footstep, right_footstep]
        else:
            return [right_footstep, left_footstep]

    # Creates footstep with the current position and orientation of the foot.
    def createFootStepInPlace(self, step_side):
        footstep = FootstepDataRosMessage()
        footstep.robot_side = step_side

        if step_side == FootstepDataRosMessage.LEFT:
            foot_frame = self.LEFT_FOOT_FRAME_NAME
        else:
            foot_frame = self.RIGHT_FOOT_FRAME_NAME

        footWorld = self.tfBuffer.lookup_transform('world', foot_frame, rospy.Time())
        footstep.orientation = footWorld.transform.rotation
        footstep.location = footWorld.transform.translation

        return footstep

    # Creates footstep offset from the current foot position. The offset is in foot frame.
    def createTranslationFootStepOffset(self, step_side, offset):
        footstep = self.createFootStepInPlace(step_side)

        # transform the offset to world frame
        quat = footstep.orientation
        rot = quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
        transformedOffset = numpy.dot(rot[0:3, 0:3], offset)

        footstep.location.x += transformedOffset[0]
        footstep.location.y += transformedOffset[1]
        footstep.location.z += transformedOffset[2]

        return footstep

    def getEmptyFootsetListMsg(self):
        msg = FootstepDataListRosMessage()
        msg.default_transfer_time = 0.5
        msg.default_swing_time = 0.8
        msg.execution_mode = 0
        msg.unique_id = -1
        return msg

    def getTranslationFootstepMsg(self, offset):
        msg = self.getEmptyFootsetListMsg()
        msg.footstep_data_list.append(self.createTranslationFootStepOffset(
            FootstepDataRosMessage.LEFT, offset))
        msg.footstep_data_list.append(self.createTranslationFootStepOffset(
            FootstepDataRosMessage.RIGHT, offset))
        return msg

    def getRotationFooststepMsg(self, yaw):
        msg = self.getEmptyFootsetListMsg()
        footsteps = self.createRotationFootStepList(yaw)
        for footstep in footsteps:
            msg.footstep_data_list.append(footstep)

        return msg

    def execute_footsteps(self, msg):
        self.footstep_count = 0
        self.footstep_publisher.publish(msg)
        number_of_footsteps = len(msg.footstep_data_list)
        max_iterations = 100
        count = 0
        while count < max_iterations:
            self.rate.sleep()
            count += 1
            if self.footstep_count == number_of_footsteps:
                return True
                break
        msg = AbortWalkingRosMessage()
        msg.unique_id = -1
        self.abort_walking_publisher.publish(msg)
        return False

    def translate(self, offset):
        msg = self.getTranslationFootstepMsg(offset)
        res = self.execute_footsteps(msg)
        if res:
            self.loginfo('done walking')
            return
        self.loginfo('failed to walk, aborting trajectory')

    def rotate(self, yaw):
        # space feet further apart if not spaced enough for safe rotation
        self.loginfo("we're going to rotate an angle of %r" % yaw)
        self.set_init_pose()
        msg = self.getRotationFooststepMsg(yaw)
        res = self.execute_footsteps(msg)
        if res:
            self.loginfo('done rotating')
            return
        self.loginfo('failed to rotate, aborting trajectory')

    def set_init_pose(self):
        left_foot_world = self.tfBuffer.lookup_transform(
            'world', self.LEFT_FOOT_FRAME_NAME, rospy.Time())
        right_foot_world = self.tfBuffer.lookup_transform(
            'world', self.RIGHT_FOOT_FRAME_NAME, rospy.Time())
        intermediate_transform = Transform()
        distance_between_feet = sqrt(
            (
                right_foot_world.transform.translation.x -
                left_foot_world.transform.translation.x
            )**2 + (
                right_foot_world.transform.translation.y -
                left_foot_world.transform.translation.y
            )**2)
        if distance_between_feet <= 0.2:
            self.loginfo('moving feet further apart\n')
            msg = self.getEmptyFootsetListMsg()
            msg.footstep_data_list.append(self.createTranslationFootStepOffset(
                FootstepDataRosMessage.LEFT, [0.0, 0.1, 0.0]))
            msg.footstep_data_list.append(self.createTranslationFootStepOffset(
                FootstepDataRosMessage.RIGHT, [0.0, -0.1, 0.0]))
            self.execute_footsteps(msg)
            self.loginfo('done moving feet further apart\n')

    def raise_pelvis(self):
        pelvisMsg = PelvisHeightTrajectoryRosMessage()
        pelvisMsg.execution_mode = 0
        pelvisMsg.unique_id = -1
    
        pelvisTraj = TrajectoryPoint1DRosMessage()
        pelvisTraj.time = 1.5
        pelvisTraj.position = 1.15
        pelvisTraj.velocity = 0.0
    
        pelvisMsg.trajectory_points.append(pelvisTraj)
    
        self.loginfo('setting pelvis height...')
        self.pelvisHeightTrajectoryPublisher.publish(pelvisMsg)
        self.rate.sleep()


    def _update_joint_values(self, label, binding, ch):
        joint_index = binding['joint_index']
        if joint_index == 'reset':
            # reset all joints to zero
            self.loginfo('Reset %s' % label)
            self.joint_values[label] = [0.0] * len(self.joint_values[label])

        else:
            # update value within boundaries
            value = self.joint_values[label][joint_index]
            upper = binding.get('uppercase', ch.upper())
            is_lower_case = ch != upper
            factor = 1.0 if is_lower_case else -1.0
            if binding.get('invert', False):
                factor *= -1.0
            STEP = 0.1
            value += STEP * factor
            value = min(max(value, binding['min']), binding['max'])
            self.joint_values[label][joint_index] = value

            self.loginfo(
                'Move %s joint #%d %s to %.1f: [%s]' %
                (label, joint_index, '++' if (is_lower_case != binding.get('invert', False)) else '--',
                 self.joint_values[label][joint_index],
                 ', '.join(
                     ['%.1f' % v for v in self.joint_values[label]])))

    def _append_trajectory_point_1d(self, msg, time, joint_values):
        if not msg.joint_trajectory_messages:
            msg.joint_trajectory_messages = [
                OneDoFJointTrajectoryRosMessage() for _ in joint_values]
        for i, joint_value in enumerate(joint_values):
            point = TrajectoryPoint1DRosMessage()
            point.time = time
            point.position = joint_value
            point.velocity = 0
            msg.joint_trajectory_messages[i].trajectory_points.append(point)

    def _append_trajectory_point_so3(self, msg, time, joint_values):
        roll, pitch, yaw = joint_values
        quat = quaternion_from_euler(roll, pitch, yaw)
        point = SO3TrajectoryPointRosMessage()
        point.time = time
        point.orientation = Quaternion()
        point.orientation.x = quat[0]
        point.orientation.y = quat[1]
        point.orientation.z = quat[2]
        point.orientation.w = quat[3]
        point.angular_velocity = Vector3()
        point.angular_velocity.x = 0
        point.angular_velocity.y = 0
        point.angular_velocity.z = 0
        msg.taskspace_trajectory_points.append(point)

    def loginfo(self, msg):
        rospy.loginfo(msg)

