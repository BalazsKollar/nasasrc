#!/usr/bin/env python
from std_msgs.msg import Float32MultiArray
import rospy
import tf2_ros
import time
import mi_getgroundpoint #The script for getting the location of a point
import mi_navigation #Brooks's script for walking to a point
import numpy

old_data=1
#tfBuffer is also a global variable

def vector_processer(data):
    global old_data
    global tfBuffer
    if data.data!=old_data: #Only read every data once
        old_data=data.data
        data_len=len(old_data)/3 #The number of points published
        i=1
        first_step=1 #some stuff we should do at the first step
        walk_points=numpy.empty((0,2))
        while i <=data_len: #Running through every point
                if old_data[i*3-1]==0:  #If it is a point to walk to
                    if first_step:
                        first_step=0
                        controller = mi_navigation.Navigator(tfBuffer)
                    temp=mi_getgroundpoint.walkie(old_data[i*3-3],old_data[i*3-2],tfBuffer)
                    walk_points=numpy.append(walk_points,temp,axis=0)
                    i=i+1
                elif old_data[i*3-1]==1 or old_data[i*3-1]==2 or old_data[i*3-1]==3:
                    i=i+2
        controller._walk_path_callback(walk_points)

if __name__ == '__main__':
    rospy.init_node('mi_listener')
    global tfBuffer 
    tfBuffer = tf2_ros.Buffer()
    tfListener = tf2_ros.TransformListener(tfBuffer)
    rate = rospy.Rate(10) # 10hz
    time.sleep(15)
    rospy.Subscriber("MI_pub", Float32MultiArray, vector_processer) #reads the data that monkey_island published

    rate = rospy.Rate(100)
    while not rospy.is_shutdown():
         rate.sleep()
         
