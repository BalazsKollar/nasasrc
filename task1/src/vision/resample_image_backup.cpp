#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>
#include <opencv2/core/core.hpp>
#include <opencv/cv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <std_msgs/MultiArrayLayout.h>
#include <std_msgs/MultiArrayDimension.h>
#include <std_msgs/Float32MultiArray.h>
using namespace cv;
using namespace std;

//Global variables
Mat leftImage2, rightImage2, leftImage4, rightImage4, leftImage8, rightImage8;


//Function declarations
float* location_color(cv::Mat diffIm);//this gives the center of the LED - on the image it is called on
void imageCallback(const sensor_msgs::ImageConstPtr& msg, bool which_cam); //This gets called every time an image gets published

//Callback functions - runs if an image is published on either camera topic
void imageCallback(const sensor_msgs::ImageConstPtr& msg, bool which_cam) //which_cam = left=true or right=false
{
	cv_bridge::CvImageConstPtr temp;
	Size size_of_new;
	cv::Mat currImage; //currImage - the current image the robot sees
	try
	{
		//Initializing a lot of stuff
		temp=cv_bridge::toCvShare(msg, "bgr8"); // used for getting the original image
		currImage=temp->image;
		size_of_new.height=currImage.size().height; size_of_new.width=currImage.size().width; 
		if(which_cam)//for the left camera, resize the image
		{
			size_of_new.height=currImage.size().height/2; size_of_new.width=currImage.size().width/2; 
			resize(currImage,leftImage2,size_of_new);
size_of_new.height=currImage.size().height/4; size_of_new.width=currImage.size().width/4; 
			resize(currImage,leftImage4,size_of_new);
size_of_new.height=currImage.size().height/8; size_of_new.width=currImage.size().width/8; 
			resize(currImage,leftImage8,size_of_new);
		} 
		else
		{
size_of_new.height=currImage.size().height/2; size_of_new.width=currImage.size().width/2; 
			resize(currImage,rightImage2,size_of_new);
size_of_new.height=currImage.size().height/4; size_of_new.width=currImage.size().width/4; 
			resize(currImage,rightImage4,size_of_new);
size_of_new.height=currImage.size().height/8; size_of_new.width=currImage.size().width/8; 
			resize(currImage,rightImage8,size_of_new);
		}

	}
	catch (cv_bridge::Exception& e)
	{
		ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
	}
}



int main(int argc, char *argv[])
{

	//Initialize
  	ros::init(argc, argv, "image_resampling");
  	ros::NodeHandle nh;

	//Getting the images
	image_transport::ImageTransport it(nh);
	image_transport::Subscriber subleft = it.subscribe("/multisense/camera/left/image_raw", 1,boost::bind(imageCallback,_1, true)); // creating the left subscriber
	image_transport::Subscriber subright = it.subscribe("/multisense/camera/right/image_raw", 1,boost::bind(imageCallback,_1, false)); // creating the right subscriber

	//creating the publishers
	image_transport::Publisher publeft21 = it.advertise("multisense/camera/left/image_resample21", 2); // creating the left publisher for resampling of 2
	image_transport::Publisher pubright21 = it.advertise("multisense/camera/right/image_resample21", 2); // creating the right publisher for resampling of 2
	image_transport::Publisher publeft41 = it.advertise("multisense/camera/left/image_resample41", 2); // creating the left publisher for resampling of 4
	image_transport::Publisher pubright41 = it.advertise("multisense/camera/right/image_resample41", 2); // creating the right publisher for resampling of 4
	image_transport::Publisher publeft81 = it.advertise("multisense/camera/left/image_resample81", 2); // creating the left publisher for resampling of 8
	image_transport::Publisher pubright81 = it.advertise("multisense/camera/right/image_resample81", 2); // creating the right publisher for resampling of 8
	image_transport::Publisher publeft22 = it.advertise("multisense/camera/left/image_resample22", 2); // creating the left publisher for resampling of 2
	image_transport::Publisher pubright22 = it.advertise("multisense/camera/right/image_resample22", 2); // creating the right publisher for resampling of 2
	image_transport::Publisher publeft42 = it.advertise("multisense/camera/left/image_resample42", 2); // creating the left publisher for resampling of 4
	image_transport::Publisher pubright42 = it.advertise("multisense/camera/right/image_resample42", 2); // creating the right publisher for resampling of 4
	image_transport::Publisher publeft82 = it.advertise("multisense/camera/left/image_resample82", 2); // creating the left publisher for resampling of 8
	image_transport::Publisher pubright82 = it.advertise("multisense/camera/right/image_resample82", 2); // creating the right publisher for resampling of 8
	image_transport::Publisher publeft24 = it.advertise("multisense/camera/left/image_resample24", 2); // creating the left publisher for resampling of 2
	image_transport::Publisher pubright24 = it.advertise("multisense/camera/right/image_resample24", 2); // creating the right publisher for resampling of 2
	image_transport::Publisher publeft44 = it.advertise("multisense/camera/left/image_resample44", 2); // creating the left publisher for resampling of 4
	image_transport::Publisher pubright44 = it.advertise("multisense/camera/right/image_resample44", 2); // creating the right publisher for resampling of 4
	image_transport::Publisher publeft84 = it.advertise("multisense/camera/left/image_resample84", 2); // creating the left publisher for resampling of 8
	image_transport::Publisher pubright84 = it.advertise("multisense/camera/right/image_resample84", 2); // creating the right publisher for resampling of 8

	//Publishing the results
	sensor_msgs::ImagePtr leftmsg2, rightmsg2,leftmsg4, rightmsg4,leftmsg8, rightmsg8;		
	//leftmsg.data.clear(); rightmsg.data.clear();
	ros::Rate loop_rate(2);
	int publish2=1;
	int publish4=1;
	while (ros::ok()) //This gets called constantly
 	{
		leftmsg2=cv_bridge::CvImage(std_msgs::Header(),"bgr8",leftImage2).toImageMsg();
		rightmsg2=cv_bridge::CvImage(std_msgs::Header(),"bgr8",rightImage2).toImageMsg();
		publeft21.publish(leftmsg2);
		pubright21.publish(rightmsg2);
		leftmsg4=cv_bridge::CvImage(std_msgs::Header(),"bgr8",leftImage4).toImageMsg();
		rightmsg4=cv_bridge::CvImage(std_msgs::Header(),"bgr8",rightImage4).toImageMsg();
		publeft41.publish(leftmsg4);
		pubright41.publish(rightmsg4);
		leftmsg8=cv_bridge::CvImage(std_msgs::Header(),"bgr8",leftImage8).toImageMsg();
		rightmsg8=cv_bridge::CvImage(std_msgs::Header(),"bgr8",rightImage8).toImageMsg();
		publeft81.publish(leftmsg8);
		pubright81.publish(rightmsg8);
		if(publish2==1)
		{
			publeft22.publish(leftmsg2);
			pubright22.publish(rightmsg2);
			publeft42.publish(leftmsg4);
			pubright42.publish(rightmsg4);
			publeft82.publish(leftmsg8);
			pubright82.publish(rightmsg8);
			publish2=0;
			if(publish4==1)
			{
				publeft24.publish(leftmsg2);
				pubright24.publish(rightmsg2);
				publeft44.publish(leftmsg4);
				pubright44.publish(rightmsg4);
				publeft84.publish(leftmsg8);
				pubright84.publish(rightmsg8);
				publish4=0;
			}
			else
			{
				publish4=1;
			}
		}
		else
		{
			publish2=1;
		}

		
		ros::spinOnce();
		loop_rate.sleep();
	}

}
