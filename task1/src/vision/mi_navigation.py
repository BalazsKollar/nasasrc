import rospy
import numpy as np
from math import radians
from cv_bridge import CvBridge
import tf
import tf2_ros
import mi_walker

robot_name = rospy.get_param('/ihmc_ros/robot_name')
LEFT_FOOT_FRAME_NAME = rospy.get_param("/ihmc_ros/{0}/left_foot_frame_name".format(robot_name))
RIGHT_FOOT_FRAME_NAME = rospy.get_param("/ihmc_ros/{0}/right_foot_frame_name".format(robot_name))

class Mapper(object):
    
    RESOLUTION = 0.1
    
    def __init__(self,buffername):
        self.tfBuffer = buffername
        
    def robot_current_location(self):
        pos = []
        direc = []
        for foot in (LEFT_FOOT_FRAME_NAME, RIGHT_FOOT_FRAME_NAME):
            t = self.tfBuffer.lookup_transform('world', foot, rospy.Time(0)).transform
            quat = np.array([t.rotation.x,t.rotation.y,t.rotation.z,t.rotation.w])
            R = tf.transformations.euler_matrix(*tf.transformations.euler_from_quaternion(quat))
            trans = np.array([t.translation.x,t.translation.y,t.translation.z])
            pos.append(np.dot(R[:3,:3], np.zeros(3)) + trans)
            direc.append(np.dot(R[:3,:3], np.array([0.25,0.,0.])) + trans)
        return np.mean(pos, 0), np.mean(direc, 0)

class Navigator(object):
    def __init__(self,buffername):
        self.livemap = Mapper(buffername)
        self.walker = mi_walker.Walker()
        self.walker.init(buffername)

    def compute_angle_and_distance(self, target):
        """ Computes the angle and distance to a given target point,
            relative to the robot's current position and orientation. """
        loc, direc = self.livemap.robot_current_location()
        print loc
        loc = np.array(loc)[:2]
        v1 = np.array(direc)[:2] - loc
        v2 = target - loc
        angle = np.rad2deg(np.arctan2(v2[1], v2[0]) - np.arctan2(v1[1], v1[0]))
        if angle > 180: # shouldn't be necessary if arctan2 is being sane...?
            angle -= 360
        if angle < -180:
            angle += 360
        dist = np.linalg.norm(v2)
        return angle, dist
    

    def rotate_val(self, target=None, angle=None):
        """ Turn the robot to look at a specified x,y point, or a specified angle. """
        if target is None:
            angle = angle
        elif angle is None:
            angle, _ = self.compute_angle_and_distance(target)
        if angle is not None:
            if np.abs(angle) > 90:
                step = radians(angle/3)
                self.walker.rotate(step)
            elif np.abs(angle) > 60:
                step = radians(angle/2)
                self.walker.rotate(step)
            else:
                self.walker.rotate(radians(angle))

    def walk_straight(self, distance):
        """ Walk in a straight line a specified distance """
        self.walker.walk_distance(distance)

    def _rotate_val_callback(self, msg):
        point = msg.data.strip()
        if point[0] == '[' and point[-1] == ']':
            point = point[1:-1]
        point = np.fromstring(point,sep=',')
        assert len(point) == 2
        rospy.loginfo("rotating towards %r" % point)
        self.rotate_val(target=point)

    def _walk_path_callback(self, path):
        for i in range(0,path.shape[0]):
            angle, distance = self.compute_angle_and_distance(np.array([path[i,0],path[i,1]]))
            self.rotate_val(angle=angle)
            self.walk_straight(distance)
