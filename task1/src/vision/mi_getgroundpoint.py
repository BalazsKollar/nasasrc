#!/usr/bin/env python
import rospy
import numpy
import tf


def walkie(xcoord,ycoord,tfBuffer):
    #First we get how high the robot is standing = how high the ground is. The robot should not move, that messes things up 
    #The hight of both rightFoot and leftFoot relative to the ground if standing is 0.097
    print "looking for leftfoot - world transform"
    world_to_leftfoot=tfBuffer.lookup_transform('leftFoot', 'world', rospy.Time(0), rospy.Duration(20.0))
    quat = world_to_leftfoot.transform.rotation
    rot = tf.transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
    transformedOffset = numpy.dot(rot[0:3, 0:3], [0, 0, 0])
    left_foot_height= world_to_leftfoot.transform.translation.z + transformedOffset[2]
    ground_height=left_foot_height+0.097    
    #the transformation to left_camera_optical_frame from world
    print "looking for left_camera_optical_frame - world transform"
    leftcam_to_world=tfBuffer.lookup_transform('left_camera_optical_frame','world',rospy.Time(0), rospy.Duration(20.0))
    #inverting the coordinates
    goto=inverter(xcoord,ycoord, ground_height, leftcam_to_world)
    return goto

def inverter(xcoord,ycoord, ground_height, leftcam_to_world):
    # Here comes a copy-paste describing how taking a camera image works:
    # Projection/camera matrix
    #     [fx'  0  cx' Tx]
    # P = [ 0  fy' cy' Ty]
    #     [ 0   0   1   0]
    # By convention, this matrix specifies the intrinsic (camera) matrix
    #  of the processed (rectified) image. That is, the left 3x3 portion
    #  is the normal camera intrinsic matrix for the rectified image.
    # It projects 3D points in the camera coordinate frame to 2D pixel
    #  coordinates using the focal lengths (fx', fy') and principal point
    #  (cx', cy') - these may differ from the values in K.
    # For monocular cameras, Tx = Ty = 0. Normally, monocular cameras will
    #  also have R = the identity and P[1:3,1:3] = K.
    # For a stereo pair, the fourth column [Tx Ty 0]' is related to the
    #  position of the optical center of the second camera in the first
    #  camera's frame. We assume Tz = 0 so both cameras are in the same
    #  stereo image plane. The first camera always has Tx = Ty = 0. For
    #  the right (second) camera of a horizontal stereo pair, Ty = 0 and
    #  Tx = -fx' * B, where B is the baseline between the cameras.
    # Given a 3D point [X Y Z]', the projection (x, y) of the point onto
    #  the rectified image is given by:
    #  [u v w]' = P * [X Y Z 1]'
    #         x = u / w
    #         y = v / w
    #  This holds for both images of a stereo pair.

    #Calling a rostopic echo /multisense/camera/left/camera_info gives 	
    #the details for the left camera:
    # P_left:[610.1799470098168, 0.0, 512.5, -0.0, 0.0, 610.1799470098168, 272.5, 0.0, 0.0, 0.0, 1.0, 0.0]
    #Thus both Tx and Ty are 0
    fx=610.1799470098168; cx=512.5; 
    fy=610.1799470098168; cy=272.5;

    # Solving the linear equation - I transformed the equation a bit
    P0=numpy.matrix([[fx, 0, cx],  [0, fy, cy], [0, 0, 1]])
    Trans=numpy.matrix([[leftcam_to_world.transform.translation.x],[leftcam_to_world.transform.translation.y],[leftcam_to_world.transform.translation.z]])
    quat = leftcam_to_world.transform.rotation    
    A=tf.transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
    A=A[0:3,0:3]
    left_hand_side=numpy.matrix([[1024-xcoord],[544-ycoord],[1]])
    right_hand_matrix=numpy.dot(P0,A[0:3, 0:3])
    x=numpy.linalg.solve(right_hand_matrix,left_hand_side)
    inverse_of_A=A.transpose()
    right_hand_vector=numpy.dot(inverse_of_A,Trans)
    w=(right_hand_vector[2]+ground_height)/x[2]
    x_floor=float(x[0]*w-right_hand_vector[0])
    y_floor=float(x[1]*w-right_hand_vector[1])
    
#    whatever=numpy.dot(A,numpy.matrix([[x_floor],[y_floor],[ground_height]]))+Trans
#    print x_floor
#    print y_floor
#    print whatever
#    uvw=numpy.dot(P0,whatever)
#    print uvw[0]/uvw[2]
#    print uvw[1]/uvw[2]
    return numpy.matrix([x_floor,y_floor])