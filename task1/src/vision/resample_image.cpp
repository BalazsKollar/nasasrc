#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>
#include <opencv2/core/core.hpp>
#include <opencv/cv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <std_msgs/MultiArrayLayout.h>
#include <std_msgs/MultiArrayDimension.h>
#include <std_msgs/Float32MultiArray.h>
using namespace cv;
using namespace std;

//Global variables
Mat leftImage2, rightImage2, leftImage4, rightImage4, leftImage8, rightImage8;
Mat leftImage2bw, leftImage4bw, leftImage8bw;

//Function declarations
float* location_color(cv::Mat diffIm);//this gives the center of the LED - on the image it is called on
void imageCallback(const sensor_msgs::ImageConstPtr& msg, bool which_cam); //This gets called every time an image gets published

//Callback functions - runs if an image is published on either camera topic
void imageCallback(const sensor_msgs::ImageConstPtr& msg, bool which_cam) //which_cam = left=true or right=false
{
	cv_bridge::CvImageConstPtr temp;
	Size size_of_new;
	cv::Mat currImage; //currImage - the current image the robot sees
	try
	{
		//Initializing a lot of stuff
		temp=cv_bridge::toCvShare(msg, "bgr8"); // used for getting the original image
		currImage=temp->image;
		size_of_new.height=currImage.size().height; size_of_new.width=currImage.size().width; 
		if(which_cam)//for the left camera, resize the image
		{
			size_of_new.height=currImage.size().height/2; size_of_new.width=currImage.size().width/2; 
			resize(currImage,leftImage2,size_of_new);
size_of_new.height=currImage.size().height/4; size_of_new.width=currImage.size().width/4; 
			resize(currImage,leftImage4,size_of_new);
size_of_new.height=currImage.size().height/8; size_of_new.width=currImage.size().width/8; 
			resize(currImage,leftImage8,size_of_new);
		} 
		else
		{
size_of_new.height=currImage.size().height/2; size_of_new.width=currImage.size().width/2; 
			resize(currImage,rightImage2,size_of_new);
size_of_new.height=currImage.size().height/4; size_of_new.width=currImage.size().width/4; 
			resize(currImage,rightImage4,size_of_new);
size_of_new.height=currImage.size().height/8; size_of_new.width=currImage.size().width/8; 
			resize(currImage,rightImage8,size_of_new);
		}
           cvtColor(leftImage2,leftImage2bw,CV_BGR2GRAY);
           cvtColor(leftImage4,leftImage4bw,CV_BGR2GRAY);
           cvtColor(leftImage8,leftImage8bw,CV_BGR2GRAY);

	}
	catch (cv_bridge::Exception& e)
	{
		ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
	}
}



int main(int argc, char *argv[])
{

	//Initialize
  	ros::init(argc, argv, "image_resampling");
  	ros::NodeHandle nh;

	//Getting the images
	image_transport::ImageTransport it(nh);
	image_transport::Subscriber subleft = it.subscribe("/multisense/camera/left/image_raw", 1,boost::bind(imageCallback,_1, true)); // creating the left subscriber
	image_transport::Subscriber subright = it.subscribe("/multisense/camera/right/image_raw", 1,boost::bind(imageCallback,_1, false)); // creating the right subscriber
	ros::Rate loop_rate(2);
    int publish2=1;
    int publish4=1;
    int publish16=2;
    vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
     compression_params.push_back(3);
	while (ros::ok()) //This gets called constantly
 	{
           imwrite( "/home/docker/ws/monkey_images/camim_left_21.jpg", leftImage2 );
           imwrite( "/home/docker/ws/monkey_images/camim_left_41.jpg", leftImage4 );
           imwrite( "/home/docker/ws/monkey_images/camim_left_81.jpg", leftImage8 );
           imwrite( "/home/docker/ws/monkey_images/camim_left_21_bw.jpg", leftImage2bw );
           imwrite( "/home/docker/ws/monkey_images/camim_left_41_bw.jpg", leftImage4bw );
           imwrite( "/home/docker/ws/monkey_images/camim_left_81_bw.jpg", leftImage8bw );
//           imwrite( "/home/docker/monkey_images/camim_left_21.png", leftImage2, compression_params );
//           imwrite( "/home/docker/monkey_images/camim_right_21.png", rightImage2, compression_params );
//           imwrite( "/home/docker/monkey_images/camim_left_41.png", leftImage4, compression_params );
//           imwrite( "/home/docker/monkey_images/camim_right_41.png", rightImage4, compression_params);
//           imwrite( "/home/docker/monkey_images/camim_left_81.png", leftImage8, compression_params  );
//           imwrite( "/home/docker/monkey_images/camim_right_81.png", rightImage8,compression_params );
		if(publish2==1)
		{
                imwrite( "/home/docker/ws/monkey_images/camim_left_22.jpg", leftImage2 );
                imwrite( "/home/docker/ws/monkey_images/camim_left_42.jpg", leftImage4 );
                imwrite( "/home/docker/ws/monkey_images/camim_left_82.jpg", leftImage8 );
                imwrite( "/home/docker/ws/monkey_images/camim_left_22_bw.jpg", leftImage2bw );
                imwrite( "/home/docker/ws/monkey_images/camim_left_42_bw.jpg", leftImage4bw );
                imwrite( "/home/docker/ws/monkey_images/camim_left_82_bw.jpg", leftImage8bw );
//                imwrite( "/home/docker/monkey_images/camim_left_22.png", leftImage2, compression_params );
//                imwrite( "/home/docker/monkey_images/camim_right_22.png", rightImage2, compression_params );
//                imwrite( "/home/docker/monkey_images/camim_left_42.png", leftImage4, compression_params );
//                imwrite( "/home/docker/monkey_images/camim_right_42.png", rightImage4, compression_params);
//                imwrite( "/home/docker/monkey_images/camim_left_82.png", leftImage8, compression_params );
//                imwrite( "/home/docker/monkey_images/camim_right_82.png", rightImage8, compression_params);
			publish2=0;
			if(publish4==1)
			{
                    imwrite( "/home/docker/ws/monkey_images/camim_left_24.jpg", leftImage2 );
                    imwrite( "/home/docker/ws/monkey_images/camim_left_44.jpg", leftImage4 );
                    imwrite( "/home/docker/ws/monkey_images/camim_left_84.jpg", leftImage8 );
                    imwrite( "/home/docker/ws/monkey_images/camim_left_24_bw.jpg", leftImage2bw );
                    imwrite( "/home/docker/ws/monkey_images/camim_left_44_bw.jpg", leftImage4bw );
                    imwrite( "/home/docker/ws/monkey_images/camim_left_84_bw.jpg", leftImage8bw );
//                    imwrite( "/home/docker/monkey_images/camim_left_24.png", leftImage2, compression_params );
 //                   imwrite( "/home/docker/monkey_images/camim_right_24.png", rightImage2, compression_params );
//                    imwrite( "/home/docker/monkey_images/camim_left_44.png", leftImage4, compression_params );
//                    imwrite( "/home/docker/monkey_images/camim_right_44.png", rightImage4, compression_params);
//                    imwrite( "/home/docker/monkey_images/camim_left_84.png", leftImage8, compression_params );
//                    imwrite( "/home/docker/monkey_images/camim_right_84.png", rightImage8, compression_params);
                    publish4=0;
                    if(publish16==2)
                    {
                    imwrite( "/home/docker/ws/monkey_images/camim_left_816.jpg", leftImage8 );
                    imwrite( "/home/docker/ws/monkey_images/camim_left_816_bw.jpg", leftImage8bw);
                    imwrite( "/home/docker/ws/monkey_images/camim_left_216.png", leftImage2, compression_params );
                    imwrite( "/home/docker/ws/monkey_images/camim_right_216.png", rightImage2, compression_params);
                    publish16=0;     
                    } 
                    publish16++; 
			}
			else
			{
				publish4=1;
			}
		}
		else
		{
			publish2=1;
		}

		
		ros::spinOnce();
		sleep(1);
	}

}
