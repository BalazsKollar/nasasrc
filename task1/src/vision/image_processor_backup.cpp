#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>
#include <opencv2/core/core.hpp>
#include <opencv/cv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <std_msgs/MultiArrayLayout.h>
#include <std_msgs/MultiArrayDimension.h>
#include <std_msgs/Float32MultiArray.h>
using namespace cv;
using namespace std;

//Global variables
float rgb_left [3]={0,0,0}, rgb_right [3]={0,0,0}, loc_left [2]={0,0}, loc_right [2]={0,0}; //what either camera sees - LED color and position
int blur_size = 0; //by how much we want to blur the image, at least 1 four outer rim of pavement, otherwise 0
int const max_blur_size = 40;
int dilate_size = 0; //by how much we want to dilate the image, 0 is OK all over
int const max_dilate_size = 40;
//the colors we are looking for
int lowred=0, upred = 255 ; //[0,255] for red wheel, [61,68] four outer rim of pavement
int lowgreen = 236, upgreen=255; //[236,255] for red wheel, [0,0] four outer rim of pavement
int lowblue = 103, upblue = 146; //[0,0] for red wheel, [0,0] four outer rim of pavement
int color_we_are_looking_for[3]={0,0,0}; //It is in RGB. I left it on 0,0,0 because it is a bit hard to define what color corresponds to the ranges given above.

bool show_windows=0; 


//Function declarations
float* location_color(cv::Mat diffIm);//this gives the center of the LED - on the image it is called on
void imageCallback(const sensor_msgs::ImageConstPtr& msg, bool which_cam); //This gets called every time an image gets published

//Callback functions - runs if an image is published on either camera topic
void imageCallback(const sensor_msgs::ImageConstPtr& msg, bool which_cam) //which_cam = left=true or right=false
{
	cv_bridge::CvImageConstPtr temp;
	cv::Mat currImage; //currImage - the current image the robot sees
	try
	{
		//Initializing a lot of stuff
		temp=cv_bridge::toCvShare(msg, "bgr8"); // used for getting the original image
		if(which_cam)
		{
			loc_left[0]=0; loc_left[1]=0;//First we say that we did not detect any LED = the location of the LEDs the camera is zero
		} 
		else
		{
			loc_right[0]=0; loc_right[1]=0;
		}
		cv::flip(temp->image, currImage, -1); // we need to flip it horizontally, cause the camera is upside-down

		float* all_the_stuff; // This is the variable with the color, and location of the LED - in this order
		all_the_stuff=location_color(currImage);

		if(which_cam)//for the left camera, what colors did we detect and in which locations - in pixels
		{
			rgb_left[0]=all_the_stuff[0]; rgb_left[1]=all_the_stuff[1]; rgb_left[2]=all_the_stuff[2];
			loc_left[0]=all_the_stuff[3]; loc_left[1]=all_the_stuff[4];
		} 
		else
		{
			rgb_right[0]=all_the_stuff[0]; rgb_right[1]=all_the_stuff[1]; rgb_right[2]=all_the_stuff[2];
			loc_right[0]=all_the_stuff[3]; loc_right[1]=all_the_stuff[4];
		}

		
		//To see what is getting detected - the following lines open the windows in which we can see the camera image, it has not actual use otherwise
		cv::Scalar color = Scalar(0,255,0);
		int temp1, temp2;
		temp1=(int) all_the_stuff[3]; 
		temp2=(int) all_the_stuff[4];
		cv::circle(currImage, Point(temp1,temp2), 4, color, -1, 8, 0 );
		if(which_cam & show_windows)
		{
			cv::imshow( "view", currImage);
		} 
		else if(show_windows)
		{
			cv::imshow( "view2",currImage);
		}
				
	}
	catch (cv_bridge::Exception& e)
	{
		ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
	}
}

float* location_color(cv::Mat currIm)
{
/*This is just a siplified version of the code of docs.opencv.org/2.4/doc/tutorials/imgproc/shapedescriptors/moments/moments.html for detecting objects and there centers of mass on an image*/
	Mat blur_src_temp, blur_src, src_temp=currIm.clone();

  	//Blurring image
  	Mat element = getStructuringElement(  cv::MORPH_RECT,cv::Size(blur_size+1,blur_size+1),cv::Point( -1, -1 ) );
  	erode(currIm,blur_src_temp,element);

  	//Selecting the color - everything else except the chosen color gets thrown out
  	cvtColor(blur_src_temp, blur_src, cv::COLOR_BGR2HSV);
  	inRange(blur_src, cv::Scalar(lowblue, lowgreen, lowred), cv::Scalar(upblue, upgreen, upred), blur_src);

  	//Dilating the image
  	element = getStructuringElement(  cv::MORPH_RECT,cv::Size(dilate_size+1,dilate_size+1),cv::Point( -1, -1 ) );
  	dilate(blur_src,blur_src_temp,element);

  	// Calculating the center of mass for the image = where the colors central location is on the image
  	Moments mu=moments( blur_src_temp, false );
  	Point2f mc=Point2f( mu.m10/mu.m00 , mu.m01/mu.m00);
  
  	// Show results
	Vec3b color;
	float all_data[5]={0,0,0,0,0}; //the color and the location of the LED center in this order
	all_data[0]= color_we_are_looking_for[0]; all_data[1]=color_we_are_looking_for[1]; all_data[2]=color_we_are_looking_for[2];
	all_data[3]=mc.x; all_data[4]=mc.y;
	return all_data;
}


int main(int argc, char **argv)
{

	std::string publish_string = argv[1];
	std::string color_to_find = argv[2];
	show_windows=atof(argv[3]);

	if( color_to_find=="blue")
	{
		lowred=0; upred = 255;lowgreen = 236; upgreen=255;lowblue = 103; upblue = 146;
	}
	else if( color_to_find=="red")
	{
		lowred=0; upred = 255;lowgreen = 236; upgreen=255;lowblue = 0; upblue = 0;
	}
	else if( color_to_find=="red_button")
	{
		lowred=128; upred = 158;lowgreen = 212; upgreen=255;lowblue = 0; upblue = 0;
	}
	else if( color_to_find=="blue_strip")
	{
		lowred=12; upred = 63;lowgreen = 169; upgreen=255;lowblue = 87; upblue = 107;
	}
	else if( color_to_find=="yellow_door")
	{
		lowred=100; upred = 189;lowgreen = 69; upgreen=192;lowblue = 25; upblue = 28;
	}
	else if( color_to_find=="solar_output")
	{
		lowred=0; upred = 118;lowgreen = 222; upgreen=255;lowblue = 53; upblue = 111;
	}
	else if( color_to_find=="cable_choke")
	{
		lowred=55; upred = 121; lowgreen = 236; upgreen=255;lowblue = 115; upblue = 126;
	}
	else if( color_to_find=="cable_choke_ends")
	{
		lowred=32; upred = 54;lowgreen = 236; upgreen=255;lowblue = 115; upblue = 126;
	}
	else if( color_to_find=="leak_detect_1")
	{
		lowred=177; upred = 209;lowgreen = 246; upgreen=255;lowblue = 69; upblue = 98;
	}
	else if( color_to_find=="leak_detect_2")
	{
		lowred=133; upred = 173;lowgreen = 142; upgreen=255;lowblue = 90; upblue = 103;
	}
	else if( color_to_find=="leak_repair")
	{
		lowred=83; upred = 226;lowgreen = 220; upgreen=255;lowblue = 101; upblue = 127;
	}
	else if( color_to_find=="endbox")
	{
		lowred=0; upred = 255;lowgreen = 81; upgreen=156;lowblue = 60; upblue = 71;
	}
	else
	{
		cout<<"invalid color input\n";
		return -1;
	}
	
	//Initialize
  	ros::init(argc, argv, "image_listener");
  	ros::NodeHandle nh;
	if(show_windows==1)
	{
  		cv::namedWindow("view");
		cv::namedWindow("view2");
  		cv::startWindowThread();
	}
	else if(show_windows!=0)
	{
  		cout<<"third argument: 0 does not show windows, 1 shows windows\n";
		return -1;
	}	

	//Getting the images
	image_transport::ImageTransport it(nh);
	image_transport::Subscriber subleft = it.subscribe("/multisense/camera/left/image_raw", 1,boost::bind(imageCallback,_1, true)); // creating the left subscriber
	image_transport::Subscriber subright = it.subscribe("/multisense/camera/right/image_raw", 1,boost::bind(imageCallback,_1, false)); // creating the right subscriber


	//Trackbars for changing the settings - e.g. for which color we are looking for
	char* window_name = "Trackbars";
 	namedWindow( window_name, WINDOW_NORMAL);
  	createTrackbar( "Blur size:", window_name, &blur_size, max_blur_size);
	createTrackbar( "Dilate size:", window_name, &dilate_size,max_dilate_size);
  	createTrackbar( "Min red:", window_name, &lowred, 255);
  	createTrackbar( "Max red:", window_name, &upred, 255);
  	createTrackbar( "Min green:", window_name, &lowgreen, 255);
  	createTrackbar( "Max green:", window_name, &upgreen, 255);
  	createTrackbar( "Min blue :", window_name, &lowblue, 255);
  	createTrackbar( "Max blue:", window_name, &upblue, 255);

	//Publishing the results
	std_msgs::Float32MultiArray LED_data; //all the data that we have [rgb_left, rgb_right, loc_left, loc_right]
	LED_data.data.clear();
	ros::Publisher led_detect = nh.advertise<std_msgs::Float32MultiArray>(publish_string, 100);
	ros::Rate loop_rate(10);
	while (ros::ok()) //This gets called constantly
 	{
		LED_data.data.clear();
		LED_data.data.push_back(rgb_left[0]);
		LED_data.data.push_back(rgb_left[1]);
		LED_data.data.push_back(rgb_left[2]);
		LED_data.data.push_back(loc_left[0]);
		LED_data.data.push_back(loc_left[1]);
		LED_data.data.push_back(rgb_right[0]);
		LED_data.data.push_back(rgb_right[1]);
		LED_data.data.push_back(rgb_right[2]);
		LED_data.data.push_back(loc_right[0]);
		LED_data.data.push_back(loc_right[1]);
		if(rgb_right[0]-rgb_left[0]<50 & rgb_right[1]-rgb_left[1]<50 & rgb_right[2]-rgb_left[2]<50 & loc_left[0]>0 & loc_left[1]>0 & loc_right[0]>0 & loc_right[1]>0)
		{
		   //ROS_INFO("LED info published from image_processor");
		   led_detect.publish(LED_data);
		}
		ros::spinOnce();
		loop_rate.sleep();
	}
	
	if(show_windows)
	{
	cv::destroyWindow("view");
	cv::destroyWindow("view2");
	}
}
