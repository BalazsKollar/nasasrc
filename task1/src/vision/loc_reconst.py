#!/usr/bin/env python
from std_msgs.msg import Float32MultiArray
import rospy
import numpy
import math
import tf
from srcsim.msg import Console
import geometry_msgs.msg
from geometry_msgs.msg import PointStamped
from std_msgs.msg import Header
from geometry_msgs.msg import Point

x_cam=0.0; y_cam=0.0; z_cam=0.0; #The location of the LED relative to the camera
rgb_r=0; rgb_g=0; rgb_b=0;

def inverter(data):
    # print rospy.get_name(), "I heard %s"%str(data.data)
    global rgb_r, rgb_g, rgb_b, x_cam, y_cam, z_cam
    x_left=data.data[3]; y_left=data.data[4]
    x_right=data.data[8]; y_right=data.data[9]
    rgb_r=(data.data[0]+data.data[5])//400 #we could use 255*5 instead of 400, but this gives a bit of tolerance
    rgb_g=(data.data[1]+data.data[6])//400
    rgb_b=(data.data[2]+data.data[7])//400

    # Here comes a copy-paste describing how taking a camera image works:
    # Projection/camera matrix
    #     [fx'  0  cx' Tx]
    # P = [ 0  fy' cy' Ty]
    #     [ 0   0   1   0]
    # By convention, this matrix specifies the intrinsic (camera) matrix
    #  of the processed (rectified) image. That is, the left 3x3 portion
    #  is the normal camera intrinsic matrix for the rectified image.
    # It projects 3D points in the camera coordinate frame to 2D pixel
    #  coordinates using the focal lengths (fx', fy') and principal point
    #  (cx', cy') - these may differ from the values in K.
    # For monocular cameras, Tx = Ty = 0. Normally, monocular cameras will
    #  also have R = the identity and P[1:3,1:3] = K.
    # For a stereo pair, the fourth column [Tx Ty 0]' is related to the
    #  position of the optical center of the second camera in the first
    #  camera's frame. We assume Tz = 0 so both cameras are in the same
    #  stereo image plane. The first camera always has Tx = Ty = 0. For
    #  the right (second) camera of a horizontal stereo pair, Ty = 0 and
    #  Tx = -fx' * B, where B is the baseline between the cameras.
    # Given a 3D point [X Y Z]', the projection (x, y) of the point onto
    #  the rectified image is given by:
    #  [u v w]' = P * [X Y Z 1]'
    #         x = u / w
    #         y = v / w
    #  This holds for both images of a stereo pair.

    #Calling a rostopic echo /multisense/camera/left/camera_info gives 	
    #the details for the left camera:
    # P_left:[610.1799470098168, 0.0, 512.5, -0.0, 0.0, 610.1799470098168, 272.5, 0.0, 0.0, 0.0, 1.0, 0.0]
    # for the right
    # P_right: [610.1799470098168, 0.0, 512.5, -42.71259629068718, 0.0, 610.1799470098168, 272.5, 0.0, 0.0, 0.0, 1.0, 0.0]

    fx=610.1799470098168; cx=512.5; 
    fy=610.1799470098168; cy=272.5;
    Tx_right=-42.71259629068718;

    # Solving the linear equation - I transformed the equation a bit
    PM_left=numpy.matrix([[fx, 0, cx],  [0, fy, cy], [0, 0, 1]])
    PM_right=PM_left
    PN_left=numpy.matrix([[0],[0],[0]])
    PN_right=numpy.matrix([[Tx_right],[0],[0]])
    Comp_left=numpy.matrix([[-x_left, 0], [-y_left, 0], [-1, 0]])
    Comp_right=numpy.matrix([[0, -x_right], [0, -y_right], [0, -1]])
    A=numpy.asarray(numpy.bmat([[PM_left, Comp_left], [PM_right, Comp_right]]))
    b=numpy.asarray(numpy.bmat([[-PN_left], [-PN_right]]))
    x=numpy.linalg.solve(numpy.dot(A.transpose(),A),numpy.dot(A.transpose(),b))
    
    # To check if the results are correctThe "neck" frame corresponds to the upperNeckPitchLink frame, which has its z axis up when the robot is standing.
    #P=numpy.asarray(numpy.bmat([[PM_left,PN_left],[PM_right,PN_right]]))
    #x0=x[0];y0=x[1];z0=x[2];
    #sol=numpy.array([x0,y0,z0,1]);
    #trans=numpy.dot(P,sol);
    #print rospy.get_name(), "calculated projection coordinates %s"%str(numpy.squeeze(numpy.array([trans[0]/trans[2], trans[1]/trans[2], trans[3]/trans[5], trans[4]/trans[5]])).astype(int)), "real projection coordinates %s"%str(numpy.array([x_left,y_left,x_right,y_right]))

    x_cam=x[0];
    y_cam=x[1];
    z_cam=abs(x[2]); #The abs is necessary, otherwise the robot could think that the LED is behind the camera

    # Now we now the location of the LED relative to left_camera_optical_frame. We need to transform that with a quaternion 
    # to the head frame.

if __name__ == '__main__':
    rospy.init_node('image_inversion')
    rospy.Subscriber("LED_data", Float32MultiArray, inverter) #This gets the LED location on the camera and transforms the data to a real coordinate system of left_camera_optical_frame

    #to publish all the stufff
    pub = rospy.Publisher('srcsim/vision/light', Console, queue_size=1)
    msg=Console() #the message to be published
    x_cam_old=0; y_cam_old=0; z_cam_old=0; rgb_r_old=0; rgb_g_old=0; rgb_b_old=0;

    #will need it for the coordinate transformation
    whatever=tf.TransformerROS()
    listener=tf.TransformListener()
    listener.waitForTransform("left_camera_optical_frame", "head", rospy.Time(), rospy.Duration(10.0))
    print "transformation channel is getting set up, wait a few secs"

    rate = rospy.Rate(9)
    while not rospy.is_shutdown():
         try:
             #these are needed to transform to head from camera
             cam_header = Header(stamp=rospy.Time.now(), frame_id="left_camera_optical_frame")
             cam_point = Point(x_cam, y_cam, z_cam)
             cam_point_stamped = PointStamped(header=cam_header, point=cam_point)
             head_point=listener.transformPoint("head", cam_point_stamped)
             if (abs(x_cam)>0) and (abs(y_cam)>0) and (abs(z_cam)>0):
                        msg.x=head_point.point.x; msg.y=head_point.point.y; msg.z=head_point.point.z; msg.r=rgb_r; msg.g=rgb_g; msg.b=rgb_b
                        pub.publish(msg)
                        print "published data",msg
                        x_cam_old=x_cam; y_cam_old=y_cam; z_cam_old=z_cam; rgb_r_old=rgb_r; rgb_g_old=rgb_g; rgb_b_old=rgb_b; #without this it always reports an LED multiple times :/                        
         except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
             continue

         rate.sleep()
         
