# CMake generated Testfile for 
# Source directory: /home/miki/repos/nasasrc/task1/src
# Build directory: /home/miki/repos/nasasrc/task1/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(image_pipeline/image_pipeline)
SUBDIRS(vision_opencv/opencv_tests)
SUBDIRS(vision_opencv/image_geometry)
SUBDIRS(vision_opencv/vision_opencv)
SUBDIRS(image_pipeline/camera_calibration)
SUBDIRS(vision_opencv/cv_bridge)
SUBDIRS(image_pipeline/image_proc)
SUBDIRS(image_pipeline/image_publisher)
SUBDIRS(image_pipeline/image_view)
SUBDIRS(image_pipeline/stereo_image_proc)
SUBDIRS(image_pipeline/depth_image_proc)
SUBDIRS(image_pipeline/image_rotate)
SUBDIRS(vision)
