# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/miki/repos/nasasrc/task1/src/image_pipeline/image_proc/test/test_rectify.cpp" "/home/miki/repos/nasasrc/task1/build/image_pipeline/image_proc/test/CMakeFiles/image_proc_test_rectify.dir/test_rectify.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"image_proc\""
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/miki/repos/nasasrc/task1/build/gtest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/home/miki/repos/nasasrc/task1/build/vision_opencv/cv_bridge/src/CMakeFiles/cv_bridge.dir/DependInfo.cmake"
  "/home/miki/repos/nasasrc/task1/build/vision_opencv/image_geometry/CMakeFiles/image_geometry.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/miki/repos/nasasrc/task1/devel/include"
  "/home/miki/repos/nasasrc/task1/src/image_pipeline/image_proc/include"
  "/home/miki/repos/nasasrc/task1/src/vision_opencv/cv_bridge/include"
  "/home/miki/repos/nasasrc/task1/src/vision_opencv/image_geometry/include"
  "/opt/ros/indigo/include"
  "/usr/local/include"
  "/usr/local/include/opencv"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
