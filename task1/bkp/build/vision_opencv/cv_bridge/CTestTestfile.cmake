# CMake generated Testfile for 
# Source directory: /home/miki/repos/nasasrc/task1/src/vision_opencv/cv_bridge
# Build directory: /home/miki/repos/nasasrc/task1/build/vision_opencv/cv_bridge
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(python)
SUBDIRS(src)
SUBDIRS(test)
