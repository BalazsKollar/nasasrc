#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
    DESTDIR_ARG="--root=$DESTDIR"
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/miki/repos/nasasrc/task1/src/vision_opencv/cv_bridge"

# snsure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/miki/repos/nasasrc/task1/install/lib/python2.7/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/miki/repos/nasasrc/task1/install/lib/python2.7/dist-packages:/home/miki/repos/nasasrc/task1/build/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/miki/repos/nasasrc/task1/build" \
    "/usr/bin/python" \
    "/home/miki/repos/nasasrc/task1/src/vision_opencv/cv_bridge/setup.py" \
    build --build-base "/home/miki/repos/nasasrc/task1/build/vision_opencv/cv_bridge" \
    install \
    $DESTDIR_ARG \
    --install-layout=deb --prefix="/home/miki/repos/nasasrc/task1/install" --install-scripts="/home/miki/repos/nasasrc/task1/install/bin"
