import rospy
import numpy as np
from math import radians

from sensor_msgs.msg import Image, PointCloud2
from std_msgs.msg import String
from std_msgs.msg import Bool
from ihmc_msgs.msg import AbortWalkingRosMessage


from . import walker, mapper


# 1.0 means definitely safe, -1.0 is definitely unsafe, 0.0 is on a boundary or no data
# I tried 0.25 earlier, and Val was walking dangerously close to the edge a lot of the time.
# This parameter essentially controls how much "empty" space lies between the green and
# red parts of the map.
SAFETY_THRESHOLD = 0.5

# How big is Val? depends on stance, I guess. How much space do we need for
# her to walk somewhere? 0.25 was okaaay but cutting it close. The feet are only actually
# about 0.2 meters apart, I thought. But, she kinda sticks her elbows out and, and
# kept bumping them into the part of the truck that sticks out over the path.
# But, raising this too high means Val will be scared to walk anywhere.
ROBOT_WIDTH = 0.55

def score(point, target):
    return np.sqrt(np.sum((np.array(target)-point)**2,1))


class Navigator(object):

    def _current_map_image_callback(self, msg):
        self.livemap._image_callback(msg)

    def _current_map_point_cloud_callback(self, msg):
        self.livemap._point_cloud_callback(msg)

    def __init__(self):
        self.livemap = mapper.Mapper(SAFETY_THRESHOLD)
        self.walker = walker.Walker()
        self.walker.init()
        # set up subscribers for populating map
        # the callbacks that handle these are in mapper.py
        rospy.Subscriber('/multisense/camera/left/image_color', Image, self._current_map_image_callback)
        rospy.Subscriber('/multisense/image_points2', PointCloud2, self._current_map_point_cloud_callback)

        # set up subscribers for walk actions
        rospy.Subscriber("/humanz/navigate", String, self._nav_to_target_callback)
        rospy.Subscriber("/humanz/walk_path", String, self._walk_path_callback)
        rospy.Subscriber("/humanz/rotate", String, self._rotate_val_callback)
        rospy.Subscriber("/humanz/turn_right", String, self._turn_right_callback)
        rospy.Subscriber("/humanz/turn_left", String, self._turn_left_callback)
        rospy.Subscriber("/humanz/walk_if_safe", String, self._walk_if_safe_callback)
        rospy.Subscriber("/humanz/walk_straight", String, self._walk_straight_callback)
        rospy.Subscriber("/humanz/sidestep_left", String, self._sidestep_left_callback)
        rospy.Subscriber("/humanz/sidestep_right", String, self._sidestep_right_callback)
        rospy.Subscriber("/humanz/backup", String, self._backup_callback)

        # reset the map after teleportation
        rospy.Subscriber("/humanz/reset_map", String, self._reset_map_callback)

        # temporarily pause mapping (and then resume later)
        rospy.Subscriber("/humanz/map/reset", String, self._reset_map_callback)
        rospy.Subscriber("/humanz/map/pause", String, self._pause_map_callback)
        rospy.Subscriber("/humanz/map/resume", String, self._resume_map_callback)

        # run task-specific detection
        rospy.Subscriber("/humanz/task2/go_to_cart", String, self._go_to_cart_callback)
        rospy.Subscriber("/humanz/task2/go_to_panel", String, self._go_to_panel_callback)
        rospy.Subscriber("/humanz/task3/go_to_stairs", String, self._go_to_stairs_callback)
        rospy.Subscriber("/humanz/task3/align_to_wheel", String, self._align_to_wheel_callback)
        
        "-----------------------Stuff added by Miki---------------------------"
        self.wandering_state=False
        rospy.Subscriber("/humanz/wandering", Bool, self._wandering_switch_callback)
        self.abort_walk = rospy.Publisher('/ihmc_ros/valkyrie/control/abort_walking', AbortWalkingRosMessage, queue_size=10)
        "---------------------------------------------------------------------"

        # set up publisher for status messages
        self.status = rospy.Publisher('/humanz/status', String, queue_size=10)
        self.status.publish("0:waiting")
        self._KILLED = False

    def is_killed(self):
        """ Only call this if you then immediately return; checking it resets the flag """
        if self._KILLED:
            self._KILLED = False
            self.status.publish("0:navigation.py terminating callback due to killswitch")
            return True
        else:
            return False

    def is_straight_line_safe(self, start, end):
        """ Takes two 2D points [x,y] in world coordinates and checks
            if it is safe to walk there in a straight line """
        mix = np.linspace(0,1,20)
        end = np.array(end)
        start = start[:2]
        traj = (start[:,None]*(1-mix) + end[:,None]*(mix))
        traj = traj[:,:,None] + np.random.rand(2,len(mix),5)*ROBOT_WIDTH - 0.5*ROBOT_WIDTH
        traj = traj.reshape((2,-1))
        xt, yt = self.livemap.point_to_bin(traj[0], traj[1])
        res = (self.livemap.filtered_map[xt, yt])
        return np.all(res > SAFETY_THRESHOLD)

    def is_point_safe(self, point):
        """ Takes one 2d point [x,y] in world coordinates
            Returns true if it is known to be a "safe" place to 
            stand / walk """
        return self.livemap.filtered_map[self.livemap.point_to_bin(*point)] > SAFETY_THRESHOLD

    def compute_angle_and_distance(self, target, loc_and_direc=None):
        """ Computes the angle and distance to a given target point,
            relative to the robot's current position and orientation. """
        if loc_and_direc is None:
            loc, direc = self.livemap.robot_current_location()
        else:
            loc, direc = loc_and_direc
        loc = np.array(loc)[:2]
        v1 = np.array(direc)[:2] - loc
        v2 = np.array(target) - loc
        angle = np.rad2deg(np.arctan2(v2[1], v2[0]) - np.arctan2(v1[1], v1[0]))
        if angle > 180: # shouldn't be necessary if arctan2 is being sane...?
            angle -= 360
        if angle < -180:
            angle += 360
        dist = np.linalg.norm(v2)
        return angle, dist

    def rotate_val(self, target=None, angle=None):
        """ Turn the robot to look at a specified x,y point, or a specified 
            angle (in DEGREES). Will split up large angles into multiple footsteps."""
        if target is None:
            angle = angle
        elif angle is None:
            angle, _ = self.compute_angle_and_distance(target)
        if angle is not None:
            if np.abs(angle) > 90:
                self.status.publish("1:rotating %s degrees in three steps" % str(angle))
                step = radians(angle/3.)
                self.walker.rotate(step)
                self.walker.rotate(step)
                self.walker.rotate(step)
            elif np.abs(angle) > 60:
                self.status.publish("1:rotating %s degrees in two steps" % str(angle))
                step = radians(angle/2.)
                self.walker.rotate(step)
                self.walker.rotate(step)
            else:
                self.status.publish("1:rotating %s degrees in one step" % str(angle))
                self.walker.rotate(radians(angle))

    def walk_straight(self, distance):
        """ Walk in a straight line a specified distance """
        self.status.publish("1:walking forward %s meters" % distance)
        self.walker.walk_distance(distance)

    def walk_if_safe(self, target):
        """ Walk in a straight line to a specified dest [x,y] point,
            but ONLY if the robot thinks it is safe to do so.
            If the Val is unsure, she will just turn to face the point
            but not walk forward. """
        loc, direc = self.livemap.robot_current_location()
        angle, distance = self.compute_angle_and_distance(target, (loc, direc))
        is_safe = self.is_straight_line_safe(loc, target)
        if is_safe:
            self.status.publish("1:walking to %s (looks safe)" % target)
            self.rotate_val(angle=angle)
            self.walk_straight(distance)
        else:
            self.status.publish("1:does not look safe to walk to %s" % target)
            self.rotate_val(angle=angle)

    def greedy_navigator(self, loc, direc, target):
        """ This is a function which greedily will select an intermediate
            target point, choosing whatever point gets it closest to its goal,
            that it can reach by walking only in a straight line. 

            This would be terrible in something like a maze, but it is
            okay here. 
            
            If it can't find a better point than its current location,
            it returns None. """
        target = np.array(target)
        x1,y1 = self.livemap.point_to_bin(*loc[:2])
        x2,y2 = self.livemap.point_to_bin(*target[:2])
        rospy.loginfo("In map coordinates: (%d,%d)->(%d,%d)" %(x1,y1,x2,y2))
        pad = 10
        xL = min(x1,x2)
        xR = max(x1,x2)
        yT = min(y1,y2)
        yB = max(y1,y2)
        box = self.livemap.filtered_map[xL-pad:xR+pad,yT-pad:yB+pad]
        safe_points = box > 0.5
        safe_indices = np.indices(box.shape)[:,safe_points].T-pad+np.array([xL,yT])
        scores = score(safe_indices, np.array([x2,y2]))

        # now that we have scored all the points in between us and the target,
        # pick the best one which we are able to walk to
        points_to_try = np.argsort(scores)
        i = -1
        rospy.loginfo("Scanning through %d safe points of %d total considered" % (len(points_to_try), len(box.ravel())))
        for i, point in enumerate(points_to_try):
            waypoint = np.array(self.livemap.bin_to_point(*safe_indices[point]))
            if self.is_straight_line_safe(loc, waypoint):
                break
            waypoint = None
        if len(points_to_try) == 0:
            waypoint = None
        rospy.loginfo("Selected point %s (considered %d)" % (waypoint, i+1))
        # if we couldn't find a good point, or if it doesn't move us at least a tile, forget it
        # How much closer does this waypoint get us (in meters)?
        improvement = np.linalg.norm(target[:2]-loc[:2]) - np.linalg.norm(waypoint[:2]-target[:2])
        rospy.loginfo("New point will get us %0.2f meters closer to target" % improvement)
        if waypoint is None or improvement < 0.15:
            return None
        else:
            return waypoint

    def _nav_to_target_callback(self, msg):
        self._KILLED = False
        point = msg.data.strip()
        if point[0] == '[' and point[-1] == ']':
            point = point[1:-1]
        point = np.fromstring(point,sep=',')
        assert len(point) == 2
        self.nav_to_target(point)
        if self.wandering_state==False:
            self.status.publish("0: waiting")
        elif self.wandering_state==True:
            self.status.publish("1: wandering on")
            self._nav_to_target_callback(self.wandering_point())

    def nav_to_target(self, point):
        loc, direc = self.livemap.robot_current_location()
        angle, distance = self.compute_angle_and_distance(point, (loc, direc))
        rospy.loginfo("pathfinding towards %s" % point)
        self.status.publish("1:pathfinding towards %s" % point)
        if self.is_straight_line_safe(loc, point):
            self.status.publish("1:safe to walk directly to %s" % point)
            self.rotate_val(angle=angle)
            if self.is_killed():
                return
            self.walk_straight(distance)
        else:
            trying = True
            if np.abs(angle) > 5:
                self.status.publish("1:turning towards target to get a better look")
                if self.is_killed():
                    return
                self.rotate_val(angle=angle)
            while trying:
                loc, direc = self.livemap.robot_current_location()
                angle, distance = self.compute_angle_and_distance(point, (loc,direc))
                if self.is_straight_line_safe(loc, point):
                    self.status.publish("1:walking to destination %s" % point)
                    if self.is_killed():
                        return
                    self.rotate_val(angle=angle)
                    if self.is_killed():
                        return
                    self.walk_straight(distance)
                    trying = False
                else:
                    waypoint = self.greedy_navigator(loc, direc, point)
                    if waypoint is None:
                        self.status.publish("1:unable to find a path to %s; giving up" % point)
                        trying = False
                    else:
                        # walk to waypoint; turn to face target
                        self.status.publish("1:headed to intermediate location %s" % waypoint)
                        angle, distance = self.compute_angle_and_distance(waypoint, (loc,direc))
                        if self.is_killed():
                            return
                        self.rotate_val(angle=angle)
                        # TODO: I don't trust the waypointing --- since it tries
                        # to go as far as it can, it ends up suggestion points
                        # which are right on boundaries, which can then cause
                        # problems when it is time to turn. So, as a simple option
                        # we can just walk only 90% of the way there, for whatever
                        # Val suggested.
                        if self.is_killed():
                            return
                        self.walk_straight(0.9*distance)
                        angle, distance = self.compute_angle_and_distance(point)
                        if self.is_killed():
                            return
                        self.rotate_val(angle=angle)

    def _rotate_val_callback(self, msg):
        self._KILLED = False
        point = msg.data.strip()
        if point[0] == '[' and point[-1] == ']':
            point = point[1:-1]
        point = np.fromstring(point,sep=',')
        assert len(point) == 2
        rospy.loginfo("rotating towards %s" % point)
        self.rotate_val(target=point)
        self.status.publish("0:waiting")

    def _turn_left_callback(self, msg):
        self._KILLED = False
        angle = float(msg.data.strip())
        rospy.loginfo("turning left %0.2f degrees" % angle)
        self.rotate_val(angle=angle)
        self.status.publish("0:waiting")

    def _turn_right_callback(self, msg):
        self._KILLED = False
        angle = float(msg.data.strip())
        rospy.loginfo("turning right %0.2f degrees" % angle)
        self.rotate_val(angle=-angle)
        self.status.publish("0:waiting")

    def _walk_if_safe_callback(self, msg):
        self._KILLED = False
        point = msg.data.strip()
        if point[0] == '[' and point[-1] == ']':
            point = point[1:-1]
        point = np.fromstring(point,sep=',')
        assert len(point) == 2
        self.walk_if_safe(point)
        self.status.publish("0:waiting")

    def _walk_straight_callback(self, msg):
        self._KILLED = False
        distance = float(msg.data.strip())
        self.status.publish("1:walking forward %0.2f meters" % distance)
        self.walk_straight(distance)
        self.status.publish("0:waiting")

    def _walk_path_callback(self, msg):
        self._KILLED = False
        rawpath = msg.data.strip()
        path = rawpath.split(';')
        path = np.array([[float(a) for a in point.split(',')] for point in path])
        self.status.publish("1:walking path %s, %d points"%(rawpath,path.shape[0]))
        for i, point in enumerate(path):
            if self.is_killed():
                return
            self.status.publish("1:(step %d of %d) headed towards (%0.2f,%0.2f)"%(i+1,path.shape[0],point[0],point[1]))
            angle, distance = self.compute_angle_and_distance(point)
            self.rotate_val(angle=angle)
            if self.is_killed():
                return
            self.walk_straight(distance)
        self.status.publish("1:walking path complete")
        self.status.publish("0:waiting")

    def _go_to_cart_callback(self, msg):
        """ Send any non-empty string to walk towards the cart """
        if len(msg.data) == 0:
            return # TODO: maybe make this cancel?
        self._KILLED = False
        dest, look_at = self.livemap.find_task2_cart()
        loc, direc = self.livemap.robot_current_location()
        if self.is_straight_line_safe(loc, dest):
	    self.status.publish("1:approaching cart, cart is near %s"%look_at)
	    self.nav_to_target(dest)
        else:
            self.rotate_val(look_at)
            if self.livemap.task2_red_button is None:
                # walk to initial estimate, can't see the button
	        self.status.publish("1:approaching cart, cart is near %s"%look_at)
		self.nav_to_target(dest)
                self.status.publish("1:turning to look at cart at %s"%look_at)
                self.rotate_val(look_at)
                self.status.publish("1:backing up for better view")
                self.walker.backup()
	        self.walker.look_around()
	        self.status.publish("1:looking down at cart")
        if self.livemap.task2_red_button is not None:
            # refine position using red button estimated (x,y)
            button_loc = self.livemap.task2_red_button
            new_dest = self.livemap.find_nearest_walkway(button_loc)
            self.status.publish("1:lining up with red button")
            self.nav_to_target(new_dest)
            self.rotate_val(button_loc)
	    self.walker.look_around()
	    self.status.publish("1:looking down at cart")
        self.status.publish("0:waiting")

    def _go_to_panel_callback(self, msg):
        """ Send any non-empty string to walk towards the solar panel plug.
            Only works if you already spotted the plug while walking towards
            the cart!! """
        if len(msg.data) == 0:
            return # TODO: maybe make this cancel?
        self._KILLED = False
        self.walker.look_down()
        if not self.livemap.run_task2_detectors:
            self.livemap.find_task2_cart()
            self.status.publish("2:ERROR did we skip the cart? starting to look for solar panel, turn to face the blue cable (manually) and call again to walk")
        elif self.livemap.task2_blue_cable is None:
            self.livemap.find_task2_cart()
            self.status.publish("2:ERROR location of solar panel is unknown")
        else:
            panel_loc = self.livemap.task2_blue_cable
            self.status.publish("1:Walking towards solar panel at %s"%panel_loc)
            new_dest = self.livemap.find_nearest_walkway(panel_loc)
            self.status.publish("1:Going to stand at %s"%new_dest)
            self.nav_to_target(new_dest)
            self.rotate_val(panel_loc)
            # TODO this will need some refinement; need to sidestep either left
            #      or right, depending which side of the cable the flat table is on
        self.status.publish("0:waiting")

    def _go_to_stairs_callback(self, msg):
        if len(msg.data) == 0:
            return # TODO: maybe make this cancel?
        self._KILLED = False
        if not self.livemap.run_task3_detectors:
            self.livemap.task3_init()
        self.walker.rate.sleep()
        self.livemap.update_task3_detectors()
        stair1 = self.livemap.task3_stair1
        stair2 = self.livemap.task3_stair2
        self.status.publish("2:bottom two stairs near points %s and %s"%(stair1,stair2))
        if stair1 is None or stair2 is None:
           self.status.publish("2:ABORT, both stairs not yet visible; try waiting a moment, or taking a step forward")
           self.status.publish("0:waiting")
           return
        loc, direc = self.livemap.robot_current_location()
        is_good, dest, stair_base = self.livemap.find_task3_stairs()
        dist_to_stairs = np.sqrt(np.sum((dest - loc[:2])**2))
        if dist_to_stairs > 3:
            self.status.publish("1:Stairs are far away (%0.2f meters), walking closer for better look"%dist_to_stairs)
            # TODO this makes sense if we are still in the startbox, otherwise maybe not the best choice
            distance = 1.0
            waypoint = loc[:2] + (dest-loc[:2])*distance/dist_to_stairs
            self.walk_if_safe(waypoint)
            self.livemap.update_task3_detectors()
	    is_good, dest, stair_base = self.livemap.find_task3_stairs()
        else:
            self.status.publish("1:Turning towards estimated stair position for better look")
            self.rotate_val(stair_base)
            self.livemap.update_task3_detectors()
            is_good, dest, stair_base = self.livemap.find_task3_stairs()
        # now, let's very gently walk towards our current guess
        self.status.publish("2:Initial stair estimate quality is %s" % ("good" if is_good else "bad"))
        loc, direc = self.livemap.robot_current_location()
        waypoint = self.greedy_navigator(loc, direc, dest)
        if self.is_killed():
            return
        self.status.publish("1:Approaching stairs, headed towards %s"%waypoint)
        self.rotate_val(target=waypoint)
        walk_dist = np.sqrt(np.sum((waypoint-loc[:2])**2))
        new_walk_dist = max(0.5*walk_dist, min(0.4, walk_dist))
        if self.is_killed():
            return
        self.walk_straight(new_walk_dist)
        # let's see whether or not we now have a better estimate
        new_stair_estimate = self.livemap.find_task3_stairs()
        if is_good and not new_stair_estimate[0]:
            self.status.publish("2:New estimate is worse; were stairs visible during approach?")
        else:
            is_good, dest, stair_base = new_stair_estimate
            self.status.publish("2:Updated stair estimate quality is %s" % ("good" if is_good else "bad"))
        self.status.publish("1:Walking towards stairs at %s"%stair_base)
        self.status.publish("1:Going to stand at %s"%dest)
        self.nav_to_target(dest)
        if self.is_killed():
            return
        self.rotate_val(stair_base)
        # final rotation
        loc, direc = self.livemap.robot_current_location()
        v = self.livemap.find_nearest_45deg_vec((direc-loc)[:2])
        angle, _ = self.compute_angle_and_distance(loc[:2] + v, loc_and_direc=(loc,direc))
        self.status.publish("1:Rotating %0.2f degrees in an attempt to line up directly with base of stairs"%angle)
        if self.is_killed():
            return
        self.rotate_val(angle=angle)
        self.status.publish("0:waiting")

    def _align_to_wheel_callback(self, msg):
        if len(msg.data) == 0:
            return # TODO: maybe make this cancel?
        self._KILLED = False
        loc, direc = self.livemap.robot_current_location()
        v = self.livemap.find_nearest_45deg_vec((direc-loc)[:2])
        angle, _ = self.compute_angle_and_distance(loc[:2] + v, loc_and_direc=(loc,direc))
        self.status.publish("1:Rotating %0.2f degrees to square off with door"%angle)
        self.rotate_val(angle=angle)
        # compute how much we need to sidestep
        loc, direc = self.livemap.robot_current_location()
        wheel_location = self.livemap.find_task3_wheel()
        a = (direc[:2] - loc[:2])
        b = (wheel_location - loc[:2])
        a /= np.sqrt(np.sum(a**2))
        b /= np.sqrt(np.sum(b**2))
        orthog = a[::-1] * np.array([-1,1])
        # need to know both distance and direction
        wheel_dist = np.sqrt(np.sum((wheel_location-loc[:2])**2))
        sidestep_dist = np.cos(np.pi/2 - np.arccos(np.dot(a, b))) * wheel_dist
        print sidestep_dist
        options = np.array([[-1,1]]).T*sidestep_dist*orthog + np.sqrt(wheel_dist**2 - sidestep_dist**2)*a
        print options
        move_right = np.argmin(np.sqrt(np.sum((options - wheel_location)**2))) == 0
        if move_right:
            self.status.publish("1:Sidestep to right to line up with wheel")
        else:
            self.status.publish("1:Sidestep to left to line up with wheel")
        num_steps = int(np.ceil(sidestep_dist / 0.2))
        each_step_dist = sidestep_dist / num_steps
        self.status.publish("1:Moving %0.2f meters in %d steps"%(sidestep_dist, num_steps))
        for i in xrange(num_steps):
            if move_right:
                sidestep_right(each_step_dist)
            else:
                sidestep_left(each_step_dist)
        self.status.publish("0:waiting")

    def _reset_map_callback(self, msg):
        """ RESET THE MAP: if the string begins with the letter `t`, then assume we
            are in a start box """
        if len(msg.data) == 0:
            return
        self._KILLED = False
        in_start_box = (msg.data[0] == "t")
        self.status.publish("2:FULL RESET of map (presumably after teleport)")
        self.livemap = mapper.Mapper(SAFETY_THRESHOLD, in_start_box=in_start_box)
        self.status.publish("1:setting robot pose")
        self.walker.set_init_pose()
        self.walker.raise_pelvis()
        self.walker.rate.sleep()
        self.walker.look_around()
        self.walker.look_down()
        self.walker.rate.sleep()
        self.status.publish("0:waiting")

    def _pause_map_callback(self, msg):
        """ Pause mapping temporarily """
        if len(msg.data) == 0:
            return
        self._KILLED = False
        self.livemap.POINT_DISCARD = np.Inf
        self.status.publish("0:map updates paused; map will not change")

    def _resume_map_callback(self, msg):
        """ Resume mapping """
        if len(msg.data) == 0:
            return
        self._KILLED = False
        self.livemap.POINT_DISCARD = 5
        self.livemap._discard = 5 # this means it will update on next point cloud message
        self.status.publish("0:map updates resumed")

    def _sidestep_left_callback(self, msg):
        """ Sidestep left """
        if len(msg.data) == 0:
            return
        self._KILLED = False
        distance = float(msg.data)
        self.status.publish("1:sidestep to left %0.2f meters" % distance)
        self.walker.sidestep_left(distance)
        self.status.publish("0:waiting")
        
    def _sidestep_right_callback(self, msg):
        """ Sidestep right """
        if len(msg.data) == 0:
            return
        self._KILLED = False
        distance = float(msg.data)
        self.status.publish("1:sidestep to right %0.2f meters" % distance)
        self.walker.sidestep_right(distance)
        self.status.publish("0:waiting")

    def _backup_callback(self, msg):
        if len(msg.data) == 0:
            return
        self._KILLED = False
        self.status.publish("1:backing up one small step")
        self.walker.backup()
        self.status.publish("0:waiting")


    "---------------------Functions added by Miki-----------------------------"
    def _wandering_switch_callback(self, msg):
        self._KILLED = False
        if msg.data==False:
            self._KILLED = True
            self.status.publish("2:navigation.py recieved killswitch")
            self.wandering_state=msg.data
            self.abort_walk.publish(123123123)
            #self.walker.stand_straight()
            self.status.publish("0: full stop of wandering")
        elif self.wandering_state==False and msg.data==True:
            self.wandering_state=msg.data
            self.status.publish("1: starting to wander")
            self._nav_to_target_callback(self.wandering_point())
        elif self.wandering_state==True and msg.data==True:
            self.status.publish("1: wandering on")
            self._nav_to_target_callback(self.wandering_point())
        
    def wandering_point(self):        
        unexplored_path = np.multiply(self.livemap.filtered_map > SAFETY_THRESHOLD,self.livemap.memory_loc==False)
        if np.all(-unexplored_path):
            self.status.publish("0: nowhere to wander to on current map")
        else:
            rownum,columnum=unexplored_path.shape
            unexplored_path[0:rownum,0:columnum]
            zero_row=np.matrix(np.zeros(shape=(1,columnum)))
            zero_col=np.matrix(np.zeros(shape=(rownum,1)))
            for i in range(0,10):
                unexplored_path=np.multiply(unexplored_path+np.concatenate((zero_row,unexplored_path[0:rownum-1,0:columnum]))+np.concatenate((unexplored_path[1:rownum,0:columnum],zero_row))+np.concatenate((zero_col.transpose(),unexplored_path[0:rownum,0:columnum-1].transpose())).transpose()+np.concatenate((unexplored_path[0:rownum,1:columnum].transpose(),zero_col.transpose())).transpose(),unexplored_path>0)
            max_index=np.argmax(unexplored_path)
            row_index=np.floor_divide(max_index,columnum)
            col_index=max_index-row_index*columnum
            # uncomment this to see on the map where the robot will be sent to
            self.livemap.memory_loc[row_index,col_index]=True
            xw, yw = self.livemap.bin_to_point(row_index, col_index)
            msg_stamped = String(data="["+str(xw)+","+str(yw)+"]")
            return msg_stamped
    
    "-------------------------------------------------------------------------"
