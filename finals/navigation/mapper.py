import rospy 
import tf
import tf2_ros
from cv_bridge import CvBridge
from sensor_msgs.msg import Image, PointCloud2

from scipy.ndimage.filters import uniform_filter

import numpy as np

robot_name = rospy.get_param('/ihmc_ros/robot_name')
LEFT_FOOT_FRAME_NAME = rospy.get_param("/ihmc_ros/{0}/left_foot_frame_name".format(robot_name))
RIGHT_FOOT_FRAME_NAME = rospy.get_param("/ihmc_ros/{0}/right_foot_frame_name".format(robot_name))


### DETECTION CODE FOR SURFACES WE INTERACT WITH ###

BOTTOM_STEP_HEIGHT = 0.13
SECOND_STEP_HEIGHT = 0.33

def find_at_height(points, height, noise=0.02):
    right_height = np.logical_and(points[:,:,2]< height + noise, points[:,:,2]> height - noise)
    possible_x = points[:,:,0][right_height]
    possible_y = points[:,:,1][right_height]
    diameter = 1.25 # all surfaces we are looking for are pretty local
    valid = np.abs(points[:,:,0] - possible_x.mean()) < diameter
    valid = valid * (np.abs(points[:,:,1] - possible_y.mean()) < diameter)
    return right_height * valid

def xy_from_image(points, which):
    return points[:,:,0][which], points[:,:,1][which]

def dmin(x, default):
    if len(x) > 0:
        return x.min()
    else:
        return default

def dmax(x, default):
    if len(x) > 0:
        return x.max()
    else:
        return default

def find_wheel_in_image(loc, direc, im, points):
    valid = ~np.isnan(points[:,:,0])
    likely_loc = loc + 2.0 * (direc - loc) / np.sqrt(np.sum((direc - loc)**2))
    likely_loc[2] += 1.0
    dist = points[:,:,:] - likely_loc
    dist = np.sqrt(np.sum(dist**2, 2))
    is_grey = ((im[:,:,0]*1.0 - im[:,:,1]*1.0) < 10) * ((im[:,:,0]*1.0 - im[:,:,2]*1.0) < 10)
    dist = (dist < 2.0)
    where = 1.0*(dist * is_grey)
    filtered = uniform_filter(where, size=100, mode='constant')
    return filtered

def get_cart_center(im, points):
    cart_edge = (points[:,:,2] > 0.8) * (points[:,:,2] < 0.9)
    wheels = (im.mean(2) < 40) * cart_edge
    light_border = (im.min(2) > 150) * cart_edge
    border_point_count = light_border.sum()
    if border_point_count > 30:
        cart_center = points[:,:,:2][light_border].mean(0)
    elif border_point_count > 0:
        cart_center = 0.8*points[:,:,:2][light_border].mean(0) + 0.2*points[:,:,:2][wheels].mean(0)
    else:
        cart_center = points[:,:,:2][wheels].mean(0)
    return cart_center, wheels, light_border

def get_red_button(im, points):
    pixels = ((1.0*im[:,:,0] - 1.0*im[:,:,1]) > 100) * ((1.0*im[:,:,0] - 1.0*im[:,:,2]) > 100)
    valid_height = (points[:,:,2] > 0.81) * (points[:,:,2] < 0.86)
    return pixels * valid_height

def get_blue_cable(im, points):
    pixels = ((1.0*im[:,:,2] - 1.0*im[:,:,1]) > 100) * ((1.0*im[:,:,2] - 1.0*im[:,:,0]) > 100)
    valid_height = (points[:,:,2] > 0.8) * (points[:,:,2] < 0.85)
    return pixels * valid_height

### END DETECTION CODE ###

def init_bounds(bounds):
    assert bounds[1] > bounds[0]
    dim = np.abs(bounds[1]-bounds[0])/4
    return np.array([bounds[0]-dim, bounds[1]+dim])

def find_walkway_xy(im, points):
    """ Detector for walkway. """
    valid = ~np.isnan(points[:,:,0])
    points = points[valid]
    im = im[valid]
    assert points.ndim == 2
    is_ground = (points[:,2] > 0.0) * (points[:,2] < 0.1)
    is_flat_color = (im[:,0] == im[:,1]) * (im[:,1] == im[:,2])
    is_red = im[:,0] - im[:,1:].mean(-1) > 40
    walkway = is_ground * is_flat_color
    danger = np.logical_or((points[:,2] < 0.1) * is_red, points[:,2] >= 0.1)
    return points[walkway,0], points[walkway,1], points[danger,0], points[danger,1]


class Mapper(object):
    """ Build up a grid map of the world. The basic idea is we break things
        down into squares, listen to the point cloud data, and record which
        particular points look safe to stand on. """
    
    RESOLUTION = 0.1
    
    def __init__(self, threshold, in_start_box=True):
        self.tfBuffer = tf2_ros.Buffer()
        self.tfListener = tf2_ros.TransformListener(self.tfBuffer)
        rospy.Rate(1).sleep()
        self.read_xyz_rgb()
        self._build_initial_map(self.im, self.points, in_start_box)
        self._discard = 0
        self.threshold = threshold
        self.run_task2_detectors = False
        self.run_task3_detectors = False
        self._map_publisher = rospy.Publisher('/humanz/livemap', Image, queue_size=1)
        #rendered = np.array(self.render(), dtype=np.uint8)
        #self._map_publisher.publish(CvBridge().cv2_to_imgmsg(rendered))

    def task2_init(self):
        self.task2_red_button = None
        self.task2_blue_cable = None
        self.run_task2_detectors = True

    def task3_init(self):
        self.task3_stair1 = None
        self.task3_stair2 = None
        self.run_task3_detectors = True

    def update_task2_detectors(self):
        # red button
        red_button = get_red_button(self.im, self.points)
        if len(red_button[0]) > 0:
           red_button_est = np.mean(xy_from_image(self.points, red_button), 1)
           if np.all(np.isfinite(red_button_est)):
               if self.task2_red_button is None:
                   self.task2_red_button = red_button_est
               else:
                   self.task2_red_button = 0.9*self.task2_red_button + 0.1*red_button_est
        # blue cable
        blue_cable = get_blue_cable(self.im, self.points)
        if len(blue_cable[0]) > 0:
           blue_cable_est = np.mean(xy_from_image(self.points, blue_cable), 1)
           if np.all(np.isfinite(blue_cable_est)):
               if self.task2_blue_cable is None:
                   self.task2_blue_cable = blue_cable_est
               else:
                   self.task2_blue_cable = 0.9*self.task2_blue_cable + 0.1*blue_cable_est

    def update_task3_detectors(self, min_stair_points=20):
        # bottom step
        stair1 = find_at_height(self.points, BOTTOM_STEP_HEIGHT)
        if stair1.sum() > min_stair_points:
            new_est = self.points[:,:,:2][stair1].mean(0)
            if np.all(np.isfinite(new_est)):
                if self.task3_stair1 is None:
                    self.task3_stair1 = new_est
                else:
                    self.task3_stair1 = 0.9*self.task3_stair1 + 0.1*new_est
        # second step
        stair2 = find_at_height(self.points, SECOND_STEP_HEIGHT)
        if stair2.sum() > min_stair_points:
            new_est = self.points[:,:,:2][stair2].mean(0)
            if np.all(np.isfinite(new_est)):
                if self.task3_stair2 is None:
                    self.task3_stair2 = new_est
                else:
                    self.task3_stair2 = 0.9*self.task3_stair2 + 0.1*new_est

    def read_xyz_rgb(self):
        """ Read an process a single point cloud / image pair.
            Don't call this except for when testing!! In general, use the callbacks instead. """
        im_msg = rospy.wait_for_message("/multisense/camera/left/image_color", Image)
        points_msg = rospy.wait_for_message("/multisense/image_points2", PointCloud2)
        im = np.fromstring(im_msg.data, dtype=np.uint8).reshape((im_msg.height, im_msg.width,3))[::-1,::-1]
        points = np.fromstring(points_msg.data, dtype=np.float32).reshape((im_msg.height, im_msg.width,-1))[::-1,::-1]
        
        # point cloud is in left camera reference frame
        camera2world = self.tfBuffer.lookup_transform('world', 'left_camera_optical_frame', rospy.Time(0))
        t = camera2world.transform
        quat = np.array([t.rotation.x,t.rotation.y,t.rotation.z,t.rotation.w])
        R = tf.transformations.euler_matrix(*tf.transformations.euler_from_quaternion(quat))
        trans = np.array([t.translation.x,t.translation.y,t.translation.z])
        new_points = np.dot(R[:3,:3], points[:,:,:3].reshape((-1,3)).T).T + trans

        self.im = im
        self.points = new_points.reshape(im.shape)

    def _image_callback(self, im_msg):
        """ Store (and do nothing with) images as they arrive.
            When a point cloud arrives, we'll use it in tandem with the most recent image. """
        self._im_msg = im_msg

    POINT_DISCARD = 5
    def _point_cloud_callback(self, points_msg):
        """ On every nth point cloud message, update the map and the local status

            Store in self.im and self.points """
        if self._discard < self.POINT_DISCARD:
            # Adjust to control how often we update the map
            self._discard += 1
            return
        self._discard = 0
        rospy.loginfo("processing a point cloud in callback")
        im_msg = self._im_msg
        im = np.fromstring(im_msg.data, dtype=np.uint8).reshape((im_msg.height, im_msg.width,3))[::-1,::-1]
        points = np.fromstring(points_msg.data, dtype=np.float32).reshape((im_msg.height, im_msg.width,-1))[::-1,::-1]
        
        # point cloud is in left camera reference frame
        camera2world = self.tfBuffer.lookup_transform('world', 'left_camera_optical_frame', rospy.Time(0))
        t = camera2world.transform
        quat = np.array([t.rotation.x,t.rotation.y,t.rotation.z,t.rotation.w])
        R = tf.transformations.euler_matrix(*tf.transformations.euler_from_quaternion(quat))
        trans = np.array([t.translation.x,t.translation.y,t.translation.z])
        new_points = np.dot(R[:3,:3], points[:,:,:3].reshape((-1,3)).T).T + trans

        self.im = im
        self.points = new_points.reshape(im.shape)
        self._run_map_update(self.im, self.points)

    def _build_initial_map(self, im, points, in_start_box):
        """ Needs an initial detection to start the map """
        safe_x, safe_y, danger_x, danger_y = find_walkway_xy(im, points)
        self.x_bounds = init_bounds([min(-1.5, dmin(danger_x,-1.5), dmin(safe_x,-1.5)), 
                                     max(1.5, dmax(danger_x,1.5), dmax(safe_x,1.5))]).round(1)
        self.y_bounds = init_bounds([min(-1.5, dmin(danger_y,-1.5), dmin(safe_y,-1.5)), 
                                     max(1.5, dmax(danger_y,1.5), dmax(safe_y,1.5))]).round(1)
        xdim = int((self.x_bounds[1]-self.x_bounds[0])/self.RESOLUTION)
        ydim = int((self.y_bounds[1]-self.y_bounds[0])/self.RESOLUTION)
        self.DETECT = np.zeros((xdim, ydim), dtype=int)
        self.DANGER = np.zeros((xdim, ydim), dtype=int)
        self.LIVE = np.zeros((xdim, ydim), dtype=int)
        xc, yc = self.point_to_bin(safe_x, safe_y)
        self.DETECT[xc,yc] = True
        self.LIVE[xc,yc] = True
        xc, yc = self.point_to_bin(danger_x, danger_y)
        self.DANGER[xc,yc] = True
        self.DETECT *= np.logical_not(self.DANGER)
        self.LIVE *= np.logical_not(self.DANGER)
        if in_start_box:
            box_dim = 1.45*np.array([-1,1])
        else:
            # tiny fake-box just in case
            box_dim = 0.25*np.array([-1,1])
        start_box = self.point_to_bin(box_dim,box_dim)
        assert start_box[0][0] >= 0
        assert start_box[1][0] >= 0
        self.DETECT[start_box[0][0]+1:start_box[0][1]+1,start_box[1][0]+1:start_box[1][1]-1] = True
        #run_detectors(self.LOCATIONS, points, self.point_to_bin)
        self.filtered_map = self._smooth_map()
        "------------------------------Stuff added by Miki--------------------"
        self.memory_loc=np.zeros(self.filtered_map.shape)
        loc, direc = self.robot_current_location()
        xr, yr = self.point_to_bin(*loc[:2])
        self.memory_loc[xr-18:xr+18,yr-18:yr+18] = True
        self.memory_loc[start_box[0][0]+1:start_box[0][1]+1,start_box[1][0]+1:start_box[1][1]-1] = True
        xd, yd = self.point_to_bin(*direc[:2])
        nx,ny = self.filtered_map.shape
        jdiff,idiff = np.ogrid[-xr:nx-xr,-yr:ny-yr]
        mask=(jdiff*jdiff+idiff*idiff<120)
        self.DETECT[mask] = True
        self.filtered_map = self._smooth_map()
        "---------------------------------------------------------------------"

    def _check_bounds(self, new_x, new_y):
        if len(new_x) == 0:
            return True
        else:
            return (new_x.min() >= self.x_bounds[0] and new_x.max() < self.x_bounds[1]
                    and new_y.min() >= self.y_bounds[0] and new_y.max() < self.y_bounds[1])
    
    def _double_map_size(self):
        """ This is all super annoying. The map will auto-expand if necessary, 
            as Val explores. """
        dimx, dimy = self.DETECT.shape
        pady = int(dimy/2)
        padx = int(dimx/2)
        new_detections = np.zeros((dimx+2*padx,dimy+2*pady), dtype=int)
        new_danger = np.zeros_like(new_detections)
        new_detections[padx:(padx+dimx),pady:(pady+dimy)] = self.DETECT
        new_danger[padx:(padx+dimx),pady:(pady+dimy)] = self.DANGER
        self.x_bounds[0] -= padx*self.RESOLUTION
        self.x_bounds[1] += padx*self.RESOLUTION
        self.y_bounds[0] -= pady*self.RESOLUTION
        self.y_bounds[1] += pady*self.RESOLUTION
        self.DETECT = new_detections
        self.DANGER = new_danger
        self.LIVE = np.zeros_like(self.DETECT)
        """----------------------------Stuff added by Miki------------------"""
        new_memory_loc = np.zeros_like(new_detections)
        new_memory_loc[padx:(padx+dimx),pady:(pady+dimy)] = self.memory_loc
        self.memory_loc = new_memory_loc
        "---------------------------------------------------------------------"
        #new_locations = np.zeros_like(new_detections)
        #new_locations[padx:(padx+dimx),pady:(pady+dimy)] = self.LOCATIONS
        #self.LOCATIONS = new_locations

    def update(self):
        """ For use in testing only. Use callbacks instead! """
        self.read_xyz_rgb()
        self._run_map_update(self.im, self.points)
        
    def _run_map_update(self, im, points):
        safe_x, safe_y, danger_x, danger_y = find_walkway_xy(im, points)
        self.LIVE.fill(False)
        while not self._check_bounds(safe_x, safe_y):
            self._double_map_size()
        while not self._check_bounds(danger_x, danger_y):
            self._double_map_size()
        xd, yd = self.point_to_bin(danger_x, danger_y)
        self.DANGER[xd,yd] = True
        xc, yc = self.point_to_bin(safe_x, safe_y)
        self.LIVE[xc,yc] = True
        self.LIVE[xd,yd] = False
        self.DANGER = np.logical_and(self.DANGER, np.logical_not(self.LIVE)) # allows recovery from bad data
        self.DETECT = np.logical_and(np.logical_or(self.LIVE, self.DETECT), np.logical_not(self.DANGER))
        #run_detectors(self.LOCATIONS, points, self.point_to_bin)
        if self.run_task2_detectors:
            self.update_task2_detectors()
        if self.run_task3_detectors:
            self.update_task3_detectors()
        self.filtered_map = self._smooth_map()
        """----------------------------Stuff added by Miki------------------"""
        loc, direc = self.robot_current_location()
        xr, yr = self.point_to_bin(*loc[:2])
        xd, yd = self.point_to_bin(*direc[:2])
        nx,ny = self.memory_loc.shape
        jdiff,idiff = np.ogrid[-xr:nx-xr,-yr:ny-yr]
        mask=(jdiff*np.full(jdiff.shape,(xd-xr))+idiff*np.full(idiff.shape,(yd-yr))<0)*(jdiff*jdiff+idiff*idiff<140)
        self.memory_loc[mask] = True
        "---------------------------------------------------------------------"
        rendered = np.array(self.render()[:,:,::-1]*255, dtype=np.uint8)
        #self._map_publisher.publish(CvBridge().cv2_to_imgmsg(rendered))
        rospy.loginfo("publishing map")
        self._map_publisher.publish(Image(data=rendered.ravel().tolist(),
                                          encoding='8UC3',
                                          height=rendered.shape[0],width=rendered.shape[1]))
        
    def point_to_bin(self, x, y):
        """ Convert point to map bin coordinates """
        xc = np.array((x - self.x_bounds[0])/self.RESOLUTION, dtype=int)
        yc = np.array((y - self.y_bounds[0])/self.RESOLUTION, dtype=int)
        return xc, yc

    def bin_to_point(self, x, y):
        """ Convert bin to map point.
            Attempts to give coordinates of the center of the bin... """
        xw = self.RESOLUTION*x + self.x_bounds[0] + 0.5*self.RESOLUTION
        yw = self.RESOLUTION*y + self.y_bounds[0] + 0.5*self.RESOLUTION
        return xw, yw

    def robot_current_location(self):
        """ Returns two points: one is the center of the two feet,
                                the other is a second point due forward """
        pos = []
        direc = []
        for foot in (LEFT_FOOT_FRAME_NAME, RIGHT_FOOT_FRAME_NAME):
            t = self.tfBuffer.lookup_transform('world', foot, rospy.Time(0)).transform
            quat = np.array([t.rotation.x,t.rotation.y,t.rotation.z,t.rotation.w])
            R = tf.transformations.euler_matrix(*tf.transformations.euler_from_quaternion(quat))
            trans = np.array([t.translation.x,t.translation.y,t.translation.z])
            pos.append(np.dot(R[:3,:3], np.zeros(3)) + trans)
            direc.append(np.dot(R[:3,:3], np.array([0.25,0.,0.])) + trans)
        return np.mean(pos, 0), np.mean(direc, 0)

    def find_nearest_walkway(self, target, radius=3):
        """ Return a point on the walkway which is "near" a particular off-walkway
            target point. Considers only locations at 45 degree angles from target. """
        all_valid = []
        for i in (-1,0,1):
            for j in (-1,0,1):
                if i == 0 and j == 0:
                    continue
                testpath = target + (np.array([[i,j]]).T*np.linspace(0,radius,100)).T
                dest = self.point_to_bin(testpath[:,0], testpath[:,1])
                which = self.filtered_map[dest[0], dest[1]] > self.threshold
                count = which.sum()
                if count > 0:
                    all_valid.append(testpath[which].mean(0))
        print "possible locations:"
        print all_valid
        distances_to_center = np.sqrt(np.sum((np.array(all_valid) - target)**2, 1))
        print "distances", distances_to_center
        destination = all_valid[np.argmin(distances_to_center)]
        return destination

    def find_nearest_45deg_vec(self, v):
        """ Given a vector v, return another vector which is a unit vector
            pointed in one of eight canonical directions """
        sim = []
        vec = []
        v = v / np.sqrt(np.sum(v**2))
        for i in (-1,0,1):
            for j in (-1,0,1):
                if i == 0 and j == 0:
                    continue
                unit = np.array([i,j]) / np.sqrt(i**2 + j**2)
                vec.append(unit)
                sim.append(np.sum(v*unit))
        return vec[np.argmax(sim)]

    def find_task2_cart(self):
        self.task2_init()
        cart_center = get_cart_center(self.im, self.points)[0]
        destination = self.find_nearest_walkway(cart_center)
        print "task 2 cart: move to", destination, "and look towards", cart_center
        return destination, cart_center

    def find_task3_stairs(self, leadup_dist=0.75):
        """ Returns a tuple containing a boolean on whether we are happy with the estimate,
            and two points:

            1) a location `distance` meters in front of the base of the staircase
            2) a point to look at which hopefully represents the center of the stairs """
        look_at = 0.5*(self.task3_stair1 + self.task3_stair2)
        v = self.task3_stair1 - self.task3_stair2
        # find the destination by looking at safe points on the nearby walkway
        walkway_dest = self.find_nearest_walkway(look_at, radius=1.0)
        # this *should* be similar to what we get from extrapolating out the means
        # of the two bottom step locations. if they aren't similar, then the estimation
        # of the step locations is not very good!
        v_reg = self.find_nearest_45deg_vec(v)
        dest = look_at + leadup_dist*v_reg
        #print "task 3 stairs: move to", dest, "and look towards", look_at
        is_dest_safe = self.filtered_map[self.point_to_bin(*dest)] >= self.threshold
        if not is_dest_safe:
            print "stair location estimate", look_at, "is too inaccurate to safely approach"
        return is_dest_safe, walkway_dest, look_at

    def find_task3_wheel(self):
        loc, direc = self.robot_current_location()
        pixels = find_wheel_in_image(loc, direc, self.im, self.points)
        where = (pixels > 0.9*pixels.max())
        wheel_location = np.array([np.nanmean(self.points[:,:,0][where]), np.nanmean(self.points[:,:,1][where])])
        return wheel_location

    def _smooth_map(self):
        """ Returns / generates a scored version of the map """
        scores = np.zeros(self.DETECT.shape)
        scores[self.DETECT > 0] = 1.0
        scores[self.DANGER > 0] = -1.0
        scores = uniform_filter(scores, size=5, mode='constant')
        return scores

#    def _filtered_location(self, identifier):
#        return (uniform_filter(1.0*(self.LOCATIONS == identifier), size=3, mode='constant') > 0.6)

#    def _estimate_location_mean_xy(self, identifier):
#        filtered_loc = self._filtered_location(identifier)
#        x, y = self.bin_to_point(*np.where(filtered_loc))
#        x_center = 0.5*(np.percentile(x, 90) + np.percentile(x, 10))
#        y_center = 0.5*(np.percentile(y, 90) + np.percentile(y, 10))
#        return np.array([x_center, y_center])

    def render(self, crop=True):
        loc, direc = self.robot_current_location()
        xr, yr = self.point_to_bin(*loc[:2])
        xd, yd = self.point_to_bin(*direc[:2])
        xdim, ydim = self.DETECT.shape
        IMAGE = np.zeros((xdim, ydim, 3), dtype=float)
        safe = self.filtered_map > self.threshold
        #warning = np.logical_and(self.filtered_map > 0.0, self.filtered_map <= 0.5)
        danger = self.filtered_map < 0
        IMAGE[safe] = np.array([0.2,0.9,0.0]) * self.filtered_map[safe][:,None]
        #IMAGE[warning] = np.array([1.,1.,0.0]) * self.filtered_map[warning][:,None]
        #IMAGE[self.LIVE > 0] = np.array([0.2,0.9,0.0])
        #IMAGE[self.LIVE > 0] = np.array([0.0, 0.9, 0.9])
        """-----------------Stuff added by Miki ----------------------------"""
        where_its_been = self.memory_loc==True
        IMAGE[where_its_been] = np.array([0,0.5,0.5]) * np.abs(self.filtered_map[where_its_been])[:,None]
        "---------------------------------------------------------------------"
        IMAGE[danger] = np.array([0.9,0.3,0.1]) * np.abs(self.filtered_map[danger])[:,None]
        IMAGE[xr-2:xr+2,yr-2:yr+2,:] = np.array([0.5,0.5,0.5])
        IMAGE[xd-1:xd+1,yd-1:yd+1,:] = np.array([0.1,0.1,0.9])
#        for i in range(1,7):
#            if (self.LOCATIONS == i).sum() > 0:
#                # add (filtered) version of locations to map
#                loc2 = self._filtered_location(i) 
#                IMAGE[loc2] = np.array([1.0,1.0,0.0])
        if crop:
            IMAGE = IMAGE[max(0,xr-100):min(xdim,xr+100),max(0,yr-150):min(ydim,yr+150),:]
        return IMAGE
        
        """-----------------Stuff added by Miki starts here-----------------"""
