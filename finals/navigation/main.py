#!/usr/bin/env python

import rospy
from . import navigation

if __name__ == '__main__':
    rospy.init_node('navigation', anonymous=True)
    controller = navigation.Navigator()
    rospy.spin()


