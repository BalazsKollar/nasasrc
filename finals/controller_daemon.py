#!/usr/bin/env python

import time
import rospy
import tf
import tf2_ros
import numpy
import copy
import sys
import subprocess
import math
import atexit
import os
import signal
import pprint

from tf2_geometry_msgs import PointStamped
import tf2_geometry_msgs


from geometry_msgs.msg import Quaternion, Transform, Vector3

from ihmc_msgs.msg import EndEffectorLoadBearingRosMessage
from ihmc_msgs.msg import FootstepStatusRosMessage
from ihmc_msgs.msg import FootstepDataListRosMessage
from ihmc_msgs.msg import FootstepDataRosMessage
from ihmc_msgs.msg import FootTrajectoryRosMessage
from ihmc_msgs.msg import PelvisHeightTrajectoryRosMessage
from ihmc_msgs.msg import TrajectoryPoint1DRosMessage
from ihmc_msgs.msg import WholeBodyTrajectoryRosMessage
from ihmc_msgs.msg import ArmTrajectoryRosMessage
from ihmc_msgs.msg import OneDoFJointTrajectoryRosMessage
from ihmc_msgs.msg import NeckTrajectoryRosMessage
from ihmc_msgs.msg import ChestTrajectoryRosMessage
from ihmc_msgs.msg import SE3TrajectoryPointRosMessage
from ihmc_msgs.msg import SO3TrajectoryPointRosMessage
from std_msgs.msg import String
from std_msgs.msg import Bool
from srcsim.msg._Console import Console
from srcsim.msg._Task import Task
from srcsim.msg import Satellite
from std_msgs.msg._Float64MultiArray import Float64MultiArray

from tf2_geometry_msgs import PoseStamped
from random import random


LEFT = 0
RIGHT = 1

ROBOT_NAME = None
LEFT_FOOT_FRAME_NAME = None
RIGHT_FOOT_FRAME_NAME = None


ZERO_VECTOR = [0.3, 1.3, -0.1, 1.3, 0.0, 0.0, 0.0]
ARM_PULLED_BACK = [-0.2, 1.4, 0.6, 1.3, 0.0, 0.0, 0.0]
ARM_PULLED_BACK_LEFT = [0.2, -1.45, 0.1, -1.65, 0.0, 0.0, 0.0]
ARM_PULLED_BACK_INIT = [0.9, 1.4, -0.1, 0.0, 0.0, 0.0, 0.0]
ARM_PULLED_BACK_INIT2 = [0.3, 1.3, -0.1, 0.65, 0.0, 0.0, 0.0]
ELBOW_BENT_UP = [-1.10, 0.85, 1.3, 0.0, 0.0, 0.0, 0.0]
ELBOW_BENT_UP_MIDDLE = [-1.10, 0.25, 1.3, 0.3, 0.0, 0.0, 0.0]
#ELBOW_BENT_UP_MIDDLE = [-0.1, 0.4, 0.56, 1.7, 0.0, 0.0, 0.0]
#touch ELBOW_BENT_UP = [1.8, -0.3, -1.5, 1.15, 0.0, 0.0, 0.0]
ELBOW_BENT_UP_INIT = [1.8, 0.4, -1.5, 0.95, 0.0, 0.0, 0.0]

ARM_DOWN_LEFT = [0.16, -1.4, 0.46, -1.7, 0.0, 0.0, 0.0]
ARM_DOWN_RIGHT = [0.16, 1.4, 0.46, 1.7, 0.0, 0.0, 0.0]
#was -0.22
ARM_DOWN_RIGHT_2 = [-0.1, 1.4, -0.18, 1.7, 0.0, 0.0, 0.0] #was [-0.1, 1.4, -0.35, 1.7, 0.0, 0.0, 0.0]
ARM_DOWN_RIGHT_3 = [-1.1, 1.5, -1.50, 1.05, 0.0, 0.0, 0.0] #not used currently- was [-1.1, 1.5, -1.55, 1.05, 0.0, 0.0, 0.0]
ARM_DOWN_RIGHT_INIT = [-1.10, 0.05, 1.3, 0.0, 0.0, 0.0, 0.0]
ARM_DOWN_STRETCHED_LEFT = [0.0, -1.4, 1.4, 0.0, 0.0, 0.0, 0.0]
ARM_DOWN_STRETCHED_RIGHT = [0.0, 1.4, 1.4, 0.0, 0.0, 0.0, 0.0]

ARM_SPREAD_LEFT = [0.16, -1.4, -0.4, -2.0, 1.0, 0.0, 0.0]
ARM_SPREAD_RIGHT = [0.16, 1.4, -0.4, 2.0, 1.0, 0.0, 0.0]

ARM_STRAIGHT_FW_LEFT = [-1.75, -1.15, 1.77, 0.0, 0.9, 0.0, 0.0]
ARM_STRAIGHT_FW_RIGHT = [-1.75, 1.15, 1.77, 0.0, 0.9, 0.0, 0.0]

LIFT_SUITCASE_LEFT = [-0.45, -1.3, 0.0, -0.8, 2.6, 0.0, 0.0]
LIFT_SUITCASE_RIGHT = [-0.45, 1.3, 0.0, 0.8, 2.6, 0.0, 0.0]
HOLD_UP_SUITCASE_LEFT = [-1.8, -1.3, 0.0, 0.0, 2.6, 0.0, 0.0]
HOLD_UP_SUITCASE_RIGHT = [-1.8, 1.3, 0.0, 0.0, 2.6, 0.0, 0.0]

ARM_STRAIGHT_FW_LEFT_CABLE = [-1.95, -1.4, 2.17, -0.8, 1.1, 0.0, 0.0]
ARM_IN_RIGHT = [0.1, 1.4, 0.52, 2.0, 0.8, 0.0, 0.0]
ARM_IN_LEFT = [0.1, -1.4, 0.52, -2.0, 0.8, 0.0, 0.0]
LIFT_CABLE_LEFT = [-1.8, -1.1, 1.8, -0.8, 2.8, -0.45, 0.0]

WHEEL_START_LEFT = [-1.74, -1.0, 2.17, -0.9, 0.0, 0.3, -0.1]
WHEEL_START_RIGHT = [-1.45, 1.0, 2.17, 0.7, -0.9, -0.4, 0.35]
WHEEL_CLOSE_LEFT = [-1.74, -1.51, 2.17, -0.55, 0.0, 0.3, -0.1]
WHEEL_CLOSE_RIGHT = [-1.45, 1.45, 2.17, 0.5, -0.9, -0.4, 0.35]
WHEEL_TURN_LEFT = [-1.54, -1.51, 2.17, -0.55, -0.85, 0.3, -0.1]
WHEEL_TURN_RIGHT = [-1.77, 1.45, 2.17, 0.5, -0.1, -0.3, 0.35]


DOOR_PUSH_PREPARE_LEFT = [0.56, -1.4, 0.0, -2.0, 0.0, 0.0, 0.49]
DOOR_PUSH_PREPARE_RIGHT = [0.56, 1.4, 0.0, 2.0, 0.0, 0.0, -0.49]
DOOR_PUSH_LEFT = [-1.3, -1.4, 0.0, -0.0, 0.0, 0.0, 0.49]
DOOR_PUSH_RIGHT = [-1.3, 1.4, 0.0, 0.0, 0.0, 0.0, -0.49]
DOOR_PUSH_LEFT_SPREAD = [-1.3, -1.1, 0.0, -0.0, 0.0, 0.0, 0.49]
DOOR_PUSH_RIGHT_SPREAD = [-1.3, 1.1, 0.0, 0.0, 0.0, 0.0, -0.49]

ARM_SPREAD_LEFT_WIDE = [0.16, -0.8, -0.4, -2.0, 1.0, 0.0, 0.0]
ARM_SPREAD_RIGHT_WIDE = [0.16, 0.8, -0.4, 2.0, 1.0, 0.0, 0.0]


class Position:
    x = 0.0
    y = 0.0
    z = 0.0

    def toString(self):
        return ("[x:[%s], y:[%s], z:[%s]]" % (self.x, self.y, self.z))
    
    
class Waypoint2D:
    x = 0.0
    y = 0.0
    angle = 0.0
    
    def toString(self):
        return ("[x:[%s], y:[%s], a:[%s]]" % (self.x, self.y, self.angle))


global footStepStatusSubscriber
global blueWheelPositionSubscrber
global blueWheelPositionSubscrber2
global blueStripPositionSubscrber
global solarOutputPositionSubscrber
global redWheelPositionSubscrber
global redWheelPositionSubscrber2
global navigationStatusSubscriber
global taskStatusSubscriber
global satelliteSubscriber
global killSwitchSubscriber
global checkPointSubscriber
global shutdownSubscriber
global grabSuitcaseAgainSubscriber
global grabCableAgainSubscriber
global stepForwardSubscriber
global walkForwardSubscriber
global armSubscriber
global turnSubscriber
global pelvisSubscriber
global neckSubscriber
global chestSubscriber
global taskStatusRequestSubscriber
global resetPoseSubscriber
global pingSubscriber

global taskStatusPublisher
global footStepListPublisher
global footTrajectoryPublisher
global pelvisHeightTrajectoryPublisher
global chestTrajectoryPublisher
global wholeBodyTrajectoryPublisher
global armTrajectoryPublisher
global neckTrajectoryPublisher
global loadPublisher
global pathPublisher
global navigationPublisher
global wanderingPublisher
global goToTruckPublisher
global goToPanelPublisher
global goToStairsPublisher
global alignToWheelPublisher
global leftArmPublisher
global rightArmPublisher
global leftHandPositionPublisher
global rightHandPositionPublisher

global pastBlueWheelPositions
global pastRedWheelPositions

global pastCableChokePositions
global pastRedButtonPositions
global pastBlueStripPositions
global pastSolarOutputPositions

global cableChokePosition
global redButtonPosition
global blueStripPosition
global solarOutputPosition
    
global pastBlueWheelPositions
global pastRedWheelPositions

global blueWheelPosition
global redWheelPosition

global targetPitch
global targetYaw
global curentPitch
global currentYaw

global blueDetectorProcess
global blueStripDetectorProcess
global redDetectorProcess
global blueReconstProcess
global blueStripReconstProcess
global redReconstProcess
global moveitProcess
global leftArmListenerProcess
global rightArmListenerProcess
global solarOutputDetectorProcess
global solarOutputReconstProcess

global blueWheelDetectorProcess
global redWheelDetectorProcess
global blueWheelReconstProcess
global redWheelReconstProcess


global shutdown
global killed

global grabSuitCaseAgain
global grabCableAgain

global carryArm

global currentTaskStatus

global stop_turning
global r0


def walkTest():
    global pastCableChokePositions
    global pastRedButtonPositions
    global pastBlueStripPositions
    global pastSolarOutputPositions
    global pastBlueWheelPositions
    global pastRedWheelPositions
    global grabSuitCaseAgain
    global grabCableAgain
    global carryArm
    global blueDetectorProcess
    global blueStripDetectorProcess
    global redDetectorProcess
    global blueReconstProcess
    global blueStripReconstProcess
    global redReconstProcess
    global leftArmListenerProcess
    global rightArmListenerProcess
    global solarOutputDetectorProcess
    global solarOutputReconstProcess

    global blueWheelDetectorProcess
    global redWheelDetectorProcess
    global blueWheelReconstProcess
    global redWheelReconstProcess

    global stop_turning
    global r0

    atexit.register(shutDownHook)
    
    pastBlueWheelPositions = [Position(), Position(), Position(), Position(), Position()]
    pastRedWheelPositions = [Position(), Position(), Position(), Position(), Position()]
    pastCableChokePositions = [Position(), Position(), Position(), Position(), Position()]
    pastRedButtonPositions = [Position(), Position(), Position(), Position(), Position()]
    pastBlueStripPositions = [Position(), Position(), Position(), Position(), Position()]
    pastSolarOutputPositions = [Position(), Position(), Position(), Position(), Position()]
    
    r0 = [0.0, 0.0, 0.0]
    stop_turning = False
    grabSuitCaseAgain = False
    grabCableAgain = False
    carryArm = LEFT
    
    
    #Initialize nodes
    rospy.loginfo('setting up 3d vision nodes...')
    FNULL = open(os.devnull, 'w')
    blueDetectorProcess = subprocess.Popen("rosrun vision image_processor /miki/blue cable_choke 0 __name:=blue_detector", shell=True, stdout=FNULL, preexec_fn=os.setsid)
    time.sleep(6)
    blueStripDetectorProcess = subprocess.Popen("rosrun vision image_processor /miki/blue_strip blue_strip 0 __name:=blue_strip_detector", shell=True, stdout=FNULL, preexec_fn=os.setsid)
    time.sleep(6)
    solarOutputDetectorProcess = subprocess.Popen("rosrun vision image_processor /miki/solar_output solar_output 0 __name:=solar_output_detector", shell=True, stdout=FNULL, preexec_fn=os.setsid)
    time.sleep(6)
    redDetectorProcess = subprocess.Popen("rosrun vision image_processor /miki/red red_button 0 __name:=red_detector", shell=True, stdout=FNULL, preexec_fn=os.setsid)
    time.sleep(6)
    blueReconstProcess = subprocess.Popen("rosrun vision loc_reconst.py LED_data:=/miki/blue srcsim/vision/light:=/srcsim/vision/blue __name:=blue_reconst", shell=True, stdout=FNULL, preexec_fn=os.setsid)
    time.sleep(6)
    blueStripReconstProcess = subprocess.Popen("rosrun vision loc_reconst.py LED_data:=/miki/blue_strip srcsim/vision/light:=/srcsim/vision/blue_strip __name:=blue_strip_reconst", shell=True, stdout=FNULL, preexec_fn=os.setsid)
    time.sleep(6)
    solarOutputReconstProcess = subprocess.Popen("rosrun vision loc_reconst.py LED_data:=/miki/solar_output srcsim/vision/light:=/srcsim/vision/solar_output __name:=solar_output_reconst", shell=True, stdout=FNULL, preexec_fn=os.setsid)
    time.sleep(6)
    redReconstProcess = subprocess.Popen("rosrun vision loc_reconst.py LED_data:=/miki/red srcsim/vision/light:=/srcsim/vision/red __name:=red_reconst", shell=True, stdout=FNULL, preexec_fn=os.setsid)    
    time.sleep(6)
    
    blueWheelDetectorProcess = subprocess.Popen("rosrun vision image_processor /miki/blue_wheel blue 0 __name:=blue_wheel_detector", shell=True, stdout=FNULL, preexec_fn=os.setsid)
    time.sleep(6)
    redWheelDetectorProcess = subprocess.Popen("rosrun vision image_processor /miki/red_wheel red 0 __name:=red_wheel_detector", shell=True, stdout=FNULL, preexec_fn=os.setsid)
    time.sleep(6)
    blueWheelReconstProcess = subprocess.Popen("rosrun vision loc_reconst.py LED_data:=/miki/blue_wheel srcsim/vision/light:=/srcsim/vision/blue_wheel __name:=blue_wheel_reconst", shell=True, stdout=FNULL, preexec_fn=os.setsid)
    time.sleep(6)
    redWheelReconstProcess = subprocess.Popen("rosrun vision loc_reconst.py LED_data:=/miki/red_wheel srcsim/vision/light:=/srcsim/vision/red_wheel __name:=red_wheel_reconst", shell=True, stdout=FNULL, preexec_fn=os.setsid)    
    time.sleep(6)

    
    rospy.loginfo('setting up grasping nodes...')
    leftArmListenerProcess = subprocess.Popen("python /nasasrc/grasping/moveit_left_arm_listener.py", shell=True, preexec_fn=os.setsid)    
    time.sleep(8)
    rightArmListenerProcess = subprocess.Popen("python /nasasrc/grasping/moveit_right_arm_listener.py", shell=True, preexec_fn=os.setsid)    
    
    rossleep(5)

    #check processes here and warn if not all initialized
    pollResult = ''
    if blueDetectorProcess.poll() != None:
        pollResult = pollResult + 'blueDetectorProcess '
        rospy.loginfo('ERROR! Dead node !')
    if blueWheelReconstProcess.poll() != None:
        pollResult = pollResult + 'blueWheelReconstProcess '
        rospy.loginfo('ERROR! Dead node !')
    if leftArmListenerProcess.poll() != None:
        pollResult = pollResult + 'leftArmListenerProcess '
        rospy.loginfo('ERROR! Dead node !')

    if pollResult:
        rospy.loginfo('ERROR !!!!!! Not all nodes are initialized! Dead processes: ' + pollResult)
    else:    
        rospy.loginfo('all nodes started')

    rospy.loginfo('daemon initialized') 
    
    while shutdown == False:
        rate.sleep()


#######################################################################################################################################################

def receivedSatelliteStatus(msg):
    global target_pitch
    global target_yaw
    global current_pitch
    global current_yaw
    global pitch_correct_now
    global yaw_correct_now
    global pitch_completed
    global yaw_completed
    
    target_pitch = msg.target_pitch
    target_yaw = msg.target_yaw
    current_pitch = msg.current_pitch
    current_yaw = msg.current_yaw
    pitch_correct_now = msg.pitch_correct_now
    yaw_correct_now = msg.yaw_correct_now
    pitch_completed = msg.pitch_completed
    yaw_completed = msg.yaw_completed
    
    
def receivedstopturningstatus(msg):
    
    global stop_turning 
    
    
    stop_turning = True
       

    
  
def turnBlueWheelDecrease(r0):
    global killed
    global shutdown
    global stop_turning
    
    i = 0
    
    j = 1

    
    stepping_angle = 5
    
    stop_turning = False
    
    while not stop_turning:
   
        calc_rP(i*stepping_angle+180,r0)
        if i%(360/stepping_angle) == 0 and not i == 0:
            adjust_hand()
  
        
        i = i + 1
        
        if j == 3:
            calc_rP((i-2)*stepping_angle+180,r0)
            calc_rP((i-1)*stepping_angle+180,r0)
            j = 1
        
        j = j + 1
        
        print stop_turning
        
        if pitch_correct_now == True:
            stop_turning = True
        
        if killed == True or shutdown == True:
            return 0
       

def grabBlueWheel():
    global killed
    global shutdown
    
    
    bwheel_pos_pelvis = Position()
    move_pos_pelvis = Position()
    
    
    print blueWheelPosition
    
    headPelvis = tfBuffer.lookup_transform('pelvis', 'head', rospy.Time(),rospy.Duration(1.0))
    headPoint = FootstepDataRosMessage()
    headPoint.orientation = headPelvis.transform.rotation
    headPoint.location = headPelvis.transform.translation
    quat = headPoint.orientation
    rot = tf.transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
    transformedOffset = numpy.dot(rot[0:3, 0:3], [blueWheelPosition.x, blueWheelPosition.y, blueWheelPosition.z])


    bwheel_pos_pelvis.x = headPoint.location.x + transformedOffset[0]
    bwheel_pos_pelvis.y = headPoint.location.y + transformedOffset[1]
    bwheel_pos_pelvis.z = headPoint.location.z + transformedOffset[2]
    
    print bwheel_pos_pelvis.x
    print bwheel_pos_pelvis.y
    print bwheel_pos_pelvis.z
   
    #bwheel_pos_pelvis.x = 0.696225173388
    #bwheel_pos_pelvis.y =-0.160718859211
    #bwheel_pos_pelvis.z =-0.0364084505282
   
    turn_chest([0.0,0.4,0.4])
    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([0.55,-0.3,0], 1.0))
    
    RIGHThandcommand('half')
    
    #point 0 (above, to the right)
    
    move_pos_pelvis.x = bwheel_pos_pelvis.x - 0.15
    move_pos_pelvis.y = bwheel_pos_pelvis.y -0.02
    move_pos_pelvis.z = bwheel_pos_pelvis.z + 0.06  
    cmd_str  = str(move_pos_pelvis.x)+","+str(move_pos_pelvis.y)+","+str(move_pos_pelvis.z)+",-0.47842,-0.15,1.47,0.03,0.15"
    print cmd_str
    rightArmPublisher.publish(cmd_str)
    rossleep(4)
    if killed == True or shutdown == True:
        return 0

    
    #point 2 (on it)
    move_pos_pelvis.x = bwheel_pos_pelvis.x - 0.28
    move_pos_pelvis.y = bwheel_pos_pelvis.y +0.03
    move_pos_pelvis.z = bwheel_pos_pelvis.z -0.08
    cmd_str  = str(move_pos_pelvis.x)+","+str(move_pos_pelvis.y)+","+str(move_pos_pelvis.z)+",-0.478423,-0.15,1.47,0.03,0.25"
    
    print cmd_str
    rightArmPublisher.publish(cmd_str)
    rossleep(4)
    if killed == True or shutdown == True:
        return 0

    RIGHThandcommand('half_finger_down')
    
    
    r0 = calc_r0() #calculates center of rotation circle
    
    #point 3 (shake it in)
    
    move_pos_pelvis.x = bwheel_pos_pelvis.x - 0.28
    move_pos_pelvis.y = bwheel_pos_pelvis.y + 0.06
    move_pos_pelvis.z = bwheel_pos_pelvis.z -0.08
    cmd_str  = str(move_pos_pelvis.x)+","+str(move_pos_pelvis.y)+","+str(move_pos_pelvis.z)+",-0.478423,-0.15,1.47,0.02,0.15"
    
    
    print cmd_str
    rightArmPublisher.publish(cmd_str)
    rossleep(4)
    if killed == True or shutdown == True:
        return 0
    
    RIGHThandcommand('full')
    
    move_pos_pelvis.x = bwheel_pos_pelvis.x - 0.24
    move_pos_pelvis.y = bwheel_pos_pelvis.y + 0.06
    move_pos_pelvis.z = bwheel_pos_pelvis.z -0.08
    cmd_str  = str(move_pos_pelvis.x)+","+str(move_pos_pelvis.y)+","+str(move_pos_pelvis.z)+",-0.478423,-0.15,1.47,0.02,0.15"
    
    
    print cmd_str
    rightArmPublisher.publish(cmd_str)
    rossleep(5)
    if killed == True or shutdown == True:
        return 0
    
    RIGHThandcommand('full_tight')
    
        
    return r0
    

    
    
    
 #------------------------------------------------------------------------------------------------------------------------------------------------------   


def assume_position():
    
    pelvisMsg2 = PelvisHeightTrajectoryRosMessage()
    pelvisMsg2.execution_mode = 0
    pelvisMsg2.unique_id = -1
    
    pelvisTraj2 = TrajectoryPoint1DRosMessage()
    pelvisTraj2.time = 1.0
    pelvisTraj2.position = 0.87
    pelvisTraj2.velocity = 0.0
    
    pelvisMsg2.trajectory_points.append(pelvisTraj2)
    
    rospy.loginfo('setting pelvis height...')
    pelvisHeightTrajectoryPublisher.publish(pelvisMsg2)
    
    turn_chest([0.0,0.4,0.0])
    
    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([0.55,0.0,0], 1.0))
    
    sendArmTrajectory('SPREAD_WIDE')
    
    rossleep(4)
    

def adjust_hand():
    
    rossleep(1)
    move_pos_pelvis = Position()
        
    palmPelvis = tfBuffer.lookup_transform('pelvis', 'rightPalm', rospy.Time(),rospy.Duration(1.0))
    palmPoint = FootstepDataRosMessage()
    palmPoint.location = palmPelvis.transform.translation
        
        
    RIGHThandcommand('half')
        
    #point 2 (on it)
    move_pos_pelvis.x = palmPelvis.transform.translation.x
    move_pos_pelvis.y = palmPelvis.transform.translation.y
    move_pos_pelvis.z = palmPelvis.transform.translation.z -0.05
    cmd_str  = str(move_pos_pelvis.x)+","+str(move_pos_pelvis.y)+","+str(move_pos_pelvis.z)+",-0.478423,-0.15,1.47,0.02,0.15"
        
    print cmd_str
    rightArmPublisher.publish(cmd_str)
    rossleep(1)
        
    RIGHThandcommand('half_finger_down')
        
        
        #point 3 (shake it in)
        
    move_pos_pelvis.x = palmPelvis.transform.translation.x
    move_pos_pelvis.y = palmPelvis.transform.translation.y +0.03
    move_pos_pelvis.z = palmPelvis.transform.translation.z -0.05
    cmd_str  = str(move_pos_pelvis.x)+","+str(move_pos_pelvis.y)+","+str(move_pos_pelvis.z)+",-0.478423,-0.15,1.47,0.02,0.15"
        
        
    print cmd_str
    rightArmPublisher.publish(cmd_str)
    rossleep(0.5)
        
    RIGHThandcommand('full')
      
    move_pos_pelvis.x = palmPelvis.transform.translation.x +0.03
    move_pos_pelvis.y = palmPelvis.transform.translation.y +0.03
    move_pos_pelvis.z = palmPelvis.transform.translation.z -0.05
    cmd_str  = str(move_pos_pelvis.x)+","+str(move_pos_pelvis.y)+","+str(move_pos_pelvis.z)+",-0.478423,-0.15,1.47,0.02,0.15"
        
        
    print cmd_str
    rightArmPublisher.publish(cmd_str)
    rossleep(0.5)
        
    RIGHThandcommand('full_tight')
    

def turn_chest(pel_dir):
    
    target_pose = tf2_geometry_msgs.PoseStamped()
   
    target_pose.header.frame_id = 'pelvis'
    target_pose.header.stamp = rospy.Time()
    
    #dir_quat = tf.transformations.quaternion_from_euler(0.0,0.4,0.4)
    
    dir_quat = tf.transformations.quaternion_from_euler(pel_dir[0],pel_dir[1],pel_dir[2])
    
    target_pose.pose.orientation.x = dir_quat[0]
    target_pose.pose.orientation.y = dir_quat[1]
    target_pose.pose.orientation.z = dir_quat[2]
    target_pose.pose.orientation.w = dir_quat[3]
    
    
    
    transformed_target_pose = tfBuffer.transform(target_pose, 'world',rospy.Duration(1.0))  

    chest_dir = tf.transformations.euler_from_quaternion([transformed_target_pose.pose.orientation.x,
                                                          transformed_target_pose.pose.orientation.y,
                                                          transformed_target_pose.pose.orientation.z,
                                                          transformed_target_pose.pose.orientation.w])
    
    
    #print chest_dir
    
    
    
    rospy.loginfo('setting chest trajectory...')
    chestTrajectoryPublisher.publish(createChestMessage([chest_dir[2],chest_dir[1],chest_dir[0]], 1.0))
    rossleep(1.5)
    
def createChestMessage(arr, time):    
    chestMsg = ChestTrajectoryRosMessage()
    chestMsg.execution_mode = 0
    chestMsg.unique_id = -1
    
    chestTraj = SO3TrajectoryPointRosMessage()
    chestTraj.time = time
    
    pista_quaternion = tf.transformations.quaternion_from_euler(arr[0], arr[1], arr[2], 'rzyx')
    
    chestTraj.orientation.x = pista_quaternion[0]
    chestTraj.orientation.y = pista_quaternion[1]
    chestTraj.orientation.z = pista_quaternion[2]
    chestTraj.orientation.w = pista_quaternion[3]    
    
    chestMsg.taskspace_trajectory_points.append(chestTraj)
    
    return chestMsg
    

def RIGHThandcommand(hand_cmd):
    hand_data = Float64MultiArray()
    
    if hand_cmd == 'letgo':
        hand_data.data = [0.3, 0.2, 0, 0, 0]
    elif hand_cmd == 'half_finger_down':
        hand_data.data = [2.2, 0.05, 0.6, 0.7, 0.7]
    elif hand_cmd == 'half':
        hand_data.data = [0.3, 0.05, 0.6, 0.7, 0.7]
    elif hand_cmd == 'full':
        hand_data.data = [2.2, 0.6, 0.6, 0.7, 1.0]
    elif hand_cmd == 'full_tight':
        hand_data.data = [1.9, 0.6, 0.9, 0.8, 1.0]  
    elif hand_cmd == 'loose_grip':
        hand_data.data = [2.2, 0.6, 0.3, 0.3, 0.3]
    elif hand_cmd == '2fingers_prep':
        hand_data.data = [1.7, 0.2, 0.3, 1.1, 1.1]
    elif hand_cmd == '2fingers_full':
        hand_data.data = [1.7, 0.6, 0.65, 1.1, 1.1]
    

    
    print ('hand command '+hand_cmd)
    rightHandPositionPublisher.publish(hand_data)    
    rossleep(0.5)
    
def LEFThandcommand(hand_cmd):
    hand_data = Float64MultiArray()
    
    if hand_cmd == 'letgo':
        hand_data.data = [0.3, 0.2, 0, 0, 0]
    elif hand_cmd == 'half_finger_down':
        hand_data.data = [2.2, 0.05, 0.6, 0.7, 0.7]
    elif hand_cmd == 'half':
        hand_data.data = [0.3, 0.05, 0.6, 0.7, 0.7]
    elif hand_cmd == 'full':
        hand_data.data = [2.2, 0.6, 0.6, 0.7, 1.0]
    elif hand_cmd == 'full_tight':
        hand_data.data = [1.9, 0.6, 0.9, 0.8, 1.0]  
    elif hand_cmd == 'loose_grip':
        hand_data.data = [2.2, 0.6, 0.3, 0.3, 0.3]
    elif hand_cmd == '2fingers_prep':
        hand_data.data = [1.7, 0.2, 0.3, 1.1, 1.1]
    elif hand_cmd == '2fingers_full':
        hand_data.data = [1.7, 0.6, 0.65, 1.1, 1.1]
    

    
    print ('hand command '+hand_cmd)
    leftHandPositionPublisher.publish(hand_data)    
    rossleep(0.5)
    
def calc_rP(alpha_deg,r0):

    alpha = alpha_deg*numpy.pi/180 # parameter of circle
    r = 0.12 #radius of wheel
    theta  = -30*numpy.pi/180 #tilt of table
    sinTh = math.sin(theta)
    cosTh = math.cos(theta)   
    traf = numpy.matrix([[cosTh,0,sinTh],
                        [0,1,0],
                        [-1*sinTh,0,cosTh]])

    
    rT = [r*math.cos(alpha),r*math.sin(alpha),0] #position of point in it's own base
    
    rP  = traf.dot(rT) + r0 #position of point in pelvis
    
    frac_angle = math.modf(float(alpha_deg)/float(360))
    
    if 0 < frac_angle[0] < 0.5 or -1 < frac_angle[0] < -0.5:
        print "trying" #different hand orientation
        print alpha_deg
        cmd_str  = str(rP[0,0])+","+str(rP[0,1])+","+str(rP[0,2])+",-0.478423,-0.15,2.0,0.02,0.15,last,quick"
    else:
        print "trying"
        print alpha_deg
        cmd_str  = str(rP[0,0])+","+str(rP[0,1])+","+str(rP[0,2])+",-0.478423,-0.15,1.47,0.02,0.15,last,quick"
        
    
    
    
    print cmd_str
    rightArmPublisher.publish(cmd_str)
    rossleep(0.7)
    
    #return rP
    
def calc_r0():
    
        
    palmPelvis = tfBuffer.lookup_transform('pelvis', 'rightPalm', rospy.Time(),rospy.Duration(1.0))
    palmPoint = FootstepDataRosMessage()
    palmPoint.location = palmPelvis.transform.translation
    
    r = 0.12 #radius of wheel
    theta  = -30*numpy.pi/180 #tilt of table
    
    r180 = [palmPelvis.transform.translation.x,palmPelvis.transform.translation.y,palmPelvis.transform.translation.z]
    
    r0 = numpy.subtract(r180,[-r*math.cos(theta),0,r*math.sin(theta)]) #position of center in pelvis
    
    print r180
    print r0
    
    return r0
    
    
def letGoOfBlueWheel():
   
    RIGHThandcommand('letgo')
    



#######################################################################################################################################################


def checkpointT1C1(): #Move within 1 meter from the communication dish (navigate close)
    global killed
    global shutdown

    global redButtonPosition
    global blueStripPosition

    worldTargetFar = getHeadTargetInWorld(1.28) #x,y,angle
    worldTargetClose = getHeadTargetInWorld(1.15) #x,y,angle
    
    rospy.loginfo('will walk through %f,%f;%f,%f' % (worldTargetFar[0], worldTargetFar[1], worldTargetClose[0], worldTargetClose[1]))
    navigationPublisher.publish('%f,%f' % (worldTargetFar[0], worldTargetFar[1]))
    waitForNavigation()
    
    if killed == True or shutdown == True:
        return 0
    
    rospy.loginfo('navigation finished...')
    pathPublisher.publish('%f,%f' % (worldTargetClose[0], worldTargetClose[1]))
    waitForNavigation()
    rospy.loginfo('path finished...')

    rospy.loginfo('Module T1C1 complete. Advancing to module T1C1b...')
    if killed == False and shutdown == False:
        checkpointT1C1b()


def checkpointT1C1b(): #Move within 1 meter from the communication dish (fine tune position)
    global killed
    global shutdown

    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([0.50, 0.0, -0.06], 1.0))
    
    rospy.loginfo('setting pelvis height...')
    pelvisHeightTrajectoryPublisher.publish(createPelvisMsg(1.08, 1.0)) 
    
    rossleep(4)
    
    if killed == True or shutdown == True:
        return 0
    
    msg = FootstepDataListRosMessage()
    msg.default_transfer_time = 1.05 #on two legs
    msg.default_swing_time = 1.15 #on one leg
    msg.execution_mode = 0
    msg.unique_id = -1
    
    headTargetInWorld = getHeadTargetInWorld(0.82) #x,y,angle
    footSourceInWorld = getFootSourceInWorld() #x,y,angle
    
    isLeft = False
    wayPoints = interpolateWayPoints(footSourceInWorld[0], footSourceInWorld[1], footSourceInWorld[2], headTargetInWorld[0], headTargetInWorld[1], headTargetInWorld[2])
    footHeight = getFootHeight(LEFT)
    
    latestWayoint = None
    for wayPoint in wayPoints:
        latestWayoint = wayPoint
        rospy.loginfo(wayPoint.toString())
        if isLeft == True:
            msg.footstep_data_list.append(createFootStepFromWayPoint(LEFT, wayPoint, footHeight, 0.1))
        else:
            msg.footstep_data_list.append(createFootStepFromWayPoint(RIGHT, wayPoint, footHeight, 0.1))
        isLeft = not isLeft
    #lining up both feet with a last step
    if isLeft == True:
        msg.footstep_data_list.append(createFootStepFromWayPoint(LEFT, latestWayoint, footHeight, 0.1))
    else:
        msg.footstep_data_list.append(createFootStepFromWayPoint(RIGHT, latestWayoint, footHeight, 0.1))


    footStepListPublisher.publish(msg)
    rospy.loginfo('walk forward...')
    waitForFootsteps(len(msg.footstep_data_list))

    if killed == True or shutdown == True:
        return 0
    
    #Move feet closer so that we don't touch any box near the panel
    msg2 = FootstepDataListRosMessage()
    msg2.default_transfer_time = 0.80 #on two legs
    msg2.default_swing_time = 0.80 #on one leg
    msg2.execution_mode = 0
    msg2.unique_id = -1
    msg2.footstep_data_list.append(createFootStepOffset(LEFT, [0.0, -0.06, 0.0], 0.10, 0.0))
    msg2.footstep_data_list.append(createFootStepOffset(RIGHT, [0.0, 0.05, 0.0], 0.10, 0.0))

    footStepListPublisher.publish(msg2)
    rospy.loginfo('moving feet closer...')
    waitForFootsteps(len(msg2.footstep_data_list))

    if killed == True or shutdown == True:
        return 0

    sendArmTrajectory('SPREAD')

    rossleep(0.45)
    
    rospy.loginfo('setting pelvis height...')
    pelvisHeightTrajectoryPublisher.publish(createPelvisMsg(0.87, 1.0)) 

    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([1.08, 0.0, -0.22], 1.0))

    rospy.loginfo('Module T1C1b complete. Call module T1C2 or skip to checkpoint 4 and call T1C4 manually.')
    return 0
    

def checkpointT1C2(): #Assume position
    global killed
    global shutdown
    global stop_turning
    
    assume_position()

    rospy.loginfo('Module T1C2 complete. Call module T1C2b manually.')
    return 0        


def checkpointT1C2b(): #Grab blue wheel
    global killed
    global shutdown
    global stop_turning
    global r0

    r0 = grabBlueWheel()
    rossleep(2.0)

    rospy.loginfo('Module T1C2b complete. Call module T1C2c manually.')
    return 0        
    

def checkpointT1C2c(): #Turn blue wheel
    global killed
    global shutdown
    global stop_turning
    
    rospy.loginfo('turning blue wheel')
    
    stop_turning = False
    
    turnBlueWheelDecrease(r0)
    
    rospy.loginfo('Module T1C2c complete. Call module T1C2d manually.')
    return 0        
    

def checkpointT1C2d(): #Let go of blue wheel
    global killed
    global shutdown
    global stop_turning

    letGoOfBlueWheel()

    rospy.loginfo('Module T1C2d complete. Skip to checkpoint 4 and call T1C4 manually.')
    return 0


def checkpointT1C4(): #Walk into task 1's finish box
    global killed
    global shutdown
    
    global currentTaskStatus
    
    #move feet back and apart
    msg3 = FootstepDataListRosMessage()
    msg3.default_transfer_time = 0.80 #on two legs
    msg3.default_swing_time = 0.80 #on one leg
    msg3.execution_mode = 0
    msg3.unique_id = -1
    msg3.footstep_data_list.append(createFootStepOffset(RIGHT, [-0.20, -0.0, 0.0], 0.10, 0.0))
    msg3.footstep_data_list.append(createFootStepOffset(LEFT, [-0.20, 0.0, 0.0], 0.10, 0.0))

    footStepListPublisher.publish(msg3)
    rospy.loginfo('moving feet apart...')
    waitForFootsteps(len(msg3.footstep_data_list))
    if killed == True or shutdown == True:
        return 0

    resetMsg = String()
    resetMsg.data = ''
    receivedResetPoseMsg(resetMsg)
    if killed == True or shutdown == True:
        return 0
    
    startWandering()
    while (currentTaskStatus.task == 1 and currentTaskStatus.current_checkpoint == 4):
        rate.sleep()
        if killed == True or shutdown == True:
            return 0
    
    stopWandering()
    
    rospy.loginfo('Module T1C4 complete. Task 1 complete. Advancing to module T2C1...')
    
    if killed == False and shutdown == False:
        checkpointT2C1()
    

def checkpointT2C1(): #Pick up the solar panel (go to the back of the truck)
    global killed
    global shutdown

    global redButtonPosition
    global blueStripPosition
    

    resetMsg = String()
    resetMsg.data = ''
    receivedResetPoseMsg(resetMsg)
    if killed == True or shutdown == True:
        return 0


    sendArmTrajectory('PULL_BACK')
    chestTrajectoryPublisher.publish(createChestMsg(0.0, 0.0, 0.0, 1.0))
    
    tMsg = String()
    tMsg.data = '3.14'
    rospy.loginfo('turn a full circle to search for truck')
    receivedTurnMsg(tMsg)
    if killed == True or shutdown == True:
        return 0

    goToTruckPublisher.publish('goooo')
    waitForNavigation()
    rospy.loginfo('reached the back of the truck')

    rossleep(0.5)
    if killed == True or shutdown == True:
        return 0

    rospy.loginfo('Module T2C1 complete. Call module T2C1b manually if the robot is at te back of the truck.')
    return 0    


def checkpointT2C1b(): #Pick up the solar panel (position yourself at the back of the truck)
    global killed
    global shutdown
 
    global grabSuitCaseAgain
    global redButtonPosition
    global blueStripPosition
    
    global carryArm
    
    chestTrajectoryPublisher.publish(createChestMsg(0.0, 0.4, 0.0, 1.0))    
    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([0.55, 0.0, 0.00], 1.0))
    pelvisHeightTrajectoryPublisher.publish(createPelvisMsg(1.05, 1.0))
    
    rossleep(4.0)
    if killed == True or shutdown == True:
        return 0

    msgS = FootstepDataListRosMessage()
    msgS.default_transfer_time = 0.80 #on two legs
    msgS.default_swing_time = 0.80 #on one leg
    msgS.execution_mode = 0
    msgS.unique_id = -1
    rospy.loginfo('bluestrip pos is %f' % blueStripPosition.y)
    if blueStripPosition.y < 0:
        msgS.footstep_data_list.append(createFootStepOffset(LEFT, [0.1, -1 * blueStripPosition.y, 0.0], 0.10, 0.0))
        msgS.footstep_data_list.append(createFootStepOffset(RIGHT, [0.1, -1 * blueStripPosition.y, 0.0], 0.10, 0.0))
    else:
        msgS.footstep_data_list.append(createFootStepOffset(RIGHT, [0.1, -1 * blueStripPosition.y, 0.0], 0.10, 0.0))
        msgS.footstep_data_list.append(createFootStepOffset(LEFT, [0.1, -1 * blueStripPosition.y, 0.0], 0.10, 0.0))
    
    chestTrajectoryPublisher.publish(createChestMsg(0.0, 0.0, 0.0, 1.0))
    rossleep(1.0)
    footStepListPublisher.publish(msgS)
    rospy.loginfo('step to the center...')
    waitForFootsteps(len(msgS.footstep_data_list))

    rossleep(2.0)
    if killed == True or shutdown == True:
        return 0
    
    chestTrajectoryPublisher.publish(createChestMsg(0.0, 0.4, 0.0, 1.0))
    
    rossleep(4.0)
    if killed == True or shutdown == True:
        return 0
    
    print (convertHeadToPelvis(redButtonPosition)).x
    print (convertHeadToPelvis(redButtonPosition)).y
    print (convertHeadToPelvis(blueStripPosition)).x
    print (convertHeadToPelvis(blueStripPosition)).y
    
    carryArm = RIGHT
    redXMore = ((convertHeadToPelvis(redButtonPosition)).x > (convertHeadToPelvis(blueStripPosition)).x)
    redYMore = ((convertHeadToPelvis(redButtonPosition)).y > (convertHeadToPelvis(blueStripPosition)).y)
    if redXMore == redYMore:
        carryArm = LEFT
        rospy.loginfo('will attempt to grab suitcase with LEFT arm')
    else:    
        rospy.loginfo('will attempt to grab suitcase with RIGHT arm')
    
    
    #getting in a good position
    chestTrajectoryPublisher.publish(createChestMsg(0.0, 0.0, 0.0, 1.0))
    rossleep(1.0)
    if killed == True or shutdown == True:
        return 0
    
    if carryArm == LEFT:
        msgS = FootstepDataListRosMessage()
        msgS.default_transfer_time = 0.80 #on two legs
        msgS.default_swing_time = 0.80 #on one leg
        msgS.execution_mode = 0
        msgS.unique_id = -1
        msgS.footstep_data_list.append(createFootStepOffset(LEFT, [0.0, 0.15, 0.0], 0.10, 0.0))
        msgS.footstep_data_list.append(createFootStepOffset(RIGHT, [0.0, 0.15, 0.0], 0.10, 0.0))
        footStepListPublisher.publish(msgS)
        rospy.loginfo('step to a better grab position...')
        waitForFootsteps(len(msgS.footstep_data_list))
        
        msgS2 = FootstepDataListRosMessage()
        msgS2.default_transfer_time = 0.80 #on two legs
        msgS2.default_swing_time = 0.80 #on one leg
        msgS2.execution_mode = 0
        msgS2.unique_id = -1
        list = createRotationFootStepList(-0.7) #turn right
        msgS2.footstep_data_list.append(list[0])
        msgS2.footstep_data_list.append(list[1])
        footStepListPublisher.publish(msgS2)
        waitForFootsteps(len(msgS2.footstep_data_list))
        
        sendArmTrajectory('STRAIGHT_FORWARD_LEFT')
    else:
        msgS = FootstepDataListRosMessage()
        msgS.default_transfer_time = 0.80 #on two legs
        msgS.default_swing_time = 0.80 #on one leg
        msgS.execution_mode = 0
        msgS.unique_id = -1
        msgS.footstep_data_list.append(createFootStepOffset(RIGHT, [0.0, -0.15, 0.0], 0.10, 0.0))
        msgS.footstep_data_list.append(createFootStepOffset(LEFT, [0.0, -0.15, 0.0], 0.10, 0.0))
        footStepListPublisher.publish(msgS)
        rospy.loginfo('step to a better grab position...')
        waitForFootsteps(len(msgS.footstep_data_list))
        
        msgS2 = FootstepDataListRosMessage()
        msgS2.default_transfer_time = 0.80 #on two legs
        msgS2.default_swing_time = 0.80 #on one leg
        msgS2.execution_mode = 0
        msgS2.unique_id = -1
        list = createRotationFootStepList(0.7) #turn left
        msgS2.footstep_data_list.append(list[0])
        msgS2.footstep_data_list.append(list[1])
        footStepListPublisher.publish(msgS2)
        waitForFootsteps(len(msgS2.footstep_data_list))
        
        sendArmTrajectory('STRAIGHT_FORWARD_RIGHT')
    
    if killed == True or shutdown == True:
        return 0
    
    #close feet
    msgC = FootstepDataListRosMessage()
    msgC.default_transfer_time = 0.80 #on two legs
    msgC.default_swing_time = 0.80 #on one leg
    msgC.execution_mode = 0
    msgC.unique_id = -1
    if carryArm == LEFT:
        msgC.footstep_data_list.append(createFootStepOffset(RIGHT, [0.0, 0.08, 0.0], 0.10, 0.0))
    else:
        msgC.footstep_data_list.append(createFootStepOffset(LEFT, [0.0, -0.08, 0.0], 0.10, 0.0))

    footStepListPublisher.publish(msgC)
    rospy.loginfo('moving feet closer...')
    waitForFootsteps(len(msgC.footstep_data_list))

    if killed == True or shutdown == True:
        return 0

    #set neck, chest and pelvis
    pelvisHeightTrajectoryPublisher.publish(createPelvisMsg(0.96, 1.0)) 
    if carryArm == LEFT:
        neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([0.55, 0.1, 0.0], 1.0))
        chestTrajectoryPublisher.publish(createChestMsg(0.0, 0.40, -0.25, 1.0))
    else:
        neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([0.55, -0.1, 0.0], 1.0))
        chestTrajectoryPublisher.publish(createChestMsg(0.0, 0.40, 0.25, 1.0))
    
    rossleep(1.0)
    if killed == True or shutdown == True:
        return 0

    if carryArm == LEFT:
        handPublisher = leftHandPositionPublisher
    else:
        handPublisher = rightHandPositionPublisher

    handMsg = Float64MultiArray()
    handMsg.data = [1.5, -0.0, -0.0, -0.0, -0.0]
    handPublisher.publish(handMsg)

    rossleep(1.5)
    if killed == True or shutdown == True:
        return 0
    
    rospy.loginfo('Module T2C1b complete. Advancing to T2C1c...')
    if killed == False and shutdown == False:
        checkpointT2C1c()


def checkpointT2C1c(): #Pick up the solar panel (place hand on suitcase handle)
    global killed
    global shutdown
 
    global redButtonPosition
    global blueStripPosition
    
    
    if carryArm == LEFT:
        sendArmTrajectory('STRAIGHT_FORWARD_LEFT')
    else:
        sendArmTrajectory('STRAIGHT_FORWARD_RIGHT')
    rossleep(2.0)    
        
    grabPosition = convertHeadToPelvis(blueStripPosition)
    rospy.loginfo('grabbing to %f,%f,%f' % (grabPosition.x, grabPosition.y, grabPosition.z))
    
    if carryArm == LEFT:
        leftArmPublisher.publish('%f,%f,%f,%f,%f,%f,%f,%f,last' % (grabPosition.x - 0.1, grabPosition.y, grabPosition.z + 0.44, -2.169, 1.171, 2.910, 0.10, 0.5))
    else:
        rightArmPublisher.publish('%f,%f,%f,%f,%f,%f,%f,%f,last' % (grabPosition.x - 0.1, grabPosition.y - 0.05, grabPosition.z + 0.44, 1.69, 1.37, 3.39, 0.10, 0.5))
    
    rossleep(9.0)
    if killed == True or shutdown == True:
        return 0

    rospy.loginfo('Module T2C1c complete. Check hand position and repeat or call next module manually.')
    return 0

    
def checkpointT2C1d(): #Pick up the solar panel (close hand and lift)
    global killed
    global shutdown
 
    global grabSuitCaseAgain
    global redButtonPosition
    global blueStripPosition
    
    
    handMsg2 = Float64MultiArray()
    if carryArm == LEFT:
        handMsg2.data = [1.5, -0.8, -1.1, -1.1, -1.1]
    else:
        handMsg2.data = [1.5, 0.8, 1.1, 1.1, 1.1]
    handPublisher.publish(handMsg2)

    rossleep(2.0)    
    if killed == True or shutdown == True:
        return 0
    
    pelvisHeightTrajectoryPublisher.publish(createPelvisMsg(1.12, 2.0)) 
    chestTrajectoryPublisher.publish(createChestMsg(0.0, 0.0, 0.0, 2.0))
    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([0.60, 0.0, 0.0], 1.0))

    if carryArm == LEFT:  
        sendArmTrajectory('LIFT_SUITCASE_LEFT')
    else:
        sendArmTrajectory('LIFT_SUITCASE_RIGHT')
        
    rossleep(10.0)
    
    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([0.25, 0.0, 0.0], 1.0))
    rossleep(2.0)
    
    if killed == True or shutdown == True:
        return 0

    rospy.loginfo('Module T2C1d complete. Check if suitcase is held firmly and recover manually or call next module.')    
    return 0

    
def checkpointT2C1e(): #Pick up the solar panel (step back)
    global killed
    global shutdown
 
    global grabSuitCaseAgain
    global redButtonPosition
    global blueStripPosition
    
    
    #take a step back and move feet apart
    msgD = FootstepDataListRosMessage()
    msgD.default_transfer_time = 0.80 #on two legs
    msgD.default_swing_time = 0.80 #on one leg
    msgD.execution_mode = 0
    msgD.unique_id = -1
    if carryArm == LEFT:
        msgD.footstep_data_list.append(createFootStepOffset(RIGHT, [0.0, -0.16, 0.0], 0.10, 0.0))
    else:
        msgD.footstep_data_list.append(createFootStepOffset(LEFT, [0.0, 0.16, 0.0], 0.10, 0.0))

    footStepListPublisher.publish(msgD)
    rospy.loginfo('moving feet apart...')
    waitForFootsteps(len(msgD.footstep_data_list))
    if killed == True or shutdown == True:
        return 0
    
    msgD.footstep_data_list = []
    msgD.footstep_data_list.append(createFootStepOffset(RIGHT, [-0.25, 0.00, 0.0], 0.10, 0.0))
    msgD.footstep_data_list.append(createFootStepOffset(LEFT, [-0.25, -0.00, 0.0], 0.10, 0.0))
    footStepListPublisher.publish(msgD)
    rospy.loginfo('stepping back...')
    waitForFootsteps(len(msgD.footstep_data_list))
    if killed == True or shutdown == True:
        return 0

    rospy.loginfo('Module T2C1e complete. Advancing to T2C2...')
    if killed == False and shutdown == False:
        checkpointT2C2()


def checkpointT2C2(): #Place solar panel within reach of the power cable (navigate there)
    global killed
    global shutdown

    chestTrajectoryPublisher.publish(createChestMsg(0.0, 0.0, 0.0, 1.0))    
    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([0.25, 0.0, 0.00], 1.0))
    pelvisHeightTrajectoryPublisher.publish(createPelvisMsg(1.12, 1.0))

    goToPanelPublisher.publish('goooo')
    waitForNavigation()
    rospy.loginfo('reached the cable panel')

    rospy.loginfo('Module T2C2 complete. Advancing to T2C2b...')
    if killed == False and shutdown == False:
        checkpointT2C2b()


def checkpointT2C2b(): #Place solar panel within reach of the power cable (line up)
    global killed
    global shutdown
    
    global carryArm

    chestTrajectoryPublisher.publish(createChestMsg(0.0, 0.0, 0.0, 1.0))    
    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([0.25, 0.0, 0.00], 1.0))
    pelvisHeightTrajectoryPublisher.publish(createPelvisMsg(1.12, 1.0))

    if carryArm == LEFT:
        sendArmTrajectory('HOLD_UP_SUITCASE_LEFT')
    else:
        sendArmTrajectory('HOLD_UP_SUITCASE_RIGHT')
    rossleep(2.5)
    if killed == True or shutdown == True:
        return 0

    #step forward
    msgS = FootstepDataListRosMessage()
    msgS.default_transfer_time = 0.80 #on two legs
    msgS.default_swing_time = 0.80 #on one leg
    msgS.execution_mode = 0
    msgS.unique_id = -1
    msgS.footstep_data_list.append(createFootStepOffset(LEFT, [0.22, 0.0, 0.0], 0.10, 0.0))
    msgS.footstep_data_list.append(createFootStepOffset(RIGHT, [0.22, 0.0, 0.0], 0.10, 0.0))
    msgS.footstep_data_list.append(createFootStepOffset(LEFT, [0.44, 0.0, 0.0], 0.10, 0.0))
    msgS.footstep_data_list.append(createFootStepOffset(RIGHT, [0.44, 0.0, 0.0], 0.10, 0.0))
    footStepListPublisher.publish(msgS)
    rospy.loginfo('step to a better drop off position...')
    waitForFootsteps(len(msgS.footstep_data_list))
    if killed == True or shutdown == True:
        return 0
    
    if carryArm == LEFT:
        handPublisher = leftHandPositionPublisher
    else:
        handPublisher = rightHandPositionPublisher

    #handMsg = Float64MultiArray()
    #handMsg.data = [1.5, -0.0, -0.0, -0.0, -0.0]
    #handPublisher.publish(handMsg)

    rospy.loginfo('Module T2C2b complete. Place down the suitcase manually, then press the button, then call the next module.')
    return 0


def checkpointT2C3(): #Deploy solar panel by pressing a button
    global killed
    global shutdown
    
    rospy.loginfo('Checkpoint T2C3 is not implemented. Press the button manually.')


def checkpointT2C4(): #Pick up the power cable (position the robot)
    global killed
    global shutdown
    global cableChokePosition
    global solarOutputPosition

    chestTrajectoryPublisher.publish(createChestMsg(0.0, 0.0, 0.0, 1.0))    
    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([0.25, 0.0, 0.00], 1.0))
    pelvisHeightTrajectoryPublisher.publish(createPelvisMsg(1.12, 1.0))
    
    sendArmTrajectory('ARM_IN_RIGHT')
    rossleep(0.35)
    sendArmTrajectory('ARM_IN_LEFT')
    rossleep(0.35)
    if killed == True or shutdown == True:
        return 0

    #step forward
    msgS = FootstepDataListRosMessage()
    msgS.default_transfer_time = 0.80 #on two legs
    msgS.default_swing_time = 0.80 #on one leg
    msgS.execution_mode = 0
    msgS.unique_id = -1
    msgS.footstep_data_list.append(createFootStepOffset(RIGHT, [0.0, -0.25, 0.0], 0.10, 0.0))
    msgS.footstep_data_list.append(createFootStepOffset(LEFT, [0.0, -0.25, 0.0], 0.10, 0.0))
    msgS.footstep_data_list.append(createFootStepOffset(RIGHT, [0.0, -0.50, 0.0], 0.10, 0.0))
    msgS.footstep_data_list.append(createFootStepOffset(LEFT, [0.0, -0.50, 0.0], 0.10, 0.0))
    msgS.footstep_data_list.append(createFootStepOffset(RIGHT, [0.22, -0.50, 0.0], 0.10, 0.0))
    msgS.footstep_data_list.append(createFootStepOffset(LEFT, [0.22, -0.50, 0.0], 0.10, 0.0))
    msgS.footstep_data_list.append(createFootStepOffset(RIGHT, [0.38, -0.50, 0.0], 0.10, 0.0))
    msgS.footstep_data_list.append(createFootStepOffset(LEFT, [0.38, -0.50, 0.0], 0.10, 0.0))
    footStepListPublisher.publish(msgS)
    rospy.loginfo('step to a better position...')
    waitForFootsteps(len(msgS.footstep_data_list))
    if killed == True or shutdown == True:
        return 0
    
    rossleep(2)
    
    sendArmTrajectory('STRAIGHT_FORWARD_LEFT_CABLE')
    rossleep(0.3)
    sendArmTrajectory('ARM_IN_RIGHT')

    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([0.55, 0.0, 0.00], 1.0))
    rossleep(0.2)
    pelvisHeightTrajectoryPublisher.publish(createPelvisMsg(0.90, 1.5))
    rossleep(0.2)
    chestTrajectoryPublisher.publish(createChestMsg(0.0, 0.4, 0.0, 1.0))    

    if killed == True or shutdown == True:
        return 0

    handMsg0 = Float64MultiArray()
    handMsg0.data = [1.9, -0.38, -0.2, -0.2, -0.2]
    leftHandPositionPublisher.publish(handMsg0)

    rossleep(5.0)

    rospy.loginfo('Module T2C4 complete. You need to call module T2C4b manually.')
    return 0


def checkpointT2C4b(): #Pick up the power cable (try to pick it up)
    global killed
    global shutdown
    global cableChokePosition
    global solarOutputPosition

    grabCableAgain = True
    while grabCableAgain == True:
        grabCableAgain = False #you can set this back to true from a ros msg - not working currently

        sendArmTrajectory('STRAIGHT_FORWARD_LEFT_CABLE')
        rossleep(3.0)
        
        grabPosition = convertHeadToPelvis(cableChokePosition)
        rospy.loginfo('grabbing to %f,%f,%f' % (grabPosition.x, grabPosition.y, grabPosition.z))
        #leftArmPublisher.publish('%f,%f,%f,%f,%f,%f,%f,%f' % (grabPosition.x, grabPosition.y, grabPosition.z, 0.30, 1.57, -1.87, 0.05, 0.5))
        leftArmPublisher.publish('%f,%f,%f,%f,%f,%f,%f,%f,last' % (grabPosition.x - 0.07, grabPosition.y + 0.07, grabPosition.z + 0.24, -1.67, 1.02, 2.32, 0.08, 0.3))
    
        rossleep(6.0)
        if killed == True or shutdown == True:
            return 0
    
        leftArmPublisher.publish('%f,%f,%f,%f,%f,%f,%f,%f,last' % (grabPosition.x - 0.07, grabPosition.y + 0.065, grabPosition.z + 0.03, -1.67, 1.02, 2.32, 0.06, 0.36))
        rossleep(6.0)
        
        if killed == True or shutdown == True:
            return 0
    
    rospy.loginfo('Module T2C4b complete. Check if hand is on the cable. You need to call module T2C4c manually.')
    return 0


def checkpointT2C4c(): #Pick up the power cable (lift it after placing the hand on it)
    global killed
    global shutdown
    global cableChokePosition
    global solarOutputPosition
    
    handMsg = Float64MultiArray()
    handMsg.data = [1.9, -0.38, -0.4, -0.4, -0.4]
    leftHandPositionPublisher.publish(handMsg)

    rossleep(1.5)
    if killed == True or shutdown == True:
        return 0

    handMsg2 = Float64MultiArray()
    handMsg2.data = [1.5, -1.1, -1.1, -1.1, -1.1]
    leftHandPositionPublisher.publish(handMsg2)

    rossleep(2.0)
    if killed == True or shutdown == True:
        return 0
    
    sendArmTrajectory('LIFT_CABLE_LEFT')
    
    rossleep(2.0)
    if killed == True or shutdown == True:
        return 0
    
    chestTrajectoryPublisher.publish(createChestMsg(0.0, 0.0, 0.0, 1.0))   
    rossleep(1.5)
    
    pelvisHeightTrajectoryPublisher.publish(createPelvisMsg(1.06, 1.0))
    rossleep(1.5)
    
    msgL = FootstepDataListRosMessage()
    msgL.default_transfer_time = 0.80 #on two legs
    msgL.default_swing_time = 0.80 #on one leg
    msgL.execution_mode = 0
    msgL.unique_id = -1
    msgL.footstep_data_list.append(createFootStepOffset(LEFT, [0.0, 0.28, 0.0], 0.10, 0.0))
    msgL.footstep_data_list.append(createFootStepOffset(RIGHT, [0.0, 0.28, 0.0], 0.10, 0.0))
    footStepListPublisher.publish(msgL)
    rospy.loginfo('step to a better plug position...')
    waitForFootsteps(len(msgL.footstep_data_list))
    rossleep(0.5)
    if killed == True or shutdown == True:
        return 0

    pelvisHeightTrajectoryPublisher.publish(createPelvisMsg(0.90, 1.0))
    rossleep(1.5)

    rospy.loginfo('Module T2C4c complete. Manual positioning is needed to the solar panel, then call module T2C5.')
    return 0 #not starting the next checkpoint to allow manual positioning


def checkpointT2C5(): #Plug the power cable into the solar panel
    global killed
    global shutdown
    global cableChokePosition
    global solarOutputPosition
    global currentTaskStatus
    
    chestTrajectoryPublisher.publish(createChestMsg(0.0, 0.4, 0.0, 1.0))       
    
    rossleep(4.5)
    if killed == True or shutdown == True:
        return 0
    
    plugPosition = convertHeadToPelvis(solarOutputPosition)
    leftArmPublisher.publish('%f,%f,%f,%f,%f,%f,%f,%f,last' % (plugPosition.x - 0.07, plugPosition.y + 0.05, plugPosition.z + 0.00, -3.1, 0.372, 1.069, 0.08, 0.5))

    rossleep(8.0)
    if killed == True or shutdown == True:
        return 0

    #if not successful, trying again until it goes in
    while not (currentTaskStatus.task == 2 and currentTaskStatus.current_checkpoint == 6):
        sendArmTrajectory('LIFT_CABLE_LEFT')
        rossleep(4.0)

        plugPosition = convertHeadToPelvis(solarOutputPosition)
        leftArmPublisher.publish('%f,%f,%f,%f,%f,%f,%f,%f,last' % (plugPosition.x - 0.07, plugPosition.y + 0.00, plugPosition.z + 0.05, -3.1, 0.372, 1.069, 0.08, 0.5))
        rossleep(8.0)
        if killed == True or shutdown == True:
            return 0


    handMsg2 = Float64MultiArray()
    handMsg2.data = [1.5, 0.0, 0.0, 0.0, 0.0]
    leftHandPositionPublisher.publish(handMsg2)
    rossleep(1.0)

    #step back
    msgL = FootstepDataListRosMessage()
    msgL.default_transfer_time = 0.80 #on two legs
    msgL.default_swing_time = 0.80 #on one leg
    msgL.execution_mode = 0
    msgL.unique_id = -1
    msgL.footstep_data_list.append(createFootStepOffset(LEFT, [-0.22, 0.0, 0.0], 0.10, 0.0))
    msgL.footstep_data_list.append(createFootStepOffset(RIGHT, [-0.22, 0.0, 0.0], 0.10, 0.0))
    footStepListPublisher.publish(msgL)
    rospy.loginfo('step to a better plug position...')
    waitForFootsteps(len(msgL.footstep_data_list))
    rossleep(0.5)

    rospy.loginfo('Checkpoint T2C5 complete. Advancing to T2C6...')    
    if killed == False and shutdown == False:
        checkpointT2C6()


def checkpointT2C5rand(): #Plug the power cable into the solar panel (slight randomness in plug position)
    global killed
    global shutdown
    global cableChokePosition
    global solarOutputPosition
    global currentTaskStatus
    
    chestTrajectoryPublisher.publish(createChestMsg(0.0, 0.4, 0.0, 1.0))       
    
    rossleep(4.5)
    if killed == True or shutdown == True:
        return 0
    
    plugPosition = convertHeadToPelvis(solarOutputPosition)
    leftArmPublisher.publish('%f,%f,%f,%f,%f,%f,%f,%f,last' % (plugPosition.x - 0.07, plugPosition.y + 0.05, plugPosition.z + 0.00, -3.1, 0.372, 1.069, 0.08, 0.5))

    rossleep(8.0)
    if killed == True or shutdown == True:
        return 0

    #if not successful, trying again with random modifications until it goes in
    while not (currentTaskStatus.task == 2 and currentTaskStatus.current_checkpoint == 6):
        sendArmTrajectory('LIFT_CABLE_LEFT')
        rossleep(4.0)

        rand1 = (random() - 0.5) / 100 * 10
        rand2 = (random() - 0.5) / 100 * 10
        rand3 = (random() - 0.5) / 100 * 10
        plugPosition = convertHeadToPelvis(solarOutputPosition)
        leftArmPublisher.publish('%f,%f,%f,%f,%f,%f,%f,%f,last' % (plugPosition.x - 0.07 + rand1, plugPosition.y + 0.00 + rand2, plugPosition.z + 0.05 + rand3, -3.1, 0.372, 1.069, 0.08, 0.5))
        rossleep(8.0)
        if killed == True or shutdown == True:
            return 0


    handMsg2 = Float64MultiArray()
    handMsg2.data = [1.5, 0.0, 0.0, 0.0, 0.0]
    leftHandPositionPublisher.publish(handMsg2)
    rossleep(1.0)
    if killed == True or shutdown == True:
        return 0

    #step back
    msgL = FootstepDataListRosMessage()
    msgL.default_transfer_time = 0.80 #on two legs
    msgL.default_swing_time = 0.80 #on one leg
    msgL.execution_mode = 0
    msgL.unique_id = -1
    msgL.footstep_data_list.append(createFootStepOffset(LEFT, [-0.22, 0.0, 0.0], 0.10, 0.0))
    msgL.footstep_data_list.append(createFootStepOffset(RIGHT, [-0.22, 0.0, 0.0], 0.10, 0.0))
    footStepListPublisher.publish(msgL)
    rospy.loginfo('step to a better plug position...')
    waitForFootsteps(len(msgL.footstep_data_list))
    rossleep(0.5)

    rospy.loginfo('Module T2C5rand complete. Advancing to T2C6...')
    if killed == False and shutdown == False:
        checkpointT2C6()


def checkpointT2C6(): #Walk into Task 2's finish box
    global killed
    global shutdown
    
    handMsg2 = Float64MultiArray()
    handMsg2.data = [0.0, 0.0, 0.0, 0.0, 0.0]
    leftHandPositionPublisher.publish(handMsg2)

    handMsg3 = Float64MultiArray()
    handMsg3.data = [0.0, 0.0, 0.0, 0.0, 0.0]
    rightHandPositionPublisher.publish(handMsg3)

    chestTrajectoryPublisher.publish(createChestMsg(0.0, 0.0, 0.0, 1.0))    
    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([0.30, 0.0, 0.00], 1.0))
    pelvisHeightTrajectoryPublisher.publish(createPelvisMsg(1.12, 1.5)) 
    sendArmTrajectory('ARM_IN_RIGHT')
    rossleep(0.35)
    sendArmTrajectory('ARM_IN_LEFT')
    rossleep(2.0)
    if killed == True or shutdown == True:
        return 0

    #turn around to see part of the map for wandering
    for j in range(0, 8):
        msgS2 = FootstepDataListRosMessage()
        msgS2.default_transfer_time = 0.80 #on two legs
        msgS2.default_swing_time = 0.80 #on one leg
        msgS2.execution_mode = 0
        msgS2.unique_id = -1
    
        list = createRotationFootStepList(-0.785)
        for i in range(0, len(list)):
            msgS2.footstep_data_list.append(list[i])
        footStepListPublisher.publish(msgS2)
        rospy.loginfo('waiting for %i steps...' % len(list))
        waitForFootsteps(len(msgS2.footstep_data_list))
        if killed == True or shutdown == True:
            return 0
    
    startWandering()
    waitForTask2()
    stopWandering()

    rospy.loginfo('Module T2C6 complete. Task 2 finished. Advancing to module T3C1...')
    if killed == False and shutdown == False:
        checkpointT3C1()

    

def checkpointT3C1(): #Climb the stairs (go to the stairs)
    global killed
    global shutdown

    stMsg = String()
    stMsg.data = 'gooo'
    goToStairsPublisher.publish(stMsg)
    rospy.loginfo('navigating to the stairs')
    waitForNavigation()
    rospy.loginfo('Module T3C1 complete: reached the base of the stairs. Now correct angle by 45 degrees or call T3C1b.')
    return 0
    

def checkpointT3C1b(): #Climb the stairs (baby steps)
    global killed
    global shutdown

    rsMsg = String()
    rsMsg.data = ''
    receivedResetPoseMsg(rsMsg)

    rospy.loginfo('taking baby steps...')
    stepWithTrajectory(LEFT, [[-0.02, 0.0, 0.04]], 0.8, 1.4, True)
    stepWithTrajectory(LEFT, [[0.07, 0.0, 0.00]], 0.8, 1.4, False)
    stepWithTrajectory(LEFT, [[0.00, 0.0, -0.08]], 0.8, 1.4, False)
    stepWithTrajectory(RIGHT, [[-0.02, 0.0, 0.04]], 0.8, 1.4, True)
    stepWithTrajectory(RIGHT, [[0.07, 0.0, 0.00]], 0.8, 1.4, False)
    stepWithTrajectory(RIGHT, [[0.00, 0.0, -0.08]], 0.8, 1.4, False)
    if killed == True or shutdown == True:
        return 0
    
    stepWithTrajectory(LEFT, [[-0.02, 0.0, 0.04]], 0.8, 1.4, True)
    stepWithTrajectory(LEFT, [[0.07, 0.0, 0.00]], 0.8, 1.4, False)
    stepWithTrajectory(LEFT, [[0.00, 0.0, -0.08]], 0.8, 1.4, False)
    stepWithTrajectory(RIGHT, [[-0.02, 0.0, 0.04]], 0.8, 1.4, True)
    stepWithTrajectory(RIGHT, [[0.07, 0.0, 0.00]], 0.8, 1.4, False)
    stepWithTrajectory(RIGHT, [[0.00, 0.0, -0.08]], 0.8, 1.4, False)
    if killed == True or shutdown == True:
        return 0
    
    stepWithTrajectory(LEFT, [[-0.02, 0.0, 0.04]], 0.8, 1.4, True)
    stepWithTrajectory(LEFT, [[0.07, 0.0, 0.00]], 0.8, 1.4, False)
    stepWithTrajectory(LEFT, [[0.00, 0.0, -0.08]], 0.8, 1.4, False)
    stepWithTrajectory(RIGHT, [[-0.02, 0.0, 0.04]], 0.8, 1.4, True)
    stepWithTrajectory(RIGHT, [[0.07, 0.0, 0.00]], 0.8, 1.4, False)
    stepWithTrajectory(RIGHT, [[0.00, 0.0, -0.08]], 0.8, 1.4, False)
    if killed == True or shutdown == True:
        return 0
    
    stepWithTrajectory(LEFT, [[-0.02, 0.0, 0.04]], 0.8, 1.4, True)
    stepWithTrajectory(LEFT, [[0.07, 0.0, 0.00]], 0.8, 1.4, False)
    stepWithTrajectory(LEFT, [[0.00, 0.0, -0.08]], 0.8, 1.4, False)
    stepWithTrajectory(RIGHT, [[-0.02, 0.0, 0.04]], 0.8, 1.4, True)
    stepWithTrajectory(RIGHT, [[0.07, 0.0, 0.00]], 0.8, 1.4, False)
    stepWithTrajectory(RIGHT, [[0.00, 0.0, -0.08]], 0.8, 1.4, False)
    if killed == True or shutdown == True:
        return 0
    
    stepWithTrajectory(LEFT, [[-0.02, 0.0, 0.04]], 0.8, 1.4, True)
    stepWithTrajectory(LEFT, [[0.07, 0.0, 0.00]], 0.8, 1.4, False)
    stepWithTrajectory(LEFT, [[0.00, 0.0, -0.08]], 0.8, 1.4, False)
    stepWithTrajectory(RIGHT, [[-0.02, 0.0, 0.04]], 0.8, 1.4, True)
    stepWithTrajectory(RIGHT, [[0.07, 0.0, 0.00]], 0.8, 1.4, False)
    stepWithTrajectory(RIGHT, [[0.00, 0.0, -0.08]], 0.8, 1.4, False)
    if killed == True or shutdown == True:
        return 0
    
    stepWithTrajectory(LEFT, [[-0.02, 0.0, 0.04]], 0.8, 1.4, True)
    stepWithTrajectory(LEFT, [[0.07, 0.0, 0.00]], 0.8, 1.4, False)
    stepWithTrajectory(LEFT, [[0.00, 0.0, -0.08]], 0.8, 1.4, False)
    stepWithTrajectory(RIGHT, [[-0.02, 0.0, 0.04]], 0.8, 1.4, True)
    stepWithTrajectory(RIGHT, [[0.07, 0.0, 0.00]], 0.8, 1.4, False)
    stepWithTrajectory(RIGHT, [[0.00, 0.0, -0.08]], 0.8, 1.4, False)
    if killed == True or shutdown == True:
        return 0
    
    stepWithTrajectory(LEFT, [[-0.02, 0.0, 0.04]], 0.8, 1.4, True)
    stepWithTrajectory(LEFT, [[0.07, 0.0, 0.00]], 0.8, 1.4, False)
    stepWithTrajectory(LEFT, [[0.00, 0.0, -0.08]], 0.8, 1.4, False)
    stepWithTrajectory(RIGHT, [[-0.02, 0.0, 0.04]], 0.8, 1.4, True)
    stepWithTrajectory(RIGHT, [[0.07, 0.0, 0.00]], 0.8, 1.4, False)
    stepWithTrajectory(RIGHT, [[0.00, 0.0, -0.08]], 0.8, 1.4, False)
    if killed == True or shutdown == True:
        return 0
    
    resetFootGap()
    
    rospy.loginfo('reached the base of the stairs')

    rospy.loginfo('Module T3C1b complete. Advancing to T3C1c...')
    if killed == False and shutdown == False:
        checkpointT3C1c()


def checkpointT3C1c(): #Climb the stairs (climb up)
    global killed
    global shutdown

    sendArmTrajectory('STRAIGHT_FORWARD_LEFT')
    sendArmTrajectory('STRAIGHT_FORWARD_RIGHT')

    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([0.50, 0.0, 0.00], 1.0))
    
    rossleep(0.2)
    if killed == True or shutdown == True:
        return 0
        
    rospy.loginfo('setting pelvis height...')
    pelvisHeightTrajectoryPublisher.publish(createPelvisMsg(1.14, 1.0)) 
    
    rossleep(0.2)
    if killed == True or shutdown == True:
        return 0
    
    rospy.loginfo('setting chest trajectory...')
    chestTrajectoryPublisher.publish(createChestMsg(0.0, 0.41, 0.0, 1.0))
    
    rossleep(1.0)
    if killed == True or shutdown == True:
        return 0

    loadLeg(RIGHT)

    msg = FootstepDataListRosMessage()
    msg.default_transfer_time = 1.80 #on two legs
    msg.default_swing_time = 1.80 #on one leg
    msg.execution_mode = 0
    msg.unique_id = -1

    msg.footstep_data_list.append(createFootStepOffset(LEFT, [0.28, 0.01, 0.10], 0.10, 0.0))
    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [0.26, -0.01, 0.10], 0.13, 0.0))
    footStepListPublisher.publish(msg)
    rospy.loginfo('walk forward...')
    waitForFootsteps(len(msg.footstep_data_list))
    msg.footstep_data_list = []
    if killed == True or shutdown == True:
        return 0

    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [0.28, 0.00, 0.18], 0.10, 0.0))
    msg.footstep_data_list.append(createFootStepOffset(LEFT, [0.28, 0.00, 0.21], 0.19, 0.0))
    footStepListPublisher.publish(msg)
    rospy.loginfo('walk forward...')
    waitForFootsteps(len(msg.footstep_data_list))
    msg.footstep_data_list = []
    if killed == True or shutdown == True:
        return 0

    msg.footstep_data_list.append(createFootStepOffset(LEFT, [0.28, 0.01, 0.18], 0.10, 0.0))
    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [0.28, -0.01, 0.21], 0.18, 0.0))
    footStepListPublisher.publish(msg)
    rospy.loginfo('walk forward...')
    waitForFootsteps(len(msg.footstep_data_list))
    msg.footstep_data_list = []
    if killed == True or shutdown == True:
        return 0

    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [0.28, -0.01, 0.18], 0.10, 0.0))
    msg.footstep_data_list.append(createFootStepOffset(LEFT, [0.28, 0.01, 0.21], 0.17, 0.0))
    footStepListPublisher.publish(msg)
    rospy.loginfo('walk forward...')
    waitForFootsteps(len(msg.footstep_data_list))
    msg.footstep_data_list = []
    if killed == True or shutdown == True:
        return 0

    msg.footstep_data_list.append(createFootStepOffset(LEFT, [0.28, 0.01, 0.18], 0.10, 0.0))
    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [0.28, -0.01, 0.21], 0.17, 0.0))
    footStepListPublisher.publish(msg)
    rospy.loginfo('walk forward...')
    waitForFootsteps(len(msg.footstep_data_list))
    msg.footstep_data_list = []
    if killed == True or shutdown == True:
        return 0

    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [0.28, -0.01, 0.18], 0.10, 0.0))
    msg.footstep_data_list.append(createFootStepOffset(LEFT, [0.28, 0.01, 0.21], 0.17, 0.0))
    footStepListPublisher.publish(msg)
    rospy.loginfo('walk forward...')
    waitForFootsteps(len(msg.footstep_data_list))
    msg.footstep_data_list = []
    if killed == True or shutdown == True:
        return 0

    msg.footstep_data_list.append(createFootStepOffset(LEFT, [0.28, 0.01, 0.18], 0.10, 0.0))
    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [0.28, -0.01, 0.20], 0.17, 0.0))
    footStepListPublisher.publish(msg)
    rospy.loginfo('walk forward...')
    waitForFootsteps(len(msg.footstep_data_list))
    msg.footstep_data_list = []
    if killed == True or shutdown == True:
        return 0

    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [0.28, 0.00, 0.18], 0.10, 0.0))
    msg.footstep_data_list.append(createFootStepOffset(LEFT, [0.28, 0.0, 0.20], 0.17, 0.0))
    footStepListPublisher.publish(msg)
    rospy.loginfo('walk forward...')
    waitForFootsteps(len(msg.footstep_data_list))
    msg.footstep_data_list = []
    if killed == True or shutdown == True:
        return 0

    msg.footstep_data_list.append(createFootStepOffset(LEFT, [0.25, 0.00, 0.18], 0.10, 0.0))
    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [0.25, 0.00, 0.20], 0.16, 0.0))
    footStepListPublisher.publish(msg)
    rospy.loginfo('walk forward...')
    waitForFootsteps(len(msg.footstep_data_list))
    msg.footstep_data_list = []
    if killed == True or shutdown == True:
        return 0

    rospy.loginfo('reached the top of the stairs')
    rossleep(1.0)
    
    rospy.loginfo('step forward once and reset pose')
    msg.footstep_data_list = []
    msg.footstep_data_list.append(createFootStepOffset(LEFT, [0.25, 0.00, 0.0], 0.10, 0.0))
    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [0.25, 0.00, 0.0], 0.10, 0.0))
    footStepListPublisher.publish(msg)
    rospy.loginfo('walk forward...')
    waitForFootsteps(len(msg.footstep_data_list))
    msg.footstep_data_list = []
    if killed == True or shutdown == True:
        return 0

    rsMsg = String()
    rsMsg.data = ''
    receivedResetPoseMsg(rsMsg)
    rossleep(1.0)

    rospy.loginfo('Module T3C1c complete. Advancing to T3C2...')
    if killed == False and shutdown == False:
        checkpointT3C2()


def checkpointT3C2(): #Go to the wheel
    global killed
    global shutdown

    rospy.loginfo('aligning to the wheel')

    bashCommand="rostopic pub --once /humanz/task3/align_to_wheel std_msgs/String 'whatever'"
    align_process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
    time.sleep(5)
    align_process.terminate()

    waitForNavigation()
    rospy.loginfo('in proper direction to the wheel')
    if killed == True or shutdown == True:
        return 0

    #walk 1.374m forward
    msg = FootstepDataListRosMessage()
    msg.default_transfer_time = 1.80 #on two legs
    msg.default_swing_time = 1.80 #on one leg
    msg.execution_mode = 0
    msg.unique_id = -1

    msg.footstep_data_list.append(createFootStepOffset(LEFT, [0.27, 0.00, 0.10], 0.10, 0.0))
    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [0.54, -0.00, 0.10], 0.10, 0.0))
    msg.footstep_data_list.append(createFootStepOffset(LEFT, [0.81, 0.00, 0.10], 0.10, 0.0))
    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [1.08, -0.00, 0.10], 0.10, 0.0))
    msg.footstep_data_list.append(createFootStepOffset(LEFT, [1.374, 0.00, 0.10], 0.10, 0.0))
    msg.footstep_data_list.append(createFootStepOffset(RIGHT, [1.374, -0.00, 0.10], 0.10, 0.0))

    footStepListPublisher.publish(msg)
    rospy.loginfo('walk forward...')
    waitForFootsteps(len(msg.footstep_data_list))
    rossleep(0.2)

    pelvisHeightTrajectoryPublisher.publish(createPelvisMsg(0.96, 1.0)) 
    
    rospy.loginfo('Module T3C2 complete. Check the position to the wheel and call T3C2b or T3C2bOnce(recommended for the first try) manually.')
    return 0


def checkpointT3C2b(): #Turn wheel until door is open
    global killed
    global shutdown

#    while not (currentTaskStatus.task == 3 and currentTaskStatus.current_checkpoint == 3):
    for i in range(0,20):       
        #slightly close fingers    
        handMsg2 = Float64MultiArray()
        handMsg2.data = [1.4, 0, -0.3, -0.3, -0.3]
        leftHandPositionPublisher.publish(handMsg2)
        rossleep(2.0)
        handMsg3 = Float64MultiArray()
        handMsg3.data = [1.4, 0, 0.3, 0.3, 0.3]
        rightHandPositionPublisher.publish(handMsg3)
        rossleep(0.5)
        if killed == True or shutdown == True:
            return 0

        sendArmTrajectory('WHEEL_START')
        rossleep(3.0)

        sendArmTrajectory('WHEEL_CLOSE')
        rossleep(2.5)
        if killed == True or shutdown == True:
            return 0
        
        #grab firmly
        handMsg2 = Float64MultiArray()
        handMsg2.data = [1.4, -0.9, -1.0, -1.0, -1.0]
        leftHandPositionPublisher.publish(handMsg2)
        rossleep(0.1)
        handMsg3 = Float64MultiArray()
        handMsg3.data = [1.4, 0.9, 1.0, 1.0, 1.0]
        rightHandPositionPublisher.publish(handMsg3)
        rossleep(1.5)

        sendArmTrajectory('WHEEL_TURN')
        rossleep(3.0)
        if killed == True or shutdown == True:
            return 0


    rospy.loginfo('door unlocked')

    handMsg2 = Float64MultiArray()
    handMsg2.data = [1.4, 0, -0.3, -0.3, -0.3]
    leftHandPositionPublisher.publish(handMsg2)
    rossleep(0.5)
    handMsg3 = Float64MultiArray()
    handMsg3.data = [1.4, 0, 0.3, 0.3, 0.3]
    rightHandPositionPublisher.publish(handMsg3)
    rossleep(0.8)

    sendArmTrajectory('WHEEL_START')
    rossleep(2.0)

    rospy.loginfo('Checkpoint T2C3b complete. Advancing to T3C2c...')
    if killed == False and shutdown == False:
        checkpointT3C2c()
    

def checkpointT3C2bOnce(): #Turn wheel once
    global killed
    global shutdown

    #slightly close fingers    
    handMsg2 = Float64MultiArray()
    handMsg2.data = [1.4, 0, -0.3, -0.3, -0.3]
    leftHandPositionPublisher.publish(handMsg2)
    rossleep(2.0)
    handMsg3 = Float64MultiArray()
    handMsg3.data = [1.4, 0, 0.3, 0.3, 0.3]
    rightHandPositionPublisher.publish(handMsg3)
    rossleep(0.5)
    if killed == True or shutdown == True:
        return 0

    sendArmTrajectory('WHEEL_START')
    rossleep(3.0)
    if killed == True or shutdown == True:
        return 0

    sendArmTrajectory('WHEEL_CLOSE')
    rossleep(2.5)
    if killed == True or shutdown == True:
        return 0
    
    #grab firmly
    handMsg2 = Float64MultiArray()
    handMsg2.data = [1.4, -0.9, -1.0, -1.0, -1.0]
    leftHandPositionPublisher.publish(handMsg2)
    rossleep(0.1)
    handMsg3 = Float64MultiArray()
    handMsg3.data = [1.4, 0.9, 1.0, 1.0, 1.0]
    rightHandPositionPublisher.publish(handMsg3)
    rossleep(1.5)
    if killed == True or shutdown == True:
        return 0

    sendArmTrajectory('WHEEL_TURN')
    rossleep(3.0)
    if killed == True or shutdown == True:
        return 0


    handMsg2 = Float64MultiArray()
    handMsg2.data = [1.4, 0, -0.3, -0.3, -0.3]
    leftHandPositionPublisher.publish(handMsg2)
    rossleep(0.5)
    handMsg3 = Float64MultiArray()
    handMsg3.data = [1.4, 0, 0.3, 0.3, 0.3]
    rightHandPositionPublisher.publish(handMsg3)
    rossleep(0.8)

    sendArmTrajectory('WHEEL_START')
    rossleep(2.0)

    rospy.loginfo('Module C3T2bOnce complete. Repeat or call T3C2c manually.')
    return 0


def checkpointT3C2c(): #Step through the door
    global killed
    global shutdown
    
    pushDoorSafe()
    rospy.loginfo('door push finished')
    rospy.loginfo('Module T3C2c complete. Now pick up the leak detector manually or skip to checkpoint 8 and go to the finish box.')
    return 0


def convertHeadToPelvis(pos):
    headPelvis = tfBuffer.lookup_transform('pelvis', 'head', rospy.Time(),rospy.Duration(1.0))
    headPoint = FootstepDataRosMessage()
    headPoint.orientation = headPelvis.transform.rotation
    headPoint.location = headPelvis.transform.translation
    quat = headPoint.orientation
    rot = tf.transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
    transformedOffset = numpy.dot(rot[0:3, 0:3], [pos.x, pos.y, pos.z])
    
    result = Position()

    result.x = headPoint.location.x + transformedOffset[0]
    result.y = headPoint.location.y + transformedOffset[1]
    result.z = headPoint.location.z + transformedOffset[2]
  
    return result


def pushDoorSafe():
    global killed
    global shutdown
    
    sendArmTrajectory('PREPARE_FOR_DOOR_PUSH')
    rossleep(1.5)
    
    for i in range(0,4):
        sendArmTrajectory('DOOR_PUSH')
        rossleep(3.5)
        if killed == True or shutdown == True:
            return 0
        sendArmTrajectory('PREPARE_FOR_DOOR_PUSH')
        rossleep(0.2)
        if killed == True or shutdown == True:
            return 0
    
        msg = FootstepDataListRosMessage()
        msg.default_transfer_time = 0.90 #on two legs
        msg.default_swing_time = 0.90 #on one leg
        msg.execution_mode = 0
        msg.unique_id = -1
        msg.footstep_data_list.append(createFootStepOffset(LEFT, [0.23, 0.00, 0.0], 0.0, 0.0))
        msg.footstep_data_list.append(createFootStepOffset(RIGHT, [0.23, -0.00, 0.0], 0.0, 0.0))
        footStepListPublisher.publish(msg)
        rospy.loginfo('walk forward...')
        waitForFootsteps(len(msg.footstep_data_list))
        msg.footstep_data_list = []
        if killed == True or shutdown == True:
            return 0

    sendArmTrajectory('DOOR_PUSH')
    rossleep(3.5)
    if killed == True or shutdown == True:
        return 0
    sendArmTrajectory('DOOR_PUSH_SPREAD')
    rossleep(1.5)
    if killed == True or shutdown == True:
        return 0
    sendArmTrajectory('PREPARE_FOR_DOOR_PUSH')
    rossleep(0.2)
    if killed == True or shutdown == True:
        return 0
        
    rospy.loginfo('door push complete')


def turnWheels():
    global targetPitch
    global targetYaw
    global curentPitch
    global currentYaw
    
    #increments in rad
    crudeIncrement = 0.52
    fineIncrement = 0.087
    #which way to turn the wheel to increase pitch/yaw
    pitchIncreaseDirection = 1
    yawIncreaseDirection = 1
    
    #LEFT: crude first phase
    if currentYaw < targetYaw:
        iterateWheelAngles(LEFT, yawIncreaseDirection * crudeIncrement)
    else:
        iterateWheelAngles(LEFT, -1 * yawIncreaseDirection * crudeIncrement)

    #LEFT: fine second phase
    if currentYaw < targetYaw:
        iterateWheelAngles(LEFT, yawIncreaseDirection * fineIncrement)
    else:
        iterateWheelAngles(LEFT, -1 * yawIncreaseDirection * fineIncrement)

    #RIGHT: crude first phase
    if currentPitch < targetPitch:
        iterateWheelAngles(RIGHT, pitchIncreaseDirection * crudeIncrement)
    else:
        iterateWheelAngles(RIGHT, -1 * pitchIncreaseDirection * crudeIncrement)

    #RIGHT: fine second phase
    if currentPitch < targetPitch:
        iterateWheelAngles(RIGHT, pitchIncreaseDirection * fineIncrement)
    else:
        iterateWheelAngles(RIGHT, -1 * pitchIncreaseDirection * fineIncrement)
    
    
def iterateWheelAngles(side, increment):
    global targetPitch
    global targetYaw
    global curentPitch
    global currentYaw
    
    sleepTime = 1.5
    
    if side == LEFT:
        lastYawDiff = targetYaw - currentYaw
        while math.copysign(1, targetYaw - currentYaw) == math.copysign(1, lastYawDiff): #while don't get to the other side of the target
            lastYawDiff = targetYaw - currentYaw
            turnWheel(LEFT, increment)
            rossleep(sleepTime)
            
        if abs(targetYaw - currentYaw) > abs(lastYawDiff): #the previous iteration was closer
            turnWheel(LEFT, -1 * increment)
    
    else:
        lastPitchDiff = targetPitch - currentPitch
        while math.copysign(1, targetPitch - currentPitch) == math.copysign(1, lastPitchDiff): #while don't get to the other side of the target
            lastPitchDiff = targetPitch - currentPitch
            turnWheel(RIGHT, increment)
            rossleep(sleepTime)
            
        if abs(targetPitch - currentPitch) > abs(lastPitchDiff): #the previous iteration was closer
            turnWheel(RIGHT, -1 * increment)
        

def grabWheels():
    #TODO Pista to implement
    
    rospy.loginfo('Pista didn\'t implement this')
    
    
def letGoOfWheels():
    #TODO Pista to implement
    
    rospy.loginfo('Pista didn\'t implement this')


def turnWheel(side, rad):
    #TODO Pista to implement
    
    rospy.loginfo('Pista didn\'t implement this')

    
def getWorldPointFromHeadPoint(headX, headY, headZ):
    headPoint = PointStamped()
   
    headPoint.header.frame_id = 'head'   
    headPoint.header.stamp = rospy.Time()
   
    headPoint.point.x = headX
    headPoint.point.y = headY
    headPoint.point.z = headZ
   
    worldPoint = tfBuffer.transform(headPoint, 'world')

    worldPosition = Position()
    worldPosition.x = worldPoint.point.x
    worldPosition.y = worldPoint.point.y
    worldPosition.z = worldPoint.point.z
    
    rospy.loginfo('world target point is x:[%f], y:[%f]' % (worldPosition.x, worldPosition.y))
    
    return worldPosition
   
    
def getWorldPointFromHeadPoint2(headX, headY, headZ):
    #using footstep only as a data structure, TODO refactor later
    footstep = FootstepDataRosMessage()

    footWorld = tfBuffer.lookup_transform('world', 'head', rospy.Time(),rospy.Duration(1.0))
    footstep.orientation = footWorld.transform.rotation
    footstep.location = footWorld.transform.translation

    # transform the offset to world frame
    quat = footstep.orientation
    rot = tf.transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
    transformedOffset = numpy.dot(rot[0:3, 0:3], [headX, headY, headZ])

    footstep.location.x += transformedOffset[0]
    footstep.location.y += transformedOffset[1]
    footstep.location.z += transformedOffset[2]

    worldPoint = Position()
    worldPoint.x = footstep.location.x
    worldPoint.y = footstep.location.y
    worldPoint.z = footstep.location.z
    
    rospy.loginfo('world target point is x:[%f], y:[%f]' % (worldPoint.x, worldPoint.y))
    
    return worldPoint


def getHeadTargetInWorld(distanceFromPanel):
    rospy.loginfo('############################################################################################')
    xPos = (blueWheelPosition.x + redWheelPosition.x) / 2
    yPos = (blueWheelPosition.y + redWheelPosition.y) / 2
    #TODO this is fucking random? - it fluctuates because readings are unstable
    alpha = math.atan(abs(blueWheelPosition.x - redWheelPosition.x) / abs(blueWheelPosition.y - redWheelPosition.y))
    yShift = math.sin(alpha) * distanceFromPanel
    xShift = math.cos(alpha) * distanceFromPanel
    pointX = xPos - xShift

    if blueWheelPosition.x > redWheelPosition.x:
        if blueWheelPosition.y > redWheelPosition.y:
            yShift = -1 * yShift
    else:
        if redWheelPosition.y > blueWheelPosition.y:
            yShift = -1 * yShift
            
    pointY = yPos - yShift

    #checking if the alternative point ("behind the panel") is actually closer, in which case that should be our target, even if the angle suggests otherwise
    pointX2 = xPos + xShift
    pointY2 = yPos + yShift
    if math.sqrt(pointX2*pointX2 + pointY2*pointY2) < math.sqrt(pointX*pointX + pointY*pointY):
        pointX = pointX2
        pointY = pointY2
        
    if pointY < yPos:
        alpha = -1 * alpha

    rospy.loginfo('blue xyz [%f] [%f] [%f]' % (blueWheelPosition.x, blueWheelPosition.y, blueWheelPosition.z))
    rospy.loginfo('red xyz [%f] [%f] [%f]' % (redWheelPosition.x, redWheelPosition.y, redWheelPosition.z))
    rospy.loginfo('xpos [%f]' % xPos)
    rospy.loginfo('ypos [%f]' % yPos)
    rospy.loginfo('alpha [%f]' % alpha)
    rospy.loginfo('yShift [%f]' % yShift)
    rospy.loginfo('xShift [%f]' % xShift)
    rospy.loginfo('pointX [%f]' % pointX)
    rospy.loginfo('pointY [%f]' % pointY)
    
    rospy.loginfo('TARGET IN HEAD FRAME COORDINATES IS: x=[%f], y=[%f], z=[%f], angle=[%f]' % (pointX, pointY, blueWheelPosition.z, alpha))

    #convert the head frame point to world frame point
    headWorld = tfBuffer.lookup_transform('world', 'head', rospy.Time(),rospy.Duration(1.0))
    headPoint = FootstepDataRosMessage()
    headPoint.orientation = headWorld.transform.rotation
    headPoint.location = headWorld.transform.translation
    quat = headPoint.orientation
    rot = tf.transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
    transformedOffset = numpy.dot(rot[0:3, 0:3], [pointX, pointY, blueWheelPosition.z])

    rospy.loginfo('TARGET IN WORLD FRAME COORDINATES IS: x=[%f], y=[%f], z=[%f], angle=[%f]' % (headPoint.location.x + transformedOffset[0], headPoint.location.y + transformedOffset[1], 0, alpha + getHeadAngle()))
    
    return [headPoint.location.x + transformedOffset[0], headPoint.location.y + transformedOffset[1], alpha + getHeadAngle()]


#get the point halfway between the two feet
def getFootSourceInWorld():
    foot_frame_left = LEFT_FOOT_FRAME_NAME
    foot_frame_right = RIGHT_FOOT_FRAME_NAME

    footWorldLeft = tfBuffer.lookup_transform('world', foot_frame_left, rospy.Time(),rospy.Duration(1.0))
    rotL = footWorldLeft.transform.rotation    
    angL = tf.transformations.euler_from_quaternion([rotL.x, rotL.y, rotL.z, rotL.w], 'rzyx')

    footWorldRight = tfBuffer.lookup_transform('world', foot_frame_right, rospy.Time(),rospy.Duration(1.0))
    rotR = footWorldRight.transform.rotation    
    angR = tf.transformations.euler_from_quaternion([rotR.x, rotR.y, rotR.z, rotR.w], 'rzyx')

    footX = (footWorldLeft.transform.translation.x + footWorldRight.transform.translation.x) / 2
    footY = (footWorldLeft.transform.translation.y + footWorldRight.transform.translation.y) / 2
    footAng = (angL[0] + angR[0]) / 2

    rospy.loginfo('AVG FOOT LOCATION IN WORLD FRAME COORDINATES IS: x=[%f], y=[%f], angle=[%f]' % (footX, footY, footAng))
    
    return [footX, footY, footAng]


def interpolateWayPoints(fromX, fromY, fromAngle, toX, toY, toAngle):
    rospy.loginfo("INTERPOLATION fromX [%f], fromY [%f], fromAngle [%f], toX [%f], toY [%f], toAngle [%f]" % (fromX, fromY, fromAngle, toX, toY, toAngle))
    
    MAX_STEP_SIZE = 0.4
    distance = math.sqrt(math.pow(toX - fromX, 2) + math.pow(toY - fromY, 2))
    stepCount = int(math.ceil(distance / MAX_STEP_SIZE))
    
    wayPoints = []
    for stepNum in range(1, stepCount + 1):
        if stepNum == 1:#small extra step at the beginning
            firstWayPoint = Waypoint2D()
            firstWayPoint.x = fromX + (toX - fromX) / stepCount * stepNum / 2
            firstWayPoint.y = fromY + (toY - fromY) / stepCount * stepNum / 2
            firstWayPoint.angle = fromAngle + (toAngle - fromAngle) / stepCount * stepNum / 2
            wayPoints.append(firstWayPoint)
        wayPoint = Waypoint2D()
        wayPoint.x = fromX + (toX - fromX) / stepCount * stepNum
        wayPoint.y = fromY + (toY - fromY) / stepCount * stepNum
        wayPoint.angle = fromAngle + (toAngle - fromAngle) / stepCount * stepNum
        wayPoints.append(wayPoint)
    return wayPoints
    
    
def createFootStepFromWayPoint(stepSide, wayPoint, z, swingHeight):
    if stepSide == LEFT:
        correction = 0.16
    else:
        correction = 0.16

    xCorr = math.sin(wayPoint.angle) * correction
    yCorr = math.cos(wayPoint.angle) * correction

    if stepSide == LEFT:
        return createFootStepInWorld(stepSide, [wayPoint.x - xCorr, wayPoint.y + yCorr, z], swingHeight, wayPoint.angle)    
    else:
        return createFootStepInWorld(stepSide, [wayPoint.x + xCorr, wayPoint.y - yCorr, z], swingHeight, wayPoint.angle)    


#offset is already in world coordinates    
def createFootStepInWorld(stepSide, offset, swingHeight, turn):
    footstep = FootstepDataRosMessage()
    footstep.robot_side = stepSide
    footstep.swing_height = swingHeight

    footstep.location.x = offset[0]
    footstep.location.y = offset[1]
    footstep.location.z = offset[2]

    rospy.loginfo("@@@@@@@ WORLD FOOTSTEP LOCATION @@@@@@@@@@ [%s] [%s] [%s] " % (footstep.location.x, footstep.location.y, footstep.location.z))

    pista_quaternion = tf.transformations.quaternion_from_euler(turn, 0, 0, 'rzyx')
    
    footstep.orientation.x = pista_quaternion[0]
    footstep.orientation.y = pista_quaternion[1]
    footstep.orientation.z = pista_quaternion[2]
    footstep.orientation.w = pista_quaternion[3]    
    
    pprint.pprint(turn)
    pprint.pprint(tf.transformations.euler_from_quaternion([pista_quaternion[0], pista_quaternion[1], pista_quaternion[2], pista_quaternion[3]], 'rzyx'))
    
    return footstep

    
# Creates footstep with the current position and orientation of the foot.
def createFootStepInPlace(stepSide, swingHeight):
    footstep = FootstepDataRosMessage()
    footstep.robot_side = stepSide
    footstep.swing_height = swingHeight
    
    if stepSide == LEFT:
        foot_frame = LEFT_FOOT_FRAME_NAME
    else:
        foot_frame = RIGHT_FOOT_FRAME_NAME

    footWorld = tfBuffer.lookup_transform('world', foot_frame, rospy.Time(),rospy.Duration(1.0))
    footstep.orientation = footWorld.transform.rotation
    footstep.location = footWorld.transform.translation
    
#    rospy.loginfo("orientation of inplace step:")
#    ang = tf.transformations.euler_from_quaternion([footstep.orientation.x, footstep.orientation.y, footstep.orientation.z, footstep.orientation.w], 'rzyx')
#    pprint.pprint(ang)
    
    return footstep


def stepWithTrajectory(stepSide, points, trajTime, sleepTime, load, turn=-999.0):
    if load == True:
        if stepSide == LEFT:
            loadLeg(RIGHT)
        else:
            loadLeg(LEFT)

    msg1 = FootTrajectoryRosMessage()
    msg1.execution_mode = 0
    msg1.robot_side = stepSide
    msg1.unique_id = rospy.Time.now().nsecs
    
    for point in points:
        rospy.loginfo('adding trajectory point [[%f], [%f], [%f]] to foot [%i]' % (point[0], point[1], point[2], stepSide))
        traj1 = createFootTrajectoryOffset(stepSide, point, trajTime, turn)
        msg1.taskspace_trajectory_points.append(traj1)
    
    footTrajectoryPublisher.publish(msg1)
    rossleep(sleepTime)


def loadLeg(stepSide):
    rospy.loginfo('loading leg [%i]' % stepSide)
    
    loadMsg = EndEffectorLoadBearingRosMessage()
    loadMsg.end_effector = 0
    loadMsg.request = 0
    loadMsg.robot_side = stepSide
    loadMsg.unique_id=rospy.Time.now().nsecs
    loadPublisher.publish(loadMsg)
    rossleep(2.0)


def createFootTrajectoryOffset(stepSide, offset, time, turn):
    traj = createFootTrajectoryInPlace(stepSide)

    # transform the offset to world frame
    quat = traj.orientation
    rot = tf.transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
    transformedOffset = numpy.dot(rot[0:3, 0:3], offset)

    traj.time = time

    traj.position.x += transformedOffset[0]
    traj.position.y += transformedOffset[1]
    traj.position.z += transformedOffset[2]
    
    if turn != -999.0:
        pista_quaternion = tf.transformations.quaternion_from_euler(turn, 0, 0, 'rzyx')
        
        traj.orientation.x = pista_quaternion[0]
        traj.orientation.y = pista_quaternion[1]
        traj.orientation.z = pista_quaternion[2]
        traj.orientation.w = pista_quaternion[3]  
    
    return traj


def createFootTrajectoryInPlace(stepSide):
    traj = SE3TrajectoryPointRosMessage()
    
    if stepSide == LEFT:
        foot_frame = LEFT_FOOT_FRAME_NAME
    else:
        foot_frame = RIGHT_FOOT_FRAME_NAME

    footWorld = tfBuffer.lookup_transform('world', foot_frame, rospy.Time(),rospy.Duration(1.0))
    traj.orientation = footWorld.transform.rotation
    traj.position = footWorld.transform.translation

    return traj


def createRotationFootStepList(yaw):
    left_footstep = FootstepDataRosMessage()
    left_footstep.robot_side = FootstepDataRosMessage.LEFT
    right_footstep = FootstepDataRosMessage()
    right_footstep.robot_side = FootstepDataRosMessage.RIGHT

    left_foot_world = tfBuffer.lookup_transform(
        'world', LEFT_FOOT_FRAME_NAME, rospy.Time(),rospy.Duration(1.0))
    right_foot_world = tfBuffer.lookup_transform(
        'world', RIGHT_FOOT_FRAME_NAME, rospy.Time(),rospy.Duration(1.0))
    intermediate_transform = Transform()
    # create a virtual fram between the feet, this will be the center of the rotation
    intermediate_transform.translation.x = (
        left_foot_world.transform.translation.x + right_foot_world.transform.translation.x)/2.
    intermediate_transform.translation.y = (
        left_foot_world.transform.translation.y + right_foot_world.transform.translation.y)/2.
    intermediate_transform.translation.z = (
        left_foot_world.transform.translation.z + right_foot_world.transform.translation.z)/2.
    # here we assume that feet have the same orientation so we can pick arbitrary left or right
    intermediate_transform.rotation = left_foot_world.transform.rotation

    left_footstep.location = left_foot_world.transform.translation
    right_footstep.location = right_foot_world.transform.translation

    # define the turning radius
    radius = math.sqrt(
        (
            right_foot_world.transform.translation.x -
            left_foot_world.transform.translation.x
        )**2 + (
            right_foot_world.transform.translation.y -
            left_foot_world.transform.translation.y
        )**2) / 2.

    left_offset = [-radius*math.sin(yaw), radius*(1-math.cos(yaw)), 0]
    right_offset = [radius*math.sin(yaw), -radius*(1-math.cos(yaw)), 0]
    intermediate_euler = tf.transformations.euler_from_quaternion([
        intermediate_transform.rotation.x,
        intermediate_transform.rotation.y,
        intermediate_transform.rotation.z,
        intermediate_transform.rotation.w])
    resulting_quat = tf.transformations.quaternion_from_euler(
        intermediate_euler[0], intermediate_euler[1],
        intermediate_euler[2] + yaw)

    rot = tf.transformations.quaternion_matrix([
        resulting_quat[0], resulting_quat[1], resulting_quat[2], resulting_quat[3]])
    left_transformedOffset = numpy.dot(rot[0:3, 0:3], left_offset)
    right_transformedOffset = numpy.dot(rot[0:3, 0:3], right_offset)
    quat_final = Quaternion(
        resulting_quat[0], resulting_quat[1], resulting_quat[2], resulting_quat[3])

    left_footstep.location.x += left_transformedOffset[0]
    left_footstep.location.y += left_transformedOffset[1]
    left_footstep.location.z += left_transformedOffset[2]
    left_footstep.orientation = quat_final

    right_footstep.location.x += right_transformedOffset[0]
    right_footstep.location.y += right_transformedOffset[1]
    right_footstep.location.z += right_transformedOffset[2]
    right_footstep.orientation = quat_final

    if yaw > 0:
        return [left_footstep, right_footstep]
    else:
        return [right_footstep, left_footstep]


# Creates footstep offset from the current foot position. The offset is in foot frame.
def createFootStepOffset(stepSide, offset, swingHeight, turn):
    rospy.loginfo('create footstep offset %i, [%f, %f, %f], height %f, turn %f' % (stepSide, offset[0], offset[1], offset[2], swingHeight, turn))
    footstep = createFootStepInPlace(stepSide, swingHeight)
    currentAngle = tf.transformations.euler_from_quaternion([footstep.orientation.x, footstep.orientation.y, footstep.orientation.z, footstep.orientation.w], 'rzyx')[0]
    resultAngle = currentAngle + turn

    # transform the offset to world frame
    quat = footstep.orientation
    rot = tf.transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
    transformedOffset = numpy.dot(rot[0:3, 0:3], offset)

    footstep.location.x += transformedOffset[0]
    footstep.location.y += transformedOffset[1]
    footstep.location.z += transformedOffset[2]

    if turn != 0:
        pista_quaternion = tf.transformations.quaternion_from_euler(resultAngle, 0, 0, 'rzyx')
    
        footstep.orientation.x = pista_quaternion[0]
        footstep.orientation.y = pista_quaternion[1]
        footstep.orientation.z = pista_quaternion[2]
        footstep.orientation.w = pista_quaternion[3]

    return footstep


def getHeadAngle():
    footWorld = tfBuffer.lookup_transform('world', 'head', rospy.Time(),rospy.Duration(1.0))
    q = footWorld.transform.rotation
    return tf.transformations.euler_from_quaternion([q.x, q.y, q.z, q.w], 'rzyx')[0]


def getFootHeight(stepSide):
    if stepSide == LEFT:
        foot_frame = LEFT_FOOT_FRAME_NAME
    else:
        foot_frame = RIGHT_FOOT_FRAME_NAME

    footWorld = tfBuffer.lookup_transform('world', foot_frame, rospy.Time(),rospy.Duration(1.0))
    return footWorld.transform.translation.z


def waitForFootsteps(numberOfSteps):
    global stepCounter
    stepCounter = 0
    while stepCounter < numberOfSteps:
        rate.sleep()

    rospy.loginfo('finished set of steps')


def recievedFootStepStatus(msg):
    global stepCounter
    if msg.status == 1:
        stepCounter += 1
        

def waitForNavigation():
    global navigationFinished
    global killed
    global shutdown
    
    navigationFinished = False
    while navigationFinished == False:
        rate.sleep()
        if killed == True or shutdown == True:
            navigationFinished = True
            return 0


    rospy.loginfo('finished navigation')


def receivedNavigationStatus(msg):
    rospy.loginfo('received navigation status: [%s]' % msg)
    global navigationFinished
    if msg.data.startswith('0'):
        navigationFinished = True
    else:
        navigationFinished = False
        

def startWandering():
    msg = Bool()
    msg.data = True
    wanderingPublisher.publish(msg)


def killWandering():
    global navigationFinished
    
    msg = Bool()
    msg.data = False

    if navigationFinished == False:
        bashCommand="rostopic pub /humanz/wandering std_msgs/Bool 0"
        stop_wandering_process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        time.sleep(5)
        stop_wandering_process.terminate()
    
    rospy.loginfo('requesting wander kill, not waiting for response')


def stopWandering():
    global navigationFinished
    
    msg = Bool()
    msg.data = False
    
    if navigationFinished == False:
        bashCommand="rostopic pub /humanz/wandering std_msgs/Bool 0"
        stop_wandering_process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        rospy.loginfo('requesting wander stop')
        if navigationFinished == False:
            waitForNavigation()
        stop_wandering_process.terminate()

    rospy.loginfo('stepping in place to reset end effectors')
    msgL = FootstepDataListRosMessage()
    msgL.default_transfer_time = 0.80 #on two legs
    msgL.default_swing_time = 0.80 #on one leg
    msgL.execution_mode = 0
    msgL.unique_id = -1
    msgL.footstep_data_list.append(createFootStepOffset(LEFT, [-0.0, 0.0, 0.0], 0.10, 0.0))
    footStepListPublisher.publish(msgL)
    rospy.loginfo('step left...')
    waitForFootsteps(len(msgL.footstep_data_list))
    msgR = FootstepDataListRosMessage()
    msgR.default_transfer_time = 0.80 #on two legs
    msgR.default_swing_time = 0.80 #on one leg
    msgR.execution_mode = 0
    msgR.unique_id = -1
    msgR.footstep_data_list.append(createFootStepOffset(RIGHT, [-0.0, 0.0, 0.0], 0.10, 0.0))
    footStepListPublisher.publish(msgR)
    rospy.loginfo('step right...')
    waitForFootsteps(len(msgR.footstep_data_list))
    
    rossleep(0.5)

    
    rospy.loginfo('wandering stopped')


def receivedBlueWheelPosition(msg):
    global blueWheelPosition
    global pastBlueWheelPositions

#    rospy.loginfo("------- got blue position: [%f], [%f], [%f]" % (blueWheelPosition.x, blueWheelPosition.y, blueWheelPosition.z))
    
    setDetectedPosition(msg, blueWheelPosition, pastBlueWheelPositions)
    
#    rospy.loginfo("[%f]" % pastBlueWheelPositions[2].x) 
#    rospy.loginfo("[%f]" % blueWheelPosition.x)  
    
#    rospy.loginfo("-------- wheel positions: [%f], [%f], [%f], [%f]" % (blueWheelPosition.x, blueWheelPosition.y, redWheelPosition.x, redWheelPosition.y))
    

def receivedRedWheelPosition(msg):
    global redWheelPosition
    global pastRedWheelPositions
    setDetectedPosition(msg, redWheelPosition, pastRedWheelPositions)    
#    rospy.loginfo("------- got red position: [%f], [%f], [%f]" % (redWheelPosition.x, redWheelPosition.y, redWheelPosition.z))
#    rospy.loginfo("-------- wheel positions: [%f], [%f], [%f], [%f]" % (blueWheelPosition.x, blueWheelPosition.y, redWheelPosition.x, redWheelPosition.y))


def receivedCableChokePosition(msg):
    global cableChokePosition
    global pastCableChokePositions

#    rospy.loginfo("------- got blue position: [%f], [%f], [%f]" % (cableChokePosition.x, cableChokePosition.y, cableChokePosition.z))
    
    setDetectedPosition(msg, cableChokePosition, pastCableChokePositions)
    
#    rospy.loginfo("[%f]" % pastCableChokePositions[2].x) 
#    rospy.loginfo("[%f]" % cableChokePosition.x)  
    
#    rospy.loginfo("-------- wheel positions: [%f], [%f], [%f], [%f]" % (cableChokePosition.x, cableChokePosition.y, redButtonPosition.x, redButtonPosition.y))


def receivedBlueStripPosition(msg):
    global blueStripPosition
    global pastBlueStripPositions

#    rospy.loginfo("------- got blue position: [%f], [%f], [%f]" % (cableChokePosition.x, cableChokePosition.y, cableChokePosition.z))
    
    setDetectedPosition(msg, blueStripPosition, pastBlueStripPositions)
#    print blueStripPosition.toString()
    

def receivedSolarOutputPosition(msg):
    global solarOutputPosition
    global pastSolarOutputPositions

#    rospy.loginfo("------- got blue position: [%f], [%f], [%f]" % (cableChokePosition.x, cableChokePosition.y, cableChokePosition.z))
    
    setDetectedPosition(msg, solarOutputPosition, pastSolarOutputPositions)
#    print blueStripPosition.toString()
    

def receivedRedButtonPosition(msg):
    global redButtonPosition
    global pastRedButtonPositions
    setDetectedPosition(msg, redButtonPosition, pastRedButtonPositions)    
#    rospy.loginfo("------- got red position: [%f], [%f], [%f]" % (redButtonPosition.x, redButtonPosition.y, redButtonPosition.z))
#    rospy.loginfo("-------- wheel positions: [%f], [%f], [%f], [%f]" % (cableChokePosition.x, cableChokePosition.y, redButtonPosition.x, redButtonPosition.y))


def setDetectedPosition(msg, pos, posArr):
    msgPosition = Position()
    msgPosition.x = msg.x
    msgPosition.y = msg.y
    msgPosition.z = msg.z
    
#    print(len(posArr))
    
    shift(posArr)
    posArr[len(posArr) - 1] = msgPosition
    updatePositionAvg(pos, posArr)


def waitForTask1():
    global currentTaskStatus
    global killed
    global shutdown
    
    while (currentTaskStatus.task == 1):
        rate.sleep()
        if killed == True or shutdown == True:
            return 0


    rospy.loginfo('finished task 1')


def waitForTask2():
    global currentTaskStatus
    global killed
    global shutdown
    
    while (currentTaskStatus.task == 2):
        rate.sleep()
        if killed == True or shutdown == True:
            return 0

    rospy.loginfo('finished task 2')
    

def waitForTask3():
    global currentTaskStatus
    global killed
    global shutdown
    
    while not (currentTaskStatus.task == 3 and currentTaskStatus.finished == True):
        rate.sleep()
        if killed == True or shutdown == True:
            return 0

    rospy.loginfo('finished task 3')


def receivedTaskStatus(msg):
    global currentTaskStatus
    
    if msg.task != currentTaskStatus.task or msg.current_checkpoint != currentTaskStatus.current_checkpoint or msg.finished != currentTaskStatus.finished:
        rospy.loginfo('Received task status: task %i, checkpoint %i, finished %s' % (msg.task, msg.current_checkpoint, str(msg.finished)))

    currentTaskStatus = msg

   

def shift(arr):
    for i in range(0, len(arr)-1):
        arr[i] = arr[i+1]


def updatePositionAvg(pos, arr):
    sumX = 0.0
    sumY = 0.0
    sumZ = 0.0

#    rospy.loginfo("from avg [%f]" % arr[2].x) 
    
    for i in range(0, len(arr)):
#        rospy.loginfo("[%i] [%f]" % (i, arr[i].x)) 

        sumX += arr[i].x
        sumY += arr[i].y
        sumZ += arr[i].z

#    rospy.loginfo("from avg sumX [%f]" % sumX) 

    pos.x = sumX / len(arr)
    pos.y = sumY / len(arr)
    pos.z = sumZ / len(arr)
        

def receivedKillSwitch(msg):
    global killed
    
    #TODO: stop all trajectories here?
    
    killWandering()
    
    killed = True
    

def receivedShutdown(msg):
    global shutdown
    global killed
    
    killWandering()
    
    killed = True
    shutdown = True


def receivedGrabSuitCaseAgain(msg):
    global grabSuitCaseAgain   
    grabSuitCaseAgain = True
    
    rospy.loginfo('will grab the suitcase again')


def receivedGrabCableAgain(msg):
    global grabCableAgain   
    grabCableAgain = True
    
    rospy.loginfo('will grab the cable again')
     

def receivedArmMsg(msg):
    msg2 = ArmTrajectoryRosMessage()
    if msg.data.split(',')[0] == 'LEFT':
        msg2.robot_side = ArmTrajectoryRosMessage.LEFT
    else:
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT

    print msg.data.split(',')[0]
    print msg.data.split(',')[1:8]
    
    msg2 = appendTrajectoryPoint(msg2, 1.0, [ float(msg.data.split(',')[1]), float(msg.data.split(',')[2]), float(msg.data.split(',')[3]), float(msg.data.split(',')[4]), float(msg.data.split(',')[5]), float(msg.data.split(',')[6]), float(msg.data.split(',')[7]) ])
    msg2.unique_id = -1
    rospy.loginfo('publishing arm trajectory')
    armTrajectoryPublisher.publish(msg2)


def receivedTurnMsg(msg):
    angle = float(msg.data)
    
    if angle > 0:
        increment = 0.785
    else:
        increment = -0.785
    
    while abs(angle) > abs(increment):
        msgS2 = FootstepDataListRosMessage()
        msgS2.default_transfer_time = 0.80 #on two legs
        msgS2.default_swing_time = 0.80 #on one leg
        msgS2.execution_mode = 0
        msgS2.unique_id = -1
    
        list = createRotationFootStepList(increment)
        for i in range(0, len(list)):
            msgS2.footstep_data_list.append(list[i])
        footStepListPublisher.publish(msgS2)
        rospy.loginfo('waiting for %i turn substeps of %f rad...' % (len(list), increment))
        waitForFootsteps(len(msgS2.footstep_data_list))
        
        angle = angle - increment

    msgS3 = FootstepDataListRosMessage()
    msgS3.default_transfer_time = 0.80 #on two legs
    msgS3.default_swing_time = 0.80 #on one leg
    msgS3.execution_mode = 0
    msgS3.unique_id = -1

    list = createRotationFootStepList(angle)
    for i in range(0, len(list)):
        msgS3.footstep_data_list.append(list[i])
    footStepListPublisher.publish(msgS3)
    rospy.loginfo('waiting for %i turn substeps of %f rad...' % (len(list), angle))
    waitForFootsteps(len(msgS3.footstep_data_list))
    
    rospy.loginfo('turn steps complete')


def receivedstopturningstatus(msg):
    
    global stop_turning 
    
    
    stop_turning = True


def receivedPelvisMsg(msg):
    height = float(msg.data)
    rospy.loginfo('setting pelvis')
    pelvisHeightTrajectoryPublisher.publish(createPelvisMsg(height, 2.0)) 
    rossleep(2.5)
    

def receivedNeckMsg(msg):
    rospy.loginfo('setting neck pose')
    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([float(msg.data.split(',')[0]), float(msg.data.split(',')[1]), float(msg.data.split(',')[2])], 2.0))
    rossleep(2.5)


def receivedChestMsg(msg):
    rospy.loginfo('setting chest pose')
    chestTrajectoryPublisher.publish(createChestMsg(float(msg.data.split(',')[0]), float(msg.data.split(',')[1]), float(msg.data.split(',')[2]), 2.0))
    rossleep(2.5)


def receivedTaskStatusRequestMsg(msg):
    global currentTaskStatus
    taskStatusPublisher.publish('Current task status: task %i, checkpoint %i, finished %s' % (currentTaskStatus.task, currentTaskStatus.current_checkpoint, str(currentTaskStatus.finished)))
    

def receivedPingMsg(msg):
    rospy.loginfo(msg.data)
    
    
def receivedResetPoseMsg(msg):
    rospy.loginfo('reset pelvis')
    pelvisHeightTrajectoryPublisher.publish(createPelvisMsg(1.12, 2.0)) 
    rossleep(1.5)
        
    rospy.loginfo('reset chest')
    chestTrajectoryPublisher.publish(createChestMsg(0.0, 0.0, 0.0, 2.0))
    rossleep(1.5)

    rospy.loginfo('reset neck')
    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([0.29, 0.0, 0.0], 2.0))
    rossleep(1.5)

    rospy.loginfo('reset hands')
    handMsg2 = Float64MultiArray()
    handMsg2.data = [0.0, 0.0, 0.0, 0.0, 0.0]
    leftHandPositionPublisher.publish(handMsg2)
    rossleep(0.5)
    handMsg3 = Float64MultiArray()
    handMsg3.data = [0.0, 0.0, 0.0, 0.0, 0.0]
    rightHandPositionPublisher.publish(handMsg3)
    rossleep(1.0)

    rospy.loginfo('reset arms')
    sendArmTrajectory('ARM_IN_RIGHT')
    rossleep(0.35)
    sendArmTrajectory('ARM_IN_LEFT')
    rossleep(2.0)
    
    rospy.loginfo('reset foot orientation')
    msg2 = String()
    msg2.data = '0.0'
    receivedTurnMsg(msg2)    
    rossleep(0.5)

    rospy.loginfo('line up feet')
    lineUpFeet()

    rospy.loginfo('reset foot gap')
    resetFootGap()

    rospy.loginfo('pose reset finished')


def lineUpFeet():
    foot_world = tfBuffer.lookup_transform(RIGHT_FOOT_FRAME_NAME, LEFT_FOOT_FRAME_NAME, rospy.Time(),rospy.Duration(1.0))

    xoffset = foot_world.transform.translation.x

    rospy.loginfo('lining up feet')
    
    msgS2 = FootstepDataListRosMessage()
    msgS2.default_transfer_time = 0.80 #on two legs
    msgS2.default_swing_time = 0.80 #on one leg
    msgS2.execution_mode = 0
    msgS2.unique_id = -1
    msgS2.footstep_data_list.append(createFootStepOffset(RIGHT, [xoffset, 0.0, 0.0], 0.10, 0.0))
    footStepListPublisher.publish(msgS2)
    waitForFootsteps(len(msgS2.footstep_data_list))

    rospy.loginfo('finished lining up feet')


def resetFootGap():
    left_foot_world = tfBuffer.lookup_transform('world', LEFT_FOOT_FRAME_NAME, rospy.Time(),rospy.Duration(1.0))
    right_foot_world = tfBuffer.lookup_transform('world', RIGHT_FOOT_FRAME_NAME, rospy.Time(),rospy.Duration(1.0))
    intermediate_transform = Transform()
    distance_between_feet = math.sqrt(
    (
        right_foot_world.transform.translation.x - left_foot_world.transform.translation.x
    )**2 + (
        right_foot_world.transform.translation.y - left_foot_world.transform.translation.y
    )**2)

    yoffset = (0.35 - distance_between_feet) / 2

    rospy.loginfo('moving feet to 0.25 distance from each other')
    
    msgS2 = FootstepDataListRosMessage()
    msgS2.default_transfer_time = 0.80 #on two legs
    msgS2.default_swing_time = 0.80 #on one leg
    msgS2.execution_mode = 0
    msgS2.unique_id = -1
    msgS2.footstep_data_list.append(createFootStepOffset(LEFT, [0.0, yoffset, 0.0], 0.04, 0.0))
    msgS2.footstep_data_list.append(createFootStepOffset(RIGHT, [0.0, -1 * yoffset, 0.0], 0.04, 0.0))
    footStepListPublisher.publish(msgS2)
    waitForFootsteps(len(msgS2.footstep_data_list))

    rospy.loginfo('finished moving feet to 0.25 distance from each other')


def receivedStepForward(msg):
    distance = float(msg.data)
    
    msgS = FootstepDataListRosMessage()
    msgS.default_transfer_time = 0.80 #on two legs
    msgS.default_swing_time = 0.80 #on one leg
    msgS.execution_mode = 0
    msgS.unique_id = -1
    msgS.footstep_data_list.append(createFootStepOffset(LEFT, [distance, 0.0, 0.0], 0.10, 0.0))
    msgS.footstep_data_list.append(createFootStepOffset(RIGHT, [distance, 0.0, 0.0], 0.10, 0.0))
    footStepListPublisher.publish(msgS)
    rospy.loginfo('step forward...')
    waitForFootsteps(len(msgS.footstep_data_list))
    
    
def receivedWalkForward(msg):
    global killed
    global shutdown
    
    totalDistance = float(msg.data)
    
    while totalDistance > 0:
        distance = min(totalDistance, 0.35)
        totalDistance = totalDistance - 0.35
        
        msgS = FootstepDataListRosMessage()
        msgS.default_transfer_time = 0.80 #on two legs
        msgS.default_swing_time = 0.80 #on one leg
        msgS.execution_mode = 0
        msgS.unique_id = -1
        msgS.footstep_data_list.append(createFootStepOffset(LEFT, [distance, 0.0, 0.0], 0.10, 0.0))
        msgS.footstep_data_list.append(createFootStepOffset(RIGHT, [distance, 0.0, 0.0], 0.10, 0.0))
        footStepListPublisher.publish(msgS)
        rospy.loginfo('step forward...')
        waitForFootsteps(len(msgS.footstep_data_list))
    
        if killed == True or shutdown == True:
            return 0
        
    rospy.loginfo('done walking forward')
    

def receivedCheckpoint(msg):
    global killed
    killed = False
    
    if msg.data == '1,1':
        rospy.loginfo('Starting checkpoint 1/1: Move within 1 meter from the communication dish (navigate there)')
        checkpointT1C1()
    elif msg.data == '1,1b':
        rospy.loginfo('Starting checkpoint 1/1b: Move within 1 meter from the communication dish (fine tune position)')
        checkpointT1C1b()
    elif msg.data == '1,2':
        rospy.loginfo('Starting checkpoint 1/2: Move the communication dish (assume position)')
        checkpointT1C2()
    elif msg.data == '1,2b':
        rospy.loginfo('Starting checkpoint 1/2b: Move the communication dish (grab blue wheel)')
        checkpointT1C2b()
    elif msg.data == '1,2c':
        rospy.loginfo('Starting checkpoint 1/2c: Move the communication dish (turn blue wheel)')
        checkpointT1C2c()
    elif msg.data == '1,2d':
        rospy.loginfo('Starting checkpoint 1/2d: Move the communication dish (let go of blue wheel)')
        checkpointT1C2d()
    elif msg.data == '1,4':
        rospy.loginfo('Starting checkpoint 1/4: Walk into Task 1''s finish box')
        checkpointT1C4()
        
        
    elif msg.data == '2,1':
        rospy.loginfo('Starting checkpoint 2/1: Pick up the solar panel (just go to the back of the truck)')
        checkpointT2C1()
    elif msg.data == '2,1b':
        rospy.loginfo('Starting checkpoint 2/1b: Pick up the solar panel (position yourself when you are already at the back of the truck)')
        checkpointT2C1b()
    elif msg.data == '2,1c':
        rospy.loginfo('Starting checkpoint 2/1c: Pick up the solar panel (try grabbing when you are already in position)')
        checkpointT2C1c()
    elif msg.data == '2,1d':
        rospy.loginfo('Starting checkpoint 2/1d: Pick up the solar panel (lift suitcase)')
        checkpointT2C1d()
    elif msg.data == '2,1e':
        rospy.loginfo('Starting checkpoint 2/1e: Pick up the solar panel (step back)')
        checkpointT2C1e()
    elif msg.data == '2,2':
        rospy.loginfo('Starting checkpoint 2/2: Place solar panel within reach of the power cable (navigate there)')
        checkpointT2C2()
    elif msg.data == '2,2b':
        rospy.loginfo('Starting checkpoint 2/2b: Place solar panel within reach of the power cable (drop off the suitcase)')
        checkpointT2C2b()
    elif msg.data == '2,3':
        rospy.loginfo('Starting checkpoint 2/3: Deploy solar panel by pressing a button')
        checkpointT2C3()
    elif msg.data == '2,4':
        rospy.loginfo('Starting checkpoint 2/4: Pick up the power cable (position he robot)')
        checkpointT2C4()
    elif msg.data == '2,4b':
        rospy.loginfo('Starting checkpoint 2/4b: Pick up the power cable (place the hand on the cable)')
        checkpointT2C4b()
    elif msg.data == '2,4c':
        rospy.loginfo('Starting checkpoint 2/4c: Pick up the power cable (lift the cable)')
        checkpointT2C4c()
    elif msg.data == '2,5':
        rospy.loginfo('Starting checkpoint 2/5: Plug the power cable into the solar panel (no randomization)')
        checkpointT2C5()
    elif msg.data == '2,5rand':
        rospy.loginfo('Starting checkpoint 2/5rand: Plug the power cable into the solar panel (slight randomization of plug position)')
        checkpointT2C5rand()
    elif msg.data == '2,6':
        rospy.loginfo('Starting checkpoint 2/6: Walk into Task 2''s finish box')
        checkpointT2C6()
    
    
    elif msg.data == '3,1':
        rospy.loginfo('Starting checkpoint 3/1: Climb the stairs (go to stairs)')
        checkpointT3C1()
    elif msg.data == '3,1b':
        rospy.loginfo('Starting checkpoint 3/1b: Climb the stairs (baby steps)')
        checkpointT3C1b()
    elif msg.data == '3,1c':
        rospy.loginfo('Starting checkpoint 3/1c: Climb the stairs (climb up when you''re already at the base)')
        checkpointT3C1c()
    elif msg.data == '3,2':
        rospy.loginfo('Starting checkpoint 3/2: Open the door (go to the door)')
        checkpointT3C2()
    elif msg.data == '3,2b':
        rospy.loginfo('Starting checkpoint 3/2b: Open the door (turn wheel until open)')
        checkpointT3C2b()
    elif msg.data == '3,2bOnce':
        rospy.loginfo('Starting checkpoint 3/2bOnce: Open the door (turn wheel just once)')
        checkpointT3C2bOnce()
    elif msg.data == '3,2c':
        rospy.loginfo('Starting checkpoint 3/2c: Open the door / pass through the door')
        checkpointT3C2c()
    elif msg.data == '3,3':
        rospy.loginfo('No such checkpoint as 3/3!!! 3/2c will pass through the door!!')
    elif msg.data == '3,8':
        rospy.loginfo('Starting checkpoint 3/8: Walk into Task 3''s finish box')
    else: #unknown
        rospy.loginfo('Unknown checkpoint: %s' % msg.data)
    

def sendArmTrajectory(mode):   
    if mode == 'PUSH':
        msg = ArmTrajectoryRosMessage()
        msg.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg = appendTrajectoryPoint(msg, 0.12, ELBOW_BENT_UP_MIDDLE)
        msg = appendTrajectoryPoint(msg, 0.20, ELBOW_BENT_UP)
        msg.unique_id = -1
        rospy.loginfo('publishing right PUSH trajectory')
        armTrajectoryPublisher.publish(msg)
        
        rossleep(0.35)

    elif mode == 'PULL':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 0.5, ARM_PULLED_BACK_INIT2)
        msg2 = appendTrajectoryPoint(msg2, 1.0, ARM_DOWN_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right PULL trajectory')
        armTrajectoryPublisher.publish(msg2)
        
        rossleep(0.2)
        
        msg3 = ArmTrajectoryRosMessage()
        msg3.robot_side = ArmTrajectoryRosMessage.LEFT
        msg3 = appendTrajectoryPoint(msg3, 0.6, ARM_DOWN_LEFT)
        msg3.unique_id = -2
        rospy.loginfo('publishing left PULL trajectory')
        armTrajectoryPublisher.publish(msg3)
        
        rossleep(0.35)

    elif mode == 'PULL_BACK':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.LEFT
        msg2 = appendTrajectoryPoint(msg2, 1.0, ARM_IN_LEFT)
        msg2.unique_id = -1
        rospy.loginfo('publishing left ARM_IN_LEFT trajectory')
        armTrajectoryPublisher.publish(msg2)
        
        rossleep(0.2)
        
        msg3 = ArmTrajectoryRosMessage()
        msg3.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg3 = appendTrajectoryPoint(msg3, 1.0, ARM_IN_RIGHT)
        msg3.unique_id = -2
        rospy.loginfo('publishing left ARM_IN_RIGHT trajectory')
        armTrajectoryPublisher.publish(msg3)
        
        rossleep(0.35)
        
    elif mode == 'SPREAD_WIDE':
        msg1 = ArmTrajectoryRosMessage()
        msg1.robot_side = ArmTrajectoryRosMessage.LEFT
        msg1 = appendTrajectoryPoint(msg1, 0.60, ARM_SPREAD_LEFT_WIDE)
        msg1.unique_id = -1
        rospy.loginfo('publishing left SPREAD WIDE trajectory')
        armTrajectoryPublisher.publish(msg1)

        rossleep(0.35)

        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 0.60, ARM_SPREAD_RIGHT_WIDE)
        msg2.unique_id = -1
        rospy.loginfo('publishing right SPREAD WIDE trajectory')
        armTrajectoryPublisher.publish(msg2)
        
        rossleep(0.35)
    
    elif mode == 'WHEEL_START':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.LEFT
        msg2 = appendTrajectoryPoint(msg2, 2.0, WHEEL_START_LEFT)
        msg2.unique_id = -1
        rospy.loginfo('publishing left WHEEL_START_LEFT trajectory')
        armTrajectoryPublisher.publish(msg2)
        
        rossleep(0.2)
        
        msg3 = ArmTrajectoryRosMessage()
        msg3.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg3 = appendTrajectoryPoint(msg3, 2.0, WHEEL_START_RIGHT)
        msg3.unique_id = -2
        rospy.loginfo('publishing left WHEEL_START_RIGHT trajectory')
        armTrajectoryPublisher.publish(msg3)
        
        rossleep(0.35)
        
    elif mode == 'WHEEL_CLOSE':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.LEFT
        msg2 = appendTrajectoryPoint(msg2, 2.0, WHEEL_CLOSE_LEFT)
        msg2.unique_id = -1
        rospy.loginfo('publishing left WHEEL_CLOSE_LEFT trajectory')
        armTrajectoryPublisher.publish(msg2)
        
        rossleep(0.2)
        
        msg3 = ArmTrajectoryRosMessage()
        msg3.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg3 = appendTrajectoryPoint(msg3, 2.0, WHEEL_CLOSE_RIGHT)
        msg3.unique_id = -2
        rospy.loginfo('publishing left WHEEL_CLOSE_RIGHT trajectory')
        armTrajectoryPublisher.publish(msg3)
        
        rossleep(0.35)
        
    elif mode == 'WHEEL_TURN':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.LEFT
        msg2 = appendTrajectoryPoint(msg2, 2.0, WHEEL_TURN_LEFT)
        msg2.unique_id = -1
        rospy.loginfo('publishing left WHEEL_TURN_LEFT trajectory')
        armTrajectoryPublisher.publish(msg2)
        
        rossleep(0.2)
        
        msg3 = ArmTrajectoryRosMessage()
        msg3.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg3 = appendTrajectoryPoint(msg3, 2.0, WHEEL_TURN_RIGHT)
        msg3.unique_id = -2
        rospy.loginfo('publishing left WHEEL_TURN_RIGHT trajectory')
        armTrajectoryPublisher.publish(msg3)
        
        rossleep(0.35)
    
    elif mode == 'PREPARE_FOR_DOOR_PUSH':    
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 1.0, DOOR_PUSH_PREPARE_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right PREPARE_FOR_DOOR_PUSH trajectory')
        armTrajectoryPublisher.publish(msg2)

        rossleep(0.35)
        
        msg3 = ArmTrajectoryRosMessage()
        msg3.robot_side = ArmTrajectoryRosMessage.LEFT
        msg3 = appendTrajectoryPoint(msg3, 1.0, DOOR_PUSH_PREPARE_LEFT)
        msg3.unique_id = -2
        rospy.loginfo('publishing left PREPARE_FOR_DOOR_PUSH trajectory')
        armTrajectoryPublisher.publish(msg3)
        rossleep(0.35)
                
    elif mode == 'DOOR_PUSH':    
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 3.0, DOOR_PUSH_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right DOOR_PUSH trajectory')
        armTrajectoryPublisher.publish(msg2)

        rossleep(0.15)
        
        msg3 = ArmTrajectoryRosMessage()
        msg3.robot_side = ArmTrajectoryRosMessage.LEFT
        msg3 = appendTrajectoryPoint(msg3, 3.0, DOOR_PUSH_LEFT)
        msg3.unique_id = -2
        rospy.loginfo('publishing left DOOR_PUSH trajectory')
        armTrajectoryPublisher.publish(msg3)
        rossleep(0.35)
             
    elif mode == 'DOOR_PUSH_SPREAD':    
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 1.5, DOOR_PUSH_RIGHT_SPREAD)
        msg2.unique_id = -1
        rospy.loginfo('publishing right DOOR_PUSH_SPREAD trajectory')
        armTrajectoryPublisher.publish(msg2)

        rossleep(0.15)
        
        msg3 = ArmTrajectoryRosMessage()
        msg3.robot_side = ArmTrajectoryRosMessage.LEFT
        msg3 = appendTrajectoryPoint(msg3, 1.5, DOOR_PUSH_LEFT_SPREAD)
        msg3.unique_id = -2
        rospy.loginfo('publishing left DOOR_PUSH_SPREAD trajectory')
        armTrajectoryPublisher.publish(msg3)
        rossleep(0.35)
        
    elif mode == 'DOWN':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 1.0, ARM_DOWN_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right DOWN trajectory')
        armTrajectoryPublisher.publish(msg2)

        rossleep(0.35)
        
        msg3 = ArmTrajectoryRosMessage()
        msg3.robot_side = ArmTrajectoryRosMessage.LEFT
        msg3 = appendTrajectoryPoint(msg3, 1.0, ARM_DOWN_LEFT)
        msg3.unique_id = -2
        rospy.loginfo('publishing left DOWN trajectory')
        armTrajectoryPublisher.publish(msg3)
        
        rossleep(0.35)

    elif mode == 'RIGHTDOWN':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 0.25, ARM_DOWN_RIGHT_INIT)
        msg2 = appendTrajectoryPoint(msg2, 0.60, ARM_DOWN_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right DOWN trajectory')
        armTrajectoryPublisher.publish(msg2)
        
        rossleep(0.35)

    elif mode == 'LIFT_SUITCASE_LEFT':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.LEFT
        msg2 = appendTrajectoryPoint(msg2, 2.0, LIFT_SUITCASE_LEFT)
        msg2.unique_id = -1
        rospy.loginfo('publishing LIFT_SUITCASE_LEFT trajectory')
        armTrajectoryPublisher.publish(msg2)
        rossleep(0.2)

    elif mode == 'LIFT_SUITCASE_RIGHT':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 2.0, LIFT_SUITCASE_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing LIFT_SUITCASE_RIGHT trajectory')
        armTrajectoryPublisher.publish(msg2)
        rossleep(0.2)

    elif mode == 'HOLD_UP_SUITCASE_LEFT':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.LEFT
        msg2 = appendTrajectoryPoint(msg2, 2.0, HOLD_UP_SUITCASE_LEFT)
        msg2.unique_id = -1
        rospy.loginfo('publishing HOLD_UP_SUITCASE_LEFT trajectory')
        armTrajectoryPublisher.publish(msg2)
        rossleep(0.2)

    elif mode == 'HOLD_UP_SUITCASE_RIGHT':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 2.0, HOLD_UP_SUITCASE_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing HOLD_UP_SUITCASE_RIGHT trajectory')
        armTrajectoryPublisher.publish(msg2)
        rossleep(0.2)

    elif mode == 'LIFT_CABLE_LEFT':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.LEFT
        msg2 = appendTrajectoryPoint(msg2, 2.0, LIFT_CABLE_LEFT)
        msg2.unique_id = -1
        rospy.loginfo('publishing LIFT_CABLE_LEFT trajectory')
        armTrajectoryPublisher.publish(msg2)
        rossleep(0.2)

    elif mode == 'SPREAD':
        msg1 = ArmTrajectoryRosMessage()
        msg1.robot_side = ArmTrajectoryRosMessage.LEFT
        msg1 = appendTrajectoryPoint(msg1, 0.60, ARM_SPREAD_LEFT)
        msg1.unique_id = -1
        rospy.loginfo('publishing left SPREAD trajectory')
        armTrajectoryPublisher.publish(msg1)

        rossleep(0.35)

        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 0.60, ARM_SPREAD_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right SPREAD trajectory')
        armTrajectoryPublisher.publish(msg2)
        
        rossleep(0.35)

    elif mode == 'STRAIGHT_FORWARD_LEFT':
        msg = ArmTrajectoryRosMessage()
        msg.robot_side = ArmTrajectoryRosMessage.LEFT
        msg = appendTrajectoryPoint(msg, 1.0, ARM_STRAIGHT_FW_LEFT)
        msg.unique_id = -1
        rospy.loginfo('publishing arm trajectory')
        armTrajectoryPublisher.publish(msg)

        rossleep(0.4)
        
    elif mode == 'ARM_IN_RIGHT':
        msg = ArmTrajectoryRosMessage()
        msg.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg = appendTrajectoryPoint(msg, 1.0, ARM_IN_RIGHT)
        msg.unique_id = -1
        rospy.loginfo('publishing ARM_IN_RIGHT trajectory')
        armTrajectoryPublisher.publish(msg)

        rossleep(0.4)

    elif mode == 'ARM_IN_LEFT':
        msg = ArmTrajectoryRosMessage()
        msg.robot_side = ArmTrajectoryRosMessage.LEFT
        msg = appendTrajectoryPoint(msg, 1.0, ARM_IN_LEFT)
        msg.unique_id = -1
        rospy.loginfo('publishing ARM_IN_LEFT trajectory')
        armTrajectoryPublisher.publish(msg)

        rossleep(0.4)

    elif mode == 'STRAIGHT_FORWARD_LEFT_CABLE':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.LEFT
        msg2 = appendTrajectoryPoint(msg2, 1.0, ARM_STRAIGHT_FW_LEFT_CABLE)
        msg2.unique_id = -1
        rospy.loginfo('publishing arm trajectory')
        armTrajectoryPublisher.publish(msg2)

        rossleep(0.4)    
        
    elif mode == 'STRAIGHT_FORWARD_RIGHT':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 1.0, ARM_STRAIGHT_FW_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing arm trajectory')
        armTrajectoryPublisher.publish(msg2)
    
        rossleep(0.4)    
        

def appendTrajectoryPoint(arm_trajectory, time, positions):
    if not arm_trajectory.joint_trajectory_messages:
        arm_trajectory.joint_trajectory_messages = [copy.deepcopy(OneDoFJointTrajectoryRosMessage()) for i in range(len(positions))]
    for i, pos in enumerate(positions):
        point = TrajectoryPoint1DRosMessage()
        point.time = time
        point.position = pos
        point.velocity = 0
        arm_trajectory.joint_trajectory_messages[i].trajectory_points.append(point)
    return arm_trajectory


def createNeckTrajectoryMessage(arr, secs):
    neckTrajPoint1 = TrajectoryPoint1DRosMessage()
    neckTrajPoint1.time = secs
    neckTrajPoint1.position = arr[0]
    neckTraj1 = OneDoFJointTrajectoryRosMessage()
    neckTraj1.trajectory_points.append(neckTrajPoint1)

    neckTrajPoint2 = TrajectoryPoint1DRosMessage()
    neckTrajPoint2.time = secs
    neckTrajPoint2.position = arr[1]
    neckTraj2 = OneDoFJointTrajectoryRosMessage()
    neckTraj2.trajectory_points.append(neckTrajPoint2)

    neckTrajPoint3 = TrajectoryPoint1DRosMessage()
    neckTrajPoint3.time = secs
    neckTrajPoint3.position = arr[2]
    neckTraj3 = OneDoFJointTrajectoryRosMessage()
    neckTraj3.trajectory_points.append(neckTrajPoint3)
    
    neckMsg = NeckTrajectoryRosMessage()
    neckMsg.unique_id = -1
    neckMsg.joint_trajectory_messages.append(neckTraj1)
    neckMsg.joint_trajectory_messages.append(neckTraj2)
    neckMsg.joint_trajectory_messages.append(neckTraj3)

    return neckMsg


def createChestMsg(r, p, y, time):
    target_pose = PoseStamped()
    transform_target_pose = PoseStamped()

    target_pose.header.frame_id = 'pelvis'
    target_pose.header.stamp = rospy.Time()

    dir_quat = tf.transformations.quaternion_from_euler(r,p,y)

    target_pose.pose.orientation.x = dir_quat[0]
    target_pose.pose.orientation.y = dir_quat[1]
    target_pose.pose.orientation.z = dir_quat[2]
    target_pose.pose.orientation.w = dir_quat[3]

    transformed_target_pose = tfBuffer.transform(target_pose, 'world', rospy.Duration(1.0))

    chest_dir = tf.transformations.euler_from_quaternion([transformed_target_pose.pose.orientation.x, transformed_target_pose.pose.orientation.y, transformed_target_pose.pose.orientation.z, transformed_target_pose.pose.orientation.w])

    chestMsg = ChestTrajectoryRosMessage()
    chestMsg.execution_mode = 0
    chestMsg.unique_id = -1
    
    chestTraj = SO3TrajectoryPointRosMessage()
    chestTraj.time = time
    
    chestTraj.orientation = transformed_target_pose.pose.orientation  
    
    chestMsg.taskspace_trajectory_points.append(chestTraj)
    
    return chestMsg

    
def createPelvisMsg(height, secs):
    pelvisMsg = PelvisHeightTrajectoryRosMessage()
    pelvisMsg.execution_mode = 0
    pelvisMsg.unique_id = -1
    
    pelvisTraj = TrajectoryPoint1DRosMessage()
    pelvisTraj.time = secs
    pelvisTraj.position = height
    pelvisTraj.velocity = 0.0
    
    pelvisMsg.trajectory_points.append(pelvisTraj)
    
    return pelvisMsg


def rossleep(time):
    try:
        seconds = float(time)
    except (ValueError):
        print("Couldn't convert argument [%s] to float" % sys.argv[1])
    rospy.sleep(rospy.Duration.from_sec(seconds))
    

def shutDownHookWrapper(signal, frame):
    shutDownHook()
    

def shutDownHook():
    global footStepStatusSubscriber
    global blueWheelPositionSubscrber
    global blueStripPositionSubscrber
    global solarOutputPositionSubscrber
    global redWheelPositionSubscrber
    global navigationStatusSubscriber
    global taskStatusSubscriber
    global satelliteSubscriber
    global killSwitchSubscriber
    global checkPointSubscriber
    global shutdownSubscriber
    global grabSuitcaseAgainSubscriber
    global grabCableAgainSubscriber
    global stepForwardSubscriber
    global walkForwardSubscriber
    global armSubscriber
    global turnSubscriber
    global pelvisSubscriber
    global neckSubscriber
    global chestSubscriber
    global taskStatusRequestSubscriber
    global resetPoseSubscriber
    global pingSubscriber
    
    global taskStatusPublisher
    global footStepListPublisher
    global footTrajectoryPublisher
    global pelvisHeightTrajectoryPublisher
    global chestTrajectoryPublisher
    global wholeBodyTrajectoryPublisher
    global armTrajectoryPublisher
    global neckTrajectoryPublisher
    global loadPublisher
    global pathPublisher
    global navigationPublisher
    global wanderingPublisher
    global goToTruckPublisher
    global goToPanelPublisher
    global goToStairsPublisher
    global alignToWheelPublisher
    global leftArmPublisher
    global rightArmPublisher
    global leftHandPositionPublisher
    global rightHandPositionPublisher

    global blueStripDetectorProcess
    global blueStripReconstProcess
    global blueDetectorProcess
    global redDetectorProcess
    global blueReconstProcess
    global redReconstProcess
    global moveitProcess
    global leftArmListenerProcess
    global rightArmListenerProcess
    global solarOutputDetectorProcess
    global solarOutputReconstProcess
    
    global blueWheelDetectorProcess
    global redWheelDetectorProcess
    global blueWheelReconstProcess
    global redWheelReconstProcess
    

    rospy.loginfo("shutdown hook called...")
    if blueDetectorProcess is not None:
        rospy.loginfo("killing blue detector...")
        os.killpg(os.getpgid(blueDetectorProcess.pid), signal.SIGKILL)
    if blueWheelDetectorProcess is not None:
        rospy.loginfo("killing blueWheelDetectorProcess...")
        os.killpg(os.getpgid(blueWheelDetectorProcess.pid), signal.SIGKILL)
    if blueStripDetectorProcess is not None:
        rospy.loginfo("killing blue strip detector...")
        os.killpg(os.getpgid(blueStripDetectorProcess.pid), signal.SIGKILL)
    if redDetectorProcess is not None:
        rospy.loginfo("killing red detector...")
        os.killpg(os.getpgid(redDetectorProcess.pid), signal.SIGKILL)
    if redWheelDetectorProcess is not None:
        rospy.loginfo("killing redWheelDetectorProcess...")
        os.killpg(os.getpgid(redWheelDetectorProcess.pid), signal.SIGKILL)
    if blueReconstProcess is not None:
        rospy.loginfo("killing blue reconst...")
        os.killpg(os.getpgid(blueReconstProcess.pid), signal.SIGKILL)
    if blueWheelReconstProcess is not None:
        rospy.loginfo("killing blueWheelReconstProcess...")
        os.killpg(os.getpgid(blueWheelReconstProcess.pid), signal.SIGKILL)
    if blueStripReconstProcess is not None:
        rospy.loginfo("killing blue strip reconst...")
        os.killpg(os.getpgid(blueStripReconstProcess.pid), signal.SIGKILL)
    if redReconstProcess is not None:
        rospy.loginfo("killing red reconst...")
        os.killpg(os.getpgid(redReconstProcess.pid), signal.SIGKILL)
    if redWheelReconstProcess is not None:
        rospy.loginfo("killing redWheelReconstProcess...")
        os.killpg(os.getpgid(redWheelReconstProcess.pid), signal.SIGKILL)
    if leftArmListenerProcess is not None:
        rospy.loginfo("killing left arm listener...")
        os.killpg(os.getpgid(leftArmListenerProcess.pid), signal.SIGKILL)
    if rightArmListenerProcess is not None:
        rospy.loginfo("killing right arm listener...")
        os.killpg(os.getpgid(rightArmListenerProcess.pid), signal.SIGKILL)
    if solarOutputDetectorProcess is not None:
        rospy.loginfo("killing solarOutputDetectorProcess...")
        os.killpg(os.getpgid(solarOutputDetectorProcess.pid), signal.SIGKILL)
    if solarOutputReconstProcess is not None:
        rospy.loginfo("killing solarOutputReconstProcess...")
        os.killpg(os.getpgid(solarOutputReconstProcess.pid), signal.SIGKILL)


    footStepStatusSubscriber.unregister()
    blueWheelPositionSubscrber.unregister()
    blueStripPositionSubscrber.unregister()
    solarOutputPositionSubscrber.unregister()
    redWheelPositionSubscrber.unregister()
    navigationStatusSubscriber.unregister()
    taskStatusSubscriber.unregister()
    satelliteSubscriber.unregister()
    killSwitchSubscriber.unregister()
    checkPointSubscriber.unregister()
    shutdownSubscriber.unregister()
    grabSuitcaseAgainSubscriber.unregister()
    grabCableAgainSubscriber.unregister()
    stepForwardSubscriber.unregister()
    walkForwardSubscriber.unregister()
    armSubscriber.unregister()
    turnSubscriber.unregister()
    pelvisSubscriber.unregister()
    neckSubscriber.unregister()
    chestSubscriber.unregister()
    taskStatusRequestSubscriber.unregister()
    resetPoseSubscriber.unregister()
    pingSubscriber.unregister()
    
    taskStatusPublisher.unregister()
    footStepListPublisher.unregister()
    footTrajectoryPublisher.unregister()
    pelvisHeightTrajectoryPublisher.unregister()
    chestTrajectoryPublisher.unregister()
    wholeBodyTrajectoryPublisher.unregister()
    armTrajectoryPublisher.unregister()
    neckTrajectoryPublisher.unregister()
    loadPublisher.unregister()
    pathPublisher.unregister()
    navigationPublisher.unregister()
    wanderingPublisher.unregister()
    goToTruckPublisher.unregister()
    goToPanelPublisher.unregister()
    goToStairsPublisher.unregister()
    alignToWheelPublisher.unregister()
    leftArmPublisher.unregister()
    rightArmPublisher.unregister()
    leftHandPositionPublisher.unregister()
    rightHandPositionPublisher.unregister()


    
    sys.exit(0)


if __name__ == '__main__':
    global footStepStatusSubscriber
    global blueWheelPositionSubscrber
    global blueWheelPositionSubscrber2
    global blueStripPositionSubscrber
    global solarOutputPositionSubscrber
    global redWheelPositionSubscrber
    global redWheelPositionSubscrber2
    global navigationStatusSubscriber
    global taskStatusSubscriber
    global satelliteSubscriber
    global killSwitchSubscriber
    global checkPointSubscriber
    global shutdownSubscriber
    global grabSuitcaseAgainSubscriber
    global grabCableAgainSubscriber
    global stepForwardSubscriber
    global walkForwardSubscriber
    global armSubscriber
    global turnSubscriber
    global pelvisSubscriber
    global neckSubscriber
    global chestSubscriber
    global taskStatusRequestSubscriber
    global resetPoseSubscriber
    global pingSubscriber

    global taskStatusPublisher
    global footStepListPublisher
    global footTrajectoryPublisher
    global pelvisHeightTrajectoryPublisher
    global chestTrajectoryPublisher
    global wholeBodyTrajectoryPublisher
    global armTrajectoryPublisher
    global neckTrajectoryPublisher
    global loadPublisher
    global pathPublisher
    global navigationPublisher
    global wanderingPublisher
    global goToTruckPublisher
    global goToPanelPublisher
    global goToStairsPublisher
    global alignToWheelPublisher
    global leftArmPublisher
    global rightArmPublisher
    global leftHandPositionPublisher
    global rightHandPositionPublisher
    
    global cableChokePosition
    global redButtonPosition
    global blueStripPosition
    global solarOutputPosition
    global navigationFinished
    global stepCounter
    global targetPitch
    global targetYaw
    global curentPitch
    global currentYaw
    global shutdown
    global killed
    global currentTaskStatus
    

    shutdown = False
    killed = False

    currentTaskStatus = Task()
    cableChokePosition = Position()
    redButtonPosition = Position()
    blueStripPosition = Position()
    solarOutputPosition = Position()
    redWheelPosition = Position()
    blueWheelPosition = Position()
    
    navigationFinished = False
    stepCounter = 0
    targetPitch = 0.0
    targetYaw = 0.0
    curentPitch = 0.0
    currentYaw = 0.0
    
    signal.signal(signal.SIGINT, shutDownHookWrapper)
    
    try:
        rospy.init_node('ihmc_walk_test')

        if not rospy.has_param('/ihmc_ros/robot_name'):
            rospy.logerr("Cannot run walk_test.py, missing parameters!")
            rospy.logerr("Missing parameter '/ihmc_ros/robot_name'")

        else:
            ROBOT_NAME = rospy.get_param('/ihmc_ros/robot_name')

            right_foot_frame_parameter_name = "/ihmc_ros/{0}/right_foot_frame_name".format(ROBOT_NAME)
            left_foot_frame_parameter_name = "/ihmc_ros/{0}/left_foot_frame_name".format(ROBOT_NAME)

            if rospy.has_param(right_foot_frame_parameter_name) and rospy.has_param(left_foot_frame_parameter_name):
                RIGHT_FOOT_FRAME_NAME = rospy.get_param(right_foot_frame_parameter_name)
                LEFT_FOOT_FRAME_NAME = rospy.get_param(left_foot_frame_parameter_name)

                footStepStatusSubscriber = rospy.Subscriber("/ihmc_ros/{0}/output/footstep_status".format(ROBOT_NAME), FootstepStatusRosMessage, recievedFootStepStatus)
                blueWheelPositionSubscrber = rospy.Subscriber("/srcsim/vision/blue", Console, receivedCableChokePosition)
                blueWheelPositionSubscrber2 = rospy.Subscriber("/srcsim/vision/blue_wheel", Console, receivedBlueWheelPosition)
                blueStripPositionSubscrber = rospy.Subscriber("/srcsim/vision/blue_strip", Console, receivedBlueStripPosition)
                solarOutputPositionSubscrber = rospy.Subscriber("/srcsim/vision/solar_output", Console, receivedSolarOutputPosition)
                redWheelPositionSubscrber = rospy.Subscriber("/srcsim/vision/red", Console, receivedRedButtonPosition)
                redWheelPositionSubscrber2 = rospy.Subscriber("/srcsim/vision/red_wheel", Console, receivedRedWheelPosition)
                navigationStatusSubscriber = rospy.Subscriber("/humanz/status", String, receivedNavigationStatus)
                taskStatusSubscriber = rospy.Subscriber("/srcsim/finals/task", Task, receivedTaskStatus)
                satelliteSubscriber = rospy.Subscriber("/task1/checkpoint2/satellite", Satellite, receivedSatelliteStatus)
                killSwitchSubscriber = rospy.Subscriber("/humanz/killswitch", String, receivedKillSwitch)
                checkPointSubscriber = rospy.Subscriber("/humanz/checkpoint", String, receivedCheckpoint)
                shutdownSubscriber = rospy.Subscriber("/humanz/shutdown", String, receivedShutdown)
                grabSuitcaseAgainSubscriber = rospy.Subscriber("/humanz/grabsuitcaseagain", String, receivedGrabSuitCaseAgain)
                grabCableAgainSubscriber = rospy.Subscriber("/humanz/grabcableagain", String, receivedGrabCableAgain)
                stepForwardSubscriber = rospy.Subscriber("/humanz/forward", String, receivedStepForward)
                walkForwardSubscriber = rospy.Subscriber("/humanz/walkforward", String, receivedWalkForward)
                armSubscriber = rospy.Subscriber("/srcsim/balazs/arm", String, receivedArmMsg)
                turnSubscriber = rospy.Subscriber("/srcsim/balazs/turn", String, receivedTurnMsg)
                pelvisSubscriber = rospy.Subscriber("/srcsim/balazs/pelvis", String, receivedPelvisMsg)
                neckSubscriber = rospy.Subscriber("/srcsim/balazs/neck", String, receivedNeckMsg)
                chestSubscriber = rospy.Subscriber("/srcsim/balazs/chest", String, receivedChestMsg)
                taskStatusRequestSubscriber = rospy.Subscriber("/srcsim/balazs/taskstatusrequest", String, receivedTaskStatusRequestMsg)
                resetPoseSubscriber = rospy.Subscriber("/srcsim/balazs/resetpose", String, receivedResetPoseMsg)
                pingSubscriber = rospy.Subscriber("/srcsim/balazs/ping", String, receivedPingMsg)
                stopturningSubscriber = rospy.Subscriber("/humanz/stopturning", String, receivedstopturningstatus)

                taskStatusPublisher = rospy.Publisher("/srcsim/balazs/controllertaskstatus".format(ROBOT_NAME), String, queue_size=100)
                footStepListPublisher = rospy.Publisher("/ihmc_ros/{0}/control/footstep_list".format(ROBOT_NAME), FootstepDataListRosMessage, queue_size=100)
                footTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/foot_trajectory".format(ROBOT_NAME), FootTrajectoryRosMessage, queue_size=100)
                pelvisHeightTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/pelvis_height_trajectory".format(ROBOT_NAME), PelvisHeightTrajectoryRosMessage, queue_size=100)
                chestTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/chest_trajectory".format(ROBOT_NAME), ChestTrajectoryRosMessage, queue_size=20)
                wholeBodyTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/whole_body_trajectory".format(ROBOT_NAME), WholeBodyTrajectoryRosMessage, queue_size=100)
                armTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/arm_trajectory".format(ROBOT_NAME), ArmTrajectoryRosMessage, queue_size=100)
                neckTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/neck_trajectory".format(ROBOT_NAME), NeckTrajectoryRosMessage, queue_size=100)
                loadPublisher = rospy.Publisher("/ihmc_ros/{0}/control/end_effector_load_bearing".format(ROBOT_NAME), EndEffectorLoadBearingRosMessage, queue_size=50)
                pathPublisher = rospy.Publisher("/humanz/walk_path", String, queue_size=100)
                navigationPublisher = rospy.Publisher("/humanz/navigate", String, queue_size=100)
                wanderingPublisher = rospy.Publisher("/humanz/wandering", Bool, queue_size=100)
                goToTruckPublisher = rospy.Publisher("/humanz/task2/go_to_cart", String, queue_size=100)
                goToPanelPublisher = rospy.Publisher("/humanz/task2/go_to_panel", String, queue_size=100)
                goToStairsPublisher = rospy.Publisher("/humanz/task3/go_to_stairs", String, queue_size=100)
                alignToWheelPublisher = rospy.Publisher("/humanz/task3/align_to_wheel", String, queue_size=100)
                leftArmPublisher = rospy.Publisher("left_arm_moveit_position", String, queue_size=100)
                rightArmPublisher = rospy.Publisher("right_arm_moveit_position", String, queue_size=100)
                leftHandPositionPublisher = rospy.Publisher("/left_hand_position_controller/command", Float64MultiArray, queue_size=100)
                rightHandPositionPublisher = rospy.Publisher("/right_hand_position_controller/command", Float64MultiArray, queue_size=100)

                tfBuffer = tf2_ros.Buffer()
                tfListener = tf2_ros.TransformListener(tfBuffer)

                rate = rospy.Rate(10) # 10hz
                time.sleep(1)

                if not rospy.is_shutdown():
                    walkTest()
            else:
                if not rospy.has_param(left_foot_frame_parameter_name):
                    rospy.logerr("Missing parameter {0}".format(left_foot_frame_parameter_name))
                if not rospy.has_param(right_foot_frame_parameter_name):
                    rospy.logerr("Missing parameter {0}".format(right_foot_frame_parameter_name))

    except rospy.ROSInterruptException:
        pass
