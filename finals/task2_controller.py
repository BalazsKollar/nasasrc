#!/usr/bin/env python

import time
import rospy
import tf
import tf2_ros
import numpy
import copy
import sys
import subprocess
import math
import atexit
import os
import signal
import pprint

from tf2_geometry_msgs import PointStamped

from ihmc_msgs.msg import FootstepStatusRosMessage
from ihmc_msgs.msg import FootstepDataListRosMessage
from ihmc_msgs.msg import FootstepDataRosMessage
from ihmc_msgs.msg import FootTrajectoryRosMessage
from ihmc_msgs.msg import PelvisHeightTrajectoryRosMessage
from ihmc_msgs.msg import TrajectoryPoint1DRosMessage
from ihmc_msgs.msg import WholeBodyTrajectoryRosMessage
from ihmc_msgs.msg import ArmTrajectoryRosMessage
from ihmc_msgs.msg import OneDoFJointTrajectoryRosMessage
from ihmc_msgs.msg import NeckTrajectoryRosMessage
from ihmc_msgs.msg import HeadTrajectoryRosMessage
from ihmc_msgs.msg import SO3TrajectoryPointRosMessage
from std_msgs.msg import String
from std_msgs.msg import Bool
from srcsim.msg._Console import Console
from srcsim.msg._Task import Task

from tf.transformations import quaternion_from_euler
from geometry_msgs.msg import Quaternion, Vector3
from navigation import walker

LEFT = 0
RIGHT = 1

ROBOT_NAME = None
LEFT_FOOT_FRAME_NAME = None
RIGHT_FOOT_FRAME_NAME = None

class Position:
    x = 0.0
    y = 0.0
    z = 0.0
    
class Waypoint2D:
    x = 0.0
    y = 0.0
    angle = 0.0
    
    def toString(self):
        return ("[x:[%s], y:[%s], a:[%s]]" % (self.x, self.y, self.angle))


global neckTrajectoryPublisher, headTrajectoryPublisher

global pastblueStripPositions, pastredButtonPositions
global blueStripPosition, redButtonPosition
global blueDetectorProcess, redDetectorProcess
global blueReconstProcess, redReconstProcess


def walkTest():
    atexit.register(shutDownHook)
    look_down()

    global pastblueStripPositions
    global pastredButtonPositions
    pastblueStripPositions = [Position(), Position(), Position(), Position(), Position()]
    pastredButtonPositions = [Position(), Position(), Position(), Position(), Position()]
    
    #Initialize nodes
    global blueDetectorProcess
    global redDetectorProcess
    global blueReconstProcess
    global redReconstProcess
    rospy.loginfo('setting up 3d vision nodes...')
    FNULL = open(os.devnull, 'w')
    blueDetectorProcess = subprocess.Popen("rosrun vision image_processor /miki/blue_strip blue_strip 1 __name:=blue_detector", shell=True, stdout=FNULL, preexec_fn=os.setsid)
    time.sleep(8)
    redDetectorProcess = subprocess.Popen("rosrun vision image_processor /miki/red_button red_button 1 __name:=red_detector", shell=True, stdout=FNULL, preexec_fn=os.setsid)
    time.sleep(8)
    blueReconstProcess = subprocess.Popen("rosrun vision loc_reconst.py LED_data:=/miki/blue_strip srcsim/vision/light:=/srcsim/vision/blue_strip __name:=blue_reconst", shell=True, stdout=FNULL, preexec_fn=os.setsid)
    time.sleep(8)
    redReconstProcess = subprocess.Popen("rosrun vision loc_reconst.py LED_data:=/miki/red_button srcsim/vision/light:=/srcsim/vision/red_button __name:=red_reconst", shell=True, stdout=FNULL, preexec_fn=os.setsid)    
#    rossleep(3)
#    rospy.loginfo('3d vision nodes started')
#    
#    #Walk to panel
#    worldTargetFar = getHeadTargetInWorld(1.28) #x,y,angle
#    worldTargetClose = getHeadTargetInWorld(1.15) #x,y,angle
#    
#    #worldTargetFar = getWorldPointFromHeadPoint(headTargetFar[0], headTargetFar[1], 0)
#    #worldTargetClose = getWorldPointFromHeadPoint(headTargetClose[0], headTargetClose[1], 0)
#    
#    rospy.loginfo('will walk through %f,%f;%f,%f' % (worldTargetFar[0], worldTargetFar[1], worldTargetClose[0], worldTargetClose[1]))
#    #TODO change this back to navigationPublisher once navigation is not too shy anymore
#    pathPublisher.publish('%f,%f' % (worldTargetFar[0], worldTargetFar[1]))
#    waitForNavigation()
#    rospy.loginfo('navigation finished...')
#    pathPublisher.publish('%f,%f' % (worldTargetClose[0], worldTargetClose[1]))
#    waitForNavigation()
#    rospy.loginfo('path finished...')
#    
#    #Initialize pose
#    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([0.50, 0.0, -0.06], 1.0))
#    
#    rospy.loginfo('setting pelvis height...')
#    pelvisHeightTrajectoryPublisher.publish(createPelvisMsg(1.08, 1.0)) 
#    
#    rossleep(4)
#    
#    msg = FootstepDataListRosMessage()
#    msg.default_transfer_time = 1.05 #on two legs
#    msg.default_swing_time = 1.15 #on one leg
#    msg.execution_mode = 0
#    msg.unique_id = -1
#    
#    headTargetInWorld = getHeadTargetInWorld(0.82) #x,y,angle
#    footSourceInWorld = getFootSourceInWorld() #x,y,angle
#    
#    isLeft = False
#    wayPoints = interpolateWayPoints(footSourceInWorld[0], footSourceInWorld[1], footSourceInWorld[2], headTargetInWorld[0], headTargetInWorld[1], headTargetInWorld[2])
#    footHeight = getFootHeight(LEFT)
#    
#    latestWayoint = None
#    for wayPoint in wayPoints:
#        latestWayoint = wayPoint
#        rospy.loginfo(wayPoint.toString())
#        if isLeft == True:
#            msg.footstep_data_list.append(createFootStepFromWayPoint(LEFT, wayPoint, footHeight, 0.1))
#        else:
#            msg.footstep_data_list.append(createFootStepFromWayPoint(RIGHT, wayPoint, footHeight, 0.1))
#        isLeft = not isLeft
#    #lining up both feet with a last step
#    if isLeft == True:
#        msg.footstep_data_list.append(createFootStepFromWayPoint(LEFT, latestWayoint, footHeight, 0.1))
#    else:
#        msg.footstep_data_list.append(createFootStepFromWayPoint(RIGHT, latestWayoint, footHeight, 0.1))
#
#
#    footStepListPublisher.publish(msg)
#    rospy.loginfo('walk forward...')
#    waitForFootsteps(len(msg.footstep_data_list))
#
#
#    #Move feet closer so that we don't touch any box near the panel
#    msg2 = FootstepDataListRosMessage()
#    msg2.default_transfer_time = 0.80 #on two legs
#    msg2.default_swing_time = 0.80 #on one leg
#    msg2.execution_mode = 0
#    msg2.unique_id = -1
#    msg2.footstep_data_list.append(createFootStepOffset(LEFT, [0.0, -0.06, 0.0], 0.10, 0.0))
#    msg2.footstep_data_list.append(createFootStepOffset(RIGHT, [0.0, 0.05, 0.0], 0.10, 0.0))
#
#    footStepListPublisher.publish(msg2)
#    rospy.loginfo('moving feet closer...')
#    waitForFootsteps(len(msg2.footstep_data_list))
#
#
#    sendArmTrajectory('SPREAD')
#
#    rossleep(0.45)
#    
#    rospy.loginfo('setting pelvis height...')
#    pelvisHeightTrajectoryPublisher.publish(createPelvisMsg(0.87, 1.0)) 
#
#    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([1.08, 0.0, -0.22], 1.0))
#        
#    #wheel turning
#    rossleep(3)
#    
#    #wandering to the finish box
#    pelvisHeightTrajectoryPublisher.publish(createPelvisMsg(1.15, 1.0)) 
#    rossleep(0.2)
#    neckTrajectoryPublisher.publish(createNeckTrajectoryMessage([0.40, 0.0, 0.0], 1.0))
#    rossleep(0.2)
#    sendArmTrajectory('DOWN')
#    rossleep(0.35)
#    
#    #move feet back and apart
#    msg3 = FootstepDataListRosMessage()
#    msg3.default_transfer_time = 0.80 #on two legs
#    msg3.default_swing_time = 0.80 #on one leg
#    msg3.execution_mode = 0
#    msg3.unique_id = -1
#    msg3.footstep_data_list.append(createFootStepOffset(RIGHT, [-0.20, -0.10, 0.0], 0.10, 0.0))
#    msg3.footstep_data_list.append(createFootStepOffset(LEFT, [-0.20, 0.10, 0.0], 0.10, 0.0))
#
#    footStepListPublisher.publish(msg3)
#    rospy.loginfo('moving feet apart...')
#    waitForFootsteps(len(msg3.footstep_data_list))
#    
#    startWandering()
#    
#    #waitForTask1()
    rossleep(200)
#    
#    #stopping wandering
#    stopWandering()
#    
#    rospy.loginfo('finished task 1 controller')

'------------------------------Copy from walker.py----------------------------'    
def look_back():
    joint_values = {
            'head': [0.0, 0.2, 0.0],
            'neck': [0.4, 0.0, 0.0],
        }
    _update_head_neck(joint_values)
    
def look_down():
    joint_values = {
            'head': [0.0, 0.0, 0.0],
            'neck': [0.75, 0.0, 0.0],
        }
    _update_head_neck(joint_values)
    
def _update_head_neck(joint_values):
    msgN = NeckTrajectoryRosMessage()
    msgN.unique_id = -1
    _append_trajectory_point_1d(msgN, 0.5, joint_values['neck'])
    neckTrajectoryPublisher.publish(msgN)
    msgH = HeadTrajectoryRosMessage()
    msgH.unique_id = -1
    _append_trajectory_point_so3(msgH, 0.5, joint_values['head'])
    headTrajectoryPublisher.publish(msgH)
    
def _append_trajectory_point_1d(msg, time, joint_values):
    if not msg.joint_trajectory_messages:
        msg.joint_trajectory_messages = [
            OneDoFJointTrajectoryRosMessage() for _ in joint_values]
    for i, joint_value in enumerate(joint_values):
        point = TrajectoryPoint1DRosMessage()
        point.time = time
        point.position = joint_value
        point.velocity = 0
        msg.joint_trajectory_messages[i].trajectory_points.append(point)
        
def _append_trajectory_point_so3(msg, time, joint_values):
    roll, pitch, yaw = joint_values
    quat = quaternion_from_euler(roll, pitch, yaw)
    point = SO3TrajectoryPointRosMessage()
    point.time = time
    point.orientation = Quaternion()
    point.orientation.x = quat[0]
    point.orientation.y = quat[1]
    point.orientation.z = quat[2]
    point.orientation.w = quat[3]
    point.angular_velocity = Vector3()
    point.angular_velocity.x = 0
    point.angular_velocity.y = 0
    point.angular_velocity.z = 0
    msg.taskspace_trajectory_points.append(point)
    
'------------------------/Copy from walker.py----------------------------------'
    
    
def getWorldPointFromHeadPoint(headX, headY, headZ):
    headPoint = PointStamped()
   
    headPoint.header.frame_id = 'head'   
    headPoint.header.stamp = rospy.Time()
   
    headPoint.point.x = headX
    headPoint.point.y = headY
    headPoint.point.z = headZ
   
    worldPoint = tfBuffer.transform(headPoint, 'world')

    worldPosition = Position()
    worldPosition.x = worldPoint.point.x
    worldPosition.y = worldPoint.point.y
    worldPosition.z = worldPoint.point.z
    
    rospy.loginfo('world target point is x:[%f], y:[%f]' % (worldPosition.x, worldPosition.y))
    
    return worldPosition
   
    
def getWorldPointFromHeadPoint2(headX, headY, headZ):
    #using footstep only as a data structure, TODO refactor later
    footstep = FootstepDataRosMessage()

    footWorld = tfBuffer.lookup_transform('world', 'head', rospy.Time())
    footstep.orientation = footWorld.transform.rotation
    footstep.location = footWorld.transform.translation

    # transform the offset to world frame
    quat = footstep.orientation
    rot = tf.transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
    transformedOffset = numpy.dot(rot[0:3, 0:3], [headX, headY, headZ])

    footstep.location.x += transformedOffset[0]
    footstep.location.y += transformedOffset[1]
    footstep.location.z += transformedOffset[2]

    worldPoint = Position()
    worldPoint.x = footstep.location.x
    worldPoint.y = footstep.location.y
    worldPoint.z = footstep.location.z
    
    rospy.loginfo('world target point is x:[%f], y:[%f]' % (worldPoint.x, worldPoint.y))
    
    return worldPoint


def getHeadTargetInWorld(distanceFromPanel):
    rospy.loginfo('############################################################################################')
    xPos = (BlueStripPosition.x + redButtonPosition.x) / 2
    yPos = (BlueStripPosition.y + redButtonPosition.y) / 2
    #TODO this is fucking random? - it fluctuates because readings are unstable
    alpha = math.atan(abs(BlueStripPosition.x - redButtonPosition.x) / abs(BlueStripPosition.y - redButtonPosition.y))
    yShift = math.sin(alpha) * distanceFromPanel
    xShift = math.cos(alpha) * distanceFromPanel
    pointX = xPos - xShift

    if BlueStripPosition.x > redButtonPosition.x:
        if BlueStripPosition.y > redButtonPosition.y:
            yShift = -1 * yShift
    else:
        if redButtonPosition.y > BlueStripPosition.y:
            yShift = -1 * yShift
            
    pointY = yPos - yShift

    #checking if the alternative point ("behind the panel") is actually closer, in which case that should be our target, even if the angle suggests otherwise
    pointX2 = xPos + xShift
    pointY2 = yPos + yShift
    if math.sqrt(pointX2*pointX2 + pointY2*pointY2) < math.sqrt(pointX*pointX + pointY*pointY):
        pointX = pointX2
        pointY = pointY2
        
    if pointY < yPos:
        alpha = -1 * alpha

    rospy.loginfo('blue xyz [%f] [%f] [%f]' % (BlueStripPosition.x, BlueStripPosition.y, BlueStripPosition.z))
    rospy.loginfo('red xyz [%f] [%f] [%f]' % (redButtonPosition.x, redButtonPosition.y, redButtonPosition.z))
    rospy.loginfo('xpos [%f]' % xPos)
    rospy.loginfo('ypos [%f]' % yPos)
    rospy.loginfo('alpha [%f]' % alpha)
    rospy.loginfo('yShift [%f]' % yShift)
    rospy.loginfo('xShift [%f]' % xShift)
    rospy.loginfo('pointX [%f]' % pointX)
    rospy.loginfo('pointY [%f]' % pointY)
    
    rospy.loginfo('TARGET IN HEAD FRAME COORDINATES IS: x=[%f], y=[%f], z=[%f], angle=[%f]' % (pointX, pointY, BlueStripPosition.z, alpha))

    #convert the head frame point to world frame point
    headWorld = tfBuffer.lookup_transform('world', 'head', rospy.Time())
    headPoint = FootstepDataRosMessage()
    headPoint.orientation = headWorld.transform.rotation
    headPoint.location = headWorld.transform.translation
    quat = headPoint.orientation
    rot = tf.transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
    transformedOffset = numpy.dot(rot[0:3, 0:3], [pointX, pointY, BlueStripPosition.z])

    rospy.loginfo('TARGET IN WORLD FRAME COORDINATES IS: x=[%f], y=[%f], z=[%f], angle=[%f]' % (headPoint.location.x + transformedOffset[0], headPoint.location.y + transformedOffset[1], 0, alpha + getHeadAngle()))
    
    return [headPoint.location.x + transformedOffset[0], headPoint.location.y + transformedOffset[1], alpha + getHeadAngle()]


#get the point halfway between the two feet
def getFootSourceInWorld():
    foot_frame_left = LEFT_FOOT_FRAME_NAME
    foot_frame_right = RIGHT_FOOT_FRAME_NAME

    footWorldLeft = tfBuffer.lookup_transform('world', foot_frame_left, rospy.Time())
    rotL = footWorldLeft.transform.rotation    
    angL = tf.transformations.euler_from_quaternion([rotL.x, rotL.y, rotL.z, rotL.w], 'rzyx')

    footWorldRight = tfBuffer.lookup_transform('world', foot_frame_right, rospy.Time())
    rotR = footWorldRight.transform.rotation    
    angR = tf.transformations.euler_from_quaternion([rotR.x, rotR.y, rotR.z, rotR.w], 'rzyx')

    footX = (footWorldLeft.transform.translation.x + footWorldRight.transform.translation.x) / 2
    footY = (footWorldLeft.transform.translation.y + footWorldRight.transform.translation.y) / 2
    footAng = (angL[0] + angR[0]) / 2

    rospy.loginfo('AVG FOOT LOCATION IN WORLD FRAME COORDINATES IS: x=[%f], y=[%f], angle=[%f]' % (footX, footY, footAng))
    
    return [footX, footY, footAng]


def interpolateWayPoints(fromX, fromY, fromAngle, toX, toY, toAngle):
    rospy.loginfo("INTERPOLATION fromX [%f], fromY [%f], fromAngle [%f], toX [%f], toY [%f], toAngle [%f]" % (fromX, fromY, fromAngle, toX, toY, toAngle))
    
    MAX_STEP_SIZE = 0.4
    distance = math.sqrt(math.pow(toX - fromX, 2) + math.pow(toY - fromY, 2))
    stepCount = int(math.ceil(distance / MAX_STEP_SIZE))
    
    wayPoints = []
    for stepNum in range(1, stepCount + 1):
        if stepNum == 1:#small extra step at the beginning
            firstWayPoint = Waypoint2D()
            firstWayPoint.x = fromX + (toX - fromX) / stepCount * stepNum / 2
            firstWayPoint.y = fromY + (toY - fromY) / stepCount * stepNum / 2
            firstWayPoint.angle = fromAngle + (toAngle - fromAngle) / stepCount * stepNum / 2
            wayPoints.append(firstWayPoint)
        wayPoint = Waypoint2D()
        wayPoint.x = fromX + (toX - fromX) / stepCount * stepNum
        wayPoint.y = fromY + (toY - fromY) / stepCount * stepNum
        wayPoint.angle = fromAngle + (toAngle - fromAngle) / stepCount * stepNum
        wayPoints.append(wayPoint)
    return wayPoints
    
    
def createFootStepFromWayPoint(stepSide, wayPoint, z, swingHeight):
    if stepSide == LEFT:
        correction = 0.16
    else:
        correction = 0.16

    xCorr = math.sin(wayPoint.angle) * correction
    yCorr = math.cos(wayPoint.angle) * correction

    if stepSide == LEFT:
        return createFootStepInWorld(stepSide, [wayPoint.x - xCorr, wayPoint.y + yCorr, z], swingHeight, wayPoint.angle)    
    else:
        return createFootStepInWorld(stepSide, [wayPoint.x + xCorr, wayPoint.y - yCorr, z], swingHeight, wayPoint.angle)    


#offset is already in world coordinates    
def createFootStepInWorld(stepSide, offset, swingHeight, turn):
    footstep = FootstepDataRosMessage()
    footstep.robot_side = stepSide
    footstep.swing_height = swingHeight

    footstep.location.x = offset[0]
    footstep.location.y = offset[1]
    footstep.location.z = offset[2]

    rospy.loginfo("@@@@@@@ WORLD FOOTSTEP LOCATION @@@@@@@@@@ [%s] [%s] [%s] " % (footstep.location.x, footstep.location.y, footstep.location.z))

    pista_quaternion = tf.transformations.quaternion_from_euler(turn, 0, 0, 'rzyx')
    
    footstep.orientation.x = pista_quaternion[0]
    footstep.orientation.y = pista_quaternion[1]
    footstep.orientation.z = pista_quaternion[2]
    footstep.orientation.w = pista_quaternion[3]    
    
    pprint.pprint(turn)
    pprint.pprint(tf.transformations.euler_from_quaternion([pista_quaternion[0], pista_quaternion[1], pista_quaternion[2], pista_quaternion[3]], 'rzyx'))
    
    return footstep

    
# Creates footstep with the current position and orientation of the foot.
def createFootStepInPlace(stepSide, swingHeight):
    footstep = FootstepDataRosMessage()
    footstep.robot_side = stepSide
    footstep.swing_height = swingHeight
    
    if stepSide == LEFT:
        foot_frame = LEFT_FOOT_FRAME_NAME
    else:
        foot_frame = RIGHT_FOOT_FRAME_NAME

    footWorld = tfBuffer.lookup_transform('world', foot_frame, rospy.Time())
    footstep.orientation = footWorld.transform.rotation
    footstep.location = footWorld.transform.translation
    
#    rospy.loginfo("orientation of inplace step:")
#    ang = tf.transformations.euler_from_quaternion([footstep.orientation.x, footstep.orientation.y, footstep.orientation.z, footstep.orientation.w], 'rzyx')
#    pprint.pprint(ang)
    
    return footstep


# Creates footstep offset from the current foot position. The offset is in foot frame.
def createFootStepOffset(stepSide, offset, swingHeight, turn):
    footstep = createFootStepInPlace(stepSide, swingHeight)


    # transform the offset to world frame
    quat = footstep.orientation
    rot = tf.transformations.quaternion_matrix([quat.x, quat.y, quat.z, quat.w])
    transformedOffset = numpy.dot(rot[0:3, 0:3], offset)

    footstep.location.x += transformedOffset[0]
    footstep.location.y += transformedOffset[1]
    footstep.location.z += transformedOffset[2]

    if turn != 0:
        pista_quaternion = tf.transformations.quaternion_from_euler(turn, 0, 0, 'rzyx')
    
        footstep.orientation.x = pista_quaternion[0]
        footstep.orientation.y = pista_quaternion[1]
        footstep.orientation.z = pista_quaternion[2]
        footstep.orientation.w = pista_quaternion[3]

    return footstep


def getHeadAngle():
    footWorld = tfBuffer.lookup_transform('world', 'head', rospy.Time())
    q = footWorld.transform.rotation
    return tf.transformations.euler_from_quaternion([q.x, q.y, q.z, q.w], 'rzyx')[0]


def getFootHeight(stepSide):
    if stepSide == LEFT:
        foot_frame = LEFT_FOOT_FRAME_NAME
    else:
        foot_frame = RIGHT_FOOT_FRAME_NAME

    footWorld = tfBuffer.lookup_transform('world', foot_frame, rospy.Time())
    return footWorld.transform.translation.z


def waitForFootsteps(numberOfSteps):
    global stepCounter
    stepCounter = 0
    while stepCounter < numberOfSteps:
        rate.sleep()

    rospy.loginfo('finished set of steps')


def recievedFootStepStatus(msg):
    global stepCounter
    if msg.status == 1:
        stepCounter += 1
        

def waitForNavigation():
    global navigationFinished
    navigationFinished = False
    while navigationFinished == False:
        rate.sleep()

    rospy.loginfo('finished navigation')


def receivedNavigationStatus(msg):
    rospy.loginfo('received navigation status: [%s]' % msg)
    global navigationFinished
    if msg.data.startswith('0'):
        navigationFinished = True
    else:
        navigationFinished = False
        

def startWandering():
    msg = Bool()
    msg.data = True
    wanderingPublisher.publish(msg)


def stopWandering():
    msg = Bool()
    msg.data = False
    wanderingPublisher.publish(msg)
    
    rospy.loginfo('requesting wander stop')
    waitForNavigation()
    rospy.loginfo('wandering stopped')


def receivedBlueStripPosition(msg):
    global BlueStripPosition
    global pastblueStripPositions

#    rospy.loginfo("------- got blue position: [%f], [%f], [%f]" % (BlueStripPosition.x, BlueStripPosition.y, BlueStripPosition.z))
    
    setWheelPosition(msg, BlueStripPosition, pastblueStripPositions)
    
#    rospy.loginfo("[%f]" % pastblueStripPositions[2].x) 
#    rospy.loginfo("[%f]" % BlueStripPosition.x)  
    
#    rospy.loginfo("-------- wheel positions: [%f], [%f], [%f], [%f]" % (BlueStripPosition.x, BlueStripPosition.y, redButtonPosition.x, redButtonPosition.y))
    

def receivedredButtonPosition(msg):
    global redButtonPosition
    global pastredButtonPositions
    setWheelPosition(msg, redButtonPosition, pastredButtonPositions)    
#    rospy.loginfo("------- got red position: [%f], [%f], [%f]" % (redButtonPosition.x, redButtonPosition.y, redButtonPosition.z))
#    rospy.loginfo("-------- wheel positions: [%f], [%f], [%f], [%f]" % (BlueStripPosition.x, BlueStripPosition.y, redButtonPosition.x, redButtonPosition.y))


def setWheelPosition(msg, pos, posArr):
    msgPosition = Position()
    msgPosition.x = msg.x
    msgPosition.y = msg.y
    msgPosition.z = msg.z
    
#    print(len(posArr))
    
    shift(posArr)
    posArr[len(posArr) - 1] = msgPosition
    updatePositionAvg(pos, posArr)


def waitForTask1():
    global task1Finished
    task1Finished = False
    while task1Finished == False:
        rate.sleep()

    rospy.loginfo('finished task 1')


def receivedTaskStatus(msg):
    global task1Finished
    if msg.finished == True:
        task1Finished = True
        

def shift(arr):
    for i in range(0, len(arr)-1):
        arr[i] = arr[i+1]


def updatePositionAvg(pos, arr):
    sumX = 0.0
    sumY = 0.0
    sumZ = 0.0

#    rospy.loginfo("from avg [%f]" % arr[2].x) 
    
    for i in range(0, len(arr)):
#        rospy.loginfo("[%i] [%f]" % (i, arr[i].x)) 

        sumX += arr[i].x
        sumY += arr[i].y
        sumZ += arr[i].z

#    rospy.loginfo("from avg sumX [%f]" % sumX) 

    pos.x = sumX / len(arr)
    pos.y = sumY / len(arr)
    pos.z = sumZ / len(arr)

def sendArmTrajectory(mode):   
    if mode == 'PUSH':
        msg = ArmTrajectoryRosMessage()
        msg.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg = appendTrajectoryPoint(msg, 0.12, ELBOW_BENT_UP_MIDDLE)
        msg = appendTrajectoryPoint(msg, 0.20, ELBOW_BENT_UP)
        msg.unique_id = -1
        rospy.loginfo('publishing right PUSH trajectory')
        armTrajectoryPublisher.publish(msg)

    elif mode == 'PULL':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 0.5, ARM_PULLED_BACK_INIT2)
        msg2 = appendTrajectoryPoint(msg2, 1.0, ARM_DOWN_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right PULL trajectory')
        armTrajectoryPublisher.publish(msg2)
        
        rossleep(0.2)
        
        msg3 = ArmTrajectoryRosMessage()
        msg3.robot_side = ArmTrajectoryRosMessage.LEFT
        msg3 = appendTrajectoryPoint(msg3, 0.6, ARM_DOWN_LEFT)
        msg3.unique_id = -2
        rospy.loginfo('publishing left PULL trajectory')
        armTrajectoryPublisher.publish(msg3)

    elif mode == 'DOWN':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 1.0, ARM_DOWN_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right DOWN trajectory')
        armTrajectoryPublisher.publish(msg2)

        rossleep(0.35)
        
        msg3 = ArmTrajectoryRosMessage()
        msg3.robot_side = ArmTrajectoryRosMessage.LEFT
        msg3 = appendTrajectoryPoint(msg3, 1.0, ARM_DOWN_LEFT)
        msg3.unique_id = -2
        rospy.loginfo('publishing left DOWN trajectory')
        armTrajectoryPublisher.publish(msg3)

    elif mode == 'RIGHTDOWN':
        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 0.25, ARM_DOWN_RIGHT_INIT)
        msg2 = appendTrajectoryPoint(msg2, 0.60, ARM_DOWN_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right DOWN trajectory')
        armTrajectoryPublisher.publish(msg2)

    elif mode == 'SPREAD':
        msg1 = ArmTrajectoryRosMessage()
        msg1.robot_side = ArmTrajectoryRosMessage.LEFT
        msg1 = appendTrajectoryPoint(msg1, 0.60, ARM_SPREAD_LEFT)
        msg1.unique_id = -1
        rospy.loginfo('publishing left SPREAD trajectory')
        armTrajectoryPublisher.publish(msg1)

        rossleep(0.35)

        msg2 = ArmTrajectoryRosMessage()
        msg2.robot_side = ArmTrajectoryRosMessage.RIGHT
        msg2 = appendTrajectoryPoint(msg2, 0.60, ARM_SPREAD_RIGHT)
        msg2.unique_id = -1
        rospy.loginfo('publishing right SPREAD trajectory')
        armTrajectoryPublisher.publish(msg2)


def appendTrajectoryPoint(arm_trajectory, time, positions):
    if not arm_trajectory.joint_trajectory_messages:
        arm_trajectory.joint_trajectory_messages = [copy.deepcopy(OneDoFJointTrajectoryRosMessage()) for i in range(len(positions))]
    for i, pos in enumerate(positions):
        point = TrajectoryPoint1DRosMessage()
        point.time = time
        point.position = pos
        point.velocity = 0
        arm_trajectory.joint_trajectory_messages[i].trajectory_points.append(point)
    return arm_trajectory


def createNeckTrajectoryMessage(arr, secs):
    neckTrajPoint1 = TrajectoryPoint1DRosMessage()
    neckTrajPoint1.time = secs
    neckTrajPoint1.position = arr[0]
    neckTraj1 = OneDoFJointTrajectoryRosMessage()
    neckTraj1.trajectory_points.append(neckTrajPoint1)

    neckTrajPoint2 = TrajectoryPoint1DRosMessage()
    neckTrajPoint2.time = secs
    neckTrajPoint2.position = arr[1]
    neckTraj2 = OneDoFJointTrajectoryRosMessage()
    neckTraj2.trajectory_points.append(neckTrajPoint2)

    neckTrajPoint3 = TrajectoryPoint1DRosMessage()
    neckTrajPoint3.time = secs
    neckTrajPoint3.position = arr[2]
    neckTraj3 = OneDoFJointTrajectoryRosMessage()
    neckTraj3.trajectory_points.append(neckTrajPoint3)
    
    neckMsg = NeckTrajectoryRosMessage()
    neckMsg.unique_id = -1
    neckMsg.joint_trajectory_messages.append(neckTraj1)
    neckMsg.joint_trajectory_messages.append(neckTraj2)
    neckMsg.joint_trajectory_messages.append(neckTraj3)

    return neckMsg


def createPelvisMsg(height, secs):
    pelvisMsg = PelvisHeightTrajectoryRosMessage()
    pelvisMsg.execution_mode = 0
    pelvisMsg.unique_id = -1
    
    pelvisTraj = TrajectoryPoint1DRosMessage()
    pelvisTraj.time = secs
    pelvisTraj.position = height
    pelvisTraj.velocity = 0.0
    
    pelvisMsg.trajectory_points.append(pelvisTraj)
    
    return pelvisMsg


def rossleep(time):
    try:
        seconds = float(time)
    except (ValueError):
        print("Couldn't convert argument [%s] to float" % sys.argv[1])
    rospy.sleep(rospy.Duration.from_sec(seconds))
    

def shutDownHookWrapper(signal, frame):
    shutDownHook()
    

def shutDownHook():
    
    global blueDetectorProcess
    global redDetectorProcess
    global blueReconstProcess
    global redReconstProcess

    rospy.loginfo("shutdown hook called...")
    if blueDetectorProcess is not None:
        rospy.loginfo("killing blue detector...")
        os.killpg(os.getpgid(blueDetectorProcess.pid), signal.SIGKILL)
    if redDetectorProcess is not None:
        rospy.loginfo("killing red detector...")
        os.killpg(os.getpgid(redDetectorProcess.pid), signal.SIGKILL)
    if blueReconstProcess is not None:
        rospy.loginfo("killing blue reconst...")
        os.killpg(os.getpgid(blueReconstProcess.pid), signal.SIGKILL)
    if redReconstProcess is not None:
        rospy.loginfo("killing red reconst...")
        os.killpg(os.getpgid(redReconstProcess.pid), signal.SIGKILL)
    sys.exit(0)


if __name__ == '__main__':
    global BlueStripPosition
    global redButtonPosition
    global navigationFinished
    global stepCounter
    global neckTrajectoryPublisher, headTrajectoryPublisher
    BlueStripPosition = Position()
    redButtonPosition = Position()
    navigationFinished = False
    stepCounter = 0
    
    signal.signal(signal.SIGINT, shutDownHookWrapper)
    
    try:
        rospy.init_node('ihmc_walk_test')

        if not rospy.has_param('/ihmc_ros/robot_name'):
            rospy.logerr("Cannot run walk_test.py, missing parameters!")
            rospy.logerr("Missing parameter '/ihmc_ros/robot_name'")

        else:
            ROBOT_NAME = rospy.get_param('/ihmc_ros/robot_name')

            right_foot_frame_parameter_name = "/ihmc_ros/{0}/right_foot_frame_name".format(ROBOT_NAME)
            left_foot_frame_parameter_name = "/ihmc_ros/{0}/left_foot_frame_name".format(ROBOT_NAME)

            if rospy.has_param(right_foot_frame_parameter_name) and rospy.has_param(left_foot_frame_parameter_name):
                RIGHT_FOOT_FRAME_NAME = rospy.get_param(right_foot_frame_parameter_name)
                LEFT_FOOT_FRAME_NAME = rospy.get_param(left_foot_frame_parameter_name)

                footStepStatusSubscriber = rospy.Subscriber("/ihmc_ros/{0}/output/footstep_status".format(ROBOT_NAME), FootstepStatusRosMessage, recievedFootStepStatus)
                blueStripPositionSubscrber = rospy.Subscriber("/srcsim/vision/blue_strip", Console, receivedBlueStripPosition)
                redButtonPositionSubscrber = rospy.Subscriber("/srcsim/vision/red_button", Console, receivedredButtonPosition)
                navigationStatusSubscriber = rospy.Subscriber("/humanz/status", String, receivedNavigationStatus)
                taskStatusSubscriber = rospy.Subscriber("/srcsim/finals/task", Task, receivedTaskStatus)
                
                footStepListPublisher = rospy.Publisher("/ihmc_ros/{0}/control/footstep_list".format(ROBOT_NAME), FootstepDataListRosMessage, queue_size=20)
                footTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/foot_trajectory".format(ROBOT_NAME), FootTrajectoryRosMessage, queue_size=20)
                pelvisHeightTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/pelvis_height_trajectory".format(ROBOT_NAME), PelvisHeightTrajectoryRosMessage, queue_size=20)
                wholeBodyTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/whole_body_trajectory".format(ROBOT_NAME), WholeBodyTrajectoryRosMessage, queue_size=50)
                armTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/arm_trajectory".format(ROBOT_NAME), ArmTrajectoryRosMessage, queue_size=1)
                neckTrajectoryPublisher = rospy.Publisher("/ihmc_ros/{0}/control/neck_trajectory".format(ROBOT_NAME), NeckTrajectoryRosMessage, queue_size=1)
                headTrajectoryPublisher = rospy.Publisher('/ihmc_ros/{0}/control/head_trajectory'.format(ROBOT_NAME), HeadTrajectoryRosMessage, queue_size=1)                
                pathPublisher = rospy.Publisher("/humanz/walk_path", String, queue_size=1)
                navigationPublisher = rospy.Publisher("/humanz/navigate", String, queue_size=1)
                wanderingPublisher = rospy.Publisher("/humanz/wandering", Bool, queue_size=1)

                tfBuffer = tf2_ros.Buffer()
                tfListener = tf2_ros.TransformListener(tfBuffer)

                rate = rospy.Rate(10) # 10hz
                time.sleep(1)

                # make sure the simulation is running otherwise wait
                if footStepListPublisher.get_num_connections() == 0:
                    rospy.loginfo('waiting for subsciber...')
                    while footStepListPublisher.get_num_connections() == 0:
                        rate.sleep()

                if not rospy.is_shutdown():
                    walkTest()
            else:
                if not rospy.has_param(left_foot_frame_parameter_name):
                    rospy.logerr("Missing parameter {0}".format(left_foot_frame_parameter_name))
                if not rospy.has_param(right_foot_frame_parameter_name):
                    rospy.logerr("Missing parameter {0}".format(right_foot_frame_parameter_name))

    except rospy.ROSInterruptException:
        pass
